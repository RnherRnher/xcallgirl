xcallgirl
Đây là dự án dành cho mục đích nghiên cứu học tập.
Ứng dụng web cung cấp dịch vụ giao dịch khiêu dâm.
Lấy ý tưởng từ các ứng dụng gọi xe.

Installation

Docker
Production
Build: sudo docker build -t prod/xcallgirl .
Run: sudo docker run -it prod/xcallgirl
Development
Build: sudo docker build -t dev/xcallgirl .
Run: sudo docker run -it dev/xcallgirl

Terminal
Production
yarn start:prod
Development
yarn start

Documentation
docs/WIKI.md

Note
Ứng dụng tối ưu hóa với giao diện di động.
