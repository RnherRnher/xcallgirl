module.exports = {
  client: {
    lib: {
      js: ['public/lib/app-*.js'],
      css: ['public/lib/app-*.css'],
    },
    js: [
      'modules/*/client/**/*.js',
      'commons/**/*.js',
    ],
    components: ['modules/*/client/components/**/*.jsx'],
    sass: ['modules/*/client/sass/**/*.sass'],
    images: [
      'modules/*/client/images/**/*.jpg',
      'modules/*/client/images/**/*.png',
      'modules/*/client/images/**/*.gif',
      'modules/*/client/images/**/*.svg',
      'modules/*/client/images/**/*.ico',
    ],
  },
  server: {
    gulpConfig: ['gulpfile.js'],
    allJS: [
      'config/**/*.js',
      'modules/*/server/**/*.js',
      'utils/**/*.js',
      'server.js',
      'commons/**/*.js',
    ],
    config: ['modules/*/server/config/*.js'],
    policies: ['modules/*/server/policies/*.js'],
    routes: ['modules/*/server/routes/**/*.js'],
    models: ['modules/*/server/models/**/*.js'],
    sockets: ['modules/*/server/sockets/**/*.js'],
    crons: ['modules/*/server/crons/**/*.js'],
    template: 'modules/core/server/views/index.server.view.html',
  },
};
