const path = require('path');
const constant = require(path.resolve('./commons/utils/constant'));

module.exports = {
  /* APP */
  app: {
    title: 'XCallgirl',
    description: {
      [constant.VI]: 'XCallgirl là ứng dụng cung cấp các dịch vụ sex "uy tính - chuyên nghiệp - chất lượng" hàng đầu thế giới',
    },
    keywords: 'xcallgirl.com, www.xcallgirl, xcallgirl, x call girl, callgirl, call girl, sexservices, sex services, sex, services',
    languages: [constant.VI],
    language: constant.VI,
    apiVersion: 0,
    config: {
      searchLimit: 15,
      loadLimit: 20,
      businessCard: {
        topNewUpLimit: 6,
        topActivityLimit: 4,
      },
    }
  },
  /* Server */
  viewEngine: 'server.view.html',
  livereload: false,
  favicon: 'public/images/core/client/images/brand/favicon.ico',
  session: {
    key: 'SID',
    secret: process.env.SESSION_SECRET || 'xcallgirl',
    collection: 'sessions',
    saveUninitialized: true,
    resave: true,
    cookie: {
      path: '/',
      httpOnly: true,
      secure: false,
      maxAge: 6 * constant.ENUM.TIME.ONE_MONTH,
    },
  },
  /* Database */
  db: {
    options: {
      useNewUrlParser: true,
    },
    promise: global.Promise,
  },
  /* Security */
  helmet: {
    csp: {
      directives: {
        defaultSrc: ['\'self\''],
        childSrc: ['\'self\''],
        connectSrc: ['\'self\''],
        fontSrc: [
          '\'self\'',
          'https://at.alicdn.com',
        ],
        frameSrc: ['\'self\''],
        imgSrc: [
          '\'self\'',
          'https://res.cloudinary.com',
          'data:'
        ],
        manifestSrc: ['\'self\''],
        mediaSrc: ['\'self\''],
        objectSrc: ['\'self\''],
        scriptSrc: [
          '\'self\'',
          '\'unsafe-eval\'',
          '\'unsafe-inline\'',
        ],
        styleSrc: [
          '\'self\'',
          '\'unsafe-inline\'',
        ],
        workerSrc: ['\'self\''],
        baseUri: ['\'self\''],
        sandbox: [
          'allow-forms',
          'allow-modals',
          'allow-orientation-lock',
          'allow-scripts',
          'allow-top-navigation',
          'allow-same-origin',
        ],
        formAction: ['\'self\''],
        frameAncestors: ['\'self\''],
        requireSriFor: ['script', 'style'],
        blockAllMixedContent: true,
      },
    },
    frameguard: {
      action: 'same-origin',
    },
    referrerPolicy: {
      policy: 'same-origin',
    },
    hsts: {
      maxAge: 6 * constant.ENUM.TIME.ONE_MONTH,
      includeSubdomains: true,
      preload: true,
      setIf: ((req, res) => {
        if (req.secure) return true;
        return false;
      }),
    },
    hpkp: {
      maxAge: 6 * constant.ENUM.TIME.ONE_MONTH,
      sha256s: [
        '6c7001ac631c7b63536fe23e8306b6b874ab8ea55fc4305930f12f2dc78c4b89',
        '736aab9a0c737b65d0c5a3fcb21de315f2547cbb309c3cdc2a01b4b536f5145a',
        'ce35829ecfb8ed4e7e4606afa7d72ae6cf861968350a35dd1febea608d7ff4a4',
        '505f60290d65689aea0d2401be6ce0a0bcdab680cb398806f5ec399cc436ff43',
        '9c7e7e5bf1a2d5d8f1115c3eda0dbd6d1a050e99e6763a704b14962a39819bb4',
      ],
      setIf: (req, res) => {
        if (req.secure) return true;
        return false;
      },
    },
  },
  /* Logger */
  log: {
    disable: process.env.DISABLE_LOG === 'true',
    validFormats: ['combined', 'common', 'dev', 'short', 'tiny'],
    fileLogger: {
      directoryPath: process.env.LOG_DIR_PATH || 'logs',
      maxsize: 10 * constant.ENUM.MEMORY_STORAGE.ONE_MB,
      maxFiles: 2,
      tailable: true,
      times: 'DD-MM-YYYY HH:mm:ss',
      eol: '\n',
      formatString: info => `${info.timestamp} ${info.level}: ${info.message}`,
    },
    consoleLogger: {
      colorize: true,
      times: 'DD-MM-YYYY HH:mm:ss',
      eol: '\n',
      formatString: info => `${info.timestamp} ${info.level}: ${info.message}`,
    },
  },
  /* API */
  cloudinary: {
    cloudName: 'xcallgirl',
    api: {
      key: '127157569777932',
      secret: '6mK3mOb0wr9R_x-XXeIQjym7YVU',
    },
  },
  uploads: {
    profile: {
      image: {
        names: [constant.AVATAR, constant.VERIFY_ACCOUNT],
        mimetypes: ['image/jpeg', 'image/png'],
        size: 2 * constant.ENUM.MEMORY_STORAGE.ONE_MB,
        format: {
          type: constant.AUTO,
          width: '1280',
          height: '960',
          getPublicID: (directoryPath, userID, fieldname) => {
            return `${directoryPath}/users/${userID}/profile/${fieldname}`;
          },
          getTags: (userID, fieldname, mimetype, date) => {
            return [
              'type-profile',
              `userID-${userID}`,
              `name-${fieldname}`,
              `mimetype-${mimetype}`,
              `date-${date.getDate()}/${date.getMonth()}/${date.getFullYear()}`,
            ];
          },
        },
      },
    },
    businessCard: {
      image: {
        names: [constant.BACKGROUND, constant.FACE, constant.BREAST, constant.HIPS, constant.WAIST, constant.BODY],
        mimetypes: ['image/jpeg', 'image/png'],
        size: 2 * constant.ENUM.MEMORY_STORAGE.ONE_MB,
        format: {
          type: constant.AUTO,
          width: '1280',
          height: '960',
          getPublicID: (directoryPath, userID, businessCardID, fieldname) => {
            return `${directoryPath}/users/${userID}/business_card/${businessCardID}/${fieldname}`;
          },
          getTags: (userID, businessCardID, fieldname, mimetype, date) => {
            return [
              'type-businessCard',
              `userID-${userID}`,
              `businessCardID-${businessCardID}`,
              `name-${fieldname}`,
              `mimetype-${mimetype}`,
              `date-${date.getDate()}/${date.getMonth()}/${date.getFullYear()}`,
            ];
          },
        },
      },
    },
  },
};
