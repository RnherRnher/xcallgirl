module.exports = {
  /* APP */
  domain: 'http://127.0.0.1:3000',
  port: process.env.PORT || 3000,
  host: process.env.HOST || '127.0.0.1',
  /* Database */
  db: {
    uri: `${process.env.MONGOHQ_URL || process.env.MONGODB_URI || 'mongodb://'}${(process.env.DB_1_PORT_27017_TCP_ADDR || 'localhost:27017')}/xcallgirl-dev`,
    debug: process.env.MONGODB_DEBUG || false,
  },
  seedDB: {
    seed: process.env.MONGO_SEED === 'true',
    options: {
      logResults: process.env.MONGO_SEED_LOG_RESULTS === 'true',
    },
  },
  /* Logger */
  log: {
    format: process.env.LOG_FORMAT || 'dev',
    fileLogger: {
      fileName: process.env.LOG_FILE || 'app-dev.log',
    },
  },
  /* API */
  cloudinary: {
    resource: 'dev',
  },
  twilio: {
    accountSid: 'AC0894e6ad5a2171ad4d0787ffe96eea48',
    authToken: '8b918b7e7899cf71478f9a9afa4850aa',
    numberPhone: '+15005550006',
  },
};
