module.exports = {
  /* APP */
  domain: 'https://0.0.0.0:443',
  port: process.env.PORT || 443,
  host: process.env.HOST || '0.0.0.0',
  secure: {
    ssl: true,
    privateKey: './config/sslcerts/key.pem',
    certificate: './config/sslcerts/cert.crt',
    caBundle: './config/sslcerts/ca.crt',
  },
  session: {
    secret: process.env.SESSION_SECRET || 'WlVkT2FHxSkhslRzVoV0Vweg=',
    cookie: {
      secure: true,
    },
  },
  /* Database */
  db: {
    uri: `${process.env.MONGOHQ_URL || process.env.MONGODB_URI || 'mongodb://'}${(process.env.DB_1_PORT_27017_TCP_ADDR || 'localhost:27017')}/xcallgirl`,
    // uri: `${process.env.MONGOHQ_URL || process.env.MONGODB_URI || 'mongodb+srv://xcallgirl:eGNhbGxxnaXJs@'}${(process.env.DB_1_PORT_27017_TCP_ADDR || 'main-wkrx6.mongodb.net')}/xcallgirl?retryWrites=true`,
    debug: process.env.MONGODB_DEBUG || false,
  },
  /* Security */
  helmet: {
    csp: {
      directives: {
        upgradeInsecureRequests: true,
      },
    },
  },
  /* Logger */
  log: {
    format: process.env.LOG_FORMAT || 'combined',
    fileLogger: {
      fileName: process.env.LOG_FILE || 'app.log',
    },
  },
  /* API */
  cloudinary: {
    resource: 'prod',
  },
  twilio: {
    accountSid: 'ACfe34d65a86c1f53d683903d45150f838',
    authToken: '5ebfb8aaa67dcf2522fc8f3abae845f3',
    numberPhone: '+13522041981',
  },
};
