module.exports = {
  /* APP */
  domain: 'http://127.0.0.1:3001',
  port: process.env.PORT || 3001,
  host: process.env.HOST || '127.0.0.1',
  /* Database */
  db: {
    uri: `${process.env.MONGOHQ_URL || process.env.MONGODB_URI || 'mongodb://'}${(process.env.DB_1_PORT_27017_TCP_ADDR || 'localhost:27017')}/xcallgirl-test`,
    debug: process.env.MONGODB_DEBUG || false,
  },
  /* Logger */
  log: {
    format: process.env.LOG_FORMAT || 'short',
    fileLogger: {
      fileName: process.env.LOG_FILE || 'app-test.log',
    },
  },
  /* API */
  cloudinary: {
    resource: 'test',
  },
};
