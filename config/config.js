const _ = require('lodash');
const path = require('path');
const glob = require('../utils/glob');
const validate = require('../utils/validate');

/**
 * Khởi tạo cấu toàn cục
 *
 * @returns {Object}
 */
const initGlobalConfig = () => {
  validate.validateEnvironmentVariable();

  const defaultAssets = require(path.join(process.cwd(), 'config/assets/default'));
  const environmentAssets = require(path.join(process.cwd(), 'config/assets/', process.env.NODE_ENV)) || {};
  const assets = _.merge(defaultAssets, environmentAssets);

  const defaultConfig = require(path.join(process.cwd(), 'config/env/default'));
  const environmentConfig = require(path.join(process.cwd(), 'config/env/', process.env.NODE_ENV)) || {};
  const pkg = require(path.resolve('./package.json'));

  const config = _.merge(defaultConfig, environmentConfig);
  config.xcallgirl = pkg;

  glob.initGlobalConfigFiles(config, assets);
  glob.initGlobalConfigFolders(config, assets);

  validate.validateConfigFile(config);
  validate.validateSecureMode(config);
  validate.validateSessionSecret(config);
  validate.validateLocales(config);

  return config;
};
module.exports = initGlobalConfig();
