const config = require('../config');
const chalk = require('chalk');
const cloudinary = require('cloudinary');

const { resource } = config.cloudinary;
const formatProfileImage = config.uploads.profile.image.format;
const formatBusinessCardImage = config.uploads.businessCard.image.format;

/**
 *@overview  Khởi tạo
 */
cloudinary.config({
  cloud_name: config.cloudinary.cloudName,
  api_key: config.cloudinary.api.key,
  api_secret: config.cloudinary.api.secret,
});

/**
 * @overview Tải hình lên cho hồ sơ người dùng
 *
 * @param    {ObjectId} userID
 * @param    {Multer.File} imageFile
 * @returns  {Promise}
 */
const uploadProfileImage = (userID, imageFile) => {
  return new Promise((resolve, reject) => {
    cloudinary.v2.uploader.upload(
      `data:${imageFile.mimetype};base64,${imageFile.buffer.toString('base64')}`,
      {
        resource_type: formatProfileImage.type,
        public_id: formatProfileImage.getPublicID(resource, userID, imageFile.fieldname),
        width: formatProfileImage.width,
        height: formatProfileImage.height,
        tags: formatProfileImage.getTags(userID, imageFile.fieldname, imageFile.mimetype, new Date()),
      },
      (err, res) => {
        if (err) {
          return reject(err);
        }

        return resolve(res);
      }
    );
  });
};
module.exports.uploadProfileImage = uploadProfileImage;

/**
 * @overview Tải hình lên cho danh thiếp
 *
 * @param    {ObjectId} userID
 * @param    {Multer.File} imageFile
 * @returns  {Promise}
 */
const uploadBusinessCardImage = (userID, businessCardID, imageFile) => {
  return new Promise((resolve, reject) => {
    cloudinary.v2.uploader.upload(
      `data:${imageFile.mimetype};base64,${imageFile.buffer.toString('base64')}`,
      {
        resource_type: formatBusinessCardImage.type,
        public_id: formatBusinessCardImage.getPublicID(resource, userID, businessCardID, imageFile.fieldname),
        width: formatBusinessCardImage.width,
        height: formatBusinessCardImage.height,
        tags: formatBusinessCardImage.getTags(userID, businessCardID, imageFile.fieldname, imageFile.mimetype, new Date()),
      },
      (err, res) => {
        if (err) {
          return reject(err);
        }

        return resolve(res);
      }
    );
  });
};
module.exports.uploadBusinessCardImage = uploadBusinessCardImage;

/**
 * @overview Xoá hình theo publicID
 *
 * @param    {String} publicID
 * @returns  {Promise}
 */
const deleteImageByPublicID = (publicID) => {
  return new Promise((resolve, reject) => {
    cloudinary.v2.uploader.destroy(
      publicID,
      {
        invalidate: true,
      },
      (err, res) => {
        if (err) {
          return reject(err);
        }

        return resolve(res);
      }
    );
  });
};
module.exports.deleteImageByPublicID = deleteImageByPublicID;

/**
 * @override Xoá tài nguyên theo tên
 *
 * @param {String} resourceName
 * @returns  {Promise}
 */
const deleteResources = (resourceName) => {
  return new Promise((resolve, reject) => {
    cloudinary.v2.api.delete_resources_by_prefix(resourceName, (err, res) => {
      if (err) {
        console.log();
        console.error(chalk.bold.red(`+ Error: Delete resource ${resourceName} from cloudinary`));
        console.error(chalk.bold.red(JSON.stringify(err)));
        console.log();

        return reject(err);
      }

      console.log(chalk.bold.green(`Successfully delete resource ${resourceName} from cloudinary`));
      return resolve(res);
    });
  });
};
module.exports.deleteResources = deleteResources;
