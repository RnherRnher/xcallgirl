const _ = require('lodash');
const multer = require('multer');
const path = require('path');
const constant = require(path.resolve('./commons/utils/constant'));
const config = require('../config');

/**
 * @overview  Kiểm tra xác nhận tên tập tin hợp lệ cho hình
 *
 * @param    {Object} configUploadsImage
 * @param    {Object} files
 * @returns  {Boolean}
 */
multer.testUploadImageName = (configUploadsImage, files) => {
  let error = false;

  if (!_.isArray(files)) {
    error = _.includes(configUploadsImage.names, files.fieldname);
  } else {
    error = !files.some((file) => {
      if (!_.includes(configUploadsImage.names, file.fieldname)) {
        return true;
      }

      return false;
    });
  }

  return error;
};

/**
 * @overview Kiểm tra định dạng tập tin hợp lệ cho hình
 *
 * @param    {Object} configUploadsImage
 * @param    {Object} files
 * @returns  {Boolean}
 */
multer.testUploadImageFormat = (configUploadsImage, files) => {
  let error = false;

  if (!_.isArray(files)) {
    error = _.includes(configUploadsImage.mimetypes, files.mimetype);
  } else {
    error = !files.some((file) => {
      if (!_.includes(configUploadsImage.mimetypes, file.mimetype)) {
        return true;
      }

      return false;
    });
  }

  return error;
};

/**
 * @overview Kiểm tra kích cở tập tin hợp lệ cho hình
 *
 * @param    {Object} configUploadsImage
 * @param    {Object} files
 * @returns  {Boolean}
 */
multer.testUploadImageSize = (configUploadsImage, files) => {
  let error = false;

  if (!_.isArray(files)) {
    error = files.size < configUploadsImage.size;
  } else {
    error = !files.some((file) => {
      if (file.size > configUploadsImage.size) {
        return true;
      }

      return false;
    });
  }

  return error;
};

/**
 * @overview Kiểm tra tập tin hợp lệ cho hình
 *
 * @param    {String} type
 * @param    {Object} files
 * @returns  {null||new Error()}
 */
multer.testUploadImage = (type, file) => {
  const errors = [];

  let configUploadsImage = null;
  switch (type) {
    case constant.PROFILE:
      configUploadsImage = config.uploads.profile.image;
      break;
    case constant.BUSINESS_CARD:
      configUploadsImage = config.uploads.businessCard.image;
      break;
    default:
      break;
  }

  if (!multer.testUploadImageFormat(configUploadsImage, file)) {
    errors.push('File format not valid');
  }

  if (!multer.testUploadImageName(configUploadsImage, file)) {
    errors.push('File name not valid');
  }

  if (!multer.testUploadImageSize(configUploadsImage, file)) {
    errors.push('File size not valid');
  }

  return _.isEmpty(errors) ? null : new Error(errors);
};

module.exports = multer;
