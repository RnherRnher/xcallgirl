﻿const chalk = require('chalk');
const _ = require('lodash');
const mongoose = require('mongoose');
const config = require('../config');
const collections = require('../../utils/collections');

/**
 * @overview Tạo mẫu cơ sở dữ liệu
 *
 * @param    {[]|[{Object}]} collection
 * @param    {Object} options
 * @returns  {Promise}
 */
const seed = (collection, options) => {
  // Hợp nhất các tùy chọn với các tùy chọn thu thập
  options = _.merge(options || {}, collection.options || {});

  return new Promise((resolve, reject) => {
    const Model = mongoose.model(collection.model);
    const { docs } = collection;

    const skipWhen = collection.skip ? collection.skip.when : null;

    if (!Model.seed) {
      return reject(new Error(`Database Seeding: Invalid Model Configuration - ${collection.model}.seed() not implemented`));
    }

    if (!docs || !docs.length) {
      return resolve();
    }

    const skipCollection = () => {
      return new Promise((resolve, reject) => {
        if (!skipWhen) {
          return resolve(false);
        }

        Model
          .find(skipWhen)
          .exec((err, results) => {
            if (err) {
              return reject(err);
            }

            if (results && results.length) {
              return resolve(true);
            }

            return resolve(false);
          });
      });
    };

    const seedDocuments = (skipCollection) => {
      return new Promise((resolve, reject) => {
        // Đóng cửa cục bộ

        function onComplete(responses) {
          if (options.logResults) {
            responses.forEach((response) => {
              if (response.message) {
                console.log(chalk.bold.magenta(response.message));
              }
            });
          }

          return resolve();
        }

        function onError(err) {
          return reject(err);
        }

        if (skipCollection) {
          return onComplete([{ message: chalk.yellow(`Database Seeding: ${collection.model} collection skipped`) }]);
        }

        const workload = docs
          .filter((doc) => {
            return doc.data;
          })
          .map((doc) => {
            return Model.seed(doc.data, { overwrite: doc.overwrite });
          });

        Promise.all(workload)
          .then(onComplete)
          .catch(onError);
      });
    };

    // Trước tiên hãy kiểm tra xem chúng tôi có nên bỏ qua bộ sưu tập này không
    // dựa trên tùy chọn "skip.when" của bộ sưu tập.
    // LƯU Ý: Nếu nó tồn tại, "skip.when" phải là một trình độ
    // Truy vấn Mongoose sẽ được sử dụng với Model.find ().
    skipCollection()
      .then(seedDocuments)
      .then(() => {
        return resolve();
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

/**
 * @overview Bắt đầu tạo mẫu cơ sở dự liệu
 *
 * @param    {Object} seedConfig
 * @returns  {Promise}
 */
const start = (seedConfig) => {
  return new Promise((resolve, reject) => {
    seedConfig = seedConfig || {};

    const options = seedConfig.options || (config.seedDB ? _.cloneDeep(config.seedDB.options) : {});

    if (!collections.length) {
      return resolve();
    }

    // Trình xử lý lời hứa địa phương
    const onSuccessComplete = () => {
      if (options.logResults) {
        console.log();
        console.log(chalk.bold.green('Database Seeding: Mongo Seed complete!'));
        console.log();
      }

      return resolve();
    };

    const onError = (err) => {
      if (options.logResults) {
        console.log();
        console.error(chalk.bold.red('Database Seeding: Mongo Seed Failed!'));
        console.error(chalk.bold.red(`Database Seeding: ${err}`));
        console.log();
      }

      return reject(err);
    };

    const seeds = collections
      .filter((collection) => {
        return collection.model;
      });

    // Sử dụng mẫu giảm để đảm bảo chúng tôi xử lý việc gieo hạt theo thứ tự mong muốn
    seeds.reduce((p, item) => {
      return p.then(() => {
        return seed(item, options);
      });
    }, Promise.resolve()) // Bắt đầu với lời hứa đã được giải quyết cho mục trước (p) ban đầu
      .then(onSuccessComplete)
      .catch(onError);
  });
};
module.exports.start = start;
