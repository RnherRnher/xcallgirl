const chalk = require('chalk');
const path = require('path');
const mongoose = require('mongoose');
const config = require('../config');

/**
 * @overview Tải các mô hình mongoose
 *
 * @param {Function} callback
 */
const loadModels = (callback) => {
  // Tệp mô hình toàn cầu
  config.files.server.models.forEach((modelPath) => {
    require(path.resolve(modelPath));
  });

  if (callback) callback();
};
module.exports.loadModels = loadModels;

/**
 * @overview Kết nối DB
 *
 * @param {Function} callback
 */
const connect = (callback) => {
  mongoose.Promise = config.db.promise;

  mongoose
    .connect(config.db.uri, config.db.options)
    .then(() => {
      // Bật chế độ gỡ lỗi mongoose nếu cần
      mongoose.set('debug', config.db.debug);

      // Gọi lại gọi FN
      if (callback) callback(mongoose.connection.db);
    })
    .catch((err) => {
      console.log();
      console.error(chalk.red('+ Error: Could not connect to MongoDB!'));
      console.error(chalk.red(err));
      console.log();
    });
};
module.exports.connect = connect;

/**
 * @overview Ngắt kết nối DB
 *
 * @param {Function} callback
 */
const disconnect = (callback) => {
  mongoose.connection.close((err) => {
    console.info(chalk.yellow('Disconnected from MongoDB'));

    return callback(err);
  });
};
module.exports.disconnect = disconnect;
