const twilioCore = require('twilio');
const path = require('path');
const config = require('../config');
const logger = require(path.resolve('./config/lib/logger'));
const constant = require(path.resolve('./commons/utils/constant'));

/**
 * @overview Khởi tạo
 */
const twilio = {
  core: process.env.NODE_ENV === 'test'
    ? null
    : twilioCore(config.twilio.accountSid, config.twilio.authToken)
};

/**
 * @overview Lấy tin nhắn
 *
 * @param   {String} type
 * @param   {Object} data
 * @returns {String}
 */
twilio.getMsg = function (type, data) {
  switch (data.language) {
    case constant.VI:
      switch (type) {
        case constant.TOKEN:
          return `XCALLGIRL mã xác thực : ${data.token}`;
        case constant.INVITE:
          switch (data.type) {
            case constant.BUSINESS_CARD_DEAL_HISTORY:
              return 'XCALLGIRL có khách hàng yêu cầu dịch vụ từ bạn. Bạn hãy phản hồi lại tại http://www.xcallgirl.com';
            default:
              return '';
          }
        default:
          return '';
      }
    default:
      return '';
  }
};

/**
 * @overview Gửi tin nhắn
 *
 * @param   {String} type
 * @param   {Object} numberPhone
 * @param   {Object} data
 * @returns {Promise}
 */
twilio.sendSMS = function (type, numberPhone, data) {
  if (this.core) {
    const options = {
      body: this.getMsg(type, data),
      to: numberPhone.code + numberPhone.number,
      from: config.twilio.numberPhone,
    };

    return this.core.messages
      .create(options)
      .catch((err) => {
        logger.logError(
          'twilio',
          'sendSMS',
          'twilio',
          options,
          err
        );
      });
  }

  return false;
};

module.exports = twilio;
