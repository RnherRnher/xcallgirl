const chalk = require('chalk');
const config = require('../config');
const express = require('./express');
const mongooseService = require('./mongoose');
const seed = require('./mongo-seed');

/**
 * @overview Khởi chạy tạo dữ liệu mẫu
 *
 */
const seedDB = () => {
  if (config.seedDB && config.seedDB.seed) {
    console.log(chalk.bold.red('Warning:  Database seeding is turned on'));
    seed.start();
  }
};

/**
 * @overview Khởi tạo ứng dụng
 *
 * @param {Function?} callback
 */
const init = (callback) => {
  mongooseService.connect((db) => {
    // Khởi tạo mô hình
    mongooseService.loadModels(seedDB);

    // Khởi tạo Express
    const app = express.init(db);
    if (callback) callback(app, db, config);
  });
};
module.exports.init = init;

/**
 * @overview Chạy ứng dụng
 *
 * @param {Function?} callback
 */
const start = function (callback) {
  this.init((app, db, config) => {
    // Khởi động ứng dụng bằng cách nghe <port> tại <host>
    app.listen(config.port, config.host, () => {
      // Tạo URL máy chủ
      const server = `${(config.secure && config.secure.ssl === true ? 'https://' : 'http://')}${config.host}:${config.port}`;
      // Đang khởi tạo nhật ký
      console.log('--');
      console.log(chalk.green(config.app.title));
      console.log();
      console.log(chalk.green(`Environment:         '${process.env.NODE_ENV}'`));
      console.log(chalk.green(`Server:              '${server}'`));
      console.log(chalk.green(`Database:            '${config.db.uri}'`));
      console.log(chalk.green(`App version:         '${config.xcallgirl.version}'`));
      if (config.xcallgirl['xcallgirl-version']) {
        console.log(chalk.green(`xcallgirl version:   '${config.xcallgirl['xcallgirl-version']}'`));
      }
      console.log('--');

      if (callback) callback(app, db, config);
    });
  });
};
module.exports.start = start;
