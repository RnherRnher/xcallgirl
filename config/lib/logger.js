const chalk = require('chalk');
const _ = require('lodash');
const fs = require('fs');
const winston = require('winston');
const config = require('../config');

/**
 * @override Khởi tạo trình ghi nhật ký ứng dụng winston mặc định bằng Bảng điều khiển vận chuyển
 */
const configConsoleLogger = config.log.consoleLogger;
const logger = winston.createLogger({
  transports: [
    new winston.transports.Console({
      eol: configConsoleLogger.eol,
      format: winston.format.combine(
        winston.format.colorize({ all: configConsoleLogger.colorize }),
        winston.format.timestamp({ format: configConsoleLogger.times }),
        winston.format.printf(configConsoleLogger.formatString)
      ),
    }),
  ],
});

/**
 * @override Một đối tượng luồng có chức năng ghi sẽ gọi winston tích hợp logger.info () chức năng.
 *  Hữu ích khi tích hợp với cơ chế liên quan đến luồng như luồng của Morgan
 *  tùy chọn để ghi lại tất cả các yêu cầu HTTP vào một tệp
 */
logger.stream = {
  write: (msg) => {
    logger.info(msg);
  },
};

/**
 * @override Các tùy chọn để sử dụng với winston logger
 *
 * @returns {Object} Trả về một đối tượng Winston để ghi nhật ký bằng cách vận chuyển Tệp
 */
logger.getLogOptions = function () {
  const configFileLogger = config.log.fileLogger;
  const logPath = `${configFileLogger.directoryPath}/${configFileLogger.fileName}`;

  return {
    format: winston.format.combine(
      winston.format.timestamp({ format: configFileLogger.times }),
      winston.format.printf(configFileLogger.formatString)
    ),
    filename: logPath,
    maxsize: configFileLogger.maxsize,
    maxFiles: configFileLogger.maxFiles,
    eol: configFileLogger.eol,
    tailable: configFileLogger.tailable,
  };
};

/**
 * @override Khởi tạo tệp tin winston's File để ghi tập tin đĩa
 *
 * @returns {Boolean}
 */
logger.setupFileLogger = function () {
  const fileLoggerTransport = this.getLogOptions();

  try {
    // Check first if the configured path is writable and only then
    // instantiate the file logging transport
    if (fs.openSync(fileLoggerTransport.filename, 'a+')) {
      logger.add(new winston.transports.File(fileLoggerTransport));
    }

    return true;
  } catch (err) {
    console.log();
    console.error(chalk.red('+ Error: Occured during the creation of the File transport logger'));
    console.error(chalk.red(err));
    console.log();

    return false;
  }
};

/**
 * @override Các tùy chọn để sử dụng với nhật ký morgan
 *
 * Trả về đối tượng log.options với luồng ghi được dựa trên winston
 * vận chuyển ghi nhật ký tệp (nếu có)
 * @returns {Object}
 */
logger.getMorganOptions = () => {
  return {
    stream: logger.stream,
  };
};

/**
 * @override Các định dạng để sử dụng với logger
 *
 * @returns {Object} Trả về tùy chọn log.format trong cấu hình môi trường hiện tại
 */
logger.getLogFormat = () => {
  let { format } = config.log;

  // Đảm bảo chúng tôi có định dạng hợp lệ
  if (!_.includes(config.log.validFormats, format)) {
    format = 'combined';

    console.log();
    console.log(chalk.yellow(`Warning: An invalid format was provided. The logger will use the default format of "${format}"`));
    console.log();
  }

  return format;
};

/**
 * Định dạng in nhật ký lỗi
 *
 * @param {String} actionName
 * @param {Object} objectName
 * @param {Error} error
 */
logger.logError = function (fileName, funcName, modelName, object, error) {
  this.error(`xcg::${fileName}::${funcName}::${modelName}
   ${JSON.stringify(object)}
   ${error}`);
};

logger.setupFileLogger();

module.exports = logger;
