const chalk = require('chalk');
const http = require('http');
const https = require('https');
const session = require('express-session');
const MongoStore = require('connect-mongo')(session);
const cookieParser = require('cookie-parser');
const passport = require('passport');
const socketio = require('socket.io');
const path = require('path');
const fs = require('fs');
const config = require('../config');
const { SOCKET_CONNECTION } = require(path.resolve('./commons/modules/core/datas/socket.data'));

/**
 * @overview Xác định phương thức cấu hình Socket.io
 *
 * @param    {Express} app
 * @param    {Mongoose} db
 * @returns  {Http|Https}
 */
const init = (app, db) => {
  let server;
  if (config.secure && config.secure.ssl === true) {
    // Tải chứng chỉ và khóa SSL
    const privateKey = fs.readFileSync(path.resolve(config.secure.privateKey), 'utf8');
    const certificate = fs.readFileSync(path.resolve(config.secure.certificate), 'utf8');
    let caBundle = '';

    try {
      caBundle = fs.readFileSync(path.resolve(config.secure.caBundle), 'utf8');
    } catch (err) {
      console.warn(chalk.yellow('Warning: couldn\'t find or read caBundle file'));
    }

    const options = {
      key: privateKey,
      cert: certificate,
      ca: caBundle,
      // requestCert: true,
      // rejectUnauthorized: true,
      secureProtocol: 'TLSv1_method',
      ciphers: [
        'ECDHE-RSA-AES128-GCM-SHA256',
        'ECDHE-ECDSA-AES128-GCM-SHA256',
        'ECDHE-RSA-AES256-GCM-SHA384',
        'ECDHE-ECDSA-AES256-GCM-SHA384',
        'DHE-RSA-AES128-GCM-SHA256',
        'ECDHE-RSA-AES128-SHA256',
        'DHE-RSA-AES128-SHA256',
        'ECDHE-RSA-AES256-SHA384',
        'DHE-RSA-AES256-SHA384',
        'ECDHE-RSA-AES256-SHA256',
        'DHE-RSA-AES256-SHA256',
        'HIGH',
        '!aNULL',
        '!eNULL',
        '!EXPORT',
        '!DES',
        '!RC4',
        '!MD5',
        '!PSK',
        '!SRP',
        '!CAMELLIA',
      ].join(':'),
      honorCipherOrder: true,
    };

    // Tạo máy chủ HTTPS mới
    server = https.createServer(options, app);
  } else {
    // Tạo một máy chủ HTTP mới
    server = http.createServer(app);
  }
  // Tạo một máy chủ Socket.io mới
  const io = socketio.listen(server);

  // Tạo một đối tượng lưu trữ MongoDB
  const mongoStore = new MongoStore({
    db,
    collection: config.session.collection,
  });

  // Đánh chặn yêu cầu bắt tay của Socket.io
  io.use((socket, next) => {
    // Sử dụng mô-đun 'cookie-parser' để phân tích cú pháp cookie yêu cầu
    cookieParser(config.session.secret)(socket.request, {}, (err) => {
      // Lấy id phiên từ cookie yêu cầu
      const sessionId = socket.request.signedCookies
        ? socket.request.signedCookies[config.session.key]
        : undefined;
      if (!sessionId) return next(new Error('sessionId was not found in socket.request'));

      // Sử dụng cá thể mongoStorage để lấy thông tin phiên Express
      mongoStore.get(sessionId, (err, session) => {
        if (err) return next(err);
        if (!session) return next(new Error(`session was not found for ${sessionId}`));

        // Đặt thông tin phiên Socket.io
        socket.request.session = session;

        // Sử dụng Hộ chiếu để điền chi tiết người dùng
        passport.initialize()(socket.request, {}, () => {
          passport.session()(socket.request, {}, () => {
            if (socket.request.user) {
              next(null);
            } else {
              next(new Error('User is not authenticated'));
            }
          });
        });
      });
    });
  });

  // Thêm trình xử lý sự kiện vào sự kiện 'kết nối'
  io.on(SOCKET_CONNECTION, (socket) => {
    config.files.server.sockets.forEach((socketConfiguration) => {
      require(path.resolve(socketConfiguration))(io, socket);
    });
  });

  return server;
};
module.exports.init = init;
