const path = require('path');
const compress = require('compression');
const favicon = require('serve-favicon');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const userAgent = require('express-useragent');
const hbs = require('express-hbs');
const csrf = require('csurf');
const session = require('express-session');
const MongoStore = require('connect-mongo')(session);
const helmet = require('helmet');
const hpp = require('hpp');
const express = require('express');
const chalk = require('chalk');
const config = require('../config');
const server = require('./socket-io');
const logger = require('./logger');
const regular = require(path.resolve('./commons/utils/regular-expression'));
const constant = require(path.resolve('./commons/utils/constant'));

/**
 * @overview Khởi tạo biến cục bộ
 *
 * @param {Express} app
 */
const initLocalVariables = (app) => {
  // Đặt biến cục bộ của ứng dụng
  app.locals.title = config.app.title;
  app.locals.description = config.app.description;
  app.locals.keywords = config.app.keywords;
  app.locals.language = config.app.language;
  app.locals.languages = config.app.languages;
  app.locals.config = config.app.config;
  app.locals.apiVersion = config.app.apiVersion;
  app.locals.jsFiles = config.files.client.js;
  app.locals.cssFiles = config.files.client.css;
  app.locals.livereload = config.livereload;
  app.locals.favicon = config.favicon;
  app.locals.env = process.env.NODE_ENV;
  app.locals.domain = config.domain;
  app.locals.logo = regular.app.logo;
  app.locals.defaultRoles = regular.app.defaultRoles;

  if (config.secure && config.secure.ssl === true) {
    app.locals.secure = config.secure.ssl;

    // Bắt buộc sử dụng HTTPS nếu được bật
    app.use((req, res, next) => {
      if (req.protocol === 'http') {
        return res.redirect(301, `https://${req.headers.host}${req.originalUrl}`);
      }

      return next();
    });
  }

  // Chuyển URL yêu cầu đến environment locals
  app.use((req, res, next) => {
    res.locals.host = `${req.protocol}://${req.hostname}`;
    res.locals.url = `${req.protocol}://'${req.headers.host}${req.originalUrl}`;
    return next();
  });
};
module.exports.initLocalVariables = initLocalVariables;

/**
 * @overview Khởi tạo phần mềm trung gian ứng dụng
 *
 * @param {Express} app
 */
const initMiddleware = (app) => {
  // Nên được đặt trước express.static
  app.use(compress({
    filter: (req, res) => {
      return (/json|text|javascript|css|font|svg/).test(res.getHeader('Content-Type'));
    },
    level: 9,
  }));

  // Khởi tạo phần mềm trung gian favicon
  app.use(favicon(app.locals.favicon));

  // Bật trình ghi nhật ký (morgan) nếu được bật trong tệp cấu hình
  if (!config.log.disable) {
    app.use(morgan(logger.getLogFormat(), logger.getMorganOptions()));
  }

  // Phần mềm trung gian phụ thuộc môi trường
  if (process.env.NODE_ENV === 'development') {
    // Tắt bộ nhớ cache chế độ xem
    app.set('view cache', false);
  } else if (process.env.NODE_ENV === 'production') {
    app.locals.cache = 'memory';
  }

  // Yêu cầu phần mềm trung gian phân tích cú pháp
  app.use(bodyParser.urlencoded({
    extended: true,
  }));
  app.use(bodyParser.json());

  // Thêm trình phân tích cú pháp cookie
  app.use(cookieParser());

  // Thêm thông tin client
  app.use(userAgent.express());
};
module.exports.initMiddleware = initMiddleware;

/**
 * @overview Định cấu hình công cụ xem
 *
 * @param {Express} app
 */
const initViewEngine = (app) => {
  app.engine(config.viewEngine, hbs.express4({
    extname: `.${config.viewEngine}`,
  }));
  app.set('view engine', config.viewEngine);
  app.set('views', path.resolve('./'));
};
module.exports.initViewEngine = initViewEngine;

/**
 * Định cấu bảo vệ tiêu đề
 *
 * @param {Express} app
 */
const initSecureHeaders = (app) => {
  app.use(helmet.contentSecurityPolicy(config.helmet.csp));
  app.use(helmet.permittedCrossDomainPolicies());
  app.use(helmet.dnsPrefetchControl());
  app.use(helmet.frameguard(config.helmet.frameguard));
  app.use(helmet.hidePoweredBy());
  app.use(helmet.xssFilter());
  app.use(helmet.referrerPolicy(config.helmet.policy));
  app.use(helmet.noSniff());
  app.use(helmet.ieNoOpen());
  app.use(helmet.hsts(config.helmet.hsts));
  app.use(helmet.hpkp(config.helmet.hpkp));
  app.use(helmet.noCache());

  app.use(hpp());
};
module.exports.initSecureHeaders = initSecureHeaders;

/**
 * @overview Định cấu hình các mô-đun tuyến tĩnh
 *
 * @param {Express} app
 */
const initModulesClientRoutes = (app) => {
  // Đặt bộ định tuyến ứng dụng và thư mục tĩnh
  app.use('/', express.static(path.resolve('./public'), { maxAge: 6 * constant.ENUM.TIME.ONE_MONTH }));

  // Định tuyến tĩnh toàn cầu
  config.folders.client.forEach((staticPath) => {
    app.use(staticPath, express.static(path.resolve(`./${staticPath}`)));
  });
};
module.exports.initModulesClientRoutes = initModulesClientRoutes;

/**
 * @overview Định cấu hình phiên Express
 *
 * @param {Express} app
 */
const initSession = (app, db) => {
  // Bộ nhớ phiên MongoDB Express
  app.use(session({
    name: config.session.key,
    saveUninitialized: config.session.saveUninitialized,
    resave: config.session.resave,
    secret: config.session.secret,
    cookie: {
      path: config.session.cookie.path,
      httpOnly: config.session.cookie.httpOnly,
      secure: config.session.cookie.secure && config.secure.ssl,
      maxAge: config.session.cookie.maxAge,
    },
    store: new MongoStore({
      db,
      collection: config.session.collection,
    }),
  }));


  // Thêm bảo vệ CSRF
  if (process.env.NODE_ENV !== 'test') {
    app.use(csrf());
  }
};
module.exports.initSession = initSession;

/**
 * @overview Gọi cấu hình máy chủ mô-đun
 *
 * @param {Express} app
 */
const initModulesConfiguration = (app) => {
  config.files.server.configs.forEach((configPath) => {
    require(path.resolve(configPath))(app);
  });
};
module.exports.initModulesConfiguration = initModulesConfiguration;

/**
 * @overview Cấu hình các mô-đun chính sách ACL
 *
 * @param {Express} app
 */
const initModulesServerPolicies = (app) => {
  // Tệp chính sách toàn cầu
  config.files.server.policies.forEach((policyPath) => {
    require(path.resolve(policyPath)).invokeRolesPolicies();
  });
};
module.exports.initModulesServerPolicies = initModulesServerPolicies;

/**
 * @overview Định cấu hình các tuyến máy chủ mô-đun
 *
 * @param {Express} app
 */
const initModulesServerRoutes = (app) => {
  // Tệp định tuyến toàn cầu
  config.files.server.routes.forEach((routePath) => {
    require(path.resolve(routePath))(app);
  });
};
module.exports.initModulesServerRoutes = initModulesServerRoutes;

/**
 * @overview Định cấu hình các lịch trình máy chủ mô-đun
 *
 * @param {Express} app
 */
const initModulesServerCrons = (app) => {
  // Tệp lịch trình toàn cầu
  config.files.server.crons.forEach((cronPath) => {
    require(path.resolve(cronPath))(app);
  });
};
module.exports.initModulesServerCrons = initModulesServerCrons;

/**
 * @overview Định cấu hình xử lý lỗi
 *
 * @param {Express} app
 */
const initErrorRoutes = (app) => {
  app.use((err, req, res, next) => {
    // Nếu lỗi tồn tại
    if (err) {
      let dataRes = null;

      if (err.code === 'EBADCSRFTOKEN') {
        // CSRF
        dataRes = {
          status: 403,
          bodyRes: {
            code: 'data.notVaild',
          },
        };
      } else if (err.message === 'validation error') {
        // Params validation
        dataRes = {
          status: 400,
          bodyRes: {
            code: 'data.notVaild',
          },
        };
      } else {
        // Server
        dataRes = {
          status: 500,
          bodyRes: {
            code: 'error.server.is',
          },
        };

        console.log();
        console.error(chalk.red('+ Error routes: '));
        console.error(chalk.red(err.message));
        console.error(chalk.red(err));
        console.log();
      }

      return res
        .status(dataRes.status)
        .send(dataRes.bodyRes)
        .end();
    }

    return next();
  });
};
module.exports.initErrorRoutes = initErrorRoutes;

/**
 * @overview Khởi tạo ứng dụng Express
 *
 * @param    {Express} app
 * @returns  {Http|Https}
 */
const init = function (db) {
  const app = express();

  this.initLocalVariables(app);
  this.initMiddleware(app);
  this.initViewEngine(app);
  this.initSecureHeaders(app);
  this.initModulesClientRoutes(app);
  this.initSession(app, db);
  this.initModulesConfiguration(app);
  this.initModulesServerPolicies(app);
  this.initModulesServerRoutes(app);
  this.initModulesServerCrons(app);
  this.initErrorRoutes(app);

  return server.init(app, db);
};
module.exports.init = init;
