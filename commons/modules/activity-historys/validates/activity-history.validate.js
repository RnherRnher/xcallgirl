const joi = require('joi');
const validation = require('../../../utils/validation');

const search = {
  query: joi.object().keys({
    type: validation.query.type,
    skip: validation.query.skip
  }).length(2)
};
module.exports.search = search;
