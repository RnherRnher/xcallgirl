module.exports = {
  BUSINESS_CARDS_SEARCH: '/business-cards/search',
  BUSINESS_CARDS_GET_ID: '/business-cards/get/:id',
  BUSINESS_CARDS: '/business-cards',
  BUSINESS_CARDS_STATUS: '/business-cards/status',
  BUSINESS_CARDS_IMAGE: '/business-cards/image',
  BUSINESS_CARDS_ADDRESS: '/business-cards/address',
  BUSINESS_CARDS_DESCRIPTIONS: '/business-cards/descriptions',
  BUSINESS_CARDS_REUPLOAD: '/business-cards/reupload',
  BUSINESS_CARDS_REPORT_ID: '/business-cards/report/:id',
  BUSINESS_CARDS_LIKE_ID: '/business-cards/like/:id',
  BUSINESS_CARDS_COMMENT_ID: '/business-cards/comment/:id',
  BUSINESS_CARDS_DEAL_ID: '/business-cards/deal/:id',
};
