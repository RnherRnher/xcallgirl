const businessCards = require('./business-cards/url.data');
const dealHistorys = require('./deal-historys/url.data');
const { assignIn } = require('lodash');

module.exports = assignIn(
  businessCards,
  dealHistorys
);
