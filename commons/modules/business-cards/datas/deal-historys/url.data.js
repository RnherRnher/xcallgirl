module.exports = {
  BUSINESS_CARDS_DEAL_HISTORYS_SEARCH: '/business-cards/deal-historys/search',
  BUSINESS_CARDS_DEAL_HISTORYS_GET_ID: '/business-cards/deal-historys/get/:id',
  BUSINESS_CARDS_DEAL_HISTORYS_ACCEPT_ID: '/business-cards/deal-historys/accept/:id',
  BUSINESS_CARDS_DEAL_HISTORYS_TOKEN_ID: '/business-cards/deal-historys/token/:id',
  BUSINESS_CARDS_DEAL_HISTORYS_REPORT_ID: '/business-cards/deal-historys/report/:id',
};
