const joi = require('joi');
joi.objectId = require('joi-objectid')(joi);
const validation = require('../../../../utils/validation');

const accept = {
  body: joi.object().keys({
    accept: joi.object().keys({
      is: validation.body.is.required(),
      reason: validation.body.reason
    }).min(1)
  }).length(1)
};
module.exports.accept = accept;

const getToken = {
  params: joi.object().keys({
    id: validation.params.id
  }).length(1)
};
module.exports.getToken = getToken;

const validateToken = {
  params: joi.object().keys({
    id: validation.params.id,
  }).length(1),
  body: joi.object().keys({
    token: validation.body.token
  }).length(1)
};
module.exports.validateToken = validateToken;

const report = {
  params: joi.object().keys({
    id: validation.params.id
  }).length(1),
  body: joi.object().keys({
    report: joi.object().keys({
      reason: validation.body.reason
    }).length(1),
  }).length(1)
};
module.exports.report = report;
