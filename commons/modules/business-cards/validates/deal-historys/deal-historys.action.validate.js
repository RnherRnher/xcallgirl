const joi = require('joi');
joi.objectId = require('joi-objectid')(joi);
const validation = require('../../../../utils/validation');

const search = {
  query: joi.object().keys({
    type: validation.query.type.required(),
    skip: validation.query.skip,
  }).min(1),
  body: joi.object().keys({
    extendSearch: joi.object().keys({
      result: validation.body.result,
      status: validation.body.status,
      initDate: joi.object().keys({
        max: validation.body.date.time,
        min: validation.body.date.time
      })
    }).min(1),
  }),
};
module.exports.search = search;

const get = {
  params: joi.object().keys({
    id: validation.params.id,
  }).length(1),
  query: joi.object().keys({
    type: validation.query.type,
  }).length(1),
};
module.exports.get = get;
