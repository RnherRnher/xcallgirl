const joi = require('joi');
const validation = require('../../../../utils/validation');

const changeDescriptions = {
  body: joi.object().keys({
    descriptions: validation.body.descriptions
  }).length(1)
};
module.exports.changeDescriptions = changeDescriptions;

const changeAddress = {
  body: joi.object().keys({
    address: joi.object().keys({
      country: validation.body.address,
      city: validation.body.address,
      county: validation.body.address,
      local: validation.body.address
    }).min(1)
  }).length(1)
};
module.exports.changeAddress = changeAddress;

const uploadImage = {
  query: joi.object().keys({
    name: validation.query.name
  }).length(1)
};
module.exports.uploadImage = uploadImage;

const deleteImage = {
  query: joi.object().keys({
    publicID: validation.query.publicID
  }).length(1)
};
module.exports.deleteImage = deleteImage;

const deletee = {
  body: joi.object().keys({
    password: validation.body.password
  }).length(1)
};
module.exports.deletee = deletee;
