const joi = require('joi');
joi.objectId = require('joi-objectid')(joi);
const validation = require('../../../../utils/validation');

const extendSearchBody = joi.object().keys({
  gender: validation.body.gender,
  blurFace: validation.body.is,
  star: validation.body.star,
  costs: joi.object().keys({
    room: joi.object().keys({
      max: validation.body.costs.moneys,
      min: validation.body.costs.moneys,
      // values: validation.body.costs.values
    }),
    service: joi.object().keys({
      max: validation.body.costs.moneys,
      min: validation.body.costs.moneys,
      // values: validation.body.costs.values
    }),
    currencyUnit: validation.body.currencyUnit,
  }),
  address: joi.object().keys({
    country: validation.body.address,
    city: validation.body.address,
    county: validation.body.address,
  }),
}).min(1);
module.exports.extendSearchBody = extendSearchBody;

const search = {
  query: joi.object().keys({
    type: validation.query.type.required(),
    skip: validation.query.skip.required(),
    extendSearch: extendSearchBody,
  }).min(2)
};
module.exports.search = search;

const get = {
  params: joi.object().keys({
    id: validation.params.id
  }).length(1)
};
module.exports.get = get;

const create = {
  body: joi.object().keys({
    title: validation.body.title,
    descriptions: validation.body.descriptions,
    blurFace: joi.boolean().truthy('true').falsy('false'),
    measurements3: joi.object().keys({
      breast: joi.number().min(0),
      waist: joi.number().min(0),
      hips: joi.number().min(0),
    }).length(3),
    address: joi.object().keys({
      country: validation.body.address,
      city: validation.body.address,
      county: validation.body.address,
      local: validation.body.address,
    }).length(4),
    costs: joi.object().keys({
      rooms: joi
        .array()
        .items(joi.object().keys({
          moneys: validation.body.costs.moneys,
          value: validation.body.costs.value
        }).length(2))
        .unique(),
      services: joi
        .array()
        .items(joi.object().keys({
          moneys: validation.body.costs.moneys,
          value: validation.body.costs.value
        }).length(2))
        .min(1)
        .unique(),
      currencyUnit: validation.body.currencyUnit,
    }).length(3),
  }).length(6)
};
module.exports.create = create;

const report = {
  params: joi.object().keys({
    id: validation.params.id,
  }).length(1),
  body: joi.object().keys({
    report: joi.object().keys({
      reason: validation.body.reason
    }).length(1),
  }).length(1)
};
module.exports.report = report;

const unLike = {
  params: joi.object().keys({
    id: validation.params.id
  }).length(1)
};
module.exports.unLike = unLike;

const like = {
  params: joi.object().keys({
    id: validation.params.id
  }).length(1)
};
module.exports.like = like;

const comments = {
  params: joi.object().keys({
    id: validation.params.id
  }).length(1),
  query: joi.object().keys({
    max: validation.query.max,
  }).length(1)
};
module.exports.comments = comments;

const comment = {
  params: joi.object().keys({
    id: validation.params.id
  }).length(1),
  body: joi.object().keys({
    comment: validation.body.comment
  }).length(1)
};
module.exports.comment = comment;

const changeComment = {
  params: joi.object().keys({
    id: validation.params.id
  }).length(1),
  body: joi.object().keys({
    comment: validation.body.comment
  }).length(1),
  query: joi.object().keys({
    commentID: validation.query.id
  }).length(1)
};
module.exports.changeComment = changeComment;

const deleteComment = {
  params: joi.object().keys({
    id: validation.params.id
  }).length(1),
  query: joi.object().keys({
    commentID: validation.query.id
  }).length(1)
};
module.exports.deleteComment = deleteComment;

const deal = {
  params: joi.object().keys({
    id: validation.params.id
  }).length(1),
  body: joi.object().keys({
    costs: joi.object().keys({
      room: joi.object().keys({
        moneys: validation.body.costs.moneys.required(),
        value: validation.body.costs.value.required()
      }),
      service: joi.object().keys({
        moneys: validation.body.costs.moneys,
        value: validation.body.costs.value
      }).length(2),
      currencyUnit: validation.body.currencyUnit.required(),
    }).min(2),
    timer: validation.body.smallExpires.required(),
    note: validation.body.note,
  }).min(2),
};
module.exports.deal = deal;
