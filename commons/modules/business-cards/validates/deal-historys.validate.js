const authorization = require('./deal-historys/deal-historys.authorization.validate');
const action = require('./deal-historys/deal-historys.action.validate');
const { assignIn } = require('lodash');

module.exports = assignIn(
  authorization,
  action
);
