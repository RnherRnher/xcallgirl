const profile = require('./business-cards/business-cards.profile.validate');
const action = require('./business-cards/business-cards.action.validate');
const { assignIn } = require('lodash');

module.exports = assignIn(
  profile,
  action
);
