const joi = require('joi');
const validation = require('../../../../utils/validation');

const forgotPassword = {
  body: joi.object().keys({
    numberPhone: joi.object().keys({
      code: validation.body.numberPhone.code,
      number: validation.body.numberPhone.number
    }).length(2),
  }).length(1),
};
module.exports.forgotPassword = forgotPassword;

const validateResetTokenPassword = {
  body: joi.object().keys({
    resetTokenPassword: validation.body.token
  }).length(1),
};
module.exports.validateResetTokenPassword = validateResetTokenPassword;

const resetPassword = {
  body: joi.object().keys({
    resetTokenPassword: validation.body.token,
    passwordDetails: joi.object().keys({
      verifyPassword: validation.body.password,
      newPassword: validation.body.password
    }).length(2),
  }).length(2),
};
module.exports.resetPassword = resetPassword;

const verifyNumberPhone = {
  body: joi.object().keys({
    verifyTokenNumberPhone: validation.body.token
  }).length(1),
};
module.exports.verifyNumberPhone = verifyNumberPhone;
