const joi = require('joi');
joi.objectId = require('joi-objectid')(joi);
const validation = require('../../../../utils/validation');

const get = {
  params: joi.object().keys({
    id: validation.params.id
  }).length(1),
};
module.exports.get = get;

const search = {
  query: joi.object().keys({
    type: validation.query.type
  }).length(1),
  body: joi.object().keys({
    absoluteSearch: joi.object().keys({
      numberPhone: joi.object().keys({
        code: validation.body.numberPhone.code,
        number: validation.body.numberPhone.number,
      }).length(2),
      nickName: validation.body.name.nick
    }).min(1)
  }).length(1),
};
module.exports.search = search;
