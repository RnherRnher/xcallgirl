const joi = require('joi');
const validation = require('../../../../utils/validation');

const changeProfileNumberPhone = {
  body: joi.object().keys({
    password: validation.body.password,
    numberPhone: joi.object().keys({
      code: validation.body.numberPhone.code,
      number: validation.body.numberPhone.number
    }).length(2),
  }).length(2),
};
module.exports.changeProfileNumberPhone = changeProfileNumberPhone;

const changeProfilePassword = {
  body: joi.object().keys({
    passwordDetails: joi.object().keys({
      currentPassword: validation.body.password,
      newPassword: validation.body.password,
      verifyPassword: validation.body.password,
    }).length(3),
  }).length(1),
};
module.exports.changeProfilePassword = changeProfilePassword;

const changeProfileSettings = {
  query: joi.object().keys({
    type: validation.query.type
  }).length(1),
  body: joi.object().keys({
    settings: validation.body.settings
  }).length(1),
};
module.exports.changeProfileSettings = changeProfileSettings;

const getProfileSettings = {
  query: joi.object().keys({
    type: validation.query.type
  }).length(1),
};
module.exports.getProfileSettings = getProfileSettings;

const changeProfileAddress = {
  body: joi.object().keys({
    address: joi.object().keys({
      country: validation.body.address.required(),
      city: validation.body.address,
      county: validation.body.address,
      local: validation.body.address
    }).min(1),
  }).length(1),
};
module.exports.changeProfileAddress = changeProfileAddress;
