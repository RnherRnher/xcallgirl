const joi = require('joi');
const validation = require('../../../../utils/validation');

const signup = {
  body: joi.object().keys({
    name: joi.object().keys({
      first: validation.body.name.first,
      last: validation.body.name.last,
      nick: validation.body.name.nick,
    }).length(3),
    numberPhone: joi.object().keys({
      code: validation.body.numberPhone.code,
      number: validation.body.numberPhone.number
    }).length(2),
    gender: validation.body.gender,
    birthday: joi.object().keys({
      day: validation.body.date.day,
      month: validation.body.date.month,
      year: validation.body.date.year
    }).length(3),
    address: joi.object().keys({
      country: validation.body.address.required(),
      city: validation.body.address,
      county: validation.body.address,
      local: validation.body.address
    }).min(1),
    password: validation.body.password,
    settings: joi.object().keys({
      general: joi.object().keys({
        language: validation.body.language,
      }).length(1),
    }).length(1),
  }).length(7),
};
module.exports.signup = signup;

const signin = {
  body: joi.object().keys({
    numberPhone: validation.body.numberPhone.full,
    password: validation.body.password
  }).length(2),
};
module.exports.signin = signin;
