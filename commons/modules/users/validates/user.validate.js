const authentication = require('./users/users.authentication.validate');
const profile = require('./users/users.profile.validate');
const security = require('./users/users.security.validate');
const action = require('./users/users.action.validate');
const { assignIn } = require('lodash');

module.exports = assignIn(
  authentication,
  profile,
  security,
  action
);
