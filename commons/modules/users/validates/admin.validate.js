const joi = require('joi');
joi.objectId = require('joi-objectid')(joi);
const validation = require('../../../utils/validation');

const extendSearchBody = joi.object().keys({
  nick: validation.commons.mediumString,
  roles: validation.body.roles,
  gender: validation.body.gender,
  age: joi.object().keys({
    min: validation.body.age,
    max: validation.body.age
  }),
  address: joi.object().keys({
    country: validation.body.address,
    city: validation.body.address,
    county: validation.body.address,
  }),
  lock: validation.body.is,
  delete: validation.body.is,
  verify: joi.object().keys({
    account: joi.object().keys({
      is: validation.body.is,
      status: validation.body.status,
    }),
    numberPhone: validation.body.is,
  }),
  online: validation.body.is,
}).min(1);
module.exports.extendSearchBody = extendSearchBody;

const search = {
  query: joi.object().keys({
    type: validation.query.type,
    skip: validation.query.skip,
    extendSearch: extendSearchBody
  }).length(3)
};
module.exports.search = search;

const verifyAccount = {
  params: joi.object().keys({
    id: validation.params.id
  }).length(1),
  body: joi.object().keys({
    verifyAccount: joi.object().keys({
      is: validation.body.is.required(),
      reason: validation.body.reason
    }).min(1),
  }).length(1),
};
module.exports.verifyAccount = verifyAccount;

const lockAccount = {
  params: joi.object().keys({
    id: validation.params.id
  }).length(1),
  body: joi.object().keys({
    lockAccount: joi.object().keys({
      expires: validation.body.smallExpires,
      reason: validation.body.reason
    }).length(2)
  }).length(1),
};
module.exports.lockAccount = lockAccount;

const unlockAccount = {
  params: joi.object().keys({
    id: validation.params.id
  }).length(1),
};
module.exports.unlockAccount = unlockAccount;

const deleteAccount = {
  params: joi.object().keys({
    id: validation.params.id
  }).length(1),
  body: joi.object().keys({
    password: validation.body.password,
    deleteAccount: joi.object().keys({
      reason: validation.body.reason
    }).length(1),
  }).length(2),
};
module.exports.deleteAccount = deleteAccount;

const restoreAccount = {
  params: joi.object().keys({
    id: validation.params.id
  }).length(1),
  body: joi.object().keys({
    password: validation.body.password,
  }).length(1),
};
module.exports.restoreAccount = restoreAccount;
