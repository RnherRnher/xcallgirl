const joi = require('joi');
joi.objectId = require('joi-objectid')(joi);
const validation = require('../../../utils/validation');

const changeRolesAccount = {
  params: joi.object().keys({
    id: validation.params.id
  }).length(1),
  body: joi.object().keys({
    password: validation.body.password,
    roles: validation.body.roles
  }).length(2),
};
module.exports.changeRolesAccount = changeRolesAccount;
