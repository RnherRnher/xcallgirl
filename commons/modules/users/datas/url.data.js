module.exports = {
  /* system */
  USERS_SYSTEM_ROLES_ID: '/users/system/roles/:id',
  /* admin */
  USERS_ADMIN_SEARCH: '/users/admin/search',
  USERS_VERIFY_ID: '/users/verify/:id',
  USERS_LOCK_ID: '/users/lock/:id',
  USERS_DELETE_ID: '/users/delete/:id',
  /* users */
  USERS_SEARCH: '/users/search',
  USERS_GET_ID: '/users/get/:id',
  USERS: '/users',
  USERS_NUMBERPHONE: '/users/numberphone',
  USERS_PASSWORD: '/users/password',
  USERS_AVATAR: '/users/avatar',
  USERS_SETTINGS: '/users/settings',
  USERS_VERIFY: '/users/verify',
  USERS_ADDRESS: '/users/address',
  /* auth */
  AUTH_FORGOT_PASSWORD: '/auth/forgot-password',
  AUTH_RESET: '/auth/reset',
  AUTH_SIGNUP: '/auth/signup',
  AUTH_SIGNIN: '/auth/signin',
  AUTH_SIGNOUT: '/auth/signout',
};
