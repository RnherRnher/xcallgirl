const joi = require('joi');
joi.objectId = require('joi-objectid')(joi);
const validation = require('../../../utils/validation');

const getParameter = {
  query: joi.object().keys({
    type: validation.query.type
  }).length(1),
};
module.exports.getParameter = getParameter;
