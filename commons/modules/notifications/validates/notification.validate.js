const joi = require('joi');
joi.objectId = require('joi-objectid')(joi);
const validation = require('../../../utils/validation');

const search = {
  query: joi.object().keys({
    type: validation.query.type,
    skip: validation.query.skip
  }).length(2),
};
module.exports.search = search;

const mark = {
  params: joi.object().keys({
    id: validation.params.id
  }).length(1),
  body: joi.object().keys({
    mark: joi.object().keys({
      is: joi.boolean().truthy('true').falsy('false')
    }).length(1)
  }).length(1),
};
module.exports.mark = mark;

const get = {
  params: joi.object().keys({
    id: validation.params.id
  }).length(1),
};
module.exports.get = get;
