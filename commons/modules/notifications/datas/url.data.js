module.exports = {
  NOTIFICATIONS_SEARCH: '/notifications/search',
  NOTIFICATIONS_GET_ID: '/notifications/get/:id',
};
