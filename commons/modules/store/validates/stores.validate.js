const authorization = require('./stores/stores.authorization.validate');
const { assignIn } = require('lodash');

module.exports = assignIn(
  authorization
);
