const joi = require('joi');
joi.objectId = require('joi-objectid')(joi);
const validation = require('../../../../utils/validation');

const pay = {
  body: joi.object().keys({
    item: joi.object().keys({
      groupID: validation.query.type,
      itemID: validation.query.type,
      cost: validation.body.costs.moneys
    }).length(3),
  }).length(1),
};
module.exports.pay = pay;
