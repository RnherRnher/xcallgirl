const authorization = require('./credits/credits.authorization.validate');
const { assignIn } = require('lodash');

module.exports = assignIn(
  authorization
);
