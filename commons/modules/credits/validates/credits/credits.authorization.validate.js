const joi = require('joi');
joi.objectId = require('joi-objectid')(joi);
const validation = require('../../../../utils/validation');

const transfer = {
  params: joi.object().keys({
    id: validation.params.id
  }).length(1),
  body: joi.object().keys({
    transfer: joi.object().keys({
      password: validation.body.password,
      moneys: validation.body.costs.moneys
    }).length(2),
  }).length(1),
};
module.exports.transfer = transfer;
