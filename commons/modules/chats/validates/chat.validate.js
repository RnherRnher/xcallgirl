const joi = require('joi');
joi.objectId = require('joi-objectid')(joi);
const validation = require('../../../utils/validation');

const get = {
  params: joi.object().keys({
    id: validation.params.id
  }).length(1),
  query: joi.object().keys({
    type: validation.query.type
  }).length(1),
};
module.exports.get = get;

const create = {
  query: joi.object().keys({
    type: validation.query.type
  }).length(1),
  body: joi.object().keys({
    usersID: validation.body.usersID.unique().min(1),
  }).length(1),
};
module.exports.create = create;

const deletee = {
  params: joi.object().keys({
    id: validation.params.id
  }).length(1),
};
module.exports.deletee = deletee;

const accept = {
  params: joi.object().keys({
    id: validation.params.id
  }).length(1),
  body: joi.object().keys({
    accept: joi.object().keys({
      is: validation.body.is,
    }).length(1)
  }).length(1),
};
module.exports.accept = accept;

const messages = {
  params: joi.object().keys({
    id: validation.params.id
  }).length(1),
  query: joi.object().keys({
    max: validation.query.max,
  }).length(1)
};
module.exports.messages = messages;

const search = {
  query: joi.object().keys({
    type: validation.query.type,
    skip: validation.query.skip,
    fragment: validation.query.fragment
  }).length(3),
};
module.exports.search = search;
