module.exports = {
  /* User */
  CHATS_SEARCH: '/chats/search',
  CHATS_GET_ID: '/chats/get/:id',
  CHATS: '/chats',
  CHATS_ACCEPT_ID: '/chats/accept/:id',
  CHATS_MESSAGE_ID: '/chats/message/:id',
};
