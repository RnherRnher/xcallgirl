/**
 * @overview Tạo cấu trúc dữ liệu socket
 *
 * @param   {Array} names
 * @param   {String} to
 * @param   {Object} body
 * @returns {Object}
 */
const createSocketData = (names, to, body) => {
  return {
    names,
    data: {
      to,
      body
    }
  };
};
module.exports.createSocketData = createSocketData;
