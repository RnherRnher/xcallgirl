const joi = require('joi');
joi.objectId = require('joi-objectid')(joi);
const regular = require('./regular-expression');
const { concat } = require('lodash');

module.exports = {
  params: {
    id: joi.objectId()
  },
  query: {
    type: joi.string().max(regular.commons.stringLength.small),
    fragment: joi.string().max(regular.commons.stringLength.small),
    skip: joi.number().min(0),
    max: joi.number().min(-1),
    name: joi.string().max(regular.commons.stringLength.medium),
    publicID: joi.string().max(regular.commons.stringLength.medium),
    id: joi.objectId()
  },
  body: {
    is: joi.boolean().truthy('true').falsy('false'),
    reason: joi.string().max(regular.commons.stringLength.large),
    smallExpires: joi.number().min(regular.commons.expires.small),
    password: joi.string().regex(regular.user.password),
    roles: joi.string().valid(regular.user.roles),
    gender: joi.string().valid(regular.user.gender),
    age: joi.number().min(regular.app.ageRestriction).max(99),
    address: joi.string().max(regular.commons.stringLength.medium),
    language: joi.string().max(regular.commons.stringLength.small),
    usersID: joi.array().items(joi.objectId()),
    token: joi.string().token().max(regular.commons.stringLength.medium),
    numberPhone: {
      code: joi.string().regex(regular.user.numberPhone.code),
      number: joi.string().regex(regular.user.numberPhone.number),
      full: joi.string().regex(regular.user.numberPhone.full),
    },
    name: {
      first: joi.string().regex(regular.user.name.first),
      last: joi.string().regex(regular.user.name.last),
      nick: joi.string().regex(regular.user.name.nick),
    },
    date: {
      time: joi.date(),
      day: joi.number().min(0).max(30),
      month: joi.number().min(0).max(11),
      year: joi.number(),
    },
    settings: joi.object(),
    title: joi.string().max(regular.commons.stringLength.medium),
    descriptions: joi.string().max(regular.commons.stringLength.large),
    star: joi.number().min(0).max(5),
    costs: {
      moneys: joi.number().min(regular.commons.moneys.min).max(regular.commons.moneys.max),
      value: joi.string().valid(concat(regular.businessCard.costs.rooms, regular.businessCard.costs.services)),
      values: joi.array().items(joi.string().valid(concat(regular.businessCard.costs.rooms, regular.businessCard.costs.services))),
    },
    currencyUnit: joi.string().valid(regular.commons.moneys.currencyUnit),
    comment: joi.string().max(regular.commons.stringLength.medium),
    note: joi.string().max(regular.commons.stringLength.medium),
    result: joi.string().max(regular.commons.stringLength.small),
    status: joi.string().max(regular.commons.stringLength.small),
  },
  commons: {
    mediumString: joi.string().max(regular.commons.stringLength.medium)
  }
};
