module.exports = {
  ENUM: {
    ROLES: {
      USER: 0,
      PROVIDER: 1,
      ADMIN: 2,
      SYSTEM: 3,
    },
    AUTH: {
      NONE: 0,
      VIEW: 1,
      FULL: 2
    },
    COUNT: {
      LIMIT: 99
    },
    TIME: {
      ONE_MONTH: 18144000000,
      ONE_DAY: 86400000,
      ONE_HOURS: 3600000,
      ONE_MINUTES: 60000,
      ONE_SECONDS: 1000,
    },
    MEMORY_STORAGE: {
      ONE_MB: 1048576
    }
  },
  /**
   * User
   */
  SELF: 'self',
  GUEST: 'guest',
  USER: 'user',
  PROVIDER: 'provider',
  ADMIN: 'admin',
  SALT: 'salt',
  ONLINE: 'online',
  PASSWORD: 'password',
  ACCOUNT: 'account',
  NUMBERPHONE: 'numberPhone',
  VERIFY_ACCOUNT: 'verifyAccount',
  VERIFY_NUMBERPHONE: 'verifyNumberPhone',
  FIRST: 'first',
  LAST: 'last',
  NICK: 'nick',
  BIRTHDAY: 'birthday',
  FEMALE: 'female',
  MALE: 'male',
  AVATAR: 'avatar',
  AVATAR_DETETE: 'avatarDelete',
  PROFILE: 'profile',
  ROLES: 'roles',
  /**
  * Businesscard
  */
  BUSINESS_CARD: 'businessCard',
  BACKGROUND: 'background',
  FACE: 'face',
  BREAST: 'breast',
  HIPS: 'hips',
  WAIST: 'waist',
  BODY: 'body',
  ONE_TIME: 'oneTime',
  ONE_SLOT: 'oneSlot',
  OVER_NIGHT: 'overNight',
  SOME: 'some',
  SOME_OVER_NIGHT: 'someOverNight',
  ROOMS: 'rooms',
  SERVICERS: 'services',
  ROOM: 'room',
  SERVICER: 'service',
  VALUE: 'value',
  MONEYS: 'moneys',
  CURRENCY_UNIT: 'currencyUnit',
  DOMESTIC: 'domestic',
  DOLLAR: 'dollar',
  DESCRIPTIONS: 'descriptions',
  /**
  * Notification
  */
  NOTIFICATION: 'notification',
  NOTIFICATIONS: 'notifications',
  /**
  * Activity History
  */
  ACTIVITY_HISTORY: 'activityHistory',
  ACTIVITY_HISTORYS: 'activityHistorys',
  /**
  * Businesscard Deal History
  */
  BUSINESS_CARDS_DEAL_HISTORYS: 'businessCardsDealHistorys',
  BUSINESS_CARD_DEAL_HISTORY: 'businessCardDealHistory',
  /**
  * Chat
  */
  CHAT: 'chat',
  /**
  * System
  */
  SYSTEM: 'system',
  /**
  * Credit
  */
  CREDIT: 'credit',
  TRANSFER: 'transfer',
  /**
  * Store
  */
  STORE: 'store',
  PAY: 'pay',
  /**
   * Info
   */
  LANGUAGE: 'language',
  GENERAL: 'general',
  PRIVCY: 'privacy',
  SECURITY: 'security',
  SMS: 'sms',
  MESSAGE: 'message',
  VERIFY: 'verify',
  CONTACTS: 'contacts',
  TOKEN: 'token',
  REASON: 'reason',
  DETAIL: 'detail',
  NOTE: 'note',
  MIN: 'min',
  MAX: 'max',
  // Address
  ADDRESS: 'address',
  COUNTRY: 'country',
  CITY: 'city',
  COUNTY: 'county',
  LOCAL: 'local',
  // Time
  TIME: 'time',
  HOURS: 'hours',
  MINUTES: 'minutes',
  DAY: 'day',
  MONTH: 'month',
  YEAR: 'year',
  /**
   * Action
   */
  REPORT: 'report',
  VIEW: 'view',
  ADD: 'add',
  REMOVE: 'remove',
  EDIT: 'edit',
  DELETE: 'delete',
  RESTORE: 'restore',
  UPLOAD: 'upload',
  REUPLOAD: 'reUpload',
  MARK: 'mark',
  UNMARK: 'unMark',
  LIKE: 'like',
  UNLIKE: 'unLike',
  COMMENT: 'comment',
  ACCEPT: 'accept',
  NOT_ACCEPT: 'notAccept',
  FORGOT_PASSWORD: 'forgotPassword',
  MORE: 'more',
  LOCK: 'lock',
  UNLOCK: 'unlock',
  SEARCH: 'search',
  DEAL: 'deal',
  INVITE: 'invite',
  /**
   * Type
   */
  ABSOLUTE: 'absolute',
  AUTO: 'auto',
  EXTEND: 'extend',
  SHORTCUT: 'shortcut',
  FULL: 'full',
  DIALOG: 'dialog',
  STATIC: 'static',
  POPUP: 'popup',
  TEXT: 'text',
  IMAGE: 'image',
  MENU: 'menu',
  POST: 'post',
  PUT: 'put',
  GET: 'get',
  /**
   * Result
   */
  RESULT: 'result',
  /**
   * Status
   */
  STATUS: 'status',
  NONE: 'none',
  WAIT: 'wait',
  FAILED: 'failed',
  SUCCESS: 'success',
  WARNING: 'warning',
  INFO: 'info',
  RESPONSE: 'response',
  ERROR: 'error',
  /**
   * Other
   */
  VI: 'vi',
  PATH: ':path',
};
