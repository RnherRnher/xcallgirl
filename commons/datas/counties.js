module.exports = {
  VN: {
    'VN-44': [
      { id: '1', name: 'TP Long Xuyên' },
      { id: '2', name: 'TP Châu Đốc' },
      { id: '3', name: 'Thị xã Tân Châu' },
      { id: '4', name: 'Huyện An Phú' },
      { id: '5', name: 'Huyện Châu Phú' },
      { id: '6', name: 'Huyện Châu Thành' },
      { id: '7', name: 'Huyện Chợ Mới' },
      { id: '8', name: 'Huyện Phú Tân' },
      { id: '9', name: 'Huyện Thoại Sơn' },
      { id: '10', name: 'Huyện Tịnh Biên' },
      { id: '11', name: 'Huyện Tri Tôn' },
    ],
    'VN-43': [
      { id: '1', name: 'TP Bà Rịa' },
      { id: '2', name: 'TP Vũng Tàu' },
      { id: '3', name: 'Huyện Châu Đức' },
      { id: '4', name: 'Huyện Côn Đảo' },
      { id: '5', name: 'Huyện Đất Đỏ' },
      { id: '6', name: 'Huyện Long Điền' },
      { id: '7', name: 'Huyện Xuyên Mộc' },
    ],
    // { 'VN-54': { 'Bắc Giang' } },
    // { 'VN-53': { 'Bắc Kạn' } },
    // { 'VN-55': { 'Bạc Liêu' } },
    // { 'VN-56': { 'Bắc Ninh' } },
    // { 'VN-50': { 'Bến Tre' } },
    'VN-31': [
      { id: '1', name: 'TP Quy Nhơn' },
      { id: '2', name: 'Thị xã An Nhơn' },
      { id: '3', name: 'Huyện An Lão' },
      { id: '4', name: 'Huyện Hoài Ân' },
      { id: '5', name: 'Huyện Hoài Nhơn' },
      { id: '6', name: 'Huyện Phù Cát' },
      { id: '7', name: 'Huyện Phù Mỹ' },
      { id: '8', name: 'Huyện Tây Sơn' },
      { id: '9', name: 'Huyện Tuy Phước' },
      { id: '10', name: 'Huyện Vân Canh' },
      { id: '11', name: 'Huyện Vĩnh Thạnh' },
    ],
    'VN-57': [
      { id: '1', name: 'TP Thủ Dầu Một' },
      { id: '2', name: 'Thị xã Bến Cát' },
      { id: '3', name: 'Thị xã Dĩ An' },
      { id: '4', name: 'Thị xã Tân Uyên' },
      { id: '5', name: 'Thị xã Thuận An' },
      { id: '6', name: 'Huyện Bắc Tân Uyên' },
      { id: '7', name: 'Huyện Bàu Bàng' },
      { id: '8', name: 'Huyện Dầu Tiếng' },
      { id: '9', name: 'Huyện Phú Giáo' },
    ],
    // { 'VN-58': { 'Bình Phước' } },
    'VN-40': [
      { id: '1', name: 'TP Phan Thiết' },
      { id: '2', name: 'Thị La Gi' },
      { id: '3', name: 'Huyện Bắc Bình' },
      { id: '4', name: 'Huyện Đức Linh' },
      { id: '5', name: 'Huyện Hàm Tân' },
      { id: '6', name: 'Huyện Hàm Thuận Bắc' },
      { id: '7', name: 'Huyện Hàm Thuận Nam' },
      { id: '8', name: 'Huyện Phú Quý' },
      { id: '9', name: 'Huyện Tánh Linh' },
      { id: '10', name: 'Huyện Tuy Phong' },
    ],
    // { 'VN-59': { 'Cà Mau' } },
    'VN-48': [
      { id: '1', name: 'Quận Bình Thủy' },
      { id: '2', name: 'Quận Cái Răng' },
      { id: '3', name: 'Quận Ninh Kiều' },
      { id: '4', name: 'Quận Ô Môn' },
      { id: '5', name: 'Quận Thốt Nốt' },
      { id: '6', name: 'Quận Cờ Đỏ' },
      { id: '7', name: 'Quận Phong Điền' },
      { id: '8', name: 'Quận Thới Lai' },
      { id: '9', name: 'Quận Vĩnh Thạnh' },
    ],
    // { 'VN-04': { 'Cao Bằng' } },
    'VN-60': [
      { id: '1', name: 'Quận Cẩm Lệ' },
      { id: '2', name: 'Quận Hải Châu' },
      { id: '3', name: 'Quận Liên Chiểu' },
      { id: '4', name: 'Quận Ngũ Hành Sơn' },
      { id: '5', name: 'Quận Sơn Trà' },
      { id: '6', name: 'Quận Thanh Khê' },
      { id: '7', name: 'Quận Hòa Vang' },
      { id: '8', name: 'Quận Hoàng Sa' },
    ],
    'VN-33': [
      { id: '1', name: 'TP Buôn Ma Thuột' },
      { id: '2', name: 'Thị xã Buôn Hồ' },
      { id: '3', name: 'Huyện Buôn Đôn' },
      { id: '4', name: 'Huyện Cư Kuin' },
      { id: '5', name: 'Huyện Cư M\'gar' },
      { id: '6', name: 'Huyện Ea H\'leo' },
      { id: '7', name: 'Huyện Ea Kar' },
      { id: '8', name: 'Huyện Ea Súp' },
      { id: '9', name: 'Huyện Krông Ana' },
      { id: '10', name: 'Huyện Krông Bông' },
      { id: '11', name: 'Huyện Krông Búk' },
      { id: '12', name: 'Huyện Krông Năng' },
      { id: '13', name: 'Huyện Krông Pắk' },
      { id: '14', name: 'Huyện Lắk' },
      { id: '15', name: 'Huyện M\'Đrắk' },
    ],
    // { 'VN-72': { 'Đắk Nông' } },
    // { 'VN-71': { 'Điện Biên' } },
    'VN-39': [
      { id: '1', name: 'TP Biên Hòa' },
      { id: '2', name: 'Thị xã Long Khánh' },
      { id: '3', name: 'Huyện Vĩnh Cửu' },
      { id: '4', name: 'Huyện Trảng Bom' },
      { id: '5', name: 'Huyện Long Thành' },
      { id: '6', name: 'Huyện Nhơn Trạch' },
      { id: '7', name: 'Huyện Xuân Lộc' },
      { id: '8', name: 'Huyện Định Quán' },
      { id: '9', name: 'Huyện Tân Phú' },
      { id: '10', name: 'Huyện Thống Nhất' },
      { id: '11', name: 'Huyện Cẩm Mỹ' },
    ],
    // { 'VN-45': { 'Đồng Tháp' } },
    // { 'VN-30': { 'Gia Lai' } },
    // { 'VN-03': { 'Hà Giang' } },
    // { 'VN-63': { 'Hà Nam' } },
    'VN-64': [
      { id: '1', name: 'Quận Ba Đình' },
      { id: '2', name: 'Quận Hoàn Kiếm' },
      { id: '3', name: 'Quận Hai Bà Trưng' },
      { id: '4', name: 'Quận Đống Đa' },
      { id: '5', name: 'Quận Tây Hồ' },
      { id: '6', name: 'Quận Cầu Giấy' },
      { id: '7', name: 'Quận Thanh Xuân' },
      { id: '8', name: 'Quận Hoàng Mai' },
      { id: '9', name: 'Quận Long Biên' },
      { id: '10', name: 'Quận Bắc Từ Liêm' },
      { id: '11', name: 'Quận Hà Đông' },
      { id: '12', name: 'Quận Nam Từ Liêm' },
      { id: '13', name: 'Thị xã Sơn Tây' },
      { id: '14', name: 'Huyện Thanh Trì' },
      { id: '15', name: 'Huyện Gia Lâm' },
      { id: '16', name: 'Huyện Đông Anh' },
      { id: '17', name: 'Huyện Sóc Sơn' },
      { id: '18', name: 'Huyện Ba Vì' },
      { id: '19', name: 'Huyện Phúc Thọ' },
      { id: '20', name: 'Huyện Thạch Thất' },
      { id: '21', name: 'Huyện Quốc Oai' },
      { id: '22', name: 'Huyện Chương Mỹ' },
      { id: '23', name: 'Huyện Đan Phượng' },
      { id: '24', name: 'Huyện Hoài Đức' },
      { id: '25', name: 'Huyện Thanh Oai' },
      { id: '26', name: 'Huyện Mỹ Đức' },
      { id: '27', name: 'Huyện Ứng Hòa' },
      { id: '28', name: 'Huyện Thường Tín' },
      { id: '29', name: 'Huyện Phú Xuyên' },
      { id: '30', name: 'Huyện Mê Linh' },
    ],
    // { 'VN-23': { 'Hà Tĩnh' } },
    // 'VN-61': [
    //   { id: '1', name: 'TP Hải Dương' },
    //   { id: '2', name: 'Quận Bình Giang' },
    //   { id: '3', name: 'Quận Cẩm Giàng' },
    //   { id: '4', name: 'Quận Nam Gia Lộc' },
    // ],
    'VN-62': [
      { id: '1', name: 'Quận Đồ Sơn' },
      { id: '2', name: 'Quận Dương Kinh' },
      { id: '3', name: 'Quận Hải An' },
      { id: '4', name: 'Quận Hồng Bàng' },
      { id: '5', name: 'Quận Kiến An' },
      { id: '6', name: 'Quận Lê Chân' },
      { id: '7', name: 'Quận Ngô Quyền' },
      { id: '8', name: 'Huyện An Dương' },
      { id: '9', name: 'Huyện An Lão' },
      { id: '10', name: 'Huyện Bạch Long Vĩ' },
      { id: '11', name: 'Huyện Cát Hải' },
      { id: '12', name: 'Huyện Kiến Thụy' },
      { id: '13', name: 'Huyện Thủy Nguyên' },
      { id: '14', name: 'Huyện Tiên Lãng' },
      { id: '15', name: 'Huyện Vĩnh Bảo' },
    ],
    // { 'VN-73': { 'Hậu Giang' } },
    'VN-65': [
      { id: '1', name: 'Quận 1' },
      { id: '2', name: 'Quận 2' },
      { id: '3', name: 'Quận 3' },
      { id: '4', name: 'Quận 4' },
      { id: '5', name: 'Quận 5' },
      { id: '6', name: 'Quận 6' },
      { id: '7', name: 'Quận 7' },
      { id: '8', name: 'Quận 8' },
      { id: '9', name: 'Quận 9' },
      { id: '10', name: 'Quận 10' },
      { id: '11', name: 'Quận 11' },
      { id: '12', name: 'Quận 12' },
      { id: '13', name: 'Quận Thủ Đức' },
      { id: '14', name: 'Quận Bình Thạnh' },
      { id: '15', name: 'Quận Gò Vấp' },
      { id: '16', name: 'Quận Phú Nhuận' },
      { id: '17', name: 'Quận Tân Phú' },
      { id: '18', name: 'Quận Bình Tân' },
      { id: '19', name: 'Quận Tân Bình' },
      { id: '20', name: 'Huyện Củ Chi' },
      { id: '21', name: 'Huyện Hóc Môn' },
      { id: '22', name: 'Huyện Bình Chánh' },
      { id: '23', name: 'Huyện Nhà Bè' },
      { id: '24', name: 'Huyện Cần Giờ' }
    ],
    // { 'VN-14': { 'Hòa Bình' } },
    // { 'VN-66': { 'Hưng Yên' } },
    'VN-34': [
      { id: '1', name: 'TP Nha Trang' },
      { id: '2', name: 'TP Cam Ranh' },
      { id: '3', name: 'Thị xã Ninh Hòa' },
      { id: '4', name: 'Huyện Vạn Ninh' },
      { id: '5', name: 'Huyện Diên Khánh' },
      { id: '6', name: 'Huyện Khánh Vĩnh' },
      { id: '7', name: 'Huyện Khánh Sơn' },
      { id: '8', name: 'Huyện Cam Lâm' },
      { id: '9', name: 'Huyện Trường Sa' },
    ],
    'VN-47': [
      { id: '1', name: 'TP Rạch Giá' },
      { id: '2', name: 'TP Hà Tiên' },
      { id: '3', name: 'Huyện An Biên' },
      { id: '4', name: 'Huyện An Minh' },
      { id: '5', name: 'Huyện Châu Thành' },
      { id: '6', name: 'Huyện Giang Thành' },
      { id: '7', name: 'Huyện Giồng Riềng' },
      { id: '8', name: 'Huyện Gò Quao' },
      { id: '9', name: 'Huyện Hòn Đất' },
      { id: '10', name: 'Huyện Kiên Hải' },
      { id: '11', name: 'Huyện Kiên Lương' },
      { id: '12', name: 'Huyện Phú Quốc' },
      { id: '13', name: 'Huyện Tân Hiệp' },
      { id: '14', name: 'Huyện U Minh Thượng' },
      { id: '15', name: 'Huyện Vĩnh Thuận' },
    ],
    // { 'VN-28': { 'Kon Tum' } },
    // { 'VN-01': { 'Lai Châu' } },
    // { 'VN-35': { 'Lâm Đồng' } },
    // { 'VN-09': { 'Lạng Sơn' } },
    // { 'VN-02': { 'Lào Cai' } },
    // { 'VN-41': { 'Long An' } },
    'VN-67': [
      { id: '1', name: 'TP Nam Định' },
      { id: '2', name: 'Huyện Giao Thủy' },
      { id: '3', name: 'Huyện Hải Hậu' },
      { id: '4', name: 'Huyện Mỹ Lộc' },
      { id: '5', name: 'Huyện Nam Trực' },
      { id: '6', name: 'Huyện Nghĩa Hưng' },
      { id: '7', name: 'Huyện Trực Ninh' },
      { id: '8', name: 'Huyện Vụ Bản' },
      { id: '9', name: 'Huyện Xuân Trường' },
      { id: '10', name: 'Huyện Ý Yên' },
    ],
    'VN-22': [
      { id: '1', name: 'TP Vinh' },
      { id: '2', name: 'Thị xã Cửa Lò' },
      { id: '3', name: 'Thị xã Thái Hòa' },
      { id: '4', name: 'Huyện Anh Sơn' },
      { id: '5', name: 'Huyện Con Cuông' },
      { id: '6', name: 'Huyện Diễn Châu' },
      { id: '7', name: 'Huyện Đô Lương' },
      { id: '8', name: 'Huyện Hưng Nguyên' },
      { id: '9', name: 'Huyện Kỳ Sơn' },
      { id: '10', name: 'Huyện Nam Đàn' },
      { id: '11', name: 'Huyện Nghi Lộc' },
      { id: '12', name: 'Huyện Nghĩa Đàn' },
      { id: '13', name: 'Huyện Quế Phong' },
      { id: '14', name: 'Huyện Quỳ Châu' },
      { id: '15', name: 'Huyện Quỳ Hợp' },
      { id: '16', name: 'Huyện Quỳnh Lưu' },
      { id: '17', name: 'Huyện Tân Kỳ' },
      { id: '18', name: 'Huyện Thanh Chương' },
      { id: '19', name: 'Huyện Tương Dương' },
      { id: '20', name: 'Huyện Yên Thành' },
    ],
    // { 'VN-18': { 'Ninh Bình' } },
    // { 'VN-36': { 'Ninh Thuận' } },
    // { 'VN-68': { 'Phú Thọ' } },
    // { 'VN-32': { 'Phú Yên' } },
    // { 'VN-24': { 'Quảng Bình' } },
    // { 'VN-27': { 'Quảng Nam' } },
    // { 'VN-29': { 'Quảng Ngãi' } },
    'VN-13': [
      { id: '1', name: 'TP Hạ Long' },
      { id: '2', name: 'TP Cẩm Phả' },
      { id: '3', name: 'TP Móng Cái' },
      { id: '4', name: 'TP Uông Bí' },
      { id: '5', name: 'Thị xã Đông Triều' },
      { id: '6', name: 'Thị xã Quảng Yên' },
      { id: '7', name: 'Huyện Vân Đồn' },
      { id: '8', name: 'Huyện Tiên Yên' },
      { id: '9', name: 'Huyện Hoành Bồ' },
      { id: '10', name: 'Huyện Hải Hà' },
      { id: '11', name: 'Huyện Cô Tô' },
      { id: '12', name: 'Huyện Đầm Hà' },
      { id: '13', name: 'Huyện Bình Liêu' },
      { id: '14', name: 'Huyện Ba Chẽ' },
    ],
    // { 'VN-25': { 'Quảng Trị' } },
    // { 'VN-52': { 'Sóc Trăng' } },
    // { 'VN-05': { 'Sơn La' } },
    // { 'VN-37': { 'Tây Ninh' } },
    // { 'VN-20': { 'Thái Bình' } },
    'VN-69': [
      { id: '1', name: 'TP Thái Nguyên' },
      { id: '2', name: 'TP Sông Công' },
      { id: '3', name: 'Thị xã Phổ Yên' },
      { id: '4', name: 'Huyện Đại Từ' },
      { id: '5', name: 'Huyện Định Hóa' },
      { id: '6', name: 'Huyện Đồng Hỷ' },
      { id: '7', name: 'Huyện Phú Bình' },
      { id: '8', name: 'Huyện Phú Lương' },
      { id: '9', name: 'Huyện Võ Nhai' },
    ],
    'VN-21': [
      { id: '1', name: 'TP Thanh Hóa' },
      { id: '2', name: 'TP Sầm Sơn' },
      { id: '3', name: 'Thị xã Bỉm Sơn' },
      { id: '4', name: 'Huyện Bá Thước' },
      { id: '5', name: 'Huyện Cẩm Thủy' },
      { id: '6', name: 'Huyện Đông Sơn' },
      { id: '7', name: 'Huyện Hà Trung' },
      { id: '8', name: 'Huyện Hậu Lộc' },
      { id: '9', name: 'Huyện Hoằng Hóa' },
      { id: '10', name: 'Huyện Lang Chánh' },
      { id: '11', name: 'Huyện Mường Lát' },
      { id: '12', name: 'Huyện Nga Sơn' },
      { id: '13', name: 'Huyện Ngọc Lặc' },
      { id: '14', name: 'Huyện Như Thanh' },
      { id: '15', name: 'Huyện Như Xuân' },
      { id: '16', name: 'Huyện Nông Cống' },
      { id: '17', name: 'Huyện Quan Hóa' },
      { id: '18', name: 'Huyện Quan Sơn' },
      { id: '19', name: 'Huyện Quảng Xương' },
      { id: '20', name: 'Huyện Thạch Thành' },
      { id: '21', name: 'Huyện Thiệu Hóa' },
      { id: '22', name: 'Huyện Thọ Xuân' },
      { id: '23', name: 'Huyện Thường Xuân' },
      { id: '24', name: 'Huyện Tĩnh Gia' },
      { id: '25', name: 'Huyện Triệu Sơn' },
      { id: '26', name: 'Huyện Vĩnh Lộc' },
      { id: '27', name: 'Huyện Yên Định' },
    ],
    'VN-26': [
      { id: '1', name: 'TP Huế' },
      { id: '2', name: 'Thị xã Hương Thủy' },
      { id: '3', name: 'Thị xã Hương Trà' },
      { id: '4', name: 'Huyện A Lưới' },
      { id: '5', name: 'Huyện Nam Đông' },
      { id: '6', name: 'Huyện Phong Điền' },
      { id: '7', name: 'Huyện Phú Lộc' },
      { id: '8', name: 'Huyện Phú Vang' },
      { id: '9', name: 'Huyện Quảng Điền' },
    ],
    // { 'VN-46': { 'Tiền Giang' } },
    // { 'VN-51': { 'Trà Vinh' } },
    // { 'VN-07': { 'Tuyên Quang' } },
    // { 'VN-49': { 'Vĩnh Long' } },
    // { 'VN-70': { 'Vĩnh Phúc' } },
    // { 'VN-06': { 'Yên Bái' }
  }
};
