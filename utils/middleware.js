const validation = require('express-validation');

/**
 * @overview Xác thực tha số cho bộ định tuyến
 *
 * @param   {Object} paramsValidationFuncations
 * @param   {String} paramName
 * @returns {Funcation}
 */
const paramsValidation = (paramsValidationFuncations, paramName) => {
  const paramValidationFuncation = paramsValidationFuncations[paramName];
  if (!paramValidationFuncation) {
    return (req, res, next) => {
      return next(new Error('Not params validation funcation'));
    };
  }

  return validation(paramValidationFuncation);
};
module.exports.paramsValidation = paramsValidation;
