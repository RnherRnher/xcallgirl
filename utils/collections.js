
const faker = require('faker');
const viFaker = require('faker/locale/vi');
const mongoose = require('mongoose');
const constant = require('../commons/utils/constant');
const regular = require('../commons/utils/regular-expression');
const countries = require('../commons/datas/countries');
const cities = require('../commons/datas/cities');
const counties = require('../commons/datas/counties');
const {
  validateModelUser,
  validateModelNotification,
  validateModelActivityHistory,
  validateModelBusinessCardDealHistory,
  validateModelCredit
} = require('./validate');

const COUNT = 100;
const curentYear = new Date().getFullYear();
const password = 'password';
const thumbnailImageURL = 'https://res.cloudinary.com/xcallgirl/image/upload/v1544726597/prod/default/core/thumbnail.png';
const thumbnailImagePublicID = 'prod/default/core/thumbnail.png';

const storeId = mongoose.Types.ObjectId();
const listAllUserId = [];
const listUserId = [];
const listProviderUserId = [];
const listAdminUserId = [];
const listSystemUserId = [];
const listBusinessCardId = [];
const lisBusinessCardDealHistoryId = [];
const listNotificationId = [];
const listActivityHistoryId = [];
const listCreditId = [];

const listPhoneCode = countries.map((country) => {
  return country.phoneCode;
});

const listCountries = countries.map((country) => {
  return country.id;
});

const listCities = (country) => {
  return cities[country].map((city) => {
    return city.id;
  });
};

const listCounties = (country, city) => {
  return (counties[country])[city].map((county) => {
    return county.id;
  });
};

const getStoreDocs = () => {
  const docs = [];

  docs.push({
    overwrite: true,
    data: { _id: storeId }
  });

  return docs;
};

const getUserDocs = (count) => {
  const docs = [];

  const validates = validateModelUser();

  for (let index = 0; index < count; index++) {
    const country = listCountries[faker.random.number({ min: 0, max: listCountries.length - 1 })];
    let city = listCities(country);
    city = city[faker.random.number({ min: 0, max: city.length - 1 })];
    let county = listCounties(country, city);
    county = county[faker.random.number({ min: 0, max: county.length - 1 })];

    const firstName = viFaker.name.firstName();
    const lastName = viFaker.name.lastName();

    const gender = regular.user.gender[faker.random.number({ min: 0, max: regular.user.gender.length - 1 })];

    let isPhoneVerify = faker.random.boolean();
    let isAccountVerify = false;
    let accountVerifyStatus = validates.statusVerfy[faker.random.number({ min: 0, max: validates.statusVerfy.length - 2 })];

    const _id = mongoose.Types.ObjectId();
    listAllUserId.push(_id);

    let roles = null;
    if (index === 0) {
      roles = constant.ADMIN;
    } else if (index === count - 1) {
      roles = constant.SYSTEM;
    } else {
      roles = regular.user.roles[faker.random.number({ min: 0, max: regular.user.roles.length - 2 })];
    }

    let isLock = false;
    let isDelete = false;
    let number = faker.random.number();

    if (roles === constant.USER && listUserId.length === 0) {
      number = 0;
    } else if (roles === constant.PROVIDER && listProviderUserId.length === 0) {
      number = 1;
    } else if (roles === constant.ADMIN && listAdminUserId.length === 0) {
      number = 2;
    } else if (roles === constant.SYSTEM && listSystemUserId.length === 0) {
      number = 3;
    } else {
      isLock = faker.random.boolean();
      isDelete = faker.random.boolean();
    }

    switch (roles) {
      case constant.USER:
        listUserId.push(_id);
        break;
      case constant.PROVIDER:
        listProviderUserId.push(_id);
        listBusinessCardId.push(mongoose.Types.ObjectId());
        isAccountVerify = true;
        accountVerifyStatus = constant.SUCCESS;
        break;
      case constant.ADMIN:
        listAdminUserId.push(_id);
        isAccountVerify = true;
        accountVerifyStatus = constant.SUCCESS;
        break;
      case constant.SYSTEM:
        listSystemUserId.push(_id);
        isPhoneVerify = true;
        isAccountVerify = true;
        accountVerifyStatus = constant.SUCCESS;
        break;
      default:
        break;
    }

    const accountVerifyAdmin = [];
    const accountVerifyData = [];
    if (accountVerifyStatus !== constant.NONE) {
      const listAdminUserLimit = faker.random.number({ min: 1, max: listAdminUserId.length });

      for (let index = 0; index < listAdminUserLimit; index++) {
        if (accountVerifyStatus === constant.WAIT && index === listAdminUserLimit - 1) {
          accountVerifyData.push(
            {
              publicID: thumbnailImagePublicID,
              url: thumbnailImageURL,
              initDate: faker.random.number({ min: Date.now() - constant.ENUM.TIME.ONE_MONTH, max: Date.now() })
            }
          );
        } else {
          accountVerifyAdmin.push(
            {
              _id: listAdminUserId[faker.random.number({ min: 0, max: listAdminUserId.length - 1 })],
              reason: isAccountVerify ? undefined : viFaker.lorem.sentence(),
              confirmDate: faker.random.number({ min: Date.now() - constant.ENUM.TIME.ONE_MONTH, max: Date.now() })
            }
          );

          accountVerifyData.push(
            {
              publicID: thumbnailImagePublicID,
              url: thumbnailImageURL,
              initDate: faker.random.number({ min: Date.now() - constant.ENUM.TIME.ONE_MONTH, max: Date.now() })
            }
          );
        }
      }
    }

    const listAdminLock = [];
    if (isLock) {
      const listAdminUserLimit = faker.random.number({ min: 1, max: listAdminUserId.length });

      for (let index = 0; index < listAdminUserLimit; index++) {
        listAdminLock.push({
          _id: listAdminUserId[index],
          reason: viFaker.lorem.sentence(),
          confirmDate: faker.random.number({ min: Date.now() - constant.ENUM.TIME.ONE_MONTH, max: Date.now() })
        });
      }
    }

    docs.push({
      // Set to true to overwrite this document
      // when it already exists in the collection.
      // If set to false, or missing, the seed operation
      // will skip this document to avoid overwriting it.
      overwrite: true,
      data: {
        _id,
        name: {
          first: firstName,
          last: lastName,
          display: `${firstName} ${lastName}`,
          nick: faker.internet.userName(),
        },
        roles: {
          value: roles
        },
        avatar: {
          url: validates.url.avatar[gender]
        },
        numberPhone: {
          code: listPhoneCode[faker.random.number({ min: 0, max: listCountries.length - 1 })],
          number
        },
        gender,
        birthday: {
          day: faker.random.number({ min: 1, max: 31 }),
          month: faker.random.number({ min: 0, max: 11 }),
          year: faker.random.number({
            min: curentYear - 100,
            max: curentYear - regular.app.ageRestriction
          }),
        },
        password,
        address: {
          country,
          city,
          county,
          local: viFaker.address.streetAddress(),
        },
        lock: {
          is: isLock,
          admin: listAdminLock,
          expiresDate: isLock
            ? faker.random.number({ min: Date.now() - constant.ENUM.TIME.ONE_MONTH, max: Date.now() + constant.ENUM.TIME.ONE_MONTH })
            : undefined
        },
        delete: {
          is: isDelete,
          reason: isDelete ? viFaker.lorem.sentence() : undefined,
          initDate: isDelete
            ? faker.random.number({ min: Date.now() - constant.ENUM.TIME.ONE_MONTH, max: Date.now() })
            : undefined
        },
        verify: {
          account: {
            is: isAccountVerify,
            status: accountVerifyStatus,
            data: accountVerifyData,
            admin: accountVerifyAdmin,
          },
          numberPhone: {
            is: isPhoneVerify,
            initDate: isPhoneVerify ? Date.now() : undefined,
          },
        },
        settings: {
          general: {
            language: constant.VI,
          },
        },
        initDate: faker.random.number({ min: Date.now() - constant.ENUM.TIME.ONE_MONTH, max: Date.now() })
      }
    });
  }

  return docs;
};

const getBusinessCardDocs = () => {
  const docs = [];

  for (let index = 0; index < listProviderUserId.length; index++) {
    const country = listCountries[faker.random.number({ min: 0, max: listCountries.length - 1 })];
    let city = listCities(country);
    city = city[faker.random.number({ min: 0, max: city.length - 1 })];
    let county = listCounties(country, city);
    county = county[faker.random.number({ min: 0, max: county.length - 1 })];

    const gender = regular.user.gender[faker.random.number({ min: 0, max: regular.user.gender.length - 1 })];

    const rooms = [{
      value: constant.ONE_TIME,
      moneys: faker.random.number({
        min: regular.commons.moneys.min,
        max: regular.commons.moneys.max
      }),
    }, {
      value: constant.OVER_NIGHT,
      moneys: faker.random.number({
        min: regular.commons.moneys.min,
        max: regular.commons.moneys.max
      }),
    }];

    const services = [{
      value: constant.ONE_SLOT,
      moneys: faker.random.number({
        min: regular.commons.moneys.min,
        max: regular.commons.moneys.max
      }),
    }, {
      value: constant.OVER_NIGHT,
      moneys: faker.random.number({
        min: regular.commons.moneys.min,
        max: regular.commons.moneys.max
      }),
    }, {
      value: constant.SOME,
      moneys: faker.random.number({
        min: regular.commons.moneys.min,
        max: regular.commons.moneys.max
      }),
    }, {
      value: constant.SOME_OVER_NIGHT,
      moneys: faker.random.number({
        min: regular.commons.moneys.min,
        max: regular.commons.moneys.max
      }),
    }];

    const currentRooms = rooms.slice(0, faker.random.number({ min: 0, max: rooms.length - 1 }));
    const currentServices = services.slice(0, faker.random.number({ min: 1, max: services.length - 1 }));

    docs.push({
      overwrite: true,
      data: {
        _id: listBusinessCardId[index],
        gender,
        userID: listProviderUserId[index],
        title: viFaker.lorem.sentence(),
        descriptions: viFaker.lorem.paragraph(),
        points: faker.random.number({ min: 0, max: 30 * 19200 }),
        pointsDay: faker.random.number({ min: 0, max: 30 * 50 }),
        blurFace: faker.random.boolean(),
        status: {
          is: faker.random.boolean(),
        },
        measurements3: {
          breast: faker.random.number(),
          waist: faker.random.number(),
          hips: faker.random.number(),
        },
        address: {
          country,
          city,
          county,
          local: viFaker.address.streetAddress(),
        },
        costs: {
          rooms: currentRooms,
          services: currentServices,
          currencyUnit: regular.commons.moneys.currencyUnit[
            faker.random.number({ min: 0, max: regular.commons.moneys.currencyUnit.length - 1 })
          ]
        },
        initDate: faker.random.number({ min: Date.now() - constant.ENUM.TIME.ONE_MONTH, max: Date.now() }),
        updateDate: faker.random.number({ min: Date.now() - constant.ENUM.TIME.ONE_MONTH, max: Date.now() }),
        reUpload: {
          is: faker.random.boolean(),
          count: faker.random.number({ min: 0, max: 10 }),
          date: faker.random.number({ min: Date.now() - constant.ENUM.TIME.ONE_MONTH, max: Date.now() })
        }
      }
    });
  }

  return docs;
};

const getBusinessCardDealHistoryDocs = () => {
  const docs = [];

  const validates = validateModelBusinessCardDealHistory();
  const listUserLimit = faker.random.number({ min: 1, max: listUserId.length });

  for (let index1 = 0; index1 < listProviderUserId.length; index1++) {
    for (let index2 = 0; index2 < listUserLimit; index2++) {
      const country = listCountries[faker.random.number({ min: 0, max: listCountries.length - 1 })];
      let city = listCities(country);
      city = city[faker.random.number({ min: 0, max: city.length - 1 })];
      let county = listCounties(country, city);
      county = county[faker.random.number({ min: 0, max: county.length - 1 })];

      const rooms = [{
        value: constant.ONE_TIME,
        moneys: faker.random.number({
          min: regular.commons.moneys.min,
          max: regular.commons.moneys.max
        }),
      }, {
        value: constant.OVER_NIGHT,
        moneys: faker.random.number({
          min: regular.commons.moneys.min,
          max: regular.commons.moneys.max
        }),
      }];

      const services = [{
        value: constant.ONE_SLOT,
        moneys: faker.random.number({
          min: regular.commons.moneys.min,
          max: regular.commons.moneys.max
        }),
      }, {
        value: constant.OVER_NIGHT,
        moneys: faker.random.number({
          min: regular.commons.moneys.min,
          max: regular.commons.moneys.max
        }),
      }, {
        value: constant.SOME,
        moneys: faker.random.number({
          min: regular.commons.moneys.min,
          max: regular.commons.moneys.max
        }),
      }, {
        value: constant.SOME_OVER_NIGHT,
        moneys: faker.random.number({
          min: regular.commons.moneys.min,
          max: regular.commons.moneys.max
        }),
      }];

      const isAcceptUser = faker.random.boolean();
      const isAcceptProvider = faker.random.boolean();
      const status = regular.businessCardDealHistory.status[faker.random.number({ min: 0, max: regular.businessCardDealHistory.status.length - 1 })];
      const timer = faker.random.number({ min: Date.now() - constant.ENUM.TIME.ONE_DAY, max: Date.now() + constant.ENUM.TIME.ONE_DAY });
      const isResult = (!isAcceptUser || !isAcceptProvider || timer - regular.businessCardDealHistory.expires.token < Date.now())
        ? constant.FAILED
        : validates.result[faker.random.number({ min: 0, max: validates.result.length - 2 })];
      const isReportUser = isResult === constant.WAIT ? false : faker.random.boolean();
      const isReportProvider = isResult === constant.WAIT ? false : faker.random.boolean();

      const _id = mongoose.Types.ObjectId();
      lisBusinessCardDealHistoryId.push(_id);

      docs.push({
        overwrite: true,
        data: {
          _id,
          providerID: listProviderUserId[index1],
          businessCardID: listBusinessCardId[index1],
          userID: listUserId[index2],
          timer,
          accept: {
            user: {
              is: isAcceptUser,
              date: Date.now(),
              reason: isAcceptUser ? undefined : viFaker.lorem.sentence()
            },
            provider: {
              is: isAcceptProvider,
              date: Date.now(),
              reason: isAcceptProvider ? undefined : viFaker.lorem.sentence()
            },
          },
          result: {
            is: isResult
          },
          status,
          reports: {
            user: {
              is: isReportUser,
              date: Date.now(),
              reason: isReportUser ? viFaker.lorem.sentence() : undefined
            },
            provider: {
              is: isReportProvider,
              date: Date.now(),
              reason: isReportProvider ? viFaker.lorem.sentence() : undefined
            },
          },
          costs: {
            room: rooms[faker.random.number({ min: 0, max: rooms.length - 1 })],
            service: services[faker.random.number({ min: 0, max: services.length - 1 })],
            currencyUnit: regular.commons.moneys.currencyUnit[
              faker.random.number({ min: 0, max: regular.commons.moneys.currencyUnit.length - 1 })
            ]
          },
          token: status === constant.VERIFY ? faker.random.number() + faker.random.word() + faker.random.number() : undefined,
          address: {
            country,
            city,
            county,
            local: viFaker.address.streetAddress(),
          },
          note: viFaker.lorem.sentence(),
          initDate: faker.random.number({ min: Date.now() - constant.ENUM.TIME.ONE_MONTH, max: Date.now() })
        }
      });
    }
  }

  return docs;
};

const getCreditDocs = () => {
  const docs = [];

  const validates = validateModelCredit();
  const listUserAllLimit = faker.random.number({ min: 0, max: listAllUserId.length });

  for (let index1 = 0; index1 < listAllUserId.length; index1++) {
    const historys = [];

    for (let index2 = 0; index2 < listUserAllLimit; index2++) {
      historys.push({
        userID: listAllUserId[index2],
        balance: faker.random.number({
          min: regular.commons.moneys.min,
          max: regular.commons.moneys.max
        }),
        type: validates.historys.type[faker.random.number({ min: 0, max: validates.historys.type.length - 1 })],
        initDate: faker.random.number({ min: Date.now() - constant.ENUM.TIME.ONE_MONTH, max: Date.now() })
      });
    }

    const _id = mongoose.Types.ObjectId();
    listCreditId.push(_id);

    docs.push({
      overwrite: true,
      data: {
        _id,
        userID: listAllUserId[index1],
        balance: faker.random.number({
          min: regular.commons.moneys.min,
          max: regular.commons.moneys.max
        }),
        historys,
        updateDate: faker.random.number({ min: Date.now() - constant.ENUM.TIME.ONE_MONTH, max: Date.now() })
      }
    });
  }

  return docs;
};

const getActivityHistoryDocs = () => {
  const docs = [];

  const validates = validateModelActivityHistory();
  const listUserAllLimit = faker.random.number({ min: 1, max: listAllUserId.length });
  const listUserLimit = faker.random.number({ min: 1, max: listUserId.length });
  const listUserLimit2 = faker.random.number({ min: 1, max: listUserId.length });

  for (let index1 = 0; index1 < listAllUserId.length; index1++) {
    for (let index2 = 0; index2 < listUserAllLimit; index2++) {
      const _id = mongoose.Types.ObjectId();
      listActivityHistoryId.push(_id);

      docs.push({
        overwrite: true,
        data: {
          _id,
          userID: listAllUserId[index1],
          toURL: listAllUserId[index1],
          type: constant.USER,
          result: validates.result[faker.random.number({ min: 0, max: validates.result.length - 1 })],
          content: 'user.is',
          initDate: faker.random.number({ min: Date.now() - constant.ENUM.TIME.ONE_MONTH, max: Date.now() })
        }
      });

      // TODO: chat
      // _id = mongoose.Types.ObjectId();

      // listActivityHistoryId.push(_id);
      // docs.push({
      //   overwrite: true,
      //   data: {
      //     _id,
      //     userID: listAllUserId[index1],
      //     toURL: _id,
      //     type: constant.CHAT,
      //     result: validates.result[faker.random.number({ min: 0, max: validates.result.length - 1 })],
      //     content: 'chat.is',
      //     initDate: faker.random.number({ min: Date.now() - constant.ENUM.TIME.ONE_MONTH, max: Date.now() })
      //   }
      // });
    }
  }

  for (let index1 = 0; index1 < listProviderUserId.length; index1++) {
    for (let index2 = 0; index2 < listUserLimit; index2++) {
      const _id = mongoose.Types.ObjectId();
      listActivityHistoryId.push(_id);

      docs.push({
        overwrite: true,
        data: {
          _id,
          userID: listProviderUserId[index1],
          toURL: listBusinessCardId[index1],
          type: constant.BUSINESS_CARD,
          result: validates.result[faker.random.number({ min: 0, max: validates.result.length - 1 })],
          content: 'businessCard.is',
          initDate: faker.random.number({ min: Date.now() - constant.ENUM.TIME.ONE_MONTH, max: Date.now() })
        }
      });
    }
  }

  for (let index1 = 0; index1 < listProviderUserId.length; index1++) {
    for (let index2 = 0; index2 < listUserLimit2; index2++) {
      const _id = mongoose.Types.ObjectId();
      listActivityHistoryId.push(_id);

      docs.push({
        overwrite: true,
        data: {
          _id,
          userID: listProviderUserId[index1],
          toURL: lisBusinessCardDealHistoryId[index1],
          type: constant.BUSINESS_CARD_DEAL_HISTORY,
          result: validates.result[faker.random.number({ min: 0, max: validates.result.length - 1 })],
          content: 'businessCardDealHistory.is',
          initDate: faker.random.number({ min: Date.now() - constant.ENUM.TIME.ONE_MONTH, max: Date.now() })
        }
      });
    }
  }

  return docs;
};

const getNotificationDocs = () => {
  const docs = [];

  const validates = validateModelNotification();

  const listUserLimit = faker.random.number({ min: 1, max: listUserId.length });
  const listUserLimit2 = faker.random.number({ min: 1, max: listUserId.length });
  const listUserAllLimit = faker.random.number({ min: 1, max: listAllUserId.length });
  const listAdminUserLimit = faker.random.number({ min: 1, max: listAdminUserId.length });

  for (let index1 = 0; index1 < listProviderUserId.length; index1++) {
    for (let index2 = 0; index2 < listUserLimit; index2++) {
      const _id = mongoose.Types.ObjectId();
      listNotificationId.push(_id);

      docs.push({
        overwrite: true,
        data: {
          _id,
          receiveID: listProviderUserId[index1],
          sendID: listUserId[index2],
          toURL: listBusinessCardId[index1],
          type: constant.BUSINESS_CARD,
          result: validates.result[faker.random.number({ min: 0, max: validates.result.length - 1 })],
          content: 'businessCard.is',
          mark: {
            is: faker.random.boolean()
          },
          initDate: faker.random.number({ min: Date.now() - constant.ENUM.TIME.ONE_MONTH, max: Date.now() })
        }
      });
    }
  }

  for (let index1 = 0; index1 < listAllUserId.length; index1++) {
    for (let index2 = 0; index2 < listUserAllLimit; index2++) {
      let _id = mongoose.Types.ObjectId();
      listNotificationId.push(_id);

      docs.push({
        overwrite: true,
        data: {
          _id,
          receiveID: listAllUserId[index1],
          sendID: listAllUserId[index2],
          toURL: listAllUserId[index1],
          type: constant.USER,
          result: validates.result[faker.random.number({ min: 0, max: validates.result.length - 1 })],
          content: 'user.is',
          mark: {
            is: faker.random.boolean()
          },
          initDate: faker.random.number({ min: Date.now() - constant.ENUM.TIME.ONE_MONTH, max: Date.now() })
        }
      });

      _id = mongoose.Types.ObjectId();
      listNotificationId.push(_id);

      docs.push({
        overwrite: true,
        data: {
          _id,
          receiveID: listAllUserId[index1],
          sendID: listAllUserId[index2],
          toURL: listAllUserId[index1],
          type: constant.CREDIT,
          result: validates.result[faker.random.number({ min: 0, max: validates.result.length - 1 })],
          content: 'credit.is',
          mark: {
            is: faker.random.boolean()
          },
          initDate: faker.random.number({ min: Date.now() - constant.ENUM.TIME.ONE_MONTH, max: Date.now() })
        }
      });
    }
  }

  for (let index1 = 0; index1 < listAllUserId.length; index1++) {
    for (let index2 = 0; index2 < listAdminUserLimit; index2++) {
      const _id = mongoose.Types.ObjectId();
      listNotificationId.push(_id);

      docs.push({
        overwrite: true,
        data: {
          _id,
          receiveID: listAllUserId[index1],
          sendID: listAdminUserId[index2],
          toURL: listAllUserId[index1],
          type: constant.ADMIN,
          result: validates.result[faker.random.number({ min: 0, max: validates.result.length - 1 })],
          content: 'user.roles.admin',
          mark: {
            is: faker.random.boolean()
          },
          initDate: faker.random.number({ min: Date.now() - constant.ENUM.TIME.ONE_MONTH, max: Date.now() })
        }
      });
    }
  }

  for (let index1 = 0; index1 < listProviderUserId.length; index1++) {
    for (let index2 = 0; index2 < listUserLimit2; index2++) {
      const randomToFrom = index1 % 2;

      const _id = mongoose.Types.ObjectId();
      listNotificationId.push(_id);

      docs.push({
        overwrite: true,
        data: {
          _id,
          receiveID: randomToFrom ? listProviderUserId[index1] : listUserId[index2],
          sendID: randomToFrom ? listUserId[index2] : listProviderUserId[index1],
          toURL: lisBusinessCardDealHistoryId[randomToFrom ? index1 : index2],
          type: constant.BUSINESS_CARD_DEAL_HISTORY,
          result: validates.result[faker.random.number({ min: 0, max: validates.result.length - 1 })],
          content: 'businessCardDealHistory.is',
          mark: {
            is: faker.random.boolean()
          },
          initDate: faker.random.number({ min: Date.now() - constant.ENUM.TIME.ONE_MONTH, max: Date.now() })
        }
      });
    }
  }

  return docs;
};

// TODO: system
// TODO: chat

// Thứ tự của các bộ sưu tập trong cấu hình sẽ xác định thứ tự của seeding.
module.exports = [
  {
    model: 'Store',
    options: {
      // Override log results setting at the collection level.
      logResults: true
    },
    skip: {
      // Skip collection when this query return results.
      // e.g. {}: Only seeds collection when it is empty.
      // when: {} // Mongoose qualified query
    },
    docs: getStoreDocs()
  },
  {
    model: 'User',
    options: {
      logResults: true
    },
    skip: {},
    docs: getUserDocs(COUNT)
  },
  {
    model: 'BusinessCard',
    options: {
      logResults: true
    },
    skip: {},
    docs: getBusinessCardDocs()
  },
  {
    model: 'BusinessCardDealHistory',
    options: {
      logResults: true
    },
    skip: {},
    docs: getBusinessCardDealHistoryDocs()
  },
  {
    model: 'Credit',
    options: {
      logResults: true
    },
    skip: {},
    docs: getCreditDocs()
  },
  {
    model: 'Notification',
    options: {
      logResults: true
    },
    skip: {},
    docs: getNotificationDocs()
  },
  {
    model: 'ActivityHistory',
    options: {
      logResults: true
    },
    skip: {},
    docs: getActivityHistoryDocs()
  }
];
