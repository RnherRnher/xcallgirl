const chalk = require('chalk');
const fs = require('fs');
const _ = require('lodash');
const path = require('path');
const glob = require('glob');
const joi = require('joi');
const constant = require(path.resolve('./commons/utils/constant'));

/**
 * @overview Xác thực tập tin NODE_ENV tồn tại
 *
 */
const validateEnvironmentVariable = () => {
  const environmentFiles = glob.sync(`./config/env/${process.env.NODE_ENV}.js`);
  if (!environmentFiles.length) {
    console.log();
    if (process.env.NODE_ENV) {
      console.error(chalk.red(`+ Error: No configuration file found for "${process.env.NODE_ENV}" environment using development instead`));
    } else {
      console.error(chalk.red('+ Error: NODE_ENV is not defined! Using default development environment'));
    }
    console.log();

    process.env.NODE_ENV = 'development';
  }
};
module.exports.validateEnvironmentVariable = validateEnvironmentVariable;

/**
 * @overview Xác thực cấu trúc file trong thư mục env theo NODE_ENV
 *
 */
const validateConfigFile = (config) => {
  const fileSchema = joi.object().keys({
    /* App */
    xcallgirl: joi.object().required(),
    folders: joi.object().required(),
    files: joi.object().required(),
    app: joi.object().keys({
      title: joi.string().required(),
      description: joi.object().keys({
        vi: joi.string().required(),
      }).required(),
      keywords: joi.string().required(),
      languages: joi.array().items(joi.string()).unique().required(),
      language: joi.string().required(),
      apiVersion: joi.number().required(),
      config: joi.object().keys({
        searchLimit: joi.number().required(),
        loadLimit: joi.number().required(),
        businessCard: joi.object().keys({
          topActivityLimit: joi.number().required(),
          topNewUpLimit: joi.number().required(),
        }).required(),
      }).required()
    }).required(),
    viewEngine: joi.string().required(),
    livereload: joi.boolean().required(),
    favicon: joi.string().required(),
    session: joi.object().keys({
      key: joi.string().required(),
      secret: joi.string().required(),
      collection: joi.string().required(),
      saveUninitialized: joi.boolean().required(),
      resave: joi.boolean().required(),
      cookie: joi.object().keys({
        path: joi.string().required(),
        httpOnly: joi.boolean().required(),
        secure: joi.boolean().required(),
        maxAge: joi.number().required(),
      }).required(),
    }).required(),
    /* Database */
    db: joi.object().keys({
      options: joi.object().keys({
        useNewUrlParser: joi.boolean().required(),
      }).required(),
      promise: joi.func().required(),
    }).required(),
    /* Security */
    helmet: joi.object().keys({
      csp: joi.object().keys({
        directives: joi.object().keys({
          defaultSrc: joi.array().items(joi.string()).unique().required(),
          childSrc: joi.array().items(joi.string()).unique().required(),
          connectSrc: joi.array().items(joi.string()).unique().required(),
          fontSrc: joi.array().items(joi.string()).unique().required(),
          frameSrc: joi.array().items(joi.string()).unique().required(),
          imgSrc: joi.array().items(joi.string()).unique().required(),
          manifestSrc: joi.array().items(joi.string()).unique().required(),
          mediaSrc: joi.array().items(joi.string()).unique().required(),
          objectSrc: joi.array().items(joi.string()).unique().required(),
          scriptSrc: joi.array().items(joi.string()).unique().required(),
          styleSrc: joi.array().items(joi.string()).unique().required(),
          workerSrc: joi.array().items(joi.string()).unique().required(),
          baseUri: joi.array().items(joi.string()).unique().required(),
          sandbox: joi.array().items(joi.string()).unique().required(),
          formAction: joi.array().items(joi.string()).unique().required(),
          frameAncestors: joi.array().items(joi.string()).unique().required(),
          requireSriFor: joi.array().items(joi.string()).unique().required(),
          blockAllMixedContent: joi.boolean().required(),
        }).required(),
      }).required(),
      frameguard: joi.object().keys({
        action: joi.string().required(),
      }).required(),
      referrerPolicy: joi.object().keys({
        policy: joi.string().required(),
      }).required(),
      hsts: joi.object().keys({
        maxAge: joi.number().required(),
        includeSubdomains: joi.boolean().required(),
        preload: joi.boolean().required(),
        setIf: joi.func().required(),
      }).required(),
      hpkp: joi.object().keys({
        maxAge: joi.number().required(),
        sha256s: joi.array().items(joi.string()).unique().required(),
        setIf: joi.func().required(),
      }).required(),
    }).required(),
    log: joi.object().keys({
      disable: joi.boolean().required(),
      validFormats: joi.array().items(joi.string()).unique().required(),
      fileLogger: joi.object().keys({
        directoryPath: joi.string().required(),
        maxsize: joi.number().required(),
        maxFiles: joi.number().required(),
        tailable: joi.boolean().required(),
        times: joi.string().required(),
        eol: joi.string().required(),
        formatString: joi.func().required(),
      }).required(),
      consoleLogger: joi.object().keys({
        colorize: joi.boolean().required(),
        times: joi.string().required(),
        eol: joi.string().required(),
        formatString: joi.func().required(),
      }).required(),
    }).required(),
    /* API */
    cloudinary: joi.object().keys({
      cloudName: joi.string().required(),
      api: joi.object().keys({
        key: joi.string().required(),
        secret: joi.string().required(),
      }).required(),
    }).required(),
    uploads: joi.object().keys({
      profile: joi.object().keys({
        image: joi.object().keys({
          names: joi.array().items(joi.string()).unique().required(),
          mimetypes: joi.array().items(joi.string()).unique().required(),
          size: joi.number().required(),
          format: joi.object().keys({
            type: joi.string().required(),
            width: joi.string().required(),
            height: joi.string().required(),
            getPublicID: joi.func().required(),
            getTags: joi.func().required(),
          }).required(),
        }).required(),
      }).required(),
      businessCard: joi.object().keys({
        image: joi.object().keys({
          names: joi.array().items(joi.string()).unique().required(),
          mimetypes: joi.array().items(joi.string()).unique().required(),
          size: joi.number().required(),
          format: joi.object().keys({
            type: joi.string().required(),
            width: joi.string().required(),
            height: joi.string().required(),
            getPublicID: joi.func().required(),
            getTags: joi.func().required(),
          }).required(),
        }).required(),
      }).required(),
    }).required(),
  }).required();

  let evnfileScheme = {};
  switch (process.env.NODE_ENV) {
    case 'development':
      evnfileScheme = joi.object().keys({
        /* APP */
        domain: joi.string().uri().required(),
        port: joi.number().required(),
        host: joi.string().required(),
        /* Database */
        db: joi.object().keys({
          uri: joi.string().uri().required(),
          debug: joi.boolean().required(),
        }).required(),
        seedDB: joi.object().keys({
          seed: joi.boolean().required(),
          options: joi.object().keys({
            logResults: joi.boolean().required(),
          }).required(),
        }).required(),
        /* Security */
        log: joi.object().keys({
          format: joi.string().required(),
          fileLogger: joi.object().keys({
            fileName: joi.string().required(),
          }).required(),
        }).required(),
        /* API */
        cloudinary: joi.object().keys({
          resource: joi.string().required(),
        }).required(),
        twilio: joi.object().keys({
          accountSid: joi.string().required(),
          authToken: joi.string().required(),
          numberPhone: joi.string().required(),
        }).required(),
      }).required();
      break;
    case 'production':
      evnfileScheme = joi.object().keys({
        /* APP */
        domain: joi.string().uri().required(),
        port: joi.number().required(),
        host: joi.string().required(),
        secure: joi.object().keys({
          ssl: joi.boolean().required(),
          privateKey: joi.string().required(),
          certificate: joi.string().required(),
          caBundle: joi.string().required(),
        }).required(),
        /* Database */
        db: joi.object().keys({
          uri: joi.string().uri().required(),
          debug: joi.boolean().required(),
        }).required(),
        /* Security */
        helmet: joi.object().keys({
          csp: joi.object().keys({
            directives: joi.object().keys({
              upgradeInsecureRequests: joi.boolean().required(),
            }).required(),
          }).required(),
        }).required(),
        /* Security */
        log: joi.object().keys({
          format: joi.string().required(),
          fileLogger: joi.object().keys({
            fileName: joi.string().required(),
          }).required(),
        }).required(),
        /* API */
        cloudinary: joi.object().keys({
          resource: joi.string().required(),
        }).required(),
        twilio: joi.object().keys({
          accountSid: joi.string().required(),
          authToken: joi.string().required(),
          numberPhone: joi.string().required(),
        }).required(),
      }).required();
      break;
    case 'test':
      evnfileScheme = joi.object().keys({
        /* APP */
        domain: joi.string().uri().required(),
        port: joi.number().required(),
        host: joi.string().required(),
        /* Database */
        db: joi.object().keys({
          uri: joi.string().uri().required(),
          debug: joi.boolean().required(),
        }).required(),
        /* Security */
        log: joi.object().keys({
          format: joi.string().required(),
          fileLogger: joi.object().keys({
            fileName: joi.string().required(),
          }).required(),
        }).required(),
        /* API */
        cloudinary: joi.object().keys({
          resource: joi.string().required(),
        }).required(),
      }).required();
      break;
    default:
      break;
  }

  joi.validate(config, fileSchema.concat(evnfileScheme), (err) => {
    if (err) {
      console.log();
      console.error(chalk.red(`+ Error: Config file "config.js" not validate with ${process.env.NODE_ENV} environment`));
      console.error(chalk.red(err));
      console.log();
    }
  });
};
module.exports.validateConfigFile = validateConfigFile;

/**
 * @overview Xác thực Secure = true tham số thực sự có thể được bật
 * vì nó yêu cầu certs và các tập tin quan trọng để có sẵn
 *
 * @param {Object} config
 * @returns {Boolean}
 */
const validateSecureMode = (config) => {
  if (!config.secure || config.secure.ssl !== true) {
    return false;
  }

  const privateKey = fs.existsSync(path.resolve(config.secure.privateKey));
  const certificate = fs.existsSync(path.resolve(config.secure.certificate));

  if (!privateKey || !certificate) {
    console.log();
    console.error(chalk.red('+ Error: Certificate file or key file is missing, falling back to non-SSL mode'));
    console.error(chalk.red('  To create them, simply run the following = require( your shell: sh ./scripts/generate-ssl-certs.sh'));
    console.log();

    config.secure.ssl = false;
    return false;
  }

  return true;
};
module.exports.validateSecureMode = validateSecureMode;

/**
 * @overview Xác thực tham số Session Secret không được đặt thành mặc định trong NODE_ENV = 'production'
 *
 * @param   {Object} config
 * @param   {String} testing
 * @returns  {Boolean}
 */
const validateSessionSecret = (config, testing) => {
  if (process.env.NODE_ENV !== 'production') {
    return true;
  }

  if (config.session.secret === 'xcallgirl') {
    if (!testing) {
      console.log();
      console.log(chalk.red('+ WARNING: It is strongly recommended that you change sessionSecret config while running in production!'));
      console.log(chalk.red('  Please add `sessionSecret: process.env.SESSION_SECRET || \'super amazing secret\'` to '));
      console.log(chalk.red('  `config/env/production.js`'));
      console.log();
    }
    return false;
  } return true;
};
module.exports.validateSessionSecret = validateSessionSecret;

/**
 * @overview Xác thực các tập tin ngôn ngữ cho ứng dụng
 *
 * @param {Object} config
 */
const validateLocales = (config) => {
  if (!_.includes(config.app.languages, config.app.language)) {
    console.log();
    console.error(chalk.red(`+ Error: Set language ${config.app.language} not validated. App only allow languages ${config.app.languages}`));
    console.log();
  }

  config.app.languages.forEach((fileName) => {
    if (!fs.existsSync(`modules/core/client/locales/${fileName}.locale.client.js`)) {
      console.log();
      console.error(chalk.red(`+ Error: Please add file 'modules/core/client/locales/${fileName}.locale.client.js'`));
      console.log();
    }
  });
};
module.exports.validateLocales = validateLocales;

/**
 * @overview Xác thực mô hình người dùng
 *
 * @returns {Object}
 */
const validateModelUser = () => {
  const config = require(path.resolve('config/config'));

  return {
    url: {
      avatar: {
        female: 'https://res.cloudinary.com/xcallgirl/image/upload/v1541529152/prod/default/user/avatar/female.jpg',
        male: 'https://res.cloudinary.com/xcallgirl/image/upload/v1541529152/prod/default/user/avatar/male.jpg',
        undefined: 'https://res.cloudinary.com/xcallgirl/image/upload/v1541529152/prod/default/user/avatar/undefined.jpg',
        admin: 'https://res.cloudinary.com/xcallgirl/image/upload/v1546182130/prod/default/user/avatar/admin.jpg',
      }
    },
    name: {
      nick: {
        admin: 'Admin XCallgirl',
        undefined: 'User XCallgirl'
      }
    },
    expires: {
      resetTokenPassword: 5 * constant.ENUM.TIME.ONE_MINUTES,
      verifyTokenNumberPhone: 5 * constant.ENUM.TIME.ONE_MINUTES,
    },
    authorized: [
      constant.NONE,
      constant.VIEW,
      constant.FULL
    ],
    languages: config.app.languages,
    statusVerfy: [
      constant.NONE,
      constant.WAIT,
      constant.FAILED,
      constant.SUCCESS],
    settings: [constant.GENERAL, constant.SECURITY]
  };
};
module.exports.validateModelUser = validateModelUser;

/**
 * @overview Xác thực mô hình thông báo
 *
 * @returns {Object}
 */
const validateModelNotification = () => {
  return {
    type: [
      constant.USER,
      constant.BUSINESS_CARD,
      constant.BUSINESS_CARD_DEAL_HISTORY,
      constant.CREDIT,
      constant.STORE,
      constant.ADMIN
    ],
    result: [
      constant.FAILED,
      constant.SUCCESS,
      constant.WARNING,
      constant.INFO
    ],
  };
};
module.exports.validateModelNotification = validateModelNotification;

/**
 * @overview Xác thực mô hình lịch sử  hoạt động
 *
 * @returns {Object}
 */
const validateModelActivityHistory = () => {
  return {
    type: [
      constant.USER,
      constant.CHAT,
      constant.BUSINESS_CARD,
      constant.BUSINESS_CARD_DEAL_HISTORY
    ],
    result: [
      constant.FAILED,
      constant.SUCCESS,
      constant.WARNING,
      constant.INFO
    ],
  };
};
module.exports.validateModelActivityHistory = validateModelActivityHistory;

/**
 * @overview Xác thực mô hình lịch sử  giao dịch
 *
 * @returns {Object}
 */
const validateModelBusinessCardDealHistory = () => {
  return {
    result: [
      constant.WAIT,
      constant.FAILED,
      constant.SUCCESS
    ]
  };
};
module.exports.validateModelBusinessCardDealHistory = validateModelBusinessCardDealHistory;

/**
 * @overview Xác thực mô hình trò truyện
 *
 * @returns {Object}
 */
const validateModelChat = () => {
  return {
    type: [constant.PRIVCY],
    dataType: [constant.TEXT],
  };
};
module.exports.validateModelChat = validateModelChat;

/**
 * @overview Xác thực mô hình tín dụng
 *
 * @returns {Object}
 */
const validateModelCredit = () => {
  return {
    historys: {
      type: [
        constant.GET,
        constant.POST,
        constant.PUT
      ],
    }
  };
};
module.exports.validateModelCredit = validateModelCredit;
