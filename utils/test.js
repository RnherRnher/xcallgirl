const path = require('path');
const usersUrl = require(path.resolve('./commons/modules/users/datas/url.data'));

const signin = (agent, numberPhone, password) => {
  return agent.post(usersUrl.AUTH_SIGNIN)
    .set('X-Requested-With', 'XMLHttpRequest')
    .send({
      numberPhone: `${numberPhone.code} ${numberPhone.number}`,
      password,
    });
};
module.exports.signin = signin;
