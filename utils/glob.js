const _ = require('lodash');
const path = require('path');
const glob = require('glob');

/**
 * @overview Lấy tập tin bằng mẫu toàn cục
 *
 * @param    {String|String[]} globPatterns
 * @param    {String|String[]} excludes
 * @returns  {String[]}
 */
const getGlobbedPaths = (globPatterns, excludes) => {
  // Biểu thức url
  const urlRegex = new RegExp('^(?:[a-z]+:)?\/\/', 'i');

  let output = [];

  // Đệ quy tìm kiếm Path nếu Patterns là mảng
  if (_.isArray(globPatterns)) {
    globPatterns.forEach((globPattern) => {
      output = _.union(output, getGlobbedPaths(globPattern, excludes));
    });
  } else if (_.isString(globPatterns)) {
    // Patterns là Path thì lưu
    if (urlRegex.test(globPatterns)) {
      output.push(globPatterns);
    } else {
      // Dùng glob để phân tích Patterns tìm kiếm Path
      let files = glob.sync(globPatterns);

      // Ngoại trừ Path nếu có
      if (excludes) {
        files = files.map((file) => {
          if (_.isArray(excludes)) {
            excludes.forEach((exclude) => {
              if (_.has(excludes, exclude)) {
                file = file.replace(excludes[exclude], '');
              }
            });
          } else {
            file = file.replace(excludes, '');
          }
          return file;
        });
      }

      // Trả KQ
      output = files;
    }
  }

  return output;
};
module.exports.getGlobbedPaths = getGlobbedPaths;

/**
 * @overview Cấu hình lấy thư mục toàn cục
 *
 * @param {[String]} config
 */
const initGlobalConfigFolders = (config) => {
  config.folders = {
    server: {},
    client: {},
  };

  config.folders.client = getGlobbedPaths(path.join(process.cwd(), 'modules/*/client/'), process.cwd().replace(new RegExp(/\\/g), '/'));
};
module.exports.initGlobalConfigFolders = initGlobalConfigFolders;

/**
 * @overview Cấu hình lấy tập tin toàn cục
 *
 * @param {[String]} config
 * @param {[String]} assets
 */
const initGlobalConfigFiles = (config, assets) => {
  config.files = {
    server: {},
    client: {},
  };

  config.files.server.configs = getGlobbedPaths(assets.server.config);
  config.files.server.policies = getGlobbedPaths(assets.server.policies);
  config.files.server.routes = getGlobbedPaths(assets.server.routes);
  config.files.server.models = getGlobbedPaths(assets.server.models);
  config.files.server.sockets = getGlobbedPaths(assets.server.sockets);
  config.files.server.crons = getGlobbedPaths(assets.server.crons);

  config.files.client.js = _.concat(getGlobbedPaths(assets.client.lib.js, 'public/'), getGlobbedPaths(assets.client.js, ['public/']));
  config.files.client.css = _.concat(getGlobbedPaths(assets.client.lib.css, 'public/'), getGlobbedPaths(assets.client.css, ['public/']));
};
module.exports.initGlobalConfigFiles = initGlobalConfigFiles;
