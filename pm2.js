const pm2 = require('pm2');
const chalk = require('chalk');

const options = {
  name: 'XCallgirl',
  script: './server.js',
  env: {
    NODE_ENV: 'development',
  },
  env_production: {
    NODE_ENV: 'production',
  },
  watch: true,
  instances: 'max',
  exec_mode: 'cluster',
  max_memory_restart: '2G',
  source_map_support: true,
  log_date_format: 'DD-MM-YYYY HH:mm:ss',
  ignore_watch: ['node_modules', 'logs/*.log']
};

pm2.connect(() => {
  pm2.start(options, (err) => {
    if (err) {
      console.log();
      console.error(chalk.bold.red(`+ Error: PM2 disconnect ${options.name} with ${options.instances} instances at ${options.max_memory_restart} max memory`));
      console.error(chalk.bold.red(err));
      console.log();

      pm2.disconnect();
    } else {
      console.log();
      console.error(chalk.green(`PM2 connect ${options.name} with ${options.instances} instances at ${options.max_memory_restart} max memory`));
      console.log();
    }
  });
});
