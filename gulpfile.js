const glob = require('glob');
const path = require('path');
const _ = require('lodash');
const chalk = require('chalk');
const gulp = require('gulp-help')(require('gulp'));
const del = require('del');
const webpack = require('webpack');
const webpackStream = require('webpack-stream');
const runSequence = require('run-sequence');
const uglifyes = require('uglify-es');
const uglifyComposer = require('gulp-uglify/composer');
const uglify = uglifyComposer(uglifyes, console);
const gulpLoadPlugins = require('gulp-load-plugins')({
  rename: {
    'gulp-sass-lint': 'sasslint',
  },
});
const globUtil = require('./utils/glob');
const defaultAssets = require('./config/assets/default');
const testAssets = require('./config/assets/test');

let changedTestFiles = [];

/*
 * Utils
 *
 */
const getClientLibFileCurrent = (type) => {
  return globUtil.getGlobbedPaths(`public/lib/*.${type}?(.map)`);
};

/*
 * Environments
 *
 */
gulp.task('env:test', 'Set NODE_ENV=test', () => {
  process.env.NODE_ENV = 'test';
});

gulp.task('env:dev', 'Set NODE_ENV=development', () => {
  process.env.NODE_ENV = 'development';
});

gulp.task('env:prod', 'Set NODE_ENV=production', () => {
  process.env.NODE_ENV = 'production';
});

/*
 * Lints
 *
 */
gulp.task('eslint', 'ESLint JS and JSX linting task', (done) => {
  const assets = _.union(
    defaultAssets.server.gulpConfig,
    defaultAssets.server.allJS,
    defaultAssets.client.js,
    defaultAssets.client.components,
    testAssets.tests.server,
    testAssets.tests.client,
    testAssets.tests.e2e,
  );

  return gulp.src(assets)
    .pipe(gulpLoadPlugins.eslint({
      useEslintrc: true,
    }))
    .pipe(gulpLoadPlugins.eslint.format())
    .pipe(gulpLoadPlugins.eslint.results((results) => {
      console.log();
      console.log(chalk.bold.green(`Total Results: ${results.length}`));
      console.log(chalk.bold.yellow(`Total Warnings: ${results.warningCount}`));
      console.log(chalk.bold.red(`Total Errors: ${results.errorCount}`));
      console.log();
    }));
});

gulp.task('sasslint', 'SASS linting task', (done) => {
  const assets = defaultAssets.client.sass;

  return gulp.src(assets)
    .pipe(gulpLoadPlugins.sasslint({
      configFile: '.sasslintrc.yml',
    }))
    .pipe(gulpLoadPlugins.sasslint.format());
});

gulp.task('lint', 'Lint CSS and JavaScript files task', (done) => {
  runSequence(['eslint', 'sasslint'], done);
});

/*
 * Builds and Minifys
 *
 */
gulp.task('fix:uglify', 'Fix uglify files task', (done) => {
  del(getClientLibFileCurrent('js'))
    .then(() => {
      done();
    })
    .catch((err) => {
      console.log();
      console.error(chalk.bold.red('+ Error: fix:uglify task'));
      console.error(chalk.bold.red(err));
      console.log();
    });
});

gulp.task('uglify', 'JS build and minifying task', (done) => {
  const __DEV__ = process.env.NODE_ENV !== 'production';
  const assets = _.union(
    defaultAssets.client.js,
    defaultAssets.client.components,
  );

  return gulp.src(assets)
    .pipe(webpackStream(
      require('./webpack.config.js'),
      webpack,
      (err, results) => {
        if (err) {
          console.log();
          console.log(chalk.bold.red('+ Error: Webpack stream'));
          console.log(chalk.bold.red(err));
          console.log();
        }
      },
    ))
    .pipe(gulpLoadPlugins.sourcemaps.init({
      loadMaps: true,
      largeFile: true,
    }))
    .pipe(gulpLoadPlugins.if(
      !__DEV__,
      uglify().on('error', (err) => {
        console.log();
        console.error(chalk.bold.red('+ Error: Uglify'));
        console.error(chalk.bold.red(err));
        console.log();
      }),
    ))
    .pipe(gulpLoadPlugins.concat('app.js'))
    .pipe(gulpLoadPlugins.rev())
    .pipe(gulpLoadPlugins.sourcemaps.write('./', {
      addComment: true,
      includeContent: true,
    }))
    .pipe(gulp.dest('public/lib'));
});

gulp.task('fix:minify', 'Fix minify files task', (done) => {
  del(getClientLibFileCurrent('css'))
    .then(() => {
      done();
    })
    .catch((err) => {
      console.log();
      console.error(chalk.bold.red('+ Error: fix:minify task'));
      console.error(chalk.bold.red(err));
      console.log();
    });
});

gulp.task('cssmin', 'Conver Sass to CSS and minifying task', (done) => {
  const __DEV__ = process.env.NODE_ENV !== 'production';
  const assets = defaultAssets.client.sass;

  return gulp.src(assets)
    .pipe(gulpLoadPlugins.sass().on('error', gulpLoadPlugins.sass.logError))
    .pipe(gulpLoadPlugins.postcss([gulpLoadPlugins.autoprefixer]))
    .pipe(gulpLoadPlugins.sourcemaps.init({
      loadMaps: true,
      largeFile: true,
    }))
    .pipe(gulpLoadPlugins.if(
      !__DEV__,
      gulpLoadPlugins.csso()
    ))
    .pipe(gulpLoadPlugins.concat('app.css'))
    .pipe(gulpLoadPlugins.rev())
    .pipe(gulpLoadPlugins.sourcemaps.write('./', {
      addComment: true,
      includeContent: true,
    }))
    .pipe(gulp.dest('public/lib'));
});

gulp.task('imagemin', 'Images minifying task', (done) => {
  const assets = defaultAssets.client.images;

  return gulp.src(assets)
    .pipe(gulpLoadPlugins.imagemin())
    .pipe(gulp.dest('public/images'));
});

/*
 * Injects
 *
 */
gulp.task('inject', 'Inject JS and CSS task', (done) => {
  const assets = _.union(
    defaultAssets.client.lib.js,
    defaultAssets.client.lib.css,
  );

  return gulp.src(defaultAssets.server.template)
    .pipe(gulpLoadPlugins.inject(
      gulp.src(assets),
      {
        transform: (filePath) => {
          let link = null;
          if (_.includes(globUtil.getGlobbedPaths(defaultAssets.client.lib.js), filePath.replace('/', ''))) {
            link = `<script type='text/javascript' src='${filePath.replace('/public', '')}'></script>`;
          } else if (_.includes(globUtil.getGlobbedPaths(defaultAssets.client.lib.css), filePath.replace('/', ''))) {
            link = `<link rel='stylesheet' type='text/css' href='${filePath.replace('/public', '')}'/>`;
          }

          return link;
        },
      },
    ))
    .pipe(gulp.dest('modules/core/server/views/'));
});

/*
 * Databases
 *
 */
gulp.task('mongo-seed', 'Seed Mongo database based on configuration task', (done) => {
  const mongooseService = require('./config/lib/mongoose');
  const seed = require('./config/lib/mongo-seed');

  // Mở kết nối cơ sở dữ liệu mongoose
  mongooseService.connect(() => {
    mongooseService.loadModels();

    seed.start({
      options: {
        logResults: true,
      },
    }).then(() => {
      // Ngắt kết nối và hoàn thành tác vụ
      mongooseService.disconnect(done);
    }).catch((err) => {
      mongooseService.disconnect((disconnectError) => {
        if (disconnectError) {
          console.log();
          console.error(chalk.bold.red('+ Error: Disconnecting from the database, but was preceded by a Mongo Seed error'));
          console.error(chalk.bold.red(disconnectError));
          console.log();
        }

        // Hoàn thành tác vụ có lỗi
        done(err);
      });
    });
  });
});

gulp.task('dropdb', 'Drops the MongoDB database task', (done) => {
  // Sử dụng cấu hình mongoose
  const mongooseService = require('./config/lib/mongoose');

  mongooseService.connect((db) => {
    db.dropDatabase((err) => {
      if (err) {
        console.log();
        console.error(chalk.bold.red('+ Error: dropped db'));
        console.error(chalk.bold.red(err));
        console.log();
      } else {
        console.log(chalk.bold.green(`Successfully dropped db: ${db.databaseName}`));
      }

      mongooseService.disconnect(done);
    });
  });
});

/*
 * Systems
 *
 */
gulp.task('nodemon', 'Nodemon task', (done) => {
  return gulpLoadPlugins.nodemon({
    script: 'server.js',
    verbose: true,
    watch: defaultAssets.server.allJS,
    ext: 'js',
    nodeArgs: ['--inspect'],
  });
});

gulp.task('nodemon-nodebug', 'Nodemon task without verbosity or debugging tack', (done) => {
  return gulpLoadPlugins.nodemon({
    script: 'server.js',
    watch: defaultAssets.server.allJS,
    ext: 'js',
  });
});

gulp.task('watch', 'Watch files for changes tack', () => {
  // Bắt đầu tải trước
  gulpLoadPlugins.refresh.listen();

  // Server
  gulp.watch(defaultAssets.server.gulpConfig, ['eslint']);
  gulp.watch(defaultAssets.server.allJS, ['eslint']).on('change', gulpLoadPlugins.refresh.changed);
  gulp.watch(defaultAssets.server.template).on('change', gulpLoadPlugins.refresh.changed);

  const jsClient = _.union(
    defaultAssets.client.js,
    defaultAssets.client.components,
  );
  gulp.watch(jsClient, () => {
    runSequence('eslint', 'fix:uglify', 'uglify', 'inject');
  }).on('change', gulpLoadPlugins.refresh.changed);

  gulp.watch(defaultAssets.client.sass, () => {
    runSequence('sasslint', 'fix:minify', 'cssmin', 'inject');
  }).on('change', gulpLoadPlugins.refresh.changed);
});

/*
 * Tests
 *
 */
gulp.task('mocha', 'Mocha test task', (done) => {
  const mongooseService = require('./config/lib/mongoose');
  const testSuites = changedTestFiles.length ? changedTestFiles : testAssets.tests.server;
  let error = null;

  mongooseService.connect((db) => {
    mongooseService.loadModels();

    gulp.src(testSuites)
      .pipe(gulpLoadPlugins.mocha({
        reporter: 'spec',
        timeout: 10000,
      }))
      .on('error', (err) => {
        error = err;
      })
      .on('end', () => {
        if (error) {
          console.log();
          console.error(chalk.bold.red('+ Error: Mocha test'));
          console.error(chalk.bold.red(error));
          console.log();
        } else {
          console.log(chalk.bold.green('Successfully mocha test'));
        }

        mongooseService.disconnect((err) => {
          if (err) {
            console.log();
            console.error(chalk.bold.red('+ Error: Disconnecting from database'));
            console.error(chalk.bold.red(err));
            console.log();
          }

          done(error);
        });
      });
  });
});

gulp.task('pre-test', 'Prepare istanbul coverage test task', () => {
  return gulp.src(defaultAssets.server.allJS)
    .pipe(gulpLoadPlugins.istanbul())
    .pipe(gulpLoadPlugins.istanbul.hookRequire());
});

gulp.task('mocha:coverage', 'Run istanbul test and write report task', ['pre-test', 'mocha'], (done) => {
  del.sync(['coverages/server']);

  const testSuites = changedTestFiles.length ? changedTestFiles : testAssets.tests.server;

  return gulp.src(testSuites)
    .pipe(gulpLoadPlugins.istanbul.writeReports({
      reportOpts: {
        dir: './coverages/server',
      },
    }));
});

gulp.task('watch:server:run-tests', 'Watch files for changes tack for test', (done) => {
  gulpLoadPlugins.refresh.listen();

  gulp.watch([testAssets.tests.server, defaultAssets.server.allJS], ['test:server']).on('change', (file) => {
    changedTestFiles = [];
    _.forEach(testAssets.tests.server, (pattern) => {
      _.forEach(glob.sync(pattern), (f) => {
        const filePath = path.resolve(f);
        if (filePath === path.resolve(file.path)) {
          changedTestFiles.push(f);
          gulpLoadPlugins.refresh.changed(f);
        }
      });
    });
  });
});

/*
 * Runs
 *
 */
// Build
gulp.task('build', 'Lint project files and minify them into two production files task', (done) => {
  del(['public/lib'])
    .then(() => {
      runSequence('lint', ['uglify', 'cssmin', 'imagemin'], 'inject', done);
    })
    .catch((err) => {
      console.log();
      console.error(chalk.bold.red('+ Error: build task'));
      console.error(chalk.bold.red(err));
      console.log();
    });
});

// Clean build
gulp.task('clean:build', 'Clean build folders or files task', (done) => {
  del([
    'public/lib',
    'public/images',
    'coverages',
    'logs/*.log',
  ])
    .then(() => {
      done();
    })
    .catch((err) => {
      console.log();
      console.error(chalk.bold.red('+ Error: clean:build task'));
      console.error(chalk.bold.red(err));
      console.log();
    });
});

// Test
gulp.task('test', 'Run the project tests task', (done) => {
  runSequence('env:test', 'lint', 'dropdb', 'mocha', /* TODO: 'karma', */ 'nodemon', /* TODO: 'protractor', */ done);
});

gulp.task('test:server', 'Run test server task', (done) => {
  runSequence('env:test', 'lint', 'dropdb', 'mocha', done);
});

gulp.task('test:server:watch', 'Watch all server files for changes & run server tests (test:server) task on changes', (done) => {
  runSequence('test:server', 'watch:server:run-tests', done);
});

gulp.task('test:client', 'Run test client task', (done) => {
  runSequence('env:test', 'lint', 'dropdb', /* TODO: 'karma', */ done);
});

gulp.task('test:e2e', 'Run test e2e task', (done) => {
  runSequence('env:test', 'lint', 'dropdb', 'nodemon', /* TODO: 'protractor', */ done);
});

gulp.task('test:coverage', 'Run test coverage task', (done) => {
  runSequence('env:test', 'lint', 'dropdb', 'mocha:coverage', /* TODO: 'karma:coverage', */ done);
});

// Start
gulp.task('default', 'Run the project in development mode with node debugger enabled task', (done) => {
  runSequence('env:dev', 'build', ['nodemon', 'watch'], done);
});

gulp.task('prod', 'Run the project in production mode task', (done) => {
  runSequence('env:prod', 'build', ['nodemon-nodebug', 'watch'], done);
});

// Seed
gulp.task('seed', 'Run Mongo Seed with default environment config tack', (done) => {
  runSequence('env:dev', 'mongo-seed', done);
});

gulp.task('seed:prod', 'Run Mongo Seed with production environment config tack', (done) => {
  runSequence('env:prod', 'mongo-seed', done);
});

gulp.task('seed:test', 'Run Mongo Seed with test environment config tack', (done) => {
  runSequence('env:test', 'mongo-seed', done);
});
