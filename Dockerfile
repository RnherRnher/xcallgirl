# Build: sudo docker build -t dev/xcallgirl .
#
# Run: sudo docker run -it dev/xcallgirl

FROM ubuntu:latest

# 80 = HTTP, 443 = HTTPS, 3000 = XCallgirl server
EXPOSE 80 443 3000

# Set development environment as default
ENV NODE_ENV development

# Install Utilities
RUN apt-get update -q  \
 && apt-get install -y \
 sudo \
 apt-utils \
 curl \
 git \
 ssh \
 gnupg2 \
 && sudo apt-get clean \
 && sudo rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Install nodejs and yarn
RUN curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash - \
 && curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add - \
 && echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
RUN sudo apt-get update -q \
 && sudo apt-get install -yqq \
 nodejs \
 yarn \
 mongodb \
 && sudo apt-get clean \
 && sudo rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Create app directory
RUN sudo mkdir -p /var/www/xcallgirl.com
WORKDIR /var/www/xcallgirl.com

# Install app dependencies
COPY package.json /var/www/xcallgirl.com/package.json
RUN sudo yarn global add \
  gulp \
  pm2 \
  && yarn install \
  && yarn cache clean

# Bundle app source
COPY . /var/www/xcallgirl.com

# Run mongodb and nginx
RUN sudo service mongodb start

# Run xcallgirl server
CMD yarn pm2
