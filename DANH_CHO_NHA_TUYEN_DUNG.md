LƯU Ý:
- Nhầm mục đích giúp cho nhà tuyển dụng tìm hiểu kỹ hơn về dự án của ứng viên
- Dự án dành chỉ dành cho học tập và nghiên cứu do tự ứng viên lặp trình
- Dự án được lấy ý tưởng từ chức năng gọi xe, và từ nhiều nguồn 'free and open-source JavaScript software stack for building dynamic web sites and web applications'
- Ứng dụng vẫn chưa hoàn hành hoàn chỉnh nên còn nhiều thiếu sót cả về  product, delevop và guide.

RUNNING YOUR APPLICATION:
- Có thể chạy bằng Docker - pm2 - trực tiếp
- Nhớ seed để có dữ liệu test

Chạy trực tiếp
1. yarn seed
2. yarn start

Đăng nhập
- Số điện thoại: '0' (user), '1' (provider), '2' (admin), 3 ('system')
- Mật khẩu: 'password'
