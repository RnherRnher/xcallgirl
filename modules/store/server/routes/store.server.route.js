const path = require('path');
const policy = require('../policies/store.server.policy');
const stores = require('../controllers/stores.server.controller');
const validate = require(path.resolve('./commons/modules/store/validates/stores.validate'));
const middleware = require(path.resolve('./utils/middleware'));
const url = require(path.resolve('./commons/modules/store/datas/url.data'));

const init = (app) => {
  app.route(url.STORE_PAY)
    .all(policy.isAllowed)
    .post(middleware.paramsValidation(validate, 'pay'), stores.pay);
};
module.exports = init;
