const path = require('path');
const mongoose = require('mongoose');
const async = require('async');
const constant = require(path.resolve('./commons/utils/constant'));

const Store = mongoose.model('Store');
const Credit = mongoose.model('Credit');
const BusinessCard = mongoose.model('BusinessCard');

/**
 * @overview Thanh toán
 *
 * @param {Request} req
 * @param {Response} res
 * @param {Function} next
 *
 * @input body    {
 *                  item: {
 *                    groupID: String,
 *                    itemID: String,
 *                    cost: Number,
 *                  }
 *                }
 * @output        {
 *                  code: String,
 *                  credit?: Credit
 *                }
 */
const pay = (req, res, next) => {
  const bodyReq = req.body;
  const userSelf = req.user;
  let dataRes = null;

  async.waterfall([
    (done) => {
      /* Kiểm tra đầu vào */

      if (userSelf) {
        Credit
          .findOne({ userID: userSelf._id })
          .exec((err, credit) => {
            if (err || !credit) {
              // Tín dụng không tồn tại
              dataRes = {
                status: 404,
                bodyRes: {
                  code: 'credit.notExist',
                },
              };

              done(err || new Error('Credit not exist'), dataRes);
            } else {
              done(null, credit);
            }
          });
      } else {
        // Chưa đăng nhập người dùng
        dataRes = {
          status: 401,
          bodyRes: {
            code: 'user.signin.notExist',
          },
        };

        done(new Error('User not signin'), dataRes);
      }
    }, (credit, done) => {
      /* Kiểm tra sản phẩm trong của hàng */

      if (Store.checkItem(bodyReq.item)) {
        done(null, credit);
      } else {
        // Thanh toán không thành công
        dataRes = {
          status: 400,
          bodyRes: {
            code: 'store.pay.failed',
          },
        };

        done(new Error('Store failed check item'), dataRes);
      }
    }, (credit, done) => {
      /* Thanh toán */

      Store.pay(credit, bodyReq.item)
        .then((store) => {
          async.parallel({
            creditSave: (callback) => {
              credit.save((err) => {
                callback(err, credit);
              });
            },
            storeSave: (callback) => {
              store.save((err) => {
                callback(err, store);
              });
            }
          }, (err, results) => {
            if (err) {
              // Thanh toán không thành công
              dataRes = {
                status: 400,
                bodyRes: {
                  code: 'store.pay.failed',
                },
              };

              done(err, dataRes);
            } else {
              done(null, results.creditSave);
            }
          });
        })
        .catch(() => {
          // Thanh toán không thành công
          dataRes = {
            status: 400,
            bodyRes: {
              code: 'store.pay.failed',
            },
          };

          done(new Error('Store failed pay'), dataRes);
        });
    }, (credit, done) => {
      /* Cập nhật */

      switch (bodyReq.item.groupID) {
        case constant.BUSINESS_CARD:
          BusinessCard
            .findOne({ userID: userSelf._id })
            .exec((err, businessCard) => {
              if (err || !businessCard) {
                // Danh thiếp không tồn tại
                dataRes = {
                  status: 404,
                  bodyRes: {
                    code: 'businessCard.notExist',
                  },
                };

                done(err || new Error('Not business card exists'), dataRes);
              } else {
                done(null, credit, businessCard);
              }
            });
          break;
        default:
          // Thanh toán không thành công
          dataRes = {
            status: 400,
            bodyRes: {
              code: 'store.pay.failed',
            },
          };

          done(new Error('Store failed pay'), dataRes);
          break;
      }
    }, (credit, model, done) => {
      /* Cập nhật người dùng */

      const item = Store.getItem(bodyReq.item);
      if (model.addItem(bodyReq.item, item.value)) {
        model.save((err) => {
          if (err) {
            // Thanh toán không thành công
            dataRes = {
              status: 400,
              bodyRes: {
                code: 'store.pay.failed',
              },
            };
          } else {
            dataRes = {
              status: 200,
              bodyRes: {
                code: 'store.pay.success',
                credit: credit.getFullInfo(),
              },
            };
          }

          done(err, dataRes);
        });
      } else {
        // Thanh toán không thành công
        dataRes = {
          status: 400,
          bodyRes: {
            code: 'store.pay.failed',
          },
        };

        done(new Error('Store failed add item'), dataRes);
      }
    }], (err, dataRes) => {
      return res
        .status(dataRes.status)
        .send(dataRes.bodyRes)
        .end();
    });
};
module.exports.pay = pay;
