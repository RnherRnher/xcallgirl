const path = require('path');
const mongoose = require('mongoose');
const _ = require('lodash');
const chalk = require('chalk');
const constant = require(path.resolve('./commons/utils/constant'));
const regular = require(path.resolve('./commons/utils/regular-expression'));

const { Schema } = mongoose;

const StoreSchema = new Schema({
  historys: {
    type: [{
      userID: {
        type: mongoose.SchemaTypes.ObjectId,
        required: true,
      },
      groupID: {
        type: String,
        required: true,
      },
      itemID: {
        type: String,
        required: true,
      },
      cost: {
        type: Number,
        min: 0,
        required: true
      },
      initDate: {
        type: Date,
        required: true
      },
    }]
  },
  updateDate: {
    type: Date,
    required: true,
    default: Date.now
  },
});

/**
 * @overview Kiểm tra sản phẩm trong của hàng
 *
 * @param   {Object} item
 * @returns {Boolean}
 */
StoreSchema.statics.checkItem = function (item) {
  return _.some(
    regular.store.groups[item.groupID],
    (value) => {
      if (value.itemID === item.itemID) {
        return _.some(
          value.values,
          (_value) => {
            return _value.cost === item.cost;
          }
        );
      }

      return false;
    }
  );
};

/**
 * @overview Lấy sản phẩm trong của hàng
 *
 * @param   {Object} item
 * @returns {Object}
 */
StoreSchema.statics.getItem = function (item) {
  const itemIndex = _.findIndex(
    regular.store.groups[item.groupID],
    (value) => {
      return value.itemID === item.itemID;
    }
  );

  if (itemIndex === -1) {
    return null;
  }

  const valueIndex = _.findIndex(
    regular.store.groups[item.groupID][itemIndex].values,
    (value) => {
      return value.cost === item.cost;
    }
  );

  if (valueIndex === -1) {
    return null;
  }

  return regular
    .store
    .groups[item.groupID][itemIndex]
    .values[valueIndex];
};


/**
 * @overview Thanh toán
 *
 * @param   {Credit} credit
 * @param   {Object} item
 * @returns {Promise}
 */
StoreSchema.statics.pay = function (credit, item) {
  return new Promise((resolve, reject) => {
    const Store = mongoose.model('Store');

    Store
      .findOne({})
      .exec((err, store) => {
        if (
          store
          && !err
          && item
          && credit
          && credit.balance >= item.cost
        ) {
          const currentDate = new Date();

          store.updateDate = currentDate;
          store.historys.push({
            userID: credit.userID,
            groupID: item.groupID,
            itemID: item.itemID,
            cost: item.cost,
            initDate: currentDate
          });

          credit.balance -= item.cost;
          credit.updateDate = currentDate;
          credit.historys.push({
            userID: store._id,
            type: constant.GET,
            balance: item.cost,
            initDate: currentDate
          });

          resolve(store);
        } else {
          reject();
        }
      });
  });
};

/**
 * @overview Tạo mẫu cơ sở dữ liệu
 *
 * @param {Object} doc
 * @param {overwrite: Boolean} options
 * @returns {Promise}
 */
StoreSchema.statics.seed = function (doc, options) {
  const Store = mongoose.model('Store');

  return new Promise((resolve, reject) => {
    function skipDocument() {
      return new Promise((resolve, reject) => {
        Store.findById(doc._id)
          .exec((err, existing) => {
            if (err) {
              return reject(err);
            }

            if (!existing) {
              return resolve(false);
            }

            if (existing && !options.overwrite) {
              return resolve(true);
            }

            // Remove Credit (overwrite)

            existing.remove((err) => {
              if (err) {
                return reject(err);
              }

              return resolve(false);
            });
          });
      });
    }

    function add(skip) {
      return new Promise((resolve, reject) => {
        if (skip) {
          return resolve({
            message: chalk.yellow('Database Seeding: Store\t\t skipped'),
          });
        }

        const store = new Store(doc);
        store.save((err) => {
          if (err) {
            return reject(err);
          }

          return resolve({
            message: 'Database Seeding: Store\t\t added',
          });
        });
      });
    }

    skipDocument()
      .then(add)
      .then((response) => {
        return resolve(response);
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

mongoose.model('Store', StoreSchema);
