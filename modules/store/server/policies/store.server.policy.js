
const path = require('path');
const Acl = require('acl');
const url = require(path.resolve('./commons/modules/store/datas/url.data'));
const constant = require(path.resolve('./commons/utils/constant'));

const MemoryBackend = Acl.memoryBackend;
const acl = new Acl(new MemoryBackend());

const invokeRolesPolicies = () => {
  acl.allow([{
    roles: [
      constant.USER,
      constant.PROVIDER,
      constant.ADMIN
    ],
    allows: [{
      resources: url.STORE_PAY,
      permissions: ['post'],
    }],
  }]);
};
module.exports.invokeRolesPolicies = invokeRolesPolicies;

const isAllowed = (req, res, next) => {
  const roles = req.user ? req.user.roles.value : req.app.locals.defaultRoles;
  acl.areAnyRolesAllowed(roles, req.route.path, req.method.toLowerCase(), (err, isAllowed) => {
    let dataRes = null;

    if (!isAllowed) {
      dataRes = {
        status: 401,
        bodyRes: {
          code: 'user.permission.notExist',
        },
      };
    } else {
      return next(err);
    }

    return res
      .status(dataRes.status)
      .send(dataRes.bodyRes)
      .end();
  });
};
module.exports.isAllowed = isAllowed;
