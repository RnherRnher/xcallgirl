const { action } = require('../../../core/client/utils/middleware');
const constant = require('../constants/store.client.constant');

/**
 * @override Yêu cầu thanh toán theo id
 *
 * @param   {Object} item
 * @returns {Object}
 */
const payStoreRequest = (item) => {
  return action(constant.PAY_STORE_REQUEST, { item });
};
module.exports.payStoreRequest = payStoreRequest;

/**
 * @override Thanh toán theo thành công
 *
 * @param   {Object}
 * @returns {Object}
 */
const payStoreSuccess = ({ code, credit }) => {
  return action(constant.PAY_STORE_SUCCESS, { code, credit });
};
module.exports.payStoreSuccess = payStoreSuccess;

/**
 * @override Thanh toán theo thất bại
 *
 * @param   {Object}
 * @returns {Object}
 */
const payStoreFailed = ({ code }) => {
  return action(constant.PAY_STORE_FAILED, { code });
};
module.exports.payStoreFailed = payStoreFailed;

/**
 * @override Thiết lặp lại cửa hàng
 *
 * @param   {Object}
 */
const resetStoreStore = () => {
  return action(constant.RESERT_STORE_STORE);
};
module.exports.resetStoreStore = resetStoreStore;
