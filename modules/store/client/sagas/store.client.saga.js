const nprogress = require('nprogress');
const api = require('../services/store.service');
const action = require('../actions/store.client.action');
const constant = require('../constants/store.client.constant');
const constantCommon = require('../../../../commons/utils/constant');
const {
  takeLatest,
  put,
  call,
} = require('redux-saga/effects');
const { pushAlertSnackbar } = require('../../../core/client/actions/core.client.action');
const { csrfRequest } = require('../../../core/client/services/core.client.service');
const { updateProfile } = require('../../../users/client/actions/user.client.action');
const { initApp } = require('../../../core/client/actions/app.client.action');

const watchPayStore = function* () {
  yield takeLatest(constant.PAY_STORE_REQUEST, function* ({ item }) {
    try {
      nprogress.start();

      const responseCSRF = yield call(csrfRequest);
      const response = yield call(
        api.payStore,
        {
          csrfToken: responseCSRF.body.csrfToken,
          item
        }
      );

      yield put(action.payStoreSuccess(response.body));
      yield put(pushAlertSnackbar({
        variant: constantCommon.SUCCESS,
        code: response.body.code
      }));

      yield put(updateProfile(constantCommon.CREDIT, response.body.credit));
    } catch (error) {
      if (error && error.body) {
        yield put(action.payStoreFailed(error.body));
        yield put(pushAlertSnackbar({
          variant: constantCommon.ERROR,
          code: error.body.code
        }));
      } else {
        yield put(initApp());
      }
    } finally {
      nprogress.done();
    }
  });
};
module.exports.watchPayStore = watchPayStore;
