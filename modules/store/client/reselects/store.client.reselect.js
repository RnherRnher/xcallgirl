const { createSelector } = require('reselect');
const { makeSelect } = require('../../../core/client/utils/middleware');

const selectStore = (state, props) => {
  return state.get('store');
};

const makeSelectActions = (type) => {
  return createSelector(
    selectStore,
    (storeState) => {
      return makeSelect(storeState.getIn(['actions', type]));
    }
  );
};
module.exports.makeSelectActions = makeSelectActions;
