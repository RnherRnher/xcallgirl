const React = require('react');
const propTypes = require('prop-types');
const classNames = require('classnames');
const constant = require('../../../../../commons/utils/constant');
const regular = require('../../../../../commons/utils/regular-expression');
const { FormattedMessage, FormattedNumber } = require('react-intl');
const {
  AddShoppingCart,
  PlusOne,
  ExpandMore
} = require('@material-ui/icons');
const {
  List,
  ListItem,
  ListItemAvatar,
  Avatar,
  ListItemText,
  ListItemSecondaryAction,
  IconButton,
  ExpansionPanel,
  ExpansionPanelSummary,
  ExpansionPanelDetails
} = require('@material-ui/core');

const BusinessCardStore = class BusinessCardStore extends React.Component {
  constructor(props) {
    super(props);

    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(groupID, itemID, cost) {
    return (event) => {
      const { handleClickParent } = this.props;

      handleClickParent({
        groupID,
        itemID,
        cost
      });
    };
  }

  ExpansionPanel(icon, label, description, items) {
    return (
      <ExpansionPanel key={label}>
        <ExpansionPanelSummary expandIcon={<ExpandMore />}>
          <ListItem>
            <ListItemAvatar>
              <Avatar>
                {icon}
              </Avatar>
            </ListItemAvatar>
            <ListItemText
              inset
              primary={label}
              secondary={description}
            />
          </ListItem>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails className="xcg-info-group">
          <List dense>
            {items}
          </List>
        </ExpansionPanelDetails>
      </ExpansionPanel>
    );
  }

  Item(icon, moneys, description, itemID, cost) {
    const { disabled } = this.props;

    return (
      <ListItem key={description}>
        <ListItemAvatar>
          <Avatar>
            {icon}
          </Avatar>
        </ListItemAvatar>
        <ListItemText
          primary={
            <span>
              <FormattedNumber value={moneys} /> <FormattedMessage id="info.moneys.is" />
            </span>
          }
          secondary={<FormattedMessage id={description} />}
        />
        <ListItemSecondaryAction>
          <IconButton
            color="secondary"
            disabled={disabled}
            onClick={
              this.handleClick(
                constant.BUSINESS_CARD,
                itemID,
                cost
              )
            }
          >
            <AddShoppingCart />
          </IconButton>
        </ListItemSecondaryAction>
      </ListItem>
    );
  }

  initElement() {
    const { className, value } = this.props;

    const Items = value.map((item) => {
      let Icon = null;
      switch (item.itemID) {
        case '0':
          Icon = <PlusOne />;
          break;
        default:
          break;
      }

      return this.ExpansionPanel(
        Icon,
        <FormattedMessage id={item.name} />,
        null,
        item.values.map((value, index) => {
          return this.Item(
            value.value,
            value.cost,
            value.descriptions,
            item.itemID,
            value.cost
          );
        })
      );
    });

    return (
      <div className={classNames('xcg-business-card-store', className)}>
        {Items}
      </div>
    );
  }

  render() {
    return this.initElement();
  }
};

BusinessCardStore.propTypes = {
  className: propTypes.string,
  value: propTypes.array,
  disabled: propTypes.bool,
  handleClickParent: propTypes.func.isRequired,
};

BusinessCardStore.defaultProps = {
  className: '',
  value: regular.store.groups[constant.BUSINESS_CARD],
  disabled: false
};

module.exports = BusinessCardStore;
