const React = require('react');
const propTypes = require('prop-types');
const classNames = require('classnames');
const joi = require('joi');
const constant = require('../../../../../commons/utils/constant');
const BusinessCardStore = require('./BusinessCardStore');
const { createStructuredSelector } = require('reselect');
const { connect } = require('react-redux');
const { FormattedMessage } = require('react-intl');
const {
  ContactMail,
} = require('@material-ui/icons');
const {
  AppBar,
  Tabs,
  Tab
} = require('@material-ui/core');
const { pay } = require('../../../../../commons/modules/store/validates/stores.validate');
const { makeSelectActions } = require('../../reselects/store.client.reselect');
const { payStoreRequest } = require('../../actions/store.client.action');
const { ResultEnum } = require('../../../../core/client/constants/core.client.constant');

const Store = class Store extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      index: 0
    };

    this.handleCheck = this.handleCheck.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.getContentTab = this.getContentTab.bind(this);
  }

  getContentTab() {
    const { index } = this.state;
    const { pay } = this.props;

    const disabled = pay.result === ResultEnum.wait;

    switch (index) {
      case 0:
        return <BusinessCardStore
          disabled={disabled}
          handleClickParent={this.handleClick(constant.BUSINESS_CARD)}
        />;
      default:
        return null;
    }
  }

  handleCheck(body, validate) {
    return joi.validate(body, validate);
  }

  handleClick(type) {
    return (event) => {
      const { validate, payStoreRequest } = this.props;

      this
        .handleCheck({ item: { ...event } }, validate)
        .then((item) => {
          payStoreRequest(item);
        });
    };
  }

  handleChange(event, index) {
    this.setState(index);
  }

  initElement() {
    const { className } = this.props;
    const { index } = this.state;

    return (
      <div className={classNames('xcg-store', className)}>
        <AppBar className="xcg-app-bar" position="fixed">
          <Tabs
            value={index}
            onChange={this.handleChange}
          >
            <Tab
              label={<FormattedMessage id="businessCard.is" />}
              icon={<ContactMail />}
            />
          </Tabs>
        </AppBar>
        <div className="xcg-tab-content">
          {this.getContentTab()}
        </div>
      </div>
    );
  }

  render() {
    return this.initElement();
  }
};

Store.propTypes = {
  className: propTypes.string,
  validate: propTypes.any,
  pay: propTypes.any.isRequired,
  payStoreRequest: propTypes.func.isRequired,
};

Store.defaultProps = {
  className: '',
  validate: pay.body
};

function mapStateToProps(state, props) {
  return createStructuredSelector({
    pay: makeSelectActions(constant.PAY)
  });
}

function mapDispatchToProps(dispatch, props) {
  return {
    payStoreRequest: (item) => {
      return dispatch(payStoreRequest(item));
    },
  };
}

module.exports = connect(
  mapStateToProps,
  mapDispatchToProps,
)(Store);
