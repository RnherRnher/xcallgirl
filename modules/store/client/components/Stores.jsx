const BusinessCardStore = require('./stores/BusinessCardStore');
const Store = require('./stores/Store');

module.exports = {
  BusinessCardStore,
  Store
};
