const request = require('superagent');
const url = require('../../../../commons/modules/store/datas/url.data');

const payStore = ({ csrfToken, item }) => {
  return new Promise((resovle, reject) => {
    request
      .post(url.STORE_PAY)
      .type('application/json')
      .accept('application/json')
      .set('X-Requested-With', 'XMLHttpRequest')
      .set('csrf-token', csrfToken)
      .send(item)
      .end((err, res) => {
        if (err) {
          return reject(res);
        }

        return resovle(res);
      });
  });
};
module.exports.payStore = payStore;
