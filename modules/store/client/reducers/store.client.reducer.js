const constant = require('../constants/store.client.constant');
const { responseState } = require('../../../core/client/utils/middleware');
const { ResultEnum } = require('../../../core/client/constants/core.client.constant');
const { fromJS } = require('immutable');

const initialState = fromJS({
  actions: {
    pay: responseState()
  }
});

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case constant.PAY_STORE_REQUEST: {
      return state.setIn(
        ['actions', 'pay'],
        { result: ResultEnum.wait }
      );
    }
    case constant.PAY_STORE_SUCCESS: {
      return state.setIn(
        ['actions', 'pay'],
        {
          result: ResultEnum.success,
          code: action.code
        }
      );
    }
    case constant.PAY_STORE_FAILED: {
      return state.setIn(
        ['actions', 'pay'],
        {
          result: ResultEnum.failed,
          code: action.code
        }
      );
    }
    case constant.RESERT_STORE_STORE: {
      return initialState;
    }
    default:
      return state;
  }
};
module.exports = reducer;
