const path = require('path');
const { SOCKET_NOTIFICATION_PUSH } = require(path.resolve('./commons/modules/notifications/datas/socket.data'));

/**
 * @overview Khởi tạo
 *
 * @param {SocketIO} io
 * @param {Socket} socket
 */
const init = (io, socket) => {
  socket.on(SOCKET_NOTIFICATION_PUSH, (dataReq) => {
    io.to(dataReq.to)
      .emit(
        SOCKET_NOTIFICATION_PUSH,
        dataReq.body
      );
  });
};
module.exports = init;
