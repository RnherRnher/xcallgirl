const mongoose = require('mongoose');

const Notification = mongoose.model('Notification');

/**
 * @overview Xem thông báo thông báo
 *
 * @param {Request} req
 * @param {Response} res
 * @param {Function} next
 *
 * @input params { id: String }
 *        body   { mark: { is: Boolean } }
 * @output       {
 *                code: String,
 *                notificationID: ObjectId
 *                mark?: Object
 *               }
 */
const mark = (req, res, next) => {
  const bodyReq = req.body;
  const paramsReq = req.params;
  const userSelf = req.user;
  let dataRes = null;

  if (userSelf) {
    Notification
      .findOneAndUpdate(
        {
          _id: paramsReq.id,
          receiveID: userSelf._id,
        },
        {
          $set: {
            'mark.is': bodyReq.mark.is,
            'mark.date': Date.now()
          },
        },
        {
          new: true,
        }
      )
      .exec((err, notification) => {
        if (notification) {
          dataRes = {
            status: 200,
            bodyRes: {
              code: 'notification.mark.success',
              mark: notification.mark,
              notificationID: paramsReq.id
            },
          };
        } else if (err) {
          // Bad request.body
          dataRes = {
            status: 400,
            bodyRes: {
              code: 'notification.mark.failed',
              notificationID: paramsReq.id
            },
          };
        } else {
          // Thông báo không tồn tại
          dataRes = {
            status: 404,
            bodyRes: {
              code: 'notification.notExist',
              notificationID: paramsReq.id
            },
          };
        }

        return res
          .status(dataRes.status)
          .send(dataRes.bodyRes)
          .end();
      });
  } else {
    // Chưa đăng nhập người dùng
    dataRes = {
      status: 401,
      bodyRes: {
        code: 'user.signin.notExist',
        notificationID: paramsReq.id
      },
    };

    return res
      .status(dataRes.status)
      .send(dataRes.bodyRes)
      .end();
  }
};
module.exports.mark = mark;
