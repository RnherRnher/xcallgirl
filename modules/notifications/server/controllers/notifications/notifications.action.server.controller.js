const async = require('async');
const mongoose = require('mongoose');
const path = require('path');
const constant = require(path.resolve('./commons/utils/constant'));

const User = mongoose.model('User');
const Notification = mongoose.model('Notification');

/**
 * @overview Lấy thông báo
 *
 * @param {Request} req
 * @param {Response} res
 * @param {Function} next
 *
 * @input params { id: String }
 * @output       {
 *                 code: String,
 *                 notification: {Notification},
 *                 user: {User}
 *               }
 */
const get = (req, res, next) => {
  const paramsReq = req.params;
  const userSelf = req.user;
  let dataRes = null;

  if (userSelf) {
    Notification
      .findOne({
        _id: paramsReq.id,
        receiveID: userSelf._id,
      })
      .exec((err, notification) => {
        if (notification) {
          dataRes = {
            status: 200,
            bodyRes: {},
          };

          switch (notification.type) {
            case constant.USER:
            case constant.BUSINESS_CARD:
            case constant.BUSINESS_CARD_DEAL_HISTORY:
            case constant.CREDIT:
              User
                .findById(notification.sendID, `-${constant.SALT} -${constant.PASSWORD}`)
                .exec((err, user) => {
                  dataRes.bodyRes = {
                    code: 'notification.exist',
                    notification: notification.getFullInfo(),
                    user: user ? user.getShortcutsInfo() : User.getShortcutsInfoUndefined(),
                  };

                  return res
                    .status(dataRes.status)
                    .send(dataRes.bodyRes)
                    .end();
                });
              break;
            case constant.ADMIN: {
              dataRes.bodyRes = {
                notification: notification.getFullInfo(),
                user: User.getShortcutsInfoAdmin(),
              };

              return res
                .status(dataRes.status)
                .send(dataRes.bodyRes)
                .end();
            }
            default:
              return res
                .status(dataRes.status)
                .send(dataRes.bodyRes)
                .end();
          }
        } else {
          // Thông báo không tồn tại
          dataRes = {
            status: 404,
            bodyRes: {
              code: 'notification.notExist',
            },
          };

          return res
            .status(dataRes.status)
            .send(dataRes.bodyRes)
            .end();
        }
      });
  } else {
    // Chưa đăng nhập người dùng
    dataRes = {
      status: 401,
      bodyRes: {
        code: 'user.signin.notExist',
      },
    };

    return res
      .status(dataRes.status)
      .send(dataRes.bodyRes)
      .end();
  }
};
module.exports.get = get;

/**
 * @overview Tìm kiếm thông báo
 *
 * @param {Request} req
 * @param {Response} res
 * @param {Function} next
 *
 * @input  query  { type: String }
 * @output        {
 *                  code: String,
 *                  skip?: Number,
 *                  count?: Number,
 *                  notifications?: [{Notification}]
 *                }
 */
const search = (req, res, next) => {
  const queryReq = req.query;
  let dataRes = null;

  /**
   * @overview Tìm kiếm thông báo tự động
   *
   * @param {Request} req
   * @param {Response} res
   * @param {Function} next
   *
   * @input query { skip: String }
   * @output      {
   *                code: String,
   *                skip?: Number,
   *                count?: Number,
   *                notifications?: [{Notification}]
   *              }
   */
  const autoSearch = (req, res, next) => {
    const queryReq = req.query;
    const userSelf = req.user;
    let dataRes = null;

    const limit = req.app.locals.config.searchLimit;
    const currentSkip = queryReq.skip > 0 ? queryReq.skip : 0;

    async.waterfall([(done) => {
      if (userSelf) {
        /* Tìm thông báo */

        Notification
          .find({ receiveID: userSelf._id })
          .sort({ initDate: -1 })
          .skip(currentSkip)
          .limit(limit)
          .exec((err, notifications) => {
            if (err) {
              // Bad request.body
              dataRes = {
                status: 400,
                bodyRes: {
                  code: 'data.notVaild',
                },
              };

              done(err, dataRes);
            } else if (!notifications.length) {
              // Thông báo không tồn tại
              dataRes = {
                status: 404,
                bodyRes: {
                  code: 'notification.notExist',
                },
              };

              done(new Error('Not notification exists'), dataRes);
            } else {
              done(null, notifications);
            }
          });
      } else {
        // Chưa đăng nhập người dùng
        dataRes = {
          status: 401,
          bodyRes: {
            code: 'user.signin.notExist',
          },
        };

        done(new Error('Not user signin'), dataRes);
      }
    }, (notifications, done) => {
      /* Lấy thêm thông tin bổ sung */

      async.map(notifications, (notification, callback) => {
        switch (notification.type) {
          case constant.USER:
          case constant.BUSINESS_CARD:
          case constant.BUSINESS_CARD_DEAL_HISTORY:
          case constant.CREDIT:
            User
              .findById(notification.sendID, `-${constant.SALT} -${constant.PASSWORD}`)
              .exec((err, user) => {
                callback(null, {
                  notification: notification.getFullInfo(),
                  user: user ? user.getShortcutsInfo() : User.getShortcutsInfoUndefined(),
                });
              });
            break;
          case constant.ADMIN:
            callback(null, {
              notification: notification.getFullInfo(),
              user: User.getShortcutsInfoAdmin(),
            });
            break;
          default:
            callback(new Error('Empty type notification'));
            break;
        }
      }, (err, resultNotifications) => {
        if (resultNotifications.length) {
          Notification
            .countDocuments({
              receiveID: userSelf._id,
              'mark.is': true
            })
            .exec((err, count) => {
              dataRes = {
                status: 200,
                bodyRes: {
                  skip: currentSkip + (notifications.length < limit ? notifications.length : limit),
                  notifications: resultNotifications,
                  count,
                  code: 'notification.exist',
                },
              };

              done(null, dataRes);
            });
        } else {
          // Thông báo không tồn tại
          dataRes = {
            status: 404,
            bodyRes: {
              code: 'notification.notExist',
            },
          };

          done(null, dataRes);
        }
      });
    }], (err, dataRes) => {
      return res
        .status(dataRes.status)
        .send(dataRes.bodyRes)
        .end();
    });
  };

  switch (queryReq.type) {
    case constant.AUTO:
      autoSearch(req, res, next);
      break;
    default: {
      dataRes = {
        status: 400,
        bodyRes: {
          code: 'data.notVaild',
        },
      };

      return res
        .status(dataRes.status)
        .send(dataRes.bodyRes)
        .end();
    }
  }
};
module.exports.search = search;
