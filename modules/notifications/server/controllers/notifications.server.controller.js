const _ = require('lodash');
const authorization = require('./notifications/notifications.authorization.server.controller');
const action = require('./notifications/notifications.action.server.controller');

module.exports = _.assignIn(
  authorization,
  action
);
