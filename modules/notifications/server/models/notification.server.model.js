const path = require('path');
const chalk = require('chalk');
const mongoose = require('mongoose');
const regular = require(path.resolve('./commons/utils/regular-expression'));
const { validateModelNotification } = require(path.resolve('./utils/validate'));

const { Schema } = mongoose;

const validates = validateModelNotification();

const NotificationSchema = new Schema({
  background: {
    type: String,
    match: regular.app.urlCloudinary
  },
  receiveID: {
    type: mongoose.SchemaTypes.ObjectId,
    required: true,
  },
  sendID: {
    type: mongoose.SchemaTypes.ObjectId,
    required: true,
  },
  toURL: {
    type: String,
    required: true,
    maxlength: regular.commons.stringLength.medium,
  },
  type: {
    type: String,
    required: true,
    enum: validates.type,
  },
  result: {
    type: String,
    required: true,
    enum: validates.result,
  },
  content: {
    type: String,
    required: true,
    maxlength: regular.commons.stringLength.medium,
  },
  mark: {
    is: {
      type: Boolean,
      required: true,
      default: true,
    },
    date: {
      type: Date,
      required: true,
      default: Date.now
    },
  },
  initDate: {
    type: Date,
    required: true,
    default: Date.now
  },
});

/**
 * @overview Lấy thông tin thông báo

 * @returns {Object}
 */
NotificationSchema.methods.getFullInfo = function () {
  const _this = this.toObject();

  _this.sendID = undefined;
  _this.receiveID = undefined;

  return _this;
};

/**
 * @overview Tạo mẫu cơ sở dữ liệu
 *
 * @param {Object} doc
 * @param {overwrite: Boolean} options
 * @returns {Promise}
 */
NotificationSchema.statics.seed = function (doc, options) {
  const Notification = mongoose.model('Notification');

  return new Promise((resolve, reject) => {
    function skipDocument() {
      return new Promise((resolve, reject) => {
        Notification.findOne({ receiveID: doc.receiveID })
          .exec((err, existing) => {
            if (err) {
              return reject(err);
            }

            if (!existing) {
              return resolve(false);
            }

            if (existing && !options.overwrite) {
              return resolve(true);
            }

            // Remove Notification (overwrite)

            existing.remove((err) => {
              if (err) {
                return reject(err);
              }

              return resolve(false);
            });
          });
      });
    }

    function add(skip) {
      return new Promise((resolve, reject) => {
        if (skip) {
          return resolve({
            message: chalk.yellow(`Database Seeding: Notification\t\t type: ${doc.type} skipped`),
          });
        }

        const notification = new Notification(doc);
        notification.save((err) => {
          if (err) {
            return reject(err);
          }

          return resolve({
            message: `Database Seeding: Notification\t\t type: ${notification.type} added`,
          });
        });
      });
    }

    skipDocument()
      .then(add)
      .then((response) => {
        return resolve(response);
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

mongoose.model('Notification', NotificationSchema);
