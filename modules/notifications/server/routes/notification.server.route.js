
const path = require('path');
const policy = require('../policies/notification.server.policy');
const notifications = require('../controllers/notifications.server.controller');
const validate = require(path.resolve('./commons/modules/notifications/validates/notification.validate'));
const middleware = require(path.resolve('./utils/middleware'));
const url = require(path.resolve('./commons/modules/notifications/datas/url.data'));

const init = (app) => {
  app.route(url.NOTIFICATIONS_SEARCH)
    .all(policy.isAllowed)
    .get(middleware.paramsValidation(validate, 'search'), notifications.search);

  app.route(url.NOTIFICATIONS_GET_ID)
    .all(policy.isAllowed)
    .get(middleware.paramsValidation(validate, 'get'), notifications.get)
    .post(middleware.paramsValidation(validate, 'mark'), notifications.mark);
};
module.exports = init;
