const React = require('react');
const propTypes = require('prop-types');
const classNames = require('classnames');
const constant = require('../../../../commons/utils/constant');
const url = require('../../../core/client/utils/url');
const { FormattedMessage } = require('react-intl');
const { push } = require('connected-react-router/immutable');
const { connect } = require('react-redux');
const {
  CheckCircle,
  Warning,
  Error,
  Info,
  Store,
  Whatshot,
  Person,
  ShoppingCart,
  MoreVert,
  VisibilityOff,
  Visibility,
  CreditCard
} = require('@material-ui/icons');
const {
  List,
  ListItem,
  Avatar,
  ListItemAvatar,
  ListItemSecondaryAction,
  Badge,
  IconButton,
  Menu,
  MenuItem,
  ListItemIcon,
  ListItemText
} = require('@material-ui/core');
const { markNotificationRequest } = require('../actions/notification.client.action');
const { UIDate } = require('../../../core/client/components/UIs');

const Notification = class Notification extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      clicked: false,
      anchorEl: null
    };

    this.handleClick = this.handleClick.bind(this);
    this.handleClickMore = this.handleClickMore.bind(this);
    this.handleClickMenuItem = this.handleClickMenuItem.bind(this);
    this.handleCloseMenu = this.handleCloseMenu.bind(this);
  }

  handleClick(event) {
    const {
      markNotificationRequest,
      redirect,
      value
    } = this.props;

    switch (value.notification.type) {
      case constant.BUSINESS_CARD:
        redirect(`${url.BUSINESS_CARD_GET}?id=${value.notification.toURL}`);
        break;
      case constant.BUSINESS_CARD_DEAL_HISTORY:
        redirect(`${url.DEAL_HISTORY_GET}?id=${value.notification.toURL}`);
        break;
      case constant.USER:
      case constant.ADMIN:
        redirect(`${url.USER_GET}?id=${value.notification.toURL}`);
        break;
      case constant.CREDIT:
        redirect(`${url.CREDIT_GET}?id=${value.notification.toURL}`);
        break;
      default:
        break;
    }

    if (value.notification.mark.is) {
      markNotificationRequest(
        value.notification._id,
        {
          mark: {
            is: !value.notification.mark.is
          }
        }
      );
    }
  }

  handleClickMore(event) {
    this.setState({
      clicked: true,
      anchorEl: event.target
    });
  }

  handleClickMenuItem(type) {
    return (event) => {
      const { value, markNotificationRequest } = this.props;

      switch (type) {
        case constant.MARK:
          markNotificationRequest(
            value.notification._id,
            {
              mark: {
                is: !value.notification.mark.is
              }
            }
          );
          break;
        default:
          break;
      }

      this.handleCloseMenu();
    };
  }

  handleCloseMenu(event) {
    this.setState({
      clicked: false,
      anchorEl: null
    });
  }

  Menu() {
    const { value } = this.props;
    const { clicked, anchorEl } = this.state;

    return (
      <Menu
        anchorEl={anchorEl}
        open={clicked}
        onClose={this.handleCloseMenu}
      >
        <MenuItem
          onClick={this.handleClickMenuItem(constant.MARK)}
          value={constant.MARK}
        >
          <ListItemIcon>
            {
              value.notification.mark.is
                ? <VisibilityOff />
                : <Visibility />
            }
          </ListItemIcon>
          <ListItemText
            inset
            primary={
              <FormattedMessage id={
                value.notification.mark.is
                  ? 'actions.unMark'
                  : 'actions.mark'
              }
              />
            }
          />
        </MenuItem>
      </Menu>
    );
  }

  Icon() {
    const { value } = this.props;

    let Icon = null;
    switch (value.notification.result) {
      case constant.FAILED:
        Icon = <Error className="xcg-error" />;
        break;
      case constant.SUCCESS:
        Icon = <CheckCircle className="xcg-success" />;
        break;
      case constant.WARNING:
        Icon = <Warning className="xcg-warning" />;
        break;
      case constant.INFO:
        Icon = <Info className="xcg-info" />;
        break;
      default:
        break;
    }

    return Icon;
  }

  Type() {
    const { value } = this.props;

    let Type = null;
    switch (value.notification.type) {
      case constant.USER:
        Type = <Person />;
        break;
      case constant.ADMIN:
        Type = <Whatshot />;
        break;
      case constant.BUSINESS_CARD_DEAL_HISTORY:
        Type = <ShoppingCart />;
        break;
      case constant.BUSINESS_CARD:
        Type = <Store />;
        break;
      case constant.CREDIT:
        Type = <CreditCard />;
        break;
      default:
        break;
    }

    return Type;
  }

  background() {
    const { value } = this.props;

    const background = {
      name: 'notification_background',
      url: value.user.avatar
    };

    if (value.notification.background) {
      background.url = value.notification.background;
    }

    return background;
  }

  initElement() {
    const { className, value } = this.props;

    const background = this.background();

    return (
      <div className={classNames('xcg-notification', className)}>
        <List className={value.notification.mark.is ? 'xcg-mark' : 'xcg-un-mark'}>
          <ListItem
            button
            onClick={this.handleClick}
          >
            <ListItemAvatar className="xcg-list-item-avatar">
              <Badge badgeContent={this.Type()}>
                <Avatar
                  className="xcg-avatar"
                  alt={background.name}
                  src={background.url}
                />
              </Badge>
            </ListItemAvatar>
            <ListItemText
              inset
              primary={<FormattedMessage id={value.notification.content} />}
              secondary={<UIDate value={value.notification.initDate} />}
            />
            <ListItemIcon>
              {this.Icon()}
            </ListItemIcon>
            <ListItemSecondaryAction>
              <IconButton onClick={this.handleClickMore}>
                <MoreVert />
              </IconButton>
            </ListItemSecondaryAction>
          </ListItem>
        </List>
        {this.Menu()}
      </div>
    );
  }

  render() {
    return this.initElement();
  }
};

Notification.propTypes = {
  className: propTypes.string,
  value: propTypes.object.isRequired,
  markNotificationRequest: propTypes.func.isRequired,
  redirect: propTypes.func.isRequired
};

Notification.defaultProps = {
  className: ''
};

function mapStateToProps(state, props) {
  return {};
}

function mapDispatchToProps(dispatch, props) {
  return {
    markNotificationRequest: (notificationID, mark) => {
      return dispatch(markNotificationRequest(notificationID, mark));
    },
    redirect: (location) => {
      return dispatch(push(location));
    }
  };
}

module.exports = connect(
  mapStateToProps,
  mapDispatchToProps,
)(Notification);
