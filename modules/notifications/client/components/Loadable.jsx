const React = require('react');
const propTypes = require('prop-types');
const classNames = require('classnames');
const ContentLoader = require('react-content-loader').default;

const Loadable = class Loadable extends React.Component {
  constructor(props) {
    super(props);
  }

  initElement() {
    const { className } = this.props;

    return (
      <div className={classNames('xcg-loadable-notification', className)}>
        <ContentLoader
          height={65}
          width={350}
          speed={2}
          primaryColor="#666666"
          secondaryColor="#737373"
        >
          <rect x="90" y="20" rx="5" ry="5" width="220" height="15" />
          <rect x="90" y="45" rx="5" ry="5" width="100" height="10" />
          <circle cx="45" cy="35" r="25" />
          <circle cx="65" cy="50" r="12" />
        </ContentLoader>
      </div>
    );
  }

  render() {
    return this.initElement();
  }
};

Loadable.propTypes = {
  className: propTypes.string,
};

Loadable.defaultProps = {
  className: '',
};

module.exports = Loadable;
