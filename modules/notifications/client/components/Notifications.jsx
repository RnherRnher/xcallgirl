const React = require('react');
const propTypes = require('prop-types');
const classNames = require('classnames');
const constant = require('../../../../commons/utils/constant');
const Notification = require('./Notification');
const Loadable = require('./Loadable');
const { connect } = require('react-redux');
const { createStructuredSelector } = require('reselect');
const { WindowScroll } = require('../../../core/client/components/Scrolls');
const { getNotificationsRequest } = require('../actions/notification.client.action');
const { makeSelectSelfNotifications } = require('../reselects/notification.client.reselect');

const Notifications = class Notifications extends React.Component {
  constructor(props) {
    super(props);

    this.handleLoad = this.handleLoad.bind(this);
  }

  handleLoad(reload) {
    const { getNotificationsRequest, notifications } = this.props;

    getNotificationsRequest(
      constant.AUTO,
      notifications.skip,
      reload
    );
  }

  initElement() {
    const {
      className,
      notifications,
      rowHeight
    } = this.props;

    return (
      <div className={classNames('xcg-notifications', className)}>
        <WindowScroll
          result={notifications.result}
          handleLoadMore={this.handleLoad}
          initialLoad={!notifications.data.length}
          value={notifications.data}
          Children={Notification}
          Loadable={Loadable}
          rowHeight={rowHeight}
        />
      </div>
    );
  }

  render() {
    return this.initElement();
  }
};

Notifications.propTypes = {
  className: propTypes.string,
  rowHeight: propTypes.number,
  notifications: propTypes.object.isRequired,
  getNotificationsRequest: propTypes.func.isRequired
};

Notifications.defaultProps = {
  className: '',
  rowHeight: 80
};

function mapStateToProps(state, props) {
  return createStructuredSelector({
    notifications: makeSelectSelfNotifications()
  });
}

function mapDispatchToProps(dispatch, props) {
  return {
    getNotificationsRequest: (
      fragment,
      skip,
      reload
    ) => {
      return dispatch(getNotificationsRequest(
        fragment,
        skip,
        reload
      ));
    }
  };
}

module.exports = connect(
  mapStateToProps,
  mapDispatchToProps,
)(Notifications);
