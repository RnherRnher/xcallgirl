const { action } = require('../../../core/client/utils/middleware');
const constant = require('../constants/notification.client.constant');

/**
 * @override Yêu cầu lấy thông báo
 *
 * @param   {String}  fragment
 * @param   {Number}  skip
 * @param   {Boolean} reload
 * @returns {Object}
 */
const getNotificationsRequest = (
  fragment,
  skip,
  reload = false
) => {
  return action(
    constant.GET_NOTIFICATIONS_REQUEST,
    {
      fragment,
      skip: reload ? 0 : (skip || 0),
      reload
    }
  );
};
module.exports.getNotificationsRequest = getNotificationsRequest;

/**
 * @override Lấy thông báo thành công
 *
 * @param   {Object}
 * @returns {Object}
 */
const getNotificationsSuccess = ({
  code,
  skip,
  notifications,
  count
}) => {
  return action(
    constant.GET_NOTIFICATIONS_SUCCESS,
    {
      code,
      skip,
      notifications,
      count
    }
  );
};
module.exports.getNotificationsSuccess = getNotificationsSuccess;

/**
 * @override Lấy thông báo thất bại
 *
 * @param   {Object}
 * @returns {Object}
 */
const getNotificationsFailed = ({ code }) => {
  return action(constant.GET_NOTIFICATIONS_FAILED, { code });
};
module.exports.getNotificationsFailed = getNotificationsFailed;

/**
 * @override Yêu cầu đánh dấu thông báo
 *
 * @param   {ObjectId}  notificationID
 * @param   {Object}   mark
 * @returns {Object}
 */
const markNotificationRequest = (notificationID, mark) => {
  return action(constant.MARK_NOTIFICATION_REQUEST, { notificationID, mark });
};
module.exports.markNotificationRequest = markNotificationRequest;

/**
 * @override Đánh dấu thông báo thành công
 *
 * @param   {Object}
 * @returns {Object}
 */
const markNotificationSuccess = ({
  code,
  notificationID,
  mark
}) => {
  return action(
    constant.MARK_NOTIFICATION_SUCCESS,
    {
      code,
      notificationID,
      mark
    }
  );
};
module.exports.markNotificationSuccess = markNotificationSuccess;

/**
 * @override Đánh dấu thông báo thất bại
 *
 * @param   {Object}
 * @returns {Object}
 */
const markNotificationFailed = ({ code, notificationID }) => {
  return action(constant.MARK_NOTIFICATION_FAILED, { code, notificationID });
};
module.exports.markNotificationFailed = markNotificationFailed;

/**
 * @override Yêu cầu lấy thông báo
 *
 * @param   {Object}
 * @returns {Object}
 */
const getNotificationRequest = ({ notificationID }) => {
  return action(constant.GET_NOTIFICATION_REQUEST, { notificationID });
};
module.exports.getNotificationRequest = getNotificationRequest;

/**
 * @override Lấy thông báo thành công
 *
 * @param   {Object}
 * @returns {Object}
 */
const getNotificationSuccess = ({ user, notification }) => {
  return action(constant.GET_NOTIFICATION_SUCCESS, { user, notification });
};
module.exports.getNotificationSuccess = getNotificationSuccess;

/**
 * @override Thiết lặp lại cửa hàng
 *
 * @param   {Object}
 */
const resetNotificationStore = () => {
  return action(constant.RESERT_NOTIFICATION_STORE);
};
module.exports.resetNotificationStore = resetNotificationStore;
