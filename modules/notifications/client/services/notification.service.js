const request = require('superagent');
const url = require('../../../../commons/modules/notifications/datas/url.data');

const getNotifications = ({ type, skip }) => {
  return new Promise((resovle, reject) => {
    request
      .get(url.NOTIFICATIONS_SEARCH)
      .accept('application/json')
      .set('X-Requested-With', 'XMLHttpRequest')
      .query({ type, skip })
      .end((err, res) => {
        if (err) {
          return reject(res);
        }

        return resovle(res);
      });
  });
};
module.exports.getNotifications = getNotifications;

const markNotification = ({ csrfToken, notificationID, mark }) => {
  return new Promise((resovle, reject) => {
    request
      .post(url.NOTIFICATIONS_GET_ID.replace(':id', notificationID))
      .type('application/json')
      .accept('application/json')
      .set('X-Requested-With', 'XMLHttpRequest')
      .set('csrf-token', csrfToken)
      .send(mark)
      .end((err, res) => {
        if (err) {
          return reject(res);
        }

        return resovle(res);
      });
  });
};
module.exports.markNotification = markNotification;

const getNotification = ({ notificationID }) => {
  return new Promise((resovle, reject) => {
    request
      .get(url.NOTIFICATIONS_GET_ID.replace(':id', notificationID))
      .accept('application/json')
      .set('X-Requested-With', 'XMLHttpRequest')
      .end((err, res) => {
        if (err) {
          return reject(res);
        }

        return resovle(res);
      });
  });
};
module.exports.getNotification = getNotification;
