const { createSelector } = require('reselect');
const { makeSelect } = require('../../../core/client/utils/middleware');

const selectNotification = (state, props) => {
  return state.get('notification');
};

const makeSelectSelfNotifications = () => {
  return createSelector(
    selectNotification,
    (notificationState) => {
      return makeSelect(notificationState.get('self'));
    }
  );
};
module.exports.makeSelectSelfNotifications = makeSelectSelfNotifications;

const makeSelectCountNotifications = () => {
  return createSelector(
    selectNotification,
    (notificationState) => {
      return makeSelect(notificationState.getIn(['self', 'count']));
    }
  );
};
module.exports.makeSelectCountNotifications = makeSelectCountNotifications;
