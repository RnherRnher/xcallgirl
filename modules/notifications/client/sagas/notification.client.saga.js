const nprogress = require('nprogress');
const api = require('../services/notification.service');
const action = require('../actions/notification.client.action');
const constant = require('../constants/notification.client.constant');
const constantCommon = require('../../../../commons/utils/constant');
const {
  takeLatest,
  put,
  call,
  takeEvery
} = require('redux-saga/effects');
const { pushAlertSnackbar } = require('../../../core/client/actions/core.client.action');
const { csrfRequest } = require('../../../core/client/services/core.client.service');
const { initApp } = require('../../../core/client/actions/app.client.action');

const watchGetNotifications = function* () {
  yield takeLatest(constant.GET_NOTIFICATIONS_REQUEST, function* ({ fragment, skip }) {
    try {
      nprogress.start();

      const response = yield call(
        api.getNotifications,
        {
          type: fragment,
          skip
        }
      );

      yield put(action.getNotificationsSuccess(response.body));
    } catch (error) {
      if (error && error.body) {
        yield put(action.getNotificationsFailed(error.body));
        if (skip !== 0) {
          yield put(pushAlertSnackbar({
            variant: constantCommon.ERROR,
            code: error.body.code
          }));
        }
      } else {
        yield put(initApp());
      }
    } finally {
      nprogress.done();
    }
  });
};
module.exports.watchGetNotifications = watchGetNotifications;

const watchMarkNotification = function* () {
  yield takeEvery(constant.MARK_NOTIFICATION_REQUEST, function* ({ notificationID, mark }) {
    try {
      const responseCSRF = yield call(csrfRequest);
      const response = yield call(
        api.markNotification,
        {
          csrfToken: responseCSRF.body.csrfToken,
          notificationID,
          mark
        }
      );

      yield put(action.markNotificationSuccess(response.body));
    } catch (error) {
      if (error && error.body) {
        yield put(action.markNotificationFailed(error.body));
        yield put(pushAlertSnackbar({
          variant: constantCommon.ERROR,
          code: error.body.code
        }));
      } else {
        yield put(initApp());
      }
    }
  });
};
module.exports.watchMarkNotification = watchMarkNotification;

const watchGetNotification = function* () {
  yield takeEvery(constant.GET_NOTIFICATION_REQUEST, function* ({ notificationID }) {
    const response = yield call(
      api.getNotification,
      { notificationID }
    );

    yield put(action.getNotificationSuccess(response.body));
    yield put(pushAlertSnackbar({
      variant: response.body.notification.result,
      code: response.body.notification.content
    }));
  });
};
module.exports.watchGetNotification = watchGetNotification;
