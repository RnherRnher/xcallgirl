const { getNotificationRequest } = require('../actions/notification.client.action');
const { SOCKET_NOTIFICATION_PUSH } = require('../../../../commons/modules/notifications/datas/socket.data');

const init = (dispatch, socket) => {
  socket.on(SOCKET_NOTIFICATION_PUSH, (body) => {
    dispatch(getNotificationRequest(body));
  });
};
module.exports = init;
