const constant = require('../constants/notification.client.constant');
const constantCommon = require('../../../../commons/utils/constant');
const { findIndex } = require('lodash');
const { fromJS } = require('immutable');
const { responseState } = require('../../../core/client/utils/middleware');
const { ResultEnum } = require('../../../core/client/constants/core.client.constant');

const initialState = fromJS({
  self: responseState({
    data: [],
    count: 0
  }),
  actions: {
    marks: {}
  }
});

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case constant.GET_NOTIFICATIONS_REQUEST: {
      return state
        .setIn(['self', 'result'], ResultEnum.wait)
        .setIn(['self', 'skip'], action.skip)
        .setIn(['self', 'fragment'], action.fragment)
        .setIn(['self', 'reload'], action.reload)
        .setIn(['self', 'code'], undefined);
    }
    case constant.GET_NOTIFICATIONS_SUCCESS: {
      let _state = state
        .setIn(['self', 'result'], ResultEnum.success)
        .setIn(['self', 'skip'], action.skip)
        .setIn(['self', 'code'], action.code)
        .setIn(['self', 'count'], action.count);

      const notifications = action.notifications.sort((a, b) => {
        return a.notification.initDate - b.notification.initDate;
      });

      if (_state.getIn(['self', 'reload'])) {
        _state = _state.setIn(['self', 'data'], notifications);
      } else {
        _state = _state.updateIn(
          ['self', 'data'],
          (data) => {
            return data.concat(notifications);
          }
        );
      }

      return _state;
    }
    case constant.GET_NOTIFICATIONS_FAILED: {
      if (state.getIn(['self', 'reload'])) {
        return state.set(
          'self',
          {
            result: ResultEnum.failed,
            code: action.code,
            data: [],
            count: 0
          }
        );
      }

      return state
        .setIn(['self', 'result'], ResultEnum.failed)
        .setIn(['self', 'code'], action.code);
    }
    case constant.MARK_NOTIFICATION_REQUEST: {
      return state.setIn(
        ['actions', 'marks', action.notificationID],
        {
          nextType: action.mark.mark.is ? constantCommon.UNMARK : constantCommon.MARK,
          result: ResultEnum.wait
        }
      );
    }
    case constant.MARK_NOTIFICATION_SUCCESS: {
      const index = findIndex(state.getIn(['self', 'data']), (data) => {
        return data.notification._id === action.notificationID;
      });

      const value = state.getIn(['self', 'data', index]);
      value.notification.mark = action.mark;

      return state
        .updateIn(
          ['self', 'data'],
          (data) => {
            data[index] = value;

            return data;
          }
        )
        .updateIn(
          ['self', 'count'],
          (count) => {
            return action.mark.is ? count + 1 : count - 1;
          }
        )
        .updateIn(
          ['actions', 'marks', action.notificationID],
          (mark) => {
            return {
              result: ResultEnum.success,
              code: action.code,
              nextType: mark.nextType
            };
          }
        );
    }
    case constant.MARK_NOTIFICATION_FAILED: {
      return state.updateIn(
        ['actions', 'marks', action.notificationID],
        (mark) => {
          return {
            result: ResultEnum.failed,
            code: action.code,
            nextType: mark.nextType === constantCommon.MARK
              ? constantCommon.UNMARK
              : constantCommon.MARK,
          };
        }
      );
    }
    case constant.GET_NOTIFICATION_SUCCESS: {
      return state
        .updateIn(
          ['self', 'skip'],
          (skip) => {
            return skip ? skip + 1 : 1;
          }
        )
        .updateIn(
          ['self', 'count'],
          (count) => {
            return action.notification.mark.is ? count + 1 : count - 1;
          }
        )
        .updateIn(
          ['self', 'data'],
          (data) => {
            return fromJS([{
              user: action.user,
              notification: action.notification
            }]).concat(data);
          }
        );
    }
    case constant.RESERT_NOTIFICATION_STORE: {
      return initialState;
    }
    default:
      return state;
  }
};
module.exports = reducer;
