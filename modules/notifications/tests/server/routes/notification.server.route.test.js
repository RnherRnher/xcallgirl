const path = require('path');
const mongoose = require('mongoose');
const request = require('supertest');
const chai = require('chai');
const express = require(path.resolve('./config/lib/express'));
const url = require(path.resolve('./commons/modules/notifications/datas/url.data'));
const constant = require(path.resolve('./commons/utils/constant'));
const utils = require(path.resolve('./utils/test'));

describe('### Notification Server Routes Tests', () => {
  let app = null;
  let agent = null;

  let Notification = null;
  let notification1 = null;
  let notification2 = null;
  let notification3 = null;

  let User = null;
  let user = null;

  const password = 'password';

  let receiveID = null;
  const sendID = mongoose.Types.ObjectId();

  /**
   * @overview Kiểm tra đã xem
   *
   * @param {ObjetcID} notificationID
   * @param {Function} done
   * @param {Boolean} isSucess
   */
  const testMark = (notificationID, done, isMark) => {
    Notification
      .findById(notificationID)
      .exec((err, notification) => {
        chai.assert.notExists(err);
        chai.assert.exists(notification);

        chai.assert.deepEqual(notification.mark.is, isMark);

        if (done) done();
      });
  };

  before((done) => {
    app = express.init(mongoose.connection.db);
    agent = request.agent(app);

    Notification = mongoose.model('Notification');
    User = mongoose.model('User');

    done();
  });

  beforeEach((done) => {
    user = new User({
      name: {
        first: 'first',
        last: 'last',
        display: 'last first',
        nick: 'user_user',
      },
      numberPhone: {
        code: '+84',
        number: '0000000000'
      },
      gender: constant.MALE,
      birthday: {
        day: 1,
        month: 1,
        year: 2000,
      },
      password,
      address: {
        country: 'country',
        city: 'city',
        county: 'county',
        local: 'local',
      },
      settings: {
        general: {
          language: constant.VI,
        },
      },
    });

    receiveID = user._id;

    notification1 = new Notification({
      receiveID,
      sendID,
      toURL: receiveID,
      type: constant.USER,
      result: constant.SUCCESS,
      content: 'user.exist',
    });

    notification2 = new Notification({
      receiveID,
      sendID,
      toURL: receiveID,
      type: constant.USER,
      result: constant.SUCCESS,
      content: 'user.exist',
    });

    notification3 = new Notification({
      receiveID: sendID,
      sendID: receiveID,
      toURL: receiveID,
      type: constant.USER,
      result: constant.SUCCESS,
      content: 'user.exist',
    });

    user.save((err) => {
      chai.assert.notExists(err);
      notification1.save((err) => {
        chai.assert.notExists(err);
        notification2.save((err) => {
          chai.assert.notExists(err);
          notification3.save((err) => {
            chai.assert.notExists(err);
            done();
          });
        });
      });
    });
  });

  afterEach((done) => {
    User.deleteMany({}, (err) => {
      chai.assert.notExists(err);
      Notification.deleteMany({}, (err) => {
        chai.assert.notExists(err);
        done();
      });
    });
  });

  describe('## Tìm kiếm thông báo', () => {
    it('should success auto search notification', (done) => {
      utils.signin(agent, user.numberPhone, password)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.get((url.NOTIFICATIONS_SEARCH))
            .set('X-Requested-With', 'XMLHttpRequest')
            .query({
              type: constant.AUTO,
              skip: 0,
            })
            .expect(200)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.skip, 2);
              chai.assert.deepEqual(res.body.notifications.length, 2);

              return done();
            });
        });
    });

    it('should success auto search notification. But not notification', (done) => {
      Notification.deleteMany({}, (err) => {
        chai.assert.notExists(err);

        utils.signin(agent, user.numberPhone, password)
          .expect(200)
          .end((err, res) => {
            if (err) {
              return done(err);
            }

            agent.get((url.NOTIFICATIONS_SEARCH))
              .set('X-Requested-With', 'XMLHttpRequest')
              .query({
                type: constant.AUTO,
                skip: 0,
              })
              .expect(404)
              .end((err, res) => {
                if (err) {
                  return done(err);
                }

                chai.assert.deepEqual(res.body.code, 'notification.notExist');

                return done();
              });
          });
      });
    });

    it('should failed auto search notification with not signin', (done) => {
      agent.get((url.NOTIFICATIONS_SEARCH))
        .set('X-Requested-With', 'XMLHttpRequest')
        .query({
          type: constant.AUTO,
          skip: 0,
        })
        .expect(401)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          chai.assert.deepEqual(res.body.code, 'user.permission.notExist');
          return done();
        });
    });

    it('should failed auto search notification with bad query', (done) => {
      utils.signin(agent, user.numberPhone, password)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.get((url.NOTIFICATIONS_SEARCH))
            .set('X-Requested-With', 'XMLHttpRequest')
            .expect(400)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.code, 'data.notVaild');
              return done();
            });
        });
    });
  });

  describe('## Lấy thông báo theo ID', () => {
    it('should success get notification', (done) => {
      utils.signin(agent, user.numberPhone, password)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.get((url.NOTIFICATIONS_GET_ID.replace(':id', notification1._id)))
            .set('X-Requested-With', 'XMLHttpRequest')
            .expect(200)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.exists(res.body.notification);
              chai.assert.exists(res.body.user);

              return done();
            });
        });
    });

    it('should failed get notification with not signin', (done) => {
      agent.get((url.NOTIFICATIONS_GET_ID.replace(':id', notification1._id)))
        .set('X-Requested-With', 'XMLHttpRequest')
        .expect(401)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          chai.assert.deepEqual(res.body.code, 'user.permission.notExist');
          return done();
        });
    });

    it('should failed get notification with not exist notification', (done) => {
      utils.signin(agent, user.numberPhone, password)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.get((url.NOTIFICATIONS_GET_ID.replace(':id', notification3._id)))
            .set('X-Requested-With', 'XMLHttpRequest')
            .expect(404)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.code, 'notification.notExist');
              return done();
            });
        });
    });

    it('should failed get notification with bad id', (done) => {
      utils.signin(agent, user.numberPhone, password)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.get((url.NOTIFICATIONS_GET_ID.replace(':id', mongoose.Types.ObjectId())))
            .set('X-Requested-With', 'XMLHttpRequest')
            .expect(404)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.code, 'notification.notExist');
              return done();
            });
        });
    });
  });

  describe('## Xem thông báo theo ID', () => {
    it('should success mark notification', (done) => {
      utils.signin(agent, user.numberPhone, password)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          const isMark = true;

          agent.post((url.NOTIFICATIONS_GET_ID.replace(':id', notification1._id)))
            .set('X-Requested-With', 'XMLHttpRequest')
            .expect(200)
            .send({ mark: { is: isMark } })
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              testMark(notification1._id, done, isMark);
            });
        });
    });

    it('should success mark notification', (done) => {
      utils.signin(agent, user.numberPhone, password)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          const isMark = false;

          agent.post((url.NOTIFICATIONS_GET_ID.replace(':id', notification1._id)))
            .set('X-Requested-With', 'XMLHttpRequest')
            .expect(200)
            .send({ mark: { is: isMark } })
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              testMark(notification1._id, done, isMark);
            });
        });
    });

    it('should failed mark notification with not signin', (done) => {
      const isMark = true;

      agent.post((url.NOTIFICATIONS_GET_ID.replace(':id', notification1._id)))
        .set('X-Requested-With', 'XMLHttpRequest')
        .expect(401)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          chai.assert.deepEqual(res.body.code, 'user.permission.notExist');
          testMark(notification1._id, done, isMark);
        });
    });

    it('should failed mark notification with not exist notification', (done) => {
      utils.signin(agent, user.numberPhone, password)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          const isMark = true;

          agent.post((url.NOTIFICATIONS_GET_ID.replace(':id', notification3._id)))
            .set('X-Requested-With', 'XMLHttpRequest')
            .expect(404)
            .send({ mark: { is: isMark } })
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.code, 'notification.notExist');
              testMark(notification3._id, done, isMark);
            });
        });
    });

    it('should failed mark notification with bad id', (done) => {
      utils.signin(agent, user.numberPhone, password)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          const isMark = true;

          agent.post((url.NOTIFICATIONS_GET_ID.replace(':id', mongoose.Types.ObjectId())))
            .set('X-Requested-With', 'XMLHttpRequest')
            .expect(404)
            .send({ mark: { is: isMark } })
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.code, 'notification.notExist');
              return done();
            });
        });
    });
  });
});
