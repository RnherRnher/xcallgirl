const path = require('path');
const mongoose = require('mongoose');
const chai = require('chai');
const constant = require(path.resolve('./commons/utils/constant'));

describe('### Notification Server Model Tests', () => {
  let Notification = null;
  let notification = null;

  before((done) => {
    Notification = mongoose.model('Notification');

    done();
  });

  beforeEach((done) => {
    notification = {
      receiveID: mongoose.Types.ObjectId(),
      sendID: mongoose.Types.ObjectId(),
      toURL: mongoose.Types.ObjectId(),
      type: constant.USER,
      result: constant.SUCCESS,
      content: 'user.exist',
    };

    done();
  });

  afterEach((done) => {
    Notification.deleteMany({}, (err) => {
      chai.assert.notExists(err);
      done();
    });
  });

  it('should success to save', (done) => {
    const _notification = new Notification(notification);

    _notification.save((err) => {
      chai.assert.notExists(err);
      done();
    });
  });

  it('should failed to save with not receiveID', (done) => {
    const _notification = new Notification(notification);
    _notification.receiveID = undefined;

    _notification.save((err) => {
      chai.assert.exists(err);
      done();
    });
  });

  it('should failed to save with not toURL', (done) => {
    const _notification = new Notification(notification);
    _notification.toURL = undefined;

    _notification.save((err) => {
      chai.assert.exists(err);
      done();
    });
  });

  it('should failed to save with not type', (done) => {
    const _notification = new Notification(notification);
    _notification.type = undefined;

    _notification.save((err) => {
      chai.assert.exists(err);
      done();
    });
  });

  it('should failed to save with not result', (done) => {
    const _notification = new Notification(notification);
    _notification.result = undefined;

    _notification.save((err) => {
      chai.assert.exists(err);
      done();
    });
  });

  it('should failed to save with not content', (done) => {
    const _notification = new Notification(notification);
    _notification.content = undefined;

    _notification.save((err) => {
      chai.assert.exists(err);
      done();
    });
  });

  it('should failed to save with bad type', (done) => {
    const _notification = new Notification(notification);
    _notification.type = 'some_type';

    _notification.save((err) => {
      chai.assert.exists(err);
      done();
    });
  });

  it('should failed to save with bad result', (done) => {
    const _notification = new Notification(notification);
    _notification.type = 'some_result';

    _notification.save((err) => {
      chai.assert.exists(err);
      done();
    });
  });
});
