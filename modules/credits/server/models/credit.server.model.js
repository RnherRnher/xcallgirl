const path = require('path');
const chalk = require('chalk');
const mongoose = require('mongoose');
const constant = require(path.resolve('./commons/utils/constant'));
const { validateModelCredit } = require(path.resolve('./utils/validate'));

const { Schema } = mongoose;

const validates = validateModelCredit();

const CreditSchema = new Schema({
  userID: {
    type: mongoose.SchemaTypes.ObjectId,
    required: true,
    unique: true,
  },
  balance: {
    type: Number,
    required: true,
    min: 0,
    default: 0
  },
  historys: {
    type: [{
      userID: {
        type: mongoose.SchemaTypes.ObjectId,
        required: true,
      },
      type: {
        type: String,
        required: true,
        enum: validates.historys.type,
      },
      balance: {
        type: Number,
        min: 0,
        required: true
      },
      initDate: {
        type: Date,
        required: true
      },
    }]
  },
  updateDate: {
    type: Date,
    required: true,
    default: Date.now
  },
});

/**
 * @overview Chuyển khoản
 *
 * @param   {Credit} credit
 * @param   {Number} moneys
 * @returns {Boolean}
 */
CreditSchema.methods.transfer = function (credit, moneys) {
  if (moneys && credit && this.balance >= moneys) {
    const currentDate = new Date();

    this.balance -= moneys;
    this.updateDate = currentDate;
    this.historys.push({
      userID: credit.userID,
      type: constant.PUT,
      balance: moneys,
      initDate: currentDate
    });

    credit.balance += moneys;
    credit.updateDate = currentDate;
    credit.historys.push({
      userID: this.userID,
      type: constant.POST,
      balance: moneys,
      initDate: currentDate
    });

    return true;
  }

  return false;
};


/**
 * @overview Lấy thông tin thẻ tín dụng

 * @returns {Object}
 */
CreditSchema.methods.getFullInfo = function () {
  const _this = this.toObject();

  _this.historys = undefined;

  return _this;
};

/**
 * @overview Tạo mẫu cơ sở dữ liệu
 *
 * @param {Object} doc
 * @param {overwrite: Boolean} options
 * @returns {Promise}
 */
CreditSchema.statics.seed = function (doc, options) {
  const Credit = mongoose.model('Credit');

  return new Promise((resolve, reject) => {
    function skipDocument() {
      return new Promise((resolve, reject) => {
        Credit.findOne({ userID: doc.userID })
          .exec((err, existing) => {
            if (err) {
              return reject(err);
            }

            if (!existing) {
              return resolve(false);
            }

            if (existing && !options.overwrite) {
              return resolve(true);
            }

            // Remove Credit (overwrite)

            existing.remove((err) => {
              if (err) {
                return reject(err);
              }

              return resolve(false);
            });
          });
      });
    }

    function add(skip) {
      return new Promise((resolve, reject) => {
        if (skip) {
          return resolve({
            message: chalk.yellow(`Database Seeding: Credit\t\t userID: ${doc.userID} skipped`),
          });
        }

        const credit = new Credit(doc);
        credit.save((err) => {
          if (err) {
            return reject(err);
          }

          return resolve({
            message: `Database Seeding: Credit\t\t userID: ${credit.userID} added`,
          });
        });
      });
    }

    skipDocument()
      .then(add)
      .then((response) => {
        return resolve(response);
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

mongoose.model('Credit', CreditSchema);
