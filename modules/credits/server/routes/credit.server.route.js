const path = require('path');
const policy = require('../policies/credit.server.policy');
const credits = require('../controllers/credits.server.controller');
const validate = require(path.resolve('./commons/modules/credits/validates/credits.validate'));
const middleware = require(path.resolve('./utils/middleware'));
const url = require(path.resolve('./commons/modules/credits/datas/url.data'));

const init = (app) => {
  app.route(url.CREDITS_TRANSFER_ID)
    .all(policy.isAllowed)
    .post(middleware.paramsValidation(validate, 'transfer'), credits.transfer);
};
module.exports = init;
