const mongoose = require('mongoose');
const async = require('async');
const path = require('path');
const logger = require(path.resolve('./config/lib/logger'));
const constant = require(path.resolve('./commons/utils/constant'));
const { createSocketData } = require(path.resolve('./commons/utils/middleware'));
const { SOCKET_NOTIFICATION_PUSH } = require(path.resolve('./commons/modules/notifications/datas/socket.data'));
const { SOCKET_SELF_USER_UPDATE } = require(path.resolve('./commons/modules/users/datas/socket.data'));

const User = mongoose.model('User');
const Credit = mongoose.model('Credit');
const Notification = mongoose.model('Notification');

/**
 * @overview Chuyển khoản
 *
 * @param {Request} req
 * @param {Response} res
 * @param {Function} next
 *
 * @input body    {
 *                  transfer: {
 *                    password: String,
 *                    moneys: Number
 *                  }
 *                 }
 *        params  { id: String }
 * @output        {
 *                  code: String,
 *                  sockets?: [{Object}],
 *                  credit?: Credit
 *                }
 */
const transfer = (req, res, next) => {
  const bodyReq = req.body;
  const paramsReq = req.params;
  const userSelf = req.user;
  let dataRes = null;

  async.waterfall([
    (done) => {
      /* Kiểm tra đầu vào */

      if (userSelf) {
        User
          .findById(userSelf._id)
          .exec((err, user) => {
            if (err || !user) {
              // Chưa đăng nhập người dùng
              dataRes = {
                status: 401,
                bodyRes: {
                  code: 'user.signin.notExist'
                }
              };

              done(err || new Error('User not signin'), dataRes);
            } else if (user.authenticate(bodyReq.transfer.password)) {
              done(null);
            } else {
              // Đăng nhập người dùng thất bại
              dataRes = {
                status: 401,
                bodyRes: {
                  code: 'user.signin.failed'
                }
              };

              done(new Error('User failed signin'), dataRes);
            }
          });
      } else {
        // Chưa đăng nhập người dùng
        dataRes = {
          status: 401,
          bodyRes: {
            code: 'user.signin.notExist'
          }
        };

        done(new Error('User not signin'), dataRes);
      }
    }, (done) => {
      /* Kiểm tra tín dụng */

      if (userSelf.equalsByID(paramsReq.id)) {
        // Người dùng không có quyền
        dataRes = {
          status: 401,
          bodyRes: {
            code: 'user.permission.notExist'
          }
        };

        done(new Error('User self'), dataRes);
      } else {
        async.parallel({
          creditFrom: (callback) => {
            Credit
              .findOne({ userID: userSelf._id })
              .exec((err, credit) => {
                if (err || !credit) {
                  callback(err || new Error('Credit not exist'));
                } else {
                  callback(null, credit);
                }
              });
          },
          creditTo: (callback) => {
            Credit
              .findOne({
                $or: [
                  { userID: paramsReq.id },
                  { _id: paramsReq.id }
                ]
              })
              .exec((err, credit) => {
                if (err || !credit) {
                  callback(err || new Error('Credit not exist'));
                } else {
                  callback(null, credit);
                }
              });
          }
        }, (err, results) => {
          if (err) {
            // Tín dụng không tồn tại
            dataRes = {
              status: 404,
              bodyRes: {
                code: 'credit.notExist'
              }
            };

            done(err, dataRes);
          } else {
            done(null, results.creditFrom, results.creditTo);
          }
        });
      }
    }, (creditFrom, creditTo, done) => {
      /* Chuyển khoản */

      if (creditFrom.transfer(creditTo, bodyReq.transfer.moneys)) {
        async.parallel({
          creditFrom: (callback) => {
            creditFrom.save((err) => {
              callback(err);
            });
          },
          creditTo: (callback) => {
            creditTo.save((err) => {
              callback(err);
            });
          },
        }, (err, results) => {
          if (err) {
            // Chuyển khoản không thành công
            dataRes = {
              status: 400,
              bodyRes: {
                code: 'credit.transfer.failed'
              }
            };

            done(err, dataRes);
          } else {
            done(null, creditFrom, creditTo);
          }
        });
      } else {
        // Chuyển khoản không thành công
        dataRes = {
          status: 401,
          bodyRes: {
            code: 'credit.transfer.failed'
          }
        };

        done(new Error('Credit filed transfer'), dataRes);
      }
    }, (creditFrom, creditTo, done) => {
      /* Tạo thông báo */

      dataRes = {
        status: 200,
        bodyRes: {
          code: 'credit.transfer.success',
          credit: creditFrom.getFullInfo(),
          sockets: [
            createSocketData(
              [SOCKET_SELF_USER_UPDATE],
              creditTo.userID,
              {}
            )
          ]
        }
      };

      const notification = new Notification({
        receiveID: creditTo.userID,
        sendID: creditFrom.userID,
        toURL: creditTo._id,
        type: constant.CREDIT,
        result: constant.INFO,
        content: 'credit.transfer.exist'
      });

      notification.save((err) => {
        if (err) {
          logger.logError(
            'credits.authorization.server.controller',
            'transfer',
            constant.CREDIT,
            notification,
            err
          );
        } else {
          dataRes.bodyRes.sockets.push(
            createSocketData(
              [SOCKET_NOTIFICATION_PUSH],
              creditTo.userID,
              { notificationID: notification._id }
            )
          );
        }

        done(null, dataRes);
      });
    }], (err, dataRes) => {
      return res
        .status(dataRes.status)
        .send(dataRes.bodyRes)
        .end();
    });
};
module.exports.transfer = transfer;
