const React = require('react');
const classNames = require('classnames');
const propTypes = require('prop-types');
const joi = require('joi');
const { FormattedMessage } = require('react-intl');
const {
  Button,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  Slide,
} = require('@material-ui/core');
const { InputPassword } = require('../../../../users/client/components/Inputs');
const { InputNumber } = require('../../../../core/client/components/Inputs');
const { transfer } = require('../../../../../commons/modules/credits/validates/credits.validate');

const DialogTransfer = class DialogTransfer extends React.Component {
  constructor(props) {
    super(props);

    this.passwordRef = React.createRef();
    this.monyesRef = React.createRef();

    this.handleCheck = this.handleCheck.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.handleClickClose = this.handleClickClose.bind(this);
  }

  handleCheck(body, validate) {
    return joi.validate(body, validate);
  }

  handleClick(event) {
    const { validate, handleClickParent } = this.props;

    const { password } = this.passwordRef.current.state;
    const moneys = this.monyesRef.current.state.value;

    this.handleCheck({
      transfer: {
        password,
        moneys
      }
    }, validate)
      .then((body) => {
        handleClickParent(body);
      });
  }

  handleClickClose(event) {
    const { handleClickCloseParent } = this.props;

    handleClickCloseParent();
  }

  Transition(props) {
    return <Slide direction="up" {...props} />;
  }

  initElement() {
    const {
      className,
      open,
      disabled,
    } = this.props;

    return (
      <div className={classNames('xcg-dialog-transfer', className)}>
        <Dialog
          open={open}
          TransitionComponent={this.Transition}
        >
          <DialogTitle>
            <FormattedMessage id="credit.transfer.is" />
          </DialogTitle>
          <DialogContent>
            <InputNumber
              label={<FormattedMessage id="info.moneys.is" />}
              ref={this.monyesRef}
              disabled={disabled}
            />
            <InputPassword
              label={<FormattedMessage id="user.password.is" />}
              ref={this.passwordRef}
              disabled={disabled}
            />
          </DialogContent>
          <DialogActions>
            <Button
              onClick={this.handleClick}
              disabled={disabled}
              color="secondary"
            >
              <FormattedMessage id="actions.send" />
            </Button>
            <Button
              onClick={this.handleClickClose}
              disabled={disabled}
            >
              <FormattedMessage id="actions.back" />
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }

  render() {
    return this.initElement();
  }
};

DialogTransfer.propTypes = {
  className: propTypes.string,
  validate: propTypes.any,
  disabled: propTypes.bool,
  open: propTypes.bool.isRequired,
  handleClickParent: propTypes.func.isRequired,
  handleClickCloseParent: propTypes.func.isRequired
};

DialogTransfer.defaultProps = {
  className: '',
  disabled: false,
  validate: transfer.body
};

module.exports = DialogTransfer;
