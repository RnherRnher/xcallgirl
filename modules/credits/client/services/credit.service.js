const request = require('superagent');
const url = require('../../../../commons/modules/credits/datas/url.data');

const transferCredits = ({ csrfToken, userID, transfer }) => {
  return new Promise((resovle, reject) => {
    request
      .post(url.CREDITS_TRANSFER_ID.replace(':id', userID))
      .type('application/json')
      .accept('application/json')
      .set('X-Requested-With', 'XMLHttpRequest')
      .set('csrf-token', csrfToken)
      .send(transfer)
      .end((err, res) => {
        if (err) {
          return reject(res);
        }

        return resovle(res);
      });
  });
};
module.exports.transferCredits = transferCredits;
