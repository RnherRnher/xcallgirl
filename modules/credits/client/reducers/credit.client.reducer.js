const constant = require('../constants/credit.client.constant');
const { responseState } = require('../../../core/client/utils/middleware');
const { ResultEnum } = require('../../../core/client/constants/core.client.constant');
const { fromJS } = require('immutable');

const initialState = fromJS({
  actions: {
    transfer: responseState()
  }
});

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case constant.TRANSFER_CREDITS_ID_REQUEST: {
      return state.setIn(
        ['actions', 'transfer'],
        { result: ResultEnum.wait }
      );
    }
    case constant.TRANSFER_CREDITS_ID_SUCCESS: {
      return state.setIn(
        ['actions', 'transfer'],
        {
          result: ResultEnum.success,
          code: action.code
        }
      );
    }
    case constant.TRANSFER_CREDITS_ID_FAILED: {
      return state.setIn(
        ['actions', 'transfer'],
        {
          result: ResultEnum.failed,
          code: action.code
        }
      );
    }
    case constant.RESERT_CREDIT_STORE: {
      return initialState;
    }
    default:
      return state;
  }
};
module.exports = reducer;
