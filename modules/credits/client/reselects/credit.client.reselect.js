const { createSelector } = require('reselect');
const { makeSelect } = require('../../../core/client/utils/middleware');

const selectCredit = (state, props) => {
  return state.get('credit');
};

const makeSelectActions = (type) => {
  return createSelector(
    selectCredit,
    (creditState) => {
      return makeSelect(creditState.getIn(['actions', type]));
    }
  );
};
module.exports.makeSelectActions = makeSelectActions;
