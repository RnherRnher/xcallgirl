const nprogress = require('nprogress');
const api = require('../services/credit.service');
const action = require('../actions/credit.client.action');
const constant = require('../constants/credit.client.constant');
const constantCommon = require('../../../../commons/utils/constant');
const {
  takeLatest,
  put,
  call,
} = require('redux-saga/effects');
const { pushAlertSnackbar } = require('../../../core/client/actions/core.client.action');
const { csrfRequest } = require('../../../core/client/services/core.client.service');
const { updateProfile } = require('../../../users/client/actions/user.client.action');
const { emitSockets } = require('../../../core/client/lib/socket-io');
const { initApp } = require('../../../core/client/actions/app.client.action');

const watchTransferCredits = function* () {
  yield takeLatest(constant.TRANSFER_CREDITS_ID_REQUEST, function* ({ userID, transfer }) {
    try {
      nprogress.start();

      const responseCSRF = yield call(csrfRequest);
      const response = yield call(
        api.transferCredits,
        {
          csrfToken: responseCSRF.body.csrfToken,
          userID,
          transfer
        }
      );

      yield put(action.transferCreditsSuccess(response.body));
      yield put(pushAlertSnackbar({
        variant: constantCommon.SUCCESS,
        code: response.body.code
      }));

      yield put(updateProfile(constantCommon.CREDIT, response.body.credit));
      yield call(emitSockets, response.body.sockets);
    } catch (error) {
      if (error && error.body) {
        yield put(action.transferCreditsFailed(error.body));
        yield put(pushAlertSnackbar({
          variant: constantCommon.ERROR,
          code: error.body.code
        }));
      } else {
        yield put(initApp());
      }
    } finally {
      nprogress.done();
    }
  });
};
module.exports.watchTransferCredits = watchTransferCredits;
