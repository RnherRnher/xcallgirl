const { action } = require('../../../core/client/utils/middleware');
const constant = require('../constants/credit.client.constant');

/**
 * @override Yêu cầu chuyển khoản theo id
 *
 * @param   {ObjectId}  userID
 * @param   {Object}    transfer
 * @returns {Object}
 */
const transferCreditsRequest = (userID, transfer) => {
  return action(constant.TRANSFER_CREDITS_ID_REQUEST, { userID, transfer });
};
module.exports.transferCreditsRequest = transferCreditsRequest;

/**
 * @override Chuyển khoản theo id thành công
 *
 * @param   {Object}
 * @returns {Object}
 */
const transferCreditsSuccess = ({ code, credit }) => {
  return action(constant.TRANSFER_CREDITS_ID_SUCCESS, { code, credit });
};
module.exports.transferCreditsSuccess = transferCreditsSuccess;

/**
 * @override Chuyển khoản theo id thất bại
 *
 * @param   {Object}
 * @returns {Object}
 */
const transferCreditsFailed = ({ code }) => {
  return action(constant.TRANSFER_CREDITS_ID_FAILED, { code });
};
module.exports.transferCreditsFailed = transferCreditsFailed;

/**
 * @override Thiết lặp lại cửa hàng
 *
 * @param   {Object}
 */
const resetCreditStore = () => {
  return action(constant.RESERT_CREDIT_STORE);
};
module.exports.resetCreditStore = resetCreditStore;
