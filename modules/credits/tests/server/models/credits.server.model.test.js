const path = require('path');
const mongoose = require('mongoose');
const chai = require('chai');
const constant = require(path.resolve('./commons/utils/constant'));

describe('### Credits Server Model Tests', () => {
  let Credit = null;
  let credit = null;

  before((done) => {
    Credit = mongoose.model('Credit');

    done();
  });

  beforeEach((done) => {
    credit = {
      userID: mongoose.Types.ObjectId(),
      balance: 0,
      updateDate: Date.now(),
    };

    done();
  });

  afterEach((done) => {
    Credit.deleteMany({}, (err) => {
      chai.assert.notExists(err);
      done();
    });
  });

  it('should success to save', (done) => {
    const _credit = new Credit(credit);
    _credit.save((err) => {
      chai.assert.notExists(err);
      done();
    });
  });

  it('should failed to save with not userID', (done) => {
    const _credit = new Credit(credit);
    _credit.userID = undefined;

    _credit.save((err) => {
      chai.assert.exists(err);
      done();
    });
  });

  it('should failed to save with not balance', (done) => {
    const _credit = new Credit(credit);
    _credit.balance = undefined;

    _credit.save((err) => {
      chai.assert.exists(err);
      done();
    });
  });

  it('should failed to save with not updateDate', (done) => {
    const _credit = new Credit(credit);
    _credit.updateDate = undefined;

    _credit.save((err) => {
      chai.assert.exists(err);
      done();
    });
  });

  it('should failed to save with balance= -1', (done) => {
    const _credit = new Credit(credit);
    _credit.balance = -1;

    _credit.save((err) => {
      chai.assert.exists(err);
      done();
    });
  });

  describe('## Thêm lịch sử  thẻ', () => {
    it('should success add historys', (done) => {
      const _credit = new Credit(credit);
      _credit.historys.push({
        userID: mongoose.Types.ObjectId(),
        type: constant.GET,
        balance: 0,
        initDate: Date.now()
      });

      _credit.save((err) => {
        chai.assert.notExists(err);
        done();
      });
    });

    it('should failed add historys with not userID', (done) => {
      const _credit = new Credit(credit);
      _credit.historys.push({
        type: constant.GET,
        balance: 0,
        initDate: Date.now()
      });

      _credit.save((err) => {
        chai.assert.exists(err);
        done();
      });
    });

    it('should failed add historys with not type', (done) => {
      const _credit = new Credit(credit);
      _credit.historys.push({
        userID: mongoose.Types.ObjectId(),
        balance: 0,
        initDate: Date.now()
      });

      _credit.save((err) => {
        chai.assert.exists(err);
        done();
      });
    });

    it('should failed add historys with not balance', (done) => {
      const _credit = new Credit(credit);
      _credit.historys.push({
        userID: mongoose.Types.ObjectId(),
        type: constant.GET,
        initDate: Date.now()
      });

      _credit.save((err) => {
        chai.assert.exists(err);
        done();
      });
    });

    it('should failed add historys with not initDate', (done) => {
      const _credit = new Credit(credit);
      _credit.historys.push({
        userID: mongoose.Types.ObjectId(),
        type: constant.GET,
        balance: 0,
      });

      _credit.save((err) => {
        chai.assert.exists(err);
        done();
      });
    });

    it('should failed add historys with balance=-1', (done) => {
      const _credit = new Credit(credit);
      _credit.historys.push({
        userID: mongoose.Types.ObjectId(),
        type: constant.GET,
        balance: -1,
        initDate: Date.now()
      });

      _credit.save((err) => {
        chai.assert.exists(err);
        done();
      });
    });

    it('should failed add historys with failed type', (done) => {
      const _credit = new Credit(credit);
      _credit.historys.push({
        userID: mongoose.Types.ObjectId(),
        type: constant.DELETE,
        balance: 0,
        initDate: Date.now()
      });

      _credit.save((err) => {
        chai.assert.exists(err);
        done();
      });
    });
  });
});
