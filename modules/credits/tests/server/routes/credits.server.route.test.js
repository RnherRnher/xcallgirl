const path = require('path');
const mongoose = require('mongoose');
const request = require('supertest');
const chai = require('chai');
const async = require('async');
const express = require(path.resolve('./config/lib/express'));
const creditsUrl = require(path.resolve('./commons/modules/credits/datas/url.data'));
const constant = require(path.resolve('./commons/utils/constant'));
const utils = require(path.resolve('./utils/test'));

describe('### Credits Server Router Tests', () => {
  let app = null;
  let agent = null;

  let Credit = null;
  let credit1 = null;
  let credit2 = null;
  const balance = 100;
  const balanceTransfer = 10;

  let Notification = null;

  let User = null;
  let user = null;
  const userID = mongoose.Types.ObjectId();
  const password = 'password';

  /**
 * @overview Kiểm tra chuyển khoản
 *
 * @param {Function} done
 * @param {Boolean} isExist
 */
  const testTransfer = (done, isExist = true) => {
    async.parallel({
      creditFrom: (callback) => {
        Credit
          .findOne({ userID: user._id })
          .exec((err, credit) => {
            if (err || !credit) {
              callback(err || new Error('Credit not exist'));
            } else {
              callback(null, credit);
            }
          });
      },
      creditTo: (callback) => {
        Credit
          .findOne({ userID })
          .exec((err, credit) => {
            if (err || !credit) {
              callback(err || new Error('Credit not exist'));
            } else {
              callback(null, credit);
            }
          });
      }
    }, (err, results) => {
      if (err) {
        done(err);
      } else {
        Notification
          .findOne({
            receiveID: results.creditTo.userID,
            sendID: results.creditFrom.userID,
            type: constant.CREDIT,
            result: constant.INFO,
            content: 'credit.transfer.exist'
          })
          .exec((err, notification) => {
            chai.assert.notExists(err);

            if (isExist) {
              chai.assert.exists(notification);

              chai.assert.deepEqual(results.creditFrom.balance, balance - balanceTransfer);
              chai.assert.deepEqual(results.creditFrom.historys.length, 1);

              chai.assert.deepEqual(results.creditTo.balance, balanceTransfer);
              chai.assert.deepEqual(results.creditTo.historys.length, 1);
            } else {
              chai.assert.notExists(notification);

              chai.assert.deepEqual(results.creditFrom.balance, balance);
              chai.assert.deepEqual(results.creditFrom.historys.length, 0);

              chai.assert.deepEqual(results.creditTo.balance, 0);
              chai.assert.deepEqual(results.creditTo.historys.length, 0);
            }

            if (done) done();
          });
      }
    });
  };

  before((done) => {
    app = express.init(mongoose.connection.db);
    agent = request.agent(app);

    Credit = mongoose.model('Credit');
    User = mongoose.model('User');
    Notification = mongoose.model('Notification');

    done();
  });

  beforeEach((done) => {
    user = new User({
      name: {
        first: 'first',
        last: 'last',
        display: 'last first',
        nick: 'user_user',
      },
      numberPhone: {
        code: '+84',
        number: '0000000000'
      },
      gender: constant.MALE,
      birthday: {
        day: 1,
        month: 1,
        year: 2000,
      },
      password,
      address: {
        country: 'country',
        city: 'city',
        county: 'county',
        local: 'local',
      },
      settings: {
        general: {
          language: constant.VI,
        },
      },
    });

    credit1 = new Credit({
      userID: user._id,
      balance,
      updateDate: Date.now(),
    });

    credit2 = new Credit({
      userID,
      balance: 0,
      updateDate: Date.now(),
    });


    user.save((err) => {
      chai.assert.notExists(err);
      credit1.save((err) => {
        chai.assert.notExists(err);
        credit2.save((err) => {
          chai.assert.notExists(err);
          done();
        });
      });
    });
  });

  afterEach((done) => {
    User.deleteMany({}, (err) => {
      chai.assert.notExists(err);
      Credit.deleteMany({}, (err) => {
        chai.assert.notExists(err);
        done();
      });
    });
  });

  describe('## Chuyển khoản', () => {
    it('should success transfer', (done) => {
      utils.signin(agent, user.numberPhone, password)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.post(creditsUrl.CREDITS_TRANSFER_ID.replace(':id', userID))
            .set('X-Requested-With', 'XMLHttpRequest')
            .send({
              transfer: {
                password,
                moneys: balanceTransfer
              }
            })
            .expect(200)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              testTransfer(done);
            });
        });
    });

    it('should failed transfer with not signin', (done) => {
      agent.post(creditsUrl.CREDITS_TRANSFER_ID.replace(':id', userID))
        .set('X-Requested-With', 'XMLHttpRequest')
        .send({
          transfer: {
            password,
            moneys: balanceTransfer
          }
        })
        .expect(401)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          chai.assert.deepEqual(res.body.code, 'user.permission.notExist');
          testTransfer(done, false);
        });
    });

    it('should failed transfer with bad body', (done) => {
      utils.signin(agent, user.numberPhone, password)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.post(creditsUrl.CREDITS_TRANSFER_ID.replace(':id', userID))
            .set('X-Requested-With', 'XMLHttpRequest')
            .send({
              transfer: {
                password,
              }
            })
            .expect(400)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.code, 'data.notVaild');
              testTransfer(done, false);
            });
        });
    });

    it('should failed transfer with user not authenticate', (done) => {
      utils.signin(agent, user.numberPhone, password)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.post(creditsUrl.CREDITS_TRANSFER_ID.replace(':id', userID))
            .set('X-Requested-With', 'XMLHttpRequest')
            .send({
              transfer: {
                password: 'somepassword',
                moneys: balanceTransfer
              }
            })
            .expect(401)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.code, 'user.signin.failed');
              testTransfer(done, false);
            });
        });
    });

    it('should failed transfer with self userID', (done) => {
      utils.signin(agent, user.numberPhone, password)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.post(creditsUrl.CREDITS_TRANSFER_ID.replace(':id', user._id))
            .set('X-Requested-With', 'XMLHttpRequest')
            .send({
              transfer: {
                password,
                moneys: balanceTransfer
              }
            })
            .expect(401)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.code, 'user.permission.notExist');
              testTransfer(done, false);
            });
        });
    });

    it('should failed transfer with bad userID', (done) => {
      utils.signin(agent, user.numberPhone, password)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.post(creditsUrl.CREDITS_TRANSFER_ID.replace(':id', mongoose.Types.ObjectId()))
            .set('X-Requested-With', 'XMLHttpRequest')
            .send({
              transfer: {
                password,
                moneys: balanceTransfer
              }
            })
            .expect(404)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.code, 'credit.notExist');
              testTransfer(done, false);
            });
        });
    });

    it('should failed transfer with limit moneys', (done) => {
      utils.signin(agent, user.numberPhone, password)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.post(creditsUrl.CREDITS_TRANSFER_ID.replace(':id', userID))
            .set('X-Requested-With', 'XMLHttpRequest')
            .send({
              transfer: {
                password,
                moneys: balanceTransfer + balance
              }
            })
            .expect(401)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.code, 'credit.transfer.failed');
              testTransfer(done, false);
            });
        });
    });

    it('should failed transfer with vaild moneys= 0', (done) => {
      utils.signin(agent, user.numberPhone, password)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.post(creditsUrl.CREDITS_TRANSFER_ID.replace(':id', userID))
            .set('X-Requested-With', 'XMLHttpRequest')
            .send({
              transfer: {
                password,
                moneys: 0
              }
            })
            .expect(401)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.code, 'credit.transfer.failed');
              testTransfer(done, false);
            });
        });
    });

    it('should failed transfer with vaild moneys= -1', (done) => {
      utils.signin(agent, user.numberPhone, password)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.post(creditsUrl.CREDITS_TRANSFER_ID.replace(':id', userID))
            .set('X-Requested-With', 'XMLHttpRequest')
            .send({
              transfer: {
                password,
                moneys: -1
              }
            })
            .expect(400)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.code, 'data.notVaild');
              testTransfer(done, false);
            });
        });
    });
  });
});
