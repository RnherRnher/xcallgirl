const path = require('path');
const mongoose = require('mongoose');
const request = require('supertest');
const chai = require('chai');
const express = require(path.resolve('./config/lib/express'));
const activityHistorysUrl = require(path.resolve('./commons/modules/activity-historys/datas/url.data'));
const constant = require(path.resolve('./commons/utils/constant'));
const utils = require(path.resolve('./utils/test'));

describe('### Activity Historys Server Router Tests', () => {
  let app = null;
  let agent = null;

  let ActivityHistory = null;
  let activityHistory1 = null;
  let activityHistory2 = null;
  let activityHistory3 = null;

  let User = null;
  let user = null;
  const password = 'password';

  before((done) => {
    app = express.init(mongoose.connection.db);
    agent = request.agent(app);

    ActivityHistory = mongoose.model('ActivityHistory');
    User = mongoose.model('User');

    done();
  });

  beforeEach((done) => {
    user = new User({
      name: {
        first: 'first',
        last: 'last',
        display: 'last first',
        nick: 'user_user',
      },
      numberPhone: {
        code: '+84',
        number: '0000000000'
      },
      gender: constant.MALE,
      birthday: {
        day: 1,
        month: 1,
        year: 2000,
      },
      password,
      address: {
        country: 'country',
        city: 'city',
        county: 'county',
        local: 'local',
      },
      settings: {
        general: {
          language: constant.VI,
        },
      },
    });

    activityHistory1 = new ActivityHistory({
      userID: user._id,
      toURL: user._id,
      type: constant.USER,
      result: constant.SUCCESS,
      content: 'user.exist',
    });

    activityHistory2 = new ActivityHistory({
      userID: user._id,
      toURL: user._id,
      type: constant.USER,
      result: constant.SUCCESS,
      content: 'user.exist',
    });

    activityHistory3 = new ActivityHistory({
      userID: mongoose.Types.ObjectId(),
      toURL: mongoose.Types.ObjectId(),
      type: constant.USER,
      result: constant.SUCCESS,
      content: 'user.exist',
    });

    user.save((err) => {
      chai.assert.notExists(err);
      activityHistory1.save((err) => {
        chai.assert.notExists(err);
        activityHistory2.save((err) => {
          chai.assert.notExists(err);
          activityHistory3.save((err) => {
            chai.assert.notExists(err);
            done();
          });
        });
      });
    });
  });

  afterEach((done) => {
    User.deleteMany({}, (err) => {
      chai.assert.notExists(err);
      ActivityHistory.deleteMany({}, (err) => {
        chai.assert.notExists(err);
        done();
      });
    });
  });

  describe('## Tìm kiếm lịch sử hoạt động', () => {
    it('should success auto search activity history', (done) => {
      utils.signin(agent, user.numberPhone, password)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.get(activityHistorysUrl.ACTIVITY_HISTORYS_SEARCH)
            .set('X-Requested-With', 'XMLHttpRequest')
            .query({
              type: constant.AUTO,
              skip: 0,
            })
            .expect(200)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.skip, 2);
              chai.assert.deepEqual(res.body.activityHistorys.length, 2);

              return done();
            });
        });
    });

    it('should success auto search activity history. But not exist', (done) => {
      ActivityHistory.deleteMany({}, (err) => {
        chai.assert.notExists(err);

        utils.signin(agent, user.numberPhone, password)
          .expect(200)
          .end((err, res) => {
            if (err) {
              return done(err);
            }

            agent.get(activityHistorysUrl.ACTIVITY_HISTORYS_SEARCH)
              .set('X-Requested-With', 'XMLHttpRequest')
              .query({
                type: constant.AUTO,
                skip: 0,
              })
              .expect(404)
              .end((err, res) => {
                if (err) {
                  return done(err);
                }

                chai.assert.deepEqual(res.body.code, 'activityHistory.notExist');

                return done();
              });
          });
      });
    });

    it('should failed auto search activity history with not signin', (done) => {
      agent.get(activityHistorysUrl.ACTIVITY_HISTORYS_SEARCH)
        .set('X-Requested-With', 'XMLHttpRequest')
        .query({
          type: constant.AUTO,
          skip: 0,
        })
        .expect(401)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          chai.assert.deepEqual(res.body.code, 'user.permission.notExist');
          return done();
        });
    });

    it('should failed auto search activity history with bad query', (done) => {
      utils.signin(agent, user.numberPhone, password)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.get(activityHistorysUrl.ACTIVITY_HISTORYS_SEARCH)
            .set('X-Requested-With', 'XMLHttpRequest')
            .expect(400)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.code, 'data.notVaild');
              return done();
            });
        });
    });
  });
});
