const path = require('path');
const mongoose = require('mongoose');
const chai = require('chai');
const constant = require(path.resolve('./commons/utils/constant'));

describe('### Activity Historys Server Model Tests', () => {
  let ActivityHistory = null;
  let activityHistory = null;

  before((done) => {
    ActivityHistory = mongoose.model('ActivityHistory');

    done();
  });

  beforeEach((done) => {
    activityHistory = {
      userID: mongoose.Types.ObjectId(),
      toURL: mongoose.Types.ObjectId(),
      type: constant.USER,
      result: constant.SUCCESS,
      content: 'user.exist',
    };

    done();
  });

  afterEach((done) => {
    ActivityHistory.deleteMany({}, (err) => {
      chai.assert.notExists(err);
      done();
    });
  });

  it('should success to save', (done) => {
    const _activityHistory = new ActivityHistory(activityHistory);
    _activityHistory.save((err) => {
      chai.assert.notExists(err);
      done();
    });
  });

  it('should failed to save with not userID', (done) => {
    const _activityHistory = new ActivityHistory(activityHistory);
    _activityHistory.userID = undefined;

    _activityHistory.save((err) => {
      chai.assert.exists(err);
      done();
    });
  });

  it('should failed to save with not type', (done) => {
    const _activityHistory = new ActivityHistory(activityHistory);
    _activityHistory.type = undefined;

    _activityHistory.save((err) => {
      chai.assert.exists(err);
      done();
    });
  });

  it('should failed to save with not result', (done) => {
    const _activityHistory = new ActivityHistory(activityHistory);
    _activityHistory.result = undefined;

    _activityHistory.save((err) => {
      chai.assert.exists(err);
      done();
    });
  });

  it('should failed to save with not toURL', (done) => {
    const _activityHistory = new ActivityHistory(activityHistory);
    _activityHistory.toURL = undefined;

    _activityHistory.save((err) => {
      chai.assert.exists(err);
      done();
    });
  });

  it('should failed to save with not content', (done) => {
    const _activityHistory = new ActivityHistory(activityHistory);
    _activityHistory.content = undefined;

    _activityHistory.save((err) => {
      chai.assert.exists(err);
      done();
    });
  });

  it('should failed to save with bad type', (done) => {
    const _activityHistory = new ActivityHistory(activityHistory);
    _activityHistory.type = 'some_type';

    _activityHistory.save((err) => {
      chai.assert.exists(err);
      done();
    });
  });

  it('should failed to save with bad result', (done) => {
    const _activityHistory = new ActivityHistory(activityHistory);
    _activityHistory.result = 'some_result';

    _activityHistory.save((err) => {
      chai.assert.exists(err);
      done();
    });
  });
});
