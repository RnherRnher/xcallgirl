const { createSelector } = require('reselect');
const { makeSelect } = require('../../../core/client/utils/middleware');

const selectActivityHistory = (state, props) => {
  return state.get('activityHistory');
};

const makeSelectSelfActivityHistorys = () => {
  return createSelector(
    selectActivityHistory,
    (activityHistorylState) => {
      return makeSelect(activityHistorylState.get('self'));
    }
  );
};
module.exports.makeSelectSelfActivityHistorys = makeSelectSelfActivityHistorys;
