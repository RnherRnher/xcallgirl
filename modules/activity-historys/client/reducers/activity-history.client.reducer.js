const constant = require('../constants/activity-history.client.constant');
const { responseState } = require('../../../core/client/utils/middleware');
const { ResultEnum } = require('../../../core/client/constants/core.client.constant');
const { fromJS } = require('immutable');

const initialState = fromJS({
  self: responseState({ data: [] })
});

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case constant.GET_ACTIVITY_HISTORYS_REQUEST: {
      return state
        .setIn(['self', 'result'], ResultEnum.wait)
        .setIn(['self', 'skip'], action.skip)
        .setIn(['self', 'fragment'], action.fragment)
        .setIn(['self', 'reload'], action.reload)
        .setIn(['self', 'code'], undefined);
    }
    case constant.GET_ACTIVITY_HISTORYS_SUCCESS: {
      let _state = state
        .setIn(['self', 'result'], ResultEnum.success)
        .setIn(['self', 'skip'], action.skip)
        .setIn(['self', 'code'], action.code);

      if (_state.getIn(['self', 'reload'])) {
        _state = _state.setIn(
          ['self', 'data'],
          action.activityHistorys
        );
      } else {
        _state = _state.updateIn(
          ['self', 'data'],
          (data) => {
            return data.concat(action.activityHistorys);
          }
        );
      }

      return _state;
    }
    case constant.GET_ACTIVITY_HISTORYS_FAILED: {
      if (state.getIn(['self', 'reload'])) {
        return state.set(
          'self',
          {
            result: ResultEnum.failed,
            code: action.code,
            data: []
          }
        );
      }

      return state
        .setIn(['self', 'result'], ResultEnum.failed)
        .setIn(['self', 'code'], action.code);
    }
    case constant.RESERT_ACTIVITY_HISTORY_STORE: {
      return initialState;
    }
    default:
      return state;
  }
};
module.exports = reducer;
