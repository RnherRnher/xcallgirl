const { action } = require('../../../core/client/utils/middleware');
const constant = require('../constants/activity-history.client.constant');

/**
 * @override Yêu cầu lấy lịch sử hoạt động
 *
 * @param   {String}  fragment
 * @param   {Number}  skip
 * @param   {Boolean} reload
 * @returns {Object}
 */
const getActivityHistorysRequest = (
  fragment,
  skip,
  reload = false
) => {
  return action(
    constant.GET_ACTIVITY_HISTORYS_REQUEST,
    {
      fragment,
      skip: reload ? 0 : (skip || 0),
      reload
    }
  );
};
module.exports.getActivityHistorysRequest = getActivityHistorysRequest;

/**
 * @override Lấy lịch sử hoạt động thành công
 *
 * @param   {Object}
 * @returns {Object}
 */
const getActivityHistorysSuccess = ({
  code,
  skip,
  activityHistorys
}) => {
  return action(
    constant.GET_ACTIVITY_HISTORYS_SUCCESS,
    {
      code,
      skip,
      activityHistorys
    }
  );
};
module.exports.getActivityHistorysSuccess = getActivityHistorysSuccess;

/**
 * @override Lấy lịch sử hoạt động thất bại
 *
 * @param   {Object}
 * @returns {Object}
 */
const getActivityHistorysFailed = ({ code }) => {
  return action(constant.GET_ACTIVITY_HISTORYS_FAILED, { code });
};
module.exports.getActivityHistorysFailed = getActivityHistorysFailed;

/**
 * @override Thiết lặp lại cửa hàng
 *
 * @param   {Object}
 */
const resetActivityHistoryStore = () => {
  return action(constant.RESERT_ACTIVITY_HISTORY_STORE);
};
module.exports.resetActivityHistoryStore = resetActivityHistoryStore;
