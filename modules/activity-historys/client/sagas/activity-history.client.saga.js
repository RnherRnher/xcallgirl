const nprogress = require('nprogress');
const api = require('../services/activity-history.service');
const action = require('../actions/activity-history.client.action');
const constant = require('../constants/activity-history.client.constant');
const constantCommon = require('../../../../commons/utils/constant');
const {
  takeLatest,
  put,
  call,
} = require('redux-saga/effects');
const { pushAlertSnackbar } = require('../../../core/client/actions/core.client.action');
const { initApp } = require('../../../core/client/actions/app.client.action');

const watchGetActivityHistorys = function* () {
  yield takeLatest(constant.GET_ACTIVITY_HISTORYS_REQUEST, function* ({ fragment, skip }) {
    try {
      nprogress.start();

      const response = yield call(
        api.getActivityHistorys,
        {
          type: fragment,
          skip
        }
      );

      yield put(action.getActivityHistorysSuccess(response.body));
    } catch (error) {
      if (error && error.body) {
        yield put(action.getActivityHistorysFailed(error.body));
        if (skip !== 0) {
          yield put(pushAlertSnackbar({
            variant: constantCommon.ERROR,
            code: error.body.code
          }));
        }
      } else {
        yield put(initApp());
      }
    } finally {
      nprogress.done();
    }
  });
};
module.exports.watchGetActivityHistorys = watchGetActivityHistorys;
