const request = require('superagent');
const url = require('../../../../commons/modules/activity-historys/datas/url.data');

const getActivityHistorys = ({ type, skip }) => {
  return new Promise((resovle, reject) => {
    request
      .get(url.ACTIVITY_HISTORYS_SEARCH)
      .accept('application/json')
      .set('X-Requested-With', 'XMLHttpRequest')
      .query({ type, skip })
      .end((err, res) => {
        if (err) {
          return reject(res);
        }

        return resovle(res);
      });
  });
};
module.exports.getActivityHistorys = getActivityHistorys;
