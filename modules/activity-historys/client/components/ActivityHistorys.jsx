const React = require('react');
const propTypes = require('prop-types');
const classNames = require('classnames');
const constant = require('../../../../commons/utils/constant');
const ActivityHistory = require('./ActivityHistory');
const Loadable = require('./Loadable');
const { connect } = require('react-redux');
const { createStructuredSelector } = require('reselect');
const { WindowScroll } = require('../../../core/client/components/Scrolls');
const { getActivityHistorysRequest, resetActivityHistoryStore } = require('../actions/activity-history.client.action');
const { makeSelectSelfActivityHistorys } = require('../reselects/activity-history.client.reselect');

const ActivityHistorys = class ActivityHistorys extends React.Component {
  constructor(props) {
    super(props);

    this.handleLoad = this.handleLoad.bind(this);
  }

  componentWillUnmount() {
    const { resetActivityHistoryStore } = this.props;

    resetActivityHistoryStore();
  }

  handleLoad(reload) {
    const { getActivityHistorysRequest, activityHistorys } = this.props;

    getActivityHistorysRequest(
      constant.AUTO,
      activityHistorys.skip,
      reload
    );
  }

  initElement() {
    const {
      className,
      activityHistorys,
      rowHeight
    } = this.props;

    return (
      <div className={classNames('xcg-activity-historys', className)}>
        <WindowScroll
          result={activityHistorys.result}
          handleLoadMore={this.handleLoad}
          initialLoad={!activityHistorys.data.length}
          value={activityHistorys.data}
          Children={ActivityHistory}
          Loadable={Loadable}
          rowHeight={rowHeight}
        />
      </div>
    );
  }

  render() {
    return this.initElement();
  }
};

ActivityHistorys.propTypes = {
  className: propTypes.string,
  rowHeight: propTypes.number,
  activityHistorys: propTypes.object.isRequired,
  getActivityHistorysRequest: propTypes.func.isRequired,
  resetActivityHistoryStore: propTypes.func.isRequired
};

ActivityHistorys.defaultProps = {
  className: '',
  rowHeight: 76.5

};

function mapStateToProps(state, props) {
  return createStructuredSelector({
    activityHistorys: makeSelectSelfActivityHistorys(),
  });
}

function mapDispatchToProps(dispatch, props) {
  return {
    getActivityHistorysRequest: (
      fragment,
      skip,
      reload
    ) => {
      return dispatch(getActivityHistorysRequest(
        fragment,
        skip,
        reload
      ));
    },
    resetActivityHistoryStore: () => {
      return dispatch(resetActivityHistoryStore());
    }
  };
}

module.exports = connect(
  mapStateToProps,
  mapDispatchToProps,
)(ActivityHistorys);
