const React = require('react');
const propTypes = require('prop-types');
const classNames = require('classnames');
const constant = require('../../../../commons/utils/constant');
const url = require('../../../core/client/utils/url');
const { FormattedMessage } = require('react-intl');
const { push } = require('connected-react-router/immutable');
const { connect } = require('react-redux');
const {
  CheckCircle,
  Warning,
  Error,
  Info,
  Store,
  Whatshot,
  Person,
  ShoppingCart,
  Chat
} = require('@material-ui/icons');
const {
  ListItem,
  Avatar,
  ListItemAvatar,
  ListItemText,
  ListItemIcon
} = require('@material-ui/core');
const { UIDate } = require('../../../core/client/components/UIs');

const ActivityHistory = class ActivityHistory extends React.Component {
  constructor(props) {
    super(props);

    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(event) {
    const { redirect, value } = this.props;

    switch (value.type) {
      case constant.BUSINESS_CARD:
        redirect(`${url.BUSINESS_CARD_GET}?id=${value.toURL}`);
        break;
      case constant.BUSINESS_CARD_DEAL_HISTORY:
        redirect(`${url.DEAL_HISTORY_GET}?id=${value.toURL}`);
        break;
      case constant.USER:
        redirect(`${url.USER_GET}?id=${value.toURL}`);
        break;
      case constant.CHAT:
        redirect(`${url.CHAT_GET}?id=${value.toURL}`);
        break;
      default:
        break;
    }
  }

  Icon() {
    const { value } = this.props;

    let Icon = null;
    switch (value.result) {
      case constant.FAILED:
        Icon = <Error className="xcg-error" />;
        break;
      case constant.SUCCESS:
        Icon = <CheckCircle className="xcg-success" />;
        break;
      case constant.WARNING:
        Icon = <Warning className="xcg-warning" />;
        break;
      case constant.INFO:
        Icon = <Info className="xcg-info" />;
        break;
      default:
        break;
    }

    return Icon;
  }

  Type() {
    const { value } = this.props;

    let Type = null;
    switch (value.type) {
      case constant.USER:
        Type = <Person />;
        break;
      case constant.ADMIN:
        Type = <Whatshot />;
        break;
      case constant.BUSINESS_CARD_DEAL_HISTORY:
        Type = <ShoppingCart />;
        break;
      case constant.BUSINESS_CARD:
        Type = <Store />;
        break;
      case constant.CHAT:
        Type = <Chat />;
        break;
      default:
        break;
    }

    return Type;
  }

  initElement() {
    const { className, value } = this.props;

    return (
      <div className={classNames('xcg-activity-history', className)}>
        <ListItem
          button
          onClick={this.handleClick}
        >
          <ListItemAvatar>
            <Avatar>
              {this.Type()}
            </Avatar>
          </ListItemAvatar>
          <ListItemText
            inset
            primary={<FormattedMessage id={value.content} />}
            secondary={<UIDate value={value.initDate} />}
          />
          <ListItemIcon>
            {this.Icon()}
          </ListItemIcon>
        </ListItem>
      </div>
    );
  }

  render() {
    return this.initElement();
  }
};

ActivityHistory.propTypes = {
  className: propTypes.string,
  value: propTypes.object.isRequired,
  redirect: propTypes.func.isRequired
};

ActivityHistory.defaultProps = {
  className: ''
};

function mapStateToProps(state, props) {
  return {};
}

function mapDispatchToProps(dispatch, props) {
  return {
    redirect: (location) => {
      return dispatch(push(location));
    }
  };
}

module.exports = connect(
  mapStateToProps,
  mapDispatchToProps,
)(ActivityHistory);
