const mongoose = require('mongoose');
const path = require('path');
const constant = require(path.resolve('./commons/utils/constant'));

const ActivityHistory = mongoose.model('ActivityHistory');

/**
 *  @overview Tìm kiếm lịch sử hoạt động
 *
 * @param {Request} req
 * @param {Response} res
 * @param {Function} next
 *
 * @input query { type: String }
 * @output      { skip?: Number, activityHistorys?: [{ActivityHistory}] }
 */
const search = (req, res, next) => {
  const queryReq = req.query;
  let dataRes = null;

  /**
 * @overview Tìm kiếm lịch sử hoạt động tự động
 *
 * @param {Request} req
 * @param {Response} res
 * @param {Function} next
 *
 * @input query { skip: Number }
 * @output      {
 *                code: String,
 *                skip?: Number,
 *                activityHistorys?: [{ActivityHistory}]
 *              }
 */
  const autoSearch = (req, res, next) => {
    const queryReq = req.query;
    const userSelf = req.user;
    let dataRes = null;

    const limit = req.app.locals.config.searchLimit;
    const currentSkip = queryReq.skip > 0 ? queryReq.skip : 0;

    if (userSelf) {
      /* Tìm thông báo */

      ActivityHistory
        .find({ userID: userSelf._id })
        .sort({ initDate: -1 })
        .skip(currentSkip)
        .limit(limit)
        .exec((err, activityHistorys) => {
          if (err) {
            // Bad request.body
            dataRes = {
              status: 400,
              bodyRes: {
                code: 'data.notVaild',
              },
            };
          } else if (activityHistorys.length) {
            dataRes = {
              status: 200,
              bodyRes: {
                skip: currentSkip + (activityHistorys.length < limit ? activityHistorys.length : limit),
                activityHistorys,
                code: 'activityHistory.exist',
              },
            };
          } else {
            // Lịch sử hoạt động không tồn tại
            dataRes = {
              status: 404,
              bodyRes: {
                code: 'activityHistory.notExist',
              },
            };
          }

          return res
            .status(dataRes.status)
            .send(dataRes.bodyRes)
            .end();
        });
    } else {
      // Chưa đăng nhập người dùng
      dataRes = {
        status: 401,
        bodyRes: {
          code: 'user.signin.notExist',
        },
      };

      return res
        .status(dataRes.status)
        .send(dataRes.bodyRes)
        .end();
    }
  };

  switch (queryReq.type) {
    case constant.AUTO:
      autoSearch(req, res, next);
      break;
    default: {
      dataRes = {
        status: 400,
        bodyRes: {
          code: 'data.notVaild',
        },
      };

      return res
        .status(dataRes.status)
        .send(dataRes.bodyRes)
        .end();
    }
  }
};
module.exports.search = search;
