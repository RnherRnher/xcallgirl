
const path = require('path');
const policy = require('../policies/activity-history.server.policy');
const activityHistory = require('../controllers/activity-history.server.controller');
const validate = require(path.resolve('./commons/modules/activity-historys/validates/activity-history.validate'));
const middleware = require(path.resolve('./utils/middleware'));
const url = require(path.resolve('./commons/modules/activity-historys/datas/url.data'));

const init = (app) => {
  app.route(url.ACTIVITY_HISTORYS_SEARCH)
    .all(policy.isAllowed)
    .get(middleware.paramsValidation(validate, 'search'), activityHistory.search);
};
module.exports = init;
