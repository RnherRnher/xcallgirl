const path = require('path');
const chalk = require('chalk');
const mongoose = require('mongoose');
const regular = require(path.resolve('./commons/utils/regular-expression'));
const { validateModelActivityHistory } = require(path.resolve('./utils/validate'));

const { Schema } = mongoose;

const validates = validateModelActivityHistory();

const ActivityHistorySchema = new Schema({
  userID: {
    type: mongoose.SchemaTypes.ObjectId,
    required: true,
  },
  toURL: {
    type: String,
    required: true,
    maxlength: regular.commons.stringLength.medium,
  },
  type: {
    type: String,
    required: true,
    enum: validates.type,
  },
  result: {
    type: String,
    required: true,
    enum: validates.result,
  },
  content: {
    type: String,
    required: true,
    maxlength: regular.commons.stringLength.medium,
  },
  initDate: {
    type: Date,
    required: true,
    default: Date.now
  },
});

/**
 * @overview Tạo mẫu cơ sở dữ liệu
 *
 * @param {Object} doc
 * @param {overwrite: Boolean} options
 * @returns {Promise}
 */
ActivityHistorySchema.statics.seed = function (doc, options) {
  const ActivityHistory = mongoose.model('ActivityHistory');

  return new Promise((resolve, reject) => {
    function skipDocument() {
      return new Promise((resolve, reject) => {
        ActivityHistory.findOne({ userID: doc.userID })
          .exec((err, existing) => {
            if (err) {
              return reject(err);
            }

            if (!existing) {
              return resolve(false);
            }

            if (existing && !options.overwrite) {
              return resolve(true);
            }

            // Remove ActivityHistory (overwrite)

            existing.remove((err) => {
              if (err) {
                return reject(err);
              }

              return resolve(false);
            });
          });
      });
    }

    function add(skip) {
      return new Promise((resolve, reject) => {
        if (skip) {
          return resolve({
            message: chalk.yellow(`Database Seeding: ActivityHistory\t\t type: ${doc.type} skipped`),
          });
        }

        const activityHistory = new ActivityHistory(doc);
        activityHistory.save((err) => {
          if (err) {
            return reject(err);
          }

          return resolve({
            message: `Database Seeding: ActivityHistory\t\t type: ${activityHistory.type} added`,
          });
        });
      });
    }

    skipDocument()
      .then(add)
      .then((response) => {
        return resolve(response);
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

mongoose.model('ActivityHistory', ActivityHistorySchema);
