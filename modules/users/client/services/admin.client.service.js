const request = require('superagent');
const url = require('../../../../commons/modules/users/datas/url.data');

const getUsers = ({ type, extendSearch, skip }) => {
  return new Promise((resovle, reject) => {
    request
      .get(url.USERS_ADMIN_SEARCH)
      .type('application/json')
      .accept('application/json')
      .set('X-Requested-With', 'XMLHttpRequest')
      .query({ type, extendSearch, skip })
      .end((err, res) => {
        if (err) {
          return reject(res);
        }

        return resovle(res);
      });
  });
};
module.exports.getUsers = getUsers;

const verifyAccount = ({ csrfToken, userID, verifyAccount }) => {
  return new Promise((resovle, reject) => {
    request
      .post(url.USERS_VERIFY_ID.replace(':id', userID))
      .type('application/json')
      .accept('application/json')
      .set('X-Requested-With', 'XMLHttpRequest')
      .set('csrf-token', csrfToken)
      .send(verifyAccount)
      .end((err, res) => {
        if (err) {
          return reject(res);
        }

        return resovle(res);
      });
  });
};
module.exports.verifyAccount = verifyAccount;

const deleteAccount = ({ csrfToken, userID, deleteAccount }) => {
  return new Promise((resovle, reject) => {
    request
      .post(url.USERS_DELETE_ID.replace(':id', userID))
      .type('application/json')
      .accept('application/json')
      .set('X-Requested-With', 'XMLHttpRequest')
      .set('csrf-token', csrfToken)
      .send(deleteAccount)
      .end((err, res) => {
        if (err) {
          return reject(res);
        }

        return resovle(res);
      });
  });
};
module.exports.deleteAccount = deleteAccount;

const restoreAccount = ({ csrfToken, userID, restoreAccount }) => {
  return new Promise((resovle, reject) => {
    request
      .put(url.USERS_DELETE_ID.replace(':id', userID))
      .type('application/json')
      .accept('application/json')
      .set('X-Requested-With', 'XMLHttpRequest')
      .set('csrf-token', csrfToken)
      .send(restoreAccount)
      .end((err, res) => {
        if (err) {
          return reject(res);
        }

        return resovle(res);
      });
  });
};
module.exports.restoreAccount = restoreAccount;

const lockAccount = ({ csrfToken, userID, lockAccount }) => {
  return new Promise((resovle, reject) => {
    request
      .post(url.USERS_LOCK_ID.replace(':id', userID))
      .type('application/json')
      .accept('application/json')
      .set('X-Requested-With', 'XMLHttpRequest')
      .set('csrf-token', csrfToken)
      .send(lockAccount)
      .end((err, res) => {
        if (err) {
          return reject(res);
        }

        return resovle(res);
      });
  });
};
module.exports.lockAccount = lockAccount;

const unlockAccount = ({ csrfToken, userID }) => {
  return new Promise((resovle, reject) => {
    request
      .put(url.USERS_LOCK_ID.replace(':id', userID))
      .accept('application/json')
      .set('X-Requested-With', 'XMLHttpRequest')
      .set('csrf-token', csrfToken)
      .end((err, res) => {
        if (err) {
          return reject(res);
        }

        return resovle(res);
      });
  });
};
module.exports.unlockAccount = unlockAccount;
