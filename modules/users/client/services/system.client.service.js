const request = require('superagent');
const url = require('../../../../commons/modules/users/datas/url.data');

const changeRolesAccount = ({ csrfToken, userID, changeRolesAccount }) => {
  return new Promise((resovle, reject) => {
    request
      .post(url.USERS_SYSTEM_ROLES_ID.replace(':id', userID))
      .type('application/json')
      .accept('application/json')
      .set('X-Requested-With', 'XMLHttpRequest')
      .set('csrf-token', csrfToken)
      .send(changeRolesAccount)
      .end((err, res) => {
        if (err) {
          return reject(res);
        }

        return resovle(res);
      });
  });
};
module.exports.changeRolesAccount = changeRolesAccount;
