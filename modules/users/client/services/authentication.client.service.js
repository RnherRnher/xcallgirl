const request = require('superagent');
const url = require('../../../../commons/modules/users/datas/url.data');

const signin = ({ csrfToken, signin }) => {
  return new Promise((resovle, reject) => {
    request
      .post(url.AUTH_SIGNIN)
      .type('application/json')
      .accept('application/json')
      .set('X-Requested-With', 'XMLHttpRequest')
      .set('csrf-token', csrfToken)
      .send(signin)
      .end((err, res) => {
        if (err) {
          return reject(res);
        }

        return resovle(res);
      });
  });
};
module.exports.signin = signin;

const signup = ({ csrfToken, signup }) => {
  return new Promise((resovle, reject) => {
    request
      .post(url.AUTH_SIGNUP)
      .type('application/json')
      .accept('application/json')
      .set('X-Requested-With', 'XMLHttpRequest')
      .set('csrf-token', csrfToken)
      .send(signup)
      .end((err, res) => {
        if (err) {
          return reject(res);
        }

        return resovle(res);
      });
  });
};
module.exports.signup = signup;

const signout = () => {
  return new Promise((resovle, reject) => {
    request
      .get(url.AUTH_SIGNOUT)
      .accept('application/json')
      .set('X-Requested-With', 'XMLHttpRequest')
      .end((err, res) => {
        if (err) {
          return reject(res);
        }

        return resovle(res);
      });
  });
};
module.exports.signout = signout;

const forgotPassword = ({ csrfToken, forgotPassword }) => {
  return new Promise((resovle, reject) => {
    request
      .post(url.AUTH_FORGOT_PASSWORD)
      .type('application/json')
      .accept('application/json')
      .set('X-Requested-With', 'XMLHttpRequest')
      .set('csrf-token', csrfToken)
      .send(forgotPassword)
      .end((err, res) => {
        if (err) {
          return reject(res);
        }

        return resovle(res);
      });
  });
};
module.exports.forgotPassword = forgotPassword;

const validateResetTokenPassword = ({ csrfToken, validateResetTokenPassword }) => {
  return new Promise((resovle, reject) => {
    request
      .post(url.AUTH_RESET)
      .type('application/json')
      .accept('application/json')
      .set('X-Requested-With', 'XMLHttpRequest')
      .set('csrf-token', csrfToken)
      .send(validateResetTokenPassword)
      .end((err, res) => {
        if (err) {
          return reject(res);
        }

        return resovle(res);
      });
  });
};
module.exports.validateResetTokenPassword = validateResetTokenPassword;

const resetPassword = ({ csrfToken, resetPassword }) => {
  return new Promise((resovle, reject) => {
    request
      .put(url.AUTH_RESET)
      .type('application/json')
      .accept('application/json')
      .set('X-Requested-With', 'XMLHttpRequest')
      .set('csrf-token', csrfToken)
      .send(resetPassword)
      .end((err, res) => {
        if (err) {
          return reject(res);
        }

        return resovle(res);
      });
  });
};
module.exports.resetPassword = resetPassword;
