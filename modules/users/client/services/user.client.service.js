const request = require('superagent');
const url = require('../../../../commons/modules/users/datas/url.data');
const constant = require('../../../../commons/utils/constant');

const getProfileUser = () => {
  return new Promise((resovle, reject) => {
    request
      .get(url.USERS)
      .accept('application/json')
      .set('X-Requested-With', 'XMLHttpRequest')
      .end((err, res) => {
        if (err) {
          return reject(res);
        }

        return resovle(res);
      });
  });
};
module.exports.getProfileUser = getProfileUser;

const getProfileSettings = ({ type }) => {
  return new Promise((resovle, reject) => {
    request
      .get(url.USERS_SETTINGS)
      .accept('application/json')
      .set('X-Requested-With', 'XMLHttpRequest')
      .query({ type })
      .end((err, res) => {
        if (err) {
          return reject(res);
        }

        return resovle(res);
      });
  });
};
module.exports.getProfileSettings = getProfileSettings;

const changeProfileSettings = ({ csrfToken, type, settings }) => {
  return new Promise((resovle, reject) => {
    request
      .post(url.USERS_SETTINGS)
      .type('application/json')
      .accept('application/json')
      .set('X-Requested-With', 'XMLHttpRequest')
      .set('csrf-token', csrfToken)
      .query({ type })
      .send(settings)
      .end((err, res) => {
        if (err) {
          return reject(res);
        }

        return resovle(res);
      });
  });
};
module.exports.changeProfileSettings = changeProfileSettings;

const changeProfilePassword = ({ csrfToken, passwordDetails }) => {
  return new Promise((resovle, reject) => {
    request
      .post(url.USERS_PASSWORD)
      .type('application/json')
      .accept('application/json')
      .set('X-Requested-With', 'XMLHttpRequest')
      .set('csrf-token', csrfToken)
      .send(passwordDetails)
      .end((err, res) => {
        if (err) {
          return reject(res);
        }

        return resovle(res);
      });
  });
};
module.exports.changeProfilePassword = changeProfilePassword;

const changeProfileNumberPhone = ({ csrfToken, numberPhone }) => {
  return new Promise((resovle, reject) => {
    request
      .post(url.USERS_NUMBERPHONE)
      .type('application/json')
      .accept('application/json')
      .set('X-Requested-With', 'XMLHttpRequest')
      .set('csrf-token', csrfToken)
      .send(numberPhone)
      .end((err, res) => {
        if (err) {
          return reject(res);
        }

        return resovle(res);
      });
  });
};
module.exports.changeProfileNumberPhone = changeProfileNumberPhone;

const changeProfileAddress = ({ csrfToken, address }) => {
  return new Promise((resovle, reject) => {
    request
      .post(url.USERS_ADDRESS)
      .type('application/json')
      .accept('application/json')
      .set('X-Requested-With', 'XMLHttpRequest')
      .set('csrf-token', csrfToken)
      .send(address)
      .end((err, res) => {
        if (err) {
          return reject(res);
        }

        return resovle(res);
      });
  });
};
module.exports.changeProfileAddress = changeProfileAddress;

const changeProfileAvatar = ({ csrfToken, image }) => {
  return new Promise((resovle, reject) => {
    request
      .post(url.USERS_AVATAR)
      .accept('application/json')
      .set('X-Requested-With', 'XMLHttpRequest')
      .set('csrf-token', csrfToken)
      .attach(constant.AVATAR, image)
      .end((err, res) => {
        if (err) {
          return reject(res);
        }

        return resovle(res);
      });
  });
};
module.exports.changeProfileAvatar = changeProfileAvatar;

const deleteProfileAvatar = ({ csrfToken, image }) => {
  return new Promise((resovle, reject) => {
    request
      .delete(url.USERS_AVATAR)
      .accept('application/json')
      .set('X-Requested-With', 'XMLHttpRequest')
      .set('csrf-token', csrfToken)
      .end((err, res) => {
        if (err) {
          return reject(res);
        }

        return resovle(res);
      });
  });
};
module.exports.deleteProfileAvatar = deleteProfileAvatar;

const requestVerifyAccount = ({ csrfToken, image }) => {
  return new Promise((resovle, reject) => {
    request
      .post(url.USERS_VERIFY)
      .accept('application/json')
      .set('X-Requested-With', 'XMLHttpRequest')
      .set('csrf-token', csrfToken)
      .attach(constant.VERIFY_ACCOUNT, image)
      .end((err, res) => {
        if (err) {
          return reject(res);
        }

        return resovle(res);
      });
  });
};
module.exports.requestVerifyAccount = requestVerifyAccount;

const getVerifyTokenNumberPhone = () => {
  return new Promise((resovle, reject) => {
    request
      .get(url.USERS_NUMBERPHONE)
      .accept('application/json')
      .set('X-Requested-With', 'XMLHttpRequest')
      .end((err, res) => {
        if (err) {
          return reject(res);
        }

        return resovle(res);
      });
  });
};
module.exports.getVerifyTokenNumberPhone = getVerifyTokenNumberPhone;

const verifyNumberPhone = ({ csrfToken, verifyTokenNumberPhone }) => {
  return new Promise((resovle, reject) => {
    request
      .put(url.USERS_NUMBERPHONE)
      .type('application/json')
      .accept('application/json')
      .set('X-Requested-With', 'XMLHttpRequest')
      .set('csrf-token', csrfToken)
      .send(verifyTokenNumberPhone)
      .end((err, res) => {
        if (err) {
          return reject(res);
        }

        return resovle(res);
      });
  });
};
module.exports.verifyNumberPhone = verifyNumberPhone;

const getUserID = ({ userID }) => {
  return new Promise((resovle, reject) => {
    request
      .get(url.USERS_GET_ID.replace(':id', userID))
      .accept('application/json')
      .set('X-Requested-With', 'XMLHttpRequest')
      .end((err, res) => {
        if (err) {
          return reject(res);
        }

        return resovle(res);
      });
  });
};
module.exports.getUserID = getUserID;
