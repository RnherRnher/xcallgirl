const React = require('react');
const propTypes = require('prop-types');
const classNames = require('classnames');
const regular = require('../../../../../commons/utils/regular-expression');
const { FormattedMessage } = require('react-intl');
const {
  MenuItem,
  TextField,
} = require('@material-ui/core');

const SelectRoles = class SelectRoles extends React.Component {
  constructor(props) {
    super(props);

    const { value } = this.props;
    this.state = {
      roles: value || '',
      error: false
    };

    this.handleChange = this.handleChange.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    const { isUpdatePropsToState, check } = this.props;

    if (isUpdatePropsToState) {
      const { value } = nextProps;

      this.setState({
        roles: value || '',
        error: check && !value
      });
    }
  }

  handleChange(event) {
    const { value } = event.target;

    this.setState({
      roles: value,
      error: false
    });
  }

  initElement() {
    const {
      className,
      disabled,
      err
    } = this.props;
    const { roles, error } = this.state;

    const Roles = regular.user.roles.map((value) => {
      return (
        <MenuItem key={value} value={value}>
          <FormattedMessage id={`user.roles.${value}`} />
        </MenuItem>
      );
    });

    return (
      <div className={classNames('xcg-select-roles-user', className)}>
        <TextField
          label={<FormattedMessage id="user.roles.is" />}
          select
          fullWidth
          value={roles}
          disabled={disabled}
          error={error || err}
          onChange={this.handleChange}
        >
          {Roles}
        </TextField>
      </div>
    );
  }

  render() {
    return this.initElement();
  }
};

SelectRoles.propTypes = {
  className: propTypes.string,
  value: propTypes.oneOf([''].concat(regular.user.roles)),
  disabled: propTypes.bool,
  check: propTypes.bool,
  err: propTypes.bool,
  isUpdatePropsToState: propTypes.bool,
};

SelectRoles.defaultProps = {
  className: '',
  value: '',
  disabled: false,
  check: false,
  err: false,
  isUpdatePropsToState: false
};

module.exports = SelectRoles;
