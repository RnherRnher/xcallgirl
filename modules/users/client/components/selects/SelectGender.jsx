const React = require('react');
const propTypes = require('prop-types');
const classNames = require('classnames');
const regular = require('../../../../../commons/utils/regular-expression');
const { FormattedMessage } = require('react-intl');
const {
  FormControl,
  FormLabel,
  RadioGroup,
  FormControlLabel,
  Radio
} = require('@material-ui/core');

const SelectGender = class SelectGender extends React.Component {
  constructor(props) {
    super(props);

    const { value } = this.props;
    this.state = {
      gender: value || '',
      error: false
    };

    this.handleChange = this.handleChange.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    const { isUpdatePropsToState, check } = this.props;

    if (isUpdatePropsToState) {
      const { value } = nextProps;

      this.setState({
        gender: value || '',
        error: check && !value
      });
    }
  }

  handleChange(event) {
    const { value } = event.target;

    this.setState({
      gender: value,
      error: false
    });
  }

  initElement() {
    const {
      className,
      disabled,
      err
    } = this.props;
    const { gender, error } = this.state;

    const Gender = regular.user.gender.map((value) => {
      return (
        <FormControlLabel
          key={value}
          value={value}
          control={<Radio />}
          label={<FormattedMessage id={`user.gender.${value}`} />}
        />
      );
    });

    return (
      <div className={classNames('xcg-select-gender-user', className)}>
        <FormControl error={error || err} disabled={disabled}>
          <FormLabel>
            <FormattedMessage id="user.gender.is" />
          </FormLabel>
          <RadioGroup
            row
            value={gender}
            onChange={this.handleChange}
          >
            {Gender}
          </RadioGroup>
        </FormControl>
      </div>
    );
  }

  render() {
    return this.initElement();
  }
};

SelectGender.propTypes = {
  className: propTypes.string,
  value: propTypes.oneOf([''].concat(regular.user.gender)),
  disabled: propTypes.bool,
  check: propTypes.bool,
  err: propTypes.bool,
  isUpdatePropsToState: propTypes.bool,
};

SelectGender.defaultProps = {
  className: '',
  value: '',
  disabled: false,
  check: false,
  err: false,
  isUpdatePropsToState: false
};

module.exports = SelectGender;
