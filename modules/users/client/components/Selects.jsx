const SelectGender = require('./selects/SelectGender');
const SelectRoles = require('./selects/SelectRoles');

module.exports = {
  SelectGender,
  SelectRoles
};
