const React = require('react');
const propTypes = require('prop-types');
const classNames = require('classnames');
const url = require('../../../../core/client/utils/url');
const { FormattedMessage } = require('react-intl');
const { connect } = require('react-redux');
const { push } = require('connected-react-router/immutable');
const {
  SupervisedUserCircle
} = require('@material-ui/icons');
const {
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
  Avatar,
} = require('@material-ui/core');
const { resetAdminStore } = require('../../actions/admin.client.action');

const NavigationAdmin = class NavigationAdmin extends React.Component {
  constructor(props) {
    super(props);

    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(location) {
    return (event) => {
      const { redirect, resetAdminStore } = this.props;

      switch (location) {
        case url.ADMIN_USERS_MANAGER:
          resetAdminStore('list');
          break;
        default:
          break;
      }

      redirect(location);
    };
  }

  initElement() {
    const { className } = this.props;

    return (
      <div className={classNames('xcg-navigation-admin', className)}>
        <List>
          <ListItem button onClick={this.handleClick(url.ADMIN_USERS_MANAGER)}>
            <ListItemAvatar>
              <Avatar>
                <SupervisedUserCircle />
              </Avatar>
            </ListItemAvatar>
            <ListItemText inset primary={<FormattedMessage id="info.manager.user" />} />
          </ListItem>
        </List>
      </div>
    );
  }

  render() {
    return this.initElement();
  }
};

NavigationAdmin.propTypes = {
  className: propTypes.string,
  resetAdminStore: propTypes.func.isRequired,
  redirect: propTypes.func.isRequired
};

NavigationAdmin.defaultProps = {
  className: '',
};

function mapStateToProps(state, props) {
  return {};
}

function mapDispatchToProps(dispatch, props) {
  return {
    resetAdminStore: (fragment) => {
      return dispatch(resetAdminStore(fragment));
    },
    redirect: (location) => {
      return dispatch(push(location));
    }
  };
}

module.exports = connect(
  mapStateToProps,
  mapDispatchToProps,
)(NavigationAdmin);
