const UIGeneralInfoAccount = require('./uis/UIGeneralInfoAccount');
const UIHandleAuthAccount = require('./uis/UIHandleAuthAccount');
const UIHandleRequestVerifyAccount = require('./uis/UIHandleRequestVerifyAccount');

module.exports = {
  UIGeneralInfoAccount,
  UIHandleAuthAccount,
  UIHandleRequestVerifyAccount
};
