const LoadableDetailUser = require('./loadables/LoadableDetailUser');
const LoadableUser = require('./loadables/LoadableUser');

module.exports = {
  LoadableDetailUser,
  LoadableUser
};
