const ForgotPassword = require('./authentication/ForgotPassword');
const Signin = require('./authentication/Signin');
const Signup = require('./authentication/Signup');

module.exports = {
  ForgotPassword,
  Signin,
  Signup
};
