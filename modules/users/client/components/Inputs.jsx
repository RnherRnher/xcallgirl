const InputName = require('./inputs/InputName');
const InputNumberPhone = require('./inputs/InputNumberPhone');
const InputPassword = require('./inputs/InputPassword');
const InputVerifyPassword = require('./inputs/InputVerifyPassword');
const InputImage = require('./inputs/InputImage');

module.exports = {
  InputName,
  InputNumberPhone,
  InputPassword,
  InputVerifyPassword,
  InputImage
};
