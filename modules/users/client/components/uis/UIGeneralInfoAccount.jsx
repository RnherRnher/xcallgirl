const React = require('react');
const classNames = require('classnames');
const propTypes = require('prop-types');
const constant = require('../../../../../commons/utils/constant');
const {
  FormattedMessage,
  FormattedRelative,
  FormattedNumber
} = require('react-intl');
const {
  CheckCircle,
  Error,
  Cake,
  Smartphone,
  LocationOn,
  AcUnit,
  Face,
  NewReleases,
  AccountBalanceWallet
} = require('@material-ui/icons');
const {
  List,
  ListItemText,
  ListItemSecondaryAction,
  ListItem,
  IconButton,
  Tooltip,
  ListItemIcon,
  Divider,
} = require('@material-ui/core');
const { getAddress } = require('../../../../core/client/utils/middleware');

const UIGeneralInfoAccount = class UIGeneralInfoAccount extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      tooltiped: null
    };

    this.handleClickTooltipOpen = this.handleClickTooltipOpen.bind(this);
    this.handleClickTooltipClose = this.handleClickTooltipClose.bind(this);
  }

  handleClickTooltipOpen(tooltiped) {
    return (event) => {
      this.setState({ tooltiped });
    };
  }

  handleClickTooltipClose(event) {
    this.setState({ tooltiped: false });
  }

  Personal() {
    const { value } = this.props;

    return (
      <List dense>
        <ListItem>
          <ListItemIcon>
            <Face />
          </ListItemIcon>
          <ListItemText
            inset
            primary={<FormattedMessage id="user.name.first.is" />}
            secondary={value.name.first}
          />
          <ListItemText
            inset
            primary={<FormattedMessage id="user.name.last.is" />}
            secondary={value.name.last}
          />
        </ListItem>
        <ListItem>
          <ListItemIcon>
            <AcUnit />
          </ListItemIcon>
          <ListItemText
            inset
            primary={<FormattedMessage id="user.gender.is" />}
            secondary={<FormattedMessage id={`user.gender.${value.gender}`} />}
          />
        </ListItem>
        {
          value.birthday
            ? <ListItem>
              <ListItemIcon>
                <Cake />
              </ListItemIcon>
              <ListItemText
                inset
                primary={<FormattedMessage id="user.birthday.is" />}
                secondary={
                  (value.birthday.day !== undefined && value.birthday.month !== undefined)
                    ? `${value.birthday.day} / ${value.birthday.month + 1} / ${value.birthday.year}`
                    : value.birthday.year
                }
              />
            </ListItem>
            : null
        }
        <ListItem>
          <ListItemIcon>
            <NewReleases />
          </ListItemIcon>
          <ListItemText
            inset
            primary={<FormattedMessage id="info.time.init" />}
            secondary={
              <FormattedRelative
                weekday="narrow"
                year="numeric"
                month="numeric"
                day="numeric"
                hour="numeric"
                minute="numeric"
                second="numeric"
                value={value.initDate}
              />
            }
          />
        </ListItem>
        {
          value.credit
            ? <ListItem>
              <ListItemIcon>
                <AccountBalanceWallet />
              </ListItemIcon>
              <ListItemText
                inset
                primary={<FormattedMessage id="credit.is" />}
                secondary={<FormattedNumber value={value.credit.balance} />}
              />
            </ListItem>
            : null
        }
      </List>
    );
  }

  Contact() {
    const { value } = this.props;
    const { tooltiped } = this.state;

    return (
      <List dense>
        {
          value.numberPhone
            ? <ListItem>
              <ListItemIcon>
                <Smartphone />
              </ListItemIcon>
              <ListItemText
                inset
                primary={<FormattedMessage id="user.numberPhone.is" />}
                secondary={`${value.numberPhone.code} ${value.numberPhone.number}`}
              />
              <ListItemSecondaryAction>
                <Tooltip
                  placement="top"
                  onClose={this.handleClickTooltipClose}
                  open={tooltiped === constant.VERIFY_NUMBERPHONE}
                  title={
                    <FormattedMessage id={value.verify.numberPhone.is
                      ? 'user.numberPhone.verify.success'
                      : 'user.numberPhone.verify.notExist'}
                    />
                  }
                >
                  <IconButton onClick={this.handleClickTooltipOpen(constant.VERIFY_NUMBERPHONE)}>
                    {
                      value.verify.numberPhone.is
                        ? <CheckCircle className="xcg-info" />
                        : <Error className="xcg-error" />
                    }
                  </IconButton>
                </Tooltip>
              </ListItemSecondaryAction>
            </ListItem>
            : null
        }
      </List>
    );
  }

  Address() {
    const { value } = this.props;

    return (
      <List dense>
        {
          value.address
            ? <div>
              {
                value.address.country
                  ? <ListItem>
                    <ListItemIcon>
                      <LocationOn />
                    </ListItemIcon>
                    <ListItemText
                      inset
                      primary={<FormattedMessage id="info.address.country.is" />}
                      secondary={getAddress(constant.COUNTRY, value.address).name}
                    />
                  </ListItem>
                  : null
              }
              {
                value.address.city
                  ? <ListItem>
                    <ListItemText
                      inset
                      primary={<FormattedMessage id="info.address.city.is" />}
                      secondary={getAddress(constant.CITY, value.address).name}
                    />
                  </ListItem>
                  : null
              }
              {
                value.address.county
                  ? <ListItem>
                    <ListItemText
                      inset
                      primary={<FormattedMessage id="info.address.county.is" />}
                      secondary={getAddress(constant.COUNTY, value.address).name}
                    />
                  </ListItem>
                  : null
              }
              {
                value.address.local
                  ? <ListItem>
                    <ListItemText
                      inset
                      primary={<FormattedMessage id="info.address.local.is" />}
                      secondary={getAddress(constant.LOCAL, value.address).name}
                    />
                  </ListItem>
                  : null
              }
            </div>
            : null
        }
      </List>
    );
  }

  initElement() {
    const { className } = this.props;

    return (
      <div className={classNames('xcg-ui-general-info-account', className)}>
        <Divider />
        {this.Personal()}
        <Divider />
        {this.Contact()}
        <Divider />
        {this.Address()}
      </div>
    );
  }

  render() {
    return this.initElement();
  }
};

UIGeneralInfoAccount.propTypes = {
  className: propTypes.string,
  value: propTypes.object.isRequired,
};

UIGeneralInfoAccount.defaultProps = {
  className: ''
};

module.exports = UIGeneralInfoAccount;
