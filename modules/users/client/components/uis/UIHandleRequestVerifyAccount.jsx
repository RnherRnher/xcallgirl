const React = require('react');
const classNames = require('classnames');
const propTypes = require('prop-types');
const joi = require('joi');
const constant = require('../../../../../commons/utils/constant');
const User = require('../users/User');
const { createStructuredSelector } = require('reselect');
const { connect } = require('react-redux');
const { FormattedMessage, FormattedDate } = require('react-intl');
const {
  KeyboardArrowRight,
  KeyboardArrowLeft,
  Sync,
  CheckCircle,
  Error
} = require('@material-ui/icons');
const {
  Table,
  TableBody,
  TableCell,
  Card,
  CardMedia,
  Button,
  TableRow,
  CardActions,
  MobileStepper,
  Dialog,
  DialogTitle,
  DialogContent,
  TextField,
  DialogActions,
  Slide
} = require('@material-ui/core');
const { AlertSnackbar } = require('../../../../core/client/components/Alerts');
const { verifyAccountRequest } = require('../../actions/admin.client.action');
const { makeSelectActions } = require('../../reselects/admin.client.reselect');
const { verifyAccount } = require('../../../../../commons/modules/users/validates/admin.validate');
const { ResultEnum } = require('../../../../core/client/constants/core.client.constant');

const UIHandleRequestVerifyAccount = class UIHandleRequestVerifyAccount extends React.Component {
  constructor(props) {
    super(props);

    const { value } = this.props;
    this.state = {
      activeStep: value.data.length - 1,
      expanded: null,
      reason: ''
    };

    this.handleCheck = this.handleCheck.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleClickExpand = this.handleClickExpand.bind(this);
    this.handleClickStep = this.handleClickStep.bind(this);
  }

  handleCheck(body, validate) {
    return joi.validate(body, validate);
  }

  handleClick(type) {
    return (event) => {
      const {
        userID,
        validate,
        verifyAccountRequest
      } = this.props;
      const { reason } = this.state;

      switch (type) {
        case constant.ACCEPT:
        case constant.NOT_ACCEPT:
          this.handleCheck({
            verifyAccount: {
              is: type === constant.ACCEPT,
              reason: type === constant.ACCEPT ? undefined : reason || undefined
            }
          }, validate)
            .then((verifyAccount) => {
              verifyAccountRequest(userID, verifyAccount);

              if (type === constant.NOT_ACCEPT) {
                this.handleClickExpand(type)();
              }
            });
          break;
        default:
          break;
      }
    };
  }

  handleChange(type) {
    return (event) => {
      const { value } = event.target;

      switch (type) {
        case constant.REASON:
          this.setState({ reason: value });
          break;
        default:
          break;
      }
    };
  }

  handleClickExpand(type) {
    return (event) => {
      this.setState((state, props) => {
        return {
          expanded: state.expanded === type ? null : type,
          reason: '',
        };
      });
    };
  }

  handleClickStep(num) {
    return (event) => {
      this.setState((state, props) => {
        return { activeStep: state.activeStep + num };
      });
    };
  }

  CustomView() {
    const { value } = this.props;
    const { activeStep } = this.state;

    const currentData = value.data[activeStep];
    const currentAdmin = value.admin[activeStep];

    const Result = () => {
      let Result = null;

      switch (
      activeStep === value.data.length - 1
        ? value.status
        : constant.FAILED
      ) {
        case constant.WAIT:
          Result = <Sync className="xcg-info" />;
          break;
        case constant.SUCCESS:
          Result = <CheckCircle className="xcg-success" />;
          break;
        case constant.FAILED:
          Result = <Error className="xcg-error" />;
          break;
        default:
          break;
      }

      return <span>{Result}</span>;
    };

    return (
      <div className="xcg-custom-view">
        {
          currentData
            ? <div>
              <CardMedia
                className="xcg-card-media"
                image={currentData.url}
              />
              <MobileStepper
                steps={value.data.length}
                position="static"
                activeStep={activeStep}
                backButton={
                  <Button
                    size="small"
                    onClick={this.handleClickStep(-1)}
                    disabled={activeStep === 0}
                  >
                    <KeyboardArrowLeft />
                    <FormattedMessage id="actions.back" />
                  </Button>
                }
                nextButton={
                  <Button
                    size="small"
                    onClick={this.handleClickStep(1)}
                    disabled={activeStep === value.data.length - 1}
                  >
                    <FormattedMessage id="actions.next" />
                    <KeyboardArrowRight />
                  </Button>
                }
              />
              {
                activeStep === value.data.length - 1
                  && value.status === constant.WAIT
                  ? null
                  : <AlertSnackbar
                    variant={
                      activeStep === value.data.length - 1
                        && value.status !== constant.WAIT
                        ? value.status
                        : constant.FAILED
                    }
                    message={
                      activeStep === value.data.length - 1
                        ? <FormattedMessage id={`user.verify.account.request.${value.status}`} />
                        : currentAdmin.reason || <FormattedMessage id="user.verify.account.request.failed" />
                    }
                  />
              }
              <Table>
                <TableBody>
                  <TableRow>
                    <TableCell>
                      <FormattedMessage id="info.status.is" />
                    </TableCell>
                    <TableCell>
                      <FormattedMessage
                        id={`info.result.${
                          activeStep === value.data.length - 1
                            ? value.status
                            : constant.FAILED
                          }`}
                      />
                    </TableCell>
                    <TableCell>
                      {Result()}
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell>
                      <FormattedMessage id="info.time.init" />
                    </TableCell>
                    <TableCell>
                      <FormattedDate
                        weekday="narrow"
                        year="numeric"
                        month="numeric"
                        day="numeric"
                        hour="numeric"
                        minute="numeric"
                        second="numeric"
                        value={currentData.initDate}
                      />
                    </TableCell>
                  </TableRow>
                  {
                    currentAdmin
                      ? <TableRow>
                        <TableCell>
                          <FormattedMessage id="info.time.confirm" />
                        </TableCell>
                        <TableCell>
                          <FormattedDate
                            weekday="narrow"
                            year="numeric"
                            month="numeric"
                            day="numeric"
                            hour="numeric"
                            minute="numeric"
                            second="numeric"
                            value={currentAdmin.confirmDate}
                          />
                        </TableCell>
                      </TableRow>
                      : null
                  }
                </TableBody>
              </Table>
            </div>
            : null
        }
        {
          currentAdmin
            ? <User value={currentAdmin.user} />
            : null
        }
      </div>
    );
  }

  DefaultView() {
    return (
      <div>
        <AlertSnackbar
          variant={constant.WARNING}
          message={<FormattedMessage id="user.verify.request.notExist" />}
        />
      </div>
    );
  }

  Transition(props) {
    return <Slide direction="up" {...props} />;
  }

  Actions() {
    const { verify } = this.props;
    const { reason } = this.state;

    const disabled = verify.result === ResultEnum.wait;

    const NotAcceptDialog = () => {
      const { expanded } = this.state;
      const open = expanded === constant.NOT_ACCEPT;

      return (
        <Dialog
          open={open}
          onClose={this.handleClickExpand(constant.NOT_ACCEPT)}
          TransitionComponent={this.Transition}
        >
          <DialogTitle>
            <FormattedMessage id="user.verify.account.failed" />
          </DialogTitle>
          <DialogContent>
            <TextField
              label={<FormattedMessage id="info.reason.is" />}
              type="text"
              multiline
              rows="5"
              rowsMax="5"
              fullWidth
              value={reason}
              disabled={disabled}
              onChange={this.handleChange(constant.REASON)}
            />
          </DialogContent>
          <DialogActions>
            <Button
              onClick={this.handleClick(constant.NOT_ACCEPT)}
              disabled={disabled}
              color="primary"
            >
              <FormattedMessage id="actions.notAccept" />
            </Button>
            <Button
              onClick={this.handleClickExpand(constant.NOT_ACCEPT)}
              disabled={disabled}
            >
              <FormattedMessage id="actions.back" />
            </Button>
          </DialogActions>
        </Dialog>
      );
    };

    return (
      <CardActions>
        <Button
          color="primary"
          disabled={disabled}
          onClick={this.handleClickExpand(constant.NOT_ACCEPT)}
        >
          <FormattedMessage color="primary" id="actions.notAccept" />
        </Button>
        <Button
          color="secondary"
          disabled={disabled}
          onClick={this.handleClick(constant.ACCEPT)}
        >
          <FormattedMessage color="secondary" id="actions.accept" />
        </Button>
        {NotAcceptDialog()}
      </CardActions>
    );
  }

  initElement() {
    const { className, value } = this.props;
    const { activeStep } = this.state;

    return (
      <div className={classNames('xcg-ui-handle-request-verify-account', className)}>
        <Card>
          {
            value.status === constant.NONE
              ? this.DefaultView()
              : this.CustomView()
          }
          {
            value.status === constant.WAIT
              && activeStep === value.data.length - 1
              ? this.Actions()
              : null
          }
        </Card>
      </div>
    );
  }

  render() {
    return this.initElement();
  }
};

UIHandleRequestVerifyAccount.propTypes = {
  className: propTypes.string,
  validate: propTypes.object,
  userID: propTypes.string.isRequired,
  value: propTypes.object.isRequired,
  verify: propTypes.object.isRequired,
  verifyAccountRequest: propTypes.func.isRequired
};

UIHandleRequestVerifyAccount.defaultProps = {
  className: '',
  validate: verifyAccount.body
};


function mapStateToProps(state, props) {
  return createStructuredSelector({
    verify: makeSelectActions(constant.ACCOUNT)
  });
}

function mapDispatchToProps(dispatch, props) {
  return {
    verifyAccountRequest: (userID, verifyAccount) => {
      return dispatch(verifyAccountRequest(userID, verifyAccount));
    }
  };
}

module.exports = connect(
  mapStateToProps,
  mapDispatchToProps,
)(UIHandleRequestVerifyAccount);
