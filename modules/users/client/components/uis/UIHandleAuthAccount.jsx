const React = require('react');
const classNames = require('classnames');
const propTypes = require('prop-types');
const joi = require('joi');
const constant = require('../../../../../commons/utils/constant');
const systemReselect = require('../../../../system/client/reselects/system.client.reselect');
const { connect } = require('react-redux');
const { createStructuredSelector } = require('reselect');
const { FormattedMessage, FormattedDate } = require('react-intl');
const {
  KeyboardArrowRight,
  KeyboardArrowLeft,
  Error,
  CheckCircle
} = require('@material-ui/icons');
const {
  Table,
  TableBody,
  TableCell,
  Button,
  TableRow,
  CardActions,
  MobileStepper,
  Dialog,
  DialogTitle,
  DialogContent,
  TextField,
  DialogActions,
  Slide,
  Typography
} = require('@material-ui/core');
const { AlertSnackbar } = require('../../../../core/client/components/Alerts');
const { ResultEnum } = require('../../../../core/client/constants/core.client.constant');
const { makeSelectActions } = require('../../reselects/admin.client.reselect');
const {
  lockAccountRequest,
  unlockAccountRequest,
  deleteAccountRequest,
  restoreAccountRequest
} = require('../../actions/admin.client.action');
const { changeRolesAccountRequest } = require('../../actions/system.client.action');
const {
  lockAccount,
  deleteAccount,
  restoreAccount
} = require('../../../../../commons/modules/users/validates/admin.validate');
const { changeRolesAccount } = require('../../../../../commons/modules/users/validates/system.validate');
const { InputPassword } = require('../Inputs');
const { SelectRoles } = require('../Selects');
const { SelectTime, SelectDate } = require('../../../../core/client/components/Selects');

const UIHandleAuthAccount = class UIHandleAuthAccount extends React.Component {
  constructor(props) {
    super(props);

    const { value } = this.props;
    this.state = {
      activeStep: value.lock ? value.lock.admin.length - 1 : 0,
      expanded: null,
      reason: '',
      date: new Date()
    };

    this.dateRef = React.createRef();
    this.timeRef = React.createRef();
    this.passwordRef = React.createRef();
    this.rolesRef = React.createRef();

    this.handleCheck = this.handleCheck.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleClickExpand = this.handleClickExpand.bind(this);
    this.handleClickStep = this.handleClickStep.bind(this);
  }

  handleCheck(body, validate) {
    return joi.validate(body, validate);
  }

  handleClick(type) {
    return (event) => {
      const {
        userID,
        validate,
        changeRolesAccountRequest,
        lockAccountRequest,
        unlockAccountRequest,
        deleteAccountRequest,
        restoreAccountRequest
      } = this.props;
      const { reason, date } = this.state;

      switch (type) {
        case constant.ROLES: {
          const { roles } = this.rolesRef.current.state;
          const { password } = this.passwordRef.current.state;

          this.handleCheck({
            password,
            roles
          }, validate.changeRolesAccount)
            .then((changeRolesAccount) => {
              changeRolesAccountRequest(userID, changeRolesAccount);
            });
        }
          break;
        case constant.LOCK: {
          const {
            day,
            month,
            year,
          } = this.dateRef.current.state;
          const { hours, minutes } = this.timeRef.current.state;

          this.handleCheck({
            lockAccount: {
              reason,
              expires: new Date(year, month, day, hours, minutes).getTime() - date.getTime()
            }
          }, validate.lockAccount)
            .then((lockAccount) => {
              lockAccountRequest(userID, lockAccount);
              this.handleClickExpand(type)();
            });
        }
          break;
        case constant.UNLOCK:
          unlockAccountRequest(userID);
          break;
        case constant.DELETE: {
          const { password } = this.passwordRef.current.state;

          this.handleCheck({
            password,
            deleteAccount: {
              reason
            }
          }, validate.deleteAccount)
            .then((deleteAccount) => {
              deleteAccountRequest(userID, deleteAccount);
              this.handleClickExpand(type)();
            });
        }
          break;
        case constant.RESTORE: {
          const { password } = this.passwordRef.current.state;

          this.handleCheck({
            password,
          }, validate.restoreAccount)
            .then((restoreAccount) => {
              restoreAccountRequest(userID, restoreAccount);
              this.handleClickExpand(type)();
            });
          break;
        }
        default:
          break;
      }
    };
  }

  handleChange(type) {
    return (event) => {
      const { value } = event.target;

      switch (type) {
        case constant.REASON:
          this.setState({ reason: value });
          break;
        default:
          break;
      }
    };
  }

  handleClickExpand(type) {
    return (event) => {
      this.setState((state, props) => {
        return {
          expanded: state.expanded === type ? null : type,
          reason: '',
        };
      });
    };
  }

  handleClickStep(num) {
    return (event) => {
      this.setState((state, props) => {
        return { activeStep: state.activeStep + num };
      });
    };
  }

  Transition(props) {
    return <Slide direction="up" {...props} />;
  }

  RolesView() {
    const { value, roles } = this.props;

    const disabled = roles.result === ResultEnum.wait;

    return (
      <div className="xcg-roles-view">
        <SelectRoles
          value={value.roles.value}
          ref={this.rolesRef}
          disabled={disabled}
        />
        <InputPassword
          ref={this.passwordRef}
          disabled={disabled}
        />
        <div className="xcg-primary-btn">
          <Button
            fullWidth
            onClick={this.handleClick(constant.ROLES)}
            disabled={disabled}
            color="secondary"
          >
            <Typography
              align="center"
              variant="button"
              color="secondary"
            >
              <FormattedMessage id="user.roles.change.is" />
            </Typography>
          </Button>
        </div>
      </div>
    );
  }

  LockView() {
    const { value } = this.props;
    const { activeStep } = this.state;

    const currentAdmin = value.lock.admin[activeStep];

    return (
      <div className="xcg-lock-view">
        {
          currentAdmin && currentAdmin.reason
            ? <AlertSnackbar
              variant={constant.ERROR}
              message={currentAdmin.reason}
            />
            : null
        }
        <Table>
          <TableBody>
            {
              value.lock.is
                ? <TableRow>
                  <TableCell>
                    <FormattedMessage id="info.time.expires" />
                  </TableCell>
                  <TableCell>
                    <FormattedDate
                      weekday="narrow"
                      year="numeric"
                      month="numeric"
                      day="numeric"
                      hour="numeric"
                      minute="numeric"
                      second="numeric"
                      value={Date.now() + value.lock.expiresDate}
                    />
                  </TableCell>
                  <TableCell>
                    <Error className="xcg-error" />
                  </TableCell>
                </TableRow>
                : null
            }
            <TableRow>
              <TableCell>
                <FormattedMessage id="info.status.is" />
              </TableCell>
              <TableCell>
                <FormattedMessage
                  id={value.lock.is ? 'user.lock.exist' : 'user.lock.notExist'}
                />
              </TableCell>
              <TableCell>
                {
                  value.lock.is
                    ? <Error className="xcg-error" />
                    : <CheckCircle className="xcg-success" />
                }
              </TableCell>
            </TableRow>
            {
              currentAdmin
                ? <TableRow>
                  <TableCell>
                    <FormattedMessage id="info.time.init" />
                  </TableCell>
                  <TableCell>
                    <FormattedDate
                      weekday="narrow"
                      year="numeric"
                      month="numeric"
                      day="numeric"
                      hour="numeric"
                      minute="numeric"
                      second="numeric"
                      value={currentAdmin.confirmDate}
                    />
                  </TableCell>
                  <TableCell>
                    {
                      activeStep === value.lock.admin.length - 1 && value.lock.is
                        ? <Error className="xcg-error" />
                        : null
                    }
                  </TableCell>
                </TableRow>
                : null
            }
          </TableBody>
        </Table>
        {
          value.lock.admin.length
            ? <MobileStepper
              steps={value.lock.admin.length}
              position="static"
              activeStep={activeStep}
              backButton={
                <Button
                  size="small"
                  onClick={this.handleClickStep(-1)}
                  disabled={activeStep === 0}
                >
                  <KeyboardArrowLeft />
                  <FormattedMessage id="actions.back" />
                </Button>
              }
              nextButton={
                <Button
                  size="small"
                  onClick={this.handleClickStep(1)}
                  disabled={activeStep === value.lock.admin.length - 1}
                >
                  <FormattedMessage id="actions.next" />
                  <KeyboardArrowRight />
                </Button>
              }
            />
            : null
        }
      </div>
    );
  }

  LockActions() {
    const {
      value,
      lock,
      unlock
    } = this.props;
    const {
      reason,
      expanded,
      date
    } = this.state;

    const lockDisabled = lock.result === ResultEnum.wait;
    const unlockDisabled = unlock.result === ResultEnum.wait;

    const LockDialog = () => {
      const open = expanded === constant.LOCK;

      const currrentDate = {
        day: date.getDate(),
        month: date.getMonth(),
        year: date.getFullYear()
      };

      return (
        <Dialog
          open={open}
          onClose={this.handleClickExpand(constant.LOCK)}
          TransitionComponent={this.Transition}
        >
          <DialogTitle>
            <FormattedMessage id="user.lock.is" />
          </DialogTitle>
          <DialogContent>
            <FormattedMessage id="info.time.expires" />
            <SelectDate
              ref={this.dateRef}
              disabled={lockDisabled}
              value={currrentDate}
              date={{
                day: {
                  min: 0,
                  max: 30,
                },
                month: {
                  min: 0,
                  max: 11,
                },
                year: {
                  min: currrentDate.year,
                  max: currrentDate.year + 1,
                },
              }}
              label={<FormattedMessage id="info.time.day" />}
            />
            <SelectTime
              label={<FormattedMessage id="info.time.hours" />}
              disabled={lockDisabled}
              ref={this.timeRef}
            />
            <AlertSnackbar
              variant="warning"
              message={<FormattedMessage id="user.lock.note" />}
            />
            <TextField
              label={<FormattedMessage id="info.reason.is" />}
              type="text"
              multiline
              rows="5"
              rowsMax="5"
              fullWidth
              value={reason}
              disabled={lockDisabled}
              onChange={this.handleChange(constant.REASON)}
            />
          </DialogContent>
          <DialogActions>
            <Button
              onClick={this.handleClick(constant.LOCK)}
              disabled={lockDisabled}
              color="primary"
            >
              <FormattedMessage id="actions.lock" />
            </Button>
            <Button
              onClick={this.handleClickExpand(constant.LOCK)}
              disabled={lockDisabled}
            >
              <FormattedMessage id="actions.back" />
            </Button>
          </DialogActions>
        </Dialog>
      );
    };

    return (
      <CardActions>
        <Button
          color="primary"
          disabled={lockDisabled}
          onClick={this.handleClickExpand(constant.LOCK)}
        >
          <FormattedMessage color="primary" id="actions.lock" />
        </Button>
        {
          value.lock.is
            ? <Button
              color="secondary"
              disabled={unlockDisabled}
              onClick={this.handleClick(constant.UNLOCK)}
            >
              <FormattedMessage color="secondary" id="actions.unlock" />
            </Button>
            : null
        }

        {LockDialog()}
      </CardActions>
    );
  }

  DeleteView() {
    const { value } = this.props;

    return (
      <div className="xcg-delete-view">
        {
          value.delete.is && value.delete.reason
            ? <AlertSnackbar
              variant={constant.ERROR}
              message={value.delete.reason}
            />
            : null
        }
        <Table>
          <TableBody>
            <TableRow>
              <TableCell>
                <FormattedMessage id="info.status.is" />
              </TableCell>
              <TableCell>
                <FormattedMessage
                  id={value.delete.is ? 'user.delete.exist' : 'user.delete.notExist'}
                />
              </TableCell>
              <TableCell>
                {
                  value.delete.is
                    ? <Error className="xcg-error" />
                    : <CheckCircle className="xcg-success" />
                }
              </TableCell>
            </TableRow>
            {
              value.delete.is
                ? <TableRow>
                  <TableCell>
                    <FormattedMessage id="info.time.init" />
                  </TableCell>
                  <TableCell>
                    <FormattedDate
                      weekday="narrow"
                      year="numeric"
                      month="numeric"
                      day="numeric"
                      hour="numeric"
                      minute="numeric"
                      second="numeric"
                      value={value.delete.initDate}
                    />
                  </TableCell>
                </TableRow>
                : null
            }
          </TableBody>
        </Table>
      </div>
    );
  }

  DeleteActions() {
    const {
      value,
      deletee,
      restore
    } = this.props;
    const { reason, expanded } = this.state;

    const restoreDisabled = restore.result === ResultEnum.wait;
    const deleteDisabled = deletee.result === ResultEnum.wait;

    const DeleteDialog = () => {
      const open = expanded === constant.DELETE;

      return (
        <Dialog
          open={open}
          onClose={this.handleClickExpand(constant.DELETE)}
          TransitionComponent={this.Transition}
        >
          <DialogTitle>
            <FormattedMessage id="user.delete.is" />
          </DialogTitle>
          <DialogContent>
            <TextField
              label={<FormattedMessage id="info.reason.is" />}
              type="text"
              multiline
              rows="5"
              rowsMax="5"
              fullWidth
              value={reason}
              disabled={deleteDisabled}
              onChange={this.handleChange(constant.REASON)}
            />
            <InputPassword
              label={<FormattedMessage id="user.password.is" />}
              ref={this.passwordRef}
              disabled={deleteDisabled}
            />
          </DialogContent>
          <DialogActions>
            <Button
              onClick={this.handleClick(constant.DELETE)}
              disabled={deleteDisabled}
              color="primary"
            >
              <FormattedMessage id="actions.delete" />
            </Button>
            <Button
              onClick={this.handleClickExpand(constant.DELETE)}
              disabled={deleteDisabled}
            >
              <FormattedMessage id="actions.back" />
            </Button>
          </DialogActions>
        </Dialog>
      );
    };

    const RestoreDialog = () => {
      const open = expanded === constant.RESTORE;

      return (
        <Dialog
          open={open}
          onClose={this.handleClickExpand(constant.RESTORE)}
          TransitionComponent={this.Transition}
        >
          <DialogTitle>
            <FormattedMessage id="user.restore.is" />
          </DialogTitle>
          <DialogContent>
            <InputPassword
              label={<FormattedMessage id="user.password.is" />}
              ref={this.passwordRef}
              disabled={restoreDisabled}
            />
          </DialogContent>
          <DialogActions>
            <Button
              onClick={this.handleClick(constant.RESTORE)}
              disabled={restoreDisabled}
              color="primary"
            >
              <FormattedMessage id="actions.restore" />
            </Button>
            <Button
              onClick={this.handleClickExpand(constant.RESTORE)}
              disabled={restoreDisabled}
            >
              <FormattedMessage id="actions.back" />
            </Button>
          </DialogActions>
        </Dialog>
      );
    };

    return (
      <CardActions>
        {
          value.delete.is
            ? <Button
              color="secondary"
              disabled={restoreDisabled}
              onClick={this.handleClickExpand(constant.RESTORE)}
            >
              <FormattedMessage color="secondary" id="actions.restore" />
            </Button>
            : <Button
              color="primary"
              disabled={deleteDisabled}
              onClick={this.handleClickExpand(constant.DELETE)}
            >
              <FormattedMessage color="primary" id="actions.delete" />
            </Button>
        }
        {DeleteDialog()}
        {RestoreDialog()}
      </CardActions>
    );
  }

  initElement() {
    const { className, type } = this.props;

    let Children = null;
    switch (type) {
      case constant.ROLES:
        Children = <div>
          {this.RolesView()}
        </div>;
        break;
      case constant.LOCK:
        Children = <div>
          {this.LockView()}
          {this.LockActions()}
        </div>;
        break;
      case constant.DELETE:
        Children = <div>
          {this.DeleteView()}
          {this.DeleteActions()}
        </div>;
        break;
      default:
        break;
    }

    return (
      <div className={classNames('xcg-ui-handle-account', className)}>
        {Children}
      </div>
    );
  }

  render() {
    return this.initElement();
  }
};

UIHandleAuthAccount.propTypes = {
  className: propTypes.string,
  validate: propTypes.object,
  type: propTypes.string.isRequired,
  userID: propTypes.string.isRequired,
  value: propTypes.object.isRequired,
  deletee: propTypes.object.isRequired,
  restore: propTypes.object.isRequired,
  roles: propTypes.object.isRequired,
  lock: propTypes.object.isRequired,
  unlock: propTypes.object.isRequired,
  changeRolesAccountRequest: propTypes.func.isRequired,
  lockAccountRequest: propTypes.func.isRequired,
  unlockAccountRequest: propTypes.func.isRequired,
  deleteAccountRequest: propTypes.func.isRequired,
  restoreAccountRequest: propTypes.func.isRequired
};

UIHandleAuthAccount.defaultProps = {
  className: '',
  validate: {
    changeRolesAccount: changeRolesAccount.body,
    lockAccount: lockAccount.body,
    deleteAccount: deleteAccount.body,
    restoreAccount: restoreAccount.body
  }
};


function mapStateToProps(state, props) {
  return createStructuredSelector({
    roles: systemReselect.makeSelectActions(constant.ROLES),
    lock: makeSelectActions(constant.LOCK),
    unlock: makeSelectActions(constant.UNLOCK),
    deletee: makeSelectActions(constant.DELETE),
    restore: makeSelectActions(constant.RESTORE)
  });
}

function mapDispatchToProps(dispatch, props) {
  return {
    changeRolesAccountRequest: (userID, changeRolesAccount) => {
      return dispatch(changeRolesAccountRequest(userID, changeRolesAccount));
    },
    lockAccountRequest: (userID, lockAccount) => {
      return dispatch(lockAccountRequest(userID, lockAccount));
    },
    unlockAccountRequest: (userID) => {
      return dispatch(unlockAccountRequest(userID));
    },
    deleteAccountRequest: (userID, deleteAccount) => {
      return dispatch(deleteAccountRequest(userID, deleteAccount));
    },
    restoreAccountRequest: (userID, restoreAccount) => {
      return dispatch(restoreAccountRequest(userID, restoreAccount));
    }
  };
}

module.exports = connect(
  mapStateToProps,
  mapDispatchToProps,
)(UIHandleAuthAccount);
