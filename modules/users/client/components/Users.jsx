const Settings = require('./users/Settings');
const CommonDetailUser = require('./users/CommonDetailUser');
const DetailUser = require('./users/DetailUser');
const ManagerUser = require('./users/ManagerUser');
const SettingsMenu = require('./users/SettingsMenu');
const User = require('./users/User');
const Users = require('./users/Users');
const { assignIn } = require('lodash');

module.exports = assignIn(
  Settings,
  {
    CommonDetailUser,
    DetailUser,
    ManagerUser,
    SettingsMenu,
    User,
    Users
  }
);
