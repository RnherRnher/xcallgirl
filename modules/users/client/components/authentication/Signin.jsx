const React = require('react');
const propTypes = require('prop-types');
const classNames = require('classnames');
const joi = require('joi');
const regular = require('../../../../../commons/utils/regular-expression');
const url = require('../../../../core/client/utils/url');
const { Link } = require('react-router-dom');
const { connect } = require('react-redux');
const { FormattedMessage } = require('react-intl');
const { createStructuredSelector } = require('reselect');
const { Button, Typography } = require('@material-ui/core');
const { InputPassword, InputNumberPhone } = require('../Inputs');
const { signin } = require('../../../../../commons/modules/users/validates/user.validate');
const { ResultEnum } = require('../../../../core/client/constants/core.client.constant');
const { signinRequest } = require('../../actions/authentication.client.action');
const { makeSelectAuthSignin } = require('../../reselects/authentication.client.reselect');

const Signin = class Signin extends React.Component {
  constructor(props) {
    super(props);

    this.passwordRef = React.createRef();
    this.numberPhoneRef = React.createRef();

    this.handleCheck = this.handleCheck.bind(this);

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleCheck(body) {
    const { validate } = this.props;

    return joi.validate(body, validate);
  }

  handleSubmit(event) {
    const { signinRequest } = this.props;

    const { numberPhone } = this.numberPhoneRef.current.state;
    const { password } = this.passwordRef.current.state;

    this.handleCheck({ numberPhone, password })
      .then((signin) => {
        signinRequest(signin);
      });

    event.preventDefault();
  }

  Form() {
    const { signin } = this.props;

    const disabled = signin.result === ResultEnum.wait;

    return (
      <div>
        <form onSubmit={this.handleSubmit}>
          <InputNumberPhone
            ref={this.numberPhoneRef}
            disabled={disabled}
          />
          <InputPassword
            ref={this.passwordRef}
            disabled={disabled}
          />
          <div className="xcg-primary-btn">
            <Button
              fullWidth
              type="submit"
              disabled={disabled}
              color="primary"
            >
              <Typography
                align="center"
                variant="button"
                color="primary"
              >
                <FormattedMessage id="user.signin.is" />
              </Typography>
            </Button>
          </div>
        </form>
      </div>
    );
  }

  Auth() {
    return (
      <div className="xcg-auth">
        <div className="xcg-or">
          <span className="xcg-or-content">
            <FormattedMessage id="or" />
          </span>
        </div>
        <div className="xcg-primary-btn">
          <Link to={url.AUTH_SIGNUP}>
            <Button>
              <Typography
                align="center"
                variant="button"
              >
                <FormattedMessage id="user.signup.is" />
              </Typography>
            </Button>
          </Link>
        </div>
        <div className="xcg-other-links">
          <ul>
            <li>
              <Link to={url.AUTH_FORGOT_PASSWORD}>
                <Typography
                  align="center"
                  variant="caption"
                >
                  <FormattedMessage id="user.password.forgot" />
                </Typography>
              </Link>
            </li>
          </ul>
        </div>
      </div>
    );
  }

  Header() {
    return (
      <div className="xcg-header">
        <div className="xcg-logo">
          <Link to={url.ROOT}>
            <img
              className="xcg-image"
              src={regular.app.logo}
              alt="xcallgirl_logo"
            />
          </Link>
        </div>
      </div>
    );
  }

  Section() {
    return (
      <div className="xcg-section">
        <div className="xcg-group-input">
          {this.Form()}
          {this.Auth()}
        </div>
      </div>
    );
  }

  Footer() {
    const year = new Date().getFullYear();

    return (
      <div className="xcg-footer">
        <div className="xcg-copyrirht">
          <Typography
            align="center"
            variant="caption"
          >
            {`XCallgirl © ${year}`}
          </Typography>
        </div>
      </div>
    );
  }

  initElement() {
    const { className } = this.props;

    return (
      <div className={classNames('xcg-signin-user', className)}>
        {this.Header()}
        {this.Section()}
        {this.Footer()}
      </div>
    );
  }

  render() {
    return this.initElement();
  }
};

Signin.propTypes = {
  className: propTypes.string,
  validate: propTypes.any,
  signin: propTypes.object.isRequired,
  signinRequest: propTypes.func.isRequired,
};

Signin.defaultProps = {
  className: '',
  validate: signin.body
};

function mapStateToProps(state, props) {
  return createStructuredSelector({
    signin: makeSelectAuthSignin()
  });
}

function mapDispatchToProps(dispatch, props) {
  return {
    signinRequest: (signin) => {
      return dispatch(signinRequest(signin));
    }
  };
}

module.exports = connect(
  mapStateToProps,
  mapDispatchToProps,
)(Signin);
