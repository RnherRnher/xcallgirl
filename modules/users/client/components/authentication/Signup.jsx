const React = require('react');
const propTypes = require('prop-types');
const classNames = require('classnames');
const languages = require('../../../../../commons/datas/languages');
const joi = require('joi');
const regular = require('../../../../../commons/utils/regular-expression');
const url = require('../../../../core/client/utils/url');
const { connect } = require('react-redux');
const { Link } = require('react-router-dom');
const { createStructuredSelector } = require('reselect');
const { FormattedMessage } = require('react-intl');
const { KeyboardArrowLeft, KeyboardArrowRight } = require('@material-ui/icons');
const {
  InputName,
  InputNumberPhone,
  InputVerifyPassword
} = require('../Inputs');
const {
  Button,
  MobileStepper,
  Typography
} = require('@material-ui/core');
const { SelectGender } = require('../Selects');
const { signup } = require('../../../../../commons/modules/users/validates/user.validate');
const { ResultEnum } = require('../../../../core/client/constants/core.client.constant');
const { signupRequest } = require('../../actions/authentication.client.action');
const { SelectAddress, SelectDate } = require('../../../../core/client/components/Selects');
const { makeSelectAuthSignup } = require('../../reselects/authentication.client.reselect');

const Signup = class Signup extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      activeStep: 0,
      body: {
        name: {},
        numberPhone: {},
        birthday: {},
        password: '',
        address: {},
        gender: '',
        settings: {
          general: {}
        }
      }
    };

    this.nameRef = React.createRef();
    this.numberPhoneRef = React.createRef();
    this.dateRef = React.createRef();
    this.prePasswordRef = React.createRef();
    this.addressRef = React.createRef();
    this.genderRef = React.createRef();

    this.handleCheck = this.handleCheck.bind(this);

    this.handleClickStep = this.handleClickStep.bind(this);
  }

  getData(disabled) {
    const { body } = this.state;

    return [
      <div className="xcg-group-input">
        <InputName
          ref={this.nameRef}
          disabled={disabled}
          value={body.name}
        />
        <InputNumberPhone
          check
          ref={this.numberPhoneRef}
          disabled={disabled}
          value={body.numberPhone}
        />
      </div>,
      <div className="xcg-group-input">
        <SelectDate
          ref={this.dateRef}
          disabled={disabled}
          value={body.birthday}
          label={<FormattedMessage id="user.birthday.is" />}
        />
        <SelectGender
          ref={this.genderRef}
          disabled={disabled}
          value={body.gender}
          check
        />
      </div>,
      <div className="xcg-group-input">
        <SelectAddress
          ref={this.addressRef}
          disabled={disabled}
          value={body.address}
          check
        />
      </div>,
      <div className="xcg-group-input">
        <InputVerifyPassword
          ref={this.prePasswordRef}
          disabled={disabled}
          value={body.address}
        />
      </div>
    ];
  }

  handleCheck(body) {
    const { validate } = this.props;

    return joi.validate(body, validate);
  }

  handleSubmit(body) {
    const { signupRequest } = this.props;

    this.handleCheck(body)
      .then((signup) => {
        signupRequest(signup);
      });
  }

  nextState() {
    const { body, activeStep } = this.state;
    let errorStep = false;

    switch (activeStep) {
      case 0: {
        const {
          first,
          last,
          nick,
          firstError,
          lastError,
          nickError
        } = this.nameRef.current.state;
        const {
          number,
          code,
          error
        } = this.numberPhoneRef.current.state;

        body.name = {
          first,
          last,
          nick
        };

        body.numberPhone = {
          code,
          number
        };

        errorStep = !code
          || !number
          || !first
          || !last
          || !nick
          || firstError
          || lastError
          || nickError
          || error;
      }
        break;
      case 1: {
        const {
          day,
          month,
          year,
          error
        } = this.dateRef.current.state;
        const { gender } = this.genderRef.current.state;

        body.birthday = {
          day,
          month,
          year
        };

        body.gender = gender;

        errorStep = this.genderRef.current.state.error
          || !gender
          || day === ''
          || month === ''
          || !year
          || error;
      }
        break;
      case 2: {
        const {
          country,
          city,
          county,
          local,
          error
        } = this.addressRef.current.state;

        body.address = { country };

        if (city) {
          body.address.city = city;
        }

        if (county) {
          body.address.county = county;
        }

        if (local) {
          body.address.local = local;
        }

        if (languages[country]) {
          body.settings = {
            general: {
              language: languages[country].id
            }
          };
        }

        errorStep = !country || error;
      }
        break;
      case 3: {
        const { password, error } = this.prePasswordRef.current.state;

        body.password = password;

        errorStep = !password || error;
      }
        break;
      default:
        break;
    }

    return {
      body,
      errorStep
    };
  }

  handleClickStep(num) {
    return (event) => {
      const promise = new Promise((resovle, reject) => {
        this.setState((state, props) => {
          const { maxSteps } = props;
          const { activeStep, body } = state;
          let nextState = { body, errorStep: false };

          if (num === 1) {
            nextState = this.nextState();
          }

          if (activeStep === maxSteps - 1 && !nextState.errorStep) {
            resovle(nextState.body);
          }

          return {
            activeStep: nextState.errorStep
              ? num ? activeStep : activeStep + num
              : (activeStep + num) === maxSteps ? activeStep : activeStep + num,
            body: nextState.body
          };
        });
      });

      promise.then((body) => {
        this.handleSubmit(body);
      });
    };
  }

  Form() {
    const { signup, maxSteps } = this.props;
    const { activeStep } = this.state;

    const disabled = signup.result === ResultEnum.wait;

    return (
      <div>
        <MobileStepper
          variant="progress"
          steps={maxSteps}
          position="static"
          activeStep={activeStep}
          backButton={
            <Button
              size="small"
              onClick={this.handleClickStep(-1)}
              disabled={activeStep === 0 || disabled}
            >
              <KeyboardArrowLeft />
              <FormattedMessage id="actions.back" />
            </Button>
          }
          nextButton={
            <Button
              size="small"
              onClick={this.handleClickStep(1)}
              disabled={disabled}
              color={
                activeStep === maxSteps - 1
                  ? 'primary'
                  : 'inherit'
              }
            >
              {
                activeStep === maxSteps - 1
                  ? <FormattedMessage id="user.signup.is" />
                  : <FormattedMessage id="actions.next" />
              }
              <KeyboardArrowRight />
            </Button>
          }
        />
        {this.getData(disabled)[activeStep]}
      </div>
    );
  }

  Auth() {
    return (
      <div className="xcg-auth">
        <div className="xcg-or">
          <span className="xcg-or-content">
            <FormattedMessage id="or" />
          </span>
        </div>
        <div className="xcg-primary-btn">
          <Link to={url.AUTH_SIGNIN}>
            <Button>
              <Typography
                align="center"
                variant="button"
              >
                <FormattedMessage id="user.signin.is" />
              </Typography>
            </Button>
          </Link>
        </div>
      </div>
    );
  }

  Header() {
    return (
      <div className="xcg-header">
        <div className="xcg-logo">
          <Link to={url.ROOT}>
            <img
              className="xcg-image"
              src={regular.app.logo}
              alt="xcallgirl_logo"
            />
          </Link>
        </div>
      </div>
    );
  }

  Section() {
    return (
      <div className="xcg-section">
        {this.Form()}
        {this.Auth()}
      </div>
    );
  }

  Footer() {
    const year = new Date().getFullYear();

    return (
      <div className="xcg-footer">
        <div className="xcg-copyrirht">
          <Typography
            align="center"
            variant="caption"
          >
            {`XCallgirl © ${year}`}
          </Typography>
        </div>
      </div>
    );
  }

  initElement() {
    const { className } = this.props;

    return (
      <div className={classNames('xcg-signup-user', className)}>
        {this.Header()}
        {this.Section()}
        {this.Footer()}
      </div>
    );
  }

  render() {
    return this.initElement();
  }
};

Signup.propTypes = {
  className: propTypes.string,
  validate: propTypes.any,
  maxSteps: propTypes.number,
  signup: propTypes.object.isRequired,
  signupRequest: propTypes.func.isRequired,
};

Signup.defaultProps = {
  className: '',
  validate: signup.body,
  maxSteps: 4,
};

function mapStateToProps(state, props) {
  return createStructuredSelector({
    signup: makeSelectAuthSignup()
  });
}

function mapDispatchToProps(dispatch, props) {
  return {
    signupRequest: (signup) => {
      return dispatch(signupRequest(signup));
    }
  };
}

module.exports = connect(
  mapStateToProps,
  mapDispatchToProps,
)(Signup);
