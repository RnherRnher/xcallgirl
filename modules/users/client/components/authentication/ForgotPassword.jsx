const React = require('react');
const propTypes = require('prop-types');
const classNames = require('classnames');
const joi = require('joi');
const regular = require('../../../../../commons/utils/regular-expression');
const url = require('../../../../core/client/utils/url');
const constant = require('../../../../../commons/utils/constant');
const { Link } = require('react-router-dom');
const { connect } = require('react-redux');
const { FormattedMessage } = require('react-intl');
const { createStructuredSelector } = require('reselect');
const {
  Button,
  Typography,
  TextField
} = require('@material-ui/core');
const { InputVerifyPassword, InputNumberPhone } = require('../Inputs');
const { ResultEnum } = require('../../../../core/client/constants/core.client.constant');
const { makeSelectForgotPassword } = require('../../reselects/authentication.client.reselect');
const {
  forgotPasswordRequest,
  validateResetTokenPasswordRequest,
  resetPasswordRequest
} = require('../../actions/authentication.client.action');
const {
  forgotPassword,
  validateResetTokenPassword,
  resetPassword
} = require('../../../../../commons/modules/users/validates/user.validate');

const ForgotPassword = class ForgotPassword extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      resetTokenPassword: ''
    };


    this.passwordRef = React.createRef();
    this.numberPhoneRef = React.createRef();

    this.handleCheck = this.handleCheck.bind(this);
    this.handleChange = this.handleChange.bind(this);

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    const { value } = event.target;

    this.setState({ resetTokenPassword: value });
  }

  handleCheck(body, validate) {
    return joi.validate(body, validate);
  }

  handleSubmit(type) {
    return (event) => {
      const {
        forgotPasswordRequest,
        validateResetTokenPasswordRequest,
        resetPasswordRequest,
        validate
      } = this.props;
      const { resetTokenPassword } = this.state;

      switch (type) {
        case constant.NUMBERPHONE: {
          const { number, code } = this.numberPhoneRef.current.state;

          this.handleCheck(
            {
              numberPhone: {
                code,
                number
              }
            },
            validate.forgotPassword
          ).then((forgotPassword) => {
            forgotPasswordRequest(forgotPassword);
          });
        }
          break;
        case constant.VERIFY:
          this.handleCheck({ resetTokenPassword }, validate.validateResetTokenPassword)
            .then((validateResetTokenPassword) => {
              validateResetTokenPasswordRequest(validateResetTokenPassword);
            });
          break;
        case constant.PASSWORD: {
          const { currentPassword, verifyPassword } = this.passwordRef.current.state;

          this.handleCheck(
            {
              resetTokenPassword,
              passwordDetails: {
                newPassword: currentPassword,
                verifyPassword
              }
            },
            validate.resetPassword
          ).then((resetPassword) => {
            resetPasswordRequest(resetPassword);
          });
        }
          break;
        default:
          break;
      }

      event.preventDefault();
    };
  }

  ForgotPasswordForm() {
    const { forgotPassword } = this.props;

    const disabled = forgotPassword.result === ResultEnum.wait;

    return (
      <div>
        <form onSubmit={this.handleSubmit(constant.NUMBERPHONE)}>
          <InputNumberPhone
            ref={this.numberPhoneRef}
            disabled={disabled}
          />
          <div className="xcg-primary-btn">
            <Button
              fullWidth
              type="submit"
              disabled={disabled}
              color="primary"
            >
              <Typography
                align="center"
                variant="button"
                color="primary"
              >
                <FormattedMessage id="actions.getToken" />
              </Typography>
            </Button>
          </div>
        </form>
      </div>
    );
  }

  ValidateResetTokenPasswordForm() {
    const { forgotPassword } = this.props;
    const { resetTokenPassword } = this.state;

    const disabled = forgotPassword.result === ResultEnum.wait;

    return (
      <div className="xcg-input-validate-reset-token-password-form">
        <form onSubmit={this.handleSubmit(constant.VERIFY)}>
          <TextField
            label={<FormattedMessage id="info.security.token.is" />}
            type="text"
            fullWidth
            value={resetTokenPassword}
            disabled={disabled}
            onChange={this.handleChange}
          />
          <div className="xcg-primary-btn">
            <Button
              fullWidth
              type="submit"
              disabled={disabled}
              color="primary"
            >
              <Typography
                align="center"
                variant="button"
                color="primary"
              >
                <FormattedMessage id="actions.vetify" />
              </Typography>
            </Button>
          </div>
        </form>
      </div>
    );
  }

  ResertPasswordForm() {
    const { forgotPassword } = this.props;

    const disabled = forgotPassword.result === ResultEnum.wait;

    return (
      <div>
        <form onSubmit={this.handleSubmit(constant.PASSWORD)}>
          <InputVerifyPassword
            label1={<FormattedMessage id="user.password.new" />}
            ref={this.passwordRef}
          />
          <div className="xcg-primary-btn">
            <Button
              fullWidth
              type="submit"
              disabled={disabled}
              color="primary"
            >
              <Typography
                align="center"
                variant="button"
                color="primary"
              >
                <FormattedMessage id="user.password.reset.is" />
              </Typography>
            </Button>
          </div>
        </form>
      </div>
    );
  }

  Auth() {
    return (
      <div className="xcg-auth">
        <div className="xcg-or">
          <span className="xcg-or-content">
            <FormattedMessage id="or" />
          </span>
        </div>
        <div className="xcg-group-btn">
          <div className="xcg-primary-btn">
            <Link to={url.AUTH_SIGNIN}>
              <Button>
                <Typography
                  align="center"
                  variant="button"
                >
                  <FormattedMessage id="user.signin.is" />
                </Typography>
              </Button>
            </Link>
          </div>
          <div className="xcg-primary-btn">
            <Link to={url.AUTH_SIGNUP}>
              <Button>
                <Typography
                  align="center"
                  variant="button"
                >
                  <FormattedMessage id="user.signup.is" />
                </Typography>
              </Button>
            </Link>
          </div>
        </div>
      </div>
    );
  }

  Header() {
    return (
      <div className="xcg-header">
        <div className="xcg-logo">
          <Link to={url.ROOT}>
            <img
              className="xcg-image"
              src={regular.app.logo}
              alt="xcallgirl_logo"
            />
          </Link>
        </div>
      </div>
    );
  }

  Section() {
    const { forgotPassword } = this.props;

    let Form = null;
    switch (forgotPassword.steps) {
      case 0:
        Form = this.ForgotPasswordForm();
        break;
      case 1:
        Form = this.ValidateResetTokenPasswordForm();
        break;
      case 2:
        Form = this.ResertPasswordForm();
        break;
      default:
        break;
    }
    return (
      <div className="xcg-section">
        <div className="xcg-group-input">
          {Form}
          {this.Auth()}
        </div>
      </div>
    );
  }

  Footer() {
    const year = new Date().getFullYear();

    return (
      <div className="xcg-footer">
        <div className="xcg-copyrirht">
          <Typography
            align="center"
            variant="caption"
          >
            {`XCallgirl © ${year}`}
          </Typography>
        </div>
      </div>
    );
  }

  initElement() {
    const { className } = this.props;

    return (
      <div className={classNames('xcg-forgot-password-user', className)}>
        {this.Header()}
        {this.Section()}
        {this.Footer()}
      </div>
    );
  }

  render() {
    return this.initElement();
  }
};

ForgotPassword.propTypes = {
  className: propTypes.string,
  validate: propTypes.object,
  forgotPassword: propTypes.object.isRequired,
  forgotPasswordRequest: propTypes.func.isRequired,
  validateResetTokenPasswordRequest: propTypes.func.isRequired,
  resetPasswordRequest: propTypes.func.isRequired,
};

ForgotPassword.defaultProps = {
  className: '',
  validate: {
    forgotPassword: forgotPassword.body,
    validateResetTokenPassword: validateResetTokenPassword.body,
    resetPassword: resetPassword.body,
  }
};

function mapStateToProps(state, props) {
  return createStructuredSelector({
    forgotPassword: makeSelectForgotPassword()
  });
}

function mapDispatchToProps(dispatch, props) {
  return {
    forgotPasswordRequest: (forgotPassword) => {
      return dispatch(forgotPasswordRequest(forgotPassword));
    },
    validateResetTokenPasswordRequest: (validateResetTokenPassword) => {
      return dispatch(validateResetTokenPasswordRequest(validateResetTokenPassword));
    },
    resetPasswordRequest: (resetPassword) => {
      return dispatch(resetPasswordRequest(resetPassword));
    }
  };
}

module.exports = connect(
  mapStateToProps,
  mapDispatchToProps,
)(ForgotPassword);
