const React = require('react');
const propTypes = require('prop-types');
const classNames = require('classnames');
const joi = require('joi');
const validation = require('../../../../../commons/utils/validation');
const constant = require('../../../../../commons/utils/constant');
const { FormattedMessage } = require('react-intl');
const { TextField, Grid } = require('@material-ui/core');

const InputName = class InputName extends React.Component {
  constructor(props) {
    super(props);

    const { value } = this.props;
    this.state = {
      first: value && value.first ? value.first : '',
      last: value && value.last ? value.last : '',
      nick: value && value.nick ? value.nick : '',
      firstError: false,
      lastError: false,
      nickError: false,
    };

    this.handleCheck = this.handleCheck.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  componentDidMount() {
    const {
      first,
      last,
      nick
    } = this.state;

    if (first) {
      this.handleCheck(constant.FIRST, first);
    }

    if (last) {
      this.handleCheck(constant.LAST, last);
    }

    if (nick) {
      this.handleCheck(constant.NICK, nick);
    }
  }

  componentWillReceiveProps(nextProps) {
    const { isUpdatePropsToState } = this.props;

    if (isUpdatePropsToState) {
      const { value } = nextProps;

      const first = value && value.first ? value.first : '';
      const last = value && value.last ? value.last : '';
      const nick = value && value.nick ? value.nick : '';

      this.setState({ first, last, nick });

      this.handleCheck(constant.FIRST, first);
      this.handleCheck(constant.LAST, last);
      this.handleCheck(constant.NICK, nick);
    }
  }

  handleCheck(type, value) {
    const { state } = this;
    const { validate } = this.props;

    const typeError = `${type}Error`;
    const error = state[typeError];

    joi.validate(
      value,
      validate[type],
      (err, value) => {
        if ((err && !error) || (!err && error)) {
          this.setState({ [typeError]: !error });
        }
      }
    );
  }

  handleChange(type) {
    return (event) => {
      const { value } = event.target;

      this.setState({ [type]: value });
      this.handleCheck(type, value);
    };
  }

  initElement() {
    const {
      className,
      disabled,
      err
    } = this.props;
    const {
      first,
      last,
      nick,
      firstError,
      lastError,
      nickError
    } = this.state;

    return (
      <div className={classNames('xcg-input-name-user', className)}>
        <Grid container spacing={8}>
          <Grid item xs={6}>
            <TextField
              label={<FormattedMessage id="user.name.first.is" />}
              type="text"
              fullWidth
              value={first}
              disabled={disabled}
              error={firstError || err}
              onChange={this.handleChange(constant.FIRST)}
              helperText={firstError || err ? <FormattedMessage id="user.name.first.regex" /> : null}
            />
          </Grid>
          <Grid item xs={6}>
            <TextField
              label={<FormattedMessage id="user.name.last.is" />}
              type="text"
              fullWidth
              value={last}
              disabled={disabled}
              error={lastError || err}
              onChange={this.handleChange(constant.LAST)}
              helperText={lastError || err ? <FormattedMessage id="user.name.last.regex" /> : null}
            />
          </Grid>
        </Grid>
        <TextField
          label={<FormattedMessage id="user.name.nick.is" />}
          type="text"
          fullWidth
          value={nick}
          disabled={disabled}
          error={nickError || err}
          onChange={this.handleChange(constant.NICK)}
          helperText={nickError || err ? <FormattedMessage id="user.name.nick.regex" /> : null}
        />
      </div>
    );
  }

  render() {
    return this.initElement();
  }
};

InputName.propTypes = {
  className: propTypes.string,
  value: propTypes.shape({
    first: propTypes.string,
    last: propTypes.string,
    nick: propTypes.string,
  }),
  disabled: propTypes.bool,
  validate: propTypes.any,
  err: propTypes.bool,
  isUpdatePropsToState: propTypes.bool,
};

InputName.defaultProps = {
  className: '',
  value: {
    first: '',
    last: '',
    nick: ''
  },
  disabled: false,
  validate: validation.body.name,
  err: false,
  isUpdatePropsToState: false
};

module.exports = InputName;
