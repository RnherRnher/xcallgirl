const React = require('react');
const propTypes = require('prop-types');
const classNames = require('classnames');
const joi = require('joi');
const validation = require('../../../../../commons/utils/validation');
const constant = require('../../../../../commons/utils/constant');
const { TextField, InputAdornment, IconButton } = require('@material-ui/core');
const { Visibility, VisibilityOff } = require('@material-ui/icons');
const { FormattedMessage } = require('react-intl');

const InputPassword = class InputPassword extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      password: '',
      show: false,
      error: false,
    };

    this.handleCheck = this.handleCheck.bind(this);
    this.handleClickShow = this.handleClickShow.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handleClickShow(event) {
    this.setState((state, props) => {
      return { show: !state.show };
    });
  }

  handleCheck(password) {
    const { error } = this.state;
    const { validate } = this.props;

    joi.validate(
      password,
      validate,
      (err, value) => {
        if ((err && !error) || (!err && error)) {
          this.setState({ error: !error });
        }
      }
    );
  }

  handleChange(event) {
    const { value } = event.target;
    const { check, handleChangeParent } = this.props;

    this.setState({ password: value });

    if (check) {
      this.handleCheck(value);
    }

    if (handleChangeParent) {
      handleChangeParent(value);
    }
  }

  initElement() {
    const {
      className,
      disabled,
      label,
      helperText,
      err
    } = this.props;
    const {
      show,
      password,
      error
    } = this.state;

    return (
      <div className={classNames('xcg-input-password-user', className)}>
        <TextField
          label={label || <FormattedMessage id="user.password.is" />}
          type={show ? constant.TEXT : constant.PASSWORD}
          fullWidth
          value={password}
          disabled={disabled}
          error={err || error}
          onChange={this.handleChange}
          helperText={(err || error) ? helperText : null}
          InputProps={{
            endAdornment: (
              <InputAdornment position="end">
                <IconButton onClick={this.handleClickShow}>
                  {show ? <VisibilityOff /> : <Visibility />}
                </IconButton>
              </InputAdornment>
            ),
          }}
        />
      </div>
    );
  }

  render() {
    return this.initElement();
  }
};

InputPassword.propTypes = {
  className: propTypes.string,
  disabled: propTypes.bool,
  validate: propTypes.any,
  check: propTypes.bool,
  err: propTypes.bool,
  label: propTypes.node,
  helperText: propTypes.node,
  handleChangeParent: propTypes.func,
};

InputPassword.defaultProps = {
  className: '',
  disabled: false,
  validate: validation.body.password,
  check: false,
  err: false,
  label: null,
  helperText: <FormattedMessage id="user.password.regex" />,
  handleChangeParent: null,
};

module.exports = InputPassword;
