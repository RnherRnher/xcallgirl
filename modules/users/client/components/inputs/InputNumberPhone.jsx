const React = require('react');
const propTypes = require('prop-types');
const classNames = require('classnames');
const joi = require('joi');
const SelectCountry = require('../../../../core/client/components/selects/SelectCountry');
const validation = require('../../../../../commons/utils/validation');
const regular = require('../../../../../commons/utils/regular-expression');
const { FormattedMessage } = require('react-intl');
const {
  TextField,
  Grid,
  FormHelperText
} = require('@material-ui/core');

const InputNumberPhone = class InputNumberPhone extends React.Component {
  constructor(props) {
    super(props);

    const { value } = this.props;
    this.state = {
      number: value && value.number ? value.number : '',
      code: value && value.code ? value.code : '',
      numberPhone: value && value.number && value.code ? `${value.code} ${value.number}` : '',
      error: false
    };

    this.handleCheck = this.handleCheck.bind(this);
    this.handleChangeNumber = this.handleChangeNumber.bind(this);
    this.handleChangeCountry = this.handleChangeCountry.bind(this);
  }

  componentDidMount() {
    const { check } = this.props;
    const {
      number,
      code,
      numberPhone
    } = this.state;

    if (check && (number || code)) {
      this.handleCheck(numberPhone);
    }
  }

  componentWillReceiveProps(nextProps) {
    const { isUpdatePropsToState } = this.props;

    if (isUpdatePropsToState) {
      const { check, value } = nextProps;

      const numberPhone = value && value.number && value.code ? `${value.code} ${value.number}` : '';

      this.setState({
        number: value && value.number ? value.number : '',
        code: value && value.code ? value.code : '',
        numberPhone,
      });

      if (check) {
        this.handleCheck(numberPhone);
      }
    }
  }

  handleCheck(numberPhone) {
    const { error } = this.state;
    const { validate } = this.props;

    joi.validate(
      numberPhone,
      validate,
      (err, value) => {
        if ((err && !error) || (!err && error)) {
          this.setState({ error: !error });
        }
      }
    );
  }

  handleChangeNumber(event) {
    const { value } = event.target;
    const { check, numberRegex } = this.props;
    const { code } = this.state;

    const numberPhone = `${code} ${value}`;

    this.setState({
      number: numberRegex.test(value) ? value : (value ? value.slice(0, -1) : ''),
      numberPhone
    });

    if (check) {
      this.handleCheck(numberPhone);
    }
  }

  handleChangeCountry(value) {
    const { check } = this.props;
    const { number } = this.state;

    const numberPhone = `${value} ${number}`;

    this.setState({
      code: `${value}`,
      numberPhone
    });

    if (check) {
      this.handleCheck(numberPhone);
    }
  }

  initElement() {
    const { className, disabled, err } = this.props;
    const { number, code, error } = this.state;

    return (
      <div className={classNames('xcg-input-numberphone-user', className)}>
        <Grid container spacing={8}>
          <Grid item xs={4}>
            <SelectCountry
              type="phoneCode"
              displayType="phoneCode"
              value={code}
              err={error || err}
              disabled={disabled}
              handleChangeParent={this.handleChangeCountry}
            />
          </Grid>
          <Grid item xs={8}>
            <TextField
              label={<FormattedMessage id="user.numberPhone.is" />}
              type="text"
              fullWidth
              error={error || err}
              value={number}
              disabled={disabled}
              onChange={this.handleChangeNumber}
            />
          </Grid>
        </Grid>
        {error || err
          ? <FormHelperText error>
            <FormattedMessage id="user.numberPhone.note" />
          </FormHelperText>
          : null
        }
      </div>
    );
  }

  render() {
    return this.initElement();
  }
};

InputNumberPhone.propTypes = {
  className: propTypes.string,
  value: propTypes.shape({
    code: propTypes.string,
    number: propTypes.string
  }),
  disabled: propTypes.bool,
  err: propTypes.bool,
  check: propTypes.bool,
  validate: propTypes.any,
  numberRegex: propTypes.any,
  isUpdatePropsToState: propTypes.bool,
};

InputNumberPhone.defaultProps = {
  className: '',
  value: {
    code: '',
    number: ''
  },
  disabled: false,
  err: false,
  check: false,
  validate: validation.body.numberPhone.full,
  numberRegex: regular.commons.number,
  isUpdatePropsToState: false
};

module.exports = InputNumberPhone;
