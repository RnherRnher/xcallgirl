const React = require('react');
const propTypes = require('prop-types');
const classNames = require('classnames');
const InputPassword = require('./InputPassword');
const { FormattedMessage } = require('react-intl');

const InputVerifyPassword = class InputVerifyPassword extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      currentPassword: '',
      password: '',
      verifyPassword: '',
      error: false,
    };

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(type) {
    return (value) => {
      this.setState((state, props) => {
        const currentState = state;
        currentState[type] = value;
        const { currentPassword, verifyPassword, error } = currentState;

        let currntError = true;
        let password = '';

        if (currentPassword === verifyPassword && error) {
          currntError = false;
          password = verifyPassword;
        }

        return {
          [type]: value,
          error: currntError,
          password
        };
      });
    };
  }

  initElement() {
    const {
      className,
      disabled,
      err,
      label1,
      label2
    } = this.props;
    const { error } = this.state;

    return (
      <div className={classNames('xcg-input-verify-password-user', className)}>
        <InputPassword
          check
          label={label1}
          handleChangeParent={this.handleChange('currentPassword')}
          disabled={disabled}
          err={err}
        />
        <InputPassword
          handleChangeParent={this.handleChange('verifyPassword')}
          err={error || err}
          label={label2}
          helperText={<FormattedMessage id="user.password.verify.failed" />}
          disabled={disabled}
        />
      </div>
    );
  }

  render() {
    return this.initElement();
  }
};

InputVerifyPassword.propTypes = {
  className: propTypes.string,
  disabled: propTypes.bool,
  err: propTypes.bool,
  label1: propTypes.node,
  label2: propTypes.node,
};

InputVerifyPassword.defaultProps = {
  className: '',
  disabled: false,
  err: false,
  label1: null,
  label2: <FormattedMessage id="user.password.verify.is" />,
};

module.exports = InputVerifyPassword;
