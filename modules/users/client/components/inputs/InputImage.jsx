const React = require('react');
const propTypes = require('prop-types');
const classNames = require('classnames');
const { FormattedMessage } = require('react-intl');
const { Delete } = require('@material-ui/icons');
const {
  ListItemText,
  ListItem,
  Typography,
  Button,
  ListItemSecondaryAction,
  IconButton,
} = require('@material-ui/core');

const InputImage = class InputImage extends React.Component {
  constructor(props) {
    super(props);

    const { value } = this.props;
    this.state = {
      url: value,
      blob: null
    };

    this.handleChangeFile = this.handleChangeFile.bind(this);
    this.handleRemoveFile = this.handleRemoveFile.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    const { isUpdatePropsToState } = this.props;

    if (isUpdatePropsToState) {
      const { value } = nextProps;

      this.setState({ url: value });
    }
  }

  handleChangeFile(event) {
    const { files } = event.target;

    this.setState({ blob: files[0] });

    const reader = new FileReader();
    reader.readAsDataURL(files[0]);
    reader.addEventListener('load', () => {
      this.setState({ url: reader.result });
    });
  }

  handleRemoveFile(event) {
    this.setState({ url: null });
  }

  handleClick(event) {
    const { handleClickUpload } = this.props;
    const { blob } = this.state;

    handleClickUpload(blob);
  }

  DefaultImage() {
    const { disabled, actionLabel } = this.props;

    return (
      <div className="xcg-image xcg-default-image-view">
        <input
          type="file"
          accept="image/*"
          className="button-file"
          id="button-file"
          onChange={this.handleChangeFile}
        />
        <label htmlFor="button-file">
          <Button
            className="xcg-upload-button"
            variant="contained"
            component="span"
            disabled={disabled}
          >
            <Typography
              align="center"
              variant="button"
              color="secondary"
            >
              {actionLabel}
            </Typography>
          </Button>
        </label>
      </div>
    );
  }

  CustomImage() {
    const { disabled } = this.props;
    const { url } = this.state;

    return (
      <div className="xcg-custom-image-view">
        <img
          className="xcg-image"
          src={url}
          alt="image_upload"
        />
        <div className="xcg-primary-btn">
          <Button
            fullWidth
            onClick={this.handleClick}
            disabled={disabled}
            color="secondary"
          >
            <Typography
              align="center"
              variant="button"
              color="secondary"
            >
              <FormattedMessage id="actions.upload" />
            </Typography>
          </Button>
        </div>
      </div>
    );
  }

  initElement() {
    const { className, nameLabel, disabled } = this.props;
    const { url } = this.state;

    return (
      <div className={classNames('xcg-input-image-user', className)}>
        <ListItem>
          {
            nameLabel
              ? <ListItemText
                className="xcg-list-item-text-center"
                primary={
                  <Typography
                    align="center"
                    variant="title"
                  >
                    {nameLabel}
                  </Typography>
                }
              />
              : null
          }
          {url
            ? <ListItemSecondaryAction>
              <IconButton
                disabled={disabled}
                onClick={this.handleRemoveFile}
              >
                <Delete />
              </IconButton>
            </ListItemSecondaryAction>
            : null
          }
        </ListItem>
        {url ? this.CustomImage() : this.DefaultImage()}
      </div>
    );
  }

  render() {
    return this.initElement();
  }
};

InputImage.propTypes = {
  className: propTypes.string,
  value: propTypes.string,
  disabled: propTypes.bool,
  nameLabel: propTypes.node,
  actionLabel: propTypes.node,
  isUpdatePropsToState: propTypes.bool,
  handleClickUpload: propTypes.func.isRequired
};

InputImage.defaultProps = {
  className: '',
  value: null,
  disabled: false,
  nameLabel: null,
  actionLabel: <FormattedMessage id="actions.upload" />,
  isUpdatePropsToState: false
};

module.exports = InputImage;
