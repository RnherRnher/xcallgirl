const UsersManager = require('./admin/UsersManager');
const UsersSearch = require('./admin/UsersSearch');

module.exports = {
  UsersManager,
  UsersSearch
};
