const React = require('react');
const classNames = require('classnames');
const propTypes = require('prop-types');
const url = require('../../../../core/client/utils/url');
const constant = require('../../../../../commons/utils/constant');
const { FormattedMessage } = require('react-intl');
const { connect } = require('react-redux');
const { push } = require('connected-react-router/immutable');
const {
  Translate,
  AccountCircle,
  Notifications,
  Lock,
  VerifiedUser,
  VpnKey
} = require('@material-ui/icons');
const {
  List,
  ListSubheader,
  ListItem,
  ListItemAvatar,
  ListItemText,
  Divider,
  Avatar
} = require('@material-ui/core');

const SettingsMenu = class SettingsMenu extends React.Component {
  constructor(props) {
    super(props);

    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(location) {
    return (event) => {
      const { redirect } = this.props;

      redirect(url.SETTINGS_PATH.replace(constant.PATH, location));
    };
  }

  initElement() {
    const { className } = this.props;

    return (
      <div className={classNames('xcg-settings-menu-user', className)}>
        <List subheader={<ListSubheader><FormattedMessage id="info.general.is" /></ListSubheader>}>
          <ListItem button onClick={this.handleClick(constant.ACCOUNT)}>
            <ListItemAvatar>
              <Avatar>
                <AccountCircle />
              </Avatar>
            </ListItemAvatar>
            <ListItemText inset primary={<FormattedMessage id="info.account.is" />} />
          </ListItem>
          <ListItem button onClick={this.handleClick(constant.LANGUAGE)}>
            <ListItemAvatar>
              <Avatar>
                <Translate />
              </Avatar>
            </ListItemAvatar>
            <ListItemText inset primary={<FormattedMessage id="info.national.language.is" />} />
          </ListItem>
          <ListItem button onClick={this.handleClick(constant.NOTIFICATIONS)}>
            <ListItemAvatar>
              <Avatar>
                <Notifications />
              </Avatar>
            </ListItemAvatar>
            <ListItemText inset primary={<FormattedMessage id="notification.is" />} />
          </ListItem>
        </List>
        <Divider />
        <List subheader={<ListSubheader><FormattedMessage id="info.security.is" /></ListSubheader>}>
          <ListItem button onClick={this.handleClick(constant.PRIVCY)}>
            <ListItemAvatar>
              <Avatar>
                <Lock />
              </Avatar>
            </ListItemAvatar>
            <ListItemText inset primary={<FormattedMessage id="info.security.privacy.is" />} />
          </ListItem>
          <ListItem button onClick={this.handleClick(constant.PASSWORD)}>
            <ListItemAvatar>
              <Avatar>
                <VpnKey />
              </Avatar>
            </ListItemAvatar>
            <ListItemText inset primary={<FormattedMessage id="user.password.is" />} />
          </ListItem>
          <ListItem button onClick={this.handleClick(constant.VERIFY)}>
            <ListItemAvatar>
              <Avatar>
                <VerifiedUser />
              </Avatar>
            </ListItemAvatar>
            <ListItemText inset primary={<FormattedMessage id="info.security.verify.is" />} />
          </ListItem>
        </List>
      </div>
    );
  }

  render() {
    return this.initElement();
  }
};

SettingsMenu.propTypes = {
  className: propTypes.string,
  redirect: propTypes.func.isRequired
};

SettingsMenu.defaultProps = {
  className: ''
};

function mapStateToProps(state, props) {
  return {};
}

function mapDispatchToProps(dispatch, props) {
  return {
    redirect: (location) => {
      return dispatch(push(location));
    }
  };
}

module.exports = connect(
  mapStateToProps,
  mapDispatchToProps,
)(SettingsMenu);
