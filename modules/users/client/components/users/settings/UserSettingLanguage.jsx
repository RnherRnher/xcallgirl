const React = require('react');
const countries = require('../../../../../../commons/datas/countries');
const languages = require('../../../../../../commons/datas/languages');
const classNames = require('classnames');
const propTypes = require('prop-types');
const constant = require('../../../../../../commons/utils/constant');
const { connect } = require('react-redux');
const { createStructuredSelector } = require('reselect');
const { FormattedMessage } = require('react-intl');
const { Translate } = require('@material-ui/icons');
const {
  List,
  ListSubheader,
  ListItemAvatar,
  ListItemText,
  ListItemSecondaryAction,
  Checkbox,
  ListItem,
  Avatar
} = require('@material-ui/core');
const { changeProfileSettingsRequest } = require('../../../actions/user.client.action');
const { makeSelectGeneralSettings } = require('../../../reselects/user.client.reselect');

const UserSettingLanguage = class UserSettingLanguage extends React.Component {
  constructor(props) {
    super(props);

    this.handleClick = this.handleClick.bind(this);
  }

  getData() {
    const { language } = this.props;

    const Languages = [];
    countries.forEach((country) => {
      const currntLanguage = languages[country.id];

      if (currntLanguage) {
        Languages.push(
          <div key={currntLanguage.id}>
            <ListItem
              button
              onClick={this.handleClick(currntLanguage.id)}
            >
              <ListItemAvatar>
                <Avatar>
                  {country.flag}
                </Avatar>
              </ListItemAvatar>
              <ListItemText
                inset
                primary={currntLanguage.name}
                secondary={country.name}
              />
              <ListItemSecondaryAction>
                <Checkbox checked={language === currntLanguage.id} />
              </ListItemSecondaryAction>
            </ListItem>
          </div>
        );
      }
    });

    return Languages;
  }

  handleClick(language) {
    return (event) => {
      const { changeProfileSettingsRequest } = this.props;

      changeProfileSettingsRequest(
        constant.GENERAL,
        { settings: { language } }
      );
    };
  }

  initElement() {
    const { className, language } = this.props;

    return (
      <div className={classNames('xcg-setting-language-user', className)}>
        <List subheader={<ListSubheader><FormattedMessage id="info.national.language.is" /></ListSubheader>}>
          <ListItem divider>
            <ListItemAvatar>
              <Avatar>
                <Translate />
              </Avatar>
            </ListItemAvatar>
            <ListItemText
              inset
              primary={<FormattedMessage id={`info.national.language.${language}`} />}
            />
          </ListItem>
          {this.getData()}
        </List>
      </div>
    );
  }

  render() {
    return this.initElement();
  }
};

UserSettingLanguage.propTypes = {
  className: propTypes.string,
  language: propTypes.string.isRequired,
  changeProfileSettingsRequest: propTypes.func.isRequired
};

UserSettingLanguage.defaultProps = {
  className: ''
};

function mapStateToProps(state, props) {
  return createStructuredSelector({
    language: makeSelectGeneralSettings(constant.LANGUAGE)
  });
}

function mapDispatchToProps(dispatch, props) {
  return {
    changeProfileSettingsRequest: (fragment, settings) => {
      return dispatch(changeProfileSettingsRequest(fragment, settings));
    }
  };
}

module.exports = connect(
  mapStateToProps,
  mapDispatchToProps,
)(UserSettingLanguage);
