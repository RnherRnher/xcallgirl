const React = require('react');
const classNames = require('classnames');
const propTypes = require('prop-types');
const joi = require('joi');
const constant = require('../../../../../../commons/utils/constant');
const { connect } = require('react-redux');
const { FormattedMessage } = require('react-intl');
const { createStructuredSelector } = require('reselect');
const {
  List,
  ListSubheader,
  Button,
  Typography
} = require('@material-ui/core');
const { ResultEnum } = require('../../../../../core/client/constants/core.client.constant');
const { changeProfilePasswordRequest } = require('../../../actions/user.client.action');
const { InputVerifyPassword, InputPassword } = require('../../Inputs');
const { changeProfilePassword } = require('../../../../../../commons/modules/users/validates/user.validate');
const { makeSelectChangesUser } = require('../../../reselects/user.client.reselect');

const UserSettingPassword = class UserSettingPassword extends React.Component {
  constructor(props) {
    super(props);

    this.currentPasswordRef = React.createRef();
    this.prePasswordRef = React.createRef();

    this.handleCheck = this.handleCheck.bind(this);

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleCheck(body) {
    const { validate } = this.props;

    return joi.validate(body, validate);
  }

  handleSubmit(event) {
    const { changeProfilePasswordRequest } = this.props;

    const { password } = this.currentPasswordRef.current.state;
    const { currentPassword, verifyPassword } = this.prePasswordRef.current.state;

    this.handleCheck({
      passwordDetails: {
        currentPassword: password,
        newPassword: currentPassword,
        verifyPassword,
      }
    }).then((passwordDetails) => {
      changeProfilePasswordRequest(passwordDetails);
    });

    event.preventDefault();
  }

  initElement() {
    const { className, passwordChange } = this.props;

    const disabled = passwordChange.result === ResultEnum.wait;

    return (
      <div className={classNames('xcg-setting-password-user', className)}>
        <List subheader={<ListSubheader><FormattedMessage id="user.password.change.is" /></ListSubheader>}>
          <form onSubmit={this.handleSubmit}>
            <div className="xcg-group-input">
              <InputPassword
                label={<FormattedMessage id="user.password.current" />}
                ref={this.currentPasswordRef}
              />
              <InputVerifyPassword
                label1={<FormattedMessage id="user.password.new" />}
                ref={this.prePasswordRef}
              />
              <div className="xcg-primary-btn">
                <Button
                  fullWidth
                  type="submit"
                  disabled={disabled}
                  color="secondary"
                >
                  <Typography
                    align="center"
                    variant="button"
                    color="secondary"
                  >
                    <FormattedMessage id="actions.change" />
                  </Typography>
                </Button>
              </div>
            </div>
          </form>
        </List>
      </div>
    );
  }

  render() {
    return this.initElement();
  }
};

UserSettingPassword.propTypes = {
  className: propTypes.string,
  validate: propTypes.any,
  passwordChange: propTypes.object.isRequired,
  changeProfilePasswordRequest: propTypes.func.isRequired,
};

UserSettingPassword.defaultProps = {
  className: '',
  validate: changeProfilePassword.body
};

function mapStateToProps(state, props) {
  return createStructuredSelector({
    passwordChange: makeSelectChangesUser(constant.PASSWORD)
  });
}

function mapDispatchToProps(dispatch, props) {
  return {
    changeProfilePasswordRequest: (passwordDetails) => {
      return dispatch(changeProfilePasswordRequest(passwordDetails));
    }
  };
}

module.exports = connect(
  mapStateToProps,
  mapDispatchToProps,
)(UserSettingPassword);
