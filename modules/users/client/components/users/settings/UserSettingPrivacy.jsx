const React = require('react');
const classNames = require('classnames');
const propTypes = require('prop-types');
const regular = require('../../../../../../commons/utils/regular-expression');
const constant = require('../../../../../../commons/utils/constant');
const { connect } = require('react-redux');
const { FormattedMessage } = require('react-intl');
const { createStructuredSelector } = require('reselect');
const { assignIn } = require('lodash');
const {
  Edit,
  Contacts,
  Cake,
  LocationOn,
  ExpandMore,
  Visibility,
  VisibilityOff,
  Link
} = require('@material-ui/icons');
const {
  List,
  ListSubheader,
  ListItemAvatar,
  ListItemText,
  ListItemSecondaryAction,
  ListItem,
  Avatar,
  MenuItem,
  IconButton,
  Menu,
  ExpansionPanel,
  ExpansionPanelSummary,
  ExpansionPanelDetails,
  ListItemIcon,
} = require('@material-ui/core');
const { changeProfileSettingsRequest } = require('../../../actions/user.client.action');
const { makeSelectSecuritySettings, makeSelectPrivacyUser } = require('../../../reselects/user.client.reselect');

const UserSettingPrivacy = class UserSettingPrivacy extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      clicked: null,
      anchorEl: null
    };

    this.handleClickEdit = this.handleClickEdit.bind(this);
    this.handleClickMenuItem = this.handleClickMenuItem.bind(this);
    this.handleCloseMenu = this.handleCloseMenu.bind(this);
  }

  handleClickEdit(clicked) {
    return (event) => {
      this.setState({
        clicked,
        anchorEl: event.target
      });
    };
  }

  handleClickMenuItem(type, index) {
    return (event) => {
      const {
        account,
        datas,
        changeProfileSettingsRequest
      } = this.props;

      const { visible } = account;
      visible[type] = datas.visible[index];

      changeProfileSettingsRequest(
        constant.SECURITY,
        {
          settings: {
            account: assignIn(
              account,
              { visible }
            )
          }
        }
      );

      this.handleCloseMenu();
    };
  }

  handleCloseMenu(event) {
    this.setState({
      clicked: false,
      anchorEl: null
    });
  }

  VisiblesPrivacy(type) {
    const { datas } = this.props;

    const LinkIcon = Link;
    const IconList = [
      <VisibilityOff />,
      <LinkIcon />,
      <Visibility />
    ];

    return datas.visible.map((value, index) => {
      return (
        <MenuItem
          key={value}
          onClick={this.handleClickMenuItem(type, index)}
          value={value}
        >
          <ListItemIcon>
            {IconList[index]}
          </ListItemIcon>
          <ListItemText
            inset
            primary={<FormattedMessage id={`info.security.visible.${value}`} />}
          />
        </MenuItem>
      );
    });
  }

  Review(type, visible) {
    const Item = (primary, secondary, type = null, isDivider = false) => {
      return (
        <ListItem divider={isDivider}>
          <ListItemText
            inset
            primary={primary}
            secondary={secondary}
          />
          {type
            ? <ListItemSecondaryAction>
              <IconButton onClick={this.handleClickEdit(type)}>
                <Edit />
              </IconButton>
            </ListItemSecondaryAction>
            : null
          }
        </ListItem>
      );
    };

    const EmptyItem = Item(
      <FormattedMessage id="info.security.visible.is" />,
      <FormattedMessage id="info.notExist" />,
      type
    );

    switch (type) {
      case constant.CONTACTS: {
        const { value } = this.props;
        let contactsVisible = null;

        switch (visible) {
          case constant.FULL:
            contactsVisible = value.contacts;
            break;
          case constant.SHORTCUT:
            contactsVisible = {
              ...value.contacts,
              numberPhone: null
            };
            break;
          case constant.NONE:
            break;
          default:
            break;
        }

        return <div>
          {
            contactsVisible
              ? Item(
                <FormattedMessage id="user.numberPhone.is" />,
                contactsVisible.numberPhone
                  ? `${contactsVisible.numberPhone.code} ${contactsVisible.numberPhone.number}`
                  : <FormattedMessage id="info.notExist" />,
                type
              )
              : EmptyItem
          }
        </div>;
      }
      case constant.BIRTHDAY: {
        const { value } = this.props;
        let birthdayVisible = null;

        switch (visible) {
          case constant.FULL:
            birthdayVisible = value.birthday;
            break;
          case constant.SHORTCUT:
            birthdayVisible = {
              day: null,
              month: null,
              year: value.birthday.year,
            };
            break;
          case constant.NONE:
            break;
          default:
            break;
        }

        return <div>
          {
            birthdayVisible
              ? Item(
                <FormattedMessage id="info.security.visible.is" />,
                (birthdayVisible.day !== null && birthdayVisible.month !== null)
                  ? `${birthdayVisible.day + 1} / ${birthdayVisible.month + 1} / ${birthdayVisible.year}`
                  : birthdayVisible.year,
                type
              )
              : EmptyItem
          }
        </div>;
      }
      case constant.ADDRESS: {
        const { value } = this.props;
        let addressVisible = {};

        switch (visible) {
          case constant.FULL:
            addressVisible.country = value.address.country ? value.address.country : <FormattedMessage id="info.notExist" />;
            addressVisible.city = value.address.city ? value.address.city : <FormattedMessage id="info.notExist" />;
            addressVisible.county = value.address.county ? value.address.county : <FormattedMessage id="info.notExist" />;
            addressVisible.local = value.address.local ? value.address.local : <FormattedMessage id="info.notExist" />;
            break;
          case constant.SHORTCUT:
            addressVisible.country = value.address.country ? value.address.country : <FormattedMessage id="info.notExist" />;
            addressVisible.city = value.address.city ? value.address.city : <FormattedMessage id="info.notExist" />;
            addressVisible.county = <FormattedMessage id="info.notExist" />;
            addressVisible.local = <FormattedMessage id="info.notExist" />;
            break;
          case constant.NONE:
            addressVisible = null;
            break;
          default:
            break;
        }

        return (
          addressVisible
            ? <div>
              {Item(<FormattedMessage id="info.address.country.is" />, addressVisible.country, type, true)}
              {Item(<FormattedMessage id="info.address.city.is" />, addressVisible.city, null, true)}
              {Item(<FormattedMessage id="info.address.county.is" />, addressVisible.county, null, true)}
              {Item(<FormattedMessage id="info.address.local.is" />, addressVisible.local)}
            </div>
            : EmptyItem
        );
      }
      default:
        break;
    }
  }

  Privacy(type, icon, name, visible) {
    const { clicked, anchorEl } = this.state;

    return {
      ListItem: (
        <ExpansionPanel>
          <ExpansionPanelSummary expandIcon={<ExpandMore />}>
            <ListItem>
              <ListItemAvatar>
                <Avatar>
                  {icon}
                </Avatar>
              </ListItemAvatar>
              <ListItemText
                inset
                primary={<FormattedMessage id={name} />}
                secondary={<FormattedMessage id={`info.security.visible.${visible}`} />}
              />
            </ListItem>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails className="xcg-info-group">
            <List>
              {this.Review(type, visible)}
            </List>
          </ExpansionPanelDetails>
        </ExpansionPanel>
      ),
      Menu: (
        <Menu
          anchorEl={anchorEl}
          open={clicked === type}
          onClose={this.handleCloseMenu}
        >
          {this.VisiblesPrivacy(type)}
        </Menu>
      )
    };
  }

  initElement() {
    const { className, account } = this.props;

    const ContactsPrivacy = this.Privacy(
      constant.CONTACTS,
      <Contacts />,
      'info.contacts.is',
      account.visible.contacts
    );
    const BirthdayPrivacy = this.Privacy(
      constant.BIRTHDAY,
      <Cake />,
      'user.birthday.is',
      account.visible.birthday
    );
    const AddressPrivacy = this.Privacy(
      constant.ADDRESS,
      <LocationOn />,
      'info.address.is',
      account.visible.address
    );

    return (
      <div className={classNames('xcg-setting-privacy-user', className)}>
        <List
          dense
          subheader={<ListSubheader><FormattedMessage id="info.security.privacy.is" /></ListSubheader>}
        >
          {ContactsPrivacy.ListItem}
          {BirthdayPrivacy.ListItem}
          {AddressPrivacy.ListItem}
        </List>
        {ContactsPrivacy.Menu}
        {BirthdayPrivacy.Menu}
        {AddressPrivacy.Menu}
      </div>
    );
  }

  render() {
    return this.initElement();
  }
};

UserSettingPrivacy.propTypes = {
  className: propTypes.string,
  value: propTypes.object.isRequired,
  account: propTypes.object.isRequired,
  datas: propTypes.object.isRequired,
  changeProfileSettingsRequest: propTypes.func.isRequired
};

UserSettingPrivacy.defaultProps = {
  className: ''
};

function mapStateToProps(state, props) {
  return createStructuredSelector({
    value: makeSelectPrivacyUser(),
    account: makeSelectSecuritySettings(constant.ACCOUNT),
    datas: () => {
      return {
        visible: regular.commons.visible
      };
    }
  });
}

function mapDispatchToProps(dispatch, props) {
  return {
    changeProfileSettingsRequest: (fragment, settings) => {
      return dispatch(changeProfileSettingsRequest(fragment, settings));
    }
  };
}

module.exports = connect(
  mapStateToProps,
  mapDispatchToProps,
)(UserSettingPrivacy);
