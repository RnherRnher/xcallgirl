const React = require('react');
const classNames = require('classnames');
const propTypes = require('prop-types');
const joi = require('joi');
const constant = require('../../../../../../commons/utils/constant');
const { connect } = require('react-redux');
const { FormattedMessage, FormattedDate } = require('react-intl');
const { createStructuredSelector } = require('reselect');
const { isEmpty } = require('lodash');
const {
  LocationOn,
  Edit,
  ExpandMore,
  Person,
  Contacts,
  CheckCircle,
  VerifiedUser,
  Error,
  Close
} = require('@material-ui/icons');
const {
  List,
  ListSubheader,
  ListItemAvatar,
  ListItemText,
  ListItemSecondaryAction,
  ListItem,
  Avatar,
  IconButton,
  ExpansionPanel,
  ExpansionPanelSummary,
  ExpansionPanelDetails,
  Button,
  Tooltip,
  Typography,
  Dialog,
  Slide,
  DialogActions
} = require('@material-ui/core');
const { ResultEnum } = require('../../../../../core/client/constants/core.client.constant');
const { changeProfileNumberPhone, changeProfileAddress } = require('../../../../../../commons/modules/users/validates/user.validate');
const { SelectAddress } = require('../../../../../core/client/components/Selects');
const { makeSelectChangesUser, makeSelectSelfUserInfo } = require('../../../reselects/user.client.reselect');
const {
  InputNumberPhone,
  InputPassword,
  InputImage
} = require('../../Inputs');
const {
  changeProfileNumberPhoneRequest,
  changeProfileAddressRequest,
  changeProfileAvatarRequest,
  deleteProfileAvatarRequest
} = require('../../../actions/user.client.action');

const UserSettingInfoAccount = class UserSettingInfoAccount extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      changed: null,
      tooltiped: null
    };

    this.passwordRef = React.createRef();
    this.numberPhoneRef = React.createRef();
    this.addressRef = React.createRef();

    this.handleCheck = this.handleCheck.bind(this);
    this.handleClickEdit = this.handleClickEdit.bind(this);
    this.handleClickTooltipClose = this.handleClickTooltipClose.bind(this);
    this.handleClickTooltipOpen = this.handleClickTooltipOpen.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }

  handleCheck(body, validate) {
    return joi.validate(body, validate);
  }

  handleClick(type) {
    return (event) => {
      const {
        validate,
        changeProfileNumberPhoneRequest,
        changeProfileAddressRequest,
        changeProfileAvatarRequest,
        deleteProfileAvatarRequest
      } = this.props;

      switch (type) {
        case constant.NUMBERPHONE: {
          const { number, code } = this.numberPhoneRef.current.state;
          const { password } = this.passwordRef.current.state;

          const body = {
            password,
            numberPhone: {
              code,
              number
            }
          };

          this.handleCheck(body, validate.changeProfileNumberPhone)
            .then((numberPhone) => {
              changeProfileNumberPhoneRequest(numberPhone);
              this.handleClickEdit(type)();
            });
        }
          break;
        case constant.ADDRESS: {
          const {
            country,
            city,
            county,
            local
          } = this.addressRef.current.state;

          const body = {
            address: { country }
          };

          if (city) {
            body.address.city = city;
          }

          if (county) {
            body.address.county = county;
          }

          if (local) {
            body.address.local = local;
          }

          this.handleCheck(body, validate.changeProfileAddress)
            .then((address) => {
              changeProfileAddressRequest(address);
              this.handleClickEdit(type)();
            });
        }
          break;
        case constant.AVATAR:
          changeProfileAvatarRequest(event);
          this.handleClickEdit(type)();
          break;
        case constant.AVATAR_DETELE:
          deleteProfileAvatarRequest(event);
          this.handleClickEdit(type)();
          break;
        default:
          break;
      }
    };
  }

  handleClickEdit(changed) {
    return (event) => {
      this.setState((state, props) => {
        return { changed: state.changed === changed ? false : changed };
      });
    };
  }

  handleClickTooltipOpen(tooltiped) {
    return (event) => {
      this.setState({ tooltiped });
    };
  }

  handleClickTooltipClose(event) {
    this.setState({ tooltiped: false });
  }

  Personal() {
    const { value } = this.props;
    const { tooltiped } = this.state;

    return (
      <ExpansionPanel>
        <ExpansionPanelSummary expandIcon={<ExpandMore />}>
          <ListItem>
            <ListItemAvatar>
              <Avatar>
                <Person />
              </Avatar>
            </ListItemAvatar>
            <ListItemText
              inset
              primary={<FormattedMessage id="user.is" />}
              secondary={value.name.display}
            />
          </ListItem>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails className="xcg-info-group">
          <List dense>
            <ListItem divider>
              <ListItemAvatar>
                <Avatar
                  alt={`${value.name.nick}_avatar`}
                  src={value.avatar.url}
                />
              </ListItemAvatar>
              <ListItemText
                inset
                primary={value.name.nick}
                secondary={<FormattedMessage id={`user.roles.${value.roles.value}`} />}
              />
              <ListItemSecondaryAction>
                <Tooltip
                  placement="top"
                  onClose={this.handleClickTooltipClose}
                  open={tooltiped === constant.VERIFY_ACCOUNT}
                  title={
                    <FormattedMessage id={value.verify.account.is
                      ? 'user.verify.account.exist'
                      : 'user.verify.account.notExist'}
                    />
                  }
                >
                  <IconButton onClick={this.handleClickTooltipOpen(constant.VERIFY_ACCOUNT)}>
                    {
                      value.verify.account.is
                        ? <VerifiedUser className="xcg-info" />
                        : <Error className="xcg-error" />
                    }
                  </IconButton>
                </Tooltip>
                <IconButton onClick={this.handleClickEdit(constant.AVATAR)}>
                  <Edit />
                </IconButton>
              </ListItemSecondaryAction>
            </ListItem>
            <ListItem divider>
              <ListItemText
                inset
                primary={<FormattedMessage id="user.name.first.is" />}
                secondary={value.name.first}
              />
              <ListItemText
                inset
                primary={<FormattedMessage id="user.name.last.is" />}
                secondary={value.name.last}
              />
            </ListItem>
            <ListItem divider>
              <ListItemText
                inset
                primary={<FormattedMessage id="user.gender.is" />}
                secondary={<FormattedMessage id={`user.gender.${value.gender}`} />}
              />
            </ListItem>
            <ListItem divider>
              <ListItemText
                inset
                primary={<FormattedMessage id="user.birthday.is" />}
                secondary={`${value.birthday.day} / ${value.birthday.month + 1} / ${value.birthday.year}`}
              />
            </ListItem>
            <ListItem>
              <ListItemText
                inset
                primary={<FormattedMessage id="info.time.init" />}
                secondary={
                  <FormattedDate
                    weekday="narrow"
                    year="numeric"
                    month="numeric"
                    day="numeric"
                    hour="numeric"
                    minute="numeric"
                    second="numeric"
                    value={value.initDate}
                  />
                }
              />
            </ListItem>
          </List>
        </ExpansionPanelDetails>
      </ExpansionPanel>
    );
  }

  Contact() {
    const { changed } = this.state;

    return (
      <ExpansionPanel>
        <ExpansionPanelSummary expandIcon={<ExpandMore />}>
          <ListItem>
            <ListItemAvatar>
              <Avatar>
                <Contacts />
              </Avatar>
            </ListItemAvatar>
            <ListItemText
              inset
              primary={<FormattedMessage id="info.contacts.is" />}
            />
          </ListItem>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails className="xcg-info-group">
          {changed === constant.NUMBERPHONE ? this.NumberPhoneEdit() : this.NumberPhoneInfo()}
        </ExpansionPanelDetails>
      </ExpansionPanel>
    );
  }

  NumberPhoneInfo() {
    const { value } = this.props;
    const { tooltiped } = this.state;

    return (
      <List dense>
        <ListItem>
          <ListItemText
            inset
            primary={<FormattedMessage id="user.numberPhone.is" />}
            secondary={`${value.contacts.numberPhone.code} ${value.contacts.numberPhone.number}`}
          />
          <ListItemSecondaryAction>
            <Tooltip
              placement="top"
              onClose={this.handleClickTooltipClose}
              open={tooltiped === constant.VERIFY_NUMBERPHONE}
              title={
                <FormattedMessage id={value.verify.numberPhone.is
                  ? 'user.numberPhone.verify.success'
                  : 'user.numberPhone.verify.notExist'}
                />
              }
            >
              <IconButton onClick={this.handleClickTooltipOpen(constant.VERIFY_NUMBERPHONE)}>
                {
                  value.verify.numberPhone.is
                    ? <CheckCircle className="xcg-info" />
                    : <Error className="xcg-error" />
                }
              </IconButton>
            </Tooltip>
            <IconButton onClick={this.handleClickEdit(constant.NUMBERPHONE)}>
              <Edit />
            </IconButton>
          </ListItemSecondaryAction>
        </ListItem>
      </List>
    );
  }

  NumberPhoneEdit() {
    const { value, numberPhoneChange } = this.props;

    const disabled = numberPhoneChange.result === ResultEnum.wait;

    return (
      <List
        className="xcg-change-group"
        dense
        subheader={
          <ListSubheader>
            <FormattedMessage id="user.numberPhone.change.is" />
          </ListSubheader>
        }
      >
        <ListItem>
          <ListItemSecondaryAction>
            <IconButton onClick={this.handleClickEdit(constant.NUMBERPHONE)}>
              <Close />
            </IconButton>
          </ListItemSecondaryAction>
        </ListItem>
        <div className="xcg-group-input">
          <InputNumberPhone
            check
            ref={this.numberPhoneRef}
            value={isEmpty(value.contacts.numberPhone) ? null : value.contacts.numberPhone}
          />
          <InputPassword ref={this.passwordRef} />
          <div className="xcg-primary-btn">
            <Button
              fullWidth
              disabled={disabled}
              color="secondary"
              onClick={this.handleClick(constant.NUMBERPHONE)}
            >
              <Typography
                align="center"
                variant="button"
                color="secondary"
              >
                <FormattedMessage id="actions.change" />
              </Typography>
            </Button>
          </div>
        </div>
      </List>
    );
  }

  Address() {
    const { changed } = this.state;

    return (
      <ExpansionPanel>
        <ExpansionPanelSummary expandIcon={<ExpandMore />}>
          <ListItem>
            <ListItemAvatar>
              <Avatar>
                <LocationOn />
              </Avatar>
            </ListItemAvatar>
            <ListItemText
              inset
              primary={<FormattedMessage id="info.address.is" />}
            />
          </ListItem>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails className="xcg-info-group">
          {changed === constant.ADDRESS ? this.AddressEdit() : this.AddressInfo()}
        </ExpansionPanelDetails>
      </ExpansionPanel>
    );
  }

  AddressInfo() {
    const { value } = this.props;

    return (
      <List dense>
        <ListItem>
          <ListItemSecondaryAction>
            <IconButton onClick={this.handleClickEdit(constant.ADDRESS)}>
              <Edit />
            </IconButton>
          </ListItemSecondaryAction>
        </ListItem>
        <ListItem divider>
          <ListItemText
            inset
            primary={<FormattedMessage id="info.address.country.is" />}
            secondary={value.address.country}
          />
        </ListItem>
        <ListItem divider>
          <ListItemText
            inset
            primary={<FormattedMessage id="info.address.city.is" />}
            secondary={value.address.city || <FormattedMessage id="info.notExist" />}
          />
        </ListItem>
        <ListItem divider>
          <ListItemText
            inset
            primary={<FormattedMessage id="info.address.county.is" />}
            secondary={value.address.county || <FormattedMessage id="info.notExist" />}
          />
        </ListItem>
        <ListItem>
          <ListItemText
            inset
            primary={<FormattedMessage id="info.address.local.is" />}
            secondary={value.address.local || <FormattedMessage id="info.notExist" />}
          />
        </ListItem>
      </List>
    );
  }

  AddressEdit() {
    const { value, addressChange } = this.props;

    const disabled = addressChange.result === ResultEnum.wait;

    return (
      <List
        dense
        subheader={
          <ListSubheader>
            <FormattedMessage id="user.address.change.is" />
          </ListSubheader>
        }
      >
        <ListItem>
          <ListItemSecondaryAction>
            <IconButton onClick={this.handleClickEdit(constant.ADDRESS)}>
              <Close />
            </IconButton>
          </ListItemSecondaryAction>
        </ListItem>
        <div className="xcg-group-input">
          <SelectAddress
            check
            ref={this.addressRef}
            value={isEmpty(value.address.root) ? null : value.address.root}
          />
          <div className="xcg-primary-btn">
            <Button
              fullWidth
              disabled={disabled}
              color="secondary"
              onClick={this.handleClick(constant.ADDRESS)}
            >
              <Typography
                align="center"
                variant="button"
                color="secondary"
              >
                <FormattedMessage id="actions.change" />
              </Typography>
            </Button>
          </div>
        </div>
      </List>
    );
  }

  Transition(props) {
    return <Slide direction="up" {...props} />;
  }

  DialogChangeAvatar() {
    const { value, avatarChange } = this.props;
    const { changed } = this.state;

    const disabled = avatarChange.result === ResultEnum.wait;

    return (
      <Dialog
        open={changed === constant.AVATAR}
        onClose={this.handleClickEdit(constant.AVATAR)}
        TransitionComponent={this.Transition}
      >
        <InputImage
          disabled={disabled}
          nameLabel={<FormattedMessage id="user.avatar.change.is" />}
          handleClickUpload={this.handleClick(constant.AVATAR)}
        />
        <DialogActions>
          {
            value.avatar.publicID
              ? <Button
                disabled={disabled}
                onClick={this.handleClick(constant.AVATAR_DELETE)}
                color="primary"
              >
                <FormattedMessage id="actions.delete" />
              </Button>
              : null
          }
          <Button
            disabled={disabled}
            onClick={this.handleClickEdit(constant.AVATAR)}
          >
            <FormattedMessage id="actions.back" />
          </Button>
        </DialogActions>
      </Dialog>
    );
  }

  initElement() {
    const { className } = this.props;

    return (
      <div className={classNames('xcg-setting-info-account-user', className)}>
        <List
          dense
          subheader={<ListSubheader><FormattedMessage id="info.account.is" /></ListSubheader>}
        >
          {this.Personal()}
          {this.Contact()}
          {this.Address()}
        </List>
        {this.DialogChangeAvatar()}
      </div>
    );
  }

  render() {
    return this.initElement();
  }
};

UserSettingInfoAccount.propTypes = {
  className: propTypes.string,
  validate: propTypes.object,
  avatarChange: propTypes.object.isRequired,
  addressChange: propTypes.object.isRequired,
  numberPhoneChange: propTypes.object.isRequired,
  value: propTypes.object.isRequired,
  changeProfileNumberPhoneRequest: propTypes.func.isRequired,
  changeProfileAddressRequest: propTypes.func.isRequired,
  changeProfileAvatarRequest: propTypes.func.isRequired,
  deleteProfileAvatarRequest: propTypes.func.isRequired,
};

UserSettingInfoAccount.defaultProps = {
  className: '',
  validate: {
    changeProfileNumberPhone: changeProfileNumberPhone.body,
    changeProfileAddress: changeProfileAddress.body
  }
};

function mapStateToProps(state, props) {
  return createStructuredSelector({
    avatarChange: makeSelectChangesUser(constant.AVATAR),
    addressChange: makeSelectChangesUser(constant.ADDRESS),
    numberPhoneChange: makeSelectChangesUser(constant.NUMBERPHONE),
    value: makeSelectSelfUserInfo()
  });
}

function mapDispatchToProps(dispatch, props) {
  return {
    changeProfileNumberPhoneRequest: (numberPhone) => {
      return dispatch(changeProfileNumberPhoneRequest(numberPhone));
    },
    changeProfileAddressRequest: (body) => {
      return dispatch(changeProfileAddressRequest(body));
    },
    changeProfileAvatarRequest: (image) => {
      return dispatch(changeProfileAvatarRequest(image));
    },
    deleteProfileAvatarRequest: () => {
      return dispatch(deleteProfileAvatarRequest());
    }
  };
}

module.exports = connect(
  mapStateToProps,
  mapDispatchToProps,
)(UserSettingInfoAccount);
