const React = require('react');
const classNames = require('classnames');
const propTypes = require('prop-types');
const joi = require('joi');
const constant = require('../../../../../../commons/utils/constant');
const AlertSnackbar = require('../../../../../core/client/components/alerts/AlertSnackbar');
const { connect } = require('react-redux');
const { createStructuredSelector } = require('reselect');
const { FormattedMessage } = require('react-intl');
const {
  HowToReg,
  ExpandMore,
  Textsms
} = require('@material-ui/icons');
const {
  List,
  ListSubheader,
  ListItemAvatar,
  ListItemText,
  ListItem,
  Avatar,
  ExpansionPanel,
  ExpansionPanelSummary,
  ExpansionPanelDetails,
  Divider,
  Button,
  Typography,
  TextField
} = require('@material-ui/core');
const { InputImage } = require('../../Inputs');
const { verifyNumberPhone } = require('../../../../../../commons/modules/users/validates/user.validate');
const { ResultEnum } = require('../../../../../core/client/constants/core.client.constant');
const {
  requestVerifyAccountRequest,
  getVerifyTokenNumberPhoneRequest,
  verifyNumberPhoneRequest,
} = require('../../../actions/user.client.action');
const { makeSelectVerifyUser, makeSelectSelfUserInfo } = require('../../../reselects/user.client.reselect');

const UserSettingVerify = class UserSettingVerify extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      verifyTokenNumberPhone: ''
    };

    this.handleClickVerifyAccount = this.handleClickVerifyAccount.bind(this);
    this.handleClickGetTokenNumberPhone = this.handleClickGetTokenNumberPhone.bind(this);
    this.handleChangeTokenNumberPhone = this.handleChangeTokenNumberPhone.bind(this);
    this.handleClickVerifyNumberPhone = this.handleClickVerifyNumberPhone.bind(this);
  }

  handleClickVerifyAccount(image) {
    const { requestVerifyAccountRequest } = this.props;
    requestVerifyAccountRequest(image);
  }

  Account() {
    const { user, accountVerify } = this.props;

    let isVerify = user.verify.account.is;
    let status = null;
    let Note = null;

    switch (user.verify.account.status) {
      case constant.NONE:
        status = 'user.verify.account.notExist';
        Note = <AlertSnackbar message={<FormattedMessage id="user.verify.account.request.note.none" />} />;
        break;
      case constant.WAIT:
        status = 'user.verify.account.request.exist';
        isVerify = true;
        Note = <AlertSnackbar message={<FormattedMessage id="user.verify.account.request.note.wait" />} />;
        break;
      case constant.SUCCESS:
        status = 'user.verify.account.exist';
        break;
      case constant.FAILED:
        status = 'user.verify.account.failed';
        Note = <AlertSnackbar
          variant="error"
          message={user.verify.account.reason}
        />;
        break;
      default:
        break;
    }

    const disabled = accountVerify.request.result === ResultEnum.wait;

    return (
      <div className="xcg-account">
        <ExpansionPanel>
          <ExpansionPanelSummary expandIcon={<ExpandMore />}>
            <ListItem>
              <ListItemAvatar>
                <Avatar>
                  <HowToReg />
                </Avatar>
              </ListItemAvatar>
              <ListItemText
                inset
                primary={<FormattedMessage id="user.is" />}
                secondary={<FormattedMessage id={status} />}
              />
            </ListItem>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails className="xcg-panel-details">
            <Divider />
            {
              isVerify
                ? null
                : <InputImage
                  disabled={disabled}
                  handleClickUpload={this.handleClickVerifyAccount}
                  nameLabel={<FormattedMessage id="user.verify.account.name" />}
                  actionLabel={<FormattedMessage id="user.verify.account.upload.is" />}
                />
            }
            {Note}
          </ExpansionPanelDetails>
        </ExpansionPanel>
      </div>
    );
  }

  handleClickGetTokenNumberPhone(event) {
    const { getVerifyTokenNumberPhoneRequest } = this.props;
    getVerifyTokenNumberPhoneRequest();
  }

  handleChangeTokenNumberPhone(event) {
    const { value } = event.target;

    this.setState({ verifyTokenNumberPhone: value });
  }

  handleCheckVerifyNumberPhone(body) {
    const { validate } = this.props;

    return joi.validate(body, validate.verifyNumberPhone);
  }

  handleClickVerifyNumberPhone(event) {
    const { verifyNumberPhoneRequest } = this.props;
    const { verifyTokenNumberPhone } = this.state;

    this.handleCheckVerifyNumberPhone({ verifyTokenNumberPhone })
      .then((verifyTokenNumberPhone) => {
        verifyNumberPhoneRequest(verifyTokenNumberPhone);
      });
  }

  InputVerifyNumberPhone() {
    const { numberPhoneVerify } = this.props;
    const { verifyTokenNumberPhone } = this.state;

    const disabled = numberPhoneVerify.verify.result === ResultEnum.wait;

    return (
      <div>
        <TextField
          className="xcg-input-verify-numberphone"
          label={<FormattedMessage id="info.security.token.is" />}
          type="text"
          fullWidth
          value={verifyTokenNumberPhone}
          disabled={disabled}
          onChange={this.handleChangeTokenNumberPhone}
        />
        <div className="xcg-primary-btn">
          <Button
            fullWidth
            onClick={this.handleClickVerifyNumberPhone}
            disabled={disabled}
            color="secondary"
          >
            <Typography
              align="center"
              variant="button"
              color="secondary"
            >
              <FormattedMessage id="actions.vetify" />
            </Typography>
          </Button>
        </div>
      </div>
    );
  }

  NumberPhone() {
    const { user, numberPhoneVerify } = this.props;

    const isVerify = user.verify.numberPhone.is;
    const disabled = numberPhoneVerify.getToken.result === ResultEnum.wait;

    return (
      <div className="xcg-numberphone">
        <ExpansionPanel>
          <ExpansionPanelSummary expandIcon={<ExpandMore />}>
            <ListItem>
              <ListItemAvatar>
                <Avatar>
                  <Textsms />
                </Avatar>
              </ListItemAvatar>
              <ListItemText
                inset
                primary={<FormattedMessage id="user.numberPhone.is" />}
                secondary={
                  <FormattedMessage
                    id={
                      isVerify
                        ? 'user.numberPhone.verify.exist'
                        : 'user.numberPhone.verify.notExist'
                    }
                  />
                }
              />
            </ListItem>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails className="xcg-panel-details">
            <Divider />
            {
              isVerify
                ? null
                : <div className="xcg-group-input">
                  {this.InputVerifyNumberPhone()}
                  <div className="xcg-primary-btn">
                    <Button
                      fullWidth
                      onClick={this.handleClickGetTokenNumberPhone}
                      disabled={disabled}
                    >
                      <Typography
                        align="center"
                        variant="button"
                      >
                        <FormattedMessage id="actions.getToken" />
                      </Typography>
                    </Button>
                  </div>
                </div>
            }
          </ExpansionPanelDetails>
        </ExpansionPanel>
      </div>
    );
  }

  initElement() {
    const { className } = this.props;

    return (
      <div className={classNames('xcg-setting-verify-user', className)}>
        <List
          dense
          subheader={<ListSubheader><FormattedMessage id="info.security.verify.is" /></ListSubheader>}
        >
          {this.Account()}
          {this.NumberPhone()}
        </List>
      </div>
    );
  }

  render() {
    return this.initElement();
  }
};

UserSettingVerify.propTypes = {
  className: propTypes.string,
  validate: propTypes.object,
  user: propTypes.object.isRequired,
  accountVerify: propTypes.object.isRequired,
  numberPhoneVerify: propTypes.object.isRequired,
  requestVerifyAccountRequest: propTypes.func.isRequired,
  getVerifyTokenNumberPhoneRequest: propTypes.func.isRequired,
  verifyNumberPhoneRequest: propTypes.func.isRequired
};

UserSettingVerify.defaultProps = {
  className: '',
  validate: {
    verifyNumberPhone: verifyNumberPhone.body
  }
};

function mapStateToProps(state, props) {
  return createStructuredSelector({
    accountVerify: makeSelectVerifyUser(constant.ACCOUNT),
    numberPhoneVerify: makeSelectVerifyUser(constant.NUMBERPHONE),
    user: makeSelectSelfUserInfo()
  });
}

function mapDispatchToProps(dispatch, props) {
  return {
    requestVerifyAccountRequest: (image) => {
      return dispatch(requestVerifyAccountRequest(image));
    },
    getVerifyTokenNumberPhoneRequest: () => {
      return dispatch(getVerifyTokenNumberPhoneRequest());
    },
    verifyNumberPhoneRequest: (verifyTokenNumberPhone) => {
      return dispatch(verifyNumberPhoneRequest(verifyTokenNumberPhone));
    }
  };
}

module.exports = connect(
  mapStateToProps,
  mapDispatchToProps,
)(UserSettingVerify);
