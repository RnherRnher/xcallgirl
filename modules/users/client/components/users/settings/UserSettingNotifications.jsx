const React = require('react');
const classNames = require('classnames');
const propTypes = require('prop-types');
const constant = require('../../../../../../commons/utils/constant');
const { assignIn } = require('lodash');
const { connect } = require('react-redux');
const { FormattedMessage } = require('react-intl');
const { createStructuredSelector } = require('reselect');
const { Textsms } = require('@material-ui/icons');
const {
  List,
  ListSubheader,
  ListItemAvatar,
  ListItemText,
  ListItemSecondaryAction,
  Switch,
  ListItem,
  Avatar
} = require('@material-ui/core');
const { changeProfileSettingsRequest } = require('../../../actions/user.client.action');
const { makeSelectGeneralSettings } = require('../../../reselects/user.client.reselect');

const UserSettingNotifications = class UserSettingNotifications extends React.Component {
  constructor(props) {
    super(props);

    this.handleToggle = this.handleToggle.bind(this);
  }

  handleToggle(type) {
    return (event) => {
      const { changeProfileSettingsRequest, notifications } = this.props;

      changeProfileSettingsRequest(
        constant.GENERAL,
        {
          settings: {
            notifications: assignIn(
              notifications,
              { [type]: !notifications[type] }
            )
          }
        }
      );
    };
  }

  initElement() {
    const { className, notifications } = this.props;

    return (
      <div className={classNames('xcg-setting-notification-user', className)}>
        <List subheader={<ListSubheader><FormattedMessage id="notification.is" /></ListSubheader>}>
          <ListItem>
            <ListItemAvatar>
              <Avatar>
                <Textsms />
              </Avatar>
            </ListItemAvatar>
            <ListItemText inset primary={<FormattedMessage id="notification.sms.is" />} />
            <ListItemSecondaryAction>
              <Switch
                onChange={this.handleToggle(constant.SMS)}
                checked={notifications.sms}
              />
            </ListItemSecondaryAction>
          </ListItem>
        </List>
      </div>
    );
  }

  render() {
    return this.initElement();
  }
};

UserSettingNotifications.propTypes = {
  className: propTypes.string,
  notifications: propTypes.object.isRequired,
  changeProfileSettingsRequest: propTypes.func.isRequired,
};

UserSettingNotifications.defaultProps = {
  className: ''
};

function mapStateToProps(state, props) {
  return createStructuredSelector({
    notifications: makeSelectGeneralSettings(constant.NOTIFICATIONS),
  });
}

function mapDispatchToProps(dispatch, props) {
  return {
    changeProfileSettingsRequest: (fragment, settings) => {
      return dispatch(changeProfileSettingsRequest(fragment, settings));
    }
  };
}

module.exports = connect(
  mapStateToProps,
  mapDispatchToProps,
)(UserSettingNotifications);
