const React = require('react');
const propTypes = require('prop-types');
const classNames = require('classnames');
const DetailUser = require('./DetailUser');
const { connect } = require('react-redux');
const { createStructuredSelector } = require('reselect');
const { getUserIDRequest } = require('../../actions/user.client.action');
const { Loadable } = require('../../../../core/client/components/Loadables');
const { LoadableDetailUser } = require('../Loadables');
const { makeSelectDetailUser } = require('../../reselects/user.client.reselect');
const { makeSelectLocation } = require('../../../../core/client/reselects/router.client.reselect');
const { ContainerNotData } = require('../../../../core/client/components/Containers');

const CommonDetailUser = class CommonDetailUser extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    const { getUserIDRequest, location } = this.props;

    const { search } = location;
    const userID = search.slice(search.indexOf('=') + 1);

    getUserIDRequest(userID);
  }

  initElement() {
    const { className, user } = this.props;

    return (
      <div className={classNames('xcg-common-detail-user', className)}>
        <Loadable
          result={user.result}
          WaitNode={<LoadableDetailUser />}
          SuccessNode={<DetailUser value={user.data} />}
          FailedNode={
            <div>
              <ContainerNotData />
              <LoadableDetailUser />
            </div>
          }
        />
      </div>
    );
  }

  render() {
    return this.initElement();
  }
};

CommonDetailUser.propTypes = {
  className: propTypes.string,
  user: propTypes.object.isRequired,
  location: propTypes.object.isRequired,
  getUserIDRequest: propTypes.func.isRequired,
};

CommonDetailUser.defaultProps = {
  className: ''
};

function mapStateToProps(state, props) {
  return createStructuredSelector({
    user: makeSelectDetailUser(),
    location: makeSelectLocation(),
  });
}

function mapDispatchToProps(dispatch, props) {
  return {
    getUserIDRequest: (userID) => {
      return dispatch(getUserIDRequest(userID));
    }
  };
}

module.exports = connect(
  mapStateToProps,
  mapDispatchToProps,
)(CommonDetailUser);
