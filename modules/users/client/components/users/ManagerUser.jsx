const React = require('react');
const propTypes = require('prop-types');
const classNames = require('classnames');
const DetailUser = require('./DetailUser');
const { connect } = require('react-redux');
const { createStructuredSelector } = require('reselect');
const { Loadable } = require('../../../../core/client/components/Loadables');
const { LoadableDetailUser } = require('../Loadables');
const { makeSelectSelfUser } = require('../../../../users/client/reselects/user.client.reselect');
const { ContainerNotData } = require('../../../../core/client/components/Containers');

const ManagerUser = class ManagerUser extends React.Component {
  constructor(props) {
    super(props);
  }

  initElement() {
    const {
      className,
      user,
    } = this.props;

    return (
      <div className={classNames('xcg-manager-user', className)}>
        <Loadable
          result={user.result}
          SuccessNode={<DetailUser value={user.data} />}
          WaitNode={<LoadableDetailUser />}
          FailedNode={
            <div>
              <ContainerNotData />
              <LoadableDetailUser />
            </div>
          }
        />
      </div>
    );
  }

  render() {
    return this.initElement();
  }
};

ManagerUser.propTypes = {
  className: propTypes.string,
  user: propTypes.object.isRequired,
};

ManagerUser.defaultProps = {
  className: ''
};

function mapStateToProps(state, props) {
  return createStructuredSelector({
    user: makeSelectSelfUser()
  });
}

function mapDispatchToProps(dispatch, props) {
  return {};
}

module.exports = connect(
  mapStateToProps,
  mapDispatchToProps,
)(ManagerUser);
