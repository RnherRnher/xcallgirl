const React = require('react');
const propTypes = require('prop-types');
const classNames = require('classnames');
const User = require('./User');
const { WindowScroll } = require('../../../../core/client/components/Scrolls');
const { LoadableUser } = require('../Loadables');

const Users = class Users extends React.Component {
  constructor(props) {
    super(props);
  }

  initElement() {
    const {
      className,
      users,
      rowHeight,
      handleLoad
    } = this.props;

    return (
      <div className={classNames('xcg-users', className)}>
        <WindowScroll
          result={users.result}
          handleLoadMore={handleLoad}
          initialLoad={!users.data.length}
          value={users.data}
          Children={User}
          Loadable={LoadableUser}
          rowHeight={rowHeight}
        />
      </div>
    );
  }

  render() {
    return this.initElement();
  }
};

Users.propTypes = {
  className: propTypes.string,
  rowHeight: propTypes.number,
  handleLoad: propTypes.func,
  users: propTypes.object.isRequired,
};

Users.defaultProps = {
  className: '',
  rowHeight: 76.5,
  handleLoad: null
};

module.exports = Users;
