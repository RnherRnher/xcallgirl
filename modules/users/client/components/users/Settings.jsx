const UserSettingInfoAccount = require('./settings/UserSettingInfoAccount');
const UserSettingLanguage = require('./settings/UserSettingLanguage');
const UserSettingNotifications = require('./settings/UserSettingNotifications');
const UserSettingPassword = require('./settings/UserSettingPassword');
const UserSettingVerify = require('./settings/UserSettingVerify');
const UserSettingPrivacy = require('./settings/UserSettingPrivacy');

module.exports = {
  UserSettingInfoAccount,
  UserSettingLanguage,
  UserSettingNotifications,
  UserSettingPassword,
  UserSettingVerify,
  UserSettingPrivacy
};
