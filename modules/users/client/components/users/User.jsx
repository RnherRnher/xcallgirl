const React = require('react');
const propTypes = require('prop-types');
const classNames = require('classnames');
const joi = require('joi');
const constant = require('../../../../../commons/utils/constant');
const url = require('../../../../core/client/utils/url');
const { FormattedMessage } = require('react-intl');
const { createStructuredSelector } = require('reselect');
const { push } = require('connected-react-router/immutable');
const { connect } = require('react-redux');
const {
  VerifiedUser,
  Store,
  Whatshot,
  Chat,
  Stars
} = require('@material-ui/icons');
const {
  ListItem,
  Avatar,
  ListItemAvatar,
  ListItemText,
  ListItemSecondaryAction,
  IconButton
} = require('@material-ui/core');
const { createChatRequest } = require('../../../../chats/client/actions/chat.client.action');
const { makeSelectCreateActionChat } = require('../../../../chats/client/reselects/chat.client.reselect');
const { ResultEnum } = require('../../../../core/client/constants/core.client.constant');
const { create } = require('../../../../../commons/modules/chats/validates/chat.validate');

const User = class User extends React.Component {
  constructor(props) {
    super(props);

    this.handleCheck = this.handleCheck.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }

  handleCheck(body, validate) {
    return joi.validate(body, validate);
  }

  handleClick(type) {
    return (event) => {
      const {
        createChatRequest,
        redirect,
        value,
        validate
      } = this.props;

      switch (type) {
        case constant.USER:
          redirect(`${url.USER_GET}?id=${value._id}`);
          break;
        case constant.CHAT:
          this.handleCheck(
            { usersID: [value._id] },
            validate.createChat
          ).then((create) => {
            createChatRequest(constant.PRIVCY, create);
          });
          break;
        default:
          break;
      }
    };
  }

  IconProfileUser() {
    const { value } = this.props;

    let IconProfileUser = null;

    if (value.roles === constant.PROVIDER) {
      IconProfileUser = <Store className="xcg-profile-user-icon xcg-success" />;
    } else if (value.roles === constant.ADMIN) {
      IconProfileUser = <Whatshot className="xcg-profile-user-icon xcg-error" />;
    } else if (value.roles === constant.SYSTEM) {
      IconProfileUser = <Stars className="xcg-profile-user-icon xcg-warning" />;
    } else if (value.verifyAccount) {
      IconProfileUser = <VerifiedUser className="xcg-profile-user-icon xcg-info" />;
    }

    return IconProfileUser;
  }

  initElement() {
    const {
      className,
      value,
      createChat
    } = this.props;

    const createChatDisabled = createChat.result === ResultEnum.wait;

    return (
      <div className={classNames('xcg-user', className)}>
        <ListItem
          button
          onClick={this.handleClick(constant.USER)}
        >
          <ListItemAvatar>
            <Avatar
              alt={value.nick}
              src={value.avatar}
            />
          </ListItemAvatar>
          <ListItemText
            inset
            primary={
              <div className="xcg-profile-user-group">
                <div className="xcg-profile-user-title">
                  {value.nick}
                </div>
                <div className="xcg-profile-user-list-icon">
                  {this.IconProfileUser()}
                </div>
              </div>
            }
            secondary={<FormattedMessage id={`user.roles.${value.roles}`} />}
          />
          <ListItemSecondaryAction>
            <IconButton
              disabled={createChatDisabled}
              onClick={this.handleClick(constant.CHAT)}
            >
              <Chat />
            </IconButton>
          </ListItemSecondaryAction>
        </ListItem>
      </div>
    );
  }

  render() {
    return this.initElement();
  }
};

User.propTypes = {
  className: propTypes.string,
  validate: propTypes.object,
  value: propTypes.object.isRequired,
  createChat: propTypes.object.isRequired,
  createChatRequest: propTypes.func.isRequired,
  redirect: propTypes.func.isRequired
};

User.defaultProps = {
  className: '',
  validate: {
    createChat: create.body
  }
};

function mapStateToProps(state, props) {
  return createStructuredSelector({
    createChat: makeSelectCreateActionChat()
  });
}

function mapDispatchToProps(dispatch, props) {
  return {
    createChatRequest: (fragment, create) => {
      return dispatch(createChatRequest(fragment, create));
    },
    redirect: (location) => {
      return dispatch(push(location));
    }
  };
}

module.exports = connect(
  mapStateToProps,
  mapDispatchToProps,
)(User);
