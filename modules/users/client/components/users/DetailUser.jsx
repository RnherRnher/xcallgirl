const React = require('react');
const classNames = require('classnames');
const propTypes = require('prop-types');
const joi = require('joi');
const constant = require('../../../../../commons/utils/constant');
const url = require('../../../../core/client/utils/url');
const { connect } = require('react-redux');
const { createStructuredSelector } = require('reselect');
const { FormattedMessage } = require('react-intl');
const { push } = require('connected-react-router/immutable');
const {
  VerifiedUser,
  Error,
  Edit,
  Info,
  Mail,
  HowToReg,
  Chat,
  Settings,
  Lock,
  Delete,
  School,
  Store,
  Whatshot,
  Stars,
  CreditCard
} = require('@material-ui/icons');
const {
  List,
  ListItemText,
  ListItem,
  IconButton,
  Tooltip,
  Card,
  CardContent,
  Typography,
  CardActionArea,
  AppBar,
  Tabs,
  Tab,
  ListItemAvatar,
  Avatar,
  CardActions,
  Badge,
} = require('@material-ui/core');
const { makeSelectIsOwner, makeSelectSelfUserInfo } = require('../../reselects/user.client.reselect');
const { AlertSnackbar } = require('../../../../core/client/components/Alerts');
const { createChatRequest } = require('../../../../chats/client/actions/chat.client.action');
const { transferCreditsRequest } = require('../../../../credits/client/actions/credit.client.action');
const { makeSelectCreateActionChat } = require('../../../../chats/client/reselects/chat.client.reselect');
const { makeSelectActions } = require('../../../../credits/client/reselects/credit.client.reselect');
const { ResultEnum } = require('../../../../core/client/constants/core.client.constant');
const { create } = require('../../../../../commons/modules/chats/validates/chat.validate');
const { DialogTransfer } = require('../../../../credits/client/components/Dialogs');
const {
  UIGeneralInfoAccount,
  UIHandleAuthAccount,
  UIHandleRequestVerifyAccount
} = require('../UIs');

const DetailUser = class DetailUser extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      index: 0,
      _index: 0,
      tooltiped: null,
      expanded: null,
    };

    this.handleCheck = this.handleCheck.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.handleClickTooltipOpen = this.handleClickTooltipOpen.bind(this);
    this.handleClickTooltipClose = this.handleClickTooltipClose.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleClickExpand = this.handleClickExpand.bind(this);
  }

  getCount() {
    const { value } = this.props;

    const count = {
      request: 0
    };

    if (value.verify.account.status === constant.WAIT) {
      count.request++;
    }

    return count;
  }

  getTabContent() {
    const { value, self } = this.props;
    const { index, _index } = this.state;

    const GeneralInfo = () => {
      return (
        <CardContent>
          <UIGeneralInfoAccount value={value} />
        </CardContent>
      );
    };

    const RequestMenu = () => {
      return (
        <List>
          <ListItem button onClick={this.handleClick(constant.VERIFY_ACCOUNT)}>
            <ListItemAvatar>
              <Avatar>
                <HowToReg />
              </Avatar>
            </ListItemAvatar>
            <ListItemText inset primary={<FormattedMessage id="user.verify.account.is" />} />
            {
              value.verify.account.status === constant.WAIT
                ? <Badge
                  color="secondary"
                  badgeContent={1}
                >.</Badge>
                : null
            }
          </ListItem>
        </List>
      );
    };

    const HandleRequestVerifyAccount = () => {
      return <UIHandleRequestVerifyAccount userID={value._id} value={value.verify.account} />;
    };

    const AuthMenu = () => {
      return (
        <List>
          {
            self.roles.value === constant.SYSTEM
              ? <ListItem button onClick={this.handleClick(constant.ROLES)}>
                <ListItemAvatar>
                  <Avatar>
                    <School />
                  </Avatar>
                </ListItemAvatar>
                <ListItemText inset primary={<FormattedMessage id="user.roles.is" />} />
              </ListItem>
              : null
          }
          <ListItem button onClick={this.handleClick(constant.LOCK)}>
            <ListItemAvatar>
              <Avatar>
                <Lock />
              </Avatar>
            </ListItemAvatar>
            <ListItemText inset primary={<FormattedMessage id="user.lock.is" />} />
          </ListItem>
          <ListItem button onClick={this.handleClick(constant.DELETE)}>
            <ListItemAvatar>
              <Avatar>
                <Delete />
              </Avatar>
            </ListItemAvatar>
            <ListItemText inset primary={<FormattedMessage id="user.delete.is" />} />
          </ListItem>
        </List>
      );
    };

    const HandleAuthAccount = (type) => {
      return <UIHandleAuthAccount
        type={type}
        userID={value._id}
        value={{ [type]: value[type] }}
      />;
    };

    let TabContent = null;
    switch (index) {
      case 0:
        TabContent = GeneralInfo();
        break;
      case 1:
        switch (_index) {
          case 0:
            TabContent = RequestMenu();
            break;
          case 1:
            TabContent = HandleRequestVerifyAccount();
            break;
          default:
            break;
        }
        break;
      case 2:
        switch (_index) {
          case 0:
            TabContent = AuthMenu();
            break;
          case 1:
            TabContent = HandleAuthAccount(constant.ROLES);
            break;
          case 2:
            TabContent = HandleAuthAccount(constant.LOCK);
            break;
          case 3:
            TabContent = HandleAuthAccount(constant.DELETE);
            break;
          default:
            break;
        }
        break;
      default:
        break;
    }

    return TabContent;
  }

  handleCheck(body, validate) {
    return joi.validate(body, validate);
  }

  handleClick(type) {
    return (event) => {
      const {
        value,
        validate,
        redirect,
        createChatRequest,
        transferCreditsRequest
      } = this.props;

      switch (type) {
        case constant.EDIT:
          redirect(url.SETTINGS_ACCOUNT);
          break;
        case constant.VERIFY_ACCOUNT:
          this.setState({ _index: 1 });
          break;
        case constant.ROLES:
          this.setState({ _index: 1 });
          break;
        case constant.LOCK:
          this.setState({ _index: 2 });
          break;
        case constant.DELETE:
          this.setState({ _index: 3 });
          break;
        case constant.CHAT:
          this.handleCheck(
            { usersID: [value._id] },
            validate.createChat
          ).then((create) => {
            createChatRequest(constant.PRIVCY, create);
          });
          break;
        case constant.TRANSFER:
          transferCreditsRequest(value._id, event);
          break;
        default:
          break;
      }
    };
  }

  handleClickTooltipOpen(tooltiped) {
    return (event) => {
      this.setState({ tooltiped });
    };
  }

  handleClickTooltipClose(event) {
    this.setState({ tooltiped: false });
  }

  handleChange(type) {
    return (event, index) => {
      switch (type) {
        case constant.VIEW:
          this.setState({
            index,
            _index: 0
          });
          break;
        default:
          break;
      }
    };
  }

  handleClickExpand(type) {
    return (event) => {
      this.setState((state, props) => {
        return {
          expanded: state.expanded === type ? null : type,
        };
      });
    };
  }

  Tab() {
    const { countLimit } = this.props;
    const { index } = this.state;

    const count = this.getCount();

    return (
      <AppBar position="static">
        <Tabs
          value={index}
          onChange={this.handleChange(constant.VIEW)}
        >
          <Tab
            label={<FormattedMessage id="info.general.is" />}
            icon={<Info />}
          />
          <Tab
            label={<FormattedMessage id="info.request.is" />}
            icon={
              count.request
                ? <Badge
                  color="secondary"
                  badgeContent={
                    count.request > countLimit
                      ? `${countLimit}+`
                      : count.request
                  }
                >
                  <Mail />
                </Badge>
                : <Mail />
            }
          />
          <Tab
            label={<FormattedMessage id="info.manager.is" />}
            icon={<Settings />}
          />
        </Tabs>
      </AppBar>
    );
  }

  Actions() {
    const {
      transferCredit,
      createChat,
      isOwner
    } = this.props;
    const { expanded } = this.state;

    const createChatDisabled = createChat.result === ResultEnum.wait;
    const transferCreditDisabled = transferCredit.result === ResultEnum.wait;
    const transferCreditOpen = transferCredit && transferCredit.result === ResultEnum.success ? false : expanded === constant.TRANSFER;

    return (
      <CardActions className="xcg-card-action" disableActionSpacing>
        <IconButton
          className="xcg-card-action-button"
          disabled={createChatDisabled}
          onClick={this.handleClick(constant.CHAT)}
        >
          <Chat />
        </IconButton>
        <IconButton
          className="xcg-card-action-button"
          disabled={transferCreditDisabled}
          onClick={this.handleClickExpand(constant.TRANSFER)}
        >
          <CreditCard />
        </IconButton>
        {
          isOwner
            ? <IconButton
              className="xcg-card-action-button"
              color="secondary"
              onClick={this.handleClick(constant.EDIT)}
            >
              <Edit />
            </IconButton>
            : null
        }
        <DialogTransfer
          open={transferCreditOpen}
          disabled={transferCreditDisabled}
          handleClickParent={this.handleClick(constant.TRANSFER)}
          handleClickCloseParent={this.handleClickExpand(constant.TRANSFER)}
        />
      </CardActions>
    );
  }

  IconProfileUser() {
    const { value } = this.props;

    let IconProfileUser = null;

    if (value.roles.value === constant.PROVIDER) {
      IconProfileUser = <Store className="xcg-profile-user-icon xcg-success" />;
    } else if (value.roles.value === constant.ADMIN) {
      IconProfileUser = <Whatshot className="xcg-profile-user-icon xcg-error" />;
    } else if (value.roles.value === constant.SYSTEM) {
      IconProfileUser = <Stars className="xcg-profile-user-icon xcg-warning" />;
    }

    return IconProfileUser;
  }

  Header() {
    const { value } = this.props;
    const { tooltiped } = this.state;

    return (
      <div className="xcg-card-header">
        <CardActionArea className="xcg-avatar-group">
          <Avatar
            className="xcg-avatar"
            src={value.avatar.url}
            alt={value.name.nick}
          />
        </CardActionArea>
        <ListItem className="xcg-list-item">
          <ListItemText
            inset
            primary={
              <div className="xcg-profile-user-group">
                <div className="xcg-profile-user-title">
                  <Typography variant="title">
                    {value.name.nick}
                  </Typography>
                </div>
                <div className="xcg-profile-user-list-icon">
                  <Tooltip
                    placement="top"
                    onClose={this.handleClickTooltipClose}
                    open={tooltiped === constant.VERIFY_ACCOUNT}
                    title={
                      <FormattedMessage id={value.verify.account.is
                        ? 'user.verify.account.exist'
                        : 'user.verify.account.notExist'}
                      />
                    }
                  >
                    <IconButton onClick={this.handleClickTooltipOpen(constant.VERIFY_ACCOUNT)}>
                      {
                        value.verify.account.is
                          ? <VerifiedUser className="xcg-info" />
                          : <Error className="xcg-error" />
                      }
                    </IconButton>
                  </Tooltip>
                </div>
              </div>
            }
            secondary={
              <span className="xcg-roles-group">
                <FormattedMessage id={`user.roles.${value.roles.value}`} />
                {this.IconProfileUser()}
              </span>
            }
          />
        </ListItem>
      </div>
    );
  }

  initElement() {
    const { className, value } = this.props;

    return (
      <div className={classNames('xcg-detail-user', className)}>
        {
          value.delete.is
            ? <AlertSnackbar
              className="xcg-top-alert-snackbar"
              variant={constant.ERROR}
              message={<FormattedMessage id="user.delete.exist" />}
            />
            : value.lock.is
              ? <AlertSnackbar
                className="xcg-top-alert-snackbar"
                variant={constant.ERROR}
                message={<FormattedMessage id="user.lock.exist" />}
              />
              : null
        }
        <Card className="xcg-card">
          {this.Header()}
          {this.Actions()}
          {value.authorizedLevel === constant.ENUM.AUTH.FULL ? this.Tab() : null}
          {this.getTabContent()}
        </Card>
      </div>
    );
  }

  render() {
    return this.initElement();
  }
};

DetailUser.propTypes = {
  className: propTypes.string,
  countLimit: propTypes.number,
  validate: propTypes.object,
  self: propTypes.object.isRequired,
  isOwner: propTypes.bool.isRequired,
  createChat: propTypes.object.isRequired,
  transferCredit: propTypes.object.isRequired,
  value: propTypes.object.isRequired,
  createChatRequest: propTypes.func.isRequired,
  transferCreditsRequest: propTypes.func.isRequired,
  redirect: propTypes.func.isRequired,
};

DetailUser.defaultProps = {
  className: '',
  countLimit: constant.ENUM.COUNT.LIMIT,
  validate: {
    createChat: create.body
  }
};

function mapStateToProps(state, props) {
  const userID = props.value._id;

  return createStructuredSelector({
    self: makeSelectSelfUserInfo(),
    isOwner: makeSelectIsOwner(userID),
    createChat: makeSelectCreateActionChat(),
    transferCredit: makeSelectActions(constant.TRANSFER)
  });
}

function mapDispatchToProps(dispatch, props) {
  return {
    createChatRequest: (fragment, create) => {
      return dispatch(createChatRequest(fragment, create));
    },
    transferCreditsRequest: (userID, transfer) => {
      return dispatch(transferCreditsRequest(userID, transfer));
    },
    redirect: (location) => {
      return dispatch(push(location));
    },
  };
}

module.exports = connect(
  mapStateToProps,
  mapDispatchToProps,
)(DetailUser);
