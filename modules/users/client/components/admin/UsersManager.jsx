const React = require('react');
const propTypes = require('prop-types');
const classNames = require('classnames');
const joi = require('joi');
const constant = require('../../../../../commons/utils/constant');
const { createStructuredSelector } = require('reselect');
const { connect } = require('react-redux');
const { FormattedMessage } = require('react-intl');
const {
  HowToReg,
  Lock,
  Delete
} = require('@material-ui/icons');
const {
  AppBar,
  Tabs,
  Tab
} = require('@material-ui/core');
const { getUsersRequest, resetAdminStore } = require('../../actions/admin.client.action');
const { makeSelectListUser } = require('../../reselects/admin.client.reselect');
const { extendSearchBody } = require('../../../../../commons/modules/users/validates/admin.validate');
const { Users } = require('../Users');

const UsersManager = class UsersManager extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      index: 0,
      extendSearch: this.getExtendSearch(0)
    };

    this.handleCheck = this.handleCheck.bind(this);
    this.handleLoad = this.handleLoad.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  componentWillUnmount() {
    const { resetAdminStore } = this.props;

    resetAdminStore('list');
  }

  getExtendSearch(index) {
    switch (index) {
      case 0:
        return {
          verify: {
            account: {
              status: constant.WAIT
            }
          }
        };
      case 1:
        return {
          lock: true
        };
      case 2:
        return {
          delete: true
        };
      default:
        return {};
    }
  }

  handleCheck(body, validate) {
    return joi.validate(body, validate);
  }

  handleLoad(reload, currentExtendSearch) {
    const {
      validate,
      getUsersRequest,
      users
    } = this.props;
    const { extendSearch } = this.state;

    this.handleCheck(currentExtendSearch || extendSearch, validate)
      .then((extendSearch) => {
        getUsersRequest(
          constant.EXTEND,
          extendSearch,
          users.skip,
          reload
        );
      });
  }

  handleChange(event, index) {
    this.setState((state, props) => {
      const isChange = state.index !== index;
      if (isChange) {
        this.handleLoad(true, this.getExtendSearch(index));
      }

      return {
        index,
        extendSearch: this.getExtendSearch(index),
      };
    });
  }

  initElement() {
    const { className, users } = this.props;
    const { index } = this.state;

    return (
      <div className={classNames('xcg-users-manager', className)}>
        <AppBar className="xcg-app-bar" position="fixed">
          <Tabs
            value={index}
            onChange={this.handleChange}
          >
            <Tab
              label={<FormattedMessage id="user.verify.account.request.is" />}
              icon={<HowToReg />}
            />
            <Tab
              label={<FormattedMessage id="user.lock.exist" />}
              icon={<Lock />}
            />
            <Tab
              label={<FormattedMessage id="user.delete.exist" />}
              icon={<Delete />}
            />
          </Tabs>
        </AppBar>
        <Users handleLoad={this.handleLoad} users={users} />
      </div>
    );
  }

  render() {
    return this.initElement();
  }
};

UsersManager.propTypes = {
  className: propTypes.string,
  validate: propTypes.object,
  users: propTypes.object.isRequired,
  getUsersRequest: propTypes.func.isRequired,
  resetAdminStore: propTypes.func.isRequired
};

UsersManager.defaultProps = {
  className: '',
  validate: extendSearchBody
};

function mapStateToProps(state, props) {
  return createStructuredSelector({
    users: makeSelectListUser()
  });
}

function mapDispatchToProps(dispatch, props) {
  return {
    getUsersRequest: (
      fragment,
      extendSearch,
      skip,
      reload
    ) => {
      return dispatch(getUsersRequest(
        fragment,
        extendSearch,
        skip,
        reload
      ));
    },
    resetAdminStore: (fragment) => {
      return dispatch(resetAdminStore(fragment));
    },
  };
}

module.exports = connect(
  mapStateToProps,
  mapDispatchToProps,
)(UsersManager);
