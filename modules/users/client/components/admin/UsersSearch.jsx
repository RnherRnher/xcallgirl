const React = require('react');
const classNames = require('classnames');
const propTypes = require('prop-types');
const joi = require('joi');
const constant = require('../../../../../commons/utils/constant');
const { assignIn, isEmpty } = require('lodash');
const { createStructuredSelector } = require('reselect');
const { FormattedMessage } = require('react-intl');
const { connect } = require('react-redux');
const {
  Search,
  Tune,
} = require('@material-ui/icons');
const {
  AppBar,
  Button,
  DialogContent,
  DialogActions,
  Dialog,
  IconButton,
  Slide,
  FormGroup,
  FormControlLabel,
  Checkbox,
  TextField,
  Toolbar,
  FormControl,
  FormLabel
} = require('@material-ui/core');
const { SelectAddress } = require('../../../../core/client/components/Selects');
const { InputLimit } = require('../../../../core/client/components/Inputs');
const { SelectGender, SelectRoles } = require('../../../../users/client/components/Selects');
const { makeSelectListUser } = require('../../reselects/admin.client.reselect');
const { ResultEnum } = require('../../../../core/client/constants/core.client.constant');
const { getUsersRequest } = require('../../actions/admin.client.action');
const { extendSearchBody } = require('../../../../../commons/modules/users/validates/admin.validate');
const { Users } = require('../Users');

const UsersSearch = class UsersSearch extends React.Component {
  constructor(props) {
    super(props);

    const { users } = this.props;
    this.state = {
      expanded: null,
      extendSearch: users.extendSearch,
      tempExtendSearch: users.extendSearch
    };

    this.rolesRef = React.createRef();
    this.limitAgeRef = React.createRef();
    this.addressRef = React.createRef();
    this.genderRef = React.createRef();

    this.handleCheck = this.handleCheck.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.handleClickClose = this.handleClickClose.bind(this);
    this.handleLoad = this.handleLoad.bind(this);
    this.handleClickExpand = this.handleClickExpand.bind(this);
  }

  getExtendSearch() {
    const { extendSearch } = this.state;

    const currentExtendSearch = {};

    if (this.rolesRef.current && this.rolesRef.current.state.roles) {
      currentExtendSearch.roles = this.rolesRef.current.state.roles;
    }

    if (this.addressRef.current) {
      const {
        country,
        city,
        county,
      } = this.addressRef.current.state;

      const address = {};
      if (country) {
        address.country = country;
      }

      if (city) {
        address.city = city;
      }

      if (county) {
        address.county = county;
      }

      if (!isEmpty(address)) {
        currentExtendSearch.address = address;
      }
    }

    if (this.genderRef.current && this.genderRef.current.state.gender) {
      currentExtendSearch.gender = this.genderRef.current.state.gender;
    }

    if (this.limitAgeRef.current) {
      const { min, max } = this.limitAgeRef.current.state;

      const age = {};
      if (min) {
        age.min = min;
      }

      if (max) {
        age.max = max;
      }

      if (!isEmpty(age)) {
        currentExtendSearch.age = age;
      }
    }

    if (extendSearch.nick) {
      currentExtendSearch.nick = extendSearch.nick;
    }

    if (extendSearch.online) {
      currentExtendSearch.online = extendSearch.online;
    }

    if (extendSearch.lock) {
      currentExtendSearch.lock = extendSearch.lock;
    }

    if (extendSearch.delete) {
      currentExtendSearch.delete = extendSearch.delete;
    }

    return currentExtendSearch;
  }

  handleCheck(body, validate) {
    return joi.validate(body, validate);
  }

  handleChange(type) {
    return (event) => {
      const { value } = event.target;

      this.setState((state, props) => {
        switch (type) {
          case constant.NICK:
            return {
              extendSearch: { nick: value },
              tempExtendSearch: { nick: value }
            };
          default:
            return {
              extendSearch: assignIn(
                state.extendSearch,
                { [type]: !(state.extendSearch[type]) }
              )
            };
        }
      });
    };
  }

  handleClick(type) {
    return (event) => {
      switch (type) {
        case constant.SEARCH:
          this.setState({
            extendSearch: this.getExtendSearch(),
            tempExtendSearch: this.getExtendSearch(),
          });
          this.handleLoad(true, this.getExtendSearch());
          this.handleClickExpand(null)();

          if (event.preventDefault) {
            event.preventDefault();
          }
          break;
        default:
          break;
      }
    };
  }

  handleLoad(reload, currentExtendSearch) {
    const {
      validate,
      getUsersRequest,
      users
    } = this.props;
    const { extendSearch } = this.state;

    this.handleCheck(currentExtendSearch || extendSearch, validate)
      .then((extendSearch) => {
        getUsersRequest(
          constant.EXTEND,
          extendSearch,
          users.skip,
          reload
        );
      });
  }

  handleClickExpand(type) {
    return (event) => {
      this.setState((state, props) => {
        return {
          expanded: state.expanded === type ? null : type
        };
      });
    };
  }

  handleClickClose(type) {
    return (event) => {
      switch (type) {
        case constant.SEARCH:
          this.setState((state, props) => {
            return {
              expanded: null,
              extendSearch: state.tempExtendSearch
            };
          });
          break;
        case constant.DELETE:
          this.setState((state, props) => {
            return {
              extendSearch: { nick: state.extendSearch.nick },
              tempExtendSearch: { nick: state.tempExtendSearch.nick }
            };
          });
          break;
        default:
          break;
      }
    };
  }

  Transition(props) {
    return <Slide direction="up" {...props} />;
  }

  InputGroup() {
    const { users } = this.props;
    const { extendSearch } = this.state;

    const disabled = users.result === ResultEnum.wait;

    return (
      <div className="xcg-input-group">
        <div className="xcg-sub-input-group">
          <SelectRoles
            ref={this.rolesRef}
            disabled={disabled}
            value={extendSearch.roles}
            isUpdatePropsToState
          />
        </div>
        <div className="xcg-sub-input-group">
          <FormLabel>
            <FormattedMessage id="info.age.is" />
          </FormLabel>
          <InputLimit
            ref={this.limitAgeRef}
            disabled={disabled}
            value={extendSearch.age}
            isUpdatePropsToState
          />
        </div>
        <div className="xcg-sub-input-group">
          <SelectGender
            ref={this.genderRef}
            disabled={disabled}
            value={extendSearch.gender}
            isUpdatePropsToState
          />
        </div>
        <div className="xcg-sub-input-group">
          <FormLabel>
            <FormattedMessage id="info.address.is" />
          </FormLabel>
          <SelectAddress
            ref={this.addressRef}
            disabled={disabled}
            value={extendSearch.address}
            isUpdatePropsToState
          />
        </div>
        <div className="xcg-sub-input-group">
          <FormControl>
            <FormGroup>
              <FormControlLabel
                disabled={disabled}
                control={
                  <Checkbox
                    checked={extendSearch.online === true}
                    onChange={this.handleChange(constant.ONLINE)}
                    value={constant.ONLINE}
                  />
                }
                label={<FormattedMessage id="user.online.exist" />}
              />
            </FormGroup>
            <FormGroup>
              <FormControlLabel
                disabled={disabled}
                control={
                  <Checkbox
                    checked={extendSearch.lock === true}
                    onChange={this.handleChange(constant.LOCK)}
                    value={constant.LOCK}
                  />
                }
                label={<FormattedMessage id="user.lock.exist" />}
              />
            </FormGroup>
            <FormGroup>
              <FormControlLabel
                disabled={disabled}
                control={
                  <Checkbox
                    checked={extendSearch.delete === true}
                    onChange={this.handleChange(constant.DELETE)}
                    value={constant.DELETE}
                  />
                }
                label={<FormattedMessage id="user.delete.exist" />}
              />
            </FormGroup>
          </FormControl>
        </div>
      </div>
    );
  }

  SearchDialog() {
    const { users } = this.props;
    const { expanded } = this.state;

    const disabled = users.result === ResultEnum.wait;

    return (
      <Dialog
        className="xcg-search-dialog-xcg-users-search"
        open={expanded === constant.SEARCH}
        TransitionComponent={this.Transition}
      >
        <DialogContent>
          {this.InputGroup()}
        </DialogContent>
        <DialogActions>
          <Button
            onClick={this.handleClickClose(constant.DELETE)}
            disabled={disabled}
            color="primary"
          >
            <FormattedMessage id="actions.delete" />
          </Button>
          <Button
            onClick={this.handleClick(constant.SEARCH)}
            disabled={disabled}
            color="secondary"
          >
            <FormattedMessage id="actions.search" />
          </Button>
          <Button
            onClick={this.handleClickClose(constant.SEARCH)}
            disabled={disabled}
          >
            <FormattedMessage id="actions.back" />
          </Button>
        </DialogActions>
      </Dialog>
    );
  }

  initElement() {
    const { className, users } = this.props;
    const { extendSearch } = this.state;

    const disabled = users.result === ResultEnum.wait;

    return (
      <div className={classNames('xcg-users-search', className)}>
        <AppBar className="xcg-app-bar" position="fixed">
          <Toolbar>
            <form
              className="xcg-form-sreach"
              onSubmit={this.handleClick(constant.SEARCH)}
            >
              <TextField
                label={<FormattedMessage id="user.name.nick.is" />}
                type="text"
                fullWidth
                value={extendSearch.nick || ''}
                disabled={disabled}
                onChange={this.handleChange(constant.NICK)}
              />
              <IconButton
                disabled={disabled}
                type="submit"
                color="secondary"
              >
                <Search />
              </IconButton>
            </form>
            <IconButton
              disabled={disabled}
              onClick={this.handleClickExpand(constant.SEARCH)}
            >
              <Tune />
            </IconButton>
          </Toolbar>
        </AppBar>
        <Users handleLoad={this.handleLoad} users={users} />
        {this.SearchDialog()}
      </div>
    );
  }

  render() {
    return this.initElement();
  }
};

UsersSearch.propTypes = {
  className: propTypes.string,
  validate: propTypes.object,
  users: propTypes.object.isRequired,
  getUsersRequest: propTypes.func.isRequired,
};

UsersSearch.defaultProps = {
  className: '',
  validate: extendSearchBody
};

function mapStateToProps(state, props) {
  return createStructuredSelector({
    users: makeSelectListUser()
  });
}

function mapDispatchToProps(dispatch, props) {
  return {
    getUsersRequest: (
      fragment,
      extendSearch,
      skip,
      reload
    ) => {
      return dispatch(getUsersRequest(
        fragment,
        extendSearch,
        skip,
        reload
      ));
    },
  };
}

module.exports = connect(
  mapStateToProps,
  mapDispatchToProps,
)(UsersSearch);
