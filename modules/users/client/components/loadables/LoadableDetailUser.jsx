const React = require('react');
const propTypes = require('prop-types');
const classNames = require('classnames');
const ContentLoader = require('react-content-loader').default;

const LoadableDetailUser = class LoadableDetailUser extends React.Component {
  constructor(props) {
    super(props);
  }

  initElement() {
    const { className } = this.props;

    return (
      <div className={classNames('xcg-loadable-detail-user', className)}>
        <ContentLoader
          height={500}
          width={350}
          speed={2}
          primaryColor="#666666"
          secondaryColor="#737373"
        >
          <circle cx="170" cy="80" r="70" />
          <rect x="20" y="170" rx="5" ry="5" width="310" height="300" />
        </ContentLoader>
      </div>
    );
  }

  render() {
    return this.initElement();
  }
};

LoadableDetailUser.propTypes = {
  className: propTypes.string,
};

LoadableDetailUser.defaultProps = {
  className: '',
};

module.exports = LoadableDetailUser;
