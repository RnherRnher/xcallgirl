const { getProfileUserReloadRequest } = require('../actions/user.client.action');
const { SOCKET_SELF_USER_UPDATE } = require('../../../../commons/modules/users/datas/socket.data');

const init = (dispatch, socket) => {
  socket.on(SOCKET_SELF_USER_UPDATE, (body) => {
    dispatch(getProfileUserReloadRequest());
  });
};
module.exports = init;
