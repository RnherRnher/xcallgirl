const { action } = require('../../../core/client/utils/middleware');
const constant = require('../constants/system.client.constant');

/**
 * @override Yêu cầu thay đổi vai trò người dùng
 *
 * @param   {ObjectId}  userID
 * @param   {Object}    changeRolesAccount
 * @returns {Object}
 */
const changeRolesAccountRequest = (userID, changeRolesAccount) => {
  return action(constant.CHANGE_ROLES_ACCOUNT_REQUEST, { userID, changeRolesAccount });
};
module.exports.changeRolesAccountRequest = changeRolesAccountRequest;

/**
 * @override Thay đổi vai trò người dùng thành công
 *
 * @param   {Object}
 * @returns {Object}
 */
const changeRolesAccountSuccess = ({ code }) => {
  return action(constant.CHANGE_ROLES_ACCOUNT_SUCCESS, { code });
};
module.exports.changeRolesAccountSuccess = changeRolesAccountSuccess;

/**
 * @override Thay đổi vai trò người dùng thất bại
 *
 * @param   {Object}
 * @returns {Object}
 */
const changeRolesAccountFailed = ({ code }) => {
  return action(constant.CHANGE_ROLES_ACCOUNT_FAILED, { code });
};
module.exports.changeRolesAccountFailed = changeRolesAccountFailed;
