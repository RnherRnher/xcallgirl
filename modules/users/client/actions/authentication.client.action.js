const { action } = require('../../../core/client/utils/middleware');
const constant = require('../constants/authentication.client.constant');

/**
 * @override Đăng nhập
 *
 * @param   {Object} signin
 * @returns {Object}
 */
const signinRequest = (signin) => {
  return action(constant.SIGNIN_REQUEST, { signin });
};
module.exports.signinRequest = signinRequest;

/**
 * @override Đăng nhập thành công
 *
 * @param   {Object}
 * @returns {Object}
 */
const signinSuccess = ({ code }) => {
  return action(constant.SIGNIN_SUCCESS, { code });
};
module.exports.signinSuccess = signinSuccess;

/**
 * @override Đăng nhập thất bại
 *
 * @param   {Object}
 * @returns {Object}
 */
const signinFailed = ({ code }) => {
  return action(constant.SIGNIN_FAILED, { code });
};
module.exports.signinFailed = signinFailed;

/**
 * @override Đăng ký
 *
 * @param   {Object} signup
 * @returns {Object}
 */
const signupRequest = (signup) => {
  return action(constant.SIGNUP_REQUEST, { signup });
};
module.exports.signupRequest = signupRequest;

/**
 * @override Đăng ký thành công
 *
 * @param   {Object}
 * @returns {Object}
 */
const signupSuccess = ({ code }) => {
  return action(constant.SIGNUP_SUCCESS, { code });
};
module.exports.signupSuccess = signupSuccess;

/**
 * @override Đăng ký thất bại
 *
 * @param   {Object}
 * @returns {Object}
 */
const signupFailed = ({ code }) => {
  return action(constant.SIGNUP_FAILED, { code });
};
module.exports.signupFailed = signupFailed;

/**
 * @override Đăng xuất
 *
 * @returns {Object}
 */
const signoutRequest = () => {
  return action(constant.SIGNOUT_REQUEST);
};
module.exports.signoutRequest = signoutRequest;

/**
 * @override Đăng xuất thành công
 *
 * @param   {Object}
 * @returns {Object}
 */
const signoutSuccess = ({ code }) => {
  return action(constant.SIGNOUT_SUCCESS, { code });
};
module.exports.signoutSuccess = signoutSuccess;

/**
 * @override Đăng xuất thất bại
 *
 * @param   {Object}
 * @returns {Object}
 */
const signoutFailed = ({ code }) => {
  return action(constant.SIGNOUT_FAILED, { code });
};
module.exports.signoutFailed = signoutFailed;

/**
 * @override
 *
 * @param   {Object} resetPassword
 * @returns {Object}
 */
const resetPasswordRequest = (resetPassword) => {
  return action(constant.RESERT_PASSWORD_REQUEST, { resetPassword });
};
module.exports.resetPasswordRequest = resetPasswordRequest;

/**
 * @override
 *
 * @param   {Object} validateResetTokenPassword
 * @returns {Object}
 */
const validateResetTokenPasswordRequest = (validateResetTokenPassword) => {
  return action(constant.VALIDATE_RESET_TOKEN_PASSWORD_REQUEST, { validateResetTokenPassword });
};
module.exports.validateResetTokenPasswordRequest = validateResetTokenPasswordRequest;

/**
 * @override
 *
 * @param   {Object} forgotPassword
 * @returns {Object}
 */
const forgotPasswordRequest = (forgotPassword) => {
  return action(constant.FORGOT_PASSWORD_REQUEST, { forgotPassword });
};
module.exports.forgotPasswordRequest = forgotPasswordRequest;

/**
 * @override thành công
 *
 * @param   {Object}
 * @returns {Object}
 */
const forgotPasswordSuccess = ({ code }) => {
  return action(constant.FORGOT_PASSWORD_SUCCESS, { code });
};
module.exports.forgotPasswordSuccess = forgotPasswordSuccess;

/**
 * @override thất bại
 *
 * @param   {Object}
 * @returns {Object}
 */
const forgotPasswordFailed = ({ code }) => {
  return action(constant.FORGOT_PASSWORD_FAILED, { code });
};
module.exports.forgotPasswordFailed = forgotPasswordFailed;

/**
 * @override Thiết lặp lại cửa hàng
 *
 * @param   {Object}
 */
const resetAuthStore = () => {
  return action(constant.RESERT_AUTH_STORE);
};
module.exports.resetAuthStore = resetAuthStore;
