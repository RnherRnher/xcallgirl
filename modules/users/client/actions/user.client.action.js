const { action } = require('../../../core/client/utils/middleware');
const constant = require('../constants/user.client.constant');

/**
 * @override Cập nhật người dùng
 *
 * @param   {String} fragment
 * @param   {Object} data
 * @returns {Object}
 */
const updateProfile = (fragment, data) => {
  return action(constant.UPDATE_PROFILE, { fragment, data });
};
module.exports.updateProfile = updateProfile;

/**
 * @override Yêu cầu lấy cài đặt người dùng
 *
 * @param   {String} fragment
 * @param   {Boolean} isAlert
 * @returns {Object}
 */
const getProfileSettingsRequest = (fragment, isAlert = true) => {
  return action(constant.GET_PROFILE_SETTINGS_REQUEST, { fragment, isAlert });
};
module.exports.getProfileSettingsRequest = getProfileSettingsRequest;

/**
 * @override Lấy cài đặt người dùng thành công
 *
 * @param   {Object}
 * @returns {Object}
 */
const getProfileSettingsSuccess = ({ code, settings }) => {
  return action(constant.GET_PROFILE_SETTINGS_SUCCESS, { code, settings });
};
module.exports.getProfileSettingsSuccess = getProfileSettingsSuccess;

/**
 * @override Lấy cài đặt người dùng thất bại
 *
 * @param   {Object}
 * @returns {Object}
 */
const getProfileSettingsFailed = ({ code }) => {
  return action(constant.GET_PROFILE_SETTINGS_FAILED, { code });
};
module.exports.getProfileSettingsFailed = getProfileSettingsFailed;

/**
 * @override Yêu cầu thay đổi cài đặt người dùng
 *
 * @param   {String} fragment
 * @param   {Object} settings
 * @returns {Object}
 */
const changeProfileSettingsRequest = (fragment, settings) => {
  return action(constant.CHANGE_PROFILE_SETTINGS_REQUEST, { fragment, settings });
};
module.exports.changeProfileSettingsRequest = changeProfileSettingsRequest;

/**
 * @override Thay đổi cài đặt người dùng thành công
 *
 * @param   {Object}
 * @returns {Object}
 */
const changeProfileSettingsSuccess = ({ code, settings }) => {
  return action(constant.CHANGE_PROFILE_SETTINGS_SUCCESS, { code, settings });
};
module.exports.changeProfileSettingsSuccess = changeProfileSettingsSuccess;

/**
 * @override Thay đổi cài đặt người dùng thất bại
 *
 * @param   {Object}
 * @returns {Object}
 */
const changeProfileSettingsFailed = ({ code }) => {
  return action(constant.CHANGE_PROFILE_SETTINGS_FAILED, { code });
};
module.exports.changeProfileSettingsFailed = changeProfileSettingsFailed;

/**
 * @override Yêu cầu lấy thông tin người dùng
 *
 * @param   {Boolean} isChangeLanguage
 * @param   {Boolean} isAlert
 * @returns {Object}
 */
const getProfileUserRequest = (isChangeLanguage = false, isAlert = true) => {
  return action(constant.GET_PROFILE_USER_REQUEST, { isChangeLanguage, isAlert });
};
module.exports.getProfileUserRequest = getProfileUserRequest;

/**
 * @override Lấy thông tin người dùng thành công
 *
 * @param   {Object}
 * @returns {Object}
 */
const getProfileUserSuccess = ({ code, user }) => {
  return action(constant.GET_PROFILE_USER_SUCCESS, { code, user });
};
module.exports.getProfileUserSuccess = getProfileUserSuccess;

/**
 * @override Lấy thông tin người dùng thất bại
 *
 * @param   {Object}
 * @returns {Object}
 */
const getProfileUserFailed = ({ code }) => {
  return action(constant.GET_PROFILE_USER_FAILED, { code });
};
module.exports.getProfileUserFailed = getProfileUserFailed;

/**
 * @override Yêu cầu lấy thông tin người dùng lập lại
 *
 * @param   {Boolean} isChangeLanguage
 * @returns {Object}
 */
const getProfileUserReloadRequest = (isChangeLanguage = false) => {
  return action(constant.GET_PROFILE_USER_RELOAD_REQUEST, { isChangeLanguage });
};
module.exports.getProfileUserReloadRequest = getProfileUserReloadRequest;

/**
 * @override Yêu cầu thay đổi mật khẩu người dùng
 *
 * @param   {Object} passwordDetails
 * @returns {Object}
 */
const changeProfilePasswordRequest = (passwordDetails) => {
  return action(constant.CHANGE_PROFILE_PASSWORD_REQUEST, { passwordDetails });
};
module.exports.changeProfilePasswordRequest = changeProfilePasswordRequest;

/**
 * @override Thay đổi mật khẩu người dùng thành công
 *
 * @param   {Object}
 * @returns {Object}
 */
const changeProfilePasswordSuccess = ({ code }) => {
  return action(constant.CHANGE_PROFILE_PASSWORD_SUCCESS, { code });
};
module.exports.changeProfilePasswordSuccess = changeProfilePasswordSuccess;

/**
 * @override Thay đổi mật khẩu người dùng thất bại
 *
 * @param   {Object}
 * @returns {Object}
 */
const changeProfilePasswordFailed = ({ code }) => {
  return action(constant.CHANGE_PROFILE_PASSWORD_FAILED, { code });
};
module.exports.changeProfilePasswordFailed = changeProfilePasswordFailed;

/**
 * @override Yêu cầu thay đổi số điện thoại người dùng
 *
 * @param   {Object} numberPhone
 * @returns {Object}
 */
const changeProfileNumberPhoneRequest = (numberPhone) => {
  return action(constant.CHANGE_PROFILE_NUMBERPHONE_REQUEST, { numberPhone });
};
module.exports.changeProfileNumberPhoneRequest = changeProfileNumberPhoneRequest;

/**
 * @override Thay đổi số điện thoại người dùng thành công
 *
 * @param   {Object}
 * @returns {Object}
 */
const changeProfileNumberPhoneSuccess = ({ code, numberPhone, verify }) => {
  return action(constant.CHANGE_PROFILE_NUMBERPHONE_SUCCESS, { code, numberPhone, verify });
};
module.exports.changeProfileNumberPhoneSuccess = changeProfileNumberPhoneSuccess;

/**
 * @override Thay đổi số điện thoại người dùng thất bại
 *
 * @param   {Object}
 * @returns {Object}
 */
const changeProfileNumberPhoneFailed = ({ code }) => {
  return action(constant.CHANGE_PROFILE_NUMBERPHONE_FAILED, { code });
};
module.exports.changeProfileNumberPhoneFailed = changeProfileNumberPhoneFailed;

/**
 * @override Yêu cầu thay đổi địa chỉ người dùng
 *
 * @param   {Object} address
 * @returns {Object}
 */
const changeProfileAddressRequest = (address) => {
  return action(constant.CHANGE_PROFILE_ADDRESS_REQUEST, { address });
};
module.exports.changeProfileAddressRequest = changeProfileAddressRequest;

/**
 * @override Thay đổi địa chỉ người dùng thành công
 *
 * @param   {Object}
 * @returns {Object}
 */
const changeProfileAddressSuccess = ({ code, address }) => {
  return action(constant.CHANGE_PROFILE_ADDRESS_SUCCESS, { code, address });
};
module.exports.changeProfileAddressSuccess = changeProfileAddressSuccess;

/**
 * @override Thay đổi địa chỉ người dùng thất bại
 *
 * @param   {Object}
 * @returns {Object}
 */
const changeProfileAddressFailed = ({ code }) => {
  return action(constant.CHANGE_PROFILE_ADDRESS_FAILED, { code });
};
module.exports.changeProfileAddressFailed = changeProfileAddressFailed;

/**
 * @override Yêu cầu thay đổi ảnh đại diện
 *
 * @param   {Object} image
 * @returns {Object}
 */
const changeProfileAvatarRequest = (image) => {
  return action(constant.CHANGE_PROFILE_AVATAR_REQUEST, { image });
};
module.exports.changeProfileAvatarRequest = changeProfileAvatarRequest;

/**
 * @override  Yêu cầu thay đổi ảnh đại diện thành công
 *
 * @param   {Object}
 * @returns {Object}
 */
const changeProfileAvatarSuccess = ({ code, urlAvatar }) => {
  return action(constant.CHANGE_PROFILE_AVATAR_SUCCESS, { code, urlAvatar });
};
module.exports.changeProfileAvatarSuccess = changeProfileAvatarSuccess;

/**
 * @override Yêu cầu thay đổi ảnh đại diện thất bại
 *
 * @param   {Object}
 * @returns {Object}
 */
const changeProfileAvatarFailed = ({ code }) => {
  return action(constant.CHANGE_PROFILE_AVATAR_FAILED, { code });
};
module.exports.changeProfileAvatarFailed = changeProfileAvatarFailed;

/**
 * @override Yêu cầu xóa ảnh đại diện
 *
 * @returns {Object}
 */
const deleteProfileAvatarRequest = () => {
  return action(constant.DELETE_PROFILE_AVATAR_REQUEST);
};
module.exports.deleteProfileAvatarRequest = deleteProfileAvatarRequest;

/**
 * @override  Yêu cầu xóa ảnh đại diện thành công
 *
 * @param   {Object}
 * @returns {Object}
 */
const deleteProfileAvatarSuccess = ({ code, urlAvatar }) => {
  return action(constant.DELETE_PROFILE_AVATAR_SUCCESS, { code, urlAvatar });
};
module.exports.deleteProfileAvatarSuccess = deleteProfileAvatarSuccess;

/**
 * @override Yêu cầu xóa ảnh đại diện thất bại
 *
 * @param   {Object}
 * @returns {Object}
 */
const deleteProfileAvatarFailed = ({ code }) => {
  return action(constant.DELETE_PROFILE_AVATAR_FAILED, { code });
};
module.exports.deleteProfileAvatarFailed = deleteProfileAvatarFailed;

/**
 * @override Yêu cầu xác thức người dùng
 *
 * @param   {Blob} image
 * @returns {Object}
 */
const requestVerifyAccountRequest = (image) => {
  return action(constant.REQUEST_VERIFY_ACCOUNT_REQUEST, { image });
};
module.exports.requestVerifyAccountRequest = requestVerifyAccountRequest;

/**
 * @override Yêu cầu xác thức người dùng thành công
 *
 * @param   {Object}
 * @returns {Object}
 */
const requestVerifyAccountSuccess = ({ code, verifyAccount }) => {
  return action(constant.REQUEST_VERIFY_ACCOUNT_SUCCESS, { code, verifyAccount });
};
module.exports.requestVerifyAccountSuccess = requestVerifyAccountSuccess;

/**
 * @override Yêu cầu xác thức người dùng thất bại
 *
 * @param   {Object}
 * @returns {Object}
 */
const requestVerifyAccountFailed = ({ code }) => {
  return action(constant.REQUEST_VERIFY_ACCOUNT_FAILED, { code });
};
module.exports.requestVerifyAccountFailed = requestVerifyAccountFailed;

/**
 * @override Yêu cầu mã xác thực số điện thoại người dùng
 *
 * @returns {Object}
 */
const getVerifyTokenNumberPhoneRequest = () => {
  return action(constant.GET_VERIFY_TOKEN_NUMBERPHONE_REQUEST);
};
module.exports.getVerifyTokenNumberPhoneRequest = getVerifyTokenNumberPhoneRequest;

/**
 * @override Yêu cầu mã xác thực số điện thoại người dùng thành công
 *
 * @param   {Object}
 * @returns {Object}
 */
const getVerifyTokenNumberPhoneSuccess = ({ code }) => {
  return action(constant.GET_VERIFY_TOKEN_NUMBERPHONE_SUCCESS, { code });
};
module.exports.getVerifyTokenNumberPhoneSuccess = getVerifyTokenNumberPhoneSuccess;

/**
 * @override Yêu cầu mã xác thực số điện thoại người dùng thất bại
 *
 * @param   {Object}
 * @returns {Object}
 */
const getVerifyTokenNumberPhoneFailed = ({ code }) => {
  return action(constant.GET_VERIFY_TOKEN_NUMBERPHONE_FAILED, { code });
};
module.exports.getVerifyTokenNumberPhoneFailed = getVerifyTokenNumberPhoneFailed;


/**
 * @override Yêu cầu mã xác thực số điện thoại người dùng
 *
 * @param   {Object} verifyTokenNumberPhone
 * @returns {Object}
 */
const verifyNumberPhoneRequest = (verifyTokenNumberPhone) => {
  return action(constant.VERIFY_NUMBERPHONE_REQUEST, { verifyTokenNumberPhone });
};
module.exports.verifyNumberPhoneRequest = verifyNumberPhoneRequest;

/**
 * @override Xác thực số điện thoại người dùng thành công
 *
 * @param   {Object}
 * @returns {Object}
 */
const verifyNumberPhoneSuccess = ({ code, verifyNumberPhone }) => {
  return action(constant.VERIFY_NUMBERPHONE_SUCCESS, { code, verifyNumberPhone });
};
module.exports.verifyNumberPhoneSuccess = verifyNumberPhoneSuccess;

/**
 * @override Xác thực số điện thoại người dùng thất bại
 *
 * @param   {Object}
 * @returns {Object}
 */
const verifyNumberPhoneFailed = ({ code }) => {
  return action(constant.VERIFY_NUMBERPHONE_FAILED, { code });
};
module.exports.verifyNumberPhoneFailed = verifyNumberPhoneFailed;

/**
 * @override Yêu cầu lấy người dùng theo id
 *
 * @param   {ObjectId} userID
 * @returns {Object}
 */
const getUserIDRequest = (userID) => {
  return action(constant.GET_USER_ID_REQUEST, { userID });
};
module.exports.getUserIDRequest = getUserIDRequest;

/**
 * @override Lấy danh người dùng id thành công
 *
 * @param   {Object}
 * @returns {Object}
 */
const getUserIDSuccess = ({ code, user }) => {
  return action(constant.GET_USER_ID_SUCCESS, { code, user });
};
module.exports.getUserIDSuccess = getUserIDSuccess;

/**
 * @override Lấy danh người dùng id thất bại
 *
 * @param   {Object}
 * @returns {Object}
 */
const getUserIDFailed = ({ code }) => {
  return action(constant.GET_USER_ID_FAILED, { code });
};
module.exports.getUserIDFailed = getUserIDFailed;

/**
 * @override Yêu cầu lấy người dùng theo id lập lại
 *
 * @param   {Object}
 * @returns {Object}
 */
const getUserIDReloadRequest = ({ userID }) => {
  return action(constant.GET_USER_ID_RELOAD_REQUEST, { userID });
};
module.exports.getUserIDReloadRequest = getUserIDReloadRequest;

/**
 * @override Thiết lặp lại cửa hàng
 *
 * @param {String}  fragment
 * @returns {Object}
 */
const resetUserStore = (fragment) => {
  return action(constant.RESERT_USER_STORE, { fragment });
};
module.exports.resetUserStore = resetUserStore;
