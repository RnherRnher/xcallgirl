const { action } = require('../../../core/client/utils/middleware');
const constant = require('../constants/admin.client.constant');

/**
 * @override Yêu cầu lấy người dùng
 *
 * @param   {String}  fragment
 * @param   {Object}  body
 * @param   {Number}  skip
 * @param   {Boolean} reload
 * @returns {Object}
 */
const getUsersRequest = (
  fragment,
  extendSearch,
  skip,
  reload = false
) => {
  return action(
    constant.GET_USERS_REQUEST,
    {
      fragment,
      extendSearch,
      skip: reload ? 0 : (skip || 0),
      reload
    }
  );
};
module.exports.getUsersRequest = getUsersRequest;

/**
 * @override Lấy người dùng thành công
 *
 * @param   {Object}
 * @returns {Object}
 */
const getUsersSuccess = ({
  code,
  skip,
  users
}) => {
  return action(
    constant.GET_USERS_SUCCESS,
    {
      code,
      skip,
      users
    }
  );
};
module.exports.getUsersSuccess = getUsersSuccess;

/**
 * @override Lấy người dùng thất bại
 *
 * @param   {Object}
 * @returns {Object}
 */
const getUsersFailed = ({ code }) => {
  return action(constant.GET_USERS_FAILED, { code });
};
module.exports.getUsersFailed = getUsersFailed;

/**
 * @override Yêu cầu xác thực người dùng
 *
 * @param   {ObjectId}  userID
 * @param   {Object}    verifyAccount
 * @returns {Object}
 */
const verifyAccountRequest = (userID, verifyAccount) => {
  return action(constant.VERIFY_ACCOUNT_REQUEST, { userID, verifyAccount });
};
module.exports.verifyAccountRequest = verifyAccountRequest;

/**
 * @override Xác thực người dùng thành công
 *
 * @param   {Object}
 * @returns {Object}
 */
const verifyAccountSuccess = ({ code }) => {
  return action(constant.VERIFY_ACCOUNT_SUCCESS, { code });
};
module.exports.verifyAccountSuccess = verifyAccountSuccess;

/**
 * @override Xác thực người dùng thất bại
 *
 * @param   {Object}
 * @returns {Object}
 */
const verifyAccountFailed = ({ code }) => {
  return action(constant.VERIFY_ACCOUNT_FAILED, { code });
};
module.exports.verifyAccountFailed = verifyAccountFailed;

/**
 * @override Yêu cầu xóa người dùng
 *
 * @param   {ObjectId}  userID
 * @param   {Object}    deleteAccount
 * @returns {Object}
 */
const deleteAccountRequest = (userID, deleteAccount) => {
  return action(constant.DELETE_ACCOUNT_REQUEST, { userID, deleteAccount });
};
module.exports.deleteAccountRequest = deleteAccountRequest;

/**
 * @override Xóa người dùng thành công
 *
 * @param   {Object}
 * @returns {Object}
 */
const deleteAccountSuccess = ({ code }) => {
  return action(constant.DELETE_ACCOUNT_SUCCESS, { code });
};
module.exports.deleteAccountSuccess = deleteAccountSuccess;

/**
 * @override Xóa người dùng thất bại
 *
 * @param   {Object}
 * @returns {Object}
 */
const deleteAccountFailed = ({ code }) => {
  return action(constant.DELETE_ACCOUNT_FAILED, { code });
};
module.exports.deleteAccountFailed = deleteAccountFailed;

/**
 * @override Yêu cầu khôi phục người dùng
 *
 * @param   {ObjectId}  userID
 * @param   {Object}    restoreAccount
 * @returns {Object}
 */
const restoreAccountRequest = (userID, restoreAccount) => {
  return action(constant.RESTORE_ACCOUNT_REQUEST, { userID, restoreAccount });
};
module.exports.restoreAccountRequest = restoreAccountRequest;

/**
 * @override Khôi phục người dùng thành công
 *
 * @param   {Object}
 * @returns {Object}
 */
const restoreAccountSuccess = ({ code }) => {
  return action(constant.RESTORE_ACCOUNT_SUCCESS, { code });
};
module.exports.restoreAccountSuccess = restoreAccountSuccess;

/**
 * @override Khôi phục người dùng thất bại
 *
 * @param   {Object}
 * @returns {Object}
 */
const restoreAccountFailed = ({ code }) => {
  return action(constant.RESTORE_ACCOUNT_FAILED, { code });
};
module.exports.restoreAccountFailed = restoreAccountFailed;

/**
 * @override Yêu cầu khóa người dùng
 *
 * @param   {ObjectId}  userID
 * @param   {Object}    lockAccount
 * @returns {Object}
 */
const lockAccountRequest = (userID, lockAccount) => {
  return action(constant.LOCK_ACCOUNT_REQUEST, { userID, lockAccount });
};
module.exports.lockAccountRequest = lockAccountRequest;

/**
 * @override Khóa người dùng thành công
 *
 * @param   {Object}
 * @returns {Object}
 */
const lockAccountSuccess = ({ code }) => {
  return action(constant.LOCK_ACCOUNT_SUCCESS, { code });
};
module.exports.lockAccountSuccess = lockAccountSuccess;

/**
 * @override Khóa người dùng thất bại
 *
 * @param   {Object}
 * @returns {Object}
 */
const lockAccountFailed = ({ code }) => {
  return action(constant.LOCK_ACCOUNT_FAILED, { code });
};
module.exports.lockAccountFailed = lockAccountFailed;

/**
 * @override Yêu cầu mở khóa người dùng
 *
 * @param   {ObjectId}  userID
 * @returns {Object}
 */
const unlockAccountRequest = (userID) => {
  return action(constant.UNLOCK_ACCOUNT_REQUEST, { userID });
};
module.exports.unlockAccountRequest = unlockAccountRequest;

/**
 * @override Mở khóa người dùng thành công
 *
 * @param   {Object}
 * @returns {Object}
 */
const unlockAccountSuccess = ({ code }) => {
  return action(constant.UNLOCK_ACCOUNT_SUCCESS, { code });
};
module.exports.unlockAccountSuccess = unlockAccountSuccess;

/**
 * @override Mở khóa người dùng thất bại
 *
 * @param   {Object}
 * @returns {Object}
 */
const unlockAccountFailed = ({ code }) => {
  return action(constant.UNLOCK_ACCOUNT_FAILED, { code });
};
module.exports.unlockAccountFailed = unlockAccountFailed;

/**
 * @override Thiết lặp lại cửa hàng
 *
 * @param {String}  fragment
 * @returns {Object}
 */
const resetAdminStore = (fragment) => {
  return action(constant.RESERT_ADMIN_STORE, { fragment });
};
module.exports.resetAdminStore = resetAdminStore;
