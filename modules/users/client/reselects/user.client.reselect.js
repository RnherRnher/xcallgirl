const constant = require('../../../../commons/utils/constant');
const { createSelector } = require('reselect');
const { getAddress, makeSelect } = require('../../../core/client/utils/middleware');

const selectUser = (state, props) => {
  return state.get('user');
};

const makeSelectSelfUser = () => {
  return createSelector(
    selectUser,
    (userState) => {
      return makeSelect(userState.get('self'));
    }
  );
};
module.exports.makeSelectSelfUser = makeSelectSelfUser;

const makeSelectIsOwner = (userID) => {
  return createSelector(
    selectUser,
    (userState) => {
      return makeSelect(userState.getIn(['self', 'data', '_id'])) === userID;
    }
  );
};
module.exports.makeSelectIsOwner = makeSelectIsOwner;

const makeSelectSelfUserInfo = () => {
  return createSelector(
    selectUser,
    (userState) => {
      const user = makeSelect(userState.getIn(['self', 'data']));

      return {
        address: {
          root: user.address,
          flag: getAddress(constant.COUNTRY, user.address).flag,
          country: getAddress(constant.COUNTRY, user.address).name,
          city: getAddress(constant.CITY, user.address).name,
          county: getAddress(constant.COUNTY, user.address).name,
          local: getAddress(constant.LOCAL, user.address).name,
        },
        name: user.name,
        birthday: user.birthday,
        contacts: {
          numberPhone: user.numberPhone,
        },
        avatar: user.avatar,
        gender: user.gender,
        roles: user.roles,
        verify: user.verify,
        initDate: user.initDate,
      };
    }
  );
};
module.exports.makeSelectSelfUserInfo = makeSelectSelfUserInfo;

const makeSelectDetailUser = () => {
  return createSelector(
    selectUser,
    (userState) => {
      return makeSelect(userState.get('detail'));
    }
  );
};
module.exports.makeSelectDetailUser = makeSelectDetailUser;

const makeSelectPrivacyUser = () => {
  return createSelector(
    selectUser,
    (userState) => {
      const user = makeSelect(userState.getIn(['self', 'data']));

      return {
        address: {
          flag: getAddress(constant.COUNTRY, user.address).flag,
          country: getAddress(constant.COUNTRY, user.address).name,
          city: getAddress(constant.CITY, user.address).name,
          county: getAddress(constant.COUNTY, user.address).name,
          local: getAddress(constant.LOCAL, user.address).name,
        },
        birthday: user.birthday,
        contacts: {
          numberPhone: user.numberPhone,
        }
      };
    }
  );
};
module.exports.makeSelectPrivacyUser = makeSelectPrivacyUser;

const makeSelectVerifyUser = (type) => {
  return createSelector(
    selectUser,
    (userState) => {
      return makeSelect(userState.getIn(['verify', type]));
    }
  );
};
module.exports.makeSelectVerifyUser = makeSelectVerifyUser;

const makeSelectChangesUser = (type) => {
  return createSelector(
    selectUser,
    (userState) => {
      return makeSelect(userState.getIn(['changes', type]));
    }
  );
};
module.exports.makeSelectChangesUser = makeSelectChangesUser;

const makeSelectGeneralSettings = (type) => {
  return createSelector(
    selectUser,
    (userState) => {
      return makeSelect(userState.getIn(['settings', 'data', 'general', type]));
    }
  );
};
module.exports.makeSelectGeneralSettings = makeSelectGeneralSettings;

const makeSelectSecuritySettings = (type) => {
  return createSelector(
    selectUser,
    (userState) => {
      return makeSelect(userState.getIn(['settings', 'data', 'security', type]));
    }
  );
};
module.exports.makeSelectSecuritySettings = makeSelectSecuritySettings;
