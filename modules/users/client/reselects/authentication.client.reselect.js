const { createSelector } = require('reselect');
const { makeSelect } = require('../../../core/client/utils/middleware');

const selectAuthentication = (state, props) => {
  return state.get('authentication');
};

const makeSelectAuthSignin = () => {
  return createSelector(
    selectAuthentication,
    (authenticationState) => {
      return makeSelect(authenticationState.get('signin'));
    }
  );
};
module.exports.makeSelectAuthSignin = makeSelectAuthSignin;

const makeSelectAuthSignup = () => {
  return createSelector(
    selectAuthentication,
    (authenticationState) => {
      return makeSelect(authenticationState.get('signup'));
    }
  );
};
module.exports.makeSelectAuthSignup = makeSelectAuthSignup;

const makeSelectForgotPassword = () => {
  return createSelector(
    selectAuthentication,
    (authenticationState) => {
      return makeSelect(authenticationState.get('forgotPassword'));
    }
  );
};
module.exports.makeSelectForgotPassword = makeSelectForgotPassword;
