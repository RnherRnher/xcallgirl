const { createSelector } = require('reselect');
const { makeSelect } = require('../../../core/client/utils/middleware');

const selectAdmin = (state, props) => {
  return state.get('admin');
};

const makeSelectListUser = () => {
  return createSelector(
    selectAdmin,
    (adminState) => {
      return makeSelect(adminState.get('list'));
    }
  );
};
module.exports.makeSelectListUser = makeSelectListUser;

const makeSelectActions = (type) => {
  return createSelector(
    selectAdmin,
    (adminState) => {
      return makeSelect(adminState.getIn(['actions', type]));
    }
  );
};
module.exports.makeSelectActions = makeSelectActions;
