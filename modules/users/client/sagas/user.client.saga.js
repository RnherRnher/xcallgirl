const nprogress = require('nprogress');
const api = require('../services/user.client.service');
const action = require('../actions/user.client.action');
const constant = require('../constants/user.client.constant');
const constantCommon = require('../../../../commons/utils/constant');
const url = require('../../../core/client/utils/url');
const { push } = require('connected-react-router/immutable');
const {
  takeLatest,
  put,
  call,
  takeEvery
} = require('redux-saga/effects');
const { pushAlertSnackbar } = require('../../../core/client/actions/core.client.action');
const { changeLanguage } = require('../../../core/client/actions/locale.client.action');
const { csrfRequest } = require('../../../core/client/services/core.client.service');
const { initApp } = require('../../../core/client/actions/app.client.action');

const watchGetProfileUser = function* () {
  yield takeLatest(constant.GET_PROFILE_USER_REQUEST, function* ({ isChangeLanguage, isAlert }) {
    try {
      if (isAlert) {
        nprogress.start();
      }

      const response = yield call(api.getProfileUser);

      yield put(action.getProfileUserSuccess(response.body));

      if (isChangeLanguage) {
        yield put(changeLanguage(response.body.user.language));
      }
    } catch (error) {
      if (error && error.body) {
        yield put(action.getProfileUserFailed(error.body));
        if (isAlert) {
          yield put(pushAlertSnackbar({
            variant: constantCommon.ERROR,
            code: error.body.code
          }));
        }
      } else {
        yield put(initApp());
      }
    } finally {
      if (isAlert) {
        nprogress.done();
      }
    }
  });
};
module.exports.watchGetProfileUser = watchGetProfileUser;

const watchGetProfileUserReload = function* () {
  yield takeLatest(constant.GET_PROFILE_USER_RELOAD_REQUEST, function* ({ isChangeLanguage }) {
    const response = yield call(api.getProfileUser);

    yield put(action.getProfileUserSuccess(response.body));

    if (isChangeLanguage) {
      yield put(changeLanguage(response.body.user.language));
    }
  });
};
module.exports.watchGetProfileUserReload = watchGetProfileUserReload;

const watchGetProfileSettings = function* () {
  yield takeLatest(constant.GET_PROFILE_SETTINGS_REQUEST, function* ({ fragment, isAlert }) {
    try {
      if (isAlert) {
        nprogress.start();
      }

      const response = yield call(
        api.getProfileSettings,
        { type: fragment }
      );

      yield put(action.getProfileSettingsSuccess(response.body));
    } catch (error) {
      if (error && error.body) {
        yield put(action.getProfileSettingsFailed(error.body));
        if (isAlert) {
          yield put(pushAlertSnackbar({
            variant: constantCommon.ERROR,
            code: error.body.code
          }));
        }
      } else {
        yield put(initApp());
      }
    } finally {
      if (isAlert) {
        nprogress.done();
      }
    }
  });
};
module.exports.watchGetProfileSettings = watchGetProfileSettings;

const watchChangeProfileSettings = function* () {
  yield takeLatest(constant.CHANGE_PROFILE_SETTINGS_REQUEST, function* ({ fragment, settings }) {
    try {
      nprogress.start();

      const responseCSRF = yield call(csrfRequest);
      const response = yield call(
        api.changeProfileSettings,
        {
          csrfToken: responseCSRF.body.csrfToken,
          type: fragment,
          settings
        }
      );

      yield put(action.changeProfileSettingsSuccess(response.body));
      yield put(pushAlertSnackbar({
        variant: constantCommon.SUCCESS,
        code: response.body.code
      }));
    } catch (error) {
      if (error && error.body) {
        yield put(action.changeProfileSettingsFailed(error.body));
        yield put(pushAlertSnackbar({
          variant: constantCommon.ERROR,
          code: error.body.code
        }));
      } else {
        yield put(initApp());
      }
    } finally {
      nprogress.done();
    }
  });
};
module.exports.watchChangeProfileSettings = watchChangeProfileSettings;

const watchChangeProfilePassword = function* () {
  yield takeLatest(constant.CHANGE_PROFILE_PASSWORD_REQUEST, function* ({ passwordDetails }) {
    try {
      nprogress.start();

      const responseCSRF = yield call(csrfRequest);
      const response = yield call(
        api.changeProfilePassword,
        {
          csrfToken: responseCSRF.body.csrfToken,
          passwordDetails
        }
      );

      yield put(action.changeProfilePasswordSuccess(response.body));
      yield put(pushAlertSnackbar({
        variant: constantCommon.SUCCESS,
        code: response.body.code
      }));

      yield put(push(url.SETTINGS));
    } catch (error) {
      if (error && error.body) {
        yield put(action.changeProfilePasswordFailed(error.body));
        yield put(pushAlertSnackbar({
          variant: constantCommon.ERROR,
          code: error.body.code
        }));
      } else {
        yield put(initApp());
      }
    } finally {
      nprogress.done();
    }
  });
};
module.exports.watchChangeProfilePassword = watchChangeProfilePassword;

const watchChangeProfileNumberPhone = function* () {
  yield takeLatest(constant.CHANGE_PROFILE_NUMBERPHONE_REQUEST, function* ({ numberPhone }) {
    try {
      nprogress.start();

      const responseCSRF = yield call(csrfRequest);
      const response = yield call(
        api.changeProfileNumberPhone,
        {
          csrfToken: responseCSRF.body.csrfToken,
          numberPhone
        }
      );

      yield put(action.changeProfileNumberPhoneSuccess(response.body));
      yield put(pushAlertSnackbar({
        variant: constantCommon.SUCCESS,
        code: response.body.code
      }));
    } catch (error) {
      if (error && error.body) {
        yield put(action.changeProfileNumberPhoneFailed(error.body));
        yield put(pushAlertSnackbar({
          variant: constantCommon.ERROR,
          code: error.body.code
        }));
      } else {
        yield put(initApp());
      }
    } finally {
      nprogress.done();
    }
  });
};
module.exports.watchChangeProfileNumberPhone = watchChangeProfileNumberPhone;

const watchChangeProfileAddress = function* () {
  yield takeLatest(constant.CHANGE_PROFILE_ADDRESS_REQUEST, function* ({ address }) {
    try {
      nprogress.start();

      const responseCSRF = yield call(csrfRequest);
      const response = yield call(
        api.changeProfileAddress,
        {
          csrfToken: responseCSRF.body.csrfToken,
          address
        }
      );

      yield put(action.changeProfileAddressSuccess(response.body));
      yield put(pushAlertSnackbar({
        variant: constantCommon.SUCCESS,
        code: response.body.code
      }));
    } catch (error) {
      if (error && error.body) {
        yield put(action.changeProfileAddressFailed(error.body));
        yield put(pushAlertSnackbar({
          variant: constantCommon.ERROR,
          code: error.body.code
        }));
      } else {
        yield put(initApp());
      }
    } finally {
      nprogress.done();
    }
  });
};
module.exports.watchChangeProfileAddress = watchChangeProfileAddress;

const watchChangeProfileAvatar = function* () {
  yield takeLatest(constant.CHANGE_PROFILE_AVATAR_REQUEST, function* ({ image }) {
    try {
      nprogress.start();

      const responseCSRF = yield call(csrfRequest);
      const response = yield call(
        api.changeProfileAvatar,
        {
          csrfToken: responseCSRF.body.csrfToken,
          image
        }
      );

      yield put(action.changeProfileAvatarSuccess(response.body));
      yield put(pushAlertSnackbar({
        variant: constantCommon.SUCCESS,
        code: response.body.code
      }));
    } catch (error) {
      if (error && error.body) {
        yield put(action.changeProfileAvatarFailed(error.body));
        yield put(pushAlertSnackbar({
          variant: constantCommon.ERROR,
          code: error.body.code
        }));
      } else {
        yield put(initApp());
      }
    } finally {
      nprogress.done();
    }
  });
};
module.exports.watchChangeProfileAvatar = watchChangeProfileAvatar;

const watchDeleteProfileAvatar = function* () {
  yield takeLatest(constant.DELETE_PROFILE_AVATAR_REQUEST, function* () {
    try {
      nprogress.start();

      const responseCSRF = yield call(csrfRequest);
      const response = yield call(
        api.deleteProfileAvatar,
        { csrfToken: responseCSRF.body.csrfToken }
      );

      yield put(action.deleteProfileAvatarSuccess(response.body));
      yield put(pushAlertSnackbar({
        variant: constantCommon.SUCCESS,
        code: response.body.code
      }));
    } catch (error) {
      if (error && error.body) {
        yield put(action.deleteProfileAvatarFailed(error.body));
        yield put(pushAlertSnackbar({
          variant: constantCommon.ERROR,
          code: error.body.code
        }));
      } else {
        yield put(initApp());
      }
    } finally {
      nprogress.done();
    }
  });
};
module.exports.watchDeleteProfileAvatar = watchDeleteProfileAvatar;

const watchRequestVerifyAccount = function* () {
  yield takeLatest(constant.REQUEST_VERIFY_ACCOUNT_REQUEST, function* ({ image }) {
    try {
      nprogress.start();

      const responseCSRF = yield call(csrfRequest);
      const response = yield call(
        api.requestVerifyAccount,
        {
          csrfToken: responseCSRF.body.csrfToken,
          image
        }
      );

      yield put(action.requestVerifyAccountSuccess(response.body));
      yield put(pushAlertSnackbar({
        variant: constantCommon.SUCCESS,
        code: response.body.code
      }));
    } catch (error) {
      if (error && error.body) {
        yield put(action.requestVerifyAccountFailed(error.body));
        yield put(pushAlertSnackbar({
          variant: constantCommon.ERROR,
          code: error.body.code
        }));
      } else {
        yield put(initApp());
      }
    } finally {
      nprogress.done();
    }
  });
};
module.exports.watchRequestVerifyAccount = watchRequestVerifyAccount;

const watchGetVerifyTokenNumberPhone = function* () {
  yield takeLatest(constant.GET_VERIFY_TOKEN_NUMBERPHONE_REQUEST, function* () {
    try {
      nprogress.start();

      const response = yield call(api.getVerifyTokenNumberPhone);

      yield put(action.getVerifyTokenNumberPhoneSuccess(response.body));
      yield put(pushAlertSnackbar({
        variant: constantCommon.SUCCESS,
        code: response.body.code
      }));
    } catch (error) {
      if (error && error.body) {
        yield put(action.getVerifyTokenNumberPhoneFailed(error.body));
        yield put(pushAlertSnackbar({
          variant: constantCommon.ERROR,
          code: error.body.code
        }));
      } else {
        yield put(initApp());
      }
    } finally {
      nprogress.done();
    }
  });
};
module.exports.watchGetVerifyTokenNumberPhone = watchGetVerifyTokenNumberPhone;

const watchVerifyNumberPhone = function* () {
  yield takeLatest(constant.VERIFY_NUMBERPHONE_REQUEST, function* ({ verifyTokenNumberPhone }) {
    try {
      nprogress.start();

      const responseCSRF = yield call(csrfRequest);
      const response = yield call(
        api.verifyNumberPhone,
        {
          csrfToken: responseCSRF.body.csrfToken,
          verifyTokenNumberPhone
        }
      );

      yield put(action.verifyNumberPhoneSuccess(response.body));
      yield put(pushAlertSnackbar({
        variant: constantCommon.SUCCESS,
        code: response.body.code
      }));
    } catch (error) {
      if (error && error.body) {
        yield put(action.verifyNumberPhoneFailed(error.body));
        yield put(pushAlertSnackbar({
          variant: constantCommon.ERROR,
          code: error.body.code
        }));
      } else {
        yield put(initApp());
      }
    } finally {
      nprogress.done();
    }
  });
};
module.exports.watchVerifyNumberPhone = watchVerifyNumberPhone;

const watchGetUserID = function* () {
  yield takeLatest(constant.GET_USER_ID_REQUEST, function* ({ userID }) {
    try {
      nprogress.start();

      const response = yield call(
        api.getUserID,
        { userID }
      );

      yield put(action.getUserIDSuccess(response.body));
    } catch (error) {
      if (error && error.body) {
        yield put(action.getUserIDFailed(error.body));
      } else {
        yield put(initApp());
      }
    } finally {
      nprogress.done();
    }
  });
};
module.exports.watchGetUserID = watchGetUserID;

const watchGetUserIDReload = function* () {
  yield takeEvery(constant.GET_USER_ID_RELOAD_REQUEST, function* ({ userID }) {
    const response = yield call(
      api.getUserID,
      { userID }
    );

    yield put(action.getUserIDSuccess(response.body));
  });
};
module.exports.watchGetUserIDReload = watchGetUserIDReload;
