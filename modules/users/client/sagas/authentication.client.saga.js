const nprogress = require('nprogress');
const api = require('../services/authentication.client.service');
const action = require('../actions/authentication.client.action');
const constant = require('../constants/authentication.client.constant');
const constantCommon = require('../../../../commons/utils/constant');
const {
  takeLatest,
  put,
  call
} = require('redux-saga/effects');
const { csrfRequest } = require('../../../core/client/services/core.client.service');
const { pushAlertSnackbar } = require('../../../core/client/actions/core.client.action');
const { initApp, resetApp } = require('../../../core/client/actions/app.client.action');

const watchSignin = function* () {
  yield takeLatest(constant.SIGNIN_REQUEST, function* ({ signin }) {
    try {
      nprogress.start();

      const responseCSRF = yield call(csrfRequest);
      const response = yield call(
        api.signin,
        {
          csrfToken: responseCSRF.body.csrfToken,
          signin
        }
      );

      yield put(action.signinSuccess(response.body));

      yield put(initApp());
    } catch (error) {
      if (error && error.body) {
        yield put(action.signinFailed(error.body));
        yield put(pushAlertSnackbar({
          variant: constantCommon.ERROR,
          code: error.body.code
        }));
      } else {
        yield put(initApp());
      }
    } finally {
      nprogress.done();
    }
  });
};
module.exports.watchSignin = watchSignin;

const watchSignup = function* () {
  yield takeLatest(constant.SIGNUP_REQUEST, function* ({ signup }) {
    try {
      nprogress.start();

      const responseCSRF = yield call(csrfRequest);
      const response = yield call(
        api.signup,
        {
          csrfToken: responseCSRF.body.csrfToken,
          signup
        }
      );

      yield put(action.signupSuccess(response.body));

      yield put(initApp());
    } catch (error) {
      if (error && error.body) {
        yield put(action.signupFailed(error.body));
        yield put(pushAlertSnackbar({
          variant: constantCommon.ERROR,
          code: error.body.code
        }));
      } else {
        yield put(initApp());
      }
    } finally {
      nprogress.done();
    }
  });
};
module.exports.watchSignup = watchSignup;

const watchSignout = function* () {
  yield takeLatest(constant.SIGNOUT_REQUEST, function* () {
    try {
      nprogress.start();

      const response = yield call(api.signout);

      yield put(action.signoutSuccess(response.body));

      yield put(resetApp());
    } catch (error) {
      if (error && error.body) {
        yield put(action.signoutFailed(error.body));
        yield put(pushAlertSnackbar({
          variant: constantCommon.ERROR,
          code: error.body.code
        }));
      } else {
        yield put(initApp());
      }
    } finally {
      nprogress.done();
    }
  });
};
module.exports.watchSignout = watchSignout;

const watchForgotPassword = function* () {
  yield takeLatest(constant.FORGOT_PASSWORD_REQUEST, function* ({ forgotPassword }) {
    try {
      nprogress.start();

      const responseCSRF = yield call(csrfRequest);
      const response = yield call(
        api.forgotPassword,
        {
          csrfToken: responseCSRF.body.csrfToken,
          forgotPassword
        }
      );

      yield put(action.forgotPasswordSuccess(response.body));
      yield put(pushAlertSnackbar({
        variant: constantCommon.SUCCESS,
        code: response.body.code
      }));
    } catch (error) {
      if (error && error.body) {
        yield put(action.forgotPasswordFailed(error.body));
        yield put(pushAlertSnackbar({
          variant: constantCommon.ERROR,
          code: error.body.code
        }));
      } else {
        yield put(initApp());
      }
    } finally {
      nprogress.done();
    }
  });
};
module.exports.watchForgotPassword = watchForgotPassword;

const watchValidateResetTokenPassword = function* () {
  yield takeLatest(constant.VALIDATE_RESET_TOKEN_PASSWORD_REQUEST, function* ({ validateResetTokenPassword }) {
    try {
      nprogress.start();

      const responseCSRF = yield call(csrfRequest);
      const response = yield call(
        api.validateResetTokenPassword,
        {
          csrfToken: responseCSRF.body.csrfToken,
          validateResetTokenPassword
        }
      );

      yield put(action.forgotPasswordSuccess(response.body));
      yield put(pushAlertSnackbar({
        variant: constantCommon.SUCCESS,
        code: response.body.code
      }));
    } catch (error) {
      if (error && error.body) {
        yield put(action.forgotPasswordFailed(error.body));
        yield put(pushAlertSnackbar({
          variant: constantCommon.ERROR,
          code: error.body.code
        }));
      } else {
        yield put(initApp());
      }
    } finally {
      nprogress.done();
    }
  });
};
module.exports.watchValidateResetTokenPassword = watchValidateResetTokenPassword;

const watchResertPassword = function* () {
  yield takeLatest(constant.RESERT_PASSWORD_REQUEST, function* ({ resetPassword }) {
    try {
      nprogress.start();

      const responseCSRF = yield call(csrfRequest);
      const response = yield call(
        api.resetPassword,
        {
          csrfToken: responseCSRF.body.csrfToken,
          resetPassword
        }
      );

      yield put(action.forgotPasswordSuccess(response.body));
      yield put(pushAlertSnackbar({
        variant: constantCommon.SUCCESS,
        code: response.body.code
      }));

      yield put(initApp());
    } catch (error) {
      if (error && error.body) {
        yield put(action.forgotPasswordFailed(error.body));
        yield put(pushAlertSnackbar({
          variant: constantCommon.ERROR,
          code: error.body.code
        }));
      } else {
        yield put(initApp());
      }
    } finally {
      nprogress.done();
    }
  });
};
module.exports.watchResertPassword = watchResertPassword;
