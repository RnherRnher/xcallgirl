const nprogress = require('nprogress');
const api = require('../services/system.client.service');
const action = require('../actions/system.client.action');
const constant = require('../constants/system.client.constant');
const constantCommon = require('../../../../commons/utils/constant');
const {
  takeLatest,
  put,
  call
} = require('redux-saga/effects');
const { csrfRequest } = require('../../../core/client/services/core.client.service');
const { pushAlertSnackbar } = require('../../../core/client/actions/core.client.action');
const { getUserIDReloadRequest } = require('../actions/user.client.action');
const { emitSockets } = require('../../../core/client/lib/socket-io');
const { initApp } = require('../../../core/client/actions/app.client.action');

const watchChangeRolesAccount = function* () {
  yield takeLatest(constant.CHANGE_ROLES_ACCOUNT_REQUEST, function* ({ userID, changeRolesAccount }) {
    try {
      nprogress.start();

      const responseCSRF = yield call(csrfRequest);
      const response = yield call(
        api.changeRolesAccount,
        {
          csrfToken: responseCSRF.body.csrfToken,
          userID,
          changeRolesAccount
        }
      );

      yield put(action.changeRolesAccountSuccess(response.body));
      yield put(pushAlertSnackbar({
        variant: constantCommon.SUCCESS,
        code: response.body.code
      }));

      yield put(getUserIDReloadRequest({ userID }));
      yield call(emitSockets, response.body.sockets);
    } catch (error) {
      if (error && error.body) {
        yield put(action.changeRolesAccountFailed(error.body));
        yield put(pushAlertSnackbar({
          variant: constantCommon.ERROR,
          code: error.body.code
        }));
      } else {
        yield put(initApp());
      }
    } finally {
      nprogress.done();
    }
  });
};
module.exports.watchChangeRolesAccount = watchChangeRolesAccount;
