const nprogress = require('nprogress');
const api = require('../services/admin.client.service');
const action = require('../actions/admin.client.action');
const constant = require('../constants/admin.client.constant');
const constantCommon = require('../../../../commons/utils/constant');
const {
  takeLatest,
  put,
  call
} = require('redux-saga/effects');
const { csrfRequest } = require('../../../core/client/services/core.client.service');
const { pushAlertSnackbar } = require('../../../core/client/actions/core.client.action');
const { getUserIDReloadRequest } = require('../actions/user.client.action');
const { emitSockets } = require('../../../core/client/lib/socket-io');
const { initApp } = require('../../../core/client/actions/app.client.action');

const watchGetUsers = function* () {
  yield takeLatest(constant.GET_USERS_REQUEST, function* ({ fragment, extendSearch, skip }) {
    try {
      nprogress.start();

      const response = yield call(
        api.getUsers,
        {
          type: fragment,
          extendSearch,
          skip
        }
      );

      yield put(action.getUsersSuccess(response.body));
    } catch (error) {
      if (error && error.body) {
        yield put(action.getUsersFailed(error.body));
        if (skip !== 0) {
          yield put(pushAlertSnackbar({
            variant: constantCommon.ERROR,
            code: error.body.code
          }));
        }
      } else {
        yield put(initApp());
      }
    } finally {
      nprogress.done();
    }
  });
};
module.exports.watchGetUsers = watchGetUsers;

const watchVerifyAccount = function* () {
  yield takeLatest(constant.VERIFY_ACCOUNT_REQUEST, function* ({ userID, verifyAccount }) {
    try {
      nprogress.start();

      const responseCSRF = yield call(csrfRequest);
      const response = yield call(
        api.verifyAccount,
        {
          csrfToken: responseCSRF.body.csrfToken,
          userID,
          verifyAccount
        }
      );

      yield put(action.verifyAccountSuccess(response.body));
      yield put(pushAlertSnackbar({
        variant: constantCommon.SUCCESS,
        code: response.body.code
      }));

      yield put(getUserIDReloadRequest({ userID }));
      yield call(emitSockets, response.body.sockets);
    } catch (error) {
      if (error && error.body) {
        yield put(action.verifyAccountFailed(error.body));
        yield put(pushAlertSnackbar({
          variant: constantCommon.ERROR,
          code: error.body.code
        }));
      } else {
        yield put(initApp());
      }
    } finally {
      nprogress.done();
    }
  });
};
module.exports.watchVerifyAccount = watchVerifyAccount;

const watchDeleteAccount = function* () {
  yield takeLatest(constant.DELETE_ACCOUNT_REQUEST, function* ({ userID, deleteAccount }) {
    try {
      nprogress.start();

      const responseCSRF = yield call(csrfRequest);
      const response = yield call(
        api.deleteAccount,
        {
          csrfToken: responseCSRF.body.csrfToken,
          userID,
          deleteAccount
        }
      );

      yield put(action.deleteAccountSuccess(response.body));
      yield put(pushAlertSnackbar({
        variant: constantCommon.SUCCESS,
        code: response.body.code
      }));

      yield put(getUserIDReloadRequest({ userID }));
    } catch (error) {
      if (error && error.body) {
        yield put(action.deleteAccountFailed(error.body));
        yield put(pushAlertSnackbar({
          variant: constantCommon.ERROR,
          code: error.body.code
        }));
      } else {
        yield put(initApp());
      }
    } finally {
      nprogress.done();
    }
  });
};
module.exports.watchDeleteAccount = watchDeleteAccount;

const watchRestoreAccount = function* () {
  yield takeLatest(constant.RESTORE_ACCOUNT_REQUEST, function* ({ userID, restoreAccount }) {
    try {
      nprogress.start();

      const responseCSRF = yield call(csrfRequest);
      const response = yield call(
        api.restoreAccount,
        {
          csrfToken: responseCSRF.body.csrfToken,
          userID,
          restoreAccount
        }
      );

      yield put(action.restoreAccountSuccess(response.body));
      yield put(pushAlertSnackbar({
        variant: constantCommon.SUCCESS,
        code: response.body.code
      }));

      yield put(getUserIDReloadRequest({ userID }));
    } catch (error) {
      if (error && error.body) {
        yield put(action.restoreAccountFailed(error.body));
        yield put(pushAlertSnackbar({
          variant: constantCommon.ERROR,
          code: error.body.code
        }));
      } else {
        yield put(initApp());
      }
    } finally {
      nprogress.done();
    }
  });
};
module.exports.watchRestoreAccount = watchRestoreAccount;

const watchLockAccount = function* () {
  yield takeLatest(constant.LOCK_ACCOUNT_REQUEST, function* ({ userID, lockAccount }) {
    try {
      nprogress.start();

      const responseCSRF = yield call(csrfRequest);
      const response = yield call(
        api.lockAccount,
        {
          csrfToken: responseCSRF.body.csrfToken,
          userID,
          lockAccount
        }
      );

      yield put(action.lockAccountSuccess(response.body));
      yield put(pushAlertSnackbar({
        variant: constantCommon.SUCCESS,
        code: response.body.code
      }));

      yield put(getUserIDReloadRequest({ userID }));
    } catch (error) {
      if (error && error.body) {
        yield put(action.lockAccountFailed(error.body));
        yield put(pushAlertSnackbar({
          variant: constantCommon.ERROR,
          code: error.body.code
        }));
      } else {
        yield put(initApp());
      }
    } finally {
      nprogress.done();
    }
  });
};
module.exports.watchLockAccount = watchLockAccount;

const watchUnlockAccount = function* () {
  yield takeLatest(constant.UNLOCK_ACCOUNT_REQUEST, function* ({ userID }) {
    try {
      nprogress.start();

      const responseCSRF = yield call(csrfRequest);
      const response = yield call(
        api.unlockAccount,
        {
          csrfToken: responseCSRF.body.csrfToken,
          userID
        }
      );

      yield put(action.unlockAccountSuccess(response.body));
      yield put(pushAlertSnackbar({
        variant: constantCommon.SUCCESS,
        code: response.body.code
      }));

      yield put(getUserIDReloadRequest({ userID }));
    } catch (error) {
      if (error && error.body) {
        yield put(action.unlockAccountFailed(error.body));
        yield put(pushAlertSnackbar({
          variant: constantCommon.ERROR,
          code: error.body.code
        }));
      } else {
        yield put(initApp());
      }
    } finally {
      nprogress.done();
    }
  });
};
module.exports.watchUnlockAccount = watchUnlockAccount;
