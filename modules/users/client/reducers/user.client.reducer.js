const constant = require('../constants/user.client.constant');
const { fromJS } = require('immutable');
const { responseState } = require('../../../core/client/utils/middleware');
const { ResultEnum } = require('../../../core/client/constants/core.client.constant');

const initialState = fromJS({
  self: responseState(),
  settings: responseState(),
  changes: {
    password: responseState(),
    numberPhone: responseState(),
    address: responseState(),
    avatar: responseState(),
  },
  verify: {
    account: {
      request: responseState()
    },
    numberPhone: {
      getToken: responseState(),
      verify: responseState()
    }
  },
  detail: responseState()
});

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case constant.UPDATE_PROFILE: {
      return state
        .setIn(
          ['self', 'data', action.fragment],
          action.data
        );
    }
    case constant.GET_PROFILE_USER_REQUEST: {
      return state.set(
        'self',
        { result: ResultEnum.wait }
      );
    }
    case constant.GET_PROFILE_USER_SUCCESS: {
      return state.set(
        'self',
        {
          result: ResultEnum.success,
          code: action.code,
          data: action.user
        }
      );
    }
    case constant.GET_PROFILE_USER_FAILED: {
      return state.set(
        'self',
        {
          result: ResultEnum.failed,
          code: action.code
        }
      );
    }
    case constant.GET_PROFILE_SETTINGS_REQUEST: {
      return state.set(
        'settings',
        {
          result: ResultEnum.wait,
          fragment: action.fragment
        }
      );
    }
    case constant.GET_PROFILE_SETTINGS_SUCCESS: {
      return state.update(
        'settings',
        (settings) => {
          return {
            result: ResultEnum.success,
            code: action.code,
            data: action.settings,
            fragment: settings.fragment
          };
        }
      );
    }
    case constant.GET_PROFILE_SETTINGS_FAILED: {
      return state.update(
        'settings',
        (settings) => {
          return {
            result: ResultEnum.failed,
            code: action.code,
            fragment: settings.fragment
          };
        }
      );
    }
    case constant.CHANGE_PROFILE_SETTINGS_REQUEST: {
      return state.update(
        'settings',
        (settings) => {
          settings.result = ResultEnum.wait;
          settings.fragment = settings.fragment;

          return settings;
        }
      );
    }
    case constant.CHANGE_PROFILE_SETTINGS_SUCCESS: {
      return state.update(
        'settings',
        (settings) => {
          return {
            result: ResultEnum.success,
            code: action.code,
            data: action.settings,
            fragment: settings.fragment
          };
        }
      );
    }
    case constant.CHANGE_PROFILE_SETTINGS_FAILED: {
      return state.update(
        'settings',
        (settings) => {
          settings.result = ResultEnum.failed;
          settings.code = action.code;
          settings.fragment = settings.fragment;

          return settings;
        }
      );
    }
    case constant.CHANGE_PROFILE_PASSWORD_REQUEST: {
      return state.setIn(
        ['changes', 'password'],
        { result: ResultEnum.wait }
      );
    }
    case constant.CHANGE_PROFILE_PASSWORD_SUCCESS: {
      return state.setIn(
        ['changes', 'password'],
        {
          result: ResultEnum.success,
          code: action.code,
        }
      );
    }
    case constant.CHANGE_PROFILE_PASSWORD_FAILED: {
      return state.setIn(
        ['changes', 'password'],
        {
          result: ResultEnum.failed,
          code: action.code
        }
      );
    }
    case constant.CHANGE_PROFILE_NUMBERPHONE_REQUEST: {
      return state.setIn(
        ['changes', 'numberPhone'],
        { result: ResultEnum.wait }
      );
    }
    case constant.CHANGE_PROFILE_NUMBERPHONE_SUCCESS: {
      return state
        .setIn(['self', 'data', 'numberPhone'], action.numberPhone)
        .setIn(['self', 'data', 'verify', 'numberPhone'], action.verify)
        .setIn(
          ['changes', 'numberPhone'],
          {
            result: ResultEnum.success,
            code: action.code
          }
        );
    }
    case constant.CHANGE_PROFILE_NUMBERPHONE_FAILED: {
      return state.setIn(
        ['changes', 'numberPhone'],
        {
          result: ResultEnum.failed,
          code: action.code
        }
      );
    }
    case constant.CHANGE_PROFILE_ADDRESS_REQUEST: {
      return state.setIn(
        ['changes', 'address'],
        { result: ResultEnum.wait }
      );
    }
    case constant.CHANGE_PROFILE_ADDRESS_SUCCESS: {
      return state
        .setIn(['self', 'data', 'address'], action.address)
        .setIn(
          ['changes', 'address'],
          {
            result: ResultEnum.success,
            code: action.code
          }
        );
    }
    case constant.CHANGE_PROFILE_ADDRESS_FAILED: {
      return state.setIn(
        ['changes', 'address'],
        {
          result: ResultEnum.failed,
          code: action.code
        }
      );
    }
    case constant.DELETE_PROFILE_AVATAR_REQUEST:
    case constant.CHANGE_PROFILE_AVATAR_REQUEST: {
      return state.setIn(
        ['changes', 'avatar'],
        { result: ResultEnum.wait }
      );
    }
    case constant.DELETE_PROFILE_AVATAR_SUCCESS:
    case constant.CHANGE_PROFILE_AVATAR_SUCCESS: {
      return state
        .setIn(['self', 'data', 'avatar', 'url'], action.urlAvatar)
        .setIn(
          ['changes', 'avatar'],
          {
            result: ResultEnum.success,
            code: action.code
          }
        );
    }
    case constant.DELETE_PROFILE_AVATAR_FAILED:
    case constant.CHANGE_PROFILE_AVATAR_FAILED: {
      return state.setIn(
        ['changes', 'avatar'],
        {
          result: ResultEnum.failed,
          code: action.code
        }
      );
    }
    case constant.REQUEST_VERIFY_ACCOUNT_REQUEST: {
      return state.setIn(
        ['verify', 'account', 'request'],
        { result: ResultEnum.wait }
      );
    }
    case constant.REQUEST_VERIFY_ACCOUNT_SUCCESS: {
      return state
        .setIn(['self', 'data', 'verify', 'account'], action.verifyAccount)
        .setIn(
          ['verify', 'account', 'request'],
          {
            result: ResultEnum.success,
            code: action.code
          }
        );
    }
    case constant.REQUEST_VERIFY_ACCOUNT_FAILED: {
      return state.setIn(
        ['verify', 'account', 'request'],
        {
          result: ResultEnum.failed,
          code: action.code
        }
      );
    }
    case constant.GET_VERIFY_TOKEN_NUMBERPHONE_REQUEST: {
      return state.setIn(
        ['verify', 'numberPhone', 'getToken'],
        { result: ResultEnum.wait }
      );
    }
    case constant.GET_VERIFY_TOKEN_NUMBERPHONE_SUCCESS: {
      return state.setIn(
        ['verify', 'numberPhone', 'getToken'],
        {
          result: ResultEnum.success,
          code: action.code
        }
      );
    }
    case constant.GET_VERIFY_TOKEN_NUMBERPHONE_FAILED: {
      return state.setIn(
        ['verify', 'numberPhone', 'getToken'],
        {
          result: ResultEnum.failed,
          code: action.code
        }
      );
    }
    case constant.VERIFY_NUMBERPHONE_REQUEST: {
      return state.setIn(
        ['verify', 'numberPhone', 'verify'],
        { result: ResultEnum.wait }
      );
    }
    case constant.VERIFY_NUMBERPHONE_SUCCESS: {
      return state
        .setIn(['self', 'data', 'verify', 'numberPhone'], action.verifyNumberPhone)
        .setIn(
          ['verify', 'numberPhone', 'verify'],
          {
            result: ResultEnum.success,
            code: action.code
          }
        );
    }
    case constant.VERIFY_NUMBERPHONE_FAILED: {
      return state.setIn(
        ['verify', 'numberPhone', 'verify'],
        {
          result: ResultEnum.failed,
          code: action.code
        }
      );
    }
    case constant.GET_USER_ID_REQUEST: {
      return state.set(
        'detail',
        { result: ResultEnum.wait }
      );
    }
    case constant.GET_USER_ID_SUCCESS: {
      return state.set(
        'detail',
        {
          result: ResultEnum.success,
          code: action.code,
          data: action.user
        }
      );
    }
    case constant.GET_USER_ID_FAILED: {
      return state.set(
        'detail',
        {
          result: ResultEnum.failed,
          code: action.code
        }
      );
    }
    case constant.RESERT_USER_STORE: {
      if (action.fragment) {
        return state.set(
          action.fragment,
          initialState.get(action.fragment)
        );
      }

      return initialState;
    }
    default:
      return state;
  }
};
module.exports = reducer;
