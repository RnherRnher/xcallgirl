const constant = require('../constants/authentication.client.constant');
const { responseState } = require('../../../core/client/utils/middleware');
const { ResultEnum } = require('../../../core/client/constants/core.client.constant');
const { fromJS, isCollection } = require('immutable');

const initialState = fromJS({
  signin: responseState(),
  signup: responseState(),
  signout: responseState(),
  forgotPassword: responseState({ steps: 0 }),
});

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case constant.SIGNIN_REQUEST: {
      return state.set(
        'signin',
        { result: ResultEnum.wait }
      );
    }
    case constant.SIGNIN_SUCCESS: {
      return state.set(
        'signin',
        {
          result: ResultEnum.success,
          code: action.code
        }
      );
    }
    case constant.SIGNIN_FAILED: {
      return state.set(
        'signin',
        {
          result: ResultEnum.failed,
          code: action.code
        }
      );
    }
    case constant.SIGNUP_REQUEST: {
      return state.set(
        'signup',
        { result: ResultEnum.wait }
      );
    }
    case constant.SIGNUP_SUCCESS: {
      return state.set(
        'signup',
        {
          result: ResultEnum.success,
          code: action.code
        }
      );
    }
    case constant.SIGNUP_FAILED: {
      return state.set(
        'signup',
        {
          result: ResultEnum.failed,
          code: action.code
        }
      );
    }
    case constant.SIGNOUT_REQUEST: {
      return state.set(
        'signout',
        { result: ResultEnum.wait }
      );
    }
    case constant.SIGNOUT_SUCCESS: {
      return state.set(
        'signout',
        {
          result: ResultEnum.success,
          code: action.code
        }
      );
    }
    case constant.SIGNOUT_FAILED: {
      return state.set(
        'signout',
        {
          result: ResultEnum.failed,
          code: action.code
        }
      );
    }
    case constant.RESERT_PASSWORD_REQUEST:
    case constant.VALIDATE_RESET_TOKEN_PASSWORD_REQUEST:
    case constant.FORGOT_PASSWORD_REQUEST: {
      return state.update(
        'forgotPassword',
        (forgotPassword) => {
          return {
            result: ResultEnum.wait,
            steps: isCollection(forgotPassword)
              ? forgotPassword.get('steps')
              : forgotPassword.steps
          };
        }
      );
    }
    case constant.FORGOT_PASSWORD_SUCCESS: {
      return state.update(
        'forgotPassword',
        (forgotPassword) => {
          return {
            result: ResultEnum.success,
            code: action.code,
            steps: 1 + (isCollection(forgotPassword)
              ? forgotPassword.get('steps')
              : forgotPassword.steps)
          };
        }
      );
    }
    case constant.FORGOT_PASSWORD_FAILED: {
      return state.update(
        'forgotPassword',
        (forgotPassword) => {
          return {
            result: ResultEnum.failed,
            code: action.code,
            steps: isCollection(forgotPassword)
              ? forgotPassword.get('steps')
              : forgotPassword.steps
          };
        }
      );
    }
    case constant.RESERT_AUTH_STORE: {
      return initialState;
    }
    default:
      return state;
  }
};
module.exports = reducer;
