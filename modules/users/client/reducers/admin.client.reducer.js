const constant = require('../constants/admin.client.constant');
const { responseState } = require('../../../core/client/utils/middleware');
const { ResultEnum } = require('../../../core/client/constants/core.client.constant');
const { fromJS } = require('immutable');

const initialState = fromJS({
  list: responseState({
    data: [],
    extendSearch: {}
  }),
  actions: {
    account: responseState(),
    delete: responseState(),
    restore: responseState(),
    lock: responseState(),
    unlock: responseState(),
  }
});

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case constant.GET_USERS_REQUEST: {
      return state
        .setIn(['list', 'result'], ResultEnum.wait)
        .setIn(['list', 'skip'], action.skip)
        .setIn(['list', 'fragment'], action.fragment)
        .setIn(['list', 'reload'], action.reload)
        .setIn(['list', 'extendSearch'], action.extendSearch)
        .setIn(['list', 'code'], undefined);
    }
    case constant.GET_USERS_SUCCESS: {
      let _state = state
        .setIn(['list', 'result'], ResultEnum.success)
        .setIn(['list', 'skip'], action.skip)
        .setIn(['list', 'code'], action.code)
        .setIn(['list', 'count'], action.count);

      if (_state.getIn(['list', 'reload'])) {
        _state = _state.setIn(['list', 'data'], action.users);
      } else {
        _state = _state.updateIn(
          ['list', 'data'],
          (data) => {
            return data.concat(action.users);
          }
        );
      }

      return _state;
    }
    case constant.GET_USERS_FAILED: {
      if (state.getIn(['list', 'reload'])) {
        return state.set(
          'list',
          {
            result: ResultEnum.failed,
            code: action.code,
            data: [],
            extendSearch: {}
          }
        );
      }

      return state
        .setIn(['list', 'result'], ResultEnum.failed)
        .setIn(['list', 'code'], action.code);
    }
    case constant.VERIFY_ACCOUNT_REQUEST: {
      return state.setIn(
        ['actions', 'account'],
        { result: ResultEnum.wait }
      );
    }
    case constant.VERIFY_ACCOUNT_SUCCESS: {
      return state.setIn(
        ['actions', 'account'],
        {
          result: ResultEnum.success,
          code: action.code
        }
      );
    }
    case constant.VERIFY_ACCOUNT_FAILED: {
      return state.setIn(
        ['actions', 'account'],
        {
          result: ResultEnum.failed,
          code: action.code
        }
      );
    }
    case constant.DELETE_ACCOUNT_REQUEST: {
      return state.setIn(
        ['actions', 'delete'],
        { result: ResultEnum.wait }
      );
    }
    case constant.DELETE_ACCOUNT_SUCCESS: {
      return state.setIn(
        ['actions', 'delete'],
        {
          result: ResultEnum.success,
          code: action.code
        }
      );
    }
    case constant.DELETE_ACCOUNT_FAILED: {
      return state.setIn(
        ['actions', 'delete'],
        {
          result: ResultEnum.failed,
          code: action.code
        }
      );
    }
    case constant.RESTORE_ACCOUNT_REQUEST: {
      return state.setIn(
        ['actions', 'restore'],
        { result: ResultEnum.wait }
      );
    }
    case constant.RESTORE_ACCOUNT_SUCCESS: {
      return state.setIn(
        ['actions', 'restore'],
        {
          result: ResultEnum.success,
          code: action.code
        }
      );
    }
    case constant.RESTORE_ACCOUNT_FAILED: {
      return state.setIn(
        ['actions', 'restore'],
        {
          result: ResultEnum.failed,
          code: action.code
        }
      );
    }
    case constant.LOCK_ACCOUNT_REQUEST: {
      return state.setIn(
        ['actions', 'lock'],
        { result: ResultEnum.wait }
      );
    }
    case constant.LOCK_ACCOUNT_SUCCESS: {
      return state.setIn(
        ['actions', 'lock'],
        {
          result: ResultEnum.success,
          code: action.code
        }
      );
    }
    case constant.LOCK_ACCOUNT_FAILED: {
      return state.setIn(
        ['actions', 'lock'],
        {
          result: ResultEnum.failed,
          code: action.code
        }
      );
    }
    case constant.UNLOCK_ACCOUNT_REQUEST: {
      return state.setIn(
        ['actions', 'unlock'],
        { result: ResultEnum.wait }
      );
    }
    case constant.UNLOCK_ACCOUNT_SUCCESS: {
      return state.setIn(
        ['actions', 'unlock'],
        {
          result: ResultEnum.success,
          code: action.code
        }
      );
    }
    case constant.UNLOCK_ACCOUNT_FAILED: {
      return state.setIn(
        ['actions', 'unlock'],
        {
          result: ResultEnum.failed,
          code: action.code
        }
      );
    }
    case constant.RESERT_ADMIN_STORE: {
      if (action.fragment) {
        return state.set(
          action.fragment,
          initialState.get(action.fragment)
        );
      }

      return initialState;
    }
    default:
      return state;
  }
};
module.exports = reducer;
