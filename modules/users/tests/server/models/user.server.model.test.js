const path = require('path');
const mongoose = require('mongoose');
const chai = require('chai');
const regular = require(path.resolve('./commons/utils/regular-expression'));
const constant = require(path.resolve('./commons/utils/constant'));

describe('### User Server Model Tests', () => {
  let User = null;
  let user = null;

  before((done) => {
    User = mongoose.model('User');

    done();
  });

  beforeEach((done) => {
    user = {
      name: {
        first: 'first',
        last: 'last',
        display: 'last first',
        nick: 'user_user',
      },
      numberPhone: {
        code: '+84',
        number: '0000000000'
      },
      gender: constant.MALE,
      birthday: {
        day: 1,
        month: 1,
        year: 2000,
      },
      password: 'password',
      address: {
        country: 'country',
        city: 'city',
        county: 'county',
        local: 'local',
      },
      settings: {
        general: {
          language: constant.VI,
        },
      },
    };

    done();
  });

  afterEach((done) => {
    User.deleteMany({}, (err) => {
      chai.assert.notExists(err);
      done(err);
    });
  });

  it('should success to save', (done) => {
    const _user = new User(user);

    _user.save((err) => {
      chai.assert.notExists(err);
      done();
    });
  });

  describe('## User Names', () => {
    describe('# User First Names && Last Names', () => {
      it('should success to save with unicode firt="Trùng"', (done) => {
        const _user = new User(user);
        _user.name.first = 'Trùng';

        _user.save((err) => {
          chai.assert.notExists(err);
          done();
        });
      });

      it('should failed to save with first name user not vaild with nick="userruserruserruserruserruserrr" length > 30', (done) => {
        const _user = new User(user);
        _user.name.nick = 'userruserruserruserruserruserrr';

        _user.save((err) => {
          chai.assert.exists(err);
          done();
        });
      });

      it('should failed to save with first name user not vaild with first="_"', (done) => {
        const _user = new User(user);
        _user.name.first = '_';

        _user.save((err) => {
          chai.assert.exists(err);
          done();
        });
      });

      it('should failed to save with first name user not vaild with first=""', (done) => {
        const _user = new User(user);
        _user.name.first = '';

        _user.save((err) => {
          chai.assert.exists(err);
          done();
        });
      });

      it('should failed to save with first name user not vaild with first="a."', (done) => {
        const _user = new User(user);
        _user.name.first = 'a.';

        _user.save((err) => {
          chai.assert.exists(err);
          done();
        });
      });
    });

    describe('# User Nick Names', () => {
      it('should failed to save with nick name user unicode nick="Trùng"', (done) => {
        const _user = new User(user);
        _user.name.nick = 'Trùng';

        _user.save((err) => {
          chai.assert.exists(err);
          done();
        });
      });

      it('should failed to save with nick name user not vaild with nick="user" length < 5', (done) => {
        const _user = new User(user);
        _user.name.nick = 'user';

        _user.save((err) => {
          chai.assert.exists(err);
          done();
        });
      });

      it('should failed to save with nick name user not vaild with nick="userruserruserruserruserruserr_" length > 30', (done) => {
        const _user = new User(user);
        _user.name.nick = 'userruserruserruserruserruserr_';

        _user.save((err) => {
          chai.assert.exists(err);
          done();
        });
      });

      it('should failed to save with nick name user not vaild with nick=""', (done) => {
        const _user = new User(user);
        _user.name.nick = '';

        _user.save((err) => {
          chai.assert.exists(err);
          done();
        });
      });

      it('should failed to save with nick name user not vaild with nick="user.user"', (done) => {
        const _user = new User(user);
        _user.name.first = 'user.user';

        _user.save((err) => {
          chai.assert.exists(err);
          done();
        });
      });

      it('should failed to save with nick name user not vaild with nick="user user"', (done) => {
        const _user = new User(user);
        _user.name.first = 'user user';

        _user.save((err) => {
          chai.assert.exists(err);
          done();
        });
      });
    });
  });

  describe('## User Number Phone', () => {
    describe('# User Code Number Phone', () => {
      it('should failed to save with code number phone not vaild with code="1"', (done) => {
        const _user = new User(user);
        _user.numberPhone.code = '1';

        _user.save((err) => {
          chai.assert.exists(err);
          done();
        });
      });

      it('should failed to save with code number phone not vaild with code="+."', (done) => {
        const _user = new User(user);
        _user.numberPhone.code = '+.';

        _user.save((err) => {
          chai.assert.exists(err);
          done();
        });
      });

      it('should failed to save with code number phone not vaild with code="+a"', (done) => {
        const _user = new User(user);
        _user.numberPhone.code = '+a';

        _user.save((err) => {
          chai.assert.exists(err);
          done();
        });
      });

      it('should failed to save with code number phone not vaild with code="+000000000000000000000000000000" length > 30', (done) => {
        const _user = new User(user);
        _user.numberPhone.code = '+000000000000000000000000000000';

        _user.save((err) => {
          chai.assert.exists(err);
          done();
        });
      });
    });

    describe('# User Number Number Phone', () => {
      it('should failed to save with number number phone not vaild with number="."', (done) => {
        const _user = new User(user);
        _user.numberPhone.number = '.';

        _user.save((err) => {
          chai.assert.exists(err);
          done();
        });
      });

      it('should failed to save with number number phone not vaild with number="a"', (done) => {
        const _user = new User(user);
        _user.numberPhone.number = 'a';

        _user.save((err) => {
          chai.assert.exists(err);
          done();
        });
      });

      it('should failed to save with number number phone not vaild with number="0000000000000000000000000000000" length > 30', (done) => {
        const _user = new User(user);
        _user.numberPhone.number = '0000000000000000000000000000000';

        _user.save((err) => {
          chai.assert.exists(err);
          done();
        });
      });
    });
  });

  describe('## User Password', () => {
    it('should failed to save with password user unicode password="Trùng"', (done) => {
      const password = regular.user.password.test('Trùng');
      chai.assert.isFalse(password);
      done();
    });

    it('should failed to save with password user not vaild with password="pass" length < 5', (done) => {
      const password = regular.user.password.test('pass');
      chai.assert.isFalse(password);
      done();
    });

    it('should failed to save with password user not vaild with password="passwordpasswordpasswordpassword" length > 30', (done) => {
      const password = regular.user.password.test('passwordpasswordpasswordpassword');
      chai.assert.isFalse(password);
      done();
    });

    it('should failed to save with password user not vaild with password=""', (done) => {
      const password = regular.user.password.test('');
      chai.assert.isFalse(password);
      done();
    });

    it('should failed to save with password user not vaild with password="password.word"', (done) => {
      const password = regular.user.password.test('password.word');
      chai.assert.isFalse(password);
      done();
    });

    it('should failed to save with nick password user not vaild with password="pass word"', (done) => {
      const password = regular.user.password.test('password word');
      chai.assert.isFalse(password);
      done();
    });
  });
});
