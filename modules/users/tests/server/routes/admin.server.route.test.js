const path = require('path');
const mongoose = require('mongoose');
const request = require('supertest');
const _ = require('lodash');
const chai = require('chai');
const express = require(path.resolve('./config/lib/express'));
const usersUrl = require(path.resolve('./commons/modules/users/datas/url.data'));
const constant = require(path.resolve('./commons/utils/constant'));
const utils = require(path.resolve('./utils/test'));

describe('### User Server Routes Admin Tests', () => {
  let app = null;
  let agent = null;

  let ActivityHistory = null;
  let Notification = null;

  let User = null;
  let user = null;
  let someUser = null;
  let adminUser = null;

  const password = 'password';

  const expires = 15 * constant.ENUM.TIME.ONE_MINUTES;
  const reason = 'reason';

  /**
   * @overview Kiểm tra xác thực người dùng
   *
   * @param {ObjectId} userID
   * @param {Function} done
   * @param {Boolean} isVerify
   * @param {Boolean} isExist
   */
  const testVerifyAccount = (userID, done, isVerify = true, isExist = true) => {
    User
      .findById(userID)
      .exec((err, user) => {
        chai.assert.notExists(err);
        chai.assert.exists(user);

        if (isExist) {
          chai.assert.isTrue(user.verify.account.is === isVerify);
          chai.assert.deepEqual(user.verify.account.status, isVerify ? constant.SUCCESS : constant.FAILED);
        } else {
          chai.assert.isFalse(user.verify.account.is);
          chai.assert.deepEqual(user.verify.account.status, constant.NONE);
        }

        const result = isVerify ? constant.SUCCESS : constant.FAILED;
        const content = 'user.verify.account.is';

        ActivityHistory
          .findOne({
            userID,
            type: constant.USER,
          })
          .exec((err, activityHistory) => {
            chai.assert.notExists(err);

            if (isExist) {
              chai.assert.exists(activityHistory);
              chai.assert.deepEqual(activityHistory.result, result);
              chai.assert.deepEqual(activityHistory.content, content);
            } else {
              chai.assert.notExists(activityHistory);
            }

            Notification
              .findOne({
                receiveID: userID,
                type: constant.ADMIN,
              })
              .exec((err, notification) => {
                chai.assert.notExists(err);

                if (isExist) {
                  chai.assert.exists(notification);
                  chai.assert.deepEqual(notification.result, result);
                  chai.assert.deepEqual(notification.content, content);
                } else {
                  chai.assert.notExists(notification);
                }

                if (done) done();
              });
          });
      });
  };

  /**
   * @overview Kiểm tra khoá người dùng
   *
   * @param {ObjectId} userID
   * @param {Number} expires
   * @param {Function} done
   * @param {Boolean} isExist
   */
  const testLockAccount = (userID, reason, expires, done, isExist = true) => {
    User
      .findById(userID)
      .exec((err, user) => {
        chai.assert.notExists(err);
        chai.assert.exists(user);

        if (isExist) {
          chai.assert.isTrue(user.lock.is);
          chai.assert.isNumber(user.lock.expiresDate);
          chai.assert.deepEqual(user.lock.expiresDate, expires);
        } else {
          chai.assert.isFalse(user.lock.is);
          chai.assert.notExists(user.lock.expiresDate);
        }

        ActivityHistory
          .findOne({
            userID,
            type: constant.USER,
            result: constant.WARNING,
            content: 'user.lock.exist',
          })
          .exec((err, activityHistory) => {
            chai.assert.notExists(err);

            if (isExist) {
              chai.assert.exists(activityHistory);
            } else {
              chai.assert.notExists(activityHistory);
            }

            if (done) done();
          });
      });
  };

  /**
   * @overview Kiểm tra mở khoá người dùng
   *
   * @param {ObjectId} userID
   * @param {Number} expires
   * @param {Function} done
   * @param {Boolean} isExist
   */
  const testUnLockAccount = (userID, reason, expires, done, isExist = true) => {
    User
      .findById(userID)
      .exec((err, user) => {
        chai.assert.notExists(err);
        chai.assert.exists(user);

        if (isExist) {
          chai.assert.isFalse(user.lock.is);
          chai.assert.notExists(user.lock.expiresDate);
        } else {
          chai.assert.isTrue(user.lock.is);
          chai.assert.isNumber(user.lock.expiresDate);
          chai.assert.deepEqual(user.lock.expiresDate, expires);
        }

        ActivityHistory
          .findOne({
            userID,
            type: constant.USER,
            result: constant.WARNING,
            content: 'user.lock.exist',
          })
          .exec((err, activityHistory) => {
            chai.assert.notExists(err);
            chai.assert.exists(activityHistory);

            ActivityHistory
              .findOne({
                userID,
                type: constant.USER,
                result: constant.WARNING,
                content: 'user.unlock.exist',
              })
              .exec((err, activityHistory) => {
                chai.assert.notExists(err);

                if (isExist) {
                  chai.assert.exists(activityHistory);
                } else {
                  chai.assert.notExists(activityHistory);
                }

                if (done) done();
              });
          });
      });
  };

  /**
   * @overview Kiểm tra xoá người dùng
   *
   * @param {ObjectId} userID
   * @param {Function} done
   * @param {Boolean} isExist
   */
  const testDeleteAccount = (userID, done, isExist = true) => {
    User
      .findById(userID)
      .exec((err, user) => {
        chai.assert.notExists(err);
        chai.assert.exists(user);

        if (isExist) {
          chai.assert.isTrue(user.delete.is);
        } else {
          chai.assert.isFalse(user.delete.is);
          chai.assert.notExists(user.delete.date);
          chai.assert.notExists(user.delete.reason);
        }

        ActivityHistory
          .findOne({
            userID,
            type: constant.USER,
            result: constant.WARNING,
            content: 'user.delete.exist',
          })
          .exec((err, activityHistory) => {
            chai.assert.notExists(err);

            if (isExist) {
              chai.assert.exists(activityHistory);
            } else {
              chai.assert.notExists(activityHistory);
            }

            if (done) done();
          });
      });
  };

  /**
   * @overview Kiểm tra khôi phục người dùng
   *
   * @param {ObjectId} userID
   * @param {Function} done
   * @param {Boolean} isExist
   */
  const testRetoreAccount = (userID, done, isExist = true) => {
    User
      .findById(userID)
      .exec((err, user) => {
        chai.assert.notExists(err);
        chai.assert.exists(user);

        if (isExist) {
          chai.assert.isFalse(user.delete.is);
          chai.assert.notExists(user.delete.date);
          chai.assert.notExists(user.delete.reason);
        } else {
          chai.assert.isTrue(user.delete.is);
        }

        ActivityHistory
          .findOne({
            userID,
            type: constant.USER,
            result: constant.WARNING,
            content: 'user.delete.exist',
          })
          .exec((err, activityHistory) => {
            chai.assert.notExists(err);
            chai.assert.exists(activityHistory);

            ActivityHistory
              .findOne({
                userID,
                type: constant.USER,
                result: constant.WARNING,
                content: 'user.restore.is',
              })
              .exec((err, activityHistory) => {
                chai.assert.notExists(err);

                if (isExist) {
                  chai.assert.exists(activityHistory);
                } else {
                  chai.assert.notExists(activityHistory);
                }

                if (done) done();
              });
          });
      });
  };

  before((done) => {
    app = express.init(mongoose.connection.db);
    agent = request.agent(app);

    User = mongoose.model('User');
    ActivityHistory = mongoose.model('ActivityHistory');
    Notification = mongoose.model('Notification');

    done();
  });

  beforeEach((done) => {
    user = {
      name: {
        first: 'first',
        last: 'last',
        display: 'last first',
        nick: 'user_user',
      },
      numberPhone: {
        code: '+84',
        number: '0000000000',
      },
      gender: constant.MALE,
      birthday: {
        day: 1,
        month: 1,
        year: 2000,
      },
      password,
      address: {
        country: 'country',
        city: 'city',
        county: 'county',
        local: 'local',
      },
      settings: {
        general: {
          language: constant.VI,
        },
      },
    };

    adminUser = _.cloneDeep(user);
    adminUser.name.nick = constant.ADMIN;
    adminUser.numberPhone.number = '9999999999';
    adminUser.roles = { value: constant.ADMIN };

    someUser = _.cloneDeep(user);
    someUser.name.nick = 'some_user';
    someUser.numberPhone.number = '1111111111';

    user = new User(user);
    someUser = new User(someUser);
    adminUser = new User(adminUser);

    user.save((err) => {
      chai.assert.notExists(err);
      adminUser.save((err) => {
        chai.assert.notExists(err);
        someUser.save((err) => {
          chai.assert.notExists(err);
          done();
        });
      });
    });
  });

  afterEach((done) => {
    User.deleteMany({}, (err) => {
      chai.assert.notExists(err);
      ActivityHistory.deleteMany({}, (err) => {
        chai.assert.notExists(err);
        Notification.deleteMany({}, (err) => {
          chai.assert.notExists(err);
          done();
        });
      });
    });
  });

  describe('## Xác thực', () => {
    it('should success verify account with verifyAccount is true', (done) => {
      utils.signin(agent, adminUser.numberPhone, password)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.post(usersUrl.USERS_VERIFY_ID.replace(':id', user._id))
            .set('X-Requested-With', 'XMLHttpRequest')
            .send({
              verifyAccount: {
                is: true,
              },
            })
            .expect(200)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              testVerifyAccount(user._id, done);
            });
        });
    });

    it('should success verify account with verifyAccount is false', (done) => {
      utils.signin(agent, adminUser.numberPhone, password)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.post(usersUrl.USERS_VERIFY_ID.replace(':id', user._id))
            .set('X-Requested-With', 'XMLHttpRequest')
            .send({
              verifyAccount: {
                is: false,
              },
            })
            .expect(200)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              testVerifyAccount(user._id, done, false);
            });
        });
    });

    it('should failed verify account with not signin', (done) => {
      agent.post(usersUrl.USERS_VERIFY_ID.replace(':id', user._id))
        .set('X-Requested-With', 'XMLHttpRequest')
        .send({
          verifyAccount: {
            is: true,
          },
        })
        .expect(401)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          chai.assert.deepEqual(res.body.code, 'user.permission.notExist');
          testVerifyAccount(user._id, done, null, false);
        });
    });

    it('should failed verify account with bad body', (done) => {
      utils.signin(agent, adminUser.numberPhone, password)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.post(usersUrl.USERS_VERIFY_ID.replace(':id', user._id))
            .set('X-Requested-With', 'XMLHttpRequest')
            .send({
              verifyAccount: {},
            })
            .expect(400)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.code, 'data.notVaild');
              testVerifyAccount(user._id, done, null, false);
            });
        });
    });

    it('should failed verify account with bad user id', (done) => {
      utils.signin(agent, adminUser.numberPhone, password)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.post(usersUrl.USERS_VERIFY_ID.replace(':id', mongoose.Types.ObjectId()))
            .set('X-Requested-With', 'XMLHttpRequest')
            .send({
              verifyAccount: {
                is: false,
              },
            })
            .expect(404)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.code, 'user.notExist');
              testVerifyAccount(user._id, done, null, false);
            });
        });
    });

    it('should failed verify account with user not permission', (done) => {
      utils.signin(agent, someUser.numberPhone, password)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.post(usersUrl.USERS_VERIFY_ID.replace(':id', adminUser._id))
            .set('X-Requested-With', 'XMLHttpRequest')
            .send({
              verifyAccount: {
                is: true,
              },
            })
            .expect(401)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.code, 'user.permission.notExist');
              testVerifyAccount(user._id, done, null, false);
            });
        });
    });

    it('should failed verify account with user has verify account', (done) => {
      utils.signin(agent, adminUser.numberPhone, password)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.post(usersUrl.USERS_VERIFY_ID.replace(':id', user._id))
            .set('X-Requested-With', 'XMLHttpRequest')
            .send({
              verifyAccount: {
                is: true,
              },
            })
            .expect(200)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              testVerifyAccount(user._id, null);

              agent.post(usersUrl.USERS_VERIFY_ID.replace(':id', user._id))
                .set('X-Requested-With', 'XMLHttpRequest')
                .send({
                  verifyAccount: {
                    is: true,
                  },
                })
                .expect(400)
                .end((err, res) => {
                  if (err) {
                    return done(err);
                  }

                  chai.assert.deepEqual(res.body.code, 'user.verify.account.exist');
                  testVerifyAccount(user._id, done);
                });
            });
        });
    });
  });

  describe('## Khoá người dùng', () => {
    it('should success lock user', (done) => {
      utils.signin(agent, adminUser.numberPhone, password)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.post(usersUrl.USERS_LOCK_ID.replace(':id', user._id))
            .set('X-Requested-With', 'XMLHttpRequest')
            .send({
              lockAccount: {
                expires,
                reason,
              },
            })
            .expect(200)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              testLockAccount(user._id, [reason], expires, done);
            });
        });
    });

    it('should success lock user with exist lock', (done) => {
      utils.signin(agent, adminUser.numberPhone, password)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.post(usersUrl.USERS_LOCK_ID.replace(':id', user._id))
            .set('X-Requested-With', 'XMLHttpRequest')
            .send({
              lockAccount: {
                expires,
                reason,
              },
            })
            .expect(200)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              testLockAccount(user._id, [reason], expires, null);

              agent.post(usersUrl.USERS_LOCK_ID.replace(':id', user._id))
                .set('X-Requested-With', 'XMLHttpRequest')
                .send({
                  lockAccount: {
                    expires,
                    reason,
                  },
                })
                .expect(200)
                .end((err, res) => {
                  if (err) {
                    return done(err);
                  }

                  testLockAccount(user._id, [reason, reason], expires * 2, done);
                });
            });
        });
    });

    it('should failed lock user with not signin', (done) => {
      agent.post(usersUrl.USERS_LOCK_ID.replace(':id', user._id))
        .set('X-Requested-With', 'XMLHttpRequest')
        .send({
          lockAccount: {
            expires,
            reason,
          },
        })
        .expect(401)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          chai.assert.deepEqual(res.body.code, 'user.permission.notExist');
          testLockAccount(user._id, [reason], expires, done, false);
        });
    });

    it('should failed lock user with bad body', (done) => {
      utils.signin(agent, adminUser.numberPhone, password)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.post(usersUrl.USERS_LOCK_ID.replace(':id', user._id))
            .set('X-Requested-With', 'XMLHttpRequest')
            .send({
              lockAccount: {},
            })
            .expect(400)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.code, 'data.notVaild');
              testLockAccount(user._id, [reason], expires, done, false);
            });
        });
    });

    it('should failed lock user with bad id', (done) => {
      utils.signin(agent, adminUser.numberPhone, password)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.post(usersUrl.USERS_LOCK_ID.replace(':id', mongoose.Types.ObjectId()))
            .set('X-Requested-With', 'XMLHttpRequest')
            .send({
              lockAccount: {
                expires,
                reason,
              },
            })
            .expect(404)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.code, 'user.notExist');
              testLockAccount(user._id, [reason], expires, done, false);
            });
        });
    });

    it('should failed lock user with user not permission', (done) => {
      utils.signin(agent, someUser.numberPhone, password)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.post(usersUrl.USERS_LOCK_ID.replace(':id', adminUser._id))
            .set('X-Requested-With', 'XMLHttpRequest')
            .send({
              lockAccount: {
                expires,
                reason,
              },
            })
            .expect(401)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.code, 'user.permission.notExist');
              testLockAccount(user._id, [reason], expires, done, false);
            });
        });
    });
  });

  describe('## Mở khoá người dùng', () => {
    beforeEach((done) => {
      utils.signin(agent, adminUser.numberPhone, password)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.post(usersUrl.USERS_LOCK_ID.replace(':id', user._id))
            .set('X-Requested-With', 'XMLHttpRequest')
            .send({
              lockAccount: {
                expires,
                reason,
              },
            })
            .expect(200)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              testLockAccount(user._id, [reason], expires, done);
            });
        });
    });

    it('should success unlock user', (done) => {
      agent.put(usersUrl.USERS_LOCK_ID.replace(':id', user._id))
        .set('X-Requested-With', 'XMLHttpRequest')
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          testUnLockAccount(user._id, [reason], expires, done);
        });
    });

    it('should failed unlock user with exist unlock user', (done) => {
      agent.put(usersUrl.USERS_LOCK_ID.replace(':id', user._id))
        .set('X-Requested-With', 'XMLHttpRequest')
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          testUnLockAccount(user._id, [reason], expires, null);

          agent.put(usersUrl.USERS_LOCK_ID.replace(':id', user._id))
            .set('X-Requested-With', 'XMLHttpRequest')
            .expect(401)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.code, 'user.unlock.failed');
              testUnLockAccount(user._id, [reason], expires, done);
            });
        });
    });

    it('should failed unlock user with not signin', (done) => {
      agent.get(usersUrl.AUTH_SIGNOUT)
        .expect(200)
        .set('X-Requested-With', 'XMLHttpRequest')
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.put(usersUrl.USERS_LOCK_ID.replace(':id', user._id))
            .set('X-Requested-With', 'XMLHttpRequest')
            .expect(401)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.code, 'user.permission.notExist');
              testUnLockAccount(user._id, [reason], expires, done, false);
            });
        });
    });

    it('should failed unlock user with bad id', (done) => {
      agent.put(usersUrl.USERS_LOCK_ID.replace(':id', mongoose.Types.ObjectId()))
        .set('X-Requested-With', 'XMLHttpRequest')
        .expect(404)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          chai.assert.deepEqual(res.body.code, 'user.notExist');
          testUnLockAccount(user._id, [reason], expires, done, false);
        });
    });

    it('should failed unlock user with user not permission', (done) => {
      agent.get(usersUrl.AUTH_SIGNOUT)
        .expect(200)
        .set('X-Requested-With', 'XMLHttpRequest')
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          utils.signin(agent, someUser.numberPhone, password)
            .expect(200)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              agent.put(usersUrl.USERS_LOCK_ID.replace(':id', adminUser._id))
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(401)
                .end((err, res) => {
                  if (err) {
                    return done(err);
                  }

                  chai.assert.deepEqual(res.body.code, 'user.permission.notExist');
                  testUnLockAccount(user._id, [reason], expires, done, false);
                });
            });
        });
    });
  });

  describe('## Xoá người dùng', () => {
    it('should success delete user', (done) => {
      utils.signin(agent, adminUser.numberPhone, password)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.post(usersUrl.USERS_DELETE_ID.replace(':id', user._id))
            .set('X-Requested-With', 'XMLHttpRequest')
            .send({
              password: 'password',
              deleteAccount: {
                reason,
              },
            })
            .expect(200)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              testDeleteAccount(user._id, done);
            });
        });
    });

    it('should failed delete user with exist delete user', (done) => {
      utils.signin(agent, adminUser.numberPhone, password)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.post(usersUrl.USERS_DELETE_ID.replace(':id', user._id))
            .set('X-Requested-With', 'XMLHttpRequest')
            .send({
              password: 'password',
              deleteAccount: {
                reason,
              },
            })
            .expect(200)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              testDeleteAccount(user._id, null);

              agent.post(usersUrl.USERS_DELETE_ID.replace(':id', user._id))
                .set('X-Requested-With', 'XMLHttpRequest')
                .send({
                  password: 'password',
                  deleteAccount: {
                    reason,
                  },
                })
                .expect(401)
                .end((err, res) => {
                  if (err) {
                    return done(err);
                  }

                  chai.assert.deepEqual(res.body.code, 'user.delete.failed');
                  testDeleteAccount(user._id, done);
                });
            });
        });
    });

    it('should failed delete user with not signin', (done) => {
      agent.post(usersUrl.USERS_DELETE_ID.replace(':id', user._id))
        .set('X-Requested-With', 'XMLHttpRequest')
        .send({
          password: 'password',
          deleteAccount: {
            reason,
          },
        })
        .expect(401)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          chai.assert.deepEqual(res.body.code, 'user.permission.notExist');
          testDeleteAccount(user._id, done, false);
        });
    });

    it('should failed delete user with bad id', (done) => {
      utils.signin(agent, adminUser.numberPhone, password)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.post(usersUrl.USERS_DELETE_ID.replace(':id', mongoose.Types.ObjectId()))
            .set('X-Requested-With', 'XMLHttpRequest')
            .send({
              password: 'password',
              deleteAccount: {
                reason,
              },
            })
            .expect(404)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.code, 'user.notExist');
              testDeleteAccount(user._id, done, false);
            });
        });
    });

    it('should failed delete user with user not permission', (done) => {
      utils.signin(agent, someUser.numberPhone, password)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.post(usersUrl.USERS_DELETE_ID.replace(':id', adminUser._id))
            .set('X-Requested-With', 'XMLHttpRequest')
            .send({
              password: 'password',
              deleteAccount: {
                reason,
              },
            })
            .expect(401)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.code, 'user.permission.notExist');
              testDeleteAccount(user._id, done, false);
            });
        });
    });

    it('should failed delete user with user not authenticate', (done) => {
      utils.signin(agent, adminUser.numberPhone, password)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.post(usersUrl.USERS_DELETE_ID.replace(':id', user._id))
            .set('X-Requested-With', 'XMLHttpRequest')
            .send({
              password: 'somepassword',
              deleteAccount: {
                reason,
              },
            })
            .expect(401)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.code, 'user.signin.failed');
              testDeleteAccount(user._id, done, false);
            });
        });
    });
  });

  describe('## Khôi phục người dùng', () => {
    beforeEach((done) => {
      utils.signin(agent, adminUser.numberPhone, password)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.post(usersUrl.USERS_DELETE_ID.replace(':id', user._id))
            .set('X-Requested-With', 'XMLHttpRequest')
            .send({
              password: 'password',
              deleteAccount: {
                reason,
              },
            })
            .expect(200)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              testDeleteAccount(user._id, done);
            });
        });
    });

    it('should success restore user', (done) => {
      agent.put(usersUrl.USERS_DELETE_ID.replace(':id', user._id))
        .set('X-Requested-With', 'XMLHttpRequest')
        .send({
          password: 'password',
        })
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          testRetoreAccount(user._id, done);
        });
    });

    it('should failed restore user with exist restore user', (done) => {
      agent.put(usersUrl.USERS_DELETE_ID.replace(':id', user._id))
        .set('X-Requested-With', 'XMLHttpRequest')
        .send({
          password: 'password',
        })
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          testRetoreAccount(user._id, null);

          agent.put(usersUrl.USERS_DELETE_ID.replace(':id', user._id))
            .set('X-Requested-With', 'XMLHttpRequest')
            .send({
              password: 'password',
            })
            .expect(401)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.code, 'user.restore.failed');
              testRetoreAccount(user._id, done);
            });
        });
    });

    it('should failed restore user with not signin', (done) => {
      agent.get(usersUrl.AUTH_SIGNOUT)
        .expect(200)
        .set('X-Requested-With', 'XMLHttpRequest')
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.put(usersUrl.USERS_DELETE_ID.replace(':id', user._id))
            .set('X-Requested-With', 'XMLHttpRequest')
            .send({
              password: 'password',
            })
            .expect(401)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.code, 'user.permission.notExist');
              testRetoreAccount(user._id, done, false);
            });
        });
    });

    it('should failed restore user with user not authenticate', (done) => {
      agent.put(usersUrl.USERS_DELETE_ID.replace(':id', user._id))
        .set('X-Requested-With', 'XMLHttpRequest')
        .send({
          password: 'somepassword',
        })
        .expect(401)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          chai.assert.deepEqual(res.body.code, 'user.signin.failed');
          testRetoreAccount(user._id, done, false);
        });
    });

    it('should failed restore user with bad id', (done) => {
      agent.put(usersUrl.USERS_DELETE_ID.replace(':id', mongoose.Types.ObjectId()))
        .set('X-Requested-With', 'XMLHttpRequest')
        .send({
          password: 'password',
        })
        .expect(404)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          chai.assert.deepEqual(res.body.code, 'user.notExist');
          testRetoreAccount(user._id, done, false);
        });
    });

    it('should failed restore user with user not permission', (done) => {
      agent.get(usersUrl.AUTH_SIGNOUT)
        .expect(200)
        .set('X-Requested-With', 'XMLHttpRequest')
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          utils.signin(agent, someUser.numberPhone, password)
            .expect(200)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              agent.put(usersUrl.USERS_DELETE_ID.replace(':id', adminUser._id))
                .set('X-Requested-With', 'XMLHttpRequest')
                .send({
                  password: 'password',
                })
                .expect(401)
                .end((err, res) => {
                  if (err) {
                    return done(err);
                  }

                  chai.assert.deepEqual(res.body.code, 'user.permission.notExist');
                  testRetoreAccount(user._id, done, false);
                });
            });
        });
    });
  });

  describe('## Tìm kiếm người dùng', () => {
    it('should success extend search user', (done) => {
      utils.signin(agent, adminUser.numberPhone, password)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.get(usersUrl.USERS_ADMIN_SEARCH)
            .set('X-Requested-With', 'XMLHttpRequest')
            .query({
              type: constant.EXTEND,
              skip: 0,
              extendSearch: {
                roles: constant.USER,
                gender: constant.MALE,
                age: {
                  min: 18,
                  max: 20,
                },
                address: {
                  country: 'country',
                  city: 'city',
                  county: 'county',
                  local: 'local',
                },
                lock: false,
                delete: false,
                verify: {
                  account: {
                    is: false
                  },
                  numberPhone: false,
                },
                online: false,
              },
            })
            .expect(200)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.skip, 2);
              chai.assert.deepEqual(res.body.users.length, 2);

              return done();
            });
        });
    });

    it('should failed extend search user with not signin', (done) => {
      agent.get(usersUrl.USERS_ADMIN_SEARCH)
        .set('X-Requested-With', 'XMLHttpRequest')
        .query({
          type: constant.EXTEND,
          skip: 0,
          extendSearch: {
            roles: constant.USER,
          },
        })
        .expect(401)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          chai.assert.deepEqual(res.body.code, 'user.permission.notExist');
          return done();
        });
    });

    it('should failed extend search user with user not authenticate', (done) => {
      utils.signin(agent, someUser.numberPhone, password)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.get(usersUrl.USERS_ADMIN_SEARCH)
            .set('X-Requested-With', 'XMLHttpRequest')
            .query({
              type: constant.EXTEND,
              skip: 0,
              extendSearch: {
                roles: constant.USER,
              },
            })
            .expect(401)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.code, 'user.permission.notExist');
              return done();
            });
        });
    });

    it('should failed extend search user with extend search user with bad body', (done) => {
      utils.signin(agent, adminUser.numberPhone, password)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.get(usersUrl.USERS_ADMIN_SEARCH)
            .set('X-Requested-With', 'XMLHttpRequest')
            .query({
              type: constant.EXTEND,
              skip: 0,
              extendSearch: {},
            })
            .expect(400)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.code, 'data.notVaild');
              return done();
            });
        });
    });

    it('should failed extend search user with extend search user with bad query', (done) => {
      utils.signin(agent, adminUser.numberPhone, password)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.get(usersUrl.USERS_ADMIN_SEARCH)
            .set('X-Requested-With', 'XMLHttpRequest')
            .query({
              type: ' absolute',
              skip: 0,
              extendSearch: {
                roles: constant.USER,
              },
            })
            .expect(400)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.code, 'data.notVaild');
              return done();
            });
        });
    });
  });
});
