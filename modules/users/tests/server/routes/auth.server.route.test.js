const path = require('path');
const mongoose = require('mongoose');
const request = require('supertest');
const chai = require('chai');
const express = require(path.resolve('./config/lib/express'));
const usersUrl = require(path.resolve('./commons/modules/users/datas/url.data'));
const constant = require(path.resolve('./commons/utils/constant'));
const utils = require(path.resolve('./utils/test'));

describe('### User Server Routes Auth Tests', () => {
  let app = null;
  let agent = null;

  let User = null;
  let user = null;

  const password = 'password';

  let ActivityHistory = null;

  /**
   * @overview Kiểm tra quên mật khẩu
   *
   * @param {ObjectId} userID
   * @param {Function} done
   * @param {Boolean} isExist
   */
  const testForgotPassword = (userID, done, isExist = true) => {
    User
      .findById(userID)
      .exec((err, user) => {
        chai.assert.notExists(err);
        chai.assert.exists(user);

        if (isExist) {
          chai.assert.isString(user.tokens.security.password.reset.token);
          chai.assert.exists(user.tokens.security.password.reset.date);
        } else {
          chai.assert.notExists(user.tokens.security.password.reset.token);
          chai.assert.notExists(user.tokens.security.password.reset.date.expires);
          chai.assert.notExists(user.tokens.security.password.reset.date.confirm);
        }

        if (done) done();
      });
  };

  const forgot = (code, number) => {
    return agent.post(usersUrl.AUTH_FORGOT_PASSWORD)
      .set('X-Requested-With', 'XMLHttpRequest')
      .send({
        numberPhone: {
          code,
          number,
        },
      });
  };

  before((done) => {
    app = express.init(mongoose.connection.db);
    agent = request.agent(app);

    User = mongoose.model('User');
    ActivityHistory = mongoose.model('ActivityHistory');

    done();
  });

  beforeEach((done) => {
    user = new User({
      name: {
        first: 'first',
        last: 'last',
        display: 'last first',
        nick: 'user_user',
      },
      numberPhone: {
        code: '+84',
        number: '0000000000',
      },
      gender: constant.MALE,
      birthday: {
        day: 1,
        month: 1,
        year: 2000,
      },
      password,
      address: {
        country: 'country',
        city: 'city',
        county: 'county',
        local: 'local',
      },
      verify: {
        numberPhone: {
          is: true,
        },
      },
      settings: {
        general: {
          language: constant.VI,
        },
      },
    });
    user.save((err) => {
      chai.assert.notExists(err);
      done();
    });
  });

  afterEach((done) => {
    User.deleteMany({}, (err) => {
      chai.assert.notExists(err);
      done();
    });
  });

  describe('## Đăng ký người dùng', () => {
    it('should success signup', (done) => {
      agent.post(usersUrl.AUTH_SIGNUP)
        .set('X-Requested-With', 'XMLHttpRequest')
        .send({
          name: {
            first: 'user',
            last: 'some',
            nick: 'usersome',
          },
          numberPhone: {
            code: '+84',
            number: '0123456789',
          },
          gender: constant.MALE,
          birthday: {
            day: 1,
            month: 1,
            year: 2000,
          },
          password: 'somepassword',
          address: {
            country: 'country',
            city: 'city',
            county: 'county',
            local: 'local',
          },
          settings: {
            general: {
              language: constant.VI,
            },
          },
        })
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          User.countDocuments({}).exec((err, count) => {
            chai.assert.notExists(err);
            chai.assert.deepEqual(count, 2);

            return done();
          });
        });
    });

    it('should failed signup with user age restriction', (done) => {
      agent.post(usersUrl.AUTH_SIGNUP)
        .set('X-Requested-With', 'XMLHttpRequest')
        .send({
          name: {
            first: 'user',
            last: 'some',
            nick: 'usersome',
          },
          numberPhone: {
            code: '+84',
            number: '0123456789',
          },
          gender: constant.MALE,
          birthday: {
            day: 1,
            month: 1,
            year: new Date().getFullYear(),
          },
          password: 'somepassword',
          address: {
            country: 'country',
            city: 'city',
            county: 'county',
            local: 'local',
          },
          settings: {
            general: {
              language: constant.VI,
            },
          },
        })
        .expect(401)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          chai.assert.deepEqual(res.body.code, 'info.age.failed');
          User.countDocuments({}).exec((err, count) => {
            chai.assert.notExists(err);
            chai.assert.deepEqual(count, 1);

            return done();
          });
        });
    });

    it('should failed signup with user has signin', (done) => {
      utils.signin(agent, user.numberPhone, password)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.post(usersUrl.AUTH_SIGNUP)
            .set('X-Requested-With', 'XMLHttpRequest')
            .send({
              name: {
                first: 'user',
                last: 'some',
                nick: 'usersome',
              },
              numberPhone: {
                code: '+84',
                number: '0123456789',
              },
              gender: constant.MALE,
              birthday: {
                day: 1,
                month: 1,
                year: 2000,
              },
              password: 'somepassword',
              address: {
                country: 'country',
                city: 'city',
                county: 'county',
                local: 'local',
              },
              settings: {
                general: {
                  language: constant.VI,
                },
              },
            })
            .expect(401)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.code, 'user.permission.notExist');
              User.countDocuments({}).exec((err, count) => {
                chai.assert.notExists(err);
                chai.assert.deepEqual(count, 1);

                return done();
              });
            });
        });
    });

    it('should failed signup with nick match', (done) => {
      agent.post(usersUrl.AUTH_SIGNUP)
        .set('X-Requested-With', 'XMLHttpRequest')
        .send({
          name: {
            first: 'user',
            last: 'some',
            nick: 'user_user',
          },
          numberPhone: {
            code: '+84',
            number: '0123456789',
          },
          gender: constant.MALE,
          birthday: {
            day: 1,
            month: 1,
            year: 2000,
          },
          password: 'somepassword',
          address: {
            country: 'country',
            city: 'city',
            county: 'county',
            local: 'local',
          },
          settings: {
            general: {
              language: constant.VI,
            },
          },
        })
        .expect(400)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          chai.assert.deepEqual(res.body.code, 'user.name.nick.exist');
          User.countDocuments({}).exec((err, count) => {
            chai.assert.notExists(err);
            chai.assert.deepEqual(count, 1);

            return done();
          });
        });
    });

    it('should failed signup with number phone match', (done) => {
      agent.post(usersUrl.AUTH_SIGNUP)
        .set('X-Requested-With', 'XMLHttpRequest')
        .send({
          name: {
            first: 'user',
            last: 'some',
            nick: 'some_user',
          },
          numberPhone: {
            code: '+84',
            number: '0000000000',
          },
          gender: constant.MALE,
          birthday: {
            day: 1,
            month: 1,
            year: 2000,
          },
          password: 'somepassword',
          address: {
            country: 'country',
            city: 'city',
            county: 'county',
            local: 'local',
          },
          settings: {
            general: {
              language: constant.VI,
            },
          },
        })
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          chai.assert.deepEqual(res.body.code, 'user.numberPhone.exist');
          User.countDocuments({}).exec((err, count) => {
            chai.assert.notExists(err);
            chai.assert.deepEqual(count, 1);

            return done();
          });
        });
    });
  });

  describe('## Đăng nhập người dùng', () => {
    it('should success signin', (done) => {
      utils.signin(agent, user.numberPhone, password)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          return done();
        });
    });

    it('should success signin with expires lock', (done) => {
      User
        .findById(user._id)
        .exec((err, user) => {
          if (err) {
            return done(err);
          }

          user.lockAccount(mongoose.Types.ObjectId(), -5 * constant.ENUM.TIME.ONE_MINUTES);
          user.save((err) => {
            chai.assert.notExists(err);

            utils.signin(agent, user.numberPhone, password)
              .expect(200)
              .end((err, res) => {
                if (err) {
                  return done(err);
                }

                return done();
              });
          });
        });
    });

    it('should failed signin with user has signin', (done) => {
      utils.signin(agent, user.numberPhone, password)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          utils.signin(agent, { code: '+84', number: '3333333333' }, 'password_unlock')
            .expect(401)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.code, 'user.permission.notExist');
              return done();
            });
        });
    });

    it('should failed signin with bad number phone', (done) => {
      utils.signin(agent, { code: '+84', number: '0123456789' }, 'password')
        .expect(404)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          chai.assert.deepEqual(res.body.code, 'user.notExist');
          return done();
        });
    });

    it('should failed signin with bad password', (done) => {
      utils.signin(agent, { code: '+84', number: '0000000000' }, 'somepassword')
        .expect(404)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          chai.assert.deepEqual(res.body.code, 'user.signin.failed');
          return done();
        });
    });

    it('should failed signin with user has delete', (done) => {
      User
        .findById(user._id)
        .exec((err, user) => {
          if (err) {
            return done(err);
          }

          user.deleteAccount('reason');
          user.save((err) => {
            chai.assert.notExists(err);

            utils.signin(agent, user.numberPhone, password)
              .expect(404)
              .end((err, res) => {
                if (err) {
                  return done(err);
                }

                chai.assert.deepEqual(res.body.code, 'user.delete.exist');
                return done();
              });
          });
        });
    });

    it('should failed signin with user has lock', (done) => {
      User
        .findById(user._id)
        .exec((err, user) => {
          if (err) {
            return done(err);
          }

          user.lockAccount(mongoose.Types.ObjectId(), 5 * constant.ENUM.TIME.ONE_MINUTES);
          user.save((err) => {
            chai.assert.notExists(err);

            utils.signin(agent, user.numberPhone, password)
              .expect(401)
              .end((err, res) => {
                if (err) {
                  return done(err);
                }

                chai.assert.deepEqual(res.body.code, 'user.lock.exist');
                return done();
              });
          });
        });
    });

    it('should failed signin with user has online', (done) => {
      User
        .findById(user._id)
        .exec((err, user) => {
          if (err) {
            return done(err);
          }

          user.setOnline();
          user.save((err) => {
            chai.assert.notExists(err);

            utils.signin(agent, user.numberPhone, password)
              .expect(401)
              .end((err, res) => {
                if (err) {
                  return done(err);
                }

                chai.assert.deepEqual(res.body.code, 'user.online.exist');
                return done();
              });
          });
        });
    });
  });

  describe('## Quên mật khẩu người dùng', () => {
    it('should success forgot password', (done) => {
      forgot('+84', '0000000000')
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          testForgotPassword(user._id, done);
        });
    });

    it('should failed forgot password with user has signin', (done) => {
      utils.signin(agent, user.numberPhone, password)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.post(usersUrl.AUTH_FORGOT_PASSWORD)
            .set('X-Requested-With', 'XMLHttpRequest')
            .expect(401)
            .send({
              numberPhone: {
                code: '+84',
                number: '0123456789',
              },
            })
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.code, 'user.permission.notExist');
              testForgotPassword(user._id, done, false);
            });
        });
    });

    it('should failed forgot password with bad number phone', (done) => {
      agent.post(usersUrl.AUTH_FORGOT_PASSWORD)
        .set('X-Requested-With', 'XMLHttpRequest')
        .send({
          numberPhone: {
            code: '+84',
            number: '0123456789',
          },
        })
        .expect(404)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          chai.assert.deepEqual(res.body.code, 'user.notExist');
          testForgotPassword(user._id, done, false);
        });
    });

    it('should failed forgot password with bad body', (done) => {
      agent.post(usersUrl.AUTH_FORGOT_PASSWORD)
        .set('X-Requested-With', 'XMLHttpRequest')
        .send({
          numberPhone: {
            number: '0000000000',
          },
        })
        .expect(400)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          chai.assert.deepEqual(res.body.code, 'data.notVaild');
          testForgotPassword(user._id, done, false);
        });
    });

    it('should failed forgot password with token unexpired', (done) => {
      agent.post(usersUrl.AUTH_FORGOT_PASSWORD)
        .set('X-Requested-With', 'XMLHttpRequest')
        .send({
          numberPhone: {
            code: '+84',
            number: '0000000000',
          },
        })
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          testForgotPassword(user._id);

          agent.post(usersUrl.AUTH_FORGOT_PASSWORD)
            .set('X-Requested-With', 'XMLHttpRequest')
            .send({
              numberPhone: {
                code: '+84',
                number: '0000000000',
              },
            })
            .expect(400)
            .end((err, res) => {
              if (err) {
                done(err);
              }

              chai.assert.deepEqual(res.body.code, 'user.token.resetPassword.create.failed');
              testForgotPassword(user._id, done);
            });
        });
    });

    it('should failed forgot password with user not verify numberphone', (done) => {
      User
        .findById(user._id)
        .exec((err, user) => {
          chai.assert.notExists(err);
          chai.assert.exists(user);

          user.verify.numberPhone.is = false;
          user.save((err) => {
            chai.assert.notExists(err);

            forgot('+84', '0000000000')
              .expect(401)
              .end((err, res) => {
                if (err) {
                  return done(err);
                }

                chai.assert.deepEqual(res.body.code, 'user.permission.notExist');
                testForgotPassword(user._id, done, false);
              });
          });
        });
    });
  });

  describe('## Xác thực mã khôi phục mật khẩu người dùng', () => {
    beforeEach((done) => {
      forgot('+84', '0000000000')
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          User
            .findById(user._id)
            .exec((err, user) => {
              chai.assert.notExists(err);
              chai.assert.exists(user);

              chai.assert.isString(user.tokens.security.password.reset.token);
              chai.assert.exists(user.tokens.security.password.reset.date);

              return done();
            });
        });
    });

    it('should sucess validate reset password token', (done) => {
      User
        .findById(user._id)
        .exec((err, user) => {
          if (err) {
            return done(err);
          }

          const resetTokenPassword = user.tokens.security.password.reset.token;

          agent.post(usersUrl.AUTH_RESET)
            .set('X-Requested-With', 'XMLHttpRequest')
            .send({
              resetTokenPassword,
            })
            .expect(200)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              return done();
            });
        });
    });

    it('should failed validate reset password token with user has signin', (done) => {
      utils.signin(agent, user.numberPhone, password)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.post(usersUrl.AUTH_RESET)
            .set('X-Requested-With', 'XMLHttpRequest')
            .send({
              resetTokenPassword: 'token',
            })
            .expect(401)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.code, 'user.permission.notExist');
              return done();
            });
        });
    });

    it('should failed validate reset password token with bad token', (done) => {
      agent.post(usersUrl.AUTH_RESET)
        .set('X-Requested-With', 'XMLHttpRequest')
        .send({
          resetTokenPassword: 'token_orther',
        })
        .expect(401)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          chai.assert.deepEqual(res.body.code, 'user.token.resetPassword.notExist');
          return done();
        });
    });

    it('should failed validate reset password token with expires token', (done) => {
      User
        .findById(user._id)
        .exec((err, user) => {
          if (err) {
            return done(err);
          }

          user.tokens.security.password.reset.date.expires = 0;
          user.save((err) => {
            chai.assert.notExists(err);

            const resetTokenPassword = user.tokens.security.password.reset.token;

            agent.post(usersUrl.AUTH_RESET)
              .set('X-Requested-With', 'XMLHttpRequest')
              .send({
                resetTokenPassword,
              })
              .expect(400)
              .end((err, res) => {
                if (err) {
                  return done(err);
                }

                chai.assert.deepEqual(res.body.code, 'user.token.resetPassword.verify.failed');
                return done();
              });
          });
        });
    });
  });

  describe('## Khôi phục mật khẩu người dùng', () => {
    beforeEach((done) => {
      forgot('+84', '0000000000')
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          testForgotPassword(user._id, done);
        });
    });

    it('should success reset password', (done) => {
      User
        .findById(user._id)
        .exec((err, user) => {
          chai.assert.notExists(err);
          chai.assert.exists(user);

          const resetTokenPassword = user.tokens.security.password.reset.token;

          agent.put(usersUrl.AUTH_RESET)
            .set('X-Requested-With', 'XMLHttpRequest')
            .send({
              resetTokenPassword,
              passwordDetails: {
                verifyPassword: 'newPasswword',
                newPassword: 'newPasswword',
              },
            })
            .expect(200)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              testForgotPassword(user._id, null, false);

              ActivityHistory
                .findOne({
                  userID: user._id,
                  type: constant.USER,
                  result: constant.SUCCESS,
                  content: 'user.password.reset.is',
                })
                .exec((err, activityHistory) => {
                  chai.assert.notExists(err);
                  chai.assert.exists(activityHistory);

                  if (done) done();
                });
            });
        });
    });

    it('should failed reset password with bad password', (done) => {
      User
        .findById(user._id)
        .exec((err, user) => {
          chai.assert.notExists(err);
          chai.assert.exists(user);

          const resetTokenPassword = user.tokens.security.password.reset.token;

          agent.put(usersUrl.AUTH_RESET)
            .set('X-Requested-With', 'XMLHttpRequest')
            .send({
              resetTokenPassword,
              passwordDetails: {
                verifyPassword: 'oldPasswword',
                newPassword: 'newPasswword',
              },
            })
            .expect(401)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.code, 'user.permission.notExist');
              testForgotPassword(user._id);

              ActivityHistory
                .findOne({
                  userID: user._id,
                  type: constant.USER,
                  result: constant.SUCCESS,
                  content: 'user.password.reset.is',
                })
                .exec((err, activityHistory) => {
                  chai.assert.notExists(err);
                  chai.assert.notExists(activityHistory);

                  if (done) done();
                });
            });
        });
    });

    it('should failed reset password with user has signin', (done) => {
      utils.signin(agent, user.numberPhone, password)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.put(usersUrl.AUTH_RESET)
            .set('X-Requested-With', 'XMLHttpRequest')
            .send({
              resetTokenPassword: 'token',
              passwordDetails: {
                verifyPassword: 'newPasswword',
                newPassword: 'newPasswword',
              },
            })
            .expect(401)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.code, 'user.permission.notExist');
              testForgotPassword(user._id, null, true);

              ActivityHistory
                .findOne({
                  userID: user._id,
                  type: constant.USER,
                  result: constant.SUCCESS,
                  content: 'user.password.reset.is',
                })
                .exec((err, activityHistory) => {
                  chai.assert.notExists(err);
                  chai.assert.notExists(activityHistory);

                  if (done) done();
                });
            });
        });
    });

    it('should failed reset password with bad token', (done) => {
      agent.put(usersUrl.AUTH_RESET)
        .set('X-Requested-With', 'XMLHttpRequest')
        .send({
          resetTokenPassword: 'token_orther',
          passwordDetails: {
            verifyPassword: 'newPasswword',
            newPassword: 'newPasswword',
          },
        })
        .expect(400)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          chai.assert.deepEqual(res.body.code, 'user.token.resetPassword.notExist');
          testForgotPassword(user._id);

          ActivityHistory
            .findOne({
              userID: user._id,
              type: constant.USER,
              result: constant.SUCCESS,
              content: 'user.password.reset.is',
            })
            .exec((err, activityHistory) => {
              chai.assert.notExists(err);
              chai.assert.notExists(activityHistory);

              if (done) done();
            });
        });
    });

    it('should failed reset password with expires', (done) => {
      User
        .findById(user._id)
        .exec((err, user) => {
          chai.assert.notExists(err);
          chai.assert.exists(user);

          user.tokens.security.password.reset.date.expires = 0;
          user.save((err) => {
            chai.assert.notExists(err);

            const resetTokenPassword = user.tokens.security.password.reset.token;

            agent.put(usersUrl.AUTH_RESET)
              .set('X-Requested-With', 'XMLHttpRequest')
              .send({
                resetTokenPassword,
                passwordDetails: {
                  verifyPassword: 'newPasswword',
                  newPassword: 'newPasswword',
                },
              })
              .expect(400)
              .end((err, res) => {
                if (err) {
                  return done(err);
                }

                chai.assert.deepEqual(res.body.code, 'user.token.resetPassword.notExist');
                testForgotPassword(user._id);

                ActivityHistory
                  .findOne({
                    userID: user._id,
                    type: constant.USER,
                    result: constant.SUCCESS,
                    content: 'user.password.reset.is',
                  })
                  .exec((err, activityHistory) => {
                    chai.assert.notExists(err);
                    chai.assert.notExists(activityHistory);

                    if (done) done();
                  });
              });
          });
        });
    });
  });
});
