const path = require('path');
const mongoose = require('mongoose');
const request = require('supertest');
const chai = require('chai');
const cloudinary = require(path.resolve('config/lib/cloudinary'));
const express = require(path.resolve('./config/lib/express'));
const usersUrl = require(path.resolve('./commons/modules/users/datas/url.data'));
const constant = require(path.resolve('./commons/utils/constant'));
const utils = require(path.resolve('./utils/test'));

describe('### User Server Routes Users Tests', () => {
  let app = null;
  let agent = null;

  let User = null;
  let user = null;

  const password = 'password';

  let ActivityHistory = null;

  /**
   * @overview Kiểm tra thay đổi số điện thoại
   *
   * @param {ObjectId} userID
   * @param {Function} done
   * @param {Boolean} isExist
   */
  const testChangeNumberPhone = (userID, done, isExist = true) => {
    User
      .findById(userID)
      .exec((err, user) => {
        chai.assert.notExists(err);
        chai.assert.exists(user);

        if (isExist) {
          chai.assert.deepInclude(user.numberPhone, {
            code: '+84',
            number: '999999999',
          });
          chai.assert.isFalse(user.verify.numberPhone.is);
        } else {
          chai.assert.deepInclude(user.numberPhone, {
            code: '+84',
            number: '0000000000',
          });
        }

        ActivityHistory
          .findOne({
            userID,
            type: constant.USER,
            result: constant.SUCCESS,
            content: 'user.numberPhone.change.is',
          })
          .exec((err, activityHistory) => {
            chai.assert.notExists(err);

            if (isExist) {
              chai.assert.exists(activityHistory);
            } else {
              chai.assert.notExists(activityHistory);
            }

            if (done) done();
          });
      });
  };

  /**
   * @overview Kiểm tra thay đổi mật khẩu
   *
   * @param {ObjectId} userID
   * @param {Function} done
   * @param {Boolean} isExist
   */
  const testChangePassword = (userID, done, isExist = true) => {
    ActivityHistory
      .findOne({
        userID,
        type: constant.USER,
        result: constant.SUCCESS,
        content: 'user.password.change.is',
      })
      .exec((err, activityHistory) => {
        chai.assert.notExists(err);

        if (isExist) {
          chai.assert.exists(activityHistory);
        } else {
          chai.assert.notExists(activityHistory);
        }

        if (done) done();
      });
  };

  /**
 * @overview Kiểm tra thay đổi địa chỉ
 *
 * @param {ObjectId} userID
 * @param {Function} done
 * @param {Boolean} isExist
 */
  const testChangeAddress = (userID, done, isExist = true) => {
    User
      .findById(userID)
      .exec((err, user) => {
        chai.assert.notExists(err);
        chai.assert.exists(user);

        if (isExist) {
          chai.assert.deepInclude(user.address, {
            country: 'some_country',
            city: 'some_city',
            county: 'some_county',
            local: 'some_local',
          });
        } else {
          chai.assert.deepInclude(user.address, {
            country: 'country',
            city: 'city',
            county: 'county',
            local: 'local',
          });
        }

        ActivityHistory
          .findOne({
            userID,
            type: constant.USER,
            result: constant.SUCCESS,
            content: 'user.address.change.is',
          })
          .exec((err, activityHistory) => {
            chai.assert.notExists(err);

            if (isExist) {
              chai.assert.exists(activityHistory);
            } else {
              chai.assert.notExists(activityHistory);
            }

            if (done) done();
          });
      });
  };

  /**
   * @overview Kiểm tra thay đổi hình đại diện
   *
   * @param {ObjectId} userID
   * @param {Function} done
   * @param {Boolean} isExist
   */
  const testChangeAvatar = (userID, done, isExist = true) => {
    ActivityHistory
      .findOne({
        userID,
        type: constant.USER,
        result: constant.SUCCESS,
        content: 'user.avatar.change.is',
      })
      .exec((err, activityHistory) => {
        chai.assert.notExists(err);

        if (isExist) {
          chai.assert.exists(activityHistory);
        } else {
          chai.assert.notExists(activityHistory);
        }

        if (done) done();
      });
  };

  /**
   * @overview Kiểm tra lấy mã xác nhận số điện thoại
   *
   * @param {ObjectId} userID
   * @param {Function} done
   * @param {Boolean} isExist
   */
  const testGetVerifyTokenNumberPhone = (userID, done, isExist = true) => {
    User
      .findById(userID)
      .exec((err, user) => {
        chai.assert.notExists(err);
        chai.assert.exists(user);

        if (isExist) {
          chai.assert.isString(user.tokens.security.numberPhone.verify.token);
          chai.assert.exists(user.tokens.security.numberPhone.verify.date);
        } else {
          chai.assert.notExists(user.tokens.security.numberPhone.verify.token);
          chai.assert.notExists(user.tokens.security.numberPhone.verify.date.expires);
          chai.assert.notExists(user.tokens.security.numberPhone.verify.date.confirm);
        }

        if (done) done();
      });
  };

  /**
   * @overview Kiểm tra xác nhận số điện thoại
   *
   * @param {ObjectId} userID
   * @param {Function} done
   * @param {Boolean} isExist
   */
  const testVerifyTokenNumberPhone = (userID, done, isExist = true) => {
    User
      .findById(userID)
      .exec((err, user) => {
        chai.assert.notExists(err);
        chai.assert.exists(user);

        if (isExist) {
          chai.assert.isTrue(user.verify.numberPhone.is);
          chai.assert.isNull(user.tokens.security.numberPhone.verify.token);
          chai.assert.notExists(user.tokens.security.numberPhone.verify.date.expires);
          chai.assert.notExists(user.tokens.security.numberPhone.verify.date.confirm);
        } else {
          chai.assert.isFalse(user.verify.numberPhone.is);
          chai.assert.isString(user.tokens.security.numberPhone.verify.token);
          chai.assert.exists(user.tokens.security.numberPhone.verify.date);
        }

        ActivityHistory
          .findOne({
            userID,
            type: constant.USER,
            result: constant.SUCCESS,
            content: 'user.numberPhone.verify.is',
          })
          .exec((err, activityHistory) => {
            chai.assert.notExists(err);

            if (isExist) {
              chai.assert.exists(activityHistory);
            } else {
              chai.assert.notExists(activityHistory);
            }

            if (done) done();
          });
      });
  };

  /**
   * @overview Kiểm tra yêu cầu xác nhận người dùng
   *
   * @param {ObjectId} userID
   * @param {Function} done
   * @param {Boolean} isExist
   */
  const testRequestVerifyTokenAccount = (userID, done, isExist = true) => {
    User
      .findById(userID)
      .exec((err, user) => {
        chai.assert.notExists(err);
        chai.assert.exists(user);

        if (isExist) {
          chai.assert.isFalse(user.verify.account.is);
          chai.assert.deepEqual(user.verify.account.status, constant.WAIT);
          chai.assert.exists(user.verify.account.data);
        } else {
          chai.assert.isFalse(user.verify.account.is);
          chai.assert.deepEqual(user.verify.account.status, constant.NONE);
          chai.assert.notExists(user.verify.account.data.publicID);
          chai.assert.notExists(user.verify.account.data.url);
        }

        ActivityHistory
          .findOne({
            userID,
            type: constant.USER,
            result: constant.SUCCESS,
            content: 'user.verify.account.request.is',
          })
          .exec((err, activityHistory) => {
            chai.assert.notExists(err);

            if (isExist) {
              chai.assert.exists(activityHistory);
            } else {
              chai.assert.notExists(activityHistory);
            }

            if (done) done();
          });
      });
  };

  before((done) => {
    app = express.init(mongoose.connection.db);
    agent = request.agent(app);

    User = mongoose.model('User');
    ActivityHistory = mongoose.model('ActivityHistory');

    done();
  });

  after((done) => {
    cloudinary.deleteResources('test')
      .catch((err) => {
        chai.assert.notExists(err);
      });

    done();
  });

  beforeEach((done) => {
    user = new User({
      name: {
        first: 'first',
        last: 'last',
        display: 'last first',
        nick: 'user_user',
      },
      numberPhone: {
        code: '+84',
        number: '0000000000',
      },
      gender: constant.MALE,
      birthday: {
        day: 1,
        month: 1,
        year: 2000,
      },
      password,
      address: {
        country: 'country',
        city: 'city',
        county: 'county',
        local: 'local',
      },
      settings: {
        general: {
          language: constant.VI,
        },
      },
    });

    user.save((err) => {
      chai.assert.notExists(err);
      done();
    });
  });

  afterEach((done) => {
    User.deleteMany({}, (err) => {
      chai.assert.notExists(err);
      done();
    });
  });

  describe('## Thay đổi số điện thoại', () => {
    it('should success change number phone user', (done) => {
      utils.signin(agent, user.numberPhone, password)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.post(usersUrl.USERS_NUMBERPHONE)
            .set('X-Requested-With', 'XMLHttpRequest')
            .send({
              numberPhone: {
                code: '+84',
                number: '999999999',
              },
              password,
            })
            .expect(200)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              testChangeNumberPhone(user._id, done);
            });
        });
    });

    it('should failed change number phone user with not signin', (done) => {
      agent.post(usersUrl.USERS_NUMBERPHONE)
        .set('X-Requested-With', 'XMLHttpRequest')
        .send({
          numberPhone: {
            code: '+84',
            number: '999999999',
          },
          password,
        })
        .expect(401)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          chai.assert.deepEqual(res.body.code, 'user.permission.notExist');
          testChangeNumberPhone(user._id, done, false);
        });
    });

    it('should failed change number phone user with bad body', (done) => {
      utils.signin(agent, user.numberPhone, password)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.post(usersUrl.USERS_NUMBERPHONE)
            .set('X-Requested-With', 'XMLHttpRequest')
            .send({
              numberPhone: {},
              password: '',
            })
            .expect(400)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.code, 'data.notVaild');
              testChangeNumberPhone(user._id, done, false);
            });
        });
    });

    it('should failed change number phone user with user not authenticate', (done) => {
      utils.signin(agent, user.numberPhone, password)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.post(usersUrl.USERS_NUMBERPHONE)
            .set('X-Requested-With', 'XMLHttpRequest')
            .send({
              numberPhone: {
                code: '+84',
                number: '999999999',
              },
              password: 'somepassword',
            })
            .expect(401)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.code, 'user.signin.failed');
              testChangeNumberPhone(user._id, done, false);
            });
        });
    });
  });

  describe('## Lấy mã xác thực số điện thoại người dùng', () => {
    it('should success get verify number phone', (done) => {
      utils.signin(agent, user.numberPhone, password)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.get(usersUrl.USERS_NUMBERPHONE)
            .set('X-Requested-With', 'XMLHttpRequest')
            .expect(200)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              testGetVerifyTokenNumberPhone(user._id, done);
            });
        });
    });

    it('should failed get verify number phone with user not signin', (done) => {
      agent.get(usersUrl.USERS_NUMBERPHONE)
        .set('X-Requested-With', 'XMLHttpRequest')
        .expect(401)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          chai.assert.deepEqual(res.body.code, 'user.permission.notExist');
          testGetVerifyTokenNumberPhone(user._id, done, false);
        });
    });

    it('should failed get verify number phone with token unexpired', (done) => {
      utils.signin(agent, user.numberPhone, password)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.get(usersUrl.USERS_NUMBERPHONE)
            .set('X-Requested-With', 'XMLHttpRequest')
            .expect(200)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              testGetVerifyTokenNumberPhone(user._id);

              agent.get(usersUrl.USERS_NUMBERPHONE)
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(400)
                .end((err, res) => {
                  if (err) {
                    return done(err);
                  }

                  chai.assert.deepEqual(res.body.code, 'user.token.verifyNumberPhone.create.failed');
                  testGetVerifyTokenNumberPhone(user._id, done);
                });
            });
        });
    });
  });

  describe('## Xác thực số điện thoại người dùng', () => {
    beforeEach((done) => {
      utils.signin(agent, user.numberPhone, password)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.get(usersUrl.USERS_NUMBERPHONE)
            .set('X-Requested-With', 'XMLHttpRequest')
            .expect(200)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              testGetVerifyTokenNumberPhone(user._id, done);
            });
        });
    });

    it('should success verify number phone', (done) => {
      User
        .findById(user._id)
        .exec((err, user) => {
          chai.assert.notExists(err);
          chai.assert.exists(user);

          const verifyTokenNumberPhone = user.tokens.security.numberPhone.verify.token;

          agent.put(usersUrl.USERS_NUMBERPHONE)
            .set('X-Requested-With', 'XMLHttpRequest')
            .send({
              verifyTokenNumberPhone,
            })
            .expect(200)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.verifyNumberPhone.is, true);
              testVerifyTokenNumberPhone(user._id, done);
            });
        });
    });

    it('should failed verify number phone with user not signin', (done) => {
      agent.get(usersUrl.AUTH_SIGNOUT)
        .expect(200)
        .set('X-Requested-With', 'XMLHttpRequest')
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.put(usersUrl.USERS_NUMBERPHONE)
            .set('X-Requested-With', 'XMLHttpRequest')
            .expect(401)
            .send({
              verifyTokenNumberPhone: 'token',
            })
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.code, 'user.permission.notExist');
              testVerifyTokenNumberPhone(user._id, done, false);
            });
        });
    });

    it('should failed verify number phone with token expired', (done) => {
      User
        .findById(user._id)
        .exec((err, user) => {
          chai.assert.notExists(err);
          chai.assert.exists(user);

          user.tokens.security.numberPhone.verify.date.expires = 0;
          user.save((err) => {
            if (err) {
              done(err);
            }

            const verifyTokenNumberPhone = user.tokens.security.numberPhone.verify.token;

            agent.put(usersUrl.USERS_NUMBERPHONE)
              .set('X-Requested-With', 'XMLHttpRequest')
              .send({
                verifyTokenNumberPhone,
              })
              .expect(400)
              .end((err, res) => {
                if (err) {
                  return done(err);
                }

                chai.assert.deepEqual(res.body.code, 'user.token.verifyNumberPhone.notExist');
                testVerifyTokenNumberPhone(user._id, done, false);
              });
          });
        });
    });
  });

  describe('## Thay đôi mật khẩu người dùng', () => {
    it('should success change password user', (done) => {
      utils.signin(agent, user.numberPhone, password)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.post(usersUrl.USERS_PASSWORD)
            .set('X-Requested-With', 'XMLHttpRequest')
            .send({
              passwordDetails: {
                currentPassword: password,
                newPassword: 'newPassword',
                verifyPassword: 'newPassword',
              },
            })
            .expect(200)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              testChangePassword(user._id, done);
            });
        });
    });

    it('should failed change password user with not signin', (done) => {
      agent.post(usersUrl.USERS_PASSWORD)
        .set('X-Requested-With', 'XMLHttpRequest')
        .send({
          passwordDetails: {
            currentPassword: password,
            newPassword: 'newPassword',
            verifyPassword: 'newPassword',
          },
        })
        .expect(401)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          chai.assert.deepEqual(res.body.code, 'user.permission.notExist');
          testChangePassword(user._id, done, false);
        });
    });

    it('should failed change password user with bad body', (done) => {
      utils.signin(agent, user.numberPhone, password)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.post(usersUrl.USERS_PASSWORD)
            .set('X-Requested-With', 'XMLHttpRequest')
            .send({
              passwordDetails: {
                currentPassword: password,
                newPassword: 'newPassword',
              },
            })
            .expect(400)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.code, 'data.notVaild');
              testChangePassword(user._id, done, false);
            });
        });
    });

    it('should failed change password user with user not compare new password', (done) => {
      utils.signin(agent, user.numberPhone, password)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.post(usersUrl.USERS_PASSWORD)
            .set('X-Requested-With', 'XMLHttpRequest')
            .send({
              passwordDetails: {
                currentPassword: password,
                newPassword: 'newPassword',
                verifyPassword: 'some_password',
              },
            })
            .expect(400)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.code, 'data.notVaild');
              testChangePassword(user._id, done, false);
            });
        });
    });

    it('should failed change password user with user not authenticate', (done) => {
      utils.signin(agent, user.numberPhone, password)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.post(usersUrl.USERS_PASSWORD)
            .set('X-Requested-With', 'XMLHttpRequest')
            .send({
              passwordDetails: {
                currentPassword: 'somepassword',
                newPassword: 'newPassword',
                verifyPassword: 'newPassword',
              },
            })
            .expect(401)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.code, 'user.signin.failed');
              testChangePassword(user._id, done, false);
            });
        });
    });
  });

  describe('## Thay đôi địa chỉ người dùng', () => {
    it('should success change address user', (done) => {
      utils.signin(agent, user.numberPhone, password)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.post(usersUrl.USERS_ADDRESS)
            .set('X-Requested-With', 'XMLHttpRequest')
            .send({
              address: {
                country: 'some_country',
                city: 'some_city',
                county: 'some_county',
                local: 'some_local'
              }
            })
            .expect(200)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              testChangeAddress(user._id, done);
            });
        });
    });

    it('should failed change address user with not signin', (done) => {
      agent.post(usersUrl.USERS_ADDRESS)
        .set('X-Requested-With', 'XMLHttpRequest')
        .send({
          address: {
            country: 'some_country',
            city: 'some_city',
            county: 'some_county',
            local: 'some_local'
          }
        })
        .expect(401)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          chai.assert.deepEqual(res.body.code, 'user.permission.notExist');
          testChangeAddress(user._id, done, false);
        });
    });

    it('should failed change address user with bad body', (done) => {
      utils.signin(agent, user.numberPhone, password)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.post(usersUrl.USERS_ADDRESS)
            .set('X-Requested-With', 'XMLHttpRequest')
            .send({
              address: {
                city: 'some_city',
                county: 'some_county',
                local: 'some_local'
              }
            })
            .expect(400)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.code, 'data.notVaild');
              testChangeAddress(user._id, done, false);
            });
        });
    });
  });

  describe('## Thay đổi hình đại diện người dùng', () => {
    it('should success change avatar user', (done) => {
      utils.signin(agent, user.numberPhone, password)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.post(usersUrl.USERS_AVATAR)
            .set('X-Requested-With', 'XMLHttpRequest')
            .attach(constant.AVATAR, './modules/users/tests/server/images/avatar.jpg')
            .expect(200)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              testChangeAvatar(user._id, done);
            });
        });
    });

    it('should failed change avatar user with user not signin', (done) => {
      agent.post(usersUrl.USERS_AVATAR)
        .set('X-Requested-With', 'XMLHttpRequest')
        .attach(constant.AVATAR, './modules/users/tests/server/images/avatar.jpg')
        .expect(401)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          chai.assert.deepEqual(res.body.code, 'user.permission.notExist');
          testChangeAvatar(user._id, done, false);
        });
    });

    it('should failed change avatar user with user change avatar with bad body', (done) => {
      utils.signin(agent, user.numberPhone, password)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.post(usersUrl.USERS_AVATAR)
            .set('X-Requested-With', 'XMLHttpRequest')
            .attach('some_avatar', './modules/users/tests/server/images/avatar.jpg')
            .expect(400)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.code, 'data.notVaild');
              testChangeAvatar(user._id, done, false);
            });
        });
    });
  });

  describe('## Xoá hình đại diện người dùng', () => {
    it('should success delete avatar user', (done) => {
      utils.signin(agent, user.numberPhone, password)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.post(usersUrl.USERS_AVATAR)
            .set('X-Requested-With', 'XMLHttpRequest')
            .attach(constant.AVATAR, './modules/users/tests/server/images/avatar.jpg')
            .expect(200)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              agent.delete(usersUrl.USERS_AVATAR)
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(200)
                .end((err, res) => {
                  if (err) {
                    return done(err);
                  }

                  chai.assert.isString(res.body.urlAvatar);
                  return done();
                });
            });
        });
    });

    it('should failed delete avatar user with user not signin', (done) => {
      agent.delete(usersUrl.USERS_AVATAR)
        .set('X-Requested-With', 'XMLHttpRequest')
        .expect(401)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          chai.assert.deepEqual(res.body.code, 'user.permission.notExist');
          return done();
        });
    });

    it('should failed delete avatar user with not exist custom avatar', (done) => {
      utils.signin(agent, user.numberPhone, password)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.post(usersUrl.USERS_AVATAR)
            .set('X-Requested-With', 'XMLHttpRequest')
            .attach(constant.AVATAR, './modules/users/tests/server/images/avatar.jpg')
            .expect(200)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              agent.delete(usersUrl.USERS_AVATAR)
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(200)
                .end((err, res) => {
                  if (err) {
                    return done(err);
                  }

                  agent.delete(usersUrl.USERS_AVATAR)
                    .set('X-Requested-With', 'XMLHttpRequest')
                    .expect(400)
                    .end((err, res) => {
                      if (err) {
                        return done(err);
                      }

                      chai.assert.deepEqual(res.body.code, 'user.avatar.delete.failed');
                      return done();
                    });
                });
            });
        });
    });
  });

  describe('## Thay đổi cài đặt người dùng', () => {
    it('should success change settings user', (done) => {
      utils.signin(agent, user.numberPhone, password)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.post(usersUrl.USERS_SETTINGS)
            .set('X-Requested-With', 'XMLHttpRequest')
            .query({
              type: constant.GENERAL,
            })
            .send({
              settings: {
                language: constant.VI,
                notifications: {
                  sms: false,
                },
              },
            })
            .expect(200)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.isNotEmpty(res.body.settings);
              return done();
            });
        });
    });

    it('should failed change settings user with user not signin', (done) => {
      agent.post(usersUrl.USERS_SETTINGS)
        .set('X-Requested-With', 'XMLHttpRequest')
        .query({
          type: constant.GENERAL,
        })
        .send({
          settings: {
            language: constant.VI,
            notifications: {
              sms: false,
            },
          },
        })
        .expect(401)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          chai.assert.deepEqual(res.body.code, 'user.permission.notExist');
          return done();
        });
    });

    it('should failed change settings user with bad body', (done) => {
      utils.signin(agent, user.numberPhone, password)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.post(usersUrl.USERS_SETTINGS)
            .set('X-Requested-With', 'XMLHttpRequest')
            .query({
              type: 'some_type',
            })
            .send({
              settings: {
                language: constant.VI,
                notifications: {
                  sms: false,
                },
              },
            })
            .expect(400)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.code, 'data.notVaild');
              return done();
            });
        });
    });
  });

  describe('## Lấy cài đặt người dùng', () => {
    it('should success get settings user', (done) => {
      utils.signin(agent, user.numberPhone, password)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.get(usersUrl.USERS_SETTINGS)
            .set('X-Requested-With', 'XMLHttpRequest')
            .query({
              type: constant.FULL,
            })
            .expect(200)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.isNotEmpty(res.body.settings);
              return done();
            });
        });
    });

    it('should failed get settings user with user not signin', (done) => {
      agent.get(usersUrl.USERS_SETTINGS)
        .set('X-Requested-With', 'XMLHttpRequest')
        .query({
          type: constant.GENERAL,
        })
        .expect(401)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          chai.assert.deepEqual(res.body.code, 'user.permission.notExist');
          return done();
        });
    });

    it('should failed get settings user with bad body', (done) => {
      utils.signin(agent, user.numberPhone, password)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.get(usersUrl.USERS_SETTINGS)
            .set('X-Requested-With', 'XMLHttpRequest')
            .query({
              type: 'some_type',
            })
            .expect(400)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.code, 'data.notVaild');
              return done();
            });
        });
    });
  });

  describe('## Yêu cầu xác thực người dùng', () => {
    it('should success request verify account', (done) => {
      utils.signin(agent, user.numberPhone, password)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.post(usersUrl.USERS_VERIFY)
            .set('X-Requested-With', 'XMLHttpRequest')
            .attach(constant.VERIFY_ACCOUNT, './modules/users/tests/server/images/verify.jpg')
            .expect(200)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              testRequestVerifyTokenAccount(user._id, done);
            });
        });
    });

    it('should failed request verify account with user not signin', (done) => {
      agent.post(usersUrl.USERS_VERIFY)
        .set('X-Requested-With', 'XMLHttpRequest')
        .attach(constant.VERIFY_ACCOUNT, './modules/users/tests/server/images/verify.jpg')
        .expect(401)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          chai.assert.deepEqual(res.body.code, 'user.permission.notExist');
          testRequestVerifyTokenAccount(user._id, done, false);
        });
    });

    it('should failed request verify account with bad body', (done) => {
      utils.signin(agent, user.numberPhone, password)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.post(usersUrl.USERS_VERIFY)
            .set('X-Requested-With', 'XMLHttpRequest')
            .attach('some_verify', './modules/users/tests/server/images/verify.jpg')
            .expect(400)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.code, 'data.notVaild');
              testRequestVerifyTokenAccount(user._id, done, false);
            });
        });
    });

    it('should failed request verify account with exists request verify account', (done) => {
      utils.signin(agent, user.numberPhone, password)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.post(usersUrl.USERS_VERIFY)
            .set('X-Requested-With', 'XMLHttpRequest')
            .attach(constant.VERIFY_ACCOUNT, './modules/users/tests/server/images/verify.jpg')
            .expect(200)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              testRequestVerifyTokenAccount(user._id);

              agent.post(usersUrl.USERS_VERIFY)
                .set('X-Requested-With', 'XMLHttpRequest')
                .attach(constant.VERIFY_ACCOUNT, './modules/users/tests/server/images/verify.jpg')
                .expect(400)
                .end((err, res) => {
                  if (err) {
                    return done(err);
                  }

                  chai.assert.deepEqual(res.body.code, 'user.verify.account.request.exist');
                  testRequestVerifyTokenAccount(user._id, done);
                });
            });
        });
    });
  });

  describe('## Tìm kiếm người dùng', () => {
    it('should success absolute search user with number phone', (done) => {
      utils.signin(agent, user.numberPhone, password)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.get(usersUrl.USERS_SEARCH)
            .set('X-Requested-With', 'XMLHttpRequest')
            .query({
              type: constant.ABSOLUTE,
            })
            .send({
              absoluteSearch: {
                numberPhone: {
                  code: '+84',
                  number: '0000000000',
                },
              },
            })
            .expect(200)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.isNotEmpty(res.body.user);
              return done();
            });
        });
    });

    it('should success absolute search user with nick', (done) => {
      utils.signin(agent, user.numberPhone, password)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.get(usersUrl.USERS_SEARCH)
            .set('X-Requested-With', 'XMLHttpRequest')
            .query({
              type: constant.ABSOLUTE,
            })
            .send({
              absoluteSearch: {
                nickName: 'user_user',
              },
            })
            .expect(200)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.isNotEmpty(res.body.user);
              return done();
            });
        });
    });

    it('should failed absolute search user with not signin', (done) => {
      agent.get(usersUrl.USERS_SEARCH)
        .set('X-Requested-With', 'XMLHttpRequest')
        .query({
          type: constant.ABSOLUTE,
        })
        .send({
          absoluteSearch: {
            numberPhone: {
              code: '+84',
              number: '0000000000',
            },
          },
        })
        .expect(401)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          chai.assert.deepEqual(res.body.code, 'user.permission.notExist');
          return done();
        });
    });

    it('should failed absolute search user with bad body', (done) => {
      utils.signin(agent, user.numberPhone, password)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.get(usersUrl.USERS_SEARCH)
            .set('X-Requested-With', 'XMLHttpRequest')
            .query({
              type: constant.ABSOLUTE,
            })
            .send({
              absoluteSearch: {
                nickName: 'some_user',
              },
            })
            .expect(404)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.code, 'user.notExist');
              return done();
            });
        });
    });

    it('should failed absolute search user with absolute search user with bad query', (done) => {
      utils.signin(agent, user.numberPhone, password)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.get(usersUrl.USERS_SEARCH)
            .set('X-Requested-With', 'XMLHttpRequest')
            .query({
              type: ' extend',
            })
            .send({
              absoluteSearch: {
                numberPhone: {
                  code: '+84',
                  number: '0000000000',
                },
              },
            })
            .expect(400)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.code, 'data.notVaild');
              return done();
            });
        });
    });
  });

  describe('## Lấy thông tin của mình', () => {
    it('should success get user self', (done) => {
      utils.signin(agent, user.numberPhone, password)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.get(usersUrl.USERS)
            .set('X-Requested-With', 'XMLHttpRequest')
            .expect(200)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.exists(res.body.user);

              chai.assert.notExists(res.body.user.tokens);
              chai.assert.notExists(res.body.user.settings);
              chai.assert.notExists(res.body.user.password);
              chai.assert.notExists(res.body.user.salt);

              return done();
            });
        });
    });

    it('should failed get user self with not signin', (done) => {
      agent.get(usersUrl.USERS)
        .set('X-Requested-With', 'XMLHttpRequest')
        .expect(401)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          chai.assert.deepEqual(res.body.code, 'user.permission.notExist');
          return done();
        });
    });
  });

  describe('## Lấy thông tin người dùng', () => {
    it('should success get user with id', (done) => {
      utils.signin(agent, user.numberPhone, password)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.get(usersUrl.USERS_GET_ID.replace(':id', user._id))
            .set('X-Requested-With', 'XMLHttpRequest')
            .expect(200)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.exists(res.body.user);
              chai.assert.exists(res.body.user.lock);
              chai.assert.exists(res.body.user.delete);

              chai.assert.notExists(res.body.user.tokens);
              chai.assert.notExists(res.body.user.settings);
              chai.assert.notExists(res.body.user.password);
              chai.assert.notExists(res.body.user.salt);

              return done();
            });
        });
    });

    it('should success get user with get user with roles user', (done) => {
      utils.signin(agent, user.numberPhone, password)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          const newUser = new User({
            name: {
              first: 'first',
              last: 'last',
              display: 'last first',
              nick: 'user_newUser',
            },
            numberPhone: {
              code: '+84',
              number: '1111111111',
            },
            gender: constant.MALE,
            birthday: {
              day: 1,
              month: 1,
              year: 2000,
            },
            password,
            address: {
              country: 'country',
              city: 'city',
              county: 'county',
              local: 'local',
            },
            settings: {
              general: {
                language: constant.VI,
              },
            },
          });
          newUser.save((err) => {
            if (err) {
              done(err);
            }

            agent.get(usersUrl.USERS_GET_ID.replace(':id', newUser._id))
              .set('X-Requested-With', 'XMLHttpRequest')
              .expect(200)
              .end((err, res) => {
                if (err) {
                  return done(err);
                }

                chai.assert.exists(res.body.user);
                chai.assert.exists(res.body.user.numberPhone);
                chai.assert.exists(res.body.user.address.country);
                chai.assert.exists(res.body.user.address.city);
                chai.assert.exists(res.body.user.address.county);
                chai.assert.exists(res.body.user.address.local);
                chai.assert.exists(res.body.user.birthday.day);
                chai.assert.exists(res.body.user.birthday.month);
                chai.assert.exists(res.body.user.birthday.year);
                chai.assert.exists(res.body.user.lock);
                chai.assert.exists(res.body.user.delete);

                chai.assert.notExists(res.body.user.tokens);
                chai.assert.notExists(res.body.user.settings);
                chai.assert.notExists(res.body.user.password);
                chai.assert.notExists(res.body.user.salt);

                return done();
              });
          });
        });
    });

    it('should success get user with roles admin', (done) => {
      const newUserAdmin = new User({
        roles: {
          value: constant.ADMIN,
        },
        name: {
          first: 'first',
          last: 'last',
          display: 'last first',
          nick: 'user_newUserAdmin',
        },
        numberPhone: {
          code: '+84',
          number: '1111111111',
        },
        gender: constant.MALE,
        birthday: {
          day: 1,
          month: 1,
          year: 2000,
        },
        password,
        address: {
          country: 'country',
          city: 'city',
          county: 'county',
          local: 'local',
        },
        settings: {
          general: {
            language: constant.VI,
          },
        },
      });
      newUserAdmin.save((err) => {
        if (err) {
          done(err);
        }

        utils.signin(agent, newUserAdmin.numberPhone, password)
          .expect(200)
          .end((err, res) => {
            if (err) {
              return done(err);
            }

            agent.get(usersUrl.USERS_GET_ID.replace(':id', user._id))
              .set('X-Requested-With', 'XMLHttpRequest')
              .expect(200)
              .end((err, res) => {
                if (err) {
                  return done(err);
                }

                chai.assert.exists(res.body.user);
                chai.assert.exists(res.body.user.numberPhone);
                chai.assert.exists(res.body.user.address.country);
                chai.assert.exists(res.body.user.address.city);
                chai.assert.exists(res.body.user.address.county);
                chai.assert.exists(res.body.user.address.local);
                chai.assert.exists(res.body.user.birthday.day);
                chai.assert.exists(res.body.user.birthday.month);
                chai.assert.exists(res.body.user.birthday.year);
                chai.assert.exists(res.body.user.lock);
                chai.assert.exists(res.body.user.delete);
                chai.assert.exists(res.body.user.settings);
                // chai.assert.exists(res.body.user.tokens);

                chai.assert.notExists(res.body.user.password);
                chai.assert.notExists(res.body.user.salt);

                return done();
              });
          });
      });
    });

    it('should failed get user with not signin', (done) => {
      agent.get(usersUrl.USERS_GET_ID.replace(':id', user._id))
        .set('X-Requested-With', 'XMLHttpRequest')
        .expect(401)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          chai.assert.deepEqual(res.body.code, 'user.permission.notExist');
          return done();
        });
    });

    it('should failed get user with get user with bad id', (done) => {
      utils.signin(agent, user.numberPhone, password)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.get(usersUrl.USERS_GET_ID.replace(':id', mongoose.Types.ObjectId()))
            .set('X-Requested-With', 'XMLHttpRequest')
            .expect(404)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.code, 'user.notExist');
              return done();
            });
        });
    });
  });
});
