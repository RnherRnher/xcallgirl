const path = require('path');
const mongoose = require('mongoose');
const request = require('supertest');
const _ = require('lodash');
const chai = require('chai');
const express = require(path.resolve('./config/lib/express'));
const usersUrl = require(path.resolve('./commons/modules/users/datas/url.data'));
const constant = require(path.resolve('./commons/utils/constant'));
const utils = require(path.resolve('./utils/test'));

describe('### User Server Routes System Tests', () => {
  let app = null;
  let agent = null;

  let ActivityHistory = null;
  let Notification = null;

  let User = null;
  let user = null;
  let systemUser = null;

  const password = 'password';

  /**
   * @overview Kiểm tra vai trò người dùng
   *
   * @param {ObjectId} userID
   * @param {String} roles
   * @param {Function} done
   * @param {Boolean} isExist
   */
  const testChangeRolesAccount = (userID, roles, done, isExist = true) => {
    User
      .findById(userID)
      .exec((err, user) => {
        chai.assert.notExists(err);
        chai.assert.exists(user);

        if (isExist) {
          chai.assert.deepEqual(user.roles.value, roles);
        } else {
          chai.assert.notDeepEqual(user.roles.value, roles);
        }

        const result = constant.INFO;
        const content = 'user.roles.change.is';

        ActivityHistory
          .findOne({
            userID,
            type: constant.USER,
            result,
            content,
          })
          .exec((err, activityHistory) => {
            chai.assert.notExists(err);

            if (isExist) {
              chai.assert.exists(activityHistory);
            } else {
              chai.assert.notExists(activityHistory);
            }

            Notification
              .findOne({
                receiveID: userID,
                type: constant.ADMIN,
                result,
                content,
              })
              .exec((err, notification) => {
                chai.assert.notExists(err);
                if (isExist) {
                  chai.assert.exists(notification);
                } else {
                  chai.assert.notExists(notification);
                }

                if (done) done();
              });
          });
      });
  };

  before((done) => {
    app = express.init(mongoose.connection.db);
    agent = request.agent(app);

    User = mongoose.model('User');
    ActivityHistory = mongoose.model('ActivityHistory');
    Notification = mongoose.model('Notification');

    done();
  });

  beforeEach((done) => {
    user = {
      name: {
        first: 'first',
        last: 'last',
        display: 'last first',
        nick: 'user_user',
      },
      numberPhone: {
        code: '+84',
        number: '0000000000',
      },
      gender: constant.MALE,
      birthday: {
        day: 1,
        month: 1,
        year: 2000,
      },
      password,
      address: {
        country: 'country',
        city: 'city',
        county: 'county',
        local: 'local',
      },
      settings: {
        general: {
          language: constant.VI,
        },
      },
    };

    systemUser = _.cloneDeep(user);
    systemUser.name.nick = constant.SYSTEM;
    systemUser.numberPhone.number = '9999999999';
    systemUser.roles = { value: constant.SYSTEM };

    user = new User(user);
    systemUser = new User(systemUser);

    user.save((err) => {
      chai.assert.notExists(err);
      systemUser.save((err) => {
        chai.assert.notExists(err);
        done();
      });
    });
  });

  afterEach((done) => {
    User.deleteMany({}, (err) => {
      chai.assert.notExists(err);
      ActivityHistory.deleteMany({}, (err) => {
        chai.assert.notExists(err);
        Notification.deleteMany({}, (err) => {
          chai.assert.notExists(err);
          done();
        });
      });
    });
  });

  describe('## Thay đổi vai trò', () => {
    it('should success change roles account', (done) => {
      utils.signin(agent, systemUser.numberPhone, password)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          const roles = constant.ADMIN;

          agent.post(usersUrl.USERS_SYSTEM_ROLES_ID.replace(':id', user._id))
            .set('X-Requested-With', 'XMLHttpRequest')
            .send({
              roles,
              password,
            })
            .expect(200)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              testChangeRolesAccount(user._id, roles, done);
            });
        });
    });

    it('should success change roles account x 2', (done) => {
      utils.signin(agent, systemUser.numberPhone, password)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          const roles = constant.ADMIN;

          agent.post(usersUrl.USERS_SYSTEM_ROLES_ID.replace(':id', user._id))
            .set('X-Requested-With', 'XMLHttpRequest')
            .send({
              roles,
              password,
            })
            .expect(200)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              const roles = constant.USER;

              agent.post(usersUrl.USERS_SYSTEM_ROLES_ID.replace(':id', user._id))
                .set('X-Requested-With', 'XMLHttpRequest')
                .send({
                  roles,
                  password,
                })
                .expect(200)
                .end((err, res) => {
                  if (err) {
                    return done(err);
                  }

                  testChangeRolesAccount(user._id, roles, done);
                });
            });
        });
    });

    it('should failed change roles account with not signin', (done) => {
      const roles = constant.ADMIN;

      agent.post(usersUrl.USERS_SYSTEM_ROLES_ID.replace(':id', user._id))
        .set('X-Requested-With', 'XMLHttpRequest')
        .send({
          roles,
          password,
        })
        .expect(401)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          chai.assert.deepEqual(res.body.code, 'user.permission.notExist');
          testChangeRolesAccount(user._id, roles, done, false);
        });
    });

    it('should failed change roles account with bad body', (done) => {
      utils.signin(agent, systemUser.numberPhone, password)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          const roles = constant.ADMIN;

          agent.post(usersUrl.USERS_SYSTEM_ROLES_ID.replace(':id', user._id))
            .set('X-Requested-With', 'XMLHttpRequest')
            .send({})
            .expect(400)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.code, 'data.notVaild');
              testChangeRolesAccount(user._id, roles, done, false);
            });
        });
    });

    it('should failed change roles account with bad user id', (done) => {
      utils.signin(agent, systemUser.numberPhone, password)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          const roles = constant.ADMIN;

          agent.post(usersUrl.USERS_SYSTEM_ROLES_ID.replace(':id', mongoose.Types.ObjectId()))
            .set('X-Requested-With', 'XMLHttpRequest')
            .send({
              roles,
              password,
            })
            .expect(404)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.code, 'user.notExist');
              testChangeRolesAccount(user._id, roles, done, false);
            });
        });
    });

    it('should failed change roles account with same roles', (done) => {
      utils.signin(agent, systemUser.numberPhone, password)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          const roles = constant.ADMIN;

          agent.post(usersUrl.USERS_SYSTEM_ROLES_ID.replace(':id', user._id))
            .set('X-Requested-With', 'XMLHttpRequest')
            .send({
              roles,
              password,
            })
            .expect(200)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              agent.post(usersUrl.USERS_SYSTEM_ROLES_ID.replace(':id', user._id))
                .set('X-Requested-With', 'XMLHttpRequest')
                .send({
                  roles,
                  password,
                })
                .expect(400)
                .end((err, res) => {
                  if (err) {
                    return done(err);
                  }

                  chai.assert.deepEqual(res.body.code, 'user.roles.change.failed');
                  testChangeRolesAccount(user._id, roles, done);
                });
            });
        });
    });
  });
});
