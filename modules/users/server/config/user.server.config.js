const path = require('path');
const passport = require('passport');
const mongoose = require('mongoose');
const logger = require(path.resolve('./config/lib/logger'));
const constant = require(path.resolve('./commons/utils/constant'));
const { Strategy } = require('passport-local');

const User = mongoose.model('User');

/**
 * @overview Khởi tạo người dùng khi khởi chạy máy chủ
 *
 * @param {Express} app
 */
const inittUser = (app) => {
  User
    .find({}, `-${constant.SALT} -${constant.PASSWORD}`)
    .exec((err, users) => {
      users.forEach((user) => {
        user.setOffline();
        user.save((err) => {
          if (err) {
            logger.logError(
              'user.server.config',
              'inittUser',
              constant.USER,
              user,
              err
            );
          }
        });
      });
    });
};

/**
 * @overview Khởi tạo Pasport
 *
 * @param {Express} app
 */
const initPassport = (app) => {
  app.use(passport.initialize());
  app.use(passport.session());

  passport.serializeUser((user, done) => {
    done(null, user._id);
  });

  passport.deserializeUser((id, done) => {
    User
      .findById(id, `-${constant.SALT} -${constant.PASSWORD}`)
      .exec((err, user) => {
        done(err, user);
      });
  });

  passport.use(new Strategy({
    usernameField: constant.NUMBERPHONE,
    passwordField: constant.PASSWORD
  }, (numberPhone, password, done) => {
    User
      .findOne({ numberPhone: User.decodeNumberPhone(numberPhone) })
      .exec((err, user) => {
        if (err) {
          done(err);
        } else if (!user) {
          done(null, false, { message: 'user.notExist' });
        } else if (!user.authenticate(password)) {
          done(null, false, { message: 'user.signin.failed' });
        } else {
          done(null, user);
        }
      });
  }));
};

/**
 * @overview Khởi tạo xử lý đầu vào từ mỗi yêu cầu
 *
 * @param {Express} app
 */
const initHandleRequest = (app) => {
  app.use((req, res, next) => {
    const userSelf = req.user;

    // Kiểm tra phiên của người dùng
    if (userSelf) {
      let dataRes = null;

      if (userSelf.delete.is) {
        dataRes = {
          status: 404
        };
      } else if (userSelf.lock.is) {
        dataRes = {
          status: 401
        };
      }

      if (dataRes) {
        req.logout();
        req.session.destroy((err) => {
          if (err) {
            logger.logError(
              'user.server.config',
              'initHandleRequest',
              'session',
              req.session,
              err
            );
          }
        });

        return res
          .status(dataRes.status)
          .end();
      }
    }

    return next();
  });
};

/**
 * @overview Khới tạo
 *
 * @param {Express} app
 */
const init = (app) => {
  initPassport(app);
  inittUser(app);
  initHandleRequest(app);
};
module.exports = init;
