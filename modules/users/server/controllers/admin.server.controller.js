const path = require('path');
const async = require('async');
const _ = require('lodash');
const mongoose = require('mongoose');
const logger = require(path.resolve('./config/lib/logger'));
const constant = require(path.resolve('./commons/utils/constant'));
const { SOCKET_NOTIFICATION_PUSH } = require(path.resolve('./commons/modules/notifications/datas/socket.data'));
const { SOCKET_SELF_USER_UPDATE } = require(path.resolve('./commons/modules/users/datas/socket.data'));
const { createSocketData } = require(path.resolve('./commons/utils/middleware'));

const User = mongoose.model('User');
const Notification = mongoose.model('Notification');
const ActivityHistory = mongoose.model('ActivityHistory');

/**
 * @overview Tìm kiếm người dùng
 *
 * @param {Request} req
 * @param {Response} res
 * @param {Function} next
 *
 * @input query { type: String }
 * @output      { code: String, skip?: Number, users?: [{user}] }
 */
const search = (req, res, next) => {
  const queryReq = req.query;
  let dataRes = null;

  /**
 * Giải mả đầy đủ query tìm kiếm mở rộng người dùng
 *
 * @param   {Object} query
 * @returns {Object}
 */
  const decodeQueryExtendSearch = (query) => {
    const conditions = {};

    if (_.has(query, 'nick')) {
      // MongoError: text index required for $text query
      // conditions.$text = { $search: query.nick };
      conditions['name.nick'] = query.nick;
    }

    if (_.has(query, 'roles')) {
      conditions['roles.value'] = query.roles;
    }

    if (_.has(query, 'gender')) {
      conditions.gender = query.gender;
    }

    if (!_.isEmpty(query.age)) {
      const currentYear = new Date().getFullYear();

      if (_.has(query, 'age.max')) {
        conditions['birthday.year'] = { $gte: currentYear - query.age.max };
      }

      if (_.has(query, 'age.min')) {
        conditions['birthday.year'] = _.merge(
          conditions['birthday.year'],
          { $lte: currentYear - query.age.min }
        );
      }
    }

    if (!_.isEmpty(query.verify)) {
      if (_.has(query, 'verify.account')) {
        if (_.has(query, 'verify.account.is')) {
          conditions['verify.account.is'] = query.verify.account.is;
        }

        if (_.has(query, 'verify.account.status')) {
          conditions['verify.account.status'] = query.verify.account.status;
        }
      }

      if (_.has(query, 'verify.numberPhone')) {
        conditions['verify.numberPhone.is'] = query.verify.numberPhone;
      }
    }

    if (_.has(query, 'lock')) {
      conditions['lock.is'] = query.lock;
    }

    if (_.has(query, 'delete')) {
      conditions['delete.is'] = query.delete;
    }

    if (_.has(query, 'online')) {
      conditions['online.is'] = query.online;
    }

    // if (!_.isEmpty(query.initDate)) {
    //   if (_.has(query, 'initDate.max')) {
    //     conditions.initDate = { $gte: query.initDate.max };
    //   }

    //   if (_.has(query, 'initDate.min')) {
    //   conditions.initDate=  _.merge(
    //       conditions.initDate,
    //       { $lte: query.initDate.min }
    //     );
    //   }
    // }

    return _.isEmpty(conditions) ? null : conditions;
  };

  /**
 * @overview Tìm kiếm mở rộng người dùng
 *
 * @param {Request} req
 * @param {Response} res
 * @param {Function} next
 *
 * @input body
  {
    extendSearch: {
      nick ?: String,
      roles ?: String,
      gender ?: String,
      age ?: {
        min: Number,
        max: Number
      },
      address ?: {
        country: String,
        city: String,
        county: String
      },
      lock ?: Boolean,
      delete ?: Boolean,
      verify ?: {
        account: {
          is: Boolean,
          status: String
        },
        numberPhone: Boolean
      },
      online ?: Boolean
    }
  }
 * query    { skip: Number }
 * @output  { code: String, skip?: Number, users?: [User] }
 */
  const extendSearch = (req, res, next) => {
    const queryReq = req.query;
    const userSelf = req.user;
    let dataRes = null;

    const limit = req.app.locals.config.searchLimit;

    if (userSelf) {
      if (userSelf.getAuthorizedLevelByRoles() >= User.getAuthorizedLevelByRoles(constant.ADMIN)) {
        const { extendSearch } = queryReq;

        if (!_.isEmpty(extendSearch)) {
          const conditions = decodeQueryExtendSearch(extendSearch);
          const currentSkip = queryReq.skip > 0 ? queryReq.skip : 0;

          User
            .find(conditions)
            .skip(currentSkip)
            .limit(limit)
            .exec((err, users) => {
              if (err) {
                // Bad request.body
                dataRes = {
                  status: 400,
                  bodyRes: {
                    code: 'data.notVaild',
                  },
                };
              } else if (users.length) {
                dataRes = {
                  status: 200,
                  bodyRes: {
                    code: 'user.exist',
                    skip: currentSkip + (users.length < limit ? users.length : limit),
                    users: users.map((user) => {
                      return user.getShortcutsInfo();
                    }),
                  },
                };
              } else {
                // Người dùng không tồn tại
                dataRes = {
                  status: 404,
                  bodyRes: {
                    code: 'user.notExist',
                  },
                };
              }

              return res
                .status(dataRes.status)
                .send(dataRes.bodyRes)
                .end();
            });
        } else {
          // Bad request.body
          dataRes = {
            status: 400,
            bodyRes: {
              code: 'data.notVaild',
            },
          };

          return res
            .status(dataRes.status)
            .send(dataRes.bodyRes)
            .end();
        }
      } else {
        // Người dùng không có quyền
        dataRes = {
          status: 401,
          bodyRes: {
            code: 'user.permission.notExist',
          },
        };

        return res
          .status(dataRes.status)
          .send(dataRes.bodyRes)
          .end();
      }
    } else {
      // Chưa đăng nhập người dùng
      dataRes = {
        status: 401,
        bodyRes: {
          code: 'user.signin.notExist',
        },
      };

      return res
        .status(dataRes.status)
        .send(dataRes.bodyRes)
        .end();
    }
  };

  switch (queryReq.type) {
    case constant.EXTEND:
      extendSearch(req, res, next);
      break;
    default: {
      dataRes = {
        status: 400,
        bodyRes: {
          code: 'data.notVaild',
        },
      };

      return res
        .status(dataRes.status)
        .send(dataRes.bodyRes)
        .end();
    }
  }
};
module.exports.search = search;

/**
 * @overview Xác thực người dùng
 *
 * @param {Request} req
 * @param {Response} res
 * @param {Function} next
 *
 * @input body    { verifyAccount: { is: Boolean, reason?: String } }
 *        params  { id: String }
 * @output        { code: String, sockets?: [{Object}] }
 */
const verifyAccount = (req, res, next) => {
  const bodyReq = req.body;
  const paramsReq = req.params;
  const userSelf = req.user;
  let dataRes = null;

  async.waterfall([
    (done) => {
      /* Kiểm tra đầu vào */

      if (userSelf) {
        User
          .findById(paramsReq.id, `-${constant.SALT} -${constant.PASSWORD}`)
          .exec((err, user) => {
            let customError = null;

            if (err || !user) {
              // Người dùng không tồn tại
              dataRes = {
                status: 404,
                bodyRes: {
                  code: 'user.notExist',
                },
              };

              customError = new Error('Not user exists');
            } else if (userSelf.authorized(user.roles.value)) {
              dataRes = user;
            } else {
              // Người dùng không có quyền
              dataRes = {
                status: 401,
                bodyRes: {
                  code: 'user.permission.notExist',
                },
              };

              customError = new Error('Not permission');
            }

            done(customError, dataRes);
          });
      } else {
        // Chưa đăng nhập người dùng
        dataRes = {
          status: 401,
          bodyRes: {
            code: 'user.signin.notExist',
          },
        };

        done(new Error('User not signin'), dataRes);
      }
    }, (user, done) => {
      /* Xác thực người dùng */

      const isVerify = bodyReq.verifyAccount.is;
      const { reason } = bodyReq.verifyAccount;

      if (user.verifyAccount(userSelf._id, isVerify, reason)) {
        user.save((err) => {
          if (err) {
            // Xác thực người dùng thất bại
            dataRes = {
              status: 400,
              bodyRes: {
                code: 'user.verify.account.failed',
              },
            };
          } else {
            dataRes = user;
          }

          done(err, dataRes);
        });
      } else {
        // Người dùng đã xác thực
        dataRes = {
          status: 400,
          bodyRes: {
            code: 'user.verify.account.exist',
          },
        };

        done(new Error('Has user deleted'), dataRes);
      }
    }, (user, done) => {
      /* Tạo thông báo */

      const result = bodyReq.verifyAccount.is ? constant.SUCCESS : constant.FAILED;
      const content = 'user.verify.account.is';

      dataRes = {
        status: 200,
        bodyRes: {
          code: 'user.verify.account.success',
          sockets: [
            createSocketData(
              [SOCKET_SELF_USER_UPDATE],
              user._id,
              {}
            )
          ]
        },
      };

      const activityHistory = new ActivityHistory({
        userID: user._id,
        toURL: user._id,
        type: constant.USER,
        result,
        content,
      });

      activityHistory.save((err) => {
        if (err) {
          logger.logError(
            'admin.server.controller',
            'verifyAccount',
            constant.ACTIVITY_HISTORY,
            activityHistory,
            err
          );
        }
      });

      const notification = new Notification({
        receiveID: user._id,
        sendID: userSelf._id,
        toURL: user._id,
        type: constant.ADMIN,
        result,
        content,
      });

      notification.save((err) => {
        if (err) {
          logger.logError(
            'admin.server.controller',
            'verifyAccount',
            constant.NOTIFICATION,
            notification,
            err
          );
        } else {
          dataRes.bodyRes.sockets.push(
            createSocketData(
              [SOCKET_NOTIFICATION_PUSH],
              user._id,
              { notificationID: notification._id }
            )
          );
        }

        done(null, dataRes);
      });
    }], (err, dataRes) => {
      return res
        .status(dataRes.status)
        .send(dataRes.bodyRes)
        .end();
    });
};
module.exports.verifyAccount = verifyAccount;

/**
 * @overview Khoá người dùng
 *
 * @param {Request} req
 * @param {Response} res
 * @param {Function} next
 *
 * @input   body    { lockAccount: { expires: Number, reason?: String } }
 *          params  { id: String }
 * @output          { code: String }
 */
const lockAccount = (req, res, next) => {
  const bodyReq = req.body;
  const paramsReq = req.params;
  const userSelf = req.user;
  let dataRes = null;

  async.waterfall([
    (done) => {
      /* Kiểm tra đầu vào */

      if (userSelf) {
        User
          .findById(paramsReq.id, `-${constant.SALT} -${constant.PASSWORD}`)
          .exec((err, user) => {
            let customError = null;

            if (err || !user) {
              // Người dùng không tồn tại
              dataRes = {
                status: 404,
                bodyRes: {
                  code: 'user.notExist',
                },
              };

              customError = new Error('Not user exists');
            } else if (userSelf.authorized(user.roles.value)) {
              dataRes = user;
            } else {
              // Người dùng không có quyền
              dataRes = {
                status: 401,
                bodyRes: {
                  code: 'user.permission.notExist',
                },
              };

              customError = new Error('Not permission');
            }

            done(customError, dataRes);
          });
      } else {
        // Chưa đăng nhập người dùng
        dataRes = {
          status: 401,
          bodyRes: {
            code: 'user.signin.notExist',
          },
        };

        done(new Error('User not signin'), dataRes);
      }
    }, (user, done) => {
      /* Khoá người dùng */

      user.lockAccount(
        userSelf._id,
        bodyReq.lockAccount.expires,
        bodyReq.lockAccount.reason
      );
      user.save((err) => {
        if (err) {
          // Khoá người dùng thất bại
          dataRes = {
            status: 400,
            bodyRes: {
              code: 'user.lock.failed',
            },
          };
        } else {
          const activityHistory = new ActivityHistory({
            userID: user._id,
            toURL: user._id,
            type: constant.USER,
            result: constant.WARNING,
            content: 'user.lock.exist',
          });

          activityHistory.save((err) => {
            if (err) {
              logger.logError(
                'admin.server.controller',
                'lockAccount',
                constant.ACTIVITY_HISTORY,
                activityHistory,
                err
              );
            }
          });

          dataRes = {
            status: 200,
            bodyRes: {
              code: 'user.lock.success',
            },
          };
        }

        done(err, dataRes);
      });
    }], (err, dataRes) => {
      return res
        .status(dataRes.status)
        .send(dataRes.bodyRes)
        .end();
    });
};
module.exports.lockAccount = lockAccount;

/**
 * @overview Mở khoá người dùng
 *
 * @param {Request} req
 * @param {Response} res
 * @param {Function} next
 *
 * @input params  { id: String }
 * @output        { code: String }
 */
const unlockAccount = (req, res, next) => {
  const paramsReq = req.params;
  const userSelf = req.user;
  let dataRes = null;

  async.waterfall([
    (done) => {
      /* Kiểm tra quyền mở khoá người dùng */

      if (userSelf) {
        User
          .findById(paramsReq.id, `-${constant.SALT} -${constant.PASSWORD}`)
          .exec((err, user) => {
            let customError = null;

            if (err || !user) {
              // Người dùng không tồn tại
              dataRes = {
                status: 404,
                bodyRes: {
                  code: 'user.notExist',
                },
              };

              customError = new Error('Not user exists');
            } else if (userSelf.authorized(user.roles.value)) {
              if (!user.lock.is) {
                // Mở khoá người dùng thất bại
                dataRes = {
                  status: 401,
                  bodyRes: {
                    code: 'user.unlock.failed',
                  },
                };

                customError = new Error('Failed user unlock');
              } else {
                dataRes = user;
              }
            } else {
              // Người dùng không có quyền
              dataRes = {
                status: 401,
                bodyRes: {
                  code: 'user.permission.notExist',
                },
              };

              customError = new Error('Not permission');
            }

            done(customError, dataRes);
          });
      } else {
        // Chưa đăng nhập người dùng
        dataRes = {
          status: 401,
          bodyRes: {
            code: 'user.signin.notExist',
          },
        };

        done(new Error('User not signin'), dataRes);
      }
    }, (user, done) => {
      /* Mở khoá người dùng */

      user.unlockAccount();
      user.save((err) => {
        if (err) {
          // Mở khoá người dùng thất bại
          dataRes = {
            status: 400,
            bodyRes: {
              code: 'user.unlock.failed',
            },
          };
        } else {
          const activityHistory = new ActivityHistory({
            userID: user._id,
            toURL: user._id,
            type: constant.USER,
            result: constant.WARNING,
            content: 'user.unlock.exist',
          });

          activityHistory.save((err) => {
            if (err) {
              logger.logError(
                'admin.server.controller',
                'unlockAccount',
                constant.ACTIVITY_HISTORY,
                activityHistory,
                err
              );
            }
          });

          dataRes = {
            status: 200,
            bodyRes: {
              code: 'user.unlock.success',
            },
          };
        }

        done(err, dataRes);
      });
    }], (err, dataRes) => {
      return res
        .status(dataRes.status)
        .send(dataRes.bodyRes)
        .end();
    });
};
module.exports.unlockAccount = unlockAccount;

/**
 * @overview Xoá người dùng
 *
 * @param {Request} req
 * @param {Response} res
 * @param {Function} next
 *
 * @input body    { password: String, deleteAccount: { reason?: String } }
 *        params  { id: String }
 * @output        { code: String }
 */
const deleteAccount = (req, res, next) => {
  const bodyReq = req.body;
  const paramsReq = req.params;
  const userSelf = req.user;
  let dataRes = null;

  async.waterfall([
    (done) => {
      /* Kiểm tra đầu vào */

      if (userSelf) {
        User
          .findById(userSelf._id)
          .exec((err, user) => {
            let customError = null;
            if (err || !user) {
              // Chưa đăng nhập người dùng
              dataRes = {
                status: 401,
                bodyRes: {
                  code: 'user.signin.notExist',
                },
              };

              customError = new Error('User not signin');
            } else if (!user.authenticate(bodyReq.password)) {
              // Đăng nhập người dùng thất bại
              dataRes = {
                status: 401,
                bodyRes: {
                  code: 'user.signin.failed',
                },
              };

              customError = new Error('User failed signin');
            }

            if (customError) {
              done(customError, dataRes);
            } else {
              done(null);
            }
          });
      } else {
        // Chưa đăng nhập người dùng
        dataRes = {
          status: 401,
          bodyRes: {
            code: 'user.signin.notExist',
          },
        };

        done(new Error('User not signin'), dataRes);
      }
    }, (done) => {
      /* Kiểm tra quyền xoá người dùng */

      User
        .findById(paramsReq.id, `-${constant.SALT} -${constant.PASSWORD}`)
        .exec((err, user) => {
          let customError = null;

          if (err || !user) {
            // Người dùng không tồn tại
            dataRes = {
              status: 404,
              bodyRes: {
                code: 'user.notExist',
              },
            };

            customError = new Error('Not user exists');
          } else if (userSelf.authorized(user.roles.value)) {
            if (user.delete.is) {
              // Xoá người dùng thất bại
              dataRes = {
                status: 401,
                bodyRes: {
                  code: 'user.delete.failed',
                },
              };

              customError = new Error('Failed user delete');
            } else {
              dataRes = user;
            }
          } else {
            // Người dùng không có quyền
            dataRes = {
              status: 401,
              bodyRes: {
                code: 'user.permission.notExist',
              },
            };

            customError = new Error('Not permission');
          }

          done(customError, dataRes);
        });
    }, (user, done) => {
      /* Xoá người dùng */

      const { reason } = bodyReq.deleteAccount;
      user.deleteAccount(reason);
      user.save((err) => {
        if (err) {
          // Xoá người dùng thất bại
          dataRes = {
            status: 400,
            bodyRes: {
              code: 'user.delete.failed',
            },
          };
        } else {
          const activityHistory = new ActivityHistory({
            userID: user._id,
            toURL: user._id,
            type: constant.USER,
            result: constant.WARNING,
            content: 'user.delete.exist',
          });

          activityHistory.save((err) => {
            if (err) {
              logger.logError(
                'admin.server.controller',
                'deleteAccount',
                constant.ACTIVITY_HISTORY,
                activityHistory,
                err
              );
            }
          });

          dataRes = {
            status: 200,
            bodyRes: {
              code: 'user.delete.success',
            },
          };
        }

        done(err, dataRes);
      });
    }], (err, dataRes) => {
      return res
        .status(dataRes.status)
        .send(dataRes.bodyRes)
        .end();
    });
};
module.exports.deleteAccount = deleteAccount;

/**
 * @overview Khôi phục người dùng
 *
 * @param {Request} req
 * @param {Response} res
 * @param {Function} next
 *
 * @input body    { password: String }
 *        params  { id: String }
 * @output        { code: String }
 */
const restoreAccount = (req, res, next) => {
  const bodyReq = req.body;
  const paramsReq = req.params;
  const userSelf = req.user;
  let dataRes = null;

  async.waterfall([
    (done) => {
      /* Kiểm tra đầu vào */

      if (userSelf) {
        User
          .findById(userSelf._id)
          .exec((err, user) => {
            if (err || !user) {
              // Chưa đăng nhập người dùng
              dataRes = {
                status: 401,
                bodyRes: {
                  code: 'user.signin.notExist',
                },
              };

              done(err || new Error('User not signin'), dataRes);
            } else if (user.authenticate(bodyReq.password)) {
              done(null);
            } else {
              // Đăng nhập người dùng thất bại
              dataRes = {
                status: 401,
                bodyRes: {
                  code: 'user.signin.failed',
                },
              };

              done(new Error('User failed signin'), dataRes);
            }
          });
      } else {
        // Chưa đăng nhập người dùng
        dataRes = {
          status: 401,
          bodyRes: {
            code: 'user.signin.notExist',
          },
        };

        done(new Error('User not signin'), dataRes);
      }
    }, (done) => {
      /* Kiểm tra quyền mở khoá người dùng */

      User
        .findById(paramsReq.id, `-${constant.SALT} -${constant.PASSWORD}`)
        .exec((err, user) => {
          let customError = null;

          if (err || !user) {
            // Người dùng không tồn tại
            dataRes = {
              status: 404,
              bodyRes: {
                code: 'user.notExist',
              },
            };

            customError = new Error('Not user exists');
          } else if (userSelf.authorized(user.roles.value)) {
            if (!user.delete.is) {
              // Khôi phục người dùng thất bại
              dataRes = {
                status: 401,
                bodyRes: {
                  code: 'user.restore.failed',
                },
              };

              customError = new Error('Failed user retore');
            } else {
              dataRes = user;
            }
          } else {
            // Người dùng không có quyền
            dataRes = {
              status: 401,
              bodyRes: {
                code: 'user.permission.notExist',
              },
            };

            customError = new Error('Not permission');
          }

          done(customError, dataRes);
        });
    }, (user, done) => {
      /* Khôi phục người dùng */

      user.restoreAccount();
      user.save((err) => {
        if (err) {
          // Khôi phục người dùng thất bại
          dataRes = {
            status: 400,
            bodyRes: {
              code: 'user.restore.failed',
            },
          };
        } else {
          const activityHistory = new ActivityHistory({
            userID: user._id,
            toURL: user._id,
            type: constant.USER,
            result: constant.WARNING,
            content: 'user.restore.is',
          });

          activityHistory.save((err) => {
            if (err) {
              logger.logError(
                'admin.server.controller',
                'restoreAccount',
                constant.ACTIVITY_HISTORY,
                activityHistory,
                err
              );
            }
          });

          dataRes = {
            status: 200,
            bodyRes: {
              code: 'user.restore.success',
            },
          };
        }

        done(err, dataRes);
      });
    }], (err, dataRes) => {
      return res
        .status(dataRes.status)
        .send(dataRes.bodyRes)
        .end();
    });
};
module.exports.restoreAccount = restoreAccount;
