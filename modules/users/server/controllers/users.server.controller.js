const _ = require('lodash');
const authentication = require('./users/users.authentication.server.controller');
const profile = require('./users/users.profile.server.controller');
const security = require('./users/users.security.server.controller');
const action = require('./users/users.action.server.controller');

module.exports = _.assignIn(
  authentication,
  profile,
  security,
  action
);
