const path = require('path');
const async = require('async');
const mongoose = require('mongoose');
const logger = require(path.resolve('./config/lib/logger'));
const constant = require(path.resolve('./commons/utils/constant'));
const { SOCKET_NOTIFICATION_PUSH } = require(path.resolve('./commons/modules/notifications/datas/socket.data'));
const { SOCKET_SELF_USER_UPDATE } = require(path.resolve('./commons/modules/users/datas/socket.data'));
const { createSocketData } = require(path.resolve('./commons/utils/middleware'));

const User = mongoose.model('User');
const Notification = mongoose.model('Notification');
const ActivityHistory = mongoose.model('ActivityHistory');

/**
 * @overview Thay đổi vai trò người dùng
 *
 * @param {Request} req
 * @param {Response} res
 * @param {Function} next
 *
 * @input body    { roles: String, password: String }
 *        params  { id: String }
 * @output        { code: String, sockets?: [Object] }
 */
const changeRolesAccount = (req, res, next) => {
  const bodyReq = req.body;
  const paramsReq = req.params;
  const userSelf = req.user;
  let dataRes = null;

  async.waterfall([
    (done) => {
      /* Kiểm tra đầu vào */

      if (userSelf) {
        User
          .findById(userSelf._id)
          .exec((err, user) => {
            if (err || !user) {
              // Chưa đăng nhập người dùng
              dataRes = {
                status: 401,
                bodyRes: {
                  code: 'user.signin.notExist',
                },
              };

              done(err || new Error('User not signin'), dataRes);
            } else if (user.authenticate(bodyReq.password)) {
              done(null);
            } else {
              // Đăng nhập người dùng thất bại
              dataRes = {
                status: 401,
                bodyRes: {
                  code: 'user.signin.failed',
                },
              };

              done(new Error('User failed signin'), dataRes);
            }
          });
      } else {
        // Chưa đăng nhập người dùng
        dataRes = {
          status: 401,
          bodyRes: {
            code: 'user.signin.notExist',
          },
        };

        done(new Error('User not signin'), dataRes);
      }
    }, (done) => {
      /* Kiểm tra quyền thay đổi vai trò người dùng */

      User
        .findById(paramsReq.id, `-${constant.SALT} -${constant.PASSWORD}`)
        .exec((err, user) => {
          let customError = null;

          if (err || !user) {
            // Người dùng không tồn tại
            dataRes = {
              status: 404,
              bodyRes: {
                code: 'user.notExist',
              },
            };

            customError = new Error('Not user exists');
          } else if (
            userSelf.getAuthorizedLevelByRoles() === User.getAuthorizedLevelByRoles(constant.SYSTEM)
            && userSelf.authorized(user.roles.value)
          ) {
            dataRes = user;
          } else {
            // Người dùng không có quyền
            dataRes = {
              status: 401,
              bodyRes: {
                code: 'user.permission.notExist',
              },
            };

            customError = new Error('Not permission');
          }

          done(customError, dataRes);
        });
    }, (user, done) => {
      /* Cập nhật người dùng */

      if (user.setRoles(bodyReq.roles)) {
        user.save((err) => {
          if (err) {
            // Thiết lập vai trò người dùng thất bại
            dataRes = {
              status: 400,
              bodyRes: {
                code: 'user.roles.change.failed',
              },
            };
          }

          done(err, user);
        });
      } else {
        // Thiết lập vai trò người dùng thất bại
        dataRes = {
          status: 400,
          bodyRes: {
            code: 'user.roles.change.failed',
          },
        };

        done(new Error('User same roles'), dataRes);
      }
    }, (user, done) => {
      /* Tạo thông báo */

      dataRes = {
        status: 200,
        bodyRes: {
          code: 'user.roles.change.success',
          sockets: [
            createSocketData(
              [SOCKET_SELF_USER_UPDATE],
              user._id,
              {}
            )
          ]
        },
      };

      const result = constant.INFO;
      const content = 'user.roles.change.is';

      const activityHistory = new ActivityHistory({
        userID: user._id,
        toURL: user._id,
        type: constant.USER,
        result,
        content,
      });

      activityHistory.save((err) => {
        if (err) {
          logger.logError(
            'system.server.controller',
            'changeRoles',
            constant.ACTIVITY_HISTORY,
            activityHistory,
            err
          );
        }
      });

      const notification = new Notification({
        receiveID: user._id,
        sendID: userSelf._id,
        toURL: user._id,
        type: constant.ADMIN,
        result,
        content,
      });

      notification.save((err) => {
        if (err) {
          logger.logError(
            'system.server.controller',
            'changeRoles',
            constant.NOTIFICATION,
            notification,
            err
          );
        } else {
          dataRes.bodyRes.sockets.push(
            createSocketData(
              [SOCKET_NOTIFICATION_PUSH],
              user._id,
              { notificationID: notification._id }
            )
          );
        }

        done(null, dataRes);
      });
    }], (err, dataRes) => {
      return res
        .status(dataRes.status)
        .send(dataRes.bodyRes)
        .end();
    });
};
module.exports.changeRolesAccount = changeRolesAccount;
