const mongoose = require('mongoose');
const async = require('async');
const path = require('path');
const constant = require(path.resolve('./commons/utils/constant'));

const User = mongoose.model('User');

/**
 * @overview Lấy thông tin của mình
 *
 * @param {Request} req
 * @param {Response} res
 * @param {Function} next
 *
 * @input  { }
 * @output { code: String, user?: {User} }
 */
const getSelf = (req, res, next) => {
  const userSelf = req.user;
  let dataRes = null;

  if (userSelf) {
    userSelf
      .getFullInfo()
      .then((user) => {
        dataRes = {
          status: 200,
          bodyRes: {
            code: 'user.exist',
            user,
          },
        };

        return res
          .status(dataRes.status)
          .send(dataRes.bodyRes)
          .end();
      });
  } else {
    // Chưa đăng nhập người dùng
    dataRes = {
      status: 401,
      bodyRes: {
        code: 'user.signin.notExist',
      },
    };

    return res
      .status(dataRes.status)
      .send(dataRes.bodyRes)
      .end();
  }
};
module.exports.getSelf = getSelf;

/**
 * @overview Tìm kiếm người dùng
 *
 * @param {Request} req
 * @param {Response} res
 * @param {Function} next
 *
 * @input query { type: String }
 * @output      { code: String, user?: {User} }
 */
const search = (req, res, next) => {
  const queryReq = req.query;
  let dataRes = null;

  /**
   * @overview Tìm kiếm người dùng tuyêt đối
   *
   * @param {Request} req
   * @param {Response} res
   * @param {Function} next
   *
   * @input body
    {
      absoluteSearch: {
        numberPhone?: {
          code: String,
          number: String,
        },
        nickName?: String,
      }
    }
   * @output { code: String, user: {User} }
   */
  const absoluteSearch = (req, res, next) => {
    const bodyReq = req.body;
    const userSelf = req.user;
    let dataRes = null;

    if (userSelf) {
      User
        .findOne({
          $or: [{
            numberPhone: bodyReq.absoluteSearch.numberPhone,
            'delete.is': false,
          }, {
            'name.nick': bodyReq.absoluteSearch.nickName,
            'delete.is': false,
          }],
        }, `-${constant.SALT} -${constant.PASSWORD}`)
        .exec((err, user) => {
          if (err || !user) {
            // Người dùng không tồn tại
            dataRes = {
              status: 404,
              bodyRes: {
                code: 'user.notExist',
              },
            };
          } else {
            dataRes = {
              status: 200,
              bodyRes: {
                user: user.getShortcutsInfo(),
              },
            };
          }

          return res
            .status(dataRes.status)
            .send(dataRes.bodyRes)
            .end();
        });
    } else {
      // Chưa đăng nhập người dùng
      dataRes = {
        status: 401,
        bodyRes: {
          code: 'user.signin.notExist',
        },
      };

      return res
        .status(dataRes.status)
        .send(dataRes.bodyRes)
        .end();
    }
  };

  switch (queryReq.type) {
    case constant.ABSOLUTE:
      absoluteSearch(req, res, next);
      break;
    default: {
      dataRes = {
        status: 400,
        bodyRes: {
          code: 'data.notVaild',
        },
      };

      return res
        .status(dataRes.status)
        .send(dataRes.bodyRes)
        .end();
    }
  }
};
module.exports.search = search;

/**
 * @overview Lấy thông tin theo ID
 *
 * @param {Request} req
 * @param {Response} res
 * @param {Function} next
 *
 * @input params { id: String }
 * @output       { code: String, user?: {User} }
 */
const get = (req, res, next) => {
  const userSelf = req.user;
  const paramsReq = req.params;
  let dataRes = null;

  async.waterfall([
    (done) => {
      /* Kiểm tra đầu vào */

      if (userSelf) {
        User
          .findById(paramsReq.id, `-${constant.SALT} -${constant.PASSWORD}`)
          .exec((err, user) => {
            if (err || !user) {
              // Người dùng không tồn tại
              dataRes = {
                status: 404,
                bodyRes: {
                  code: 'user.notExist',
                },
              };

              done(err || new Error('Not user exists'), dataRes);
            } else if (user.lock.is && user.checkExpiresLockAccount()) {
              user.save((err) => {
                done(null, user);
              });
            } else {
              done(null, user);
            }
          });
      } else {
        // Chưa đăng nhập người dùng
        dataRes = {
          status: 401,
          bodyRes: {
            code: 'user.signin.notExist',
          },
        };

        done(new Error('User not signin'), dataRes);
      }
    }, (user, done) => {
      /* Lấy thông tin người dùng */

      user.getInfoByAuthorizedLevel(userSelf)
        .then((user) => {
          dataRes = {
            status: 200,
            bodyRes: {
              code: 'user.exist',
              user
            }
          };

          done(null, dataRes);
        });
    }], (err, dataRes) => {
      return res
        .status(dataRes.status)
        .send(dataRes.bodyRes)
        .end();
    });
};
module.exports.get = get;
