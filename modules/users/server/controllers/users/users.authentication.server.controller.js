const passport = require('passport');
const async = require('async');
const mongoose = require('mongoose');
const path = require('path');
const logger = require(path.resolve('./config/lib/logger'));
const constant = require(path.resolve('./commons/utils/constant'));

const User = mongoose.model('User');
const Credit = mongoose.model('Credit');
const ActivityHistory = mongoose.model('ActivityHistory');

/**
 * @overview Đăng ký
 *
 * @param {Request} req
 * @param {Response} res
 * @param {Function} next
 *
 * @input body
  {
    name: {
      first: String,
      last: String,
      nick: String,
    },
    numberPhone: {
      code: String,
      number: String
    },
    gender: String,
    birthday: {
      day: Number,
      month: Number,
      year: Number
    },
    address: {
      country: String,
      city?: String,
      county?: String,
      local?: String,
    },
    password: String,
    settings: {
      general: {
        language: String
      }
    }
  }
 * @output { code: String }
 */
const signup = (req, res, next) => {
  const bodyReq = req.body;
  const userSelf = req.user;
  let dataRes = null;

  async.waterfall([
    (done) => {
      /* Kiểm tra đầu vào */

      if (!userSelf) {
        const newUser = new User(User.getDefaultValue(bodyReq));

        if (newUser.testAgeRestriction()) {
          User
            .findOne({
              $or: [{
                numberPhone: newUser.numberPhone,
              }, {
                'name.nick': newUser.name.nick,
              }],
            })
            .exec((err, user) => {
              if (user) {
                if (user.equalsByNumberPhone(newUser.numberPhone)) {
                  // Số điện thoại người dùng đã tồn tại.
                  dataRes = {
                    status: 400,
                    bodyRes: {
                      code: 'user.numberPhone.exist',
                    },
                  };
                } else if (user.name.nick === newUser.name.nick) {
                  // Biệt danh người dùng đã tồn tại.
                  dataRes = {
                    status: 400,
                    bodyRes: {
                      code: 'user.name.nick.exist',
                    },
                  };
                }
              }

              if (dataRes) {
                done(new Error('Exist user'), dataRes);
              } else {
                done(null, newUser);
              }
            });
        } else {
          // Chưa đủ tuổi cho phép
          dataRes = {
            status: 401,
            bodyRes: {
              code: 'info.age.failed',
            },
          };

          done(new Error('User age restriction'), dataRes);
        }
      } else {
        // Người dùng đã đăng nhập
        dataRes = {
          status: 401,
          bodyRes: {
            code: 'user.signin.exist',
          },
        };

        done(new Error('User has signin'), dataRes);
      }
    }, (user, done) => {
      /* Tạo người dùng và thẻ tín dụng */

      user.save((err) => {
        if (err) {
          // Tạo người dùng thất bại
          dataRes = {
            status: 400,
            bodyRes: {
              code: 'user.signup.failed',
            },
          };
        } else {
          const credit = new Credit();
          credit.userID = user._id;

          credit.save((err) => {
            if (err) {
              logger.logError(
                'users.authentication.server.controller',
                'signup',
                constant.CREDIT,
                credit,
                err
              );
            }
          });

          dataRes = user;
        }

        done(err, dataRes);
      });
    }, (user, done) => {
      /* Đăng nhập người dùng */

      // Xoá các dự liệu nhạy cảm
      user.deleteSensitiveData();
      // Đăng nhập để hoàn thành đăng ký người dùng
      req.login(user, (err) => {
        if (err) {
          // Đăng nhập thất bại
          dataRes = {
            status: 401,
            bodyRes: {
              code: 'user.signin.failed',
            },
          };
        } else {
          const activityHistory = new ActivityHistory({
            userID: user._id,
            toURL: user._id,
            type: constant.USER,
            result: constant.SUCCESS,
            content: 'user.signup.is',
          });

          activityHistory.save((err) => {
            if (err) {
              logger.logError(
                'users.authentication.server.controller',
                'signup',
                constant.ACTIVITY_HISTORY,
                user,
                err
              );
            }
          });

          dataRes = {
            status: 200,
            bodyRes: {
              code: 'user.signup.success',
            },
          };
        }

        done(err, dataRes);
      });
    },
  ], (err, dataRes) => {
    return res
      .status(dataRes.status)
      .send(dataRes.bodyRes)
      .end();
  });
};
module.exports.signup = signup;

/**
 * @overview Đăng nhập
 *
 * @param {Request} req
 * @param {Response} res
 * @param {Function} next
 *
 * @input body  { numberPhone: String, password: String}
 * @output      { code: String }
 *
 */
const signin = (req, res, next) => {
  const userSelf = req.user;
  let dataRes = null;

  async.waterfall([
    (done) => {
      /* Kiểm tra đầu vào */

      if (!userSelf) {
        passport.authenticate('local', (err, user, info) => {
          let isUpdateUser = false;

          if (err) {
            // Bad request.body
            dataRes = {
              status: 400,
              bodyRes: {
                code: 'data.notVaild',
              },
            };
          } else if (!user) {
            // Người dùng không tồn tại
            dataRes = {
              status: 404,
              bodyRes: {
                code: info.message,
              },
            };
          } else if (user.online.is) {
            // Người dùng đang trực tuyến
            dataRes = {
              status: 401,
              bodyRes: {
                code: 'user.online.exist',
              },
            };
          } else if (user.delete.is) {
            // Người dùng đã bị xoá
            dataRes = {
              status: 404,
              bodyRes: {
                code: 'user.delete.exist',
              },
            };
          } else if (user.lock.is) {
            if (user.checkExpiresLockAccount()) {
              isUpdateUser = true;
            } else {
              // Người dùng đã bị khoá
              dataRes = {
                status: 401,
                bodyRes: {
                  code: 'user.lock.exist',
                },
              };
            }
          }

          if (dataRes) {
            done(new Error(dataRes.bodyRes.code), dataRes);
          } else {
            done(null, user, isUpdateUser);
          }
        })(req, res, next);
      } else {
        // Đã đăng nhập người dùng
        dataRes = {
          status: 401,
          bodyRes: {
            code: 'user.signin.exist',
          },
        };

        done(new Error('User signin'), dataRes);
      }
    }, (user, isUpdateUser, done) => {
      /* Cập nhật người dùng */

      if (isUpdateUser) {
        user.save((err) => {
          if (err) {
            // Đăng nhập người dùng thất bại
            dataRes = {
              status: 400,
              bodyRes: {
                code: 'user.signin.failed',
              },
            };
          }

          done(err, user);
        });
      } else {
        done(null, user);
      }
    }, (user, done) => {
      /* Hoàn thành đăng nhập */

      // Xoá các dữ liệu nhạy cảm người dùng
      user.deleteSensitiveData();
      // Đăng nhập người dùng
      req.login(user, (err) => {
        if (err) {
          // Đăng nhập người dùng thất bại
          dataRes = {
            status: 400,
            bodyRes: {
              code: 'user.signin.failed',
            },
          };
        } else {
          dataRes = {
            status: 200,
            bodyRes: {
              code: 'user.signin.success',
            },
          };
        }

        done(err, dataRes);
      });
    }], (err, dataRes) => {
      return res
        .status(dataRes.status)
        .send(dataRes.bodyRes)
        .end();
    });
};
module.exports.signin = signin;

/**
 * @overview Đăng xuất
 *
 * @param {Request} req
 * @param {Response} res
 * @param {Function} next
 *
 * @input  { }
 * @output { code: String }
 */
const signout = (req, res, next) => {
  req.logout();
  req.session.destroy((err) => {
    if (err) {
      logger.logError(
        'users.authentication.server.controller',
        'signout',
        'session',
        req.session,
        err
      );
    }
  });

  const dataRes = {
    status: 200,
    bodyRes: {
      code: 'user.sigout.success',
    },
  };

  return res
    .status(dataRes.status)
    .send(dataRes.bodyRes)
    .end();
};
module.exports.signout = signout;
