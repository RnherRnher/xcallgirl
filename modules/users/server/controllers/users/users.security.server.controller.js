const async = require('async');
const mongoose = require('mongoose');
const path = require('path');
const twilio = require(path.resolve('config/lib/twilio'));
const cloudinary = require(path.resolve('config/lib/cloudinary'));
const multer = require(path.resolve('config/lib/multer'));
const constant = require(path.resolve('./commons/utils/constant'));

const User = mongoose.model('User');
const ActivityHistory = mongoose.model('ActivityHistory');

/**
 * @overview Quên mật khẩu người dùng
 *
 * @param {Request} req
 * @param {Response} res
 * @param {Function} next
 *
 * @input body  { numberPhone: { code: String, number: String } }
 * @output      { code: String }
 */
const forgotPassword = (req, res, next) => {
  const bodyReq = req.body;
  const userSelf = req.user;
  let dataRes = null;

  async.waterfall([
    (done) => {
      /* Kiểm tra đầu vào */

      if (!userSelf) {
        User
          .findOne({ numberPhone: bodyReq.numberPhone }, `-${constant.SALT} -${constant.PASSWORD}`)
          .exec((err, user) => {
            if (err || !user) {
              // Người dùng không tồn tại
              dataRes = {
                status: 404,
                bodyRes: {
                  code: 'user.notExist',
                },
              };

              done(err || new Error('Not user exists'), dataRes);
            } else if (!user.verify.numberPhone.is) {
              // Người dùng không có quyền
              dataRes = {
                status: 401,
                bodyRes: {
                  code: 'user.permission.notExist',
                },
              };

              done(new Error('Not permission'), dataRes);
            } else {
              done(null, user);
            }
          });
      } else {
        // Người dùng đã đăng nhập
        dataRes = {
          status: 401,
          bodyRes: {
            code: 'user.signin.exist',
          },
        };

        done(new Error('User has signin'), dataRes);
      }
    }, (user, done) => {
      /* Tạo mã khôi phục mật khẩu người dùng */

      user.createResetTokenPassword(false)
        .then((token) => {
          done(null, user, token);
        })
        .catch((err) => {
          // Tạo mã xác thực khôi phục mật khẩu người dùng thất bại
          dataRes = {
            status: 400,
            bodyRes: {
              code: 'user.token.resetPassword.create.failed',
            },
          };

          done(err, dataRes);
        });
    }, (user, token, done) => {
      /* Thay đổi người dùng */

      user.save((err) => {
        if (err) {
          // Thay đổi mã xác thực khôi phục mật khẩu người dùng thất bại
          dataRes = {
            status: 400,
            bodyRes: {
              code: 'user.token.resetPassword.create.failed',
            },
          };
        } else {
          twilio.sendSMS(
            constant.TOKEN,
            user.numberPhone,
            {
              language: user.settings.general.language,
              token
            }
          );

          dataRes = {
            status: 200,
            bodyRes: {
              code: 'user.token.resetPassword.create.success',
            },
          };
        }

        done(err, dataRes);
      });
    },
  ], (err, dataRes) => {
    return res
      .status(dataRes.status)
      .send(dataRes.bodyRes)
      .end();
  });
};
module.exports.forgotPassword = forgotPassword;

/**
 * @overview Xác thực mã quên mật khẩu người dùng
 *
 * @param {Request} req
 * @param {Response} res
 * @param {Function} next
 *
 * @input body  { resetTokenPassword: String }
 * @output      { }
 */
const validateResetTokenPassword = (req, res, next) => {
  const bodyReq = req.body;
  const userSelf = req.user;
  let dataRes = null;

  if (!userSelf) {
    User
      .findOne({ 'tokens.security.password.reset.token': bodyReq.resetTokenPassword }, `-${constant.SALT} -${constant.PASSWORD}`)
      .exec((err, user) => {
        if (err || !user) {
          // Mã xác thực khôi phục mật khẩu người dùng không tồn tại
          dataRes = {
            status: 401,
            bodyRes: {
              code: 'user.token.resetPassword.notExist',
            },
          };
        } else if (user.checkExpiresResetTokenPassword()) {
          // Xác thực mã khôi phục mật khẩu người dùng thất bại
          dataRes = {
            status: 400,
            bodyRes: {
              code: 'user.token.resetPassword.verify.failed',
            },
          };
        } else {
          dataRes = {
            status: 200,
            bodyRes: {
              code: 'user.token.resetPassword.verify.success',
            },
          };
        }

        return res
          .status(dataRes.status)
          .send(dataRes.bodyRes)
          .end();
      });
  } else {
    // Người dùng đã đăng nhập
    dataRes = {
      status: 401,
      bodyRes: {
        code: 'user.signin.exist',
      },
    };

    return res
      .status(dataRes.status)
      .send(dataRes.bodyRes)
      .end();
  }
};
module.exports.validateResetTokenPassword = validateResetTokenPassword;

/**
 * @overview Khôi phục mật khẩu người dùng
 *
 * @param {Request} req
 * @param {Response} res
 * @param {Function} next
 *
 * @input body  {
 *                resetTokenPassword: String,
 *                passwordDetails: {
 *                  verifyPassword: String,
 *                  newPassword: String
 *                }
 *              }
 * @output      { code: String }
 */
const resetPassword = (req, res, next) => {
  const bodyReq = req.body;
  const userSelf = req.user;
  let dataRes = null;

  async.waterfall([
    (done) => {
      /* Kiểm tra đầu vào */

      if (!userSelf) {
        if (bodyReq.passwordDetails.newPassword === bodyReq.passwordDetails.verifyPassword) {
          User
            .findOne({ 'tokens.security.password.reset.token': bodyReq.resetTokenPassword }, `-${constant.SALT} -${constant.PASSWORD}`)
            .exec((err, user) => {
              if (err || !user || user.checkExpiresResetTokenPassword()) {
                // Mã xác thực khôi phục mật khẩu người dùng không tồn tại
                dataRes = {
                  status: 400,
                  bodyRes: {
                    code: 'user.token.resetPassword.notExist',
                  },
                };

                done(new Error(err) || new Error('Not exists user reset token password'), dataRes);
              } else {
                done(null, user);
              }
            });
        } else {
          // Người dùng không có quyền
          dataRes = {
            status: 401,
            bodyRes: {
              code: 'user.permission.notExist',
            },
          };

          done(new Error('Not permission'), dataRes);
        }
      } else {
        // Người dùng đã đăng nhập
        dataRes = {
          status: 401,
          bodyRes: {
            code: 'user.signin.exist',
          },
        };

        done(new Error('User has signin'), dataRes);
      }
    }, (user, done) => {
      /* Thiết lập mật khẩu người dùng mới */

      user.setPassword(bodyReq.passwordDetails.newPassword);
      user.deleteResetTokenPassword();
      user.save((err) => {
        if (err) {
          // Thay đổi mật khẩu người dùng thất bại
          dataRes = {
            status: 400,
            bodyRes: {
              code: 'user.password.change.failed',
            },
          };
        } else {
          dataRes = user;
        }

        done(err, dataRes);
      });
    }, (user, done) => {
      /* Đăng nhập người dùng */

      user.deleteSensitiveData();
      req.login(user, (err) => {
        if (err) {
          // Đăng nhập người dùng thất bại
          dataRes = {
            status: 401,
            bodyRes: {
              code: 'user.signin.failed',
            },
          };
        } else {
          const activityHistory = new ActivityHistory({
            userID: user._id,
            toURL: user._id,
            type: constant.USER,
            result: constant.SUCCESS,
            content: 'user.password.reset.is',
          });
          activityHistory.save();

          dataRes = {
            status: 200,
            bodyRes: {
              code: 'user.password.reset.success',
            },
          };
        }

        done(err, dataRes);
      });
    },
  ], (err, dataRes) => {
    return res
      .status(dataRes.status)
      .send(dataRes.bodyRes)
      .end();
  });
};
module.exports.resetPassword = resetPassword;

/**
 * @overview Lấy mã xác thực số diện thoại người dùng
 *
 * @param {Request} req
 * @param {Response} res
 * @param {Function} next
 *
 * @input  { }
 * @output { code: String }
 */
const getVerifyTokenNumberPhone = (req, res, next) => {
  const userSelf = req.user;
  let dataRes = null;

  if (userSelf) {
    userSelf.createVerifyTokenNumberPhone(false)
      .then((token) => {
        userSelf.save((err) => {
          if (err) {
            // Tạo mã xác thực số điện thoại người dùng thất bại
            dataRes = {
              status: 400,
              bodyRes: {
                code: 'user.token.verifyNumberPhone.create.failed',
              },
            };

            return res
              .status(dataRes.status)
              .send(dataRes.bodyRes)
              .end();
          }

          twilio.sendSMS(
            constant.TOKEN,
            userSelf.numberPhone,
            {
              language: userSelf.settings.general.language,
              token
            }
          );

          dataRes = {
            status: 200,
            bodyRes: {
              code: 'user.token.verifyNumberPhone.create.success',
            },
          };

          return res
            .status(dataRes.status)
            .send(dataRes.bodyRes)
            .end();
        });
      }, (err) => {
        // Tạo mã xác thực số điện thoại người dùng thất bại
        dataRes = {
          status: 400,
          bodyRes: {
            code: 'user.token.verifyNumberPhone.create.failed',
          },
        };

        return res
          .status(dataRes.status)
          .send(dataRes.bodyRes)
          .end();
      });
  } else {
    // Chưa đăng nhập người dùng
    dataRes = {
      status: 401,
      bodyRes: {
        code: 'user.signin.notExist',
      },
    };

    return res
      .status(dataRes.status)
      .send(dataRes.bodyRes)
      .end();
  }
};
module.exports.getVerifyTokenNumberPhone = getVerifyTokenNumberPhone;

/**
 * @overview Xác thực số điện thoại người dùng
 *
 * @param {Request} req
 * @param {Response} res
 * @param {Function} next
 *
 * @input body  { verifyTokenNumberPhone: String }
 * @output      { code: String, verifyNumberPhone?: { is: Boolean, date: Date } }
 */
const verifyNumberPhone = (req, res, next) => {
  const bodyReq = req.body;
  const userSelf = req.user;
  let dataRes = null;

  if (userSelf) {
    User
      .findOne({
        'tokens.security.numberPhone.verify.token': bodyReq.verifyTokenNumberPhone,
        'verify.numberPhone.is': false,
      }, `-${constant.SALT} -${constant.PASSWORD}`)
      .exec((err, user) => {
        if (err || !user || user.checkExpiresVerifyTokenNumberPhone()) {
          // Mã xác thực số điện thoại người dùng không tồn tại
          dataRes = {
            status: 400,
            bodyRes: {
              code: 'user.token.verifyNumberPhone.notExist',
            },
          };

          return res
            .status(dataRes.status)
            .send(dataRes.bodyRes)
            .end();
        }

        user.verifyNumberPhone(true);
        user.save((err) => {
          if (err) {
            // Xác thực số điện thoại người dùng thất bại
            dataRes = {
              status: 400,
              bodyRes: {
                code: 'user.numberPhone.verify.failed',
              },
            };
          } else {
            const activityHistory = new ActivityHistory({
              userID: userSelf._id,
              toURL: userSelf._id,
              type: constant.USER,
              result: constant.SUCCESS,
              content: 'user.numberPhone.verify.is',
            });
            activityHistory.save();

            dataRes = {
              status: 200,
              bodyRes: {
                code: 'user.numberPhone.verify.success',
                verifyNumberPhone: user.verify.numberPhone,
              },
            };
          }

          return res
            .status(dataRes.status)
            .send(dataRes.bodyRes)
            .end();
        });
      });
  } else {
    // Chưa đăng nhập người dùng
    dataRes = {
      status: 401,
      bodyRes: {
        code: 'user.signin.notExist',
      },
    };

    return res
      .status(dataRes.status)
      .send(dataRes.bodyRes)
      .end();
  }
};
module.exports.verifyNumberPhone = verifyNumberPhone;

/**
 * @overview Yêu cầu xác thực người dùng
 *
 * @param {Request} req
 * @param {Response} res
 * @param {Function} next
 *
 * @input file  {Object}
 * @output      { verifyAccount?:  Object, code: String }
 */
const requestVerifyAccount = (req, res, next) => {
  const userSelf = req.user;
  let dataRes = null;

  async.waterfall([
    (done) => {
      /* Kiểm tra đầu vào */

      if (userSelf) {
        if (userSelf.checkVerifyAccount()) {
          // Đã yêu cầu xác thực người dùng
          dataRes = {
            status: 400,
            bodyRes: {
              code: 'user.verify.account.request.exist',
            },
          };

          done(new Error('Exist request user verify'), dataRes);
        } else {
          const upload = multer().single(constant.VERIFY_ACCOUNT);
          upload(req, res, (err) => {
            const { file } = req;
            const errorFile = file ? multer.testUploadImage(constant.PROFILE, file) : new Error('Not file');

            if (!err && file && !errorFile) {
              dataRes = file;
            } else {
              // Bad request.file
              dataRes = {
                status: 400,
                bodyRes: {
                  code: 'data.notVaild',
                },
              };
            }

            done(err || errorFile, dataRes);
          });
        }
      } else {
        // Chưa đăng nhập người dùng
        dataRes = {
          status: 401,
          bodyRes: {
            code: 'user.signin.notExist',
          },
        };

        done(new Error('User not signin'), dataRes);
      }
    }, (file, done) => {
      /* Tải hình xác thực người dùng */

      cloudinary.uploadProfileImage(userSelf._id, file)
        .then((dataImage) => {
          done(null, dataImage);
        })
        .catch((err) => {
          // Tải hình xác thực người dùng thất bại
          dataRes = {
            status: 400,
            bodyRes: {
              code: 'user.verify.account.upload.failed',
            },
          };

          done(err, dataRes);
        });
    }, (image, done) => {
      /* Thay đổi xác thực người dùng */

      if (!userSelf.createVerifyAccount({ url: image.secure_url, publicID: image.public_id })) {
        // Tải hình xác thực người dùng thất bại
        dataRes = {
          status: 400,
          bodyRes: {
            code: 'user.verify.account.upload.failed',
          },
        };

        done(new Error('Exist request verify account'), dataRes);
      } else {
        userSelf.save((err) => {
          if (err) {
            // Yêu cầu xác thực người dùng thất bại
            dataRes = {
              status: 400,
              bodyRes: {
                code: 'user.verify.account.request.failed',
              },
            };
          } else {
            const activityHistory = new ActivityHistory({
              userID: userSelf._id,
              toURL: userSelf._id,
              type: constant.USER,
              result: constant.SUCCESS,
              content: 'user.verify.account.request.is',
            });
            activityHistory.save();

            dataRes = {
              status: 200,
              bodyRes: {
                code: 'user.verify.account.request.success',
                verifyAccount: userSelf.verify.account
              },
            };
          }

          done(err, dataRes);
        });
      }
    }], (err, dataRes) => {
      return res
        .status(dataRes.status)
        .send(dataRes.bodyRes)
        .end();
    });
};
module.exports.requestVerifyAccount = requestVerifyAccount;
