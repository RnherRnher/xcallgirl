const path = require('path');
const async = require('async');
const _ = require('lodash');
const mongoose = require('mongoose');
const multer = require(path.resolve('config/lib/multer'));
const cloudinary = require(path.resolve('config/lib/cloudinary'));
const logger = require(path.resolve('./config/lib/logger'));
const constant = require(path.resolve('./commons/utils/constant'));

const ActivityHistory = mongoose.model('ActivityHistory');
const User = mongoose.model('User');

/**
 * @overview Thay đổi số điện ngoài của hồ sơ người dùng
 *
 * @param {Request} req
 * @param {Response} res
 * @param {Function} next
 *
 * @input body  { numberPhone: { code: String, number: String }, password: String }
 * @output      {
 *                code: String,
 *                numberPhone?: {
 *                  code: String,
 *                  number: String
 *                },
 *                verify?:      {
 *                  is: Boolen,
 *                  date: Date
 *                }
 *               }
 */
const changeProfileNumberPhone = (req, res, next) => {
  const bodyReq = req.body;
  const userSelf = req.user;
  let dataRes = null;

  async.waterfall([
    (done) => {
      /* Kiểm tra đầu vào */
      if (userSelf) {
        User
          .findById(userSelf._id)
          .exec((err, user) => {
            if (err || !user) {
              // Chưa đăng nhập người dùng
              dataRes = {
                status: 401,
                bodyRes: {
                  code: 'user.signin.notExist',
                },
              };

              done(err || new Error('User not signin', dataRes));
            } else if (user.authenticate(bodyReq.password)) {
              done(null, bodyReq.numberPhone);
            } else {
              // Đăng nhập người dùng thất bại
              dataRes = {
                status: 401,
                bodyRes: {
                  code: 'user.signin.failed',
                },
              };

              done(new Error('User failed signin'), dataRes);
            }
          });
      } else {
        // Chưa đăng nhập người dùng
        dataRes = {
          status: 401,
          bodyRes: {
            code: 'user.signin.notExist',
          },
        };

        done(new Error('User not signin'), dataRes);
      }
    }, (numberPhone, done) => {
      /* Thay đổi SĐT người dùng */

      userSelf.setNumberPhone(numberPhone);
      userSelf.verifyNumberPhone(false);
      userSelf.save((err) => {
        if (err) {
          // Thay đổi số diện thoại người dùng thất bại
          dataRes = {
            status: 400,
            bodyRes: {
              code: 'user.numberPhone.change.failed',
            },
          };

          done(err, dataRes);
        } else {
          done(null);
        }
      });
    },
    (done) => {
      /* Đăng nhập người dùng */

      const activityHistory = new ActivityHistory({
        userID: userSelf._id,
        toURL: userSelf._id,
        type: constant.USER,
        result: constant.SUCCESS,
        content: 'user.numberPhone.change.is',
      });

      activityHistory.save((err) => {
        if (err) {
          logger.logError(
            'users.profile.server.controller',
            'changeProfileNumberPhone',
            constant.ACTIVITY_HISTORY,
            activityHistory,
            err
          );
        }
      });

      req.login(userSelf, (err) => {
        if (err) {
          // Đăng nhập người dùng thất bại
          dataRes = {
            status: 401,
            bodyRes: {
              code: 'user.signin.failed',
            },
          };
        } else {
          dataRes = {
            status: 200,
            bodyRes: {
              code: 'user.numberPhone.change.success',
              numberPhone: userSelf.numberPhone,
              verify: userSelf.verify.numberPhone,
            },
          };
        }

        done(err, dataRes);
      });
    }], (err, dataRes) => {
      return res
        .status(dataRes.status)
        .send(dataRes.bodyRes)
        .end();
    });
};
module.exports.changeProfileNumberPhone = changeProfileNumberPhone;

/**
 * @overview Thay đổi mật khẩu của hồ sơ người dùng
 *
 * @param {Request} req
 * @param {Response} res
 * @param {Function} next
 *
 * @input body  {
 *                passwordDetails: {
 *                  currentPassword: String,
 *                  newPassword: String,
 *                  verifyPassword: String
 *                }
 *              }
 * @output      { code: String }
 */
const changeProfilePassword = (req, res, next) => {
  const bodyReq = req.body;
  const userSelf = req.user;
  let dataRes = null;

  async.waterfall([(done) => {
    /* Kiểm tra đầu vào */

    if (userSelf) {
      if (bodyReq.passwordDetails.newPassword === bodyReq.passwordDetails.verifyPassword) {
        User
          .findById(userSelf._id)
          .exec((err, user) => {
            if (err || !user) {
              // Chưa đăng nhập người dùng
              dataRes = {
                status: 401,
                bodyRes: {
                  code: 'user.signin.notExist',
                },
              };

              done(err || new Error('User not signin', dataRes));
            } else if (user.authenticate(bodyReq.passwordDetails.currentPassword)) {
              done(null, bodyReq.passwordDetails);
            } else {
              // Đăng nhập người dùng thất bại
              dataRes = {
                status: 401,
                bodyRes: {
                  code: 'user.signin.failed',
                },
              };

              done(new Error('User failed signin'), dataRes);
            }
          });
      } else {
        // Bad request.body
        dataRes = {
          status: 400,
          bodyRes: {
            code: 'data.notVaild',
          },
        };

        done(new Error('Bad request.body'), dataRes);
      }
    } else {
      // Chưa đăng nhập người dùng
      dataRes = {
        status: 401,
        bodyRes: {
          code: 'user.signin.notExist',
        },
      };

      done(new Error('User not signin'), dataRes);
    }
  }, (passwordDetails, done) => {
    /* Cập nhất mật khẩu người dùng */

    userSelf.setPassword(passwordDetails.newPassword);
    userSelf.save((err) => {
      if (err) {
        // Thay đổi mật khẩu người dùng thất bại
        dataRes = {
          status: 400,
          bodyRes: {
            code: 'user.password.change.failed',
          },
        };

        done(err, dataRes);
      } else {
        done(null);
      }
    });
  }, (done) => {
    /* Đăng nhập người dùng */

    const activityHistory = new ActivityHistory({
      userID: userSelf._id,
      toURL: userSelf._id,
      type: constant.USER,
      result: constant.SUCCESS,
      content: 'user.password.change.is',
    });

    activityHistory.save((err) => {
      if (err) {
        logger.logError(
          'users.profile.server.controller',
          'changeProfilePassword',
          constant.ACTIVITY_HISTORY,
          activityHistory,
          err
        );
      }
    });

    req.login(userSelf, (err) => {
      if (err) {
        // Đăng nhập người dùng thất bại
        dataRes = {
          status: 401,
          bodyRes: {
            code: 'user.signin.failed',
          },
        };
      } else {
        dataRes = {
          status: 200,
          bodyRes: {
            code: 'user.password.change.success',
          },
        };
      }

      done(err, dataRes);
    });
  }], (err, dataRes) => {
    return res
      .status(dataRes.status)
      .send(dataRes.bodyRes)
      .end();
  });
};
module.exports.changeProfilePassword = changeProfilePassword;

/**
 * @overview Thay đổi hình đại diện của hồ sơ người dùng
 *
 * @param {Request} req
 * @param {Response} res
 * @param {Function} next
 *
 * @input file {Object}
 * @output     { code: String, urlAvatar?: String }
 */
const changeProfileAvatar = (req, res, next) => {
  const userSelf = req.user;
  let dataRes = null;

  async.waterfall([
    (done) => {
      /* Kiểm tra đầu vào */
      if (userSelf) {
        const upload = multer().single(constant.AVATAR);
        upload(req, res, (err) => {
          const { file } = req;
          const errorFile = file ? multer.testUploadImage(constant.PROFILE, file) : new Error('Not file');

          if (!err && file && !errorFile) {
            dataRes = file;
          } else {
            // Bad request.file
            dataRes = {
              status: 400,
              bodyRes: {
                code: 'data.notVaild',
              },
            };
          }

          done(err || errorFile, dataRes);
        });
      } else {
        // Chưa đăng nhập người dùng
        dataRes = {
          status: 401,
          bodyRes: {
            code: 'user.signin.notExist',
          },
        };

        done(new Error('User not signin'), dataRes);
      }
    }, (file, done) => {
      /* Kiểm tra người dùng và xoá hình cũ */

      if (userSelf.avatar.publicID) {
        cloudinary.deleteImageByPublicID(userSelf.avatar.publicID)
          .then((dataImage) => {
            done(null, file);
          })
          .catch((err) => {
            // Xoá hình đại diện người dùng thất bại
            dataRes = {
              status: 400,
              bodyRes: {
                code: 'user.avatar.delete.failed',
              },
            };

            done(err, dataRes);
          });
      } else {
        done(null, file);
      }
    }, (file, done) => {
      /* Đăng hình đại diện người dùng */

      cloudinary.uploadProfileImage(userSelf._id, file)
        .then((dataImage) => {
          done(null, dataImage);
        })
        .catch((err) => {
          // Tải hình đại diện người dùng thất bại
          dataRes = {
            status: 400,
            bodyRes: {
              code: 'user.avatar.upload.failed',
            },
          };

          done(err, dataRes);
        });
    }, (image, done) => {
      /* Thay đổi người dùng */

      userSelf.setAvatar({
        url: image.secure_url,
        publicID: image.public_id,
      });
      userSelf.save((err) => {
        if (err) {
          // Thay đổi hình đại diện người dùng thất bại
          dataRes = {
            status: 400,
            bodyRes: {
              code: 'user.avatar.change.failed',
            },
          };
        } else {
          const activityHistory = new ActivityHistory({
            userID: userSelf._id,
            toURL: userSelf._id,
            type: constant.USER,
            result: constant.SUCCESS,
            content: 'user.avatar.change.is',
          });

          activityHistory.save((err) => {
            if (err) {
              logger.logError(
                'users.profile.server.controller',
                'changeProfileAvatar',
                constant.ACTIVITY_HISTORY,
                activityHistory,
                err
              );
            }
          });

          dataRes = {
            status: 200,
            bodyRes: {
              code: 'user.avatar.change.success',
              urlAvatar: userSelf.avatar.url,
            },
          };
        }

        done(err, dataRes);
      });
    }], (err, dataRes) => {
      return res
        .status(dataRes.status)
        .send(dataRes.bodyRes)
        .end();
    });
};
module.exports.changeProfileAvatar = changeProfileAvatar;

/**
 * @overview Xoá hình đại diện của hồ sơ người dùng
 *
 * @param {Request} req
 * @param {Response} res
 * @param {Function} next
 *
 * @input  { }
 * @output { code: String, urlAvatar?: String }
 */
const deleteProfileAvatar = (req, res, next) => {
  const userSelf = req.user;
  let dataRes = null;

  async.waterfall([(done) => {
    /* Kiểm tra đầu vào */

    if (userSelf) {
      if (userSelf.avatar.publicID) {
        cloudinary.deleteImageByPublicID(userSelf.avatar.publicID)
          .then((dataImage) => {
            done(null);
          })
          .catch((err) => {
            // Xoá hình đại hiện thất bại
            dataRes = {
              status: 400,
              bodyRes: {
                code: 'user.avatar.delete.failed',
              },
            };

            done(err, dataRes);
          });
      } else {
        // Xoá hình đại hiện thất bại
        dataRes = {
          status: 400,
          bodyRes: {
            code: 'user.avatar.delete.failed',
          },
        };

        done(new Error('Bad request'), dataRes);
      }
    } else {
      // Chưa đăng nhập người dùng
      dataRes = {
        status: 401,
        bodyRes: {
          code: 'user.signin.notExist',
        },
      };

      done(new Error('User not signin'), dataRes);
    }
  }, (done) => {
    /* Cập nhật người dùng */

    userSelf.setDefaultAvatar();
    userSelf.save((err) => {
      if (err) {
        // Thay đổi hình đại diện người dùng thất bại
        dataRes = {
          status: 400,
          bodyRes: {
            code: 'user.avatar.change.failed',
          },
        };
      } else {
        const activityHistory = new ActivityHistory({
          userID: userSelf._id,
          toURL: userSelf._id,
          type: constant.USER,
          result: constant.SUCCESS,
          content: 'user.avatar.delete.is',
        });

        activityHistory.save((err) => {
          if (err) {
            logger.logError(
              'users.profile.server.controller',
              'deleteProfileAvatar',
              constant.ACTIVITY_HISTORY,
              activityHistory,
              err
            );
          }
        });

        dataRes = {
          status: 200,
          bodyRes: {
            code: 'user.avatar.delete.success',
            urlAvatar: userSelf.avatar.url,
          },
        };
      }

      done(err, dataRes);
    });
  }], (err, dataRes) => {
    return res
      .status(dataRes.status)
      .send(dataRes.bodyRes)
      .end();
  });
};
module.exports.deleteProfileAvatar = deleteProfileAvatar;

/**
 * @overview Thay đổi cài đặt người dùng
 *
 * @param {Request} req
 * @param {Response} res
 * @param {Function} next
 *
 * @input body  { settings: {Object} }
 *        query { type: String }
 * @output      { code: String, settings?: {Object} }
 */
const changeProfileSettings = (req, res, next) => {
  const bodyReq = req.body;
  const queryReq = req.query;
  const userSelf = req.user;
  let dataRes = null;

  if (userSelf) {
    if (userSelf.setSettings(queryReq.type, bodyReq.settings)) {
      userSelf.save((err) => {
        if (err) {
          // Thay đổi cài đặt người dùng thất bại
          dataRes = {
            status: 400,
            bodyRes: {
              code: 'user.setting.change.failed',
            },
          };
        } else {
          dataRes = {
            status: 200,
            bodyRes: {
              code: 'user.setting.change.success',
              settings: userSelf.getSettings(queryReq.type),
            },
          };
        }

        return res
          .status(dataRes.status)
          .send(dataRes.bodyRes)
          .end();
      });
    } else {
      // Thay đổi cài đặt người dùng thất bại
      dataRes = {
        status: 400,
        bodyRes: {
          code: 'data.notVaild',
        },
      };

      return res
        .status(dataRes.status)
        .send(dataRes.bodyRes)
        .end();
    }
  } else {
    // Chưa đăng nhập người dùng
    dataRes = {
      status: 401,
      bodyRes: {
        code: 'user.signin.notExist',
      },
    };

    return res
      .status(dataRes.status)
      .send(dataRes.bodyRes)
      .end();
  }
};
module.exports.changeProfileSettings = changeProfileSettings;

/**
 * @overview Lấy cài đặt người dùng
 *
 * @param {Request} req
 * @param {Response} res
 * @param {Function} next
 *
 * @input query { type: String }
 * @output      { settings?: {Object} }
 */
const getProfileSettings = (req, res, next) => {
  const queryReq = req.query;
  const userSelf = req.user;
  let dataRes = null;

  if (userSelf) {
    const settings = userSelf.getSettings(queryReq.type);
    if (!_.isEmpty(settings)) {
      dataRes = {
        status: 200,
        bodyRes: {
          code: 'user.setting.exist',
          settings,
        },
      };
    } else {
      // Bad request.body
      dataRes = {
        status: 400,
        bodyRes: {
          code: 'data.notVaild',
        },
      };
    }
  } else {
    // Chưa đăng nhập người dùng
    dataRes = {
      status: 401,
      bodyRes: {
        code: 'user.signin.notExist',
      },
    };
  }

  return res
    .status(dataRes.status)
    .send(dataRes.bodyRes)
    .end();
};
module.exports.getProfileSettings = getProfileSettings;

/**
 * @overview Thay đổi địa chỉ người dùng
 *
 * @param {Request} req
 * @param {Response} res
 * @param {Function} next
 *
 * @input body  { address: {Object} }
 * @output      { code: String, address?: {Object} }
 */
const changeProfileAddress = (req, res, next) => {
  const bodyReq = req.body;
  const userSelf = req.user;
  let dataRes = null;

  if (userSelf) {
    if (!_.isEmpty(bodyReq.address)) {
      userSelf.address = bodyReq.address;
      userSelf.save((err) => {
        if (err) {
          // Thay đổi cài đặt người dùng thất bại
          dataRes = {
            status: 400,
            bodyRes: {
              code: 'user.address.change.failed',
            },
          };
        } else {
          const activityHistory = new ActivityHistory({
            userID: userSelf._id,
            toURL: userSelf._id,
            type: constant.USER,
            result: constant.SUCCESS,
            content: 'user.address.change.is',
          });

          activityHistory.save((err) => {
            if (err) {
              logger.logError(
                'users.profile.server.controller',
                'changeProfileAddress',
                constant.ACTIVITY_HISTORY,
                activityHistory,
                err
              );
            }
          });

          dataRes = {
            status: 200,
            bodyRes: {
              code: 'user.address.change.success',
              address: userSelf.address,
            },
          };
        }

        return res
          .status(dataRes.status)
          .send(dataRes.bodyRes)
          .end();
      });
    } else {
      // Thay đổi địa chỉ người dùng thất bại
      dataRes = {
        status: 400,
        bodyRes: {
          code: 'data.notVaild',
        },
      };

      return res
        .status(dataRes.status)
        .send(dataRes.bodyRes)
        .end();
    }
  } else {
    // Chưa đăng nhập người dùng
    dataRes = {
      status: 401,
      bodyRes: {
        code: 'user.signin.notExist',
      },
    };

    return res
      .status(dataRes.status)
      .send(dataRes.bodyRes)
      .end();
  }
};
module.exports.changeProfileAddress = changeProfileAddress;
