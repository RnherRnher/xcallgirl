const path = require('path');
const logger = require(path.resolve('./config/lib/logger'));
const constant = require(path.resolve('./commons/utils/constant'));
const { SOCKET_DISCONNECT } = require(path.resolve('./commons/modules/core/datas/socket.data'));
const { SOCKET_SELF_USER_UPDATE } = require(path.resolve('./commons/modules/users/datas/socket.data'));

/**
 * @overview Khởi tạo khi người dùng online
 *
 * @param {SocketIO} io
 * @param {Socket} socket
 */
const initUserConnect = (io, socket) => {
  const userSelf = socket.request.user;
  if (userSelf) {
    userSelf.setOnline();
    userSelf.save((err) => {
      if (err) {
        socket.disconnect(true);
      } else {
        socket.join(userSelf._id);
      }
    });
  }
};

/**
 * @overview Khởi tạo khi người dùng offline
 *
 * @param {SocketIO} io
 * @param {Socket} socket
 */
const initUserDisconnect = (io, socket) => {
  const userSelf = socket.request.user;
  if (userSelf) {
    userSelf.setOffline();
    userSelf.save((err) => {
      if (err) {
        logger.logError(
          'user.server.socket',
          'initUserDisconnect',
          constant.USER,
          userSelf,
          err
        );
      }
    });
  }
};

/**
 * @overview Khởi tạo
 *
 * @param {SocketIO} io
 * @param {Socket} socket
 */
const init = (io, socket) => {
  initUserConnect(io, socket);

  socket.on(SOCKET_SELF_USER_UPDATE, (dataReq) => {
    io.to(dataReq.to)
      .emit(
        SOCKET_SELF_USER_UPDATE,
        dataReq.body
      );
  });

  socket.on(SOCKET_DISCONNECT, () => {
    initUserDisconnect(io, socket);
  });
};
module.exports = init;
