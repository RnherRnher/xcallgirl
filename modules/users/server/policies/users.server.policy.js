
const Acl = require('acl');
const path = require('path');
const allowsAdmin = require('./users/admin.server.policy');
const allowsProvider = require('./users/provider.server.policy');
const allowsUser = require('./users/user.server.policy');
const allowsGuest = require('./users/guest.server.policy');
const allowsSystem = require('./users/system.server.policy');
const constant = require(path.resolve('./commons/utils/constant'));

const MemoryBackend = Acl.memoryBackend;
const acl = new Acl(new MemoryBackend());

const invokeRolesPolicies = () => {
  acl.allow([{
    roles: constant.GUEST,
    allows: allowsGuest,
  }, {
    roles: constant.USER,
    allows: allowsUser,
  }, {
    roles: constant.PROVIDER,
    allows: allowsProvider,
  }, {
    roles: constant.ADMIN,
    allows: allowsAdmin,
  }, {
    roles: constant.SYSTEM,
    allows: allowsSystem,
  }]);
};
module.exports.invokeRolesPolicies = invokeRolesPolicies;

const isAllowed = (req, res, next) => {
  const roles = req.user ? req.user.roles.value : req.app.locals.defaultRoles;
  acl.areAnyRolesAllowed(roles, req.route.path, req.method.toLowerCase(), (err, isAllowed) => {
    let dataRes = null;

    if (!isAllowed) {
      dataRes = {
        status: 401,
        bodyRes: {
          code: 'user.permission.notExist',
        },
      };
    } else {
      return next(err);
    }

    return res
      .status(dataRes.status)
      .send(dataRes.bodyRes)
      .end();
  });
};
module.exports.isAllowed = isAllowed;
