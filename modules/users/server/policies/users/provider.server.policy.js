const path = require('path');
const url = require(path.resolve('./commons/modules/users/datas/url.data'));

module.exports = [
  /* auth */
  {
    resources: url.AUTH_SIGNOUT,
    permissions: ['get'],
  },
  /* users */
  {
    resources: url.USERS,
    permissions: ['get'],
  }, {
    resources: url.USERS_GET_ID,
    permissions: ['get'],
  }, {
    resources: url.USERS_SEARCH,
    permissions: ['get'],
  }, {
    resources: url.USERS_NUMBERPHONE,
    permissions: ['get', 'post', 'put'],
  }, {
    resources: url.USERS_PASSWORD,
    permissions: ['post'],
  }, {
    resources: url.USERS_AVATAR,
    permissions: ['post', 'delete'],
  }, {
    resources: url.USERS_SETTINGS,
    permissions: ['get', 'post'],
  }, {
    resources: url.USERS_VERIFY,
    permissions: ['post'],
  }, {
    resources: url.USERS_ADDRESS,
    permissions: ['post'],
  },
];
