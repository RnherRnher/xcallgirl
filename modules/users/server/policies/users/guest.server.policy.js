const path = require('path');
const url = require(path.resolve('./commons/modules/users/datas/url.data'));

module.exports = [
  /* auth */
  {
    resources: url.AUTH_FORGOT_PASSWORD,
    permissions: ['post'],
  }, {
    resources: url.AUTH_RESET,
    permissions: ['post', 'put'],
  }, {
    resources: url.AUTH_SIGNUP,
    permissions: ['post'],
  }, {
    resources: url.AUTH_SIGNIN,
    permissions: ['post'],
  },
];
