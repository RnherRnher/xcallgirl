const path = require('path');
const policy = require('../policies/users.server.policy');
const users = require('../controllers/users.server.controller');
const validate = require(path.resolve('./commons/modules/users/validates/user.validate'));
const url = require(path.resolve('./commons/modules/users/datas/url.data'));
const middleware = require(path.resolve('./utils/middleware'));

const init = (app) => {
  app.route(url.AUTH_FORGOT_PASSWORD)
    .all(policy.isAllowed)
    .post(middleware.paramsValidation(validate, 'forgotPassword'), users.forgotPassword);
  app.route(url.AUTH_RESET)
    .all(policy.isAllowed)
    .post(middleware.paramsValidation(validate, 'validateResetTokenPassword'), users.validateResetTokenPassword)
    .put(middleware.paramsValidation(validate, 'resetPassword'), users.resetPassword);

  app.route(url.AUTH_SIGNUP)
    .all(policy.isAllowed)
    .post(middleware.paramsValidation(validate, 'signup'), users.signup);
  app.route(url.AUTH_SIGNIN)
    .all(policy.isAllowed)
    .post(middleware.paramsValidation(validate, 'signin'), users.signin);
  app.route(url.AUTH_SIGNOUT)
    .all(policy.isAllowed)
    .get(users.signout);
};
module.exports = init;
