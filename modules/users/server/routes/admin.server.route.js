
const path = require('path');
const policy = require('../policies/users.server.policy');
const admin = require('../controllers/admin.server.controller');
const validate = require(path.resolve('./commons/modules/users/validates/admin.validate'));
const url = require(path.resolve('./commons/modules/users/datas/url.data'));
const middleware = require(path.resolve('./utils/middleware'));

const init = (app) => {
  app.route(url.USERS_ADMIN_SEARCH)
    .all(policy.isAllowed)
    .get(middleware.paramsValidation(validate, 'search'), admin.search);

  app.route(url.USERS_VERIFY_ID)
    .all(policy.isAllowed)
    .post(middleware.paramsValidation(validate, 'verifyAccount'), admin.verifyAccount);
  app.route(url.USERS_LOCK_ID)
    .all(policy.isAllowed)
    .post(middleware.paramsValidation(validate, 'lockAccount'), admin.lockAccount)
    .put(middleware.paramsValidation(validate, 'unlockAccount'), admin.unlockAccount);
  app.route(url.USERS_DELETE_ID)
    .all(policy.isAllowed)
    .post(middleware.paramsValidation(validate, 'deleteAccount'), admin.deleteAccount)
    .put(middleware.paramsValidation(validate, 'restoreAccount'), admin.restoreAccount);
};
module.exports = init;
