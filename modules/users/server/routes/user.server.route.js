
const path = require('path');
const policy = require('../policies/users.server.policy');
const users = require('../controllers/users.server.controller');
const validate = require(path.resolve('./commons/modules/users/validates/user.validate'));
const url = require(path.resolve('./commons/modules/users/datas/url.data'));
const middleware = require(path.resolve('./utils/middleware'));

const init = (app) => {
  app.route(url.USERS_SEARCH)
    .all(policy.isAllowed)
    .get(middleware.paramsValidation(validate, 'search'), users.search);

  app.route(url.USERS_GET_ID)
    .all(policy.isAllowed)
    .get(middleware.paramsValidation(validate, 'get'), users.get);

  app.route(url.USERS)
    .all(policy.isAllowed)
    .get(users.getSelf);
  app.route(url.USERS_NUMBERPHONE)
    .all(policy.isAllowed)
    .get(users.getVerifyTokenNumberPhone)
    .post(middleware.paramsValidation(validate, 'changeProfileNumberPhone'), users.changeProfileNumberPhone)
    .put(middleware.paramsValidation(validate, 'verifyNumberPhone'), users.verifyNumberPhone);
  app.route(url.USERS_ADDRESS)
    .all(policy.isAllowed)
    .post(middleware.paramsValidation(validate, 'changeProfileAddress'), users.changeProfileAddress);
  app.route(url.USERS_PASSWORD)
    .all(policy.isAllowed)
    .post(middleware.paramsValidation(validate, 'changeProfilePassword'), users.changeProfilePassword);
  app.route(url.USERS_AVATAR)
    .all(policy.isAllowed)
    .post(users.changeProfileAvatar)
    .delete(users.deleteProfileAvatar);
  app.route(url.USERS_SETTINGS)
    .all(policy.isAllowed)
    .get(middleware.paramsValidation(validate, 'getProfileSettings'), users.getProfileSettings)
    .post(middleware.paramsValidation(validate, 'changeProfileSettings'), users.changeProfileSettings);
  app.route(url.USERS_VERIFY)
    .all(policy.isAllowed)
    .post(users.requestVerifyAccount);
};
module.exports = init;
