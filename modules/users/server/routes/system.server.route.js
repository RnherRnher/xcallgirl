
const path = require('path');
const policy = require('../policies/users.server.policy');
const system = require('../controllers/system.server.controller');
const validate = require(path.resolve('./commons/modules/users/validates/system.validate'));
const url = require(path.resolve('./commons/modules/users/datas/url.data'));
const middleware = require(path.resolve('./utils/middleware'));

const init = (app) => {
  app.route(url.USERS_SYSTEM_ROLES_ID)
    .all(policy.isAllowed)
    .post(middleware.paramsValidation(validate, 'changeRolesAccount'), system.changeRolesAccount);
};
module.exports = init;
