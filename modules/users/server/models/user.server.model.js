const path = require('path');
const chalk = require('chalk');
const crypto = require('crypto');
const _ = require('lodash');
const async = require('async');
const mongoose = require('mongoose');
const regular = require(path.resolve('./commons/utils/regular-expression'));
const constant = require(path.resolve('./commons/utils/constant'));
const { validateModelUser } = require(path.resolve('./utils/validate'));

const { Schema } = mongoose;

const validates = validateModelUser();

const UserSchema = new Schema({
  name: {
    first: {
      type: String,
      required: true,
      trim: true,
      match: regular.user.name.first,
    },
    last: {
      type: String,
      required: true,
      trim: true,
      match: regular.user.name.last,
    },
    display: {
      type: String,
      required: true,
      trim: true,
      match: regular.user.name.display,
    },
    nick: {
      type: String,
      // index: true,
      required: true,
      unique: true,
      trim: true,
      match: regular.user.name.nick,
    },
  },
  numberPhone: {
    code: {
      type: String,
      required: true,
      match: regular.user.numberPhone.code,
    },
    number: {
      type: String,
      required: true,
      unique: true,
      match: regular.user.numberPhone.number,
    },
  },
  gender: {
    type: String,
    required: true,
    enum: regular.user.gender,
  },
  birthday: {
    day: {
      type: Number,
      min: 1,
      max: 31,
      required: true,
    },
    month: {
      type: Number,
      min: 0,
      max: 11,
      required: true,
    },
    year: {
      type: Number,
      min: 1900,
      required: true,
    },
  },
  address: {
    country: {
      type: String,
      required: true,
      maxlength: regular.commons.stringLength.medium,
    },
    city: {
      type: String,
      maxlength: regular.commons.stringLength.medium,
    },
    county: {
      type: String,
      maxlength: regular.commons.stringLength.medium,
    },
    local: {
      type: String,
      maxlength: regular.commons.stringLength.medium,
    },
  },
  password: {
    type: String,
    required: true,
    maxlength: regular.commons.stringLength.medium,
    default: '',
  },
  salt: {
    type: String,
    maxlength: regular.commons.stringLength.medium,
  },
  avatar: {
    url: {
      type: String,
      required: true,
      match: regular.app.urlCloudinary,
      default: validates.url.avatar.undefined
    },
    publicID: {
      type: String,
      maxlength: regular.commons.stringLength.medium,
    },
  },
  roles: {
    value: {
      type: String,
      required: true,
      enum: regular.user.roles,
      default: constant.USER,
    },
    initDate: {
      type: Date,
      required: true,
      default: Date.now
    },
  },
  lock: {
    is: {
      type: Boolean,
      required: true,
      default: false,
    },
    admin: {
      type: [{
        _id: {
          required: true,
          type: mongoose.SchemaTypes.ObjectId,
        },
        reason: {
          type: String,
          trim: true,
          maxlength: regular.commons.stringLength.large,
        },
        confirmDate: {
          required: true,
          type: Date,
        },
      }]
    },
    expiresDate: {
      type: Number,
    },
  },
  delete: {
    is: {
      type: Boolean,
      required: true,
      default: false,
    },
    reason: {
      type: String,
      trim: true,
      maxlength: regular.commons.stringLength.large,
    },
    initDate: {
      type: Date,
    },
  },
  tokens: {
    security: {
      password: {
        reset: {
          token: {
            type: String,
            unique: true,
            sparse: true,
            maxlength: regular.commons.stringLength.medium,
          },
          date: {
            confirm: {
              type: Date,
            },
            expires: {
              type: Number,
              min: 0
            },
          },
        },
      },
      numberPhone: {
        verify: {
          token: {
            type: String,
            unique: true,
            sparse: true,
            maxlength: regular.commons.stringLength.medium,
          },
          date: {
            confirm: {
              type: Date,
            },
            expires: {
              type: Number,
              min: 0
            },
          },
        },
      },
    },
  },
  verify: {
    account: {
      is: {
        type: Boolean,
        required: true,
        default: false,
      },
      status: {
        type: String,
        required: true,
        enum: validates.statusVerfy,
        default: constant.NONE,
      },
      data: {
        type: [{
          publicID: {
            type: String,
            required: true,
            maxlength: regular.commons.stringLength.medium,
          },
          url: {
            type: String,
            required: true,
            match: regular.app.urlCloudinary,
          },
          initDate: {
            required: true,
            type: Date,
          },
        }]
      },
      admin: {
        type: [{
          _id: {
            required: true,
            type: mongoose.SchemaTypes.ObjectId,
          },
          reason: {
            type: String,
            trim: true,
            maxlength: regular.commons.stringLength.large,
          },
          confirmDate: {
            required: true,
            type: Date,
          },
        }]
      },
    },
    numberPhone: {
      is: {
        type: Boolean,
        required: true,
        default: false,
      },
      initDate: {
        type: Date,
      },
    },
  },
  settings: {
    general: {
      language: {
        type: String,
        required: true,
        enum: validates.languages,
      },
      notifications: {
        sms: {
          type: Boolean,
          required: true,
          default: true,
        },
      },
    },
    security: {
      account: {
        visible: {
          contacts: {
            type: String,
            required: true,
            enum: regular.commons.visible,
            default: constant.FULL,
          },
          address: {
            type: String,
            required: true,
            enum: regular.commons.visible,
            default: constant.FULL,
          },
          birthday: {
            type: String,
            required: true,
            enum: regular.commons.visible,
            default: constant.FULL,
          },
        },
      },
    },
  },
  online: {
    is: {
      type: Boolean,
      required: true,
      default: false,
    },
    date: {
      type: Date,
      required: true,
      default: Date.now
    },
  },
  initDate: {
    type: Date,
    required: true,
    default: Date.now
  }
});

// UserSchema.index({
//   'name.nick': 'text'
// });

/**
 * @overview Trước khi lưu
 *
 */
UserSchema.pre('save', function (next) {
  if (this.password && this.isModified(constant.PASSWORD)) {
    this.salt = crypto.randomBytes(16).toString('base64');
    this.password = this.hashPassword(this.password);
  }

  return next();
});

/**
 * @overview Mã hoá mật khẩu người dùng
 *
 * @param {String} password
 */
UserSchema.methods.hashPassword = function (password) {
  if (this.salt && password) {
    return crypto.pbkdf2Sync(password, Buffer.from(this.salt, 'base64'), 10000, 64, 'SHA1').toString('base64');
  }

  return password;
};

/**
 * @overview Kiểm tra độ tuổi cho phép
 *
 * @param {Boolean}
 */
UserSchema.methods.testAgeRestriction = function () {
  return this.birthday.year <= (new Date().getFullYear() - regular.app.ageRestriction);
};

/**
 * @overview Thiết lập mật khẩu người dùng
 *
 * @param {String} password
 */
UserSchema.methods.setPassword = function (password) {
  this.password = password;
};

/**
 * @overview Tạo token khôi phục mật khẩu
 *
 * @param {Boolean} overwrite true: Vẫn tạo token khi chưa hết hạn
 * @param {Promise}
 */
UserSchema.methods.createResetTokenPassword = function (overwrite) {
  return new Promise((resolve, reject) => {
    crypto.randomBytes(4, (err, buffer) => {
      if (err) {
        return reject(err);
      }

      const resetTokenPassword = this.tokens.security.password.reset;

      if (resetTokenPassword.token && !overwrite) {
        const expiresDate = resetTokenPassword.date.confirm.getTime() + resetTokenPassword.date.expires;
        if (expiresDate > Date.now()) {
          return reject(new Error('Exist reset token password'));
        }
      }

      const token = buffer.toString('base64').replace(/=/g, '').replace(/\+/g, '').replace(/\//g, '');
      resetTokenPassword.token = token;
      resetTokenPassword.date.expires = validates.expires.resetTokenPassword;
      resetTokenPassword.date.confirm = Date.now();

      this.tokens.security.password.reset = resetTokenPassword;

      return resolve(token);
    });
  });
};

/**
 * @overview Kiểm trả hết hạn token khôi phục mật khẩu
 *
 * @param {Boolean}
 */
UserSchema.methods.checkExpiresResetTokenPassword = function () {
  const resetTokenPassword = this.tokens.security.password.reset;
  const expiresDate = resetTokenPassword.date.confirm.getTime() + resetTokenPassword.date.expires;

  return expiresDate < Date.now();
};

/**
 * @overview Xoá token cài lại mật khẩu
 */
UserSchema.methods.deleteResetTokenPassword = function () {
  const resetTokenPassword = this.tokens.security.password.reset;
  resetTokenPassword.token = undefined;
  resetTokenPassword.date = undefined;
  this.tokens.security.password.reset = resetTokenPassword;
};

/**
 * @overview Tạo token xác thực số điện thoại khi vẫn chưa xác thực
 *
 * @param {Boolean} overwrite true: Vẫn tạo token khi chưa hết hạn
 * @param {Promise}
 */
UserSchema.methods.createVerifyTokenNumberPhone = function (overwrite) {
  return new Promise((resolve, reject) => {
    if (this.verify.numberPhone.is) {
      return reject(new Error('Account has verify number phone'));
    }
    crypto.randomBytes(4, (err, buffer) => {
      if (err) {
        return reject(err);
      }

      const verifyTokenNumberPhone = this.tokens.security.numberPhone.verify;

      if (verifyTokenNumberPhone.token && !overwrite) {
        const expiresDate = verifyTokenNumberPhone.date.confirm.getTime() + verifyTokenNumberPhone.date.expires;
        if (expiresDate > Date.now()) {
          return reject(new Error('Exist verify token number phone'));
        }
      }

      const token = buffer.toString('base64').replace(/=/g, '').replace(/\+/g, '').replace(/\//g, '');
      verifyTokenNumberPhone.token = token;
      verifyTokenNumberPhone.date.expires = validates.expires.verifyTokenNumberPhone;
      verifyTokenNumberPhone.date.confirm = Date.now();

      this.tokens.security.numberPhone.verify = verifyTokenNumberPhone;

      return resolve(token);
    });
  });
};

/**
 * @overview Kiểm trả hết hạn token xác thực số điện thoại người dùng
 *
 * @param {Boolean}
 */
UserSchema.methods.checkExpiresVerifyTokenNumberPhone = function () {
  const verifyTokenNumberPhone = this.tokens.security.numberPhone.verify;
  const expiresDate = verifyTokenNumberPhone.date.confirm.getTime() + verifyTokenNumberPhone.date.expires;

  return expiresDate < Date.now();
};

/**
 * @overview Xoá token xác thực số điện thoại
 *
 */
UserSchema.methods.deleteVerifyTokenNumberPhone = function () {
  const verifyTokenNumberPhone = this.tokens.security.numberPhone.verify;
  verifyTokenNumberPhone.token = undefined;
  verifyTokenNumberPhone.date = undefined;
  this.tokens.security.numberPhone.verify = verifyTokenNumberPhone;
};

/**
 * @overview So sánh khớp mập khẩu khi đăng nhập
 *
 * @param {String} password
 */
UserSchema.methods.authenticate = function (password) {
  return this.password === this.hashPassword(password);
};

/**
 * @overview Xoá dữ liệu nhậy cảm
 *
 */
UserSchema.methods.deleteSensitiveData = function () {
  this.password = undefined;
  this.salt = undefined;
};

/**
 * @overview Thiết lập số điện thoại người dùng
 *
 * @param   {String|Object} numberPhone
 * @returns {Boolean}
 */
UserSchema.methods.setNumberPhone = function (numberPhone) {
  const User = mongoose.model('User');

  let _numberPhone = numberPhone;

  if (_.isString(numberPhone)) {
    _numberPhone = User.decodeNumberPhone(numberPhone);
  }

  this.numberPhone = _numberPhone;
};


/**
 * @overview Xác thực số điện thoại người dùng
 *
 * @param {Boolean} is
 */
UserSchema.methods.verifyNumberPhone = function (is) {
  this.deleteVerifyTokenNumberPhone();

  this.verify.numberPhone.is = is;
  this.verify.numberPhone.initDate = Date.now();
};

/**
 * @overview Tạo mới xác thực người dùng
 *
 * @returns {Boolean}
 */
UserSchema.methods.createVerifyAccount = function (data) {
  const verifyAccount = this.verify.account;

  if (this.checkVerifyAccount()) {
    return false;
  }

  verifyAccount.data.push(_.assignIn(data, { initDate: Date.now() }));
  verifyAccount.status = constant.WAIT;

  this.verify.account = verifyAccount;

  return true;
};

/**
 * @overview Kiểm tra xác thực người dùng
 *
 * @returns {Boolean}
 */
UserSchema.methods.checkVerifyAccount = function () {
  const verifyAccount = this.verify.account;

  if (verifyAccount.is || verifyAccount.status === constant.WAIT) {
    return true;
  }

  return false;
};

/**
 * @overview Xác nhận xác thực người dùng
 *
 * @param   {ObjectId} adminID
 * @param   {Boolean} verify
 * @param   {String} reason
 * @returns {Boolean}  true : Đã xử lý xác nhận xác thực người dùng
 *                    false: Đã xác thực thành công trước đó rồi
 */
UserSchema.methods.verifyAccount = function (adminID, verify, reason) {
  const verifyAccount = this.verify.account;

  if (verifyAccount.is) {
    return false;
  }
  verifyAccount.is = verify;
  verifyAccount.status = verify ? constant.SUCCESS : constant.FAILED;
  verifyAccount.admin.push({
    _id: adminID,
    reason,
    confirmDate: Date.now()
  });

  this.verify.account = verifyAccount;

  return true;
};

/**
 * @overview Cài đặt hình đại diện mặc định
 *
 */
UserSchema.methods.setDefaultAvatar = function () {
  this.avatar.url = validates.url.avatar[this.gender];
  this.avatar.publicID = undefined;
};

/**
 * @overview Thiết lập vai trò người dùng
 *
 * @param   {String} roles
 * @returns {Boolean}
 *
 */
UserSchema.methods.setRoles = function (roles) {
  if (
    (this.roles.value === constant.PROVIDER
      && roles === constant.USER)
    || roles === constant.PROVIDER
    || this.roles.value === roles
  ) {
    return false;
  }

  this.roles = {
    value: roles,
    initDate: Date.now()
  };

  return true;
};

/**
 * @overview Kiểm tra gửi tin nhắn người dùng
 *
 * @returns {Boolean}
 *
 */
UserSchema.methods.checkSendSMS = function () {
  return this.settings.general.notifications.sms;
};

/**
 * @overview Thiết lập dữ liệu hình đại diện người dùng
 *
 * @param   {Object} image
 * @returns {Boolean}
 *
 */
UserSchema.methods.setAvatar = function (image) {
  this.avatar.url = image.url;
  this.avatar.publicID = image.publicID;
};

/**
 * @overview Thiết lập cài đặt người dùng
 *
 * @param    {Object} data
 * @returns  {Boolean}  true : Thành công
 *                      false: Kiểu cài đặt không hợp lệ
 */
UserSchema.methods.setSettings = function (type, settings) {
  if (type === constant.FULL) {
    this.settings = _.assignIn(this.settings, settings);
    return true;
  }

  if (_.includes(validates.settings, type)) {
    this.settings[type] = _.assignIn(this.settings[type], settings);
    return true;
  }

  return false;
};

/**
 * @overview Lấy thiết lập cài đặt người dùng
 *
 * @param   {String} type
 * @returns {{Object} || {}}
 */
UserSchema.methods.getSettings = function (type) {
  if (type === constant.FULL || _.includes(validates.settings, type)) {
    return this.settings;
  }

  return {};
};

/**
 * @overview Khoá người dùng
 *
 * @param    {ObjectId} adminID
 * @param    {Number} expires
 * @param    {String} reason
 */
UserSchema.methods.lockAccount = function (adminID, expires, reason) {
  const _thisExpireDate = this.lock.expiresDate;

  this.lock.is = true;
  this.lock.admin.push({
    _id: adminID,
    reason,
    confirmDate: Date.now()
  });
  this.lock.expiresDate = _thisExpireDate ? _thisExpireDate + expires : expires;
};

/**
 * @overview Mở khoá người dùng
 */
UserSchema.methods.unlockAccount = function () {
  this.lock.is = false;
  this.lock.expiresDate = undefined;
};

/**
 * @overview Kiểm tra hết hạn khoá
 *
 * @returns {Boolean} true : Hết hạn khoá người dùng
 *                    false: Thời gian khoá còn hiệu lực
 */
UserSchema.methods.checkExpiresLockAccount = function () {
  const { lock } = this;

  if ((lock.admin[lock.admin.length - 1].confirmDate.getTime() + lock.expiresDate) <= Date.now()) {
    this.unlockAccount();

    return true;
  }

  return false;
};

/**
 * @overview Xoá người dùng
 *
 * @param    {String} reason
 * @returns  {is: Boolean, reason: String, date: Date}
 */
UserSchema.methods.deleteAccount = function (reason) {
  this.delete.is = true;
  this.delete.reason = reason;
  this.delete.initDate = Date.now();

  return this.delete;
};

/**
 * @overview Khôi phục người dùng
 *
 */
UserSchema.methods.restoreAccount = function () {
  this.delete.is = false;
  this.delete.reason = undefined;
  this.delete.initDate = undefined;
};

/**
 * @overview Online
 *
 * @returns {is: Boolean, date: Date}
 */
UserSchema.methods.setOnline = function () {
  this.online.is = true;
  this.online.date = Date.now();

  return this.online;
};

/**
 * @overview Offline
 *
 * @returns {is: Boolean, date: Date}
 */
UserSchema.methods.setOffline = function () {
  this.online.is = false;
  this.online.date = Date.now();

  return this.online;
};

/**
 * @overview Lấy cấp độ uỷ quyền bằng vai trò
 *
 * @param    {String} roles ['user', 'provider', 'admin']
 * @returns  {Number}       Không được làm gì       => ['none'] => 0
 *                          Chỉ được xem            => ['view'] => 1
 *                          Xem và chỉnh sửa        => ['full'] => 2
 */
UserSchema.methods.getAuthorizedLevel = function (roles) {
  const User = mongoose.model('User');

  let level = -1;

  if (roles && _.includes(regular.user.roles, roles)) {
    level = this.getAuthorizedLevelByRoles(roles);
  }

  if (level > 0) {
    if (this.getAuthorizedLevelByRoles() === User.getAuthorizedLevelByRoles(constant.PROVIDER)) {
      return User.getAuthorizedLevelByType(constant.VIEW);
    }

    return User.getAuthorizedLevelByType(constant.FULL);
  }

  if (level === 0) {
    return User.getAuthorizedLevelByType(constant.VIEW);
  }

  return User.getAuthorizedLevelByType(constant.NONE);
};

/**
 * @overview Lấy cấp độ uỷ quyền
 *
 * @param     {String} roles ['user', 'provider', 'admin']
 * @returns   {Number} 0|1|2
 */
UserSchema.methods.getAuthorizedLevelByRoles = function (roles) {
  if (roles) {
    return _.indexOf(regular.user.roles, this.roles.value) - _.indexOf(regular.user.roles, roles);
  }

  return _.indexOf(regular.user.roles, this.roles.value);
};

/**
 * @overview Lấy thông tin cơ bản nhất của của người dùng
 *
 * @returns {Object}
 */
UserSchema.methods.getShortcutsInfo = function () {
  return {
    _id: this._id,
    avatar: this.avatar.url,
    nick: this.name.nick,
    verifyAccount: this.verify.account.is,
    roles: this.roles.value,
  };
};

/**
 * @overview Lấy thông tin của của người dùng
 *
 * @returns {Object}
 */
UserSchema.methods.getFullInfo = function () {
  return new Promise((resolve, reject) => {
    const User = mongoose.model('User');

    const _this = this.toObject();

    _this.authorizedLevel = this.getAuthorizedLevelByRoles();
    _this.language = _this.settings.general.language;
    _this.settings = undefined;
    _this.tokens = undefined;
    _this.password = undefined;
    _this.salt = undefined;

    if (_this.authorizedLevel >= User.getAuthorizedLevelByRoles(constant.ADMIN)) {
      this.getDataVerifyAccount()
        .then((admins) => {
          _this.verify.account.admin = admins;

          resolve(_this);
        })
        .catch((err) => {
          resolve(_this);
        });
    } else {
      _this.lock.admin = undefined;
      if (_this.delete.is) {
        _this.delete.reason = undefined;
      }

      resolve(_this);
    }
  })
    .then((_this) => {
      return new Promise((resolve, reject) => {
        const Credit = mongoose.model('Credit');

        Credit
          .findOne({ userID: _this._id })
          .exec((err, credit) => {
            if (credit) {
              _this.credit = credit.getFullInfo();
            }

            resolve(_this);
          });
      });
    });
};

/**
 * @overview  Lấy thông tin xác thực của người dùng
 *
 * @returns {Promise}
 */
UserSchema.methods.getDataVerifyAccount = function () {
  return new Promise((resolve, reject) => {
    const User = mongoose.model('User');
    const _this = this.toObject();

    if (_this.verify.account.status !== constant.NONE) {
      async.map(_this.verify.account.admin, (admin, callback) => {
        User
          .findById(admin._id, `-${constant.SALT} -${constant.PASSWORD}`)
          .exec((err, user) => {
            if (user) {
              callback(
                null,
                _.assignIn(
                  admin,
                  { user: user.getShortcutsInfo() }
                )
              );
            } else {
              callback(
                null,
                _.assignIn(
                  admin,
                  { user: user.getShortcutsInfoUndefined() }
                )
              );
            }
          });
      }, (err, resultAdmins) => {
        return resolve(resultAdmins);
      });
    } else {
      return reject();
    }
  });
};

/**
 * @overview  Lấy thông tin của người dùng theo cấp vai trò
 *
 * @param   {User} user
 * @returns {Promise}
 */
UserSchema.methods.getInfoByAuthorizedLevel = function (user) {
  /**
 * @overview Xoá thông tin địa chỉ người dùng
 *
 */
  const deleteAccountAddress = function (user) {
    const visibleAddress = user.settings.security.account.visible.address;

    switch (visibleAddress) {
      case constant.NONE:
        user.address = undefined;
        break;
      case constant.SHORTCUT:
        user.address.county = undefined;
        user.address.local = undefined;
        break;
      case constant.FULL:
      default:
        break;
    }
  };

  const deleteAccountBirthday = function (user) {
    const visibleBirthday = user.settings.security.account.visible.birthday;

    switch (visibleBirthday) {
      case constant.NONE:
        user.birthday = undefined;
        break;
      case constant.SHORTCUT:
        user.birthday.day = undefined;
        user.birthday.month = undefined;
        break;
      case constant.FULL:
      default:
        break;
    }
  };

  const deleteAccountContacts = function (user) {
    const visibleContact = user.settings.security.account.visible.contacts;

    switch (visibleContact) {
      case constant.NONE:
        user.numberPhone = undefined;
        break;
      case constant.SHORTCUT:
      case constant.FULL:
      default:
        break;
    }
  };

  const _this = this.toObject();

  _this.authorizedLevel = user.getAuthorizedLevel(_this.roles.value);
  _this.password = undefined;
  _this.salt = undefined;

  if (user.authorized(_this.roles.value)) {
    return new Promise((resolve, reject) => {
      this.getDataVerifyAccount()
        .then((admins) => {
          _this.verify.account.admin = admins;

          resolve(_this);
        })
        .catch((err) => {
          resolve(_this);
        });
    });
  }

  return new Promise((resolve, reject) => {
    deleteAccountContacts(_this);
    deleteAccountAddress(_this);
    deleteAccountBirthday(_this);

    const verifyAccount = _this.verify.account;
    if (verifyAccount.status !== constant.NONE) {
      verifyAccount.data = undefined;
      verifyAccount.admin = undefined;
      verifyAccount.status = undefined;

      _this.verify.account = verifyAccount;
    }

    _this.lock.admin = undefined;
    if (_this.delete.is) {
      _this.delete.reason = undefined;
    }

    _this.settings = undefined;
    _this.tokens = undefined;

    resolve(_this);
  });
};

/**
 * @overview So sánh id người dùng
 *
 * @param   {ObjectId} id
 * @returns {Boolean}
 */
UserSchema.methods.authorized = function (roles) {
  const User = mongoose.model('User');

  return this.getAuthorizedLevelByRoles() >= User.getAuthorizedLevelByRoles(constant.ADMIN)
    && this.getAuthorizedLevel(roles) === User.getAuthorizedLevelByType(constant.FULL);
};

/**
 * @overview So sánh id người dùng
 *
 * @param   {ObjectId} id
 * @returns {Boolean}
 */
UserSchema.methods.equalsByID = function (id) {
  return this._id.equals(id);
};

/**
 * @overview So sánh số điện thoại người dùng
 *
 * @param   {String|Object} numberPhone
 * @returns {Boolean}
 */
UserSchema.methods.equalsByNumberPhone = function (numberPhone) {
  const User = mongoose.model('User');

  let _numberPhone = numberPhone;

  if (_.isString(numberPhone)) {
    _numberPhone = User.decodeNumberPhone(numberPhone);
  }

  if (this.numberPhone.code === _numberPhone.code
    && this.numberPhone.number === _numberPhone.number) {
    return true;
  }

  return false;
};

/**
 * @overview Lấy thông tin cơ bản nhất của của không xác định
 *
 * @returns {Object}
 */
UserSchema.statics.getShortcutsInfoUndefined = function () {
  return {
    avatar: validates.url.avatar.undefined,
    nick: validates.name.nick.undefined,
    verifyAccount: false,
    roles: constant.USER,
  };
};

/**
 * @overview Lấy thông tin cơ bản nhất của quản trị viên
 *
 * @returns {Object}
 */
UserSchema.statics.getShortcutsInfoAdmin = function () {
  return {
    avatar: validates.url.avatar.admin,
    nick: validates.name.nick.admin,
    verifyAccount: true,
    roles: constant.ADMIN,
  };
};

/**
 * @overview Tạo mẫu cơ sở dữ liệu
 *
 * @param   {Object} doc
 * @param   {Boolean} options
 * @returns {Promise}
 */
UserSchema.statics.seed = function (doc, options) {
  const User = mongoose.model('User');

  return new Promise((resolve, reject) => {
    function skipDocument() {
      return new Promise((resolve, reject) => {
        User.findOne({ numberPhone: doc.numberPhone })
          .exec((err, existing) => {
            if (err) {
              return reject(err);
            }

            if (!existing) {
              return resolve(false);
            }

            if (existing && !options.overwrite) {
              return resolve(true);
            }

            // Remove User (overwrite)

            existing.remove((err) => {
              if (err) {
                return reject(err);
              }

              return resolve(false);
            });
          });
      });
    }

    function add(skip) {
      return new Promise((resolve, reject) => {
        if (skip) {
          return resolve({
            message: chalk.yellow(`Database Seeding: User\t\t nick: ${doc.name.nick} skipped`),
          });
        }

        const user = new User(doc);
        user.save((err) => {
          if (err) {
            return reject(err);
          }

          return resolve({
            message: `Database Seeding: User\t\t nick: ${user.name.nick} added`,
          });
        });
      });
    }

    skipDocument()
      .then(add)
      .then((response) => {
        return resolve(response);
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

/**
 * @overview Lấy giá trị mặt định để dử sụng cho Signup
 *
 * @param     {User} data
 * @returns   {Object}
 */
UserSchema.statics.getDefaultValue = function (data) {
  return {
    name: {
      first: data.name.first,
      last: data.name.last,
      display: `${data.name.last} ${data.name.first}`,
      nick: data.name.nick,
    },
    avatar: {
      url: validates.url.avatar[data.gender]
    },
    numberPhone: data.numberPhone,
    gender: data.gender,
    birthday: data.birthday,
    address: data.address,
    password: data.password,
    settings: {
      general: {
        language: data.settings.general.language,
      },
    },
  };
};

/**
 * @overview Giải mã số điện thoại từ chuỗi sang object
 *
 * @param     {String} strNumberPhone
 * @returns   {Object}
 */
UserSchema.statics.decodeNumberPhone = function (strNumberPhone) {
  return {
    code: strNumberPhone.slice(0, strNumberPhone.indexOf(' ')),
    number: strNumberPhone.slice(strNumberPhone.indexOf(' ') + 1, strNumberPhone.length),
  };
};

/**
 * @overview Lấy index vai trò người dùng
 *
 * @param     {String} roles ['user', 'provider', 'admin']
 * @returns   {Number} 0|1|2
 */
UserSchema.statics.getAuthorizedLevelByRoles = (roles) => {
  return _.indexOf(regular.user.roles, roles);
};

/**
 * @overview Lấy index quyền người dùng theo kiểu
 *
 * @param     {String} type ['none', 'view', 'full']
 * @returns   {Number}  Không được làm gì       => ['none'] => 0
 *                      Chỉ được xem            => ['view'] => 1
 *                      Xem và chỉnh sửa        => ['full'] => 2
 */
UserSchema.statics.getAuthorizedLevelByType = (type) => {
  return _.indexOf(validates.authorized, type);
};

mongoose.model('User', UserSchema);
