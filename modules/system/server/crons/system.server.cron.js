const path = require('path');
const cron = require('cron');
const async = require('async');
const mongoose = require('mongoose');
const logger = require(path.resolve('./config/lib/logger'));
const constant = require(path.resolve('./commons/utils/constant'));

const System = mongoose.model('System');

const startUpdate = () => {
  const updateParameter = () => {
    async.waterfall([
      (done) => {
        /* Tìm kiếm hệ thống */

        System.findOne({})
          .exec((err, system) => {
            if (err || system) {
              done(err, system);
            } else {
              done(null, new System());
            }
          });
      }, (system, done) => {
        /* Lấy thông số  và cập nhật hệ thống */

        async.parallel({
          users: (callback) => {
            System.getUsersParameter()
              .then((usersParameter) => {
                const {
                  summary,
                  details,
                } = usersParameter.details;

                summary.onlineDay = undefined;
                details.user.onlineDay = undefined;
                details.provider.onlineDay = undefined;
                details.admin.onlineDay = undefined;

                callback(
                  null,
                  {
                    summary,
                    details
                  }
                );
              });
          },
          businessCardDealHistorys: (callback) => {
            System.getBusinessCardDealHistorysParameter()
              .then((businessCardDealHistorysParameter) => {
                const {
                  details,
                  summary,
                } = businessCardDealHistorysParameter;

                callback(
                  null,
                  {
                    summary,
                    details
                  }
                );
              });
          },
          getBusinessCards: (callback) => {
            System.getBusinessCardsParameter()
              .then((businessCardsParameter) => {
                const { summary } = businessCardsParameter;

                callback(
                  null,
                  {
                    summary
                  }
                );
              });
          }
        }, (err, results) => {
          system.users.push(results.users);
          system.businessCardDealHistorys.push(results.businessCardDealHistorys);
          system.getBusinessCards.push(results.getBusinessCards);

          system.save((err) => {
            done(err, system);
          });
        });
      }], (err, dataRes) => {
        if (err) {
          logger.logError(
            'system.server.cron',
            'updateParameter',
            constant.SYSTEM,
            dataRes,
            err
          );
        }
      });
  };

  const onTick = () => {
    updateParameter();
  };

  const onComplete = () => {
  };

  return new cron.CronJob('0 */24 * * *', onTick, onComplete, true);
};

const init = (app) => {
  startUpdate();
};
module.exports = init;
