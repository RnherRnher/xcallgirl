const path = require('path');
const policy = require('../policies/system.server.policy');
const system = require('../controllers/system.server.controller');
const url = require(path.resolve('./commons/modules/system/datas/url.data'));
const validate = require(path.resolve('./commons/modules/system/validates/system.validate'));
const middleware = require(path.resolve('./utils/middleware'));

const init = (app) => {
  app.route(url.SYSTEM_PARAMETER)
    .all(policy.isAllowed)
    .get(middleware.paramsValidation(validate, 'getParameter'), system.getParameter);
};
module.exports = init;
