const path = require('path');
const mongoose = require('mongoose');
const constant = require(path.resolve('./commons/utils/constant'));

const System = mongoose.model('System');
const User = mongoose.model('User');

/**
 * @overview Lấy thông số người dùng
 *
 * @param {Request} req
 * @param {Response} res
 * @param {Function} next
 *
 * @input  query  { type: String }
 * @output        {
 *                  code: String,
 *                  usersParameter?: {Object},
 *                  businessCardDealHistorysParameter?: {Object},
 *                  businessCardsParameter?: {Object},
 *                }
 */
const getParameter = (req, res, next) => {
  const queryReq = req.query;
  const userSelf = req.user;
  let dataRes = null;

  if (userSelf) {
    if (userSelf.getAuthorizedLevelByRoles() === User.getAuthorizedLevelByRoles(constant.SYSTEM)) {
      // Lấy thông số người dùng

      switch (queryReq.type) {
        case constant.USER:
          System.getUsersParameter()
            .then((usersParameter) => {
              dataRes = {
                status: 200,
                bodyRes: {
                  code: 'system.usersParameter.exist',
                  usersParameter
                },
              };

              return res
                .status(dataRes.status)
                .send(dataRes.bodyRes)
                .end();
            });
          break;
        case constant.BUSINESS_CARD_DEAL_HISTORY:
          System.getBusinessCardDealHistorysParameter()
            .then((businessCardDealHistorysParameter) => {
              dataRes = {
                status: 200,
                bodyRes: {
                  code: 'system.businessCardDealHistorysParameter.exist',
                  businessCardDealHistorysParameter
                },
              };

              return res
                .status(dataRes.status)
                .send(dataRes.bodyRes)
                .end();
            });
          break;
        case constant.BUSINESS_CARD:
          System.getBusinessCardsParameter()
            .then((businessCardsParameter) => {
              dataRes = {
                status: 200,
                bodyRes: {
                  code: 'system.businessCardsParameter.exist',
                  businessCardsParameter
                },
              };

              return res
                .status(dataRes.status)
                .send(dataRes.bodyRes)
                .end();
            });
          break;
        default:
          dataRes = {
            status: 400,
            bodyRes: {
              code: 'data.notVaild',
            },
          };

          return res
            .status(dataRes.status)
            .send(dataRes.bodyRes)
            .end();
      }
    } else {
      // Người dùng không có quyền
      dataRes = {
        status: 401,
        bodyRes: {
          code: 'user.permission.notExist',
        },
      };

      return res
        .status(dataRes.status)
        .send(dataRes.bodyRes)
        .end();
    }
  } else {
    // Chưa đăng nhập người dùng
    dataRes = {
      status: 401,
      bodyRes: {
        code: 'user.signin.notExist',
      },
    };

    return res
      .status(dataRes.status)
      .send(dataRes.bodyRes)
      .end();
  }
};
module.exports.getParameter = getParameter;
