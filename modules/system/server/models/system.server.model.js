const async = require('async');
const path = require('path');
const mongoose = require('mongoose');
const constant = require(path.resolve('./commons/utils/constant'));

const { Schema } = mongoose;

const SchemaUser = {
  type: {
    count: {
      type: Number,
      required: true,
      min: 0,
      default: 0
    },
    newDay: {
      type: Number,
      required: true,
      min: 0,
      default: 0
    },
    verifyAccount: {
      type: Number,
      required: true,
      min: 0,
      default: 0
    },
    verifyAccountDay: {
      type: Number,
      required: true,
      min: 0,
      default: 0
    },
    lock: {
      type: Number,
      required: true,
      min: 0,
      default: 0
    },
    lockDay: {
      type: Number,
      required: true,
      min: 0,
      default: 0
    },
    delete: {
      type: Number,
      required: true,
      min: 0,
      default: 0
    },
    deleteDay: {
      type: Number,
      required: true,
      min: 0,
      default: 0
    },
  },
  required: true
};

const SchemaBusinessCardDealHistory = (isFull = false) => {
  const scheme = {
    type: {
      successResult: {
        type: Number,
        required: true,
        min: 0,
        default: 0
      },
      failedResult: {
        type: Number,
        required: true,
        min: 0,
        default: 0
      }
    },
    required: true
  };

  if (isFull) {
    scheme.type.waitResult = {
      type: Number,
      required: true,
      min: 0,
      default: 0
    };
  }

  return scheme;
};

const SystemSchema = new Schema({
  users: {
    type: [{
      summary: SchemaUser,
      details: {
        type: {
          user: SchemaUser,
          provider: SchemaUser,
          admin: SchemaUser,
        }
      },
      initDate: {
        type: Date,
        required: true,
        default: Date.now
      }
    }]
  },
  businessCardDealHistorys: {
    type: [{
      summary: {
        type: {
          count: {
            type: Number,
            required: true,
            min: 0,
            default: 0
          },
          newDay: {
            type: Number,
            required: true,
            min: 0,
            default: 0
          },
          report: {
            type: Number,
            required: true,
            min: 0,
            default: 0
          },
          reportDay: {
            type: Number,
            required: true,
            min: 0,
            default: 0
          },
        },
        required: true,
      },
      details: {
        type: {
          count: SchemaBusinessCardDealHistory(true),
          newDay: SchemaBusinessCardDealHistory(true),
          report: {
            type: {
              user: SchemaBusinessCardDealHistory(),
              provider: SchemaBusinessCardDealHistory()
            },
            required: true,
          },
          reportDay: {
            type: {
              user: SchemaBusinessCardDealHistory(),
              provider: SchemaBusinessCardDealHistory()
            },
            required: true,
          },
        },
        required: true,
      },
      initDate: {
        type: Date,
        required: true,
        default: Date.now
      }
    }]
  },
  businessCards: {
    type: [{
      summary: {
        type: {
          count: {
            type: Number,
            required: true,
            min: 0,
            default: 0
          },
          newDay: {
            type: Number,
            required: true,
            min: 0,
            default: 0
          },
          blurFace: {
            type: Number,
            required: true,
            min: 0,
            default: 0
          },
          gender: {
            type: {
              male: {
                type: Number,
                required: true,
                min: 0,
                default: 0
              },
              female: {
                type: Number,
                required: true,
                min: 0,
                default: 0
              },
            },
            required: true,
          },
          open: {
            type: Number,
            required: true,
            min: 0,
            default: 0
          },
          openDay: {
            type: Number,
            required: true,
            min: 0,
            default: 0
          },
          reUploadDay: {
            type: Number,
            required: true,
            min: 0,
            default: 0
          },
          star: {
            type: [{
              type: Number,
              required: true,
              min: 0,
              default: 0
            }]
          }
        },
        required: true
      }
    }]
  }
});

/**
 * @overview Lấy thông số người dùng
 *
 * @returns {Promise}
 */
SystemSchema.statics.getUsersParameter = function () {
  return new Promise((resolve, reject) => {
    const User = mongoose.model('User');

    const currentDate = new Date();
    const currentDay = new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate());

    async.parallel({
      user: (callback) => {
        User
          .countDocuments({ 'roles.value': constant.USER })
          .exec((err, count) => {
            callback(null, count);
          });
      },
      provider: (callback) => {
        User
          .countDocuments({ 'roles.value': constant.PROVIDER })
          .exec((err, count) => {
            callback(null, count);
          });
      },
      admin: (callback) => {
        User
          .countDocuments({ 'roles.value': constant.ADMIN })
          .exec((err, count) => {
            callback(null, count);
          });
      },
      newUsersDay: (callback) => {
        User
          .countDocuments({
            initDate: {
              $lte: currentDate,
              $gte: currentDay
            }
          })
          .exec((err, count) => {
            callback(null, count);
          });
      },
      newUserDay: (callback) => {
        User
          .countDocuments({
            'roles.value': constant.USER,
            'roles.initDate': {
              $lte: currentDate,
              $gte: currentDay
            }
          })
          .exec((err, count) => {
            callback(null, count);
          });
      },
      newProviderDay: (callback) => {
        User
          .countDocuments({
            'roles.value': constant.PROVIDER,
            'roles.initDate': {
              $lte: currentDate,
              $gte: currentDay
            }
          })
          .exec((err, count) => {
            callback(null, count);
          });
      },
      newAdminDay: (callback) => {
        User
          .countDocuments({
            'roles.value': constant.ADMIN,
            'roles.initDate': {
              $lte: currentDate,
              $gte: currentDay
            }
          })
          .exec((err, count) => {
            callback(null, count);
          });
      },
      onlineUserDay: (callback) => {
        User
          .countDocuments({
            'roles.value': constant.USER,
            'online.is': true,
            'roles.date': {
              $lte: currentDate,
              $gte: currentDay
            }
          })
          .exec((err, count) => {
            callback(null, count);
          });
      },
      onlineProviderDay: (callback) => {
        User
          .countDocuments({
            'roles.value': constant.PROVIDER,
            'online.is': true,
            'roles.date': {
              $lte: currentDate,
              $gte: currentDay
            }
          })
          .exec((err, count) => {
            callback(null, count);
          });
      },
      onlineAdminDay: (callback) => {
        User
          .countDocuments({
            'roles.value': constant.ADMIN,
            'online.is': true,
            'online.date': {
              $lte: currentDate,
              $gte: currentDay
            }
          })
          .exec((err, count) => {
            callback(null, count);
          });
      },
      verifyAccountUser: (callback) => {
        User
          .countDocuments({
            'roles.value': constant.USER,
            'verify.account.is': true
          })
          .exec((err, count) => {
            callback(null, count);
          });
      },
      verifyAccountProvider: (callback) => {
        User
          .countDocuments({
            'roles.value': constant.PROVIDER,
            'verify.account.is': true
          })
          .exec((err, count) => {
            callback(null, count);
          });
      },
      verifyAccountAdmin: (callback) => {
        User
          .countDocuments({
            'roles.value': constant.ADMIN,
            'verify.account.is': true
          })
          .exec((err, count) => {
            callback(null, count);
          });
      },
      verifyAccountUserDay: (callback) => {
        User
          .countDocuments({
            'roles.value': constant.USER,
            'verify.account.is': true,
            'verify.account.admin.confirmDate': {
              $lte: currentDate,
              $gte: currentDay
            }
          })
          .exec((err, count) => {
            callback(null, count);
          });
      },
      verifyAccountProviderDay: (callback) => {
        User
          .countDocuments({
            'roles.value': constant.PROVIDER,
            'verify.account.is': true,
            'verify.account.admin.confirmDate': {
              $lte: currentDate,
              $gte: currentDay
            }
          })
          .exec((err, count) => {
            callback(null, count);
          });
      },
      verifyAccountAdminDay: (callback) => {
        User
          .countDocuments({
            'roles.value': constant.ADMIN,
            'verify.account.is': true,
            'verify.account.admin.confirmDate': {
              $lte: currentDate,
              $gte: currentDay
            }
          })
          .exec((err, count) => {
            callback(null, count);
          });
      },
      lockUser: (callback) => {
        User
          .countDocuments({
            'roles.value': constant.USER,
            'lock.is': true
          })
          .exec((err, count) => {
            callback(null, count);
          });
      },
      lockProvider: (callback) => {
        User
          .countDocuments({
            'roles.value': constant.PROVIDER,
            'lock.is': true
          })
          .exec((err, count) => {
            callback(null, count);
          });
      },
      lockAdmin: (callback) => {
        User
          .countDocuments({
            'roles.value': constant.ADMIN,
            'lock.is': true
          })
          .exec((err, count) => {
            callback(null, count);
          });
      },
      lockUserDay: (callback) => {
        User
          .countDocuments({
            'roles.value': constant.USER,
            'lock.is': true,
            'lock.initDate': {
              $lte: currentDate,
              $gte: currentDay
            }
          })
          .exec((err, count) => {
            callback(null, count);
          });
      },
      lockProviderDay: (callback) => {
        User
          .countDocuments({
            'roles.value': constant.PROVIDER,
            'lock.is': true,
            'lock.initDate': {
              $lte: currentDate,
              $gte: currentDay
            }
          })
          .exec((err, count) => {
            callback(null, count);
          });
      },
      lockAdminDay: (callback) => {
        User
          .countDocuments({
            'roles.value': constant.ADMIN,
            'lock.is': true,
            'lock.initDate': {
              $lte: currentDate,
              $gte: currentDay
            }
          })
          .exec((err, count) => {
            callback(null, count);
          });
      },
      deleteUser: (callback) => {
        User
          .countDocuments({
            'roles.value': constant.USER,
            'delete.is': true
          })
          .exec((err, count) => {
            callback(null, count);
          });
      },
      deleteProvider: (callback) => {
        User
          .countDocuments({
            'roles.value': constant.PROVIDER,
            'delete.is': true
          })
          .exec((err, count) => {
            callback(null, count);
          });
      },
      deleteAdmin: (callback) => {
        User
          .countDocuments({
            'roles.value': constant.ADMIN,
            'delete.is': true
          })
          .exec((err, count) => {
            callback(null, count);
          });
      },
      deleteUserDay: (callback) => {
        User
          .countDocuments({
            'roles.value': constant.USER,
            'delete.is': true,
            'delete.initDate': {
              $lte: currentDate,
              $gte: currentDay
            }
          })
          .exec((err, count) => {
            callback(null, count);
          });
      },
      deleteProviderDay: (callback) => {
        User
          .countDocuments({
            'roles.value': constant.PROVIDER,
            'delete.is': true,
            'delete.initDate': {
              $lte: currentDate,
              $gte: currentDay
            }
          })
          .exec((err, count) => {
            callback(null, count);
          });
      },
      deleteAdminDay: (callback) => {
        User
          .countDocuments({
            'roles.value': constant.ADMIN,
            'delete.is': true,
            'delete.initDate': {
              $lte: currentDate,
              $gte: currentDay
            }
          })
          .exec((err, count) => {
            callback(null, count);
          });
      }
    }, (err, results) => {
      resolve({
        summary: {
          count: results.user + results.provider + results.admin,
          newDay: results.newUsersDay,
          onlineDay: results.onlineUserDay + results.onlineProviderDay + results.onlineAdminDay,
          verifyAccount: results.verifyAccountUser + results.verifyAccountProvider + results.verifyAccountAdmin,
          verifyAccountDay: results.verifyAccountUserDay + results.verifyAccountProviderDay + results.verifyAccountAdminDay,
          lock: results.lockUser + results.lockProvider + results.lockAdmin,
          lockDay: results.lockUserDay + results.lockProviderDay + results.lockAdminDay,
          delete: results.deleteUser + results.deleteProvider + results.deleteAdmin,
          deleteDay: results.deleteUserDay + results.deleteProviderDay + results.deleteAdminDay,
        },
        details: {
          user: {
            count: results.user,
            newDay: results.newUserDay,
            onlineDay: results.onlineUserDay,
            verifyAccount: results.verifyAccountUser,
            verifyAccountDay: results.verifyAccountUserDay,
            lock: results.lockUser,
            lockDay: results.lockUserDay,
            delete: results.deleteUser,
            deleteDay: results.deleteUserDay,
          },
          provider: {
            count: results.provider,
            newDay: results.newProviderDay,
            onlineDay: results.onlineProviderDay,
            verifyAccount: results.verifyAccountProvider,
            verifyAccountDay: results.verifyAccountProviderDay,
            lock: results.lockProvider,
            lockDay: results.lockProviderDay,
            delete: results.deleteProvider,
            deleteDay: results.deleteProviderDay,
          },
          admin: {
            count: results.admin,
            newDay: results.newAdminDay,
            onlineDay: results.onlineAdminDay,
            verifyAccount: results.verifyAccountAdmin,
            verifyAccountDay: results.verifyAccountAdminDay,
            lock: results.lockAdmin,
            lockDay: results.lockAdminDay,
            delete: results.deleteAdmin,
            deleteDay: results.deleteAdminDay,
          }
        }
      });
    });
  });
};

/**
 * @overview Lấy thông số  lịch sử giao dịch
 *
 * @returns {Promise}
 */
SystemSchema.statics.getBusinessCardDealHistorysParameter = function () {
  return new Promise((resolve, reject) => {
    const BusinessCardDealHistory = mongoose.model('BusinessCardDealHistory');

    const currentDate = new Date();
    const currentDay = new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate());

    async.parallel({
      waitResult: (callback) => {
        BusinessCardDealHistory
          .countDocuments({ 'result.is': constant.WAIT })
          .exec((err, count) => {
            callback(null, count);
          });
      },
      successResult: (callback) => {
        BusinessCardDealHistory
          .countDocuments({ 'result.is': constant.SUCCESS })
          .exec((err, count) => {
            callback(null, count);
          });
      },
      failedResult: (callback) => {
        BusinessCardDealHistory
          .countDocuments({ 'result.is': constant.FAILED })
          .exec((err, count) => {
            callback(null, count);
          });
      },
      waitResultDay: (callback) => {
        BusinessCardDealHistory
          .countDocuments({
            'result.is': constant.WAIT,
            initDate: {
              $lte: currentDate,
              $gte: currentDay
            }
          })
          .exec((err, count) => {
            callback(null, count);
          });
      },
      successResultDay: (callback) => {
        BusinessCardDealHistory
          .countDocuments({
            'result.is': constant.SUCCESS,
            initDate: {
              $lte: currentDate,
              $gte: currentDay
            }
          })
          .exec((err, count) => {
            callback(null, count);
          });
      },
      failedResultDay: (callback) => {
        BusinessCardDealHistory
          .countDocuments({
            'result.is': constant.FAILED,
            initDate: {
              $lte: currentDate,
              $gte: currentDay
            }
          })
          .exec((err, count) => {
            callback(null, count);
          });
      },
      successResult_ReportUser: (callback) => {
        BusinessCardDealHistory
          .countDocuments({
            'result.is': constant.SUCCESS,
            'reports.user.is': true
          })
          .exec((err, count) => {
            callback(null, count);
          });
      },
      failedResult_ReportUser: (callback) => {
        BusinessCardDealHistory
          .countDocuments({
            'result.is': constant.FAILED,
            'reports.user.is': true
          })
          .exec((err, count) => {
            callback(null, count);
          });
      },
      successResult_ReportUserDay: (callback) => {
        BusinessCardDealHistory
          .countDocuments({
            'result.is': constant.SUCCESS,
            'reports.user.is': true,
            initDate: {
              $lte: currentDate,
              $gte: currentDay
            }
          })
          .exec((err, count) => {
            callback(null, count);
          });
      },
      failedResult_ReportUserDay: (callback) => {
        BusinessCardDealHistory
          .countDocuments({
            'result.is': constant.FAILED,
            'reports.user.is': true,
            initDate: {
              $lte: currentDate,
              $gte: currentDay
            }
          })
          .exec((err, count) => {
            callback(null, count);
          });
      },
      successResult_ReportProvider: (callback) => {
        BusinessCardDealHistory
          .countDocuments({
            'result.is': constant.SUCCESS,
            'reports.provider.is': true
          })
          .exec((err, count) => {
            callback(null, count);
          });
      },
      failedResult_ReportProvider: (callback) => {
        BusinessCardDealHistory
          .countDocuments({
            'result.is': constant.FAILED,
            'reports.provider.is': true
          })
          .exec((err, count) => {
            callback(null, count);
          });
      },
      successResult_ReportProviderDay: (callback) => {
        BusinessCardDealHistory
          .countDocuments({
            'result.is': constant.SUCCESS,
            'reports.provider.is': true,
            initDate: {
              $lte: currentDate,
              $gte: currentDay
            }
          })
          .exec((err, count) => {
            callback(null, count);
          });
      },
      failedResult_ReportProviderDay: (callback) => {
        BusinessCardDealHistory
          .countDocuments({
            'result.is': constant.FAILED,
            'reports.provider.is': true,
            initDate: {
              $lte: currentDate,
              $gte: currentDay
            }
          })
          .exec((err, count) => {
            callback(null, count);
          });
      },
    }, (err, results) => {
      resolve({
        summary: {
          count: results.waitResult + results.successResult + results.failedResult,
          newDay: results.waitResultDay + results.successResultDay + results.failedResultDay,
          report:
            results.successResult_ReportUser
            + results.failedResult_ReportUser
            + results.successResult_ReportProvider
            + results.failedResult_ReportProvider,
          reportDay:
            results.successResult_ReportUserDay
            + results.failedResult_ReportUserDay
            + results.successResult_ReportProviderDay
            + results.failedResult_ReportProviderDay,
        },
        details: {
          count: {
            waitResult: results.waitResult,
            successResult: results.successResult,
            failedResult: results.failedResult,
          },
          newDay: {
            waitResult: results.waitResultDay,
            successResult: results.successResultDay,
            failedResult: results.failedResultDay,
          },
          report: {
            user: {
              successResult: results.successResult_ReportUser,
              failedResult: results.failedResult_ReportUser,
            },
            provider: {
              successResult: results.successResult_ReportProvider,
              failedResult: results.failedResult_ReportProvider,
            }
          },
          reportDay: {
            user: {
              successResult: results.successResult_ReportUserDay,
              failedResult: results.failedResult_ReportUserDay,
            },
            provider: {
              successResult: results.successResult_ReportProviderDay,
              failedResult: results.failedResult_ReportProviderDay,
            }
          }
        }
      });
    });
  });
};

/**
 * @overview Lấy thông số  danh thiếp
 *
 * @returns {Promise}
 */
SystemSchema.statics.getBusinessCardsParameter = function () {
  return new Promise((resolve, reject) => {
    const BusinessCard = mongoose.model('BusinessCard');

    const currentDate = new Date();
    const currentDay = new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate());

    async.parallel({
      star0: (callback) => {
        BusinessCard
          .countDocuments({ points: BusinessCard.getQueryPoints(0) })
          .exec((err, count) => {
            callback(null, count);
          });
      },
      star05: (callback) => {
        BusinessCard
          .countDocuments({ points: BusinessCard.getQueryPoints(0.5) })
          .exec((err, count) => {
            callback(null, count);
          });
      },
      star1: (callback) => {
        BusinessCard
          .countDocuments({ points: BusinessCard.getQueryPoints(1) })
          .exec((err, count) => {
            callback(null, count);
          });
      },
      star15: (callback) => {
        BusinessCard
          .countDocuments({ points: BusinessCard.getQueryPoints(1.5) })

          .exec((err, count) => {
            callback(null, count);
          });
      },
      star2: (callback) => {
        BusinessCard
          .countDocuments({ points: BusinessCard.getQueryPoints(2) })
          .exec((err, count) => {
            callback(null, count);
          });
      },
      star25: (callback) => {
        BusinessCard
          .countDocuments({ points: BusinessCard.getQueryPoints(2.5) })
          .exec((err, count) => {
            callback(null, count);
          });
      },
      star3: (callback) => {
        BusinessCard
          .countDocuments({ points: BusinessCard.getQueryPoints(3) })
          .exec((err, count) => {
            callback(null, count);
          });
      },
      star35: (callback) => {
        BusinessCard
          .countDocuments({ points: BusinessCard.getQueryPoints(3.5) })
          .exec((err, count) => {
            callback(null, count);
          });
      },
      star4: (callback) => {
        BusinessCard
          .countDocuments({ points: BusinessCard.getQueryPoints(4) })
          .exec((err, count) => {
            callback(null, count);
          });
      },
      star45: (callback) => {
        BusinessCard
          .countDocuments({ points: BusinessCard.getQueryPoints(4.5) })
          .exec((err, count) => {
            callback(null, count);
          });
      },
      star5: (callback) => {
        BusinessCard
          .countDocuments({ points: BusinessCard.getQueryPoints(5) })
          .exec((err, count) => {
            callback(null, count);
          });
      },
      male: (callback) => {
        BusinessCard
          .countDocuments({ points: constant.MALE })
          .exec((err, count) => {
            callback(null, count);
          });
      },
      female: (callback) => {
        BusinessCard
          .countDocuments({ gender: constant.FEMALE })
          .exec((err, count) => {
            callback(null, count);
          });
      },
      count: (callback) => {
        BusinessCard
          .countDocuments({})
          .exec((err, count) => {
            callback(null, count);
          });
      },
      newDay: (callback) => {
        BusinessCard
          .countDocuments({
            initDate: {
              $lte: currentDate,
              $gte: currentDay
            }
          })
          .exec((err, count) => {
            callback(null, count);
          });
      },
      open: (callback) => {
        BusinessCard
          .countDocuments({ 'status.is': true })
          .exec((err, count) => {
            callback(null, count);
          });
      },
      openDay: (callback) => {
        BusinessCard
          .countDocuments({
            'status.is': true,
            date: {
              $lte: currentDate,
              $gte: currentDay
            }
          })
          .exec((err, count) => {
            callback(null, count);
          });
      },
      blurFace: (callback) => {
        BusinessCard
          .countDocuments({ blurFace: true })
          .exec((err, count) => {
            callback(null, count);
          });
      },
      reUploadDay: (callback) => {
        BusinessCard
          .countDocuments({
            'reUpload.is': true,
            date: {
              $lte: currentDate,
              $gte: currentDay
            }
          })
          .exec((err, count) => {
            callback(null, count);
          });
      }
    }, (err, results) => {
      resolve({
        summary: {
          count: results.count,
          newDay: results.newDay,
          blurFace: results.blurFace,
          gender: {
            male: results.male,
            female: results.female,
          },
          open: results.open,
          openDay: results.openDay,
          reUploadDay: results.reUploadDay,
          star: [
            results.star0,
            results.star05,
            results.star1,
            results.star15,
            results.star2,
            results.star25,
            results.star3,
            results.star35,
            results.star4,
            results.star45,
            results.star5,
          ]
        }
      });
    });
  });
};

mongoose.model('System', SystemSchema);
