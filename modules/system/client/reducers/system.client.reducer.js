const constant = require('../constants/system.client.constant');
const constantSystemUser = require('../../../users/client/constants/system.client.constant');
const { responseState } = require('../../../core/client/utils/middleware');
const { ResultEnum } = require('../../../core/client/constants/core.client.constant');
const { fromJS } = require('immutable');

const initialState = fromJS({
  usersParameter: responseState(),
  businessCardDealHistorysParameter: responseState(),
  businessCardsParameter: responseState(),
  actions: {
    roles: responseState(),
  }
});

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case constant.GET_SYSTEM_USERS_PARAMETER_REQUEST: {
      return state.set(
        'usersParameter',
        { result: ResultEnum.wait }
      );
    }
    case constant.GET_SYSTEM_USERS_PARAMETER_SUCCESS: {
      return state.set(
        'usersParameter',
        {
          result: ResultEnum.success,
          data: action.usersParameter
        }
      );
    }
    case constant.GET_SYSTEM_USERS_PARAMETER_FAILED: {
      return state.set(
        'usersParameter',
        { result: ResultEnum.failed }
      );
    }
    case constant.GET_SYSTEM_BUSINESS_CARD_DEAL_HISTORYS_PARAMETER_REQUEST: {
      return state.set(
        'businessCardDealHistorysParameter',
        { result: ResultEnum.wait }
      );
    }
    case constant.GET_SYSTEM_BUSINESS_CARD_DEAL_HISTORYS_PARAMETER_SUCCESS: {
      return state.set(
        'businessCardDealHistorysParameter',
        {
          result: ResultEnum.success,
          data: action.businessCardDealHistorysParameter
        }
      );
    }
    case constant.GET_SYSTEM_BUSINESS_CARD_DEAL_HISTORYS_PARAMETER_FAILED: {
      return state.set(
        'businessCardDealHistorysParameter',
        { result: ResultEnum.failed }
      );
    }
    case constant.GET_SYSTEM_BUSINESS_CARDS_PARAMETER_REQUEST: {
      return state.set(
        'businessCardsParameter',
        { result: ResultEnum.wait }
      );
    }
    case constant.GET_SYSTEM_BUSINESS_CARDS_PARAMETER_SUCCESS: {
      return state.set(
        'businessCardsParameter',
        {
          result: ResultEnum.success,
          data: action.businessCardsParameter
        }
      );
    }
    case constant.GET_SYSTEM_BUSINESS_CARDS_PARAMETER_FAILED: {
      return state.set(
        'businessCardsParameter',
        { result: ResultEnum.failed }
      );
    }
    case constantSystemUser.CHANGE_ROLES_ACCOUNT_REQUEST: {
      return state.setIn(
        ['actions', 'roles'],
        { result: ResultEnum.wait }
      );
    }
    case constantSystemUser.CHANGE_ROLES_ACCOUNT_SUCCESS: {
      return state.setIn(
        ['actions', 'roles'],
        {
          result: ResultEnum.success,
          code: action.code
        }
      );
    }
    case constantSystemUser.CHANGE_ROLES_ACCOUNT_FAILED: {
      return state.setIn(
        ['actions', 'roles'],
        {
          result: ResultEnum.failed,
          code: action.code
        }
      );
    }
    case constant.RESERT_SYSTEM_STORE: {
      if (action.fragment) {
        return state.set(
          action.fragment,
          initialState.get(action.fragment)
        );
      }

      return initialState;
    }
    default:
      return state;
  }
};
module.exports = reducer;
