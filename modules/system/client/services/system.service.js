const request = require('superagent');
const url = require('../../../../commons/modules/system/datas/url.data');

const getSystemParameter = ({ type }) => {
  return new Promise((resovle, reject) => {
    request
      .get(url.SYSTEM_PARAMETER)
      .query({ type })
      .accept('application/json')
      .set('X-Requested-With', 'XMLHttpRequest')
      .end((err, res) => {
        if (err) {
          return reject(res);
        }

        return resovle(res);
      });
  });
};
module.exports.getSystemParameter = getSystemParameter;
