const nprogress = require('nprogress');
const api = require('../services/system.service');
const action = require('../actions/system.client.action');
const constant = require('../constants/system.client.constant');
const constantCommon = require('../../../../commons/utils/constant');
const {
  takeLatest,
  put,
  call,
} = require('redux-saga/effects');
const { pushAlertSnackbar } = require('../../../core/client/actions/core.client.action');
const { initApp } = require('../../../core/client/actions/app.client.action');

const watchGetSystemUsersParameter = function* () {
  yield takeLatest(constant.GET_SYSTEM_USERS_PARAMETER_REQUEST, function* ({ fragment }) {
    try {
      nprogress.start();

      const response = yield call(
        api.getSystemParameter,
        { type: fragment }
      );

      yield put(action.getSystemUsersParameterSuccess(response.body));
    } catch (error) {
      if (error && error.body) {
        yield put(action.getSystemUsersParameterFailed(error.body));
        yield put(pushAlertSnackbar({
          variant: constantCommon.ERROR,
          code: error.body.code
        }));
      } else {
        yield put(initApp());
      }
    } finally {
      nprogress.done();
    }
  });
};
module.exports.watchGetSystemUsersParameter = watchGetSystemUsersParameter;

const watchGetSystemBusinessCardDealHistorysParameter = function* () {
  yield takeLatest(constant.GET_SYSTEM_BUSINESS_CARD_DEAL_HISTORYS_PARAMETER_REQUEST, function* ({ fragment }) {
    try {
      nprogress.start();

      const response = yield call(
        api.getSystemParameter,
        { type: fragment }
      );

      yield put(action.getSystemBusinessCardDealHistorysParameterSuccess(response.body));
    } catch (error) {
      if (error && error.body) {
        yield put(action.getSystemBusinessCardDealHistorysParameterFailed(error.body));
        yield put(pushAlertSnackbar({
          variant: constantCommon.ERROR,
          code: error.body.code
        }));
      } else {
        yield put(initApp());
      }
    } finally {
      nprogress.done();
    }
  });
};
module.exports.watchGetSystemBusinessCardDealHistorysParameter = watchGetSystemBusinessCardDealHistorysParameter;

const watchGetSystemBusinessCardsParameter = function* () {
  yield takeLatest(constant.GET_SYSTEM_BUSINESS_CARDS_PARAMETER_REQUEST, function* ({ fragment }) {
    try {
      nprogress.start();

      const response = yield call(
        api.getSystemParameter,
        { type: fragment }
      );

      yield put(action.getSystemBusinessCardsParameterSuccess(response.body));
    } catch (error) {
      if (error && error.body) {
        yield put(action.getSystemBusinessCardsParameterFailed(error.body));
        yield put(pushAlertSnackbar({
          variant: constantCommon.ERROR,
          code: error.body.code
        }));
      } else {
        yield put(initApp());
      }
    } finally {
      nprogress.done();
    }
  });
};
module.exports.watchGetSystemBusinessCardsParameter = watchGetSystemBusinessCardsParameter;
