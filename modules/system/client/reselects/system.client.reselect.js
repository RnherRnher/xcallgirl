const { createSelector } = require('reselect');
const { makeSelect } = require('../../../core/client/utils/middleware');

const selectSystem = (state, props) => {
  return state.get('system');
};

const makeSelectUsersParameter = () => {
  return createSelector(
    selectSystem,
    (systemState) => {
      return makeSelect(systemState.get('usersParameter'));
    }
  );
};
module.exports.makeSelectUsersParameter = makeSelectUsersParameter;

const makeSelectBusinessCardDealHistorysParameter = () => {
  return createSelector(
    selectSystem,
    (systemState) => {
      return makeSelect(systemState.get('businessCardDealHistorysParameter'));
    }
  );
};
module.exports.makeSelectBusinessCardDealHistorysParameter = makeSelectBusinessCardDealHistorysParameter;

const makeSelectBusinessCardsParameter = () => {
  return createSelector(
    selectSystem,
    (systemState) => {
      return makeSelect(systemState.get('businessCardsParameter'));
    }
  );
};
module.exports.makeSelectBusinessCardsParameter = makeSelectBusinessCardsParameter;

const makeSelectActions = (type) => {
  return createSelector(
    selectSystem,
    (systemState) => {
      return makeSelect(systemState.getIn(['actions', type]));
    }
  );
};
module.exports.makeSelectActions = makeSelectActions;
