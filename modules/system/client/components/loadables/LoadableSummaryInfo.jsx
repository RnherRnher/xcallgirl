const React = require('react');
const propTypes = require('prop-types');
const classNames = require('classnames');
const ContentLoader = require('react-content-loader').default;

const LoadableSummaryInfo = class LoadableSummaryInfo extends React.Component {
  constructor(props) {
    super(props);
  }

  initElement() {
    const { className } = this.props;

    return (
      <div className={classNames('xcg-loadable-summary-info', className)}>
        <ContentLoader
          height={450}
          width={350}
          speed={2}
          primaryColor="#666666"
          secondaryColor="#737373"
        >
          <rect x="20" y="10" rx="5" ry="5" width="310" height="60" />
          <rect x="20" y="80" rx="5" ry="5" width="310" height="60" />
          <rect x="20" y="150" rx="5" ry="5" width="310" height="60" />
          <rect x="20" y="220" rx="5" ry="5" width="310" height="60" />
          <rect x="20" y="290" rx="5" ry="5" width="310" height="60" />
          <rect x="20" y="360" rx="5" ry="5" width="310" height="60" />
        </ContentLoader>
      </div>
    );
  }

  render() {
    return this.initElement();
  }
};

LoadableSummaryInfo.propTypes = {
  className: propTypes.string,
};

LoadableSummaryInfo.defaultProps = {
  className: '',
};

module.exports = LoadableSummaryInfo;
