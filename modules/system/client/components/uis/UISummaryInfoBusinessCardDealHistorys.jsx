const React = require('react');
const propTypes = require('prop-types');
const classNames = require('classnames');
const { FormattedMessage, FormattedNumber } = require('react-intl');
const {
  Gavel,
  Flag,
  ExpandMore,
  Info,
  Error,
  CheckCircle,
  Store,
  AccountCircle
} = require('@material-ui/icons');
const {
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  ExpansionPanel,
  ExpansionPanelSummary,
  ExpansionPanelDetails,
  ListSubheader
} = require('@material-ui/core');

const UISummaryInfoBusinessCardDealHistorys = class UISummaryInfoBusinessCardDealHistorys extends React.Component {
  constructor(props) {
    super(props);
  }

  Row(icon, label, value, total) {
    return (
      <ListItem dense divider>
        <ListItemIcon>
          {icon}
        </ListItemIcon>
        <ListItemText
          inset
          primary={label}
          secondary={
            <span>
              {value} / <FormattedMessage id="info.time.day" />
            </span>
          }
        />
        {
          total
            ? <ListItemIcon>
              {total}
            </ListItemIcon>
            : null
        }
      </ListItem>
    );
  }

  ExpansionPanel(list, icon, label, value, total) {
    return (
      <ExpansionPanel>
        <ExpansionPanelSummary expandIcon={<ExpandMore />}>
          {this.Row(icon, label, value, total)}
        </ExpansionPanelSummary>
        <ExpansionPanelDetails>
          {list}
        </ExpansionPanelDetails>
      </ExpansionPanel>
    );
  }

  initElement() {
    const { className, value } = this.props;

    return (
      <div className={classNames('xcg-ui-summary-info-business-card-deal-historys', className)}>
        {
          this.ExpansionPanel(
            <List className="xcg-info-group" dense>
              {
                this.Row(
                  <Error className="xcg-error" />,
                  <FormattedMessage id="info.result.failed" />,
                  <FormattedNumber value={value.details.newDay.failedResult} />,
                  <FormattedNumber value={value.details.count.failedResult} />
                )
              }
              {
                this.Row(
                  <Info className="xcg-info" />,
                  <FormattedMessage id="info.result.wait" />,
                  <FormattedNumber value={value.details.newDay.waitResult} />,
                  <FormattedNumber value={value.details.count.waitResult} />
                )
              }
              {
                this.Row(
                  <CheckCircle className="xcg-success" />,
                  <FormattedMessage id="info.result.success" />,
                  <FormattedNumber value={value.details.newDay.successResult} />,
                  <FormattedNumber value={value.details.count.successResult} />
                )
              }
            </List>,
            <Gavel />,
            <FormattedMessage id="businessCardDealHistory.is" />,
            <FormattedNumber value={value.summary.newDay} />,
            <FormattedNumber value={value.summary.count} />
          )
        }
        {
          this.ExpansionPanel(
            <div className="xcg-info-group">
              <List
                dense
                subheader={
                  <ListSubheader>
                    <FormattedMessage id="info.result.success" />
                  </ListSubheader>
                }
              >
                {
                  this.Row(
                    <Store />,
                    <FormattedMessage id="user.roles.provider" />,
                    <FormattedNumber value={value.details.reportDay.provider.successResult} />,
                    <FormattedNumber value={value.details.report.provider.successResult} />
                  )
                }
                {
                  this.Row(
                    <AccountCircle />,
                    <FormattedMessage id="user.roles.user" />,
                    <FormattedNumber value={value.details.reportDay.user.successResult} />,
                    <FormattedNumber value={value.details.report.user.successResult} />
                  )
                }
              </List>
              <List
                dense
                subheader={
                  <ListSubheader>
                    <FormattedMessage id="info.result.failed" />
                  </ListSubheader>
                }
              >
                {
                  this.Row(
                    <Store />,
                    <FormattedMessage id="user.roles.provider" />,
                    <FormattedNumber value={value.details.reportDay.provider.failedResult} />,
                    <FormattedNumber value={value.details.report.provider.failedResult} />
                  )
                }
                {
                  this.Row(
                    <AccountCircle />,
                    <FormattedMessage id="user.roles.user" />,
                    <FormattedNumber value={value.details.reportDay.user.failedResult} />,
                    <FormattedNumber value={value.details.report.user.failedResult} />
                  )
                }
              </List>
            </div>,
            <Flag />,
            <FormattedMessage id="businessCardDealHistory.report.is" />,
            <FormattedNumber value={value.summary.reportDay} />,
            <FormattedNumber value={value.summary.report} />
          )
        }
      </div>
    );
  }

  render() {
    return this.initElement();
  }
};

UISummaryInfoBusinessCardDealHistorys.propTypes = {
  className: propTypes.string,
  value: propTypes.object.isRequired,
};

UISummaryInfoBusinessCardDealHistorys.defaultProps = {
  className: '',
};

module.exports = UISummaryInfoBusinessCardDealHistorys;
