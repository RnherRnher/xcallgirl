const React = require('react');
const propTypes = require('prop-types');
const classNames = require('classnames');
const { FormattedMessage, FormattedNumber } = require('react-intl');
const {
  Gavel,
  PlusOne,
  ExpandMore,
  RadioButtonChecked,
  Face,
  Star
} = require('@material-ui/icons');
const {
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  ExpansionPanel,
  ExpansionPanelSummary,
  ExpansionPanelDetails,
  LinearProgress
} = require('@material-ui/core');
const { UIStars } = require('../../../../core/client/components/UIs');

const UISummaryInfoBusinessCards = class UISummaryInfoBusinessCards extends React.Component {
  constructor(props) {
    super(props);
  }

  Row(icon = null, label = null, value = null, total = null, key = null) {
    return (
      <ListItem
        dense
        divider
        key={key || label}
      >
        {
          icon
            ? <ListItemIcon>
              {icon}
            </ListItemIcon>
            : null
        }
        <ListItemText
          inset
          primary={label}
          secondary={
            value
              ? <span>
                {value} / <FormattedMessage id="info.time.day" />
              </span>
              : null
          }
        />
        {
          total
            ? <ListItemIcon>
              {total}
            </ListItemIcon>
            : null
        }
      </ListItem>
    );
  }

  ExpansionPanel(list, icon = null, label = null, value = null, total = null) {
    return (
      <ExpansionPanel>
        <ExpansionPanelSummary expandIcon={<ExpandMore />}>
          {this.Row(icon, label, value, total)}
        </ExpansionPanelSummary>
        <ExpansionPanelDetails>
          {list}
        </ExpansionPanelDetails>
      </ExpansionPanel>
    );
  }

  ListUIStar() {
    const { value } = this.props;

    let currentMaxStar = 0;
    for (let index = 0; index < value.star.length; index++) {
      const count = value.star[index];

      if (currentMaxStar < count) {
        currentMaxStar = count;
      }
    }

    const ListStar = [];
    let star = 0;
    for (let index = 0; index < value.star.length; index++) {
      const count = value.star[index];

      ListStar.push(
        this.Row(
          <UIStars value={star} />,
          <LinearProgress
            color="secondary"
            variant="determinate"
            value={count * 100 / currentMaxStar}
          />,
          null,
          <FormattedNumber value={count} />,
          star
        )
      );

      star += 0.5;
    }

    return this.ExpansionPanel(
      <List className="xcg-info-group" dense>
        {ListStar}
      </List>,
      <Star />
    );
  }

  initElement() {
    const { className, value } = this.props;

    return (
      <div className={classNames('xcg-ui-summary-info-business-cards', className)}>
        {
          this.Row(
            <Gavel />,
            <FormattedMessage id="businessCard.is" />,
            <FormattedNumber value={value.newDay} />,
            <FormattedNumber value={value.count} />
          )
        }
        {
          this.Row(
            <RadioButtonChecked />,
            <FormattedMessage id="info.status.online" />,
            <FormattedNumber value={value.openDay} />,
            <FormattedNumber value={value.open} />
          )
        }
        {
          this.Row(
            <Face />,
            <FormattedMessage id="businessCard.image.blurFace.is" />,
            <FormattedNumber value={value.blurFace} />
          )
        }
        {
          this.Row(
            <PlusOne />,
            <FormattedMessage id="businessCard.reUpload.is" />,
            <FormattedNumber value={value.reUploadDay} />
          )
        }
        {this.ListUIStar()}
      </div>
    );
  }

  render() {
    return this.initElement();
  }
};

UISummaryInfoBusinessCards.propTypes = {
  className: propTypes.string,
  value: propTypes.object.isRequired,
};

UISummaryInfoBusinessCards.defaultProps = {
  className: '',
};

module.exports = UISummaryInfoBusinessCards;
