const React = require('react');
const propTypes = require('prop-types');
const classNames = require('classnames');
const { FormattedMessage, FormattedNumber } = require('react-intl');
const {
  People,
  PersonAdd,
  RadioButtonChecked,
  HowToReg,
  Lock,
  Delete
} = require('@material-ui/icons');
const {
  List,
  ListItem,
  ListItemIcon,
  ListItemText
} = require('@material-ui/core');

const UISummaryInfoUsers = class UISummaryInfoUsers extends React.Component {
  constructor(props) {
    super(props);
  }

  Row(icon, label, value, total) {
    return (
      <ListItem divider>
        <ListItemIcon>
          {icon}
        </ListItemIcon>
        <ListItemText
          inset
          primary={label}
          secondary={
            <span>
              {value} / <FormattedMessage id="info.time.day" />
            </span>
          }
        />
        {
          total
            ? <ListItemIcon>
              {total}
            </ListItemIcon>
            : null
        }
      </ListItem>
    );
  }

  initElement() {
    const { className, value } = this.props;

    return (
      <div className={classNames('xcg-ui-summary-info-users', className)}>
        <List>
          {
            this.Row(
              <People />,
              <FormattedMessage id="user.is" />,
              <FormattedNumber value={value.count} />
            )
          }
          {
            this.Row(
              <RadioButtonChecked />,
              <FormattedMessage id="user.online.exist" />,
              <FormattedNumber value={value.onlineDay} />
            )
          }
          {
            this.Row(
              <PersonAdd />,
              <FormattedMessage id="user.signup.exist" />,
              <FormattedNumber value={value.newDay} />
            )
          }
          {
            this.Row(
              <HowToReg />,
              <FormattedMessage id="user.verify.account.exist" />,
              <FormattedNumber value={value.verifyAccountDay} />,
              <FormattedNumber value={value.verifyAccount} />
            )
          }
          {
            this.Row(
              <Lock />,
              <FormattedMessage id="user.lock.exist" />,
              <FormattedNumber value={value.lockDay} />,
              <FormattedNumber value={value.lock} />
            )
          }
          {
            this.Row(
              <Delete />,
              <FormattedMessage id="user.delete.exist" />,
              <FormattedNumber value={value.deleteDay} />,
              <FormattedNumber value={value.delete} />
            )
          }
        </List>
      </div>
    );
  }

  render() {
    return this.initElement();
  }
};

UISummaryInfoUsers.propTypes = {
  className: propTypes.string,
  value: propTypes.object.isRequired,
};

UISummaryInfoUsers.defaultProps = {
  className: '',
};

module.exports = UISummaryInfoUsers;
