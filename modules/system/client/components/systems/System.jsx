const React = require('react');
const propTypes = require('prop-types');
const classNames = require('classnames');
const { connect } = require('react-redux');
const { createStructuredSelector } = require('reselect');
const { FormattedMessage } = require('react-intl');
const {
  Gavel,
  AccountCircle,
  ContactMail
} = require('@material-ui/icons');
const {
  AppBar,
  Tabs,
  Tab
} = require('@material-ui/core');
const { Loadable } = require('../../../../core/client/components/Loadables');
const { LoadableSummaryInfo } = require('../Loadables');
const { ContainerNotData } = require('../../../../core/client/components/Containers');
const {
  makeSelectUsersParameter,
  makeSelectBusinessCardDealHistorysParameter,
  makeSelectBusinessCardsParameter
} = require('../../reselects/system.client.reselect');
const {
  getSystemUsersParameterRequest,
  getSystemBusinessCardDealHistorysParameterRequest,
  getSystemBusinessCardsParameterRequest,
  resetSystemStore
} = require('../../actions/system.client.action');
const {
  UISummaryInfoUsers,
  UISummaryInfoBusinessCardDealHistorys,
  UISummaryInfoBusinessCards
} = require('../UIs');

const System = class System extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      index: 0,
    };

    this.handleChange = this.handleChange.bind(this);
  }

  componentDidMount() {
    const {
      getSystemUsersParameterRequest,
      getSystemBusinessCardDealHistorysParameterRequest,
      getSystemBusinessCardsParameterRequest
    } = this.props;

    getSystemUsersParameterRequest();
    getSystemBusinessCardDealHistorysParameterRequest();
    getSystemBusinessCardsParameterRequest();
  }

  componentWillUnmount() {
    const { resetSystemStore } = this.props;

    resetSystemStore('usersParameter');
    resetSystemStore('businessCardDealHistorysParameter');
    resetSystemStore('businessCardsParameter');
  }

  getTabContent() {
    const {
      usersParameter,
      businessCardDealHistorysParameter,
      businessCardsParameter
    } = this.props;
    const { index } = this.state;

    let TabContent = null;
    switch (index) {
      case 0:
        TabContent = <Loadable
          result={usersParameter.result}
          WaitNode={<LoadableSummaryInfo />}
          SuccessNode={
            <UISummaryInfoUsers
              value={
                usersParameter.data
                  ? usersParameter.data.summary
                  : {}
              }
            />
          }
          FailedNode={
            <div>
              <ContainerNotData />
              <LoadableSummaryInfo />
            </div>
          }
        />;
        break;
      case 1:
        TabContent = <Loadable
          result={businessCardsParameter.result}
          WaitNode={<LoadableSummaryInfo />}
          SuccessNode={
            <UISummaryInfoBusinessCards
              value={
                businessCardsParameter.data
                  ? businessCardsParameter.data.summary
                  : {}
              }
            />
          }
          FailedNode={
            <div>
              <ContainerNotData />
              <LoadableSummaryInfo />
            </div>
          }
        />;
        break;
      case 2:
        TabContent = <Loadable
          result={businessCardDealHistorysParameter.result}
          WaitNode={<LoadableSummaryInfo />}
          SuccessNode={
            <UISummaryInfoBusinessCardDealHistorys
              value={
                businessCardDealHistorysParameter.data
                  ? businessCardDealHistorysParameter.data
                  : {}
              }
            />
          }
          FailedNode={
            <div>
              <ContainerNotData />
              <LoadableSummaryInfo />
            </div>
          }
        />;
        break;
      default:
        break;
    }

    return TabContent;
  }

  handleChange(event, index) {
    this.setState({ index });
  }

  initElement() {
    const { className } = this.props;
    const { index } = this.state;

    return (
      <div className={classNames('xcg-system', className)}>
        <AppBar className="xcg-app-bar" position="fixed">
          <Tabs
            value={index}
            onChange={this.handleChange}
          >
            <Tab
              label={<FormattedMessage id="user.is" />}
              icon={<AccountCircle />}
            />
            <Tab
              label={<FormattedMessage id="businessCard.is" />}
              icon={<ContactMail />}
            />
            <Tab
              label={<FormattedMessage id="businessCardDealHistory.is" />}
              icon={<Gavel />}
            />
          </Tabs>
        </AppBar>
        {this.getTabContent()}
      </div>
    );
  }

  render() {
    return this.initElement();
  }
};

System.propTypes = {
  className: propTypes.string,
  usersParameter: propTypes.object.isRequired,
  businessCardDealHistorysParameter: propTypes.object.isRequired,
  businessCardsParameter: propTypes.object.isRequired,
  getSystemUsersParameterRequest: propTypes.func.isRequired,
  getSystemBusinessCardDealHistorysParameterRequest: propTypes.func.isRequired,
  getSystemBusinessCardsParameterRequest: propTypes.func.isRequired,
  resetSystemStore: propTypes.func.isRequired,
};

System.defaultProps = {
  className: ''
};

function mapStateToProps(state, props) {
  return createStructuredSelector({
    usersParameter: makeSelectUsersParameter(),
    businessCardDealHistorysParameter: makeSelectBusinessCardDealHistorysParameter(),
    businessCardsParameter: makeSelectBusinessCardsParameter()
  });
}

function mapDispatchToProps(dispatch, props) {
  return {
    getSystemUsersParameterRequest: () => {
      return dispatch(getSystemUsersParameterRequest());
    },
    getSystemBusinessCardDealHistorysParameterRequest: () => {
      return dispatch(getSystemBusinessCardDealHistorysParameterRequest());
    },
    getSystemBusinessCardsParameterRequest: () => {
      return dispatch(getSystemBusinessCardsParameterRequest());
    },
    resetSystemStore: (fragment) => {
      return dispatch(resetSystemStore(fragment));
    },
  };
}

module.exports = connect(
  mapStateToProps,
  mapDispatchToProps,
)(System);
