const UISummaryInfoBusinessCardDealHistorys = require('./uis/UISummaryInfoBusinessCardDealHistorys');
const UISummaryInfoBusinessCards = require('./uis/UISummaryInfoBusinessCards');
const UISummaryInfoUsers = require('./uis/UISummaryInfoUsers');

module.exports = {
  UISummaryInfoBusinessCardDealHistorys,
  UISummaryInfoBusinessCards,
  UISummaryInfoUsers
};
