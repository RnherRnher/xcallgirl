const { action } = require('../../../core/client/utils/middleware');
const constant = require('../constants/system.client.constant');
const constantCommon = require('../../../../commons/utils/constant');

/**
 * @override Yêu cầu lấy thông số người dùng
 *
 * @returns {Object}
 */
const getSystemUsersParameterRequest = () => {
  return action(constant.GET_SYSTEM_USERS_PARAMETER_REQUEST, { fragment: constantCommon.USER });
};
module.exports.getSystemUsersParameterRequest = getSystemUsersParameterRequest;

/**
 * @override Lấy thông số người dùng thành công
 *
 * @param   {Object}
 * @returns {Object}
 */
const getSystemUsersParameterSuccess = ({ code, usersParameter }) => {
  return action(constant.GET_SYSTEM_USERS_PARAMETER_SUCCESS, { code, usersParameter });
};
module.exports.getSystemUsersParameterSuccess = getSystemUsersParameterSuccess;

/**
 * @override Lấy thông số người dùng thất bại
 *
 * @param   {Object}
 * @returns {Object}
 */
const getSystemUsersParameterFailed = ({ code }) => {
  return action(constant.GET_SYSTEM_USERS_PARAMETER_FAILED, { code });
};
module.exports.getSystemUsersParameterFailed = getSystemUsersParameterFailed;

/**
 * @override Yêu cầu lấy thông số  giao dịch danh thiếp
 *
 * @returns {Object}
 */
const getSystemBusinessCardDealHistorysParameterRequest = () => {
  return action(constant.GET_SYSTEM_BUSINESS_CARD_DEAL_HISTORYS_PARAMETER_REQUEST, { fragment: constantCommon.BUSINESS_CARD_DEAL_HISTORY });
};
module.exports.getSystemBusinessCardDealHistorysParameterRequest = getSystemBusinessCardDealHistorysParameterRequest;

/**
 * @override Lấy thông số  giao dịch danh thiếp thành công
 *
 * @param   {Object}
 * @returns {Object}
 */
const getSystemBusinessCardDealHistorysParameterSuccess = ({ code, businessCardDealHistorysParameter }) => {
  return action(constant.GET_SYSTEM_BUSINESS_CARD_DEAL_HISTORYS_PARAMETER_SUCCESS, { code, businessCardDealHistorysParameter });
};
module.exports.getSystemBusinessCardDealHistorysParameterSuccess = getSystemBusinessCardDealHistorysParameterSuccess;

/**
 * @override Lấy thông số  giao dịch danh thiếp thất bại
 *
 * @param   {Object}
 * @returns {Object}
 */
const getSystemBusinessCardDealHistorysParameterFailed = ({ code }) => {
  return action(constant.GET_SYSTEM_BUSINESS_CARD_DEAL_HISTORYS_PARAMETER_FAILED, { code });
};
module.exports.getSystemBusinessCardDealHistorysParameterFailed = getSystemBusinessCardDealHistorysParameterFailed;

/**
 * @override Yêu cầu lấy thông số  danh thiếp
 *
 * @returns {Object}
 */
const getSystemBusinessCardsParameterRequest = () => {
  return action(constant.GET_SYSTEM_BUSINESS_CARDS_PARAMETER_REQUEST, { fragment: constantCommon.BUSINESS_CARD });
};
module.exports.getSystemBusinessCardsParameterRequest = getSystemBusinessCardsParameterRequest;

/**
 * @override Lấy thông số  danh thiếp thành công
 *
 * @param   {Object}
 * @returns {Object}
 */
const getSystemBusinessCardsParameterSuccess = ({ code, businessCardsParameter }) => {
  return action(constant.GET_SYSTEM_BUSINESS_CARDS_PARAMETER_SUCCESS, { code, businessCardsParameter });
};
module.exports.getSystemBusinessCardsParameterSuccess = getSystemBusinessCardsParameterSuccess;

/**
 * @override Lấy thông số danh thiếp thất bại
 *
 * @param   {Object}
 * @returns {Object}
 */
const getSystemBusinessCardsParameterFailed = ({ code }) => {
  return action(constant.GET_SYSTEM_BUSINESS_CARDS_PARAMETER_FAILED, { code });
};
module.exports.getSystemBusinessCardsParameterFailed = getSystemBusinessCardsParameterFailed;

/**
 * @override Thiết lặp lại cửa hàng
 *
 * @param   {Object}
 */
const resetSystemStore = (fragment) => {
  return action(constant.RESERT_SYSTEM_STORE, { fragment });
};
module.exports.resetSystemStore = resetSystemStore;
