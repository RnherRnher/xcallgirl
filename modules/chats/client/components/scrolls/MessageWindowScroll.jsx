const React = require('react');
const propTypes = require('prop-types');
const classNames = require('classnames');
const constant = require('../../../../../commons/utils/constant');
const CommonLoadable = require('../../../../core/client/components/loadables/Loadable');
const { List, WindowScroller } = require('react-virtualized');
const { ContainerNotData } = require('../../../../core/client/components/Containers');
const { ResultEnum } = require('../../../../core/client/constants/core.client.constant');

const MessageWindowScroll = class MessageWindowScroll extends React.Component {
  constructor(props) {
    super(props);

    this.listRef = React.createRef();

    this.rowRenderer = this.rowRenderer.bind(this);
    this.onScroll = this.onScroll.bind(this);
    this.getRowHeight = this.getRowHeight.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (this.listRef && this.listRef.current) {
      this.listRef.current.forceUpdateGrid();
    }
  }

  onScroll(event) {
    const {
      result,
      initialLoad,
      handleLoadMore
    } = this.props;

    if (
      result !== ResultEnum.wait
      && result !== ResultEnum.failed
      && handleLoadMore
      && event.scrollTop < event.clientHeight / 3
    ) {
      handleLoadMore(initialLoad);
    }
  }

  getRowHeight({ index }) {
    const { value } = this.props;

    const currentValue = value[index];
    const preValue = value[index - 1];

    let height = 0;
    switch (currentValue.type) {
      case constant.TEXT: {
        const maxLength = 35;
        const numHeight = currentValue.data.length / maxLength > 1
          ? currentValue.data.length / maxLength
          : 1;

        height = 52.5 + 17.5 * (numHeight - 1);
      }
        break;
      default:
        break;
    }

    const currentHeight = preValue
      ? (
        preValue.userID === currentValue.userID
          ? height
          : 30 + height
      )
      : 30 + height;

    return currentHeight;
  }

  rowRenderer({ key, style, index }) {
    const { value, Children } = this.props;

    const currentValue = value[index];
    const preValue = value[index - 1];

    return (
      <div key={key} style={style}>
        <Children
          value={currentValue}
          isNext={
            preValue
              ? preValue.userID === currentValue.userID
              : false
          }
        />
      </div>
    );
  }

  WindowScroller() {
    const {
      List,
      value,
    } = this.props;

    return (
      <WindowScroller>
        {(propsWindowScroller) => {
          return <List
            ref={this.listRef}
            autoHeight
            scrollToAlignment="end"
            rowCount={value.length}
            rowHeight={this.getRowHeight}
            onScroll={this.onScroll}
            rowRenderer={this.rowRenderer}
            isScrolling={propsWindowScroller.isScrolling}
            scrollTop={propsWindowScroller.scrollTop}
            width={propsWindowScroller.width}
            height={propsWindowScroller.height}
          />;
        }}
      </WindowScroller>
    );
  }

  Loadables() {
    const { Loadable } = this.props;

    return (
      <div>
        <Loadable />
        <Loadable />
        <Loadable />
      </div>
    );
  }

  ContainerNotData() {
    const { Loadable } = this.props;

    return (
      <div>
        <ContainerNotData />
        <Loadable />
      </div>
    );
  }

  initElement() {
    const {
      className,
      value,
      result
    } = this.props;

    return (
      <div className={classNames('xcg-message-window-scroll', className)}>
        {
          result === null
            ? this.WindowScroller()
            : <CommonLoadable
              result={result}
              NoneNode={this.WindowScroller()}
              WaitNode={
                value.length
                  ? this.WindowScroller()
                  : this.Loadables()
              }
              SuccessNode={
                value.length
                  ? this.WindowScroller()
                  : this.ContainerNotData()
              }
              FailedNode={
                value.length
                  ? this.WindowScroller()
                  : this.ContainerNotData()
              }
            />
        }
      </div>
    );
  }

  render() {
    return this.initElement();
  }
};

MessageWindowScroll.propTypes = {
  className: propTypes.string,
  value: propTypes.array,
  initialLoad: propTypes.bool,
  result: propTypes.number,
  List: propTypes.any,
  Loadable: propTypes.any,
  handleLoadMore: propTypes.func,
  Children: propTypes.any.isRequired,
};

MessageWindowScroll.defaultProps = {
  className: '',
  value: [],
  initialLoad: false,
  result: null,
  List,
  Loadable: null,
  handleLoadMore: null
};

module.exports = MessageWindowScroll;
