const React = require('react');
const propTypes = require('prop-types');
const classNames = require('classnames');
const joi = require('joi');
const constant = require('../../../../../commons/utils/constant');
const {
  Send,
  Edit,
  Clear
} = require('@material-ui/icons');
const {
  TextField,
  IconButton
} = require('@material-ui/core');

const InputMessage = class InputMessage extends React.Component {
  constructor(props) {
    super(props);

    const { value } = this.props;
    this.state = {
      msg: value || '',
      error: false
    };

    this.handleCheck = this.handleCheck.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    const { isUpdatePropsToState } = this.props;

    if (isUpdatePropsToState) {
      const { value } = nextProps;

      this.setState({ msg: value || '' });
    }
  }

  handleCheck(body) {
    const { validate } = this.props;

    return joi.validate(body, validate);
  }

  handleChange(event) {
    const { value } = event.target;
    const { variant } = this.props;

    let body = null;
    switch (variant) {
      case constant.COMMENT:
        body = { comment: value };
        break;
      default:
        break;
    }

    this.handleCheck(body)
      .then((body) => {
        this.setState({
          msg: value,
          error: false
        });
      }).catch((err) => {
        this.setState({
          msg: value,
          error: true
        });
      });
  }

  handleClick(event) {
    const { handleSubmit } = this.props;
    const { msg } = this.state;

    handleSubmit(msg);

    this.setState({ msg: '' });
  }

  initElement() {
    const {
      className,
      TitleNode,
      disabled,
      type,
      handleClickReset
    } = this.props;
    const { msg, error } = this.state;

    let AfterIcon = null;
    let ResetButton = null;

    switch (type) {
      case constant.ADD:
        AfterIcon = <Send />;
        break;
      case constant.EDIT:
        AfterIcon = <Edit />;
        ResetButton = <IconButton
          className="xcg-button xcg-after-button"
          onClick={handleClickReset}
        >
          <Clear />
        </IconButton>;
        break;
      default:
        break;
    }

    return (
      <div className={classNames('xcg-input-message', className)}>
        {ResetButton}
        <TextField
          label={TitleNode}
          variant="filled"
          type="text"
          multiline
          rows="1"
          rowsMax="5"
          fullWidth
          value={msg}
          disabled={disabled}
          onChange={this.handleChange}
        />
        <IconButton
          color="secondary"
          className="xcg-button"
          disabled={disabled || !msg || error}
          onClick={this.handleClick}
        >
          {AfterIcon}
        </IconButton>
      </div>
    );
  }

  render() {
    return this.initElement();
  }
};

InputMessage.propTypes = {
  className: propTypes.string,
  value: propTypes.string,
  variant: propTypes.oneOf([
    constant.COMMENT,
    constant.CHAT,
  ]),
  type: propTypes.string,
  validate: propTypes.any,
  handleClickReset: propTypes.func,
  isUpdatePropsToState: propTypes.bool,
  disabled: propTypes.bool,
  TitleNode: propTypes.node.isRequired,
  handleSubmit: propTypes.func.isRequired
};

InputMessage.defaultProps = {
  className: '',
  value: '',
  variant: constant.CHAT,
  type: constant.ADD,
  validate: null,
  handleClickReset: null,
  isUpdatePropsToState: false,
  disabled: false
};

module.exports = InputMessage;
