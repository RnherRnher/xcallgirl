const UIListMessage = require('./uis/UIListMessage');
const UIMessage = require('./uis/UIMessage');

module.exports = {
  UIListMessage,
  UIMessage
};
