const Chat = require('./chats/Chat');
const Chats = require('./chats/Chats');
const CommonDetailChat = require('./chats/CommonDetailChat');
const DetailChat = require('./chats/DetailChat');

module.exports = {
  Chat,
  Chats,
  CommonDetailChat,
  DetailChat
};
