const React = require('react');
const propTypes = require('prop-types');
const classNames = require('classnames');
const ContentLoader = require('react-content-loader').default;

const DetailLoadable = class DetailLoadable extends React.Component {
  constructor(props) {
    super(props);
  }

  initElement() {
    const { className } = this.props;

    return (
      <div className={classNames('xcg-loadable-detail-chat', className)}>
        <ContentLoader
          height={50}
          width={350}
          speed={2}
          primaryColor="#666666"
          secondaryColor="#737373"
        >
          <rect x="65" y="20" rx="5" ry="5" width="220" height="10" />
          <rect x="65" y="40" rx="5" ry="5" width="220" height="10" />
        </ContentLoader>
      </div>
    );
  }

  render() {
    return this.initElement();
  }
};

DetailLoadable.propTypes = {
  className: propTypes.string,
};

DetailLoadable.defaultProps = {
  className: '',
};

module.exports = DetailLoadable;
