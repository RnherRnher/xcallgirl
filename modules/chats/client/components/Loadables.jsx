const DetailLoadable = require('./loadables/DetailLoadable');
const Loadable = require('./loadables/Loadable');
const LoadableMessage = require('./loadables/LoadableMessage');

module.exports = {
  DetailLoadable,
  Loadable,
  LoadableMessage
};
