const React = require('react');
const propTypes = require('prop-types');
const classNames = require('classnames');
const constant = require('../../../../../commons/utils/constant');
const url = require('../../../../core/client/utils/url');
const { createStructuredSelector } = require('reselect');
const { push } = require('connected-react-router/immutable');
const { connect } = require('react-redux');
const {
  Store,
  Whatshot,
  VerifiedUser,
} = require('@material-ui/icons');
const {
  Card,
  CardHeader,
  CardActionArea,
  Avatar,
  Typography,
  CardContent,
} = require('@material-ui/core');
const { makeSelectDetailUsersChat } = require('../../reselects/chat.client.reselect');
const { UIDate } = require('../../../../core/client/components/UIs');
const { makeSelectIsOwner } = require('../../../../users/client/reselects/user.client.reselect');

const UIMessage = class UIMessage extends React.Component {
  constructor(props) {
    super(props);

    this.handleClick = this.handleClick.bind(this);
  }

  getDataContent() {
    const { value } = this.props;

    switch (value.type) {
      case constant.TEXT:
        return (
          <Typography>
            {value.data}
          </Typography>
        );
      default:
        return null;
    }
  }

  handleClick(type) {
    return (event) => {
      const {
        redirect,
        user
      } = this.props;

      switch (type) {
        case constant.USER:
          redirect(`${url.USER_GET}?id=${user._id}`);
          break;
        default:
          break;
      }
    };
  }

  IconProfileUser() {
    const { user } = this.props;

    let IconProfileUser = null;

    if (user.roles === constant.PROVIDER) {
      IconProfileUser = <Store className="xcg-profile-user-icon xcg-success" />;
    } else if (user.roles === constant.ADMIN) {
      IconProfileUser = <Whatshot className="xcg-profile-user-icon xcg-error" />;
    } else if (user.verifyAccount) {
      IconProfileUser = <VerifiedUser className="xcg-profile-user-icon xcg-info" />;
    }

    return IconProfileUser;
  }

  initElement() {
    const {
      className,
      user,
      value,
      isOwner,
      isNext
    } = this.props;

    return (
      <div className={classNames('xcg-ui-message', className)}>
        <Card className={classNames(isOwner ? 'rtl' : '', 'xcg-card')}>
          <CardHeader
            className="xcg-card-header"
            title={
              isNext
                ? null
                : <div className="xcg-profile-user-group">
                  {
                    isOwner
                      ? null
                      : <div className="xcg-profile-user-title">
                        {user.nick}
                      </div>
                  }
                  {
                    isOwner
                      ? null
                      : <div className="xcg-profile-user-list-icon">
                        {this.IconProfileUser()}
                      </div>
                  }
                  <span className="xcg-space">&bull;</span>
                  <div className="xcg-profile-user-caption">
                    <Typography variant="caption">
                      <UIDate value={value.initDate} />
                    </Typography>
                  </div>
                </div>
            }
            avatar={
              isNext
                ? null
                : <CardActionArea
                  className="xcg-profile-user-avatar"
                  onClick={this.handleClick(constant.USER)}
                >
                  <Avatar
                    className="xcg-avatar"
                    alt={user.nick}
                    src={user.avatar}
                  />
                </CardActionArea>
            }
          />
          <CardContent className="xcg-card-content">
            {this.getDataContent()}
          </CardContent>
        </Card>
      </div>
    );
  }

  render() {
    return this.initElement();
  }
};

UIMessage.propTypes = {
  className: propTypes.string,
  value: propTypes.object.isRequired,
  user: propTypes.object.isRequired,
  isNext: propTypes.bool.isRequired,
  isOwner: propTypes.bool.isRequired,
  redirect: propTypes.func.isRequired,
};

UIMessage.defaultProps = {
  className: '',
};


function mapStateToProps(state, props) {
  return createStructuredSelector({
    user: makeSelectDetailUsersChat(props.value.userID),
    isOwner: makeSelectIsOwner(props.value.userID),
  });
}

function mapDispatchToProps(dispatch, props) {
  return {
    redirect: (location) => {
      return dispatch(push(location));
    }
  };
}

module.exports = connect(
  mapStateToProps,
  mapDispatchToProps,
)(UIMessage);
