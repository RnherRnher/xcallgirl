const React = require('react');
const propTypes = require('prop-types');
const classNames = require('classnames');
const UIMessage = require('./UIMessage');
const { FormattedMessage } = require('react-intl');
const { Done } = require('@material-ui/icons');
const {
  Chip,
  Avatar,
  AppBar,
  Toolbar,
} = require('@material-ui/core');
const { InputMessage } = require('../Inputs');
const { LoadableMessage } = require('../Loadables');
const { MessageWindowScroll } = require('../Scrolls');
const { ResultEnum } = require('../../../../core/client/constants/core.client.constant');
const { UIDate } = require('../../../../core/client/components/UIs');

const UIListMessages = class UIListMessages extends React.Component {
  constructor(props) {
    super(props);

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(msg) {
    const { handleSubmit } = this.props;

    handleSubmit(msg);
  }

  SeenView() {
    const { users } = this.props;

    const seens = [];
    users.forEach((user) => {
      if (user.view.is) {
        seens.push(
          <div
            className="xcg-seen"
            key={user.nick}
          >
            <Chip
              color="secondary"
              avatar={
                <Avatar
                  alt={user.nick}
                  src={user.avatar}
                />
              }
              label={
                <span>
                  {user.nick} <FormattedMessage id="info.seen.exist" /> <UIDate value={user.view.date} />
                </span>
              }
              deleteIcon={<Done />}
              onDelete={() => { }}
            />
          </div>
        );
      }
    });

    return (
      <div className="xcg-seen-gourp">
        {seens}
      </div>
    );
  }

  initElement() {
    const {
      className,
      pushTitleNode,
      messages,
      handleClickPush,
      value,
    } = this.props;

    const addDisabled = messages && messages.post ? messages.post.result === ResultEnum.wait : false;

    return (
      <div className={classNames('xcg-ui-list-message', className)}>
        <Toolbar>
          {pushTitleNode}
        </Toolbar>
        <div className="xcg-view-gourp">
          <MessageWindowScroll
            handleLoadMore={handleClickPush}
            value={value}
            Children={UIMessage}
            result={
              messages && messages.get
                ? messages.get.result
                : ResultEnum.none
            }
            Loadable={LoadableMessage}
          />
          {this.SeenView()}
        </div>
        <AppBar className="xcg-botton-app-bar" position="fixed">
          <InputMessage
            TitleNode={<FormattedMessage id="info.message.is" />}
            disabled={addDisabled}
            handleSubmit={this.handleSubmit}
          />
        </AppBar>
      </div>
    );
  }

  render() {
    return this.initElement();
  }
};

UIListMessages.propTypes = {
  className: propTypes.string,
  value: propTypes.array,
  users: propTypes.array,
  pushTitleNode: propTypes.node,
  messages: propTypes.shape({
    get: propTypes.object,
    post: propTypes.object
  }),
  handleClickPush: propTypes.func.isRequired,
  handleSubmit: propTypes.func.isRequired,
};

UIListMessages.defaultProps = {
  className: 'xcg-dialog',
  value: [],
  users: [],
  messages: null,
  pushTitleNode: null,
};

module.exports = UIListMessages;
