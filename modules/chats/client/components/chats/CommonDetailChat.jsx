const React = require('react');
const propTypes = require('prop-types');
const classNames = require('classnames');
const DetailChat = require('./DetailChat');
const { connect } = require('react-redux');
const { createStructuredSelector } = require('reselect');
const { getChatIDRequest } = require('../../actions/chat.client.action');
const { DetailLoadable } = require('../Loadables');
const { Loadable } = require('../../../../core/client/components/Loadables');
const { makeSelectDetailChat } = require('../../reselects/chat.client.reselect');
const { makeSelectLocation } = require('../../../../core/client/reselects/router.client.reselect');
const { ContainerNotData } = require('../../../../core/client/components/Containers');

const CommonDetailChat = class CommonDetailChat extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    const { getChatIDRequest, location } = this.props;

    const { search } = location;
    const chatID = search.slice(search.indexOf('=') + 1);

    getChatIDRequest({ chatID });
  }

  initElement() {
    const { className, chat } = this.props;

    return (
      <div className={classNames('xcg-common-detail-chat', className)}>
        <Loadable
          result={chat.result}
          WaitNode={<DetailLoadable />}
          SuccessNode={<DetailChat value={chat.data} />}
          FailedNode={
            <div>
              <ContainerNotData />
              <DetailLoadable />
            </div>
          }
        />
      </div>
    );
  }

  render() {
    return this.initElement();
  }
};

CommonDetailChat.propTypes = {
  className: propTypes.string,
  chat: propTypes.object.isRequired,
  location: propTypes.object.isRequired,
  getChatIDRequest: propTypes.func.isRequired,
};

CommonDetailChat.defaultProps = {
  className: ''
};

function mapStateToProps(state, props) {
  return createStructuredSelector({
    chat: makeSelectDetailChat(),
    location: makeSelectLocation(),
  });
}

function mapDispatchToProps(dispatch, props) {
  return {
    getChatIDRequest: (body) => {
      return dispatch(getChatIDRequest(body));
    }
  };
}

module.exports = connect(
  mapStateToProps,
  mapDispatchToProps,
)(CommonDetailChat);
