const React = require('react');
const propTypes = require('prop-types');
const classNames = require('classnames');
const constant = require('../../../../../commons/utils/constant');
const Chat = require('./Chat');
const { connect } = require('react-redux');
const { createStructuredSelector } = require('reselect');
const { FormattedMessage } = require('react-intl');
const {
  Archive,
  Inbox
} = require('@material-ui/icons');
const {
  AppBar,
  Tabs,
  Tab,
  Badge
} = require('@material-ui/core');
const { Loadable } = require('../Loadables');
const { WindowScroll } = require('../../../../core/client/components/Scrolls');
const { getChatsRequest } = require('../../actions/chat.client.action');
const { makeSelectSelfChats, makeSelectCountChats } = require('../../reselects/chat.client.reselect');

const Chats = class Chats extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      index: 0,
      fragment: constant.SUCCESS
    };

    this.handleLoad = this.handleLoad.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  getTabContent() {
    const { fragment } = this.state;
    const { chats, rowHeight } = this.props;

    return (
      <WindowScroll
        result={chats[fragment].result}
        handleLoadMore={this.handleLoad(fragment)}
        initialLoad={!chats[fragment].data.length}
        value={chats[fragment].data}
        Children={Chat}
        Loadable={Loadable}
        rowHeight={rowHeight}
      />
    );
  }

  handleLoad(fragment) {
    return (reload) => {
      const { getChatsRequest, chats } = this.props;
      const { fragment } = this.state;

      getChatsRequest(
        constant.AUTO,
        fragment,
        chats[fragment].skip,
        reload
      );
    };
  }

  handleChange(type) {
    return (event, index) => {
      switch (type) {
        case constant.VIEW:
          this.setState((state, props) => {
            let fragment = null;
            switch (index) {
              case 0:
                fragment = constant.SUCCESS;
                break;
              case 1:
                fragment = constant.WAIT;
                break;
              default:
                break;
            }

            return {
              index,
              fragment
            };
          });
          break;
        default:
          break;
      }
    };
  }

  Tab() {
    const { count, countLimit } = this.props;
    const { index } = this.state;

    return (
      <AppBar className="xcg-app-bar" position="fixed">
        <Tabs
          value={index}
          onChange={this.handleChange(constant.VIEW)}
        >
          <Tab
            label={<FormattedMessage id="chat.is" />}
            icon={
              count.update
                ? <Badge
                  color="secondary"
                  badgeContent={
                    count.update > countLimit
                      ? `${countLimit}+`
                      : count.update
                  }
                >
                  <Inbox />
                </Badge>
                : <Inbox />
            }
          />
          <Tab
            label={<FormattedMessage id="chat.wait.is" />}
            icon={
              count.wait
                ? <Badge
                  color="secondary"
                  badgeContent={
                    count.wait > countLimit
                      ? `${countLimit}+`
                      : count.wait
                  }
                >
                  <Archive />
                </Badge>
                : <Archive />
            }
          />
        </Tabs>
      </AppBar>
    );
  }

  initElement() {
    const { className } = this.props;

    return (
      <div className={classNames('xcg-chats', className)}>
        {this.Tab()}
        {this.getTabContent()}
      </div>
    );
  }

  render() {
    return this.initElement();
  }
};

Chats.propTypes = {
  className: propTypes.string,
  countLimit: propTypes.number,
  rowHeight: propTypes.number,
  chats: propTypes.object.isRequired,
  count: propTypes.object.isRequired,
  getChatsRequest: propTypes.func.isRequired,
};

Chats.defaultProps = {
  className: '',
  countLimit: constant.ENUM.COUNT.LIMIT,
  rowHeight: 75
};

function mapStateToProps(state, props) {
  return createStructuredSelector({
    chats: makeSelectSelfChats(),
    count: makeSelectCountChats()
  });
}

function mapDispatchToProps(dispatch, props) {
  return {
    getChatsRequest: (
      typee,
      fragment,
      skip,
      reload
    ) => {
      return dispatch(getChatsRequest(
        typee,
        fragment,
        skip,
        reload
      ));
    }
  };
}

module.exports = connect(
  mapStateToProps,
  mapDispatchToProps,
)(Chats);
