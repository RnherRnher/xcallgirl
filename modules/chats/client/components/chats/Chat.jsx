const React = require('react');
const propTypes = require('prop-types');
const classNames = require('classnames');
const joi = require('joi');
const constant = require('../../../../../commons/utils/constant');
const url = require('../../../../core/client/utils/url');
const { FormattedMessage } = require('react-intl');
const { createStructuredSelector } = require('reselect');
const { push } = require('connected-react-router/immutable');
const { connect } = require('react-redux');
const {
  MoreVert,
  CheckCircle,
  Error,
  Delete,
  Store,
  Whatshot,
  VerifiedUser
} = require('@material-ui/icons');
const {
  List,
  ListItem,
  Avatar,
  ListItemAvatar,
  ListItemSecondaryAction,
  Badge,
  IconButton,
  Button,
  Menu,
  MenuItem,
  ListItemIcon,
  ListItemText,
  Typography
} = require('@material-ui/core');
const { acceptChatRequest, deleteChatRequest } = require('../../actions/chat.client.action');
const { makeSelectAcceptActionChat, makeSelectDeleteActionChat } = require('../../reselects/chat.client.reselect');
const { UIDate } = require('../../../../core/client/components/UIs');
const { ResultEnum } = require('../../../../core/client/constants/core.client.constant');
const { accept } = require('../../../../../commons/modules/chats/validates/chat.validate');

const Chat = class Chat extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      clicked: false,
      anchorEl: null
    };

    this.handleCheck = this.handleCheck.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.handleClickMenuItem = this.handleClickMenuItem.bind(this);
    this.handleCloseMenu = this.handleCloseMenu.bind(this);
  }

  getData() {
    const { value } = this.props;

    let _data = '';

    if (value.datas) {
      switch (value.datas.type) {
        case constant.TEXT:
          _data = <Typography component="span" noWrap>
            {value.datas.data}
          </Typography>;
          break;
        default:
          break;
      }
    }

    return _data;
  }

  handleCheck(body, validate) {
    return joi.validate(body, validate);
  }

  handleClick(type) {
    return (event) => {
      const {
        value,
        validate,
        acceptChatRequest,
        redirect
      } = this.props;

      switch (type) {
        case constant.CHAT:
          redirect(`${url.CHAT_GET}?id=${value._id}`);
          break;
        case constant.ACCEPT:
        case constant.NOT_ACCEPT:
          this.handleCheck(
            { accept: { is: type === constant.ACCEPT } },
            validate.accept
          ).then((accept) => {
            acceptChatRequest(value._id, accept);
          });
          break;
        case constant.MORE:
          this.setState({
            clicked: true,
            anchorEl: event.target
          });
          break;
        default:
          break;
      }
    };
  }

  handleClickMenuItem(type) {
    return (event) => {
      const { value, deleteChatRequest } = this.props;

      switch (type) {
        case constant.DELETE:
          deleteChatRequest(value._id);
          break;
        default:
          break;
      }

      this.handleCloseMenu();
    };
  }

  handleCloseMenu(event) {
    this.setState({
      clicked: false,
      anchorEl: null
    });
  }

  Menu() {
    const { deletee } = this.props;
    const { clicked, anchorEl } = this.state;

    const deleteDisabled = deletee ? deletee.result === ResultEnum.wait : false;

    return (
      <Menu
        anchorEl={anchorEl}
        open={clicked}
        onClose={this.handleCloseMenu}
      >
        <MenuItem
          disabled={deleteDisabled}
          onClick={this.handleClickMenuItem(constant.DELETE)}
          value={constant.DELETE}
        >
          <ListItemIcon>
            <Delete />
          </ListItemIcon>
          <ListItemText
            inset
            primary={<FormattedMessage id="actions.delete" />}
          />
        </MenuItem>
      </Menu>
    );
  }

  Avatar() {
    const { value } = this.props;

    const Type = null;
    const background = {
      name: 'chat_background',
      url: null
    };

    switch (value.type) {
      case constant.PRIVCY:
        value.users.forEach((user) => {
          if (user) {
            background.url = user.avatar;
          }
        });
        break;
      default:
        break;
    }

    return (
      value.type === constant.PRIVCY
        ? <Avatar
          className="xcg-avatar"
          alt={background.name}
          src={background.url}
        />
        : <Badge badgeContent={Type}>
          <Avatar
            className="xcg-avatar"
            alt={background.name}
            src={background.url}
          />
        </Badge>
    );
  }

  Title() {
    const { value } = this.props;

    const IconProfileUser = (user) => {
      if (user.roles === constant.PROVIDER) {
        return <Store className="xcg-profile-user-icon xcg-success" />;
      }

      if (user.roles === constant.ADMIN) {
        return <Whatshot className="xcg-profile-user-icon xcg-error" />;
      }

      if (user.verifyAccount) {
        return <VerifiedUser className="xcg-profile-user-icon xcg-info" />;
      }

      return null;
    };

    let names = '';
    let Icon = null;
    value.users.forEach((user) => {
      if (user) {
        names += user.nick;

        switch (value.type) {
          case constant.PRIVCY:
            Icon = IconProfileUser(user);
            break;
          default:
            break;
        }
      }
    });

    return (
      <div className="xcg-profile-user-group">
        <div className="xcg-profile-user-title">
          {names}
        </div>
        <div className="xcg-profile-user-list-icon">
          {Icon}
        </div>
        <span className="xcg-space">&bull;</span>
        <Typography variant="caption">
          <UIDate value={value.updateDate} />
        </Typography>
      </div>
    );
  }

  Actions() {
    const { value, accept } = this.props;

    const acceptDisabled = accept ? accept.result === ResultEnum.wait : false;

    return (
      <ListItemSecondaryAction>
        {
          value.isAccept
            ? <IconButton onClick={this.handleClick(constant.MORE)}>
              <MoreVert />
            </IconButton>
            : <div className="xcg-actions">
              <Button
                disabled={acceptDisabled}
                color="secondary"
                onClick={this.handleClick(constant.ACCEPT)}
              >
                <CheckCircle />
                <FormattedMessage id="actions.accept" />
              </Button>
              <Button
                disabled={acceptDisabled}
                color="primary"
                onClick={this.handleClick(constant.NOT_ACCEPT)}
              >
                <Error />
                <FormattedMessage id="actions.notAccept" />
              </Button>
            </div>
        }
      </ListItemSecondaryAction>
    );
  }

  initElement() {
    const { className, value } = this.props;


    return (
      <div className={classNames('xcg-chat', className)}>
        <List className={value.isView ? 'xcg-view' : 'xcg-un-view'}>
          <ListItem
            button
            onClick={value.isAccept ? this.handleClick(constant.CHAT) : null}
          >
            <ListItemAvatar>
              {this.Avatar()}
            </ListItemAvatar>
            <ListItemText
              inset
              primary={this.Title()}
              secondary={this.getData()}
            />
            {this.Actions()}
          </ListItem>
        </List>
        {this.Menu()}
      </div>
    );
  }

  render() {
    return this.initElement();
  }
};

Chat.propTypes = {
  className: propTypes.string,
  accept: propTypes.object,
  deletee: propTypes.object,
  validate: propTypes.object,
  value: propTypes.object.isRequired,
  acceptChatRequest: propTypes.func.isRequired,
  deleteChatRequest: propTypes.func.isRequired,
  redirect: propTypes.func.isRequired
};

Chat.defaultProps = {
  className: '',
  accept: null,
  deletee: null,
  validate: {
    accept: accept.body
  }
};

function mapStateToProps(state, props) {
  const chatID = props.value._id;

  return createStructuredSelector({
    accept: makeSelectAcceptActionChat(chatID),
    deletee: makeSelectDeleteActionChat(chatID),
  });
}

function mapDispatchToProps(dispatch, props) {
  return {
    acceptChatRequest: (chatID, accept) => {
      return dispatch(acceptChatRequest(chatID, accept));
    },
    deleteChatRequest: (chatID) => {
      return dispatch(deleteChatRequest(chatID));
    },
    redirect: (location) => {
      return dispatch(push(location));
    }
  };
}

module.exports = connect(
  mapStateToProps,
  mapDispatchToProps,
)(Chat);
