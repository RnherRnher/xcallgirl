const React = require('react');
const propTypes = require('prop-types');
const classNames = require('classnames');
const constant = require('../../../../../commons/utils/constant');
const { connect } = require('react-redux');
const { createStructuredSelector } = require('reselect');
const { ResultEnum } = require('../../../../core/client/constants/core.client.constant');
const { UIListMessage } = require('../UIs');
const { makeSelectMessagesActionChat } = require('../../reselects/chat.client.reselect');
const {
  getMessagesChatRequest,
  resetChatStore,
  messagesChatRequest,
  listenChat
} = require('../../actions/chat.client.action');

const DetailChat = class DetailChat extends React.Component {
  constructor(props) {
    super(props);

    this.handleClickPush = this.handleClickPush.bind(this);

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    const {
      value,
      messages,
      listenChat
    } = this.props;

    const pushDisabled = messages && messages.get ? messages.get.result === ResultEnum.wait : false;
    if (!pushDisabled && !value.messages.length) {
      this.handleClickPush(constant.MESSAGE)();
    }

    listenChat(constant.ADD, value._id);
  }

  componentWillUnmount() {
    const {
      value,
      listenChat,
      resetChatStore
    } = this.props;

    listenChat(constant.REMOVE, value._id);
    resetChatStore(constant.DETAIL);
  }

  handleClickPush(type) {
    return (event) => {
      const {
        value,
        messages,
        getMessagesChatRequest
      } = this.props;

      switch (type) {
        case constant.MESSAGE:
          getMessagesChatRequest(
            value._id,
            messages && messages.get ? messages.get.max : -1
          );
          break;
        default:
          break;
      }
    };
  }

  handleSubmit(type) {
    const { value, messagesChatRequest } = this.props;

    switch (type) {
      case constant.MESSAGE:
        return (data) => {
          messagesChatRequest(
            value._id,
            {
              chatID: value._id,
              type: constant.TEXT,
              data
            }
          );
        };
      default:
        break;
    }
  }

  ListMessage() {
    const { value, messages } = this.props;

    return (
      <UIListMessage
        value={value.messages}
        users={value.users}
        messages={messages}
        handleClickPush={this.handleClickPush(constant.MESSAGE)}
        handleSubmit={this.handleSubmit(constant.MESSAGE)}
      />
    );
  }

  initElement() {
    const { className } = this.props;

    return (
      <div className={classNames('xcg-detail-chat', className)}>
        {this.ListMessage()}
      </div>
    );
  }

  render() {
    return this.initElement();
  }
};

DetailChat.propTypes = {
  className: propTypes.string,
  messages: propTypes.object,
  value: propTypes.object.isRequired,
  getMessagesChatRequest: propTypes.func.isRequired,
  messagesChatRequest: propTypes.func.isRequired,
  listenChat: propTypes.func.isRequired,
  resetChatStore: propTypes.func.isRequired,
};

DetailChat.defaultProps = {
  className: '',
  messages: null,
};

function mapStateToProps(state, props) {
  const chatID = props.value._id;

  return createStructuredSelector({
    messages: makeSelectMessagesActionChat(chatID),
  });
}

function mapDispatchToProps(dispatch, props) {
  return {
    getMessagesChatRequest: (chatID, max) => {
      return dispatch(getMessagesChatRequest(chatID, max));
    },
    messagesChatRequest: (to, body) => {
      return dispatch(messagesChatRequest(to, body));
    },
    listenChat: (fragment, chatID) => {
      return dispatch(listenChat(fragment, chatID));
    },
    resetChatStore: (fragment) => {
      return dispatch(resetChatStore(fragment));
    }
  };
}

module.exports = connect(
  mapStateToProps,
  mapDispatchToProps,
)(DetailChat);
