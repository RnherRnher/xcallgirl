const constant = require('../constants/chat.client.constant');
const commonConstant = require('../../../../commons/utils/constant');
const { action } = require('../../../core/client/utils/middleware');

/**
 * @override Yêu cầu tạo trò chuyện
 *
 * @param   {String}  fragment
 * @param   {Object}  body
 * @returns {Object}
 */
const createChatRequest = (fragment, create) => {
  return action(constant.CREATE_CHAT_REQUEST, { fragment, create });
};
module.exports.createChatRequest = createChatRequest;

/**
 * @override Tạo trò chuyện thành công
 *
 * @param   {Object}
 * @returns {Object}
 */
const createChatSuccess = ({ code, chat }) => {
  return action(constant.CREATE_CHAT_SUCCESS, { code, chat });
};
module.exports.createChatSuccess = createChatSuccess;

/**
 * @override Tạo trò chuyện thất bại
 *
 * @param   {Object}
 * @returns {Object}
 */
const createChatFailed = ({ code }) => {
  return action(constant.CREATE_CHAT_FAILED, { code });
};
module.exports.createChatFailed = createChatFailed;

/**
 * @override Yêu cầu lấy trò chuyện
 *
 * @param   {Object}
 * @param   {Boolean} reload
 * @returns {Object}
 */
const getChatRequest = ({ chatID }, reload = false) => {
  return action(
    constant.GET_CHAT_REQUEST,
    {
      chatID,
      fragment: commonConstant.SHORTCUT,
      reload
    }
  );
};
module.exports.getChatRequest = getChatRequest;

/**
 * @override Lấy trò chuyện thành công
 *
 * @param   {Object}
 * @returns {Object}
 */
const getChatSuccess = ({
  code,
  chat,
  count
}) => {
  return action(
    constant.GET_CHAT_SUCCESS,
    {
      code,
      chat,
      count
    }
  );
};
module.exports.getChatSuccess = getChatSuccess;

/**
 * @override Yêu cầu lấy trò chuyện theo ID
 *
 * @param   {Object}
 * @returns {Object}
 */
const getChatIDRequest = ({ chatID }) => {
  return action(constant.GET_CHAT_ID_REQUEST, { chatID, fragment: commonConstant.FULL });
};
module.exports.getChatIDRequest = getChatIDRequest;

/**
 * @override Lấy trò chuyện theo ID thành công
 *
 * @param   {Object}
 * @returns {Object}
 */
const getChatIDSuccess = ({
  code,
  chat,
  count
}) => {
  return action(
    constant.GET_CHAT_ID_SUCCESS,
    {
      code,
      chat,
      count
    }
  );
};
module.exports.getChatIDSuccess = getChatIDSuccess;

/**
 * @override Lấy trò chuyện theo ID thất bại
 *
 * @param   {Object}
 * @returns {Object}
 */
const getChatIDFailed = ({ code }) => {
  return action(constant.GET_CHAT_ID_FAILED, { code });
};
module.exports.getChatIDFailed = getChatIDFailed;

/**
 * @override Yêu cầu lấy theo ID trò chuyện
 *
 * @param   {String}  typee
 * @param   {String}  fragment
 * @param   {Number}  skip
 * @param   {Boolean} reload
 * @returns {Object}
 */
const getChatsRequest = (
  typee,
  fragment,
  skip,
  reload = false
) => {
  return action(
    constant.GET_CHATS_REQUEST,
    {
      typee,
      fragment,
      skip: reload ? 0 : (skip || 0),
      reload
    }
  );
};
module.exports.getChatsRequest = getChatsRequest;

/**
 * @override Lấy trò chuyện thành công
 *
 * @param   {Object}
 * @returns {Object}
 */
const getChatsSuccess = ({
  code,
  skip,
  chats,
  count,
  fragment
}) => {
  return action(
    constant.GET_CHATS_SUCCESS,
    {
      code,
      skip,
      chats,
      count,
      fragment
    }
  );
};
module.exports.getChatsSuccess = getChatsSuccess;

/**
 * @override Lấy trò chuyện thất bại
 *
 * @param   {Object}
 * @returns {Object}
 */
const getChatsFailed = ({
  code,
  count,
  fragment
}) => {
  return action(
    constant.GET_CHATS_FAILED,
    {
      code,
      count,
      fragment
    }
  );
};
module.exports.getChatsFailed = getChatsFailed;

/**
 * @override Yêu cầu chấp nhận trò chuyện
 *
 * @param   {ObjectId}  chatID
 * @param   {Object}    body
 * @returns {Object}
 */
const acceptChatRequest = (chatID, accept) => {
  return action(constant.ACCEPT_CHAT_REQUEST, { chatID, accept });
};
module.exports.acceptChatRequest = acceptChatRequest;

/**
 * @override Chấp nhận trò chuyện thành công
 *
 * @param   {Object}
 * @returns {Object}
 */
const acceptChatSuccess = ({
  code,
  chat,
  chatID
}) => {
  return action(
    constant.ACCEPT_CHAT_SUCCESS,
    {
      code,
      chat,
      chatID
    }
  );
};
module.exports.acceptChatSuccess = acceptChatSuccess;

/**
 * @override Chấp nhận trò chuyện thất bại
 *
 * @param   {Object}
 * @returns {Object}
 */
const acceptChatFailed = ({ code, chatID }) => {
  return action(constant.ACCEPT_CHAT_FAILED, { code, chatID });
};
module.exports.acceptChatFailed = acceptChatFailed;

/**
 * @override Yêu cầu xóa trò chuyện
 *
 * @param   {ObjectId}  chatID
 * @returns {Object}
 */
const deleteChatRequest = (chatID) => {
  return action(constant.DELETE_CHAT_REQUEST, { chatID });
};
module.exports.deleteChatRequest = deleteChatRequest;

/**
 * @override Xóa trò chuyện thành công
 *
 * @param   {Object}
 * @returns {Object}
 */
const deleteChatSuccess = ({ code, chatID }) => {
  return action(constant.DELETE_CHAT_SUCCESS, { code, chatID });
};
module.exports.deleteChatSuccess = deleteChatSuccess;

/**
 * @override Xóa trò chuyện thất bại
 *
 * @param   {Object}
 * @returns {Object}
 */
const deleteChatFailed = ({ code, chatID }) => {
  return action(constant.DELETE_CHAT_FAILED, { code, chatID });
};
module.exports.deleteChatFailed = deleteChatFailed;

/**
 * @override Yêu cầu lấy tin nhắn trò chuyện
 *
 * @param   {ObjectId} chatID
 * @param   {Number} max
 * @returns {Object}
 */
const getMessagesChatRequest = (chatID, max) => {
  return action(constant.GET_MESSAGES_CHAT_REQUEST, { chatID, max });
};
module.exports.getMessagesChatRequest = getMessagesChatRequest;

/**
 * @override Lấy tin nhắn trò chuyện thành công
 *
 * @param   {Object}
 * @returns {Object}
 */
const getMessagesChatSuccess = ({
  code,
  chatID,
  max,
  messages
}) => {
  return action(
    constant.GET_MESSAGES_CHAT_SUCCESS,
    {
      code,
      chatID,
      max,
      messages
    }
  );
};
module.exports.getMessagesChatSuccess = getMessagesChatSuccess;

/**
 * @override Lấy tin nhắn trò chuyện thất bại
 *
 * @param   {Object}
 * @returns {Object}
 */
const getMessagesChatFailed = ({ code, chatID }) => {
  return action(constant.GET_MESSAGES_CHAT_FAILED, { code, chatID });
};
module.exports.getMessagesChatFailed = getMessagesChatFailed;

/**
 * @override Yêu cầu tin nhắn trò chuyện
 *
 * @param   {ObjectId} to
 * @param   {Object} body
 * @returns {Object}
 */
const messagesChatRequest = (to, body) => {
  return action(constant.MESSAGE_CHAT_REQUEST, { to, body });
};
module.exports.messagesChatRequest = messagesChatRequest;

/**
 * @override Tin nhắn trò chuyện thành công
 *
 * @param   {Object}
 * @returns {Object}
 */
const messageChatSuccess = ({
  code,
  chatID,
  data
}) => {
  return action(
    constant.MESSAGE_CHAT_SUCCESS,
    {
      code,
      chatID,
      data
    }
  );
};
module.exports.messageChatSuccess = messageChatSuccess;

/**
 * @override Tin nhắn trò chuyện thất bại
 *
 * @param   {Object}
 * @returns {Object}
 */
const messageChatFailed = ({ code, chatID }) => {
  return action(constant.MESSAGE_CHAT_FAILED, { code, chatID });
};
module.exports.messageChatFailed = messageChatFailed;

/**
 * @override Xem tin nhắn trò chuyện thành công
 *
 * @param   {Object}
 * @returns {Object}
 */
const seenChatSuccess = ({
  code,
  chatID,
  users
}) => {
  return action(
    constant.SEEN_CHAT_SUCCESS,
    {
      code,
      chatID,
      users
    }
  );
};
module.exports.seenChatSuccess = seenChatSuccess;

/**
 * @override Xem tin nhắn trò chuyện thất bại
 *
 * @param   {Object}
 * @returns {Object}
 */
const seenChatFailed = ({ code, chatID }) => {
  return action(constant.SEEN_CHAT_FAILED, { code, chatID });
};
module.exports.seenChatFailed = seenChatFailed;

/**
 * @override Lắng nghe trò chuyện theo id
 *
 * @param   {Object}
 * @returns {Object}
 */
const listenChat = (fragment, chatID) => {
  return action(constant.LISTEN_CHAT, { fragment, chatID });
};
module.exports.listenChat = listenChat;

/**
 * @override Thiết lặp lại cửa hàng
 *
 * @param {String}  fragment
 * @returns {Object}
 *
 */
const resetChatStore = (fragment) => {
  return action(constant.RESERT_CHAT_STORE, { fragment });
};
module.exports.resetChatStore = resetChatStore;
