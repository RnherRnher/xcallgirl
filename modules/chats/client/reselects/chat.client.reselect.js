const { createSelector } = require('reselect');
const { findIndex } = require('lodash');
const { makeSelect } = require('../../../core/client/utils/middleware');

const selectChat = (state, props) => {
  return state.get('chat');
};

const makeSelectSelfChats = () => {
  return createSelector(
    selectChat,
    (chatState) => {
      return makeSelect(chatState.get('self'));
    }
  );
};
module.exports.makeSelectSelfChats = makeSelectSelfChats;

const makeSelectDetailChat = () => {
  return createSelector(
    selectChat,
    (chatState) => {
      return makeSelect(chatState.get('detail'));
    }
  );
};
module.exports.makeSelectDetailChat = makeSelectDetailChat;

const makeSelectDetailUsersChat = (userID) => {
  return createSelector(
    selectChat,
    (chatState) => {
      const users = makeSelect(chatState.getIn(['detail', 'data', 'users']));

      if (userID) {
        const index = findIndex(users, (user) => {
          return user._id === userID;
        });

        return users[index];
      }

      return users;
    }
  );
};
module.exports.makeSelectDetailUsersChat = makeSelectDetailUsersChat;

const makeSelectAllCountChats = () => {
  return createSelector(
    selectChat,
    (chatState) => {
      return makeSelect(chatState.getIn(['self', 'count', 'update']))
        + makeSelect(chatState.getIn(['self', 'count', 'wait']));
    }
  );
};
module.exports.makeSelectAllCountChats = makeSelectAllCountChats;

const makeSelectCountChats = () => {
  return createSelector(
    selectChat,
    (chatState) => {
      return makeSelect(chatState.getIn(['self', 'count']));
    }
  );
};
module.exports.makeSelectCountChats = makeSelectCountChats;

const makeSelectAcceptActionChat = (chatID) => {
  return createSelector(
    selectChat,
    (chatState) => {
      return makeSelect(chatState.getIn(['actions', 'accept', chatID]));
    }
  );
};
module.exports.makeSelectAcceptActionChat = makeSelectAcceptActionChat;

const makeSelectDeleteActionChat = (chatID) => {
  return createSelector(
    selectChat,
    (chatState) => {
      return makeSelect(chatState.getIn(['actions', 'delete', chatID]));
    }
  );
};
module.exports.makeSelectDeleteActionChat = makeSelectDeleteActionChat;

const makeSelectCreateActionChat = () => {
  return createSelector(
    selectChat,
    (chatState) => {
      return makeSelect(chatState.getIn(['actions', 'create']));
    }
  );
};
module.exports.makeSelectCreateActionChat = makeSelectCreateActionChat;

const makeSelectMessagesActionChat = (chatID) => {
  return createSelector(
    selectChat,
    (chatState) => {
      return makeSelect(chatState.getIn(['actions', 'messages', chatID]));
    }
  );
};
module.exports.makeSelectMessagesActionChat = makeSelectMessagesActionChat;
