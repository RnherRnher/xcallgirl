const constant = require('../constants/chat.client.constant');
const constantCommon = require('../../../../commons/utils/constant');
const { fromJS, isCollection } = require('immutable');
const { responseState } = require('../../../core/client/utils/middleware');
const { ResultEnum } = require('../../../core/client/constants/core.client.constant');

const initialState = fromJS({
  self: {
    success: responseState({ data: [] }),
    wait: responseState({ data: [] }),
    count: {
      update: 0,
      wait: 0
    }
  },
  detail: responseState(),
  actions: {
    create: responseState(),
    get: responseState(),
    delete: {},
    accept: {},
    messages: {},
  }
});

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case constant.CREATE_CHAT_REQUEST: {
      return state
        .setIn(
          ['actions', 'create'],
          {
            result: ResultEnum.wait,
            fragment: action.fragment
          }
        );
    }
    case constant.CREATE_CHAT_SUCCESS: {
      const index = state
        .getIn(['self', 'success', 'data'])
        .findIndex((chat) => {
          return (
            isCollection(chat)
              ? chat.get('_id')
              : chat._id
          ) === action.chat._id;
        });

      return state
        .updateIn(
          ['actions', 'create'],
          (create) => {
            create.result = ResultEnum.success;
            create.code = action.code;

            return create;
          }
        )
        .updateIn(
          ['self', 'success', 'data'],
          (data) => {
            if (index === -1) {
              return fromJS([action.chat]).concat(data);
            }

            data[index] = action.chat;
            return data;
          }
        )
        .updateIn(
          ['self', 'success', 'skip'],
          (skip) => {
            return index === -1 ? (skip ? skip + 1 : 1) : skip;
          }
        );
    }
    case constant.CREATE_CHAT_FAILED: {
      return state
        .updateIn(
          ['actions', 'create'],
          (create) => {
            create.result = ResultEnum.failed;
            create.code = action.code;

            return create;
          }
        );
    }
    case constant.GET_CHATS_REQUEST: {
      return state
        .setIn(['self', action.fragment, 'result'], ResultEnum.wait)
        .setIn(['self', action.fragment, 'skip'], action.skip)
        .setIn(['self', action.fragment, 'type'], action.typee)
        .setIn(['self', action.fragment, 'reload'], action.reload)
        .setIn(['self', action.fragment, 'code'], undefined);
    }
    case constant.GET_CHATS_SUCCESS: {
      let _state = state
        .setIn(['self', action.fragment, 'result'], ResultEnum.success)
        .setIn(['self', action.fragment, 'skip'], action.skip)
        .setIn(['self', action.fragment, 'code'], action.code)
        .setIn(['self', 'count'], action.count);

      if (_state.getIn(['self', action.fragment, 'reload'])) {
        _state = _state.setIn(['self', action.fragment, 'data'], action.chats);
      } else {
        _state = _state.updateIn(
          ['self', action.fragment, 'data'],
          (data) => {
            return data.concat(action.chats);
          }
        );
      }

      return _state;
    }
    case constant.GET_CHATS_FAILED: {
      let _state = state;

      if (state.getIn(['self', action.fragment, 'reload'])) {
        _state = _state.setIn(
          ['self', action.fragment],
          {
            result: ResultEnum.failed,
            code: action.code,
            data: []
          }
        );
      }

      _state = _state
        .setIn(['self', action.fragment, 'result'], ResultEnum.failed)
        .setIn(['self', action.fragment, 'code'], action.code);

      if (action.count) {
        _state = _state.setIn(['self', 'count'], action.count);
      }

      return _state;
    }
    case constant.ACCEPT_CHAT_REQUEST: {
      return state.setIn(
        ['actions', 'accept', action.chatID],
        {
          type: action.accept.accept.is ? constantCommon.ACCEPT : constantCommon.NOT_ACCEPT,
          result: ResultEnum.wait
        }
      );
    }
    case constant.GET_CHAT_ID_REQUEST: {
      return state.setIn(['detail', 'result'], ResultEnum.wait);
    }
    case constant.GET_CHAT_ID_SUCCESS: {
      return state
        .set(
          'detail',
          {
            result: ResultEnum.success,
            code: action.code,
            data: action.chat
          }
        )
        .setIn(['self', 'count'], action.count);
    }
    case constant.GET_CHAT_ID_FAILED: {
      return state.set(
        'detail',
        {
          result: ResultEnum.failed,
          code: action.code
        }
      );
    }
    case constant.ACCEPT_CHAT_SUCCESS: {
      let _state = state
        .updateIn(
          ['self', 'wait', 'data'],
          (data) => {
            const index = data.findIndex((chat) => {
              return chat._id === action.chatID;
            });

            return data.splice(index, 1);
          }
        )
        .updateIn(
          ['self', 'wait', 'skip'],
          (skip) => {
            return skip ? skip - 1 : 0;
          }
        )
        .updateIn(
          ['self', 'count'],
          (count) => {
            count.wait -= 1;

            return count;
          }
        )
        .updateIn(
          ['actions', 'accept', action.chatID],
          (accept) => {
            return {
              result: ResultEnum.success,
              code: action.code,
              type: accept.type
            };
          }
        );

      if (
        _state.getIn([
          'actions',
          'accept',
          action.chatID,
          'type'])
        === constantCommon.ACCEPT
      ) {
        _state = _state
          .updateIn(
            ['self', 'success', 'data'],
            (data) => {
              return fromJS([action.chat]).concat(data);
            }
          )
          .updateIn(
            ['self', 'success', 'skip'],
            (skip) => {
              return skip + 1;
            }
          );
      }

      return _state;
    }
    case constant.ACCEPT_CHAT_FAILED: {
      return state.updateIn(
        ['actions', 'accept', action.chatID],
        (accept) => {
          return {
            result: ResultEnum.failed,
            code: action.code,
            type: accept.type,
          };
        }
      );
    }
    case constant.DELETE_CHAT_REQUEST: {
      return state
        .setIn(
          ['actions', 'delete', action.chatID],
          {
            result: ResultEnum.wait,
            fragment: action.fragment
          }
        );
    }
    case constant.DELETE_CHAT_SUCCESS: {
      return state
        .updateIn(
          ['actions', 'delete', action.chatID],
          (create) => {
            create.result = ResultEnum.success;
            create.code = action.code;

            return create;
          }
        )
        .updateIn(
          ['self', 'success', 'data'],
          (data) => {
            const index = data.findIndex((chat) => {
              return chat._id === action.chatID;
            });

            data.splice(index, 1);

            return data.splice(index, 1);
          }
        )
        .updateIn(
          ['self', 'wait', 'skip'],
          (skip) => {
            return skip ? skip - 1 : 0;
          }
        );
    }
    case constant.DELETE_CHAT_FAILED: {
      return state
        .updateIn(
          ['actions', 'delete', action.chatID],
          (create) => {
            create.result = ResultEnum.failed;
            create.code = action.code;

            return create;
          }
        );
    }
    case constant.GET_CHAT_REQUEST: {
      return state.setIn(
        ['actions', 'get'],
        {
          result: ResultEnum.wait,
          reload: action.reload
        }
      );
    }
    case constant.GET_CHAT_SUCCESS: {
      if (state.getIn(['actions', 'get', 'reload'])) {
        const chatIndex = state.getIn(['self', 'success', 'data']).findIndex((value) => {
          return value ? value._id === action.chat._id : false;
        });

        return state
          .setIn(
            ['actions', 'get'],
            {
              result: ResultEnum.success,
              code: action.code,
            }
          )
          .setIn(['self', 'success', 'data', chatIndex], action.chat)
          .setIn(['self', 'count'], action.count);
      }

      return state
        .setIn(
          ['actions', 'get'],
          {
            result: ResultEnum.success,
            code: action.code,
          }
        )
        .updateIn(
          ['self', 'count', 'wait'],
          (wait) => {
            return wait + 1;
          }
        )
        .updateIn(
          ['self', 'wait', 'data'],
          (data) => {
            return fromJS([action.chat]).concat(data);
          }
        )
        .updateIn(
          ['self', 'wait', 'skip'],
          (skip) => {
            return skip + 1;
          }
        );
    }
    case constant.GET_MESSAGES_CHAT_REQUEST: {
      const get = {
        result: ResultEnum.wait,
        max: action.max
      };

      return state.getIn(['actions', 'messages', action.chatID])
        ? state.setIn(['actions', 'messages', action.chatID, 'get'], get)
        : state.setIn(['actions', 'messages', action.chatID], { get });
    }
    case constant.GET_MESSAGES_CHAT_SUCCESS: {
      return state
        .setIn(
          ['actions', 'messages', action.chatID, 'get'],
          {
            result: ResultEnum.success,
            code: action.code,
            max: action.max
          }
        )
        .updateIn(
          ['detail', 'data'],
          (data) => {
            data.messages = action.messages.concat(data.messages);

            return data;
          }
        );
    }
    case constant.GET_MESSAGES_CHAT_FAILED: {
      return state.updateIn(
        ['actions', 'messages', action.chatID, 'get'],
        (get) => {
          return {
            result: ResultEnum.failed,
            code: action.code,
            max: get.max,
          };
        }
      );
    }
    case constant.MESSAGE_CHAT_REQUEST: {
      const post = {
        result: ResultEnum.wait,
      };

      return state.getIn(['actions', 'messages', action.to])
        ? state.setIn(['actions', 'messages', action.to, 'post'], post)
        : state.setIn(['actions', 'messages', action.to], { post });
    }
    case constant.MESSAGE_CHAT_SUCCESS: {
      return state
        .setIn(
          ['actions', 'messages', action.chatID, 'post'],
          {
            result: ResultEnum.success,
            code: action.code,
            dataID: action.data._id
          }
        )
        .updateIn(
          ['actions', 'messages', action.chatID, 'get', 'max'],
          (max) => {
            if (max === -1) {
              max = 0;
            }

            return max;
          }
        )
        .updateIn(
          ['detail', 'data'],
          (data) => {
            data.messages.push(action.data);

            return data;
          }
        );
    }
    case constant.MESSAGE_CHAT_FAILED: {
      return state.setIn(
        ['actions', 'messages', action.chatID, 'post'],
        {
          result: ResultEnum.failed,
          code: action.code
        }
      );
    }
    case constant.SEEN_CHAT_SUCCESS: {
      return state
        .setIn(
          ['actions', 'messages', action.chatID, 'put'],
          {
            result: ResultEnum.success,
            code: action.code
          }
        )
        .updateIn(
          ['detail', 'data'],
          (data) => {
            data.users = action.users;

            return data;
          }
        );
    }
    case constant.SEEN_CHAT_FAILED: {
      return state.setIn(
        ['actions', 'messages', action.chatID, 'put'],
        {
          result: ResultEnum.failed,
          code: action.code
        }
      );
    }
    case constant.RESERT_CHAT_STORE: {
      if (action.fragment) {
        let _state = state;

        if (action.fragment === constantCommon.DETAIL) {
          _state = _state.setIn(
            ['actions', 'messages'],
            initialState.getIn(['actions', 'messages'])
          );
        }

        _state = _state.set(
          action.fragment,
          initialState.get(action.fragment)
        );

        return _state;
      }

      return initialState;
    }

    default:
      return state;
  }
};
module.exports = reducer;
