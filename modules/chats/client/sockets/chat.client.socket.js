const {
  getChatRequest,
  messageChatSuccess,
  messageChatFailed,
  seenChatSuccess,
  seenChatFailed
} = require('../actions/chat.client.action');
const {
  SOCKET_CHAT_PUSH,
  SOCKET_CHAT_ADD,
  SOCKET_CHAT_SEEN,
  SOCKET_CHAT_UPDATE
} = require('../../../../commons/modules/chats/datas/socket.data');
const { emitSockets } = require('../../../core/client/lib/socket-io');
const { createSocketData } = require('../../../..//commons/utils/middleware');

const init = (dispatch, socket) => {
  socket.on(SOCKET_CHAT_PUSH, (body) => {
    dispatch(getChatRequest(body));
  });

  socket.on(SOCKET_CHAT_ADD, (response) => {
    if (response.status === 200) {
      dispatch(messageChatSuccess(response.bodyRes));

      emitSockets([
        createSocketData(
          [SOCKET_CHAT_SEEN],
          response.bodyRes.chatID,
          { chatID: response.bodyRes.chatID }
        )
      ]);
    } else {
      dispatch(messageChatFailed(response.bodyRes));
    }
  });

  socket.on(SOCKET_CHAT_SEEN, (response) => {
    if (response.status === 200) {
      dispatch(seenChatSuccess(response.bodyRes));
    } else {
      dispatch(seenChatFailed(response.bodyRes));
    }
  });

  socket.on(SOCKET_CHAT_UPDATE, (body) => {
    dispatch(getChatRequest(body, true));
  });
};
module.exports = init;
