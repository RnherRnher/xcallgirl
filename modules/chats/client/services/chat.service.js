const request = require('superagent');
const url = require('../../../../commons/modules/chats/datas/url.data');

const getChatID = ({ chatID, type }) => {
  return new Promise((resovle, reject) => {
    request
      .get(url.CHATS_GET_ID.replace(':id', chatID))
      .accept('application/json')
      .set('X-Requested-With', 'XMLHttpRequest')
      .query({ type })
      .end((err, res) => {
        if (err) {
          return reject(res);
        }

        return resovle(res);
      });
  });
};
module.exports.getChatID = getChatID;

const createChat = ({ csrfToken, type, create }) => {
  return new Promise((resovle, reject) => {
    request
      .post(url.CHATS)
      .type('application/json')
      .accept('application/json')
      .set('X-Requested-With', 'XMLHttpRequest')
      .query({ type })
      .set('csrf-token', csrfToken)
      .send(create)
      .end((err, res) => {
        if (err) {
          return reject(res);
        }

        return resovle(res);
      });
  });
};
module.exports.createChat = createChat;

const getChats = ({ type, fragment, skip }) => {
  return new Promise((resovle, reject) => {
    request
      .get(url.CHATS_SEARCH)
      .accept('application/json')
      .set('X-Requested-With', 'XMLHttpRequest')
      .query({ type, fragment, skip })
      .end((err, res) => {
        if (err) {
          return reject(res);
        }

        return resovle(res);
      });
  });
};
module.exports.getChats = getChats;

const acceptChat = ({ csrfToken, chatID, accept }) => {
  return new Promise((resovle, reject) => {
    request
      .post(url.CHATS_ACCEPT_ID.replace(':id', chatID))
      .type('application/json')
      .accept('application/json')
      .set('X-Requested-With', 'XMLHttpRequest')
      .set('csrf-token', csrfToken)
      .send(accept)
      .end((err, res) => {
        if (err) {
          return reject(res);
        }

        return resovle(res);
      });
  });
};
module.exports.acceptChat = acceptChat;

const deleteChat = ({ csrfToken, chatID }) => {
  return new Promise((resovle, reject) => {
    request
      .delete(url.CHATS_GET_ID.replace(':id', chatID))
      .accept('application/json')
      .set('X-Requested-With', 'XMLHttpRequest')
      .set('csrf-token', csrfToken)
      .end((err, res) => {
        if (err) {
          return reject(res);
        }

        return resovle(res);
      });
  });
};
module.exports.deleteChat = deleteChat;

const getMessagesChat = ({ chatID, max }) => {
  return new Promise((resovle, reject) => {
    request
      .get(url.CHATS_MESSAGE_ID.replace(':id', chatID))
      .query({ max })
      .accept('application/json')
      .set('X-Requested-With', 'XMLHttpRequest')
      .end((err, res) => {
        if (err) {
          return reject(res);
        }

        return resovle(res);
      });
  });
};
module.exports.getMessagesChat = getMessagesChat;
