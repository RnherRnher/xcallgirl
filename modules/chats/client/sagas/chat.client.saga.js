const nprogress = require('nprogress');
const api = require('../services/chat.service');
const action = require('../actions/chat.client.action');
const constant = require('../constants/chat.client.constant');
const url = require('../../../core/client/utils/url');
const constantCommon = require('../../../../commons/utils/constant');
const {
  takeLatest,
  put,
  call,
  takeEvery
} = require('redux-saga/effects');
const { push } = require('connected-react-router/immutable');
const { csrfRequest } = require('../../../core/client/services/core.client.service');
const { pushAlertSnackbar } = require('../../../core/client/actions/core.client.action');
const { emitSockets } = require('../../../core/client/lib/socket-io');
const { SOCKET_CHAT_LISTEN, SOCKET_CHAT_ADD } = require('../../../../commons/modules/chats/datas/socket.data');
const { createSocketData } = require('../../../..//commons/utils/middleware');
const { initApp } = require('../../../core/client/actions/app.client.action');

const watchCreateChat = function* () {
  yield takeLatest(constant.CREATE_CHAT_REQUEST, function* ({ fragment, create }) {
    try {
      nprogress.start();

      const responseCSRF = yield call(csrfRequest);
      const response = yield call(
        api.createChat,
        {
          csrfToken: responseCSRF.body.csrfToken,
          type: fragment,
          create
        }
      );

      yield put(action.createChatSuccess(response.body));

      yield call(emitSockets, response.body.sockets);

      yield put(action.listenChat(constantCommon.ADD, response.body.chat._id + response.body.chat._id));

      yield put(push(`${url.CHAT_GET}?id=${response.body.chat._id}`));
    } catch (error) {
      if (error && error.body) {
        yield put(action.createChatFailed(error.body));
        yield put(pushAlertSnackbar({
          variant: constantCommon.ERROR,
          code: error.body.code
        }));
      } else {
        yield put(initApp());
      }
    } finally {
      nprogress.done();
    }
  });
};
module.exports.watchCreateChat = watchCreateChat;

const watchGetChats = function* () {
  yield takeEvery(
    constant.GET_CHATS_REQUEST,
    function* ({
      typee,
      fragment,
      skip
    }) {
      try {
        nprogress.start();

        const response = yield call(
          api.getChats,
          {
            type: typee,
            fragment,
            skip
          }
        );

        yield put(action.getChatsSuccess(response.body));

        if (response.body.chats) {
          for (let i = 0; i < response.body.chats.length; i++) {
            const chat = response.body.chats[i];
            yield put(action.listenChat(constantCommon.ADD, chat._id + chat._id));
          }
        }
      } catch (error) {
        if (error && error.body) {
          yield put(action.getChatsFailed(error.body));
          if (skip !== 0) {
            yield put(pushAlertSnackbar({
              variant: constantCommon.ERROR,
              code: error.body.code
            }));
          }
        } else {
          yield put(initApp());
        }
      } finally {
        nprogress.done();
      }
    }
  );
};
module.exports.watchGetChats = watchGetChats;

const watchAcceptChat = function* () {
  yield takeEvery(constant.ACCEPT_CHAT_REQUEST, function* ({ chatID, accept }) {
    try {
      const responseCSRF = yield call(csrfRequest);
      const response = yield call(
        api.acceptChat,
        {
          csrfToken: responseCSRF.body.csrfToken,
          chatID,
          accept
        }
      );

      yield put(action.acceptChatSuccess(response.body));
      yield put(pushAlertSnackbar({
        variant: constantCommon.SUCCESS,
        code: response.body.code
      }));

      if (accept.accept.is) {
        yield put(push(`${url.CHAT_GET}?id=${response.body.chatID}`));
      }
    } catch (error) {
      if (error && error.body) {
        yield put(action.acceptChatFailed(error.body));
        yield put(pushAlertSnackbar({
          variant: constantCommon.ERROR,
          code: error.body.code
        }));
      } else {
        yield put(initApp());
      }
    }
  });
};
module.exports.watchAcceptChat = watchAcceptChat;

const watchDeleteChat = function* () {
  yield takeEvery(constant.DELETE_CHAT_REQUEST, function* ({ chatID }) {
    try {
      const responseCSRF = yield call(csrfRequest);
      const response = yield call(
        api.deleteChat,
        {
          csrfToken: responseCSRF.body.csrfToken,
          chatID,
        }
      );

      yield put(action.deleteChatSuccess(response.body));
      yield put(pushAlertSnackbar({
        variant: constantCommon.SUCCESS,
        code: response.body.code
      }));
    } catch (error) {
      if (error && error.body) {
        yield put(action.deleteChatFailed(error.body));
        yield put(pushAlertSnackbar({
          variant: constantCommon.ERROR,
          code: error.body.code
        }));
      } else {
        yield put(initApp());
      }
    }
  });
};
module.exports.watchDeleteChat = watchDeleteChat;

const watchGetChat = function* () {
  yield takeEvery(constant.GET_CHAT_REQUEST, function* ({ chatID, fragment }) {
    const response = yield call(
      api.getChatID,
      {
        chatID,
        type: fragment
      }
    );

    yield put(action.getChatSuccess(response.body));
  });
};
module.exports.watchGetChat = watchGetChat;

const watchGetChatID = function* () {
  yield takeLatest(constant.GET_CHAT_ID_REQUEST, function* ({ chatID, fragment }) {
    try {
      const response = yield call(
        api.getChatID,
        {
          chatID,
          type: fragment
        }
      );

      yield put(action.getChatIDSuccess(response.body));
    } catch (error) {
      if (error && error.body) {
        yield put(action.getChatIDFailed(error.body));
        yield put(pushAlertSnackbar({
          variant: constantCommon.ERROR,
          code: error.body.code
        }));
      } else {
        yield put(initApp());
      }
    }
  });
};
module.exports.watchGetChatID = watchGetChatID;

const watchGetMessagesChat = function* () {
  yield takeLatest(constant.GET_MESSAGES_CHAT_REQUEST, function* ({ chatID, max }) {
    try {
      nprogress.start();

      const response = yield call(
        api.getMessagesChat,
        {
          chatID,
          max
        }
      );

      yield put(action.getMessagesChatSuccess(response.body));
    } catch (error) {
      if (error && error.body) {
        yield put(action.getMessagesChatFailed(error.body));
        if (max !== -1) {
          yield put(pushAlertSnackbar({
            variant: constantCommon.ERROR,
            code: error.body.code
          }));
        }
      } else {
        yield put(initApp());
      }
    } finally {
      nprogress.done();
    }
  });
};
module.exports.watchGetMessagesChat = watchGetMessagesChat;

const watchMessageChat = function* () {
  yield takeEvery(constant.MESSAGE_CHAT_REQUEST, function* ({ to, body }) {
    yield call(
      emitSockets,
      [
        createSocketData(
          [SOCKET_CHAT_ADD],
          to,
          body
        )
      ]
    );
  });
};
module.exports.watchMessageChat = watchMessageChat;

const watchListenChat = function* () {
  yield takeEvery(constant.LISTEN_CHAT, function* ({ fragment, chatID }) {
    yield call(
      emitSockets,
      [{
        names: [SOCKET_CHAT_LISTEN],
        data: {
          type: fragment,
          id: chatID
        },
      }]
    );
  });
};
module.exports.watchListenChat = watchListenChat;
