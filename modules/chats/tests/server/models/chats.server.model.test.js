const mongoose = require('mongoose');
const chai = require('chai');
const path = require('path');
const constant = require(path.resolve('./commons/utils/constant'));

describe('### Chats Server Model Tests', () => {
  let Chat = null;
  let chat = null;

  before((done) => {
    Chat = mongoose.model('Chat');

    done();
  });

  beforeEach((done) => {
    chat = { type: constant.PRIVCY };

    done();
  });

  afterEach((done) => {
    Chat.deleteMany({}, (err) => {
      chai.assert.notExists(err);
      done();
    });
  });

  it('Should success new chat', (done) => {
    const _chat = new Chat(chat);
    _chat.save((err) => {
      chai.assert.notExists(err);
      done();
    });
  });

  it('Should failed new chat with not type', (done) => {
    chat.type = undefined;
    const _chat = new Chat(chat);
    _chat.save((err) => {
      chai.assert.exists(err);
      done();
    });
  });

  it('Should success accept user chat', (done) => {
    const _chat = new Chat(chat);
    _chat.acceptUser(mongoose.Types.ObjectId(), true)
      .then((existUser) => {
        _chat.save((err) => {
          chai.assert.notExists(err);
          chai.assert.deepEqual(_chat.users.length, 1);
          chai.assert.isTrue(_chat.users[0].accept.is);
          chai.assert.isFalse(_chat.users[0].view.is);
          chai.assert.isFalse(_chat.users[0].delete.is);

          done();
        });
      });
  });

  it('Should success add data chat', (done) => {
    const _chat = new Chat(chat);

    const userID = mongoose.Types.ObjectId();
    const type = constant.TEXT;
    const data = 'Hi';

    _chat.addData(userID, type, data)
      .then((chatData) => {
        chai.assert.deepEqual(chatData.userID, userID);
        chai.assert.deepEqual(chatData.type, type);
        chai.assert.deepEqual(chatData.data, data);

        _chat.save((err) => {
          chai.assert.notExists(err);
          chai.assert.deepEqual(_chat.datas.length, 1);

          done();
        });
      });
  });
});
