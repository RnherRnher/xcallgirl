const path = require('path');
const mongoose = require('mongoose');
const request = require('supertest');
const chai = require('chai');
const _ = require('lodash');
const express = require(path.resolve('./config/lib/express'));
const chatsUrl = require(path.resolve('./commons/modules/chats/datas/url.data'));
const usersUrl = require(path.resolve('./commons/modules/users/datas/url.data'));
const constant = require(path.resolve('./commons/utils/constant'));

describe('### Chats Server Routes Tests', () => {
  let app = null;
  let agent = null;

  let Chat = null;

  let ActivityHistory = null;

  let User = null;
  let user = null;
  let someUser = null;

  const password = 'password';

  let chatID = null;
  let chatData = null;
  const isAccept = false;

  /**
   * @overview Kiểm tra tạo trò chuyện
   *
   * @param {ObjectId} userID1
   * @param {ObjectId} userID2
   * @param {ObjectId} chatID
   * @param {Object} chatData
   * @param {Function} done
   * @param {Boolean} isExist
   */
  const testCreatChat = (userID1, userID2, chatID, chatData, done, isExist = true) => {
    Chat
      .findOne({
        type: chatData.type,
        'users._id': [userID1, userID2],
      })
      .exec((err, chat) => {
        chai.assert.notExists(err);

        if (isExist) {
          chai.assert.exists(chat);
          chai.assert.isTrue(chat._id.equals(chatID));

          chat.users.forEach((user) => {
            if (user._id.equals(userID1)) {
              chai.assert.isTrue(user.accept.is);
            } else if (user._id.equals(userID2)) {
              chai.assert.isFalse(user.accept.is);
            }
          });
        } else {
          chai.assert.notExists(chat);
        }

        ActivityHistory
          .findOne({
            userID: userID1,
            type: constant.CHAT,
            result: constant.SUCCESS,
            content: 'chat.create.is',
          })
          .exec((err, activityHistory) => {
            chai.assert.notExists(err);

            if (isExist) {
              chai.assert.exists(activityHistory);
            } else {
              chai.assert.notExists(activityHistory);
            }

            if (done) done();
          });
      });
  };

  /**
   * @overview Kiểm tra xoá trò chuyện
   *
   * @param {ObjectId} userID1
   * @param {ObjectId} userID2
   * @param {ObjectId} chatID
   * @param {Object} chatData
   * @param {Function} done
   * @param {Boolean} isExist
   */
  const testDeleteChat = (userID, chatID, chatData, done, isExist = true) => {
    Chat
      .findOne({
        type: chatData.type,
        'users._id': _.concat(userID, chatData.usersID),
      })
      .exec((err, chat) => {
        chai.assert.notExists(err);
        chai.assert.exists(chat);
        chai.assert.isTrue(chat._id.equals(chatID));

        chat.users.forEach((user) => {
          if (user._id.equals(userID)) {
            if (isExist) {
              chai.assert.isTrue(user.delete.is);
            } else {
              chai.assert.isFalse(user.delete.is);
            }
          }
        });

        if (done) done();
      });
  };

  /**
   * @overview Kiểm tra chấp nhận trò chuyện
   *
   * @param {ObjectId} userID
   * @param {ObjectId} chatID
   * @param {Boolean} isAccept
   * @param {Function} done
   * @param {Boolean} isExist
   */
  const testAccept = (userID, chatID, isAccept, done, isExist = true) => {
    Chat
      .findById({ _id: chatID })
      .exec((err, chat) => {
        chai.assert.notExists(err);
        chai.assert.exists(chat);

        chat.users.forEach((user) => {
          if (user._id.equals(userID)) {
            if (isExist) {
              chai.assert.isTrue(user.accept.is === isAccept);
            } else {
              chai.assert.isFalse(user.accept.is === isAccept);
            }
          }
        });

        ActivityHistory
          .findOne({
            userID,
            type: constant.CHAT,
            result: constant.SUCCESS,
            content: `chat.accept.${isAccept}`,
          })
          .exec((err, activityHistory) => {
            chai.assert.notExists(err);

            if (isExist) {
              chai.assert.exists(activityHistory);
            } else {
              chai.assert.notExists(activityHistory);
            }

            if (done) done();
          });
      });
  };

  const signin = (numberPhone, password) => {
    return agent.post(usersUrl.AUTH_SIGNIN)
      .set('X-Requested-With', 'XMLHttpRequest')
      .send({
        numberPhone: `${numberPhone.code} ${numberPhone.number}`,
        password,
      });
  };

  const createChat = (chatData) => {
    return new Promise((resolve, reject) => {
      signin(user.numberPhone, password)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return reject(err);
          }

          agent.post(chatsUrl.CHATS)
            .set('X-Requested-With', 'XMLHttpRequest')
            .query({
              type: chatData.type,
            })
            .send({
              usersID: chatData.usersID,
            })
            .expect(200)
            .end((err, res) => {
              if (err) {
                return reject(err);
              }

              chai.assert.exists(res.body.chat);
              chatID = res.body.chat._id;

              return resolve(res.body.chat);
            });
        });
    });
  };

  before((done) => {
    app = express.init(mongoose.connection.db);
    agent = request.agent(app);

    Chat = mongoose.model('Chat');
    User = mongoose.model('User');
    ActivityHistory = mongoose.model('ActivityHistory');

    done();
  });

  beforeEach((done) => {
    user = {
      name: {
        first: 'first',
        last: 'last',
        display: 'last first',
        nick: 'user_one',
      },
      numberPhone: {
        code: '+84',
        number: '0000000000',
      },
      gender: constant.MALE,
      birthday: {
        day: 1,
        month: 1,
        year: 2000,
      },
      password,
      address: {
        country: 'country',
        city: 'city',
        county: 'county',
        local: 'local',
      },
      settings: {
        general: {
          language: constant.VI,
        },
      },
    };

    someUser = _.cloneDeep(user);
    someUser.name.nick = 'some_user';
    someUser.numberPhone.number = '1111111111';

    user = new User(user);
    someUser = new User(someUser);

    chatData = {
      type: constant.PRIVCY,
      usersID: [someUser._id],
    };

    user.save((err) => {
      chai.assert.notExists(err);
      someUser.save((err) => {
        chai.assert.notExists(err);
        done();
      });
    });
  });

  afterEach((done) => {
    User.deleteMany({}, (err) => {
      chai.assert.notExists(err);
      Chat.deleteMany({}, (err) => {
        chai.assert.notExists(err);
        ActivityHistory.deleteMany({}, (err) => {
          chai.assert.notExists(err);
          done();
        });
      });
    });
  });

  describe('## Tạo trò chuyện', () => {
    it('should success create chat', (done) => {
      createChat(chatData)
        .then((chat) => {
          testCreatChat(user._id, someUser._id, chatID, chatData, done);
        })
        .catch((err) => {
          return done(err);
        });
    });

    it('should fialed create chat with not signin', (done) => {
      agent.post(chatsUrl.CHATS)
        .set('X-Requested-With', 'XMLHttpRequest')
        .query({
          type: chatData.type,
        })
        .send({
          usersID: chatData.usersID,
        })
        .expect(401)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          chai.assert.deepEqual(res.body.code, 'user.permission.notExist');
          testCreatChat(user._id, someUser._id, chatID, chatData, done, false);
        });
    });
  });

  describe('## Lấy trò chuyện theo ID', () => {
    it('should success get chat', (done) => {
      createChat(chatData)
        .then((chat) => {
          agent.get(chatsUrl.CHATS_GET_ID.replace(':id', chatID))
            .set('X-Requested-With', 'XMLHttpRequest')
            .query({
              type: constant.FULL,
            })
            .expect(200)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.exists(res.body.chat);
              return done();
            });
        }, (err) => {
          return done(err);
        });
    });

    it('should failed get chat with not signin', (done) => {
      createChat(chatData)
        .then((chat) => {
          agent.get(usersUrl.AUTH_SIGNOUT)
            .expect(200)
            .set('X-Requested-With', 'XMLHttpRequest')
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              agent.get(chatsUrl.CHATS_GET_ID.replace(':id', chatID))
                .set('X-Requested-With', 'XMLHttpRequest')
                .query({
                  type: constant.FULL,
                })
                .expect(401)
                .end((err, res) => {
                  if (err) {
                    return done(err);
                  }

                  chai.assert.deepEqual(res.body.code, 'user.permission.notExist');
                  return done();
                });
            });
        }, (err) => {
          return done(err);
        });
    });

    it('should failed get chat with bad id', (done) => {
      createChat(chatData)
        .then((chat) => {
          agent.get(chatsUrl.CHATS_GET_ID.replace(':id', mongoose.Types.ObjectId()))
            .set('X-Requested-With', 'XMLHttpRequest')
            .query({
              type: constant.FULL,
            })
            .expect(404)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.code, 'chat.notExist');
              return done();
            });
        }, (err) => {
          return done(err);
        });
    });
  });

  describe('## Tìm kiếm trò chuyện', () => {
    beforeEach((done) => {
      createChat(chatData)
        .then((chat) => {
          return done();
        }, (err) => {
          return done(err);
        });
    });

    it('should success auto search chat', (done) => {
      agent.get(chatsUrl.CHATS_SEARCH)
        .set('X-Requested-With', 'XMLHttpRequest')
        .query({
          type: constant.AUTO,
          fragment: constant.SUCCESS,
          skip: 0
        })
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          chai.assert.deepEqual(res.body.skip, 1);
          chai.assert.deepEqual(res.body.chats.length, 1);

          return done();
        });
    });

    it('should failed auto search chat with not signin', (done) => {
      agent.get(usersUrl.AUTH_SIGNOUT)
        .expect(200)
        .set('X-Requested-With', 'XMLHttpRequest')
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.get(chatsUrl.CHATS_SEARCH)
            .set('X-Requested-With', 'XMLHttpRequest')
            .query({
              type: constant.AUTO,
              fragment: console.WAIT
            })
            .query({
              max: 0,
            })
            .expect(401)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.code, 'user.permission.notExist');
              return done();
            });
        });
    });

    it('should failed auto search chat with bad query', (done) => {
      agent.get(chatsUrl.CHATS_SEARCH)
        .set('X-Requested-With', 'XMLHttpRequest')
        .expect(400)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          chai.assert.deepEqual(res.body.code, 'data.notVaild');
          return done();
        });
    });
  });

  describe('## Xoá trò chuyện', () => {
    beforeEach((done) => {
      createChat(chatData)
        .then((chat) => {
          return done();
        }, (err) => {
          return done(err);
        });
    });

    it('should success delete chat', (done) => {
      agent.delete(chatsUrl.CHATS_GET_ID.replace(':id', chatID))
        .set('X-Requested-With', 'XMLHttpRequest')
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          testDeleteChat(user._id, chatID, chatData, done);
        });
    });

    it('should failed delete chat with not signin', (done) => {
      agent.get(usersUrl.AUTH_SIGNOUT)
        .expect(200)
        .set('X-Requested-With', 'XMLHttpRequest')
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.delete(chatsUrl.CHATS_GET_ID.replace(':id', chatID))
            .set('X-Requested-With', 'XMLHttpRequest')
            .expect(401)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.code, 'user.permission.notExist');
              testDeleteChat(user._id, chatID, chatData, done, false);
            });
        });
    });

    it('should failed delete chat with bad id', (done) => {
      agent.delete(chatsUrl.CHATS_GET_ID.replace(':id', mongoose.Types.ObjectId()))
        .set('X-Requested-With', 'XMLHttpRequest')
        .expect(404)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          chai.assert.deepEqual(res.body.code, 'chat.delete.failed');
          testDeleteChat(user._id, chatID, chatData, done, false);
        });
    });
  });

  describe('## Chấp nhận trò chuyện', () => {
    beforeEach((done) => {
      createChat(chatData)
        .then((chat) => {
          return done();
        }, (err) => {
          return done(err);
        });
    });

    it('should success accept chat', (done) => {
      agent.post(chatsUrl.CHATS_ACCEPT_ID.replace(':id', chatID))
        .set('X-Requested-With', 'XMLHttpRequest')
        .expect(200)
        .send({
          accept: {
            is: isAccept,
          },
        })
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          testAccept(user._id, chatID, isAccept, done);
        });
    });

    it('should failed accept chat with not signin', (done) => {
      agent.get(usersUrl.AUTH_SIGNOUT)
        .expect(200)
        .set('X-Requested-With', 'XMLHttpRequest')
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.post(chatsUrl.CHATS_ACCEPT_ID.replace(':id', chatID))
            .set('X-Requested-With', 'XMLHttpRequest')
            .expect(401)
            .send({
              accept: {
                is: isAccept,
              },
            })
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.code, 'user.permission.notExist');
              testAccept(user._id, chatID, isAccept, done, false);
            });
        });
    });

    it('should failed accept chat with bad id', (done) => {
      agent.post(chatsUrl.CHATS_ACCEPT_ID.replace(':id', mongoose.Types.ObjectId()))
        .set('X-Requested-With', 'XMLHttpRequest')
        .expect(404)
        .send({
          accept: {
            is: isAccept,
          },
        })
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          chai.assert.deepEqual(res.body.code, 'chat.notExist');
          testAccept(user._id, chatID, isAccept, done, false);
        });
    });
  });
});
