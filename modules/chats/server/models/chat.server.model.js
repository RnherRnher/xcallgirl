const async = require('async');
const path = require('path');
const mongoose = require('mongoose');
const _ = require('lodash');
const regular = require(path.resolve('./commons/utils/regular-expression'));
const constant = require(path.resolve('./commons/utils/constant'));
const { validateModelChat } = require(path.resolve('./utils/validate'));

const { Schema } = mongoose;

const validates = validateModelChat();

const ChatSchema = new Schema({
  type: {
    type: String,
    required: true,
    enum: validates.type,
  },
  users: {
    type: [{
      _id: {
        type: mongoose.SchemaTypes.ObjectId,
        required: true
      },
      accept: {
        is: {
          type: Boolean,
          required: true,
        },
        date: {
          type: Date,
          required: true,
        },
      },
      view: {
        is: {
          type: Boolean,
          required: true,
        },
        date: {
          type: Date,
          required: true,
        },
      },
      delete: {
        is: {
          type: Boolean,
          required: true,
        },
        date: {
          type: Date,
          required: true,
        },
      },
    }],
  },
  datas: {
    type: [{
      _id: {
        type: mongoose.SchemaTypes.ObjectId,
        required: true,
        unique: true,
        sparse: true,
      },
      type: {
        type: String,
        required: true,
        enum: validates.dataType,
      },
      userID: {
        type: mongoose.SchemaTypes.ObjectId,
        required: true,
      },
      data: {
        type: String,
        required: true,
        trim: true,
        maxlength: regular.commons.stringLength.large,
      },
      initDate: {
        type: Date,
        required: true,
        default: Date.now
      },
    }],
  },
  initDate: {
    type: Date,
    required: true,
    default: Date.now
  },
  updateDate: {
    type: Date,
    required: true,
    default: Date.now
  },
});

/**
 * @overview Thêm tin nhắn vào trò chuyện
 *
 * @param {ObjectId} userID
 * @param {String} type
 * @param {Object} data
 * @returns {Promise}
 */
ChatSchema.methods.addData = function (
  userID,
  type,
  data
) {
  return new Promise((resolve, reject) => {
    const date = Date.now();

    const chatData = {
      _id: mongoose.Types.ObjectId(),
      userID,
      type,
      data,
      initDate: date
    };

    this.datas.push(chatData);
    this.updateDate = date;

    async.map(this.users, (user, done) => {
      if (user._id.equals(userID)) {
        user.view.is = true;
      } else {
        user.view.is = false;
      }
      user.view.date = date;

      done(null, user);
    }, (err, resultUsers) => {
      this.users = resultUsers;

      return resolve(chatData);
    });
  });
};

/**
 * @overview Thêm đã xem tin nhắn vào trò chuyện
 *
 * @param {ObjectId} userID
 * @returns {Promise}
 */
ChatSchema.methods.addView = function (userID) {
  return new Promise((resolve, reject) => {
    async.map(this.users, (user, done) => {
      if (user._id.equals(userID)) {
        if (!user.view.is) {
          user.view = {
            is: true,
            date: Date.now()
          };
        }

        done(new Error(user), null);
      }

      done(null, user);
    }, (err, resultUsers) => {
      if (err) {
        return resolve(err);
      }

      return reject();
    });
  });
};

/**
 * @overview Chấp nhận người dùng
 *
 * @param {ObjectId} userID
 * @param {Boolean} isAccept
 * @param {Boolean} isView
 * @param {Boolean} isDelete
 * @returns {Promise}
 *
 */
ChatSchema.methods.acceptUser = function (
  userID,
  isAccept,
  isView = false,
  isDelete = false
) {
  return new Promise((resolve, reject) => {
    let existUser = false;
    const date = Date.now();

    const userObject = {
      _id: userID,
      accept: {
        is: isAccept,
        date,
      },
      view: {
        is: isView,
        date
      },
      delete: {
        is: isDelete,
        date
      },
    };

    async.map(this.users, (user, done) => {
      if (user._id.equals(userID)) {
        existUser = true;

        done(null, userObject);
      } else {
        done(null, user);
      }
    }, (err, resUsers) => {
      if (existUser) {
        this.users = resUsers;
      } else {
        this.users.push(userObject);
      }

      return resolve(existUser);
    });
  });
};

/**
 * @overview Lấy thông tin cơ bản của trò chuyện
 *
 * @param   {ObjectId} userID
 * @returns {Promise}
 */
ChatSchema.methods.getShortcutsInfo = function (userID) {
  return new Promise((resolve, reject) => {
    const User = mongoose.model('User');

    const _this = this.toObject();

    if (_this.datas.length) {
      _this.datas = _this.datas[_this.datas.length - 1];
    } else {
      _this.datas = undefined;
    }

    async.map(_this.users, (u, done) => {
      User
        .findById(u._id, `-${constant.SALT} -${constant.PASSWORD}`)
        .exec((err, user) => {
          if (user) {
            if (user._id.equals(userID)) {
              _this.isView = u.view.is;
              _this.isAccept = u.accept.is;

              done(null, null);
            } else {
              done(null, user.getShortcutsInfo());
            }
          } else {
            done(null, User.getShortcutsInfoUndefined());
          }
        });
    }, (err, resultUsers) => {
      _this.users = resultUsers;

      return resolve(_this);
    });
  });
};

/**
 * @overview Lấy thông tin đầy đủ của trò chuyện
 *
 * @param   {ObjectId} userID
 * @returns {Promise}
 */
ChatSchema.methods.getFullInfo = function (userID) {
  return new Promise((resolve, reject) => {
    const User = mongoose.model('User');

    const _this = this.toObject();

    _this.datas = undefined;
    _this.messages = [];

    async.map(_this.users, (u, done) => {
      User
        .findById(u._id, `-${constant.SALT} -${constant.PASSWORD}`)
        .exec((err, user) => {
          if (user) {
            done(
              null,
              _.assignIn(
                u,
                user.getShortcutsInfo()
              )
            );
          } else {
            done(
              null,
              _.assignIn(
                u,
                user.getShortcutsInfoUndefined()
              )
            );
          }
        });
    }, (err, resultUsers) => {
      _this.users = resultUsers;

      return resolve(_this);
    });
  });
};

/**
 * @overview Lấy số lượng cuộc trò truyện
 *
 * @param   {ObjectId} userID
 * @returns {Promise}
 */
ChatSchema.statics.counts = function (userID) {
  return new Promise((resolve, reject) => {
    const Chat = mongoose.model('Chat');

    async.parallel({
      update: (callback) => {
        Chat
          .countDocuments({
            users: {
              $elemMatch: {
                _id: userID,
                'accept.is': true,
                'view.is': false,
              },
            },
          })
          .exec((err, count) => {
            callback(null, count);
          });
      },
      wait: (callback) => {
        Chat
          .countDocuments({
            users: {
              $elemMatch: {
                _id: userID,
                'accept.is': false,
                'view.is': false,
              },
            },
          })
          .exec((err, count) => {
            callback(null, count);
          });
      }
    }, (err, results) => {
      resolve(results);
    });
  });
};

mongoose.model('Chat', ChatSchema);
