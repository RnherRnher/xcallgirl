
const path = require('path');
const Acl = require('acl');
const constant = require(path.resolve('./commons/utils/constant'));
const url = require(path.resolve('./commons/modules/chats/datas/url.data'));

const MemoryBackend = Acl.memoryBackend;
const acl = new Acl(new MemoryBackend());

const invokeRolesPolicies = () => {
  acl.allow([{
    roles: [constant.USER, constant.PROVIDER, constant.ADMIN],
    allows: [{
      resources: url.CHATS,
      permissions: ['post'],
    }, {
      resources: url.CHATS_ACCEPT_ID,
      permissions: ['post'],
    }, {
      resources: url.CHATS_GET_ID,
      permissions: ['get', 'delete'],
    }, {
      resources: url.CHATS_SEARCH,
      permissions: ['get'],
    }, {
      resources: url.CHATS_MESSAGE_ID,
      permissions: ['get'],
    }]
  }]);
};
module.exports.invokeRolesPolicies = invokeRolesPolicies;

const isAllowed = (req, res, next) => {
  const roles = req.user ? req.user.roles.value : req.app.locals.defaultRoles;
  acl.areAnyRolesAllowed(roles, req.route.path, req.method.toLowerCase(), (err, isAllowed) => {
    let dataRes = null;

    if (!isAllowed) {
      dataRes = {
        status: 401,
        bodyRes: {
          code: 'user.permission.notExist',
        },
      };
    } else {
      return next(err);
    }

    return res
      .status(dataRes.status)
      .send(dataRes.bodyRes)
      .end();
  });
};
module.exports.isAllowed = isAllowed;
