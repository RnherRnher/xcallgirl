const path = require('path');
const policy = require('../policies/chat.server.policy');
const chats = require('../controllers/chats.server.controller');
const validate = require(path.resolve('./commons/modules/chats/validates/chat.validate'));
const middleware = require(path.resolve('./utils/middleware'));
const url = require(path.resolve('./commons/modules/chats/datas/url.data'));

const init = (app) => {
  app.route(url.CHATS_SEARCH)
    .all(policy.isAllowed)
    .get(middleware.paramsValidation(validate, 'search'), chats.search);
  app.route(url.CHATS_GET_ID)
    .all(policy.isAllowed)
    .get(middleware.paramsValidation(validate, 'get'), chats.get)
    .delete(middleware.paramsValidation(validate, 'deletee'), chats.deletee);

  app.route(url.CHATS)
    .all(policy.isAllowed)
    .post(middleware.paramsValidation(validate, 'create'), chats.create);
  app.route(url.CHATS_ACCEPT_ID)
    .all(policy.isAllowed)
    .post(middleware.paramsValidation(validate, 'accept'), chats.accept);
  app.route(url.CHATS_MESSAGE_ID)
    .all(policy.isAllowed)
    .get(middleware.paramsValidation(validate, 'messages'), chats.messages);
};
module.exports = init;
