const _ = require('lodash');
const authorization = require('./chats/chats.authorization.server.controller');
const action = require('./chats/chats.action.server.controller');

module.exports = _.assignIn(
  authorization,
  action
);
