const path = require('path');
const mongoose = require('mongoose');
const _ = require('lodash');
const async = require('async');
const logger = require(path.resolve('./config/lib/logger'));
const constant = require(path.resolve('./commons/utils/constant'));
const { createSocketData } = require(path.resolve('./commons/utils/middleware'));
const { SOCKET_CHAT_PUSH } = require(path.resolve('./commons/modules/chats/datas/socket.data'));

const Chat = mongoose.model('Chat');
const ActivityHistory = mongoose.model('ActivityHistory');

/**
 * @overview Lấy tin nhắn trò chuyện theo ID
 *
 * @param {Request} req
 * @param {Response} res
 * @param {Function} next
 *
 * @input  params { id: String }
 *         query  { max: Number }
 * @output        {
 *                   code: String,
 *                   chatID: {ObjectId}
 *                   messages?: Object,
 *                   max?: Number
 *                 }
 */
const messages = (req, res, next) => {
  const paramsReq = req.params;
  const queryReq = req.query;
  const userSelf = req.user;
  let dataRes = null;

  async.waterfall([
    (done) => {
      /* Tìm trò chuyện */

      if (userSelf) {
        Chat
          .findOne({
            _id: paramsReq.id,
            'users._id': userSelf._id,
          })
          .exec((err, chat) => {
            if (err || !chat) {
              // Trò chuyện không tồn tại
              dataRes = {
                status: 404,
                bodyRes: {
                  code: 'chat.notExist',
                },
              };

              done(err || new Error('Not chat exists'), dataRes);
            } else {
              done(null, chat);
            }
          });
      } else {
        // Chưa đăng nhập người dùng
        dataRes = {
          status: 401,
          bodyRes: {
            code: 'user.signin.notExist',
          },
        };

        done(new Error('User not signin'), dataRes);
      }
    }, (chat, done) => {
      /* Lấy thông tin chi tiết tin nhắn */

      let { max } = queryReq;
      if (max === -1 || max > chat.datas.length) {
        max = chat.datas.length;
      }

      const limit = req.app.locals.config.loadLimit;
      const min = (max - limit) > 0 ? (max - limit) : 0;

      const currentMessages = _.slice(chat.datas, min, max);

      if (currentMessages.length) {
        async.map(currentMessages, (data, callback) => {
          callback(null, data);
        }, (err, resultMessages) => {
          dataRes = {
            status: 200,
            bodyRes: {
              code: 'chat.message.exist',
              messages: resultMessages,
              max: min,
            },
          };

          done(null, dataRes);
        });
      } else {
        // Tin nhắn trò chuyện không tồn tại
        dataRes = {
          status: 404,
          bodyRes: {
            code: 'chat.message.notExist'
          },
        };

        done(new Error('Not message chat exists'), dataRes);
      }
    }], (err, dataRes) => {
      dataRes.bodyRes.chatID = paramsReq.id;

      return res
        .status(dataRes.status)
        .send(dataRes.bodyRes)
        .end();
    });
};
module.exports.messages = messages;

/**
 * @overview Tạo trò chuyện
 *
 * @param {Request} req
 * @param {Response} res
 * @param {Function} next
 *
 * @input  query  { type: String }
 *         body   { usersID?: [ObjectId] }
 * @output        {
 *                    code: String,
 *                    chat?: Object
 *                    sockets?: [{Object}]
 *                }
 */
const create = (req, res, next) => {
  const bodyReq = req.body;
  const queryReq = req.query;
  const userSelf = req.user;
  let dataRes = null;

  async.waterfall([
    (done) => {
      /* Tìm và cập nhật trò chuyện */

      if (userSelf) {
        const usersID = _.concat([userSelf._id], bodyReq.usersID);

        Chat
          .findOne(
            {
              type: queryReq.type,
              'users._id': {
                $all: usersID
              }
            }
          )
          .exec((err, chat) => {
            if (err || !chat) {
              const newChat = new Chat();
              newChat.type = queryReq.type;

              async.map(usersID, (userID, callback) => {
                newChat.acceptUser(userID, userSelf._id.equals(userID))
                  .then(() => {
                    callback(null);
                  });
              }, (err, resUsers) => {
                newChat.save((err) => {
                  if (err) {
                    // Tạo trò chuyện thất bại
                    dataRes = {
                      status: 400,
                      bodyRes: {
                        code: 'chat.create.failed',
                      },
                    };
                  } else {
                    dataRes = newChat;
                  }

                  done(err, dataRes, false);
                });
              });
            } else {
              // Trò chuyện tồn tại

              chat.acceptUser(userSelf._id, true, true)
                .then(() => {
                  chat.save((err) => {
                    if (err) {
                      // Chấp nhận trò chuyện thất bại
                      dataRes = {
                        status: 400,
                        bodyRes: {
                          code: 'chat.accept.failed',
                        },
                      };
                    } else {
                      dataRes = chat;
                    }

                    done(err, dataRes, true);
                  });
                });
            }
          });
      } else {
        // Chưa đăng nhập người dùng
        dataRes = {
          status: 401,
          bodyRes: {
            code: 'user.signin.notExist',
          },
        };

        done(new Error('User not signin'), dataRes);
      }
    }, (chat, isExist, done) => {
      /* Lấy thông tin trò chuyện */

      dataRes = {
        status: 200,
        bodyRes: {
          code: 'chat.create.success',
          sockets: []
        },
      };

      if (!isExist) {
        const activityHistory = new ActivityHistory({
          userID: userSelf._id,
          toURL: chat._id,
          type: constant.CHAT,
          result: constant.SUCCESS,
          content: 'chat.create.is',
        });
        activityHistory.save((err) => {
          if (err) {
            logger.logError(
              'chat.server.controller',
              'create',
              constant.ACTIVITY_HISTORY,
              activityHistory,
              err
            );
          }
        });

        dataRes.bodyRes.sockets = bodyReq.usersID.map((userID) => {
          return createSocketData(
            [SOCKET_CHAT_PUSH],
            userID,
            { chatID: chat._id }
          );
        });
      }

      chat.getShortcutsInfo(userSelf._id)
        .then((chat) => {
          dataRes.bodyRes.chat = chat;

          done(null, dataRes);
        });
    }], (err, dataRes) => {
      return res
        .status(dataRes.status)
        .send(dataRes.bodyRes)
        .end();
    });
};
module.exports.create = create;

/**
 * @overview Xoá trò chuyện
 *
 * @param {Request} req
 * @param {Response} res
 * @param {Function} next
 *
 * @input  params { id: String }
 * @output        { code: String, chatID: ObjectId }
 */
const deletee = (req, res, next) => {
  const paramsReq = req.params;
  const userSelf = req.user;
  let dataRes = null;

  if (userSelf) {
    Chat.findOneAndUpdate(
      {
        _id: paramsReq.id,
        users: {
          $elemMatch: {
            _id: userSelf._id,
            'accept.is': true,
            'delete.is': false,
          },
        },
      },
      {
        $set: {
          'users.$.delete': {
            is: true,
            date: Date.now(),
          }
        },
      },
      {
        new: true,
      }
    )
      .exec((err, chat) => {
        if (err || !chat) {
          // Trò chuyện không tồn tại
          dataRes = {
            status: 404,
            bodyRes: {
              code: 'chat.delete.failed',
              chatID: paramsReq.id
            },
          };
        } else {
          dataRes = {
            status: 200,
            bodyRes: {
              code: 'chat.delete.success',
              chatID: paramsReq.id
            },
          };
        }

        return res
          .status(dataRes.status)
          .send(dataRes.bodyRes)
          .end();
      });
  } else {
    // Chưa đăng nhập người dùng
    dataRes = {
      status: 401,
      bodyRes: {
        code: 'user.signin.notExist',
        chatID: paramsReq.id
      },
    };

    return res
      .status(dataRes.status)
      .send(dataRes.bodyRes)
      .end();
  }
};
module.exports.deletee = deletee;

/**
 * @overview Chấp nhận trò chuyện
 *
 * @param {Request} req
 * @param {Response} res
 * @param {Function} next
 *
 * @input  params { id: String }
 * @input  body   { accept { is: Boolean } }
 * @output        {
 *                  code: String,
 *                  chat?: {Chat},
 *                  chatID: ObjectId
 *                }
 */
const accept = (req, res, next) => {
  const paramsReq = req.params;
  const bodyReq = req.body;
  const userSelf = req.user;
  let dataRes = null;

  async.waterfall([
    (done) => {
      /* Tìm trò chuyện */

      if (userSelf) {
        Chat
          .findOne(
            {
              _id: paramsReq.id,
              'users._id': userSelf._id,
            }
          )
          .exec((err, chat) => {
            if (err || !chat) {
              // Trò chuyện không tồn tại
              dataRes = {
                status: 404,
                bodyRes: {
                  code: 'chat.notExist',
                },
              };

              done(err || new Error('Not chat exists'), dataRes);
            } else {
              done(null, chat);
            }
          });
      } else {
        // Chưa đăng nhập người dùng
        dataRes = {
          status: 401,
          bodyRes: {
            code: 'user.signin.notExist',
          },
        };

        done(new Error('User not signin'), dataRes);
      }
    }, (chat, done) => {
      /* Cập nhật thông tin trò chuyện */

      chat.acceptUser(userSelf._id, bodyReq.accept.is, true)
        .then(() => {
          chat.updateDate = Date.now();
          chat.save((err) => {
            if (err) {
              // Chấp nhận trò chuyện thất bại
              dataRes = {
                status: 400,
                bodyRes: {
                  code: 'chat.accept.failed',
                },
              };
            } else {
              dataRes = chat;
            }

            done(err, dataRes);
          });
        });
    }, (chat, done) => {
      /* Lấy thông tin trò chuyện */

      const activityHistory = new ActivityHistory({
        userID: userSelf._id,
        toURL: chat._id,
        type: constant.CHAT,
        result: constant.SUCCESS,
        content: `chat.accept.${bodyReq.accept.is}`,
      });
      activityHistory.save((err) => {
        if (err) {
          logger.logError(
            'chat.server.controller',
            'accept',
            constant.ACTIVITY_HISTORY,
            activityHistory,
            err
          );
        }
      });

      if (bodyReq.accept.is) {
        chat.getShortcutsInfo(userSelf._id)
          .then((chat) => {
            dataRes = {
              status: 200,
              bodyRes: {
                code: `chat.accept.${bodyReq.accept.is}`,
                chat,
              },
            };

            done(null, dataRes);
          });
      } else {
        dataRes = {
          status: 200,
          bodyRes: {
            code: `chat.accept.${bodyReq.accept.is}`
          },
        };

        done(null, dataRes);
      }
    }], (err, dataRes) => {
      dataRes.bodyRes.chatID = paramsReq.id;

      return res
        .status(dataRes.status)
        .send(dataRes.bodyRes)
        .end();
    });
};
module.exports.accept = accept;
