const path = require('path');
const mongoose = require('mongoose');
const async = require('async');
const logger = require(path.resolve('./config/lib/logger'));
const constant = require(path.resolve('./commons/utils/constant'));

const Chat = mongoose.model('Chat');

/**
 * @overview Lấy trò chuyện theo ID
 *
 * @param {Request} req
 * @param {Response} res
 * @param {Function} next
 *
 * @input  params { id: String }
 *         query  { type: String }
 * @output        {
 *                   code: String,
 *                   chat?: {Chat},
 *                   count?: Object,
 *                 }
 */
const get = (req, res, next) => {
  const paramsReq = req.params;
  const queryReq = req.query;
  const userSelf = req.user;
  let dataRes = null;

  async.waterfall([
    (done) => {
      /* Tìm trò chuyện */

      if (userSelf) {
        Chat
          .findOne({
            _id: paramsReq.id,
            'users._id': userSelf._id,
          })
          .exec((err, chat) => {
            if (err || !chat) {
              // Trò chuyện không tồn tại
              dataRes = {
                status: 404,
                bodyRes: {
                  code: 'chat.notExist',
                },
              };

              done(err || new Error('Not chat exists'), dataRes);
            } else {
              done(null, chat);
            }
          });
      } else {
        // Chưa đăng nhập người dùng
        dataRes = {
          status: 401,
          bodyRes: {
            code: 'user.signin.notExist',
          },
        };

        done(new Error('User not signin'), dataRes);
      }
    }, (chat, done) => {
      /* Lấy thông tin trò chuyện */

      switch (queryReq.type) {
        case constant.SHORTCUT:
          chat.getShortcutsInfo(userSelf._id)
            .then((chat) => {
              done(null, chat);
            });
          break;
        case constant.FULL:
          chat.addView(userSelf._id)
            .then(() => {
              chat.save((err) => {
                if (err) {
                  logger.logError(
                    'chat.server.controller',
                    'get',
                    constant.CHAT,
                    chat,
                    err
                  );
                }

                chat.getFullInfo(userSelf._id)
                  .then((chat) => {
                    done(null, chat);
                  });
              });
            });
          break;
        default:
          break;
      }
    }, (chat, done) => {
      /* Trả kết quả trò chuyện */

      Chat.counts(userSelf._id)
        .then((count) => {
          dataRes = {
            status: 200,
            bodyRes: {
              code: 'chat.exist',
              chat,
              count
            },
          };

          done(null, dataRes);
        });
    }], (err, dataRes) => {
      return res
        .status(dataRes.status)
        .send(dataRes.bodyRes)
        .end();
    });
};
module.exports.get = get;

/**
 * @overview Tìm kiếm mở rộng
 *
 * @param {Request} req
 * @param {Response} res
 * @param {Function} next
 * @param {Function} getInfo
 *
 * @input query {
 *                type: String,
 *                fragment?: String
 *                skip?: Number
 *              }
 * @output      {
 *                code: String,
 *                chats?: [{Chat}],
 *                skip?: Number,
 *                count?: Object,
 *              }
 */
const search = (req, res, next) => {
  const queryReq = req.query;
  let dataRes = null;

  /**
 * @overview Lấy trò chuyện theo ID
 *
 * @param {Request} req
 * @param {Response} res
 * @param {Function} next
 *
 * @input query   {
 *                  type: String,
 *                  fragment?: String
 *                  skip?: Number
 *                }
 * @output        {
 *                  code: String,
*                   fragment: String
 *                  chats?: [{Chat}],
 *                  skip?: Number,
 *                  count?: Object,
 *                }
 */
  const autoSearch = (req, res, next) => {
    const queryReq = req.query;
    const userSelf = req.user;
    let dataRes = null;

    const limit = req.app.locals.config.searchLimit;
    const currentSkip = queryReq.skip > 0 ? queryReq.skip : 0;

    async.waterfall([(done) => {
      if (userSelf) {
        /* Tìm trò chuyện */

        let conditions = null;

        switch (queryReq.fragment) {
          case constant.SUCCESS:
            conditions = {
              users: {
                $elemMatch: {
                  _id: userSelf._id,
                  'accept.is': true,
                  'delete.is': false,
                },
              },
            };
            break;
          case constant.WAIT:
            conditions = {
              users: {
                $elemMatch: {
                  _id: userSelf._id,
                  'accept.is': false,
                  'view.is': false,
                },
              },
            };
            break;
          default:
            break;
        }

        Chat
          .find(conditions)
          .sort({ updateDate: -1 })
          .skip(currentSkip)
          .limit(limit)
          .exec((err, chats) => {
            if (err) {
              // Bad request.body
              dataRes = {
                status: 400,
                bodyRes: {
                  code: 'data.notVaild',
                },
              };

              done(err, dataRes);
            } else if (!chats.length) {
              // Trò chuyện không tồn tại
              dataRes = {
                status: 404,
                bodyRes: {
                  code: 'chat.notExist',
                },
              };

              done(new Error('Not chat exists'), dataRes);
            } else {
              done(null, chats);
            }
          });
      } else {
        // Chưa đăng nhập người dùng
        dataRes = {
          status: 401,
          bodyRes: {
            code: 'user.signin.notExist',
          },
        };

        done(new Error('Not user signin'), dataRes);
      }
    }, (chats, done) => {
      /* Lấy thêm thông tin bổ sung */

      async.map(chats, (chat, callback) => {
        chat.getShortcutsInfo(userSelf._id)
          .then((chat) => {
            callback(null, chat);
          });
      }, (err, resultChats) => {
        dataRes = {
          status: 200,
          bodyRes: {
            code: 'chat.exist',
            skip: currentSkip + (chats.length < limit ? chats.length : limit),
            chats: resultChats
          },
        };

        done(null, dataRes);
      });
    }], (err, dataRes) => {
      dataRes.bodyRes.fragment = queryReq.fragment;

      if (userSelf) {
        Chat.counts(userSelf._id)
          .then((count) => {
            dataRes.bodyRes.count = count;

            return res
              .status(dataRes.status)
              .send(dataRes.bodyRes)
              .end();
          });
      } else {
        return res
          .status(dataRes.status)
          .send(dataRes.bodyRes)
          .end();
      }
    });
  };

  switch (queryReq.type) {
    case constant.AUTO:
      autoSearch(req, res, next);
      break;
    default: {
      dataRes = {
        status: 400,
        bodyRes: {
          code: 'data.notVaild',
        },
      };

      return res
        .status(dataRes.status)
        .send(dataRes.bodyRes)
        .end();
    }
  }
};
module.exports.search = search;
