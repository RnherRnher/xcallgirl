const path = require('path');
const mongoose = require('mongoose');
const constant = require(path.resolve('./commons/utils/constant'));
const {
  SOCKET_CHAT_PUSH,
  SOCKET_CHAT_LISTEN,
  SOCKET_CHAT_UPDATE,
  SOCKET_CHAT_ADD,
  SOCKET_CHAT_SEEN,
} = require(path.resolve('./commons/modules/chats/datas/socket.data'));

const Chat = mongoose.model('Chat');

/**
 * @overview Thêm tin nhắn trò chuyện
 *
 * @param {Socket} socket
 * @param {Object} body
 * @returns {Promise}
 */
const addChat = (socket, body) => {
  return new Promise((resolve, reject) => {
    let dataRes = null;
    const userSelf = socket.request.user;

    if (userSelf) {
      Chat
        .findOne({
          _id: body.chatID,
          users: {
            $elemMatch: {
              _id: userSelf._id,
              'accept.is': true,
              'delete.is': false,
            },
          },
        })
        .exec((err, chat) => {
          if (err || !chat) {
            // Tin nhắn khôn tồn tại
            dataRes = {
              status: 404,
              bodyRes: {
                code: 'chat.notExist',
                chatID: body.chatID
              },
            };

            return resolve(dataRes);
          }

          chat
            .addData(
              userSelf._id,
              body.type,
              body.data
            )
            .then((data) => {
              chat.save((err) => {
                if (err) {
                  // Gửi tin nhắn trò chuyện thất bại
                  dataRes = {
                    status: 400,
                    bodyRes: {
                      code: 'chat.send.failed',
                      chatID: body.chatID
                    },
                  };
                } else {
                  dataRes = {
                    status: 200,
                    bodyRes: {
                      code: 'chat.send.success',
                      chatID: body.chatID,
                      data
                    },
                  };
                }

                return resolve(dataRes);
              });
            });
        });
    } else {
      // Chưa đăng nhập người dùng
      dataRes = {
        status: 401,
        bodyRes: {
          code: 'user.signin.notExist',
          chatID: body.chatID
        },
      };

      return resolve(dataRes);
    }
  });
};

/**
 * @overview Thêm đã xem tin nhắn trò chuyện
 *
 * @param {Socket} socket
 * @param {Object} body
 * @returns {Promise}
 */
const addSeen = (socket, body) => {
  return new Promise((resolve, reject) => {
    let dataRes = null;
    const userSelf = socket.request.user;

    if (userSelf) {
      Chat.findOneAndUpdate(
        {
          _id: body.chatID,
          users: {
            $elemMatch: {
              _id: userSelf._id,
              'accept.is': true,
              'delete.is': false,
            },
          },
        },
        {
          $set: {
            'users.$.view': {
              is: true,
              date: Date.now(),
            }
          }
        },
        {
          new: true,
        }
      )
        .exec((err, chat) => {
          if (err || !chat) {
            // Tin nhắn khôn tồn tại
            dataRes = {
              status: 404,
              bodyRes: {
                code: 'chat.notExist',
                chatID: body.chatID
              },
            };

            return resolve(dataRes);
          }

          chat.getFullInfo(userSelf._id)
            .then((chat) => {
              dataRes = {
                status: 200,
                bodyRes: {
                  code: 'chat.seen.success',
                  chatID: body.chatID,
                  users: chat.users
                },
              };

              return resolve(dataRes);
            });
        });
    } else {
      // Chưa đăng nhập người dùng
      dataRes = {
        status: 401,
        bodyRes: {
          code: 'user.signin.notExist',
          chatID: body.chatID
        },
      };

      return resolve(dataRes);
    }
  });
};

/**
 * @overview Khởi tạo
 *
 * @param {SocketIO} io
 * @param {Socket} socket
 */
const init = (io, socket) => {
  socket.on(SOCKET_CHAT_LISTEN, (dataReq) => {
    switch (dataReq.type) {
      case constant.ADD:
        socket.join(dataReq.id);
        break;
      case constant.REMOVE:
        socket.leave(dataReq.id);
        break;
      default:
        break;
    }
  });

  socket.on(SOCKET_CHAT_PUSH, (dataReq) => {
    io.to(dataReq.to)
      .emit(
        SOCKET_CHAT_PUSH,
        dataReq.body
      );
  });

  socket.on(SOCKET_CHAT_ADD, (dataReq) => {
    addChat(socket, dataReq.body)
      .then((dataRes) => {
        if (dataRes.status === 200) {
          io.in(dataReq.to).emit(SOCKET_CHAT_ADD, dataRes);
          io.in(dataReq.to + dataReq.to).emit(SOCKET_CHAT_UPDATE, dataReq.body);
        } else {
          socket.emit(SOCKET_CHAT_ADD, dataRes);
        }
      });
  });

  socket.on(SOCKET_CHAT_SEEN, (dataReq) => {
    addSeen(socket, dataReq.body)
      .then((dataRes) => {
        if (dataRes.status === 200) {
          io.in(dataReq.to).emit(SOCKET_CHAT_SEEN, dataRes);
        } else {
          socket.emit(SOCKET_CHAT_SEEN, dataRes);
        }
      });
  });

  // socket.on(SOCKET_CHAT_UPDATE, (dataReq) => {
  //   io.in(dataReq.to).emit(SOCKET_CHAT_UPDATE, dataReq.body);
  // });
};
module.exports = init;
