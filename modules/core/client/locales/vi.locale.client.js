const statusCode = require('./vi/status-code.locale.client');
const constant = require('../../../../commons/utils/constant');
const { assignIn } = require('lodash');

module.exports = {
  locale: constant.VI,
  messages: assignIn(
    statusCode,
  ),
};
