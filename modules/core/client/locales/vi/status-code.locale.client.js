module.exports = {
  /* User */
  user: {
    is: 'Người dùng',
    exist: 'Người dùng tồn tại',
    notExist: 'Người dùng không tồn tại',
    permission: {
      notExist: 'Người dùng không có quyền',
    },
    roles: {
      is: 'Vai trò',
      user: 'Người dùng',
      admin: 'Quản trị viên',
      provider: 'Nhà cung cấp dịch vụ',
      system: 'Hệ thống',
      change: {
        is: 'Thay đổi vai trò người dùng',
        success: 'Thay đổi vai trò người dùng thành công',
        failed: 'Thay đổi vai trò người dùng thất bại',
      }
    },
    signin: {
      is: 'Đăng nhập',
      exist: 'Người dùng đã đăng nhập',
      notExist: 'Người dùng chưa đăng nhập',
      success: 'Đăng nhập người dùng thành công',
      failed: 'Đăng nhập người dùng thất bại',
    },
    signup: {
      is: 'Đăng ký',
      exist: 'Đăng ký người dùng tồn tại',
      success: 'Đăng ký người dùng thành công',
      failed: 'Đăng ký người dùng thất bại',
    },
    sigout: {
      is: 'Đăng xuất',
      success: 'Đăng xuất người dùng thành công',
    },
    birthday: {
      is: 'Ngày sinh',
    },
    address: {
      change: {
        is: 'Thay đổi địa chỉ người dùng',
        success: 'Thay đổi địa chỉ người dùng thành công',
        failed: 'Thay đổi địa chỉ người dùng thất bại',
      },
    },
    numberPhone: {
      is: 'Số điện thoại',
      note: 'Bạn nên cung cấp sdt chính xác để  được sử dụng tất cả dịch vụ từ XCallgirl',
      exist: 'Số điện thoại người dùng tồn tại',
      change: {
        is: 'Thay đổi số diện thoại người dùng',
        success: 'Thay đổi số diện thoại người dùng thành công',
        failed: 'Thay đổi số diện thoại người dùng thất bại',
      },
      verify: {
        is: 'Xác thực số điện thoại người dùng',
        exist: 'Đã xác thực số điện thoại người dùng',
        notExist: 'Chưa xác thực số điện thoại người dùng',
        success: 'Xác thực số điện thoại người dùng thành công',
        failed: 'Xác thực số điện thoại người dùng thất bại',
      },
    },
    name: {
      first: {
        is: 'Họ',
        regex: 'Chỉ cho phép các ký tự a->z, A->Z. Có dấu hoặc không dấu. Độ dài từ 1->30 ký tự',
      },
      last: {
        is: 'Tên',
        regex: 'Chỉ cho phép các ký tự a->z, A->Z. Có dấu hoặc không dấu .Độ dài từ 1->30 ký tự',
      },
      nick: {
        is: 'Biệt danh',
        exist: 'Biệt danh người dùng tồn tại',
        regex: 'Chỉ cho phép các ký tự a->z, A->Z, 0->9, ". và _". Không dấu .Độ dài từ 5->30 ký tự',
      }
    },
    gender: {
      is: 'Giới tính',
      female: 'Nữ',
      male: 'Nam',
      other: 'Khác'
    },
    password: {
      is: 'Mật khẩu',
      regex: 'Chỉ cho phép các ký tự a->z, A->Z, 0->9. Độ dài từ 5->30 ký tự',
      forgot: 'Quên mật khẩu ?',
      current: 'Mật khẩu hiện tại',
      new: 'Mật khẩu mới',
      verify: {
        is: 'Xác thực mật khẩu',
        failed: 'Xác thực mật khẩu thất bại',
      },
      change: {
        is: 'Thay đổi mật khẩu',
        success: 'Thay đổi mật khẩu người dùng thành công',
        failed: 'Thay đổi mật khẩu người dùng thất bại',
      },
      reset: {
        is: 'Thiết lặp lại mật khẩu',
        success: 'Thiết lặp lại mật khẩu người dùng thành công',
      },
    },
    avatar: {
      change: {
        is: 'Thay đổi hình đại diện',
        success: 'Thay đổi hình đại diện người dùng thành công',
        failed: 'Thay đổi hình đại diện người dùng thất bại',
      },
      delete: {
        is: 'Xoá hình đại diện người dùng',
        success: 'Xoá hình đại diện người dùng thành công',
        failed: 'Xoá hình đại diện người dùng thất bại',
      },
      upload: {
        failed: 'Tải hình đại diện người dùng thất bại',
      },
    },
    setting: {
      is: 'Cài đặt',
      exist: 'Cài đặt tồn tại',
      change: {
        success: 'Thay đổi cài đặt người dùng thành công',
        failed: 'Thay đổi cài đặt người dùng thất bại',
      },
    },
    delete: {
      is: 'Xoá người dùng',
      exist: 'Người dùng đã bị xoá',
      notExist: 'Người dùng không bị xoá',
      success: 'Xoá người dùng thành công',
      failed: 'Xoá người dùng thất bại',
    },
    restore: {
      is: 'Khôi phục người dùng',
      exist: 'Người dùng đã khôi phục',
      success: 'Khôi phục người dùng thành công',
      failed: 'Khôi phục người dùng thất bại',
    },
    lock: {
      is: 'Khoá người dùng',
      exist: 'Người dùng đã bị khoá',
      notExist: 'Người dùng không bị khoá',
      success: 'Khoá người dùng thành công',
      failed: 'Khoá người dùng thất bại',
      note: 'Thời gian bị khóa tối thiểu cho mỗi lần là 30 phút'
    },
    unlock: {
      exist: 'Người dùng đã mở khoá',
      success: 'Mở khoá người dùng thành công',
      failed: 'Mở khoá người dùng thất bại',
    },
    online: {
      exist: 'Người dùng đang trực tuyến',
    },
    token: {
      resetPassword: {
        is: 'Mã xác thực khôi phục mật khẩu người dùng',
        notExist: 'Mã xác thực khôi phục mật khẩu người dùng không tồn tại',
        create: {
          success: 'Tạo mã xác thực khôi phục mật khẩu người dùng thành công',
          failed: 'Tạo mã xác thực khôi phục mật khẩu người dùng thất bại',
        },
        verify: {
          success: 'Xác thực mã khôi phục mật khẩu người dùng thành công',
          failed: 'Xác thực mã khôi phục mật khẩu người dùng thất bại',
        },
      },
      verifyNumberPhone: {
        notExist: 'Mã xác thực số điện thoại người dùng không tồn tại',
        create: {
          success: 'Tạo mã xác thực số điện thoại người dùng thành công',
          failed: 'Tạo mã xác thực số điện thoại người dùng thất bại',
        },
      },
    },
    verify: {
      account: {
        name: 'Chứng mình thư / CMND',
        is: 'Xác thực người dùng',
        exist: 'Đã xác thực người dùng',
        notExist: 'Chưa xác thực người dùng',
        success: 'Xác thực người dùng thành công',
        failed: 'Xác thực người dùng thất bại',
        note: 'Bạn cần xác thực người dùng để có thể sử dụng dịch vụ này',
        request: {
          is: 'Yêu cầu xác thực người dùng',
          exist: 'Đã yêu cầu xác thực người dùng',
          notExist: 'Yêu cầu xác thực người dùng không tồn tại',
          success: 'Yêu cầu xác thực người dùng thành công',
          failed: 'Yêu cầu xác thực người dùng thất bại',
          note: {
            none: 'Để sử dụng đầy đủ các dịch vụ từ chúng tôi và để đảm bảo quyền lợi tránh bị lừa đảo cho người dùng. Bạn cần cung cấp cho chúng tôi hình ảnh chứng mình thư / CMND. Thông tin này là tuyệt mật, chúng tôi chỉ sử dụng để so sánh khớp với các không tin mà bạn đã khai báo trước đó',
            wait: 'Hiện tại chúng tôi đang xử  lý yêu cầu xác thực từ bạn',
          }
        },
        upload: {
          is: 'Tải hình xác thực người dùng',
          failed: 'Tải hình xác thực người dùng thất bại',
        },
      },
    },
  },
  /* Notification */
  notification: {
    is: 'Thông báo',
    exist: 'Thông báo tồn tại',
    notExist: 'Thông báo không tồn tại',
    sms: {
      is: 'Thông báo tin nhắn'
    },
    mark: {
      success: 'Đánh dấu thông báo thành công',
      failed: 'Đánh dấu thông báo thất bại',
    }
  },
  /* ActivityHistory */
  activityHistory: {
    is: 'Lịch sử hoạt động',
    exist: 'Lịch sử hoạt động tồn tại',
    notExist: 'Lịch sử hoạt động không tồn tại',
  },
  /* Chat */
  chat: {
    is: 'Trò chuyện',
    wait: {
      is: 'Trò chuyện chờ',
    },
    exist: 'Trò chuyện tồn tại',
    notExist: 'Trò chuyện không tồn tại',
    create: {
      is: 'Tạo trò chuyện',
      success: 'Tạo trò chuyện thành công',
      failed: 'Tạo trò chuyện thất bại',
    },
    delete: {
      is: 'Xoá trò chuyện',
      success: 'Xoá trò chuyện thành công',
      failed: 'Xoá trò chuyện thất bại',
    },
    message: {
      exist: 'Tin nhắn trò chuyện tồn tại',
      notExist: 'Tin nhắn trò chuyện không tồn tại',
    },
    send: {
      success: 'Gửi tin nhắn trò chuyện thành công',
      failed: 'Gửi tin nhắn trò chuyện thất bại',
    },
    accept: {
      is: 'Chấp nhận trò chuyện',
      success: 'Chấp nhận trò chuyện thành công',
      faile: 'Chấp nhận trò chuyện thất bại',
      true: 'Có chấp nhận trò chuyện',
      false: 'Không chấp nhận trò chuyện',

    },
    seen: {
      success: 'Xem tin nhắn trò chuyện thành công'
    }
  },
  /* BusinessCard */
  businessCard: {
    is: 'Danh thiếp',
    exist: 'Danh thiếp tồn tại',
    notExist: 'Danh thiếp không tồn tại',
    create: {
      is: 'Tạo danh thiếp',
      success: 'Tạo danh thiếp thành công',
      failed: 'Tạo danh thiếp thất bại',
    },
    delete: {
      is: 'Xoá danh thiếp',
      success: 'Xoá danh thiếp thành công',
      failed: 'Xoá danh thiếp thất bại',
    },
    status: {
      change: {
        is: 'Thay đổi trạng thái danh thiếp',
        success: 'Thay đổi trạng thái danh thiếp thành công',
        failed: 'Thay đổi trạng thái danh thiếp thất bại',
      },
    },
    title: {
      is: ' Tiêu đề danh thiếp',
    },
    descriptions: {
      is: ' Mô tả danh thiếp',
      change: {
        is: 'Thay đổi mô tả danh thiếp',
        success: 'Thay đổi mô tả danh thiếp thành công',
        failed: 'Thay đổi mô tả danh thiếp thất bại',
      },
    },
    address: {
      change: {
        is: 'Thay đổi địa chỉ danh thiếp',
        success: 'Thay đổi địa chỉ danh thiếp thành công',
        failed: 'Thay đổi địa chỉ danh thiếp thất bại',
      },
    },
    image: {
      blurFace: {
        is: 'Che mặt'
      },
      names: {
        background: 'Hình nền',
        face: 'Gương mặt',
        breast: 'Vòng 1',
        hips: 'Vòng 2',
        waist: 'Vòng 3',
        body: 'Toàn thân'
      },
      delete: {
        is: 'Xoá hình danh thiếp',
        success: 'Xoá hình danh thiếp thành công',
        failed: 'Xoá hình danh thiếp thất bại',
      },
      upload: {
        is: 'Tải hình danh thiếp',
        success: 'Tải hình danh thiếp thành công',
        failed: 'Tải hình danh thiếp thất bại',
        note: 'Nên đăng đúng định dạng hình ảnh. Điều này sẻ giúp người dùng dễ dàng tiếp cận được bạn'
      },
    },
    like: {
      is: 'Thích danh thiếp',
      exist: 'Thích danh thiếp tồn tại',
      notExist: 'Thích danh thiếp không tồn tại',
      success: 'Thích danh thiếp thành công',
      failed: 'Thích danh thiếp không thành công',
    },
    unlike: {
      is: 'Bỏ thích danh thiếp',
      success: 'Bỏ thích danh thiếp thành công',
      failed: 'Bỏ thích danh thiếp không thành công',
    },
    comment: {
      is: 'Nhận xét danh thiếp',
      exist: 'Nhận xét danh thiếp tồn tại',
      notExist: 'Nhận xét danh thiếp không tồn tại',
      success: 'Nhận xét danh thiếp thành công',
      failed: 'Nhận xét danh thiếp không thành công',
      change: {
        is: 'Thay đổi nhận xét danh thiếp',
        success: 'Thay đổi nhận xét danh thiếp thành công',
        failed: 'Thay đổi nhận xét danh thiếp không thành công',
      },
      delete: {
        is: 'Xoá nhận xét danh thiếp',
        success: 'Xoá nhận xét danh thiếp thành công',
        failed: 'Xoá nhận xét danh thiếp không thành công',
      },
    },
    report: {
      is: 'Tố cáo danh thiếp',
      exist: 'Tố cáo danh thiếp tồn tại',
      success: 'Tố cáo danh thiếp thành công',
      failed: 'Tố cáo danh thiếp không thành công',
    },
    reUpload: {
      is: 'Lên Top danh thiếp',
      success: 'Lên Top danh thiếp thành công',
      failed: 'Lên Top danh thiếp không thành công',
    },
    measurements3: {
      is: 'Số đo 3 vòng',
      breast: {
        is: 'Vòng 1'
      },
      hips: {
        is: 'Vòng 2'
      },
      waist: {
        is: 'Vòng 3'
      },
      note: 'Để chính xác. Thông tin này sẻ được người dùng đánh giá sau khi có sử dụng dịch vụ của bạn'
    },
    costs: {
      is: 'Giá cả',
      value: 'Giá trị',
      moneys: 'Giá',
      rooms: {
        is: 'Giá phòng',
        oneTime: 'Một giờ',
        overNight: 'Qua đêm',
      },
      services: {
        is: 'Giá dịch vụ',
        oneSlot: 'Một lần',
        overNight: 'Qua đêm',
        some: 'Tay ba',
        someOverNight: 'Tay ba qua đêm',
        note: 'Bạn phải có ít nhất một dịch vụ'
      }
    }
  },
  /* BusinessCardDealHistory */
  businessCardDealHistory: {
    is: 'Lịch sử giao dịch danh thiếp',
    exist: 'Lịch sử giao dịch danh thiếp tồn tại',
    notExist: 'Lịch sử giao dịch danh thiếp không tồn tại',
    success: 'Lịch sử giao dịch danh thiếp thành công',
    failed: 'Lịch sử giao dịch danh thiếp không thành công',
    create: {
      is: 'Tạo lịch sử giao dịch danh thiếp',
      success: 'Tạo lịch sử giao dịch danh thiếp thành công',
      failed: 'Tạo lịch sử giao dịch danh thiếp không thành công',
      note: 'Giao dịch sẻ được bắt đầu sau'
    },
    accept: {
      is: 'Chấp nhận lịch sử giao dịch danh thiếp',
      exist: 'Chấp nhận lịch sử giao dịch danh thiếp tồn tại',
      true: 'Có chấp nhận lịch sử giao dịch danh thiếp',
      false: 'Không chấp nhận lịch sử giao dịch danh thiếp',
      success: 'Chấp nhận lịch sử giao dịch danh thiếp thành công',
      failed: 'Chấp nhận lịch sử giao dịch danh thiếp không thành công',
      reason: {
        is: 'Lý do không chấp nhận lịch sử giao dịch danh thiếp (nếu có)'
      }
    },
    token: {
      is: 'Mã thông báo lịch sử giao dịch danh thiếp',
      create: {
        is: 'Tạo mã thông báo lịch sử giao dịch danh thiếp',
        success: 'Tạo mã thông báo lịch sử giao dịch danh thiếp thành công',
        failed: 'Tạo mã thông báo sử giao dịch danh thiếp không thành công',
      },
      verify: {
        is: 'Xác thực mã thông báo lịch sử giao dịch danh thiếp',
        success: 'Xác thực mã thông báo lịch sử giao dịch danh thiếp thành công',
        failed: 'Xác thực mã thông báo sử giao dịch danh thiếp không thành công',
        note: 'Chưa hết thời gian chờ nên không thể xác thực mã thông báo lịch sử giao dịch danh thiếp'
      },
    },
    report: {
      is: 'Tố cáo giao dịch danh thiếp',
      success: 'Tố cáo giao dịch danh thiếp thành công',
      failed: 'Tố cáo giao dịch danh thiếp không thành công',
    },
    note: {
      is: 'Lưu ý giao dịch (nếu có)'
    }
  },
  /* System */
  system: {
    usersParameter: {
      exist: 'Thông số người dùng tồn tại'
    },
    businessCardDealHistorysParameter: {
      exist: 'Thông số  lịch sử giao dịch tồn tại'
    },
    businessCardsParameter: {
      exist: 'Thông số  danh thiếp tồn tại'
    }
  },
  /* Credit */
  credit: {
    is: 'Thẻ tín dụng',
    notExist: 'Thẻ tín dụng không tồn tại',
    transfer: {
      is: 'Chuyển khoản tín dụng',
      exist: 'Chuyển khoản tín dụng tồn tại',
      success: 'Chuyển khoản tín dụng thành công',
      failed: 'Chuyển khoản tín dụng không thành công',
    }
  },
  /* Store */
  store: {
    is: 'Cửa hàng',
    pay: {
      is: 'Thanh toán',
      exist: 'Thanh toán tồn tại',
      success: 'Thanh toán thành công',
      failed: 'Thanh toán không thành công',
    },
    items: {
      businessCard: {
        reUpload: {
          descriptions1: 'Với 50 tiền bạn được 5 lần lên Top',
          descriptions2: 'Với 100 tiền bạn được 15 lần lên Top (+50%)',
          descriptions3: 'Với 500 tiền bạn được 100 lần lên Top (+100%)',
        }
      }
    }
  },
  /* Commons */
  data: {
    notVaild: 'Dữ liệu không hợp lệ',
    loading: {
      is: 'Đang tải dữ liệu ...'
    },
    csrf: {
      success: 'Tạo mã CSRF thành công',
      failed: 'Tạo mã CSRF thất bại',
    }
  },
  error: {
    server: {
      is: 'Lỗi máy chủ',
    },
    client: {
      support: {
        notExist: 'Thiết bị không được hỗ trợ',
      },
    },
  },
  actions: {
    next: 'Tiếp theo',
    back: 'Quay lại',
    add: 'Thêm',
    delete: 'Xóa',
    restore: 'Khôi phục',
    edit: 'Chỉnh sửa',
    change: 'Thay đổi',
    upload: 'Tải lên',
    getToken: 'Lấy mã',
    vetify: 'Xác thực',
    select: 'Chọn',
    schedule: 'Lên lịch',
    send: 'Gửi',
    mark: 'Đánh dấu',
    unMark: 'Bỏ đánh dấu',
    accept: 'Chấp nhận',
    pay: 'Thanh toán',
    notAccept: 'Từ chối',
    cancel: 'Hủy bỏ',
    report: 'Tố cáo',
    search: 'Tìm kiếm',
    lock: 'Khóa',
    unlock: 'Mở khóa'
  },
  info: {
    notExist: 'Không có thông tin',
    manager: {
      user: 'Quản lý người dùng',
      is: 'Quản lý'
    },
    account: {
      is: 'Thông tin tài khoản',
    },
    contacts: {
      is: 'Thông tin liên lạc'
    },
    general: {
      is: 'Thông tin chung'
    },
    request: {
      is: 'Yêu cầu'
    },
    number: {
      is: 'Số '
    },
    seen: {
      exist: 'Đã xem'
    },
    address: {
      is: 'Địa chỉ',
      notVaild: 'Địa chỉ không hợp lệ',
      country: {
        is: 'Quốc gia',
        notVaild: 'Quốc gia không hợp lệ'
      },
      city: {
        is: 'Thành phố'
      },
      county: {
        is: 'Quận / Huyện'
      },
      local: {
        is: 'Địa điểm cụ thể'
      }
    },
    message: {
      is: 'Tin nhắn',
      push: 'Thêm tin nhắn',
    },
    comment: {
      is: 'Nhận xét',
      push: 'Thêm nhận xét',
      count: 'Lượt nhận xét'
    },
    like: {
      count: 'Lượt thích'
    },
    view: {
      count: 'Lượt xem'
    },
    count: {
      min: 'Tối thiểu',
      max: 'Tối đa'
    },
    note: {
      is: 'Lưu ý'
    },
    result: {
      is: 'Kết quả',
      wait: 'Chờ đợi',
      success: 'Thành công',
      failed: 'Thất bại'
    },
    status: {
      is: 'Trạng thái',
      online: 'Đang hoạt động',
      offline: 'Không hoạt động',
      response: 'Phản hồi',
      token: 'Mã thông báo',
      verify: 'Xác thực'
    },
    reason: {
      is: 'Lý do'
    },
    time: {
      is: 'Thời gian',
      notVaild: 'Thời gian không hợp lệ',
      init: 'Thời gian tạo',
      update: 'Thời gian cập nhật',
      confirm: 'Thời gian xác nhận',
      expires: 'Thời gian hết hạn',
      day: 'Ngày',
      month: 'Tháng',
      year: 'Năm',
      timer: 'Hẹn giờ',
      hours: 'Giờ',
      minutes: 'Phút'
    },
    age: {
      is: 'Tuổi',
      failed: 'Chưa đủ tuổi cho phép',
    },
    security: {
      is: 'Bảo mật',
      privacy: {
        is: 'Quyền riêng tư'
      },
      verify: {
        is: 'Xác thực',
      },
      token: {
        is: 'Mã xác thực',
      },
      visible: {
        is: 'Hiển thị',
        full: 'Hiển thị đầy đủ',
        shortcut: 'Hiển thị cơ bản / tóm tắt',
        none: 'Không hiển thị bất cứ gì',
      }
    },
    national: {
      language: {
        is: 'Ngôn ngữ',
        vi: 'Tiếng Việt'
      },
    },
    moneys: {
      is: 'Tiền',
      free: 'Miễn phí',
      currencyUnit: {
        is: 'Tiền tệ',
        domestic: 'VND (Đồng)',
        dollar: 'Ngoại tệ (VD: Dollar ...)',
      }
    }
  },
  or: 'hoặc',
};
