module.exports = {
  ROOT: '/',
  MENU: '/menu',
  SEARCH: '/search',
  /**
  * User
  */
  USER: '/user',
  USER_GET: '/user/get',
  /**
  * Admin
  */
  ADMIN: '/admin',
  ADMIN_USERS_MANAGER: '/admin/users-manager',
  /**
   * Setting
   */
  SETTINGS: '/settings',
  SETTINGS_PATH: '/settings/:path',
  SETTINGS_LANGUAGE: '/settings/language',
  SETTINGS_NOTIFICATIONS: '/settings/notifications',
  SETTINGS_ACCOUNT: '/settings/account',
  SETTINGS_PASSWORD: '/settings/password',
  SETTINGS_VERIFY: '/settings/verify',
  SETTINGS_PRIVACY: '/settings/privacy',
  /**
   * Auth
   */
  AUTH_SIGNIN: '/auth/signin',
  AUTH_SIGNUP: '/auth/signup',
  AUTH_FORGOT_PASSWORD: '/auth/forgot-password',
  /**
   * Business Card
   */
  BUSINESS_CARD: '/business-card',
  BUSINESS_CARD_EDIT: '/business-card/edit',
  BUSINESS_CARD_GET: '/business-card/get',
  /**
  * Deal History
  */
  DEAL_HISTORYS: '/deal-historys',
  DEAL_HISTORY_GET: '/deal-history/get',
  /**
   * Notification
   */
  NOTIFICATIONS: '/notifications',
  /**
   * Activity History
   */
  ACTIVITY_HISTORYS: '/activity-historys',
  /**
   * Chat
   */
  CHATS: '/chats',
  CHAT_GET: '/chat/get',
  /**
   * Credit
   */
  CREDIT_GET: '/credit/get',
  /**
   * Store
   */
  STORE: '/store',
};
