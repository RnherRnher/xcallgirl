const countries = require('../../../../commons/datas/countries');
const cities = require('../../../../commons/datas/cities');
const counties = require('../../../../commons/datas/counties');
const constant = require('../../../../commons/utils/constant');
const { assignIn } = require('lodash');
const { find } = require('lodash');
const { isCollection } = require('immutable');
const { ResultEnum } = require('../constants/core.client.constant');

const action = (type, payload = {}) => {
  return assignIn(payload, { type });
};
module.exports.action = action;

const responseState = (payload = {}) => {
  return assignIn({ result: ResultEnum.none }, payload);
};
module.exports.responseState = responseState;

const getAddress = (type, { country, city, county, local }) => {
  let address = { name: null };

  switch (type) {
    case constant.COUNTRY:
      if (country) {
        address = find(countries, ['id', country]);
      }
      break;
    case constant.CITY:
      if (country && city) {
        address = find(cities[country], ['id', city]);
      }
      break;
    case constant.COUNTY:
      if (country && city && county) {
        address = find(counties[country][city], ['id', county]);
      }
      break;
    case constant.LOCAL:
      address = { name: local };
      break;
    default:
  }

  return address;
};
module.exports.getAddress = getAddress;

const makeSelect = (state) => {
  if (isCollection(state)) {
    return state.toJS();
  }

  return state;
};
module.exports.makeSelect = makeSelect;
