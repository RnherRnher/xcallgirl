const request = require('superagent');
const url = require('../../../../commons/modules/core/datas/url.data');

const csrfRequest = () => {
  return new Promise((resovle, reject) => {
    request
      .get(url.CSRF)
      .accept('application/json')
      .set('X-Requested-With', 'XMLHttpRequest')
      .end((err, res) => {
        if (err) {
          return reject(res);
        }

        return resovle(res);
      });
  });
};
module.exports.csrfRequest = csrfRequest;
