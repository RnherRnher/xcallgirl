const { all, fork } = require('redux-saga/effects');
const { watchInitApp, watchResertApp } = require('./app.client.saga');
const {
  watchGetSystemUsersParameter,
  watchGetSystemBusinessCardDealHistorysParameter,
  watchGetSystemBusinessCardsParameter
} = require('../../../system/client/sagas/system.client.saga');
const {
  watchSignin,
  watchSignup,
  watchSignout,
  watchForgotPassword,
  watchValidateResetTokenPassword,
  watchResertPassword
} = require('../../../users/client/sagas/authentication.client.saga');
const {
  watchGetUsers,
  watchVerifyAccount,
  watchDeleteAccount,
  watchRestoreAccount,
  watchLockAccount,
  watchUnlockAccount
} = require('../../../users/client/sagas/admin.client.saga');
const { watchChangeRolesAccount } = require('../../../users/client/sagas/system.client.saga');
const {
  watchGetProfileUser,
  watchGetProfileUserReload,
  watchGetProfileSettings,
  watchChangeProfileSettings,
  watchChangeProfilePassword,
  watchChangeProfileNumberPhone,
  watchChangeProfileAddress,
  watchChangeProfileAvatar,
  watchDeleteProfileAvatar,
  watchRequestVerifyAccount,
  watchGetVerifyTokenNumberPhone,
  watchVerifyNumberPhone,
  watchGetUserID,
  watchGetUserIDReload
} = require('../../../users/client/sagas/user.client.saga');
const {
  watchGetProfileBusinessCard,
  watchGetBusinessCardID,
  watchCreateBusinessCard,
  watchDeleteBusinessCard,
  watchUploadImageBusinessCard,
  watchDeleteImageBusinessCard,
  watchLikeBusinessCard,
  watchUnLikeBusinessCard,
  watchGetCommentsBusinessCard,
  watchCommentBusinessCard,
  watchChangeCommentBusinessCard,
  watchDeleteCommentBusinessCard,
  watchDealBusinessCard,
  watchReportBusinessCard,
  watchChangeAddressBusinessCard,
  watchChangeDescriptionsBusinessCard,
  watchChangeStatusBusinessCard,
  watchReUploadBusinessCard,
  watchGetBusinessCards
} = require('../../../business-cards/client/sagas/business-card.client.saga');
const {
  watchGetBusinessCardsDealHistorys,
  watchGetBusinessCardDealHistoryID,
  watchGetBusinessCardDealHistory,
  watchAcceptBusinessCardsDealHistoryID,
  watchGetTokenBusinessCardsDealHistoryID,
  watchValidateTokenBusinessCardsDealHistoryID,
  watchReportBusinessCardsDealHistoryID,
  watchListenBusinessCardsDealHistory,
} = require('../../../business-cards/client/sagas/deal-history.client.saga');
const {
  watchGetNotifications,
  watchMarkNotification,
  watchGetNotification
} = require('../../../notifications/client/sagas/notification.client.saga');
const { watchGetActivityHistorys } = require('../../../activity-historys/client/sagas/activity-history.client.saga');
const {
  watchCreateChat,
  watchGetChats,
  watchAcceptChat,
  watchDeleteChat,
  watchGetChat,
  watchGetChatID,
  watchGetMessagesChat,
  watchListenChat,
  watchMessageChat
} = require('../../../chats/client/sagas/chat.client.saga');
const { watchTransferCredits } = require('../../../credits/client/sagas/credit.client.saga');
const { watchPayStore } = require('../../../store/client/sagas/store.client.saga');

module.exports = function* () {
  yield all([
    fork(watchInitApp),
    fork(watchResertApp),

    fork(watchGetSystemUsersParameter),
    fork(watchGetSystemBusinessCardDealHistorysParameter),
    fork(watchGetSystemBusinessCardsParameter),

    fork(watchSignin),
    fork(watchSignup),
    fork(watchSignout),
    fork(watchForgotPassword),
    fork(watchValidateResetTokenPassword),
    fork(watchResertPassword),

    fork(watchGetUsers),
    fork(watchVerifyAccount),
    fork(watchDeleteAccount),
    fork(watchRestoreAccount),
    fork(watchLockAccount),
    fork(watchUnlockAccount),

    fork(watchChangeRolesAccount),

    fork(watchGetProfileUser),
    fork(watchGetProfileUserReload),
    fork(watchGetProfileSettings),
    fork(watchChangeProfileSettings),
    fork(watchChangeProfilePassword),
    fork(watchChangeProfileNumberPhone),
    fork(watchChangeProfileAddress),
    fork(watchChangeProfileAvatar),
    fork(watchDeleteProfileAvatar),
    fork(watchRequestVerifyAccount),
    fork(watchGetVerifyTokenNumberPhone),
    fork(watchVerifyNumberPhone),
    fork(watchGetUserID),
    fork(watchGetUserIDReload),

    fork(watchGetProfileBusinessCard),
    fork(watchGetBusinessCardID),
    fork(watchCreateBusinessCard),
    fork(watchDeleteBusinessCard),
    fork(watchUploadImageBusinessCard),
    fork(watchDeleteImageBusinessCard),
    fork(watchLikeBusinessCard),
    fork(watchUnLikeBusinessCard),
    fork(watchGetCommentsBusinessCard),
    fork(watchCommentBusinessCard),
    fork(watchChangeCommentBusinessCard),
    fork(watchDeleteCommentBusinessCard),
    fork(watchChangeAddressBusinessCard),
    fork(watchChangeDescriptionsBusinessCard),
    fork(watchChangeStatusBusinessCard),
    fork(watchReUploadBusinessCard),
    fork(watchDealBusinessCard),
    fork(watchReportBusinessCard),
    fork(watchGetBusinessCards),

    fork(watchGetBusinessCardsDealHistorys),
    fork(watchGetBusinessCardDealHistoryID),
    fork(watchGetBusinessCardDealHistory),
    fork(watchAcceptBusinessCardsDealHistoryID),
    fork(watchGetTokenBusinessCardsDealHistoryID),
    fork(watchValidateTokenBusinessCardsDealHistoryID),
    fork(watchReportBusinessCardsDealHistoryID),
    fork(watchListenBusinessCardsDealHistory),

    fork(watchGetNotifications),
    fork(watchMarkNotification),
    fork(watchGetNotification),

    fork(watchGetActivityHistorys),

    fork(watchCreateChat),
    fork(watchGetChats),
    fork(watchAcceptChat),
    fork(watchDeleteChat),
    fork(watchGetChat),
    fork(watchGetChatID),
    fork(watchGetMessagesChat),
    fork(watchListenChat),
    fork(watchMessageChat),

    fork(watchTransferCredits),

    fork(watchPayStore)
  ]);
};
