const constant = require('../constants/app.client.constant');
const action = require('../actions/app.client.action');
const constantCommon = require('../../../../commons/utils/constant');
const api = require('../../../users/client/services/user.client.service');
const url = require('../utils/url');
const { takeLatest, put, call } = require('redux-saga/effects');
const { push } = require('connected-react-router/immutable');
const { resetCoreStore } = require('../../../core/client/actions/core.client.action');
const { changeLanguage, resetLocalStore } = require('../../../core/client/actions/locale.client.action');
const { resetThemeStore } = require('../../../core/client/actions/theme.client.action');
const { resetSystemStore } = require('../../../system/client/actions/system.client.action');
const {
  getProfileUserSuccess,
  getProfileUserFailed,
  getProfileSettingsRequest,
  resetUserStore
} = require('../../../users/client/actions/user.client.action');
const { resetAdminStore } = require('../../../users/client/actions/admin.client.action');
const { resetAuthStore } = require('../../../users/client/actions/authentication.client.action');
const { resetActivityHistoryStore } = require('../../../activity-historys/client/actions/activity-history.client.action');
const { getNotificationsRequest, resetNotificationStore } = require('../../../notifications/client/actions/notification.client.action');
const { resetBusinessCardStore } = require('../../../business-cards/client/actions/business-card.client.action');
const { getBusinessCardsDealHistorysRequest, resetBusinessCardsDealHistoryStore } = require('../../../business-cards/client/actions/deal-history.client.action');
const { getChatsRequest, resetChatStore } = require('../../../chats/client/actions/chat.client.action');
const { resetCreditStore } = require('../../../credits/client/actions/credit.client.action');
const { resetStoreStore } = require('../../../store/client/actions/store.client.action');
const { socket } = require('../lib/socket-io');
// const { resetApp, pushAlertSnackbar } = require('../actions/app.client.action');

const watchInitApp = function* () {
  yield takeLatest(constant.INIT_APP, function* () {
    try {
      const response = yield call(api.getProfileUser);

      yield put(getProfileUserSuccess(response.body));
      yield put(changeLanguage(response.body.user.language));
      yield put(getProfileSettingsRequest(constantCommon.FULL, false));
      yield put(getNotificationsRequest(constantCommon.AUTO, 0, true));

      if (response.body.user.roles.value !== constantCommon.SYSTEM) {
        yield put(getChatsRequest(constantCommon.AUTO, constantCommon.SUCCESS, 0, true));
        yield put(getChatsRequest(constantCommon.AUTO, constantCommon.WAIT, 0, true));
        yield put(getBusinessCardsDealHistorysRequest(constantCommon.AUTO, 0, true));
      }

      if (socket.disconnected) {
        socket.open();
      }
    } catch (error) {
      yield put(getProfileUserFailed((error && error.body) ? error.body : ''));

      if (socket.connected) {
        socket.close();
      }
    } finally {
      yield put(push(url.ROOT));
    }
  });
};
module.exports.watchInitApp = watchInitApp;

const watchResertApp = function* () {
  yield takeLatest(constant.RESERT_APP, function* () {
    yield put(resetCoreStore());
    yield put(resetLocalStore());
    yield put(resetThemeStore());
    yield put(resetSystemStore());
    yield put(resetAuthStore());
    yield put(resetUserStore());
    yield put(resetAdminStore());
    yield put(resetActivityHistoryStore());
    yield put(resetNotificationStore());
    yield put(resetBusinessCardStore());
    yield put(resetBusinessCardsDealHistoryStore());
    yield put(resetChatStore());
    yield put(resetCreditStore());
    yield put(resetStoreStore());

    if (socket.connected) {
      socket.close();
    }

    yield put(action.initApp());
  });
};
module.exports.watchResertApp = watchResertApp;
