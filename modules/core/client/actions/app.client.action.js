const constant = require('../constants/app.client.constant');
const { action } = require('../utils/middleware');

/**
 * @override Khởi tạo ứng dụng
 *
 * @param   {Object}
 * @returns {Object}
 */
const initApp = () => {
  return action(constant.INIT_APP);
};
module.exports.initApp = initApp;

/**
 * @override Khôi phục ứng dụng
 *
 * @param   {Object}
 * @returns {Object}
 */
const resetApp = () => {
  return action(constant.RESERT_APP);
};
module.exports.resetApp = resetApp;
