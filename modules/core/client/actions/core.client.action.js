const constant = require('../constants/core.client.constant');
const { action } = require('../utils/middleware');

/**
 * @override Thêm thông báo
 *
 * @param   {Object}
 * @returns {Object}
 */
const pushAlertSnackbar = ({ variant, code }) => {
  return action(constant.PUSH_ALERT_SNACKBAR, { variant, code });
};
module.exports.pushAlertSnackbar = pushAlertSnackbar;

/**
 * @override Bỏ thông báo
 *
 * @param   {Object}
 * @returns {Object}
 */
const popAlertSnackbar = () => {
  return action(constant.POP_ALERT_SNACKBAR);
};
module.exports.popAlertSnackbar = popAlertSnackbar;

/**
 * @override Thiết lặp lại cửa hàng
 *
 * @param   {Object}
 */
const resetCoreStore = () => {
  return action(constant.RESERT_CORE_STORE);
};
module.exports.resetCoreStore = resetCoreStore;
