const constant = require('../constants/theme.client.constant');
const { action } = require('../utils/middleware');

/**
 * @override Thay đổi giao diện
 *
 * @param   {Object} theme
 * @returns {Object}
 */
const changeTheme = (theme) => {
  return action(constant.CHANGE_THEME, { theme });
};
module.exports.changeTheme = changeTheme;

/**
 * @override Thiết lặp lại cửa hàng
 *
 * @param   {Object}
 */
const resetThemeStore = () => {
  return action(constant.RESERT_THEME_STORE);
};
module.exports.resetThemeStore = resetThemeStore;
