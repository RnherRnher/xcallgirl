const constant = require('../constants/locale.client.constant');
const { action } = require('../utils/middleware');

/**
 * @override Thay đổi ngôn ngữ
 *
 * @param   {String} language
 * @returns {Object}
 */
const changeLanguage = (language) => {
  return action(constant.CHANGE_LANGUAGE, { language });
};
module.exports.changeLanguage = changeLanguage;

/**
 * @override Thiết lặp lại cửa hàng
 *
 * @param   {Object}
 */
const resetLocalStore = () => {
  return action(constant.RESERT_LOCALE_STORE);
};
module.exports.resetLocalStore = resetLocalStore;
