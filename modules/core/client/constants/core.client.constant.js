module.exports = {
  ResultEnum: {
    none: -2,
    wait: -1,
    failed: 0,
    success: 1,
  },
  PUSH_ALERT_SNACKBAR: 'PUSH_ALERT_SNACKBAR',
  POP_ALERT_SNACKBAR: 'POP_ALERT_SNACKBAR',
  RESERT_CORE_STORE: 'RESERT_CORE_STORE'
};
