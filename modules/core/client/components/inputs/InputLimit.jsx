const React = require('react');
const propTypes = require('prop-types');
const classNames = require('classnames');
const regular = require('../../../../../commons/utils/regular-expression');
const constant = require('../../../../../commons/utils/constant');
const { FormattedMessage } = require('react-intl');
const {
  ArrowForward
} = require('@material-ui/icons');
const {
  TextField,
  Grid
} = require('@material-ui/core');

const InputLimit = class InputLimit extends React.Component {
  constructor(props) {
    super(props);

    const { value } = this.props;
    this.state = {
      min: value && value.min ? value.min : 0,
      max: value && value.max ? value.max : 0,
    };

    this.handleChange = this.handleChange.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    const { isUpdatePropsToState } = this.props;

    if (isUpdatePropsToState) {
      const { value } = nextProps;

      const min = value && value.min ? value.min : 0;
      const max = value && value.max ? value.max : 0;

      this.setState({
        min,
        max
      });
    }
  }

  handleChange(type) {
    return (event) => {
      const { value } = event.target;
      const { validate } = this.props;

      this.setState({
        [type]: validate.test(value)
          ? parseInt(value, 10)
          : value ? parseInt(value.slice(0, -1), 10) : 0
      });
    };
  }

  initElement() {
    const {
      className,
      disabled,
      err
    } = this.props;
    const { min, max } = this.state;

    return (
      <div className={classNames('xcg-input-limit', className)}>
        <Grid container spacing={8}>
          <Grid item xs={5}>
            <TextField
              label={<FormattedMessage id="info.count.min" />}
              type="text"
              fullWidth
              error={err}
              value={min}
              disabled={disabled}
              onChange={this.handleChange(constant.MIN)}
            />
          </Grid>
          <Grid item xs={2}>
            <ArrowForward />
          </Grid>
          <Grid item xs={5}>
            <TextField
              label={<FormattedMessage id="info.count.max" />}
              type="text"
              fullWidth
              error={err}
              value={max}
              disabled={disabled}
              onChange={this.handleChange(constant.MAX)}
            />
          </Grid>
        </Grid>
      </div>
    );
  }

  render() {
    return this.initElement();
  }
};

InputLimit.propTypes = {
  className: propTypes.string,
  value: propTypes.shape({
    min: propTypes.number,
    max: propTypes.number,
  }),
  disabled: propTypes.bool,
  err: propTypes.bool,
  validate: propTypes.any,
  isUpdatePropsToState: propTypes.bool,
};

InputLimit.defaultProps = {
  className: '',
  value: {
    breast: 0,
    waist: 0,
    hips: 0
  },
  disabled: false,
  err: false,
  validate: regular.commons.number,
  isUpdatePropsToState: false
};

module.exports = InputLimit;
