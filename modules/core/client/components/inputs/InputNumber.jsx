const React = require('react');
const propTypes = require('prop-types');
const classNames = require('classnames');
const regular = require('../../../../../commons/utils/regular-expression');
const { FormattedMessage, FormattedNumber } = require('react-intl');
const { TextField } = require('@material-ui/core');

const InputNumber = class InputNumber extends React.Component {
  constructor(props) {
    super(props);

    const { value } = this.props;
    this.state = {
      value: value || 0
    };

    this.handleChange = this.handleChange.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    const { isUpdatePropsToState } = this.props;

    if (isUpdatePropsToState) {
      const { value } = nextProps;

      this.setState({ value: value || 0 });
    }
  }

  handleChange(event) {
    const { value } = event.target;
    const { validate } = this.props;

    this.setState({
      value: validate.test(value)
        ? parseInt(value, 10)
        : value ? parseInt(value.slice(0, -1), 10) : 0
    });
  }

  initElement() {
    const {
      className,
      disabled,
      err,
      label
    } = this.props;
    const { value } = this.state;

    return (
      <div className={classNames('xcg-input-number', className)}>
        <TextField
          label={label}
          type="text"
          fullWidth
          error={err}
          value={value}
          disabled={disabled}
          onChange={this.handleChange}
          helperText={<FormattedNumber value={value} />}
        />
      </div>
    );
  }

  render() {
    return this.initElement();
  }
};

InputNumber.propTypes = {
  className: propTypes.string,
  value: propTypes.number,
  label: propTypes.node,
  disabled: propTypes.bool,
  err: propTypes.bool,
  validate: propTypes.any,
  isUpdatePropsToState: propTypes.bool,
};

InputNumber.defaultProps = {
  className: '',
  value: 0,
  label: <FormattedMessage id="info.number.is" />,
  disabled: false,
  err: false,
  validate: regular.commons.number,
  isUpdatePropsToState: false
};

module.exports = InputNumber;
