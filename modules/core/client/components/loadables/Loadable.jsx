const React = require('react');
const propTypes = require('prop-types');
const classNames = require('classnames');
const { ResultEnum } = require('../../constants/core.client.constant');
const { ContainerLoading } = require('./../Containers');

const Loadable = class Loadable extends React.Component {
  constructor(props) {
    super(props);
  }

  getData() {
    const {
      result,
      NoneNode,
      WaitNode,
      SuccessNode,
      FailedNode,
    } = this.props;

    switch (result) {
      case ResultEnum.none:
        return NoneNode || WaitNode || FailedNode;
      case ResultEnum.wait:
        return WaitNode;
      case ResultEnum.success:
        return SuccessNode;
      case ResultEnum.failed:
        return FailedNode;
      default:
        return null;
    }
  }

  initElement() {
    const { className } = this.props;

    return (
      <div className={classNames('xcg-loadable', className)}>
        {this.getData()}
      </div>
    );
  }

  render() {
    return this.initElement();
  }
};

Loadable.propTypes = {
  className: propTypes.string,
  NoneNode: propTypes.node,
  WaitNode: propTypes.node,
  FailedNode: propTypes.node,
  result: propTypes.number,
  SuccessNode: propTypes.node.isRequired,
};

Loadable.defaultProps = {
  className: '',
  NoneNode: null,
  WaitNode: <ContainerLoading />,
  FailedNode: null,
  result: ResultEnum.none
};

module.exports = Loadable;
