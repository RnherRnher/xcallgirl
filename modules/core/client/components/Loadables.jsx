const Loadable = require('./loadables/Loadable');
const LoadableComment = require('./loadables/LoadableComment');

module.exports = {
  Loadable,
  LoadableComment
};
