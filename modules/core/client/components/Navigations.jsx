const NavigationMain = require('./navigations/NavigationMain');
const NavigationMenu = require('./navigations/NavigationMenu');

module.exports = {
  NavigationMain,
  NavigationMenu
};
