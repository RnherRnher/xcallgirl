const React = require('react');
const RoutersDev = require('./RoutersDev');
const RoutersProd = require('./RoutersProd');
const { render } = require('react-dom');

/**
 * @override Ứng dụng client
 *
 * @param {StoreCreator} store
 * @param {History} history
 * @param {Boolean} isDev
 */
const App = (store, history, isDev) => {
  render(
    isDev
      ? <RoutersDev store={store} history={history} />
      : <RoutersProd store={store} history={history} />,
    document.getElementById('xcg'),
  );
};
module.exports = App;
