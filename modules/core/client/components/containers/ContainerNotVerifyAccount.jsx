const React = require('react');
const propTypes = require('prop-types');
const classNames = require('classnames');
const url = require('../../../../core/client/utils/url');
const { FormattedMessage } = require('react-intl');
const { Link } = require('react-router-dom');
const { Button, Typography } = require('@material-ui/core');

const ContainerNotVerifyAccount = class ContainerNotVerifyAccount extends React.Component {
  constructor(props) {
    super(props);
  }

  Background() {
    return (
      <div className="xcg-background">
        <img
          src="/images/core/client/images/banners/404.png"
          alt="banners_404"
        />
      </div>
    );
  }

  Container() {
    return (
      <div className="xcg-container">
        <Typography
          align="center"
          variant="title"
        >
          <FormattedMessage id="user.verify.account.note" />
        </Typography>
      </div>
    );
  }

  OtherLink() {
    return (
      <div className="xcg-other-link">
        <div className="xcg-primary-btn">
          <Link to={url.SETTINGS_VERIFY}>
            <Button color="secondary">
              <Typography
                align="center"
                variant="button"
                color="secondary"
              >
                <FormattedMessage id="user.verify.account.is" />
              </Typography>
            </Button>
          </Link>
        </div>
      </div>
    );
  }

  initElement() {
    const { className } = this.props;

    return (
      <div className={classNames('xcg-container-not-verify-account', className)}>
        {this.Background()}
        {this.Container()}
        {this.OtherLink()}
      </div>
    );
  }

  render() {
    return this.initElement();
  }
};

ContainerNotVerifyAccount.propTypes = {
  className: propTypes.string,
};

ContainerNotVerifyAccount.defaultProps = {
  className: 'xcg-containers'
};

module.exports = ContainerNotVerifyAccount;
