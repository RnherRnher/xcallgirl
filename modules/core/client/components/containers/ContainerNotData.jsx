const React = require('react');
const propTypes = require('prop-types');
const classNames = require('classnames');
const { FormattedMessage } = require('react-intl');
const { Typography } = require('@material-ui/core');

const ContainerNotData = class ContainerNotData extends React.Component {
  constructor(props) {
    super(props);
  }

  Background() {
    return (
      <div className="xcg-background">
        <img
          src="/images/core/client/images/banners/404.png"
          alt="banners_404"
        />
      </div>
    );
  }

  Container() {
    return (
      <div className="xcg-container">
        <Typography
          align="center"
          variant="title"
        >
          <FormattedMessage id="info.notExist" />
        </Typography>
      </div>
    );
  }

  initElement() {
    const { className } = this.props;

    return (
      <div className={classNames('xcg-container-not-data', className)}>
        {this.Background()}
        {this.Container()}
      </div>
    );
  }

  render() {
    return this.initElement();
  }
};

ContainerNotData.propTypes = {
  className: propTypes.string,
};

ContainerNotData.defaultProps = {
  className: 'xcg-containers'
};

module.exports = ContainerNotData;
