const React = require('react');
const propTypes = require('prop-types');
const classNames = require('classnames');
const nprogress = require('nprogress');
const regular = require('../../../../../commons/utils/regular-expression');
const { FormattedMessage } = require('react-intl');
const { Typography } = require('@material-ui/core');

const ContainerLoading = class ContainerLoading extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    nprogress.start();
  }

  componentWillUnmount() {
    nprogress.done();
  }

  Background() {
    return (
      <div className="xcg-background">
        <img
          className="xcg-image"
          src={regular.app.logo}
          alt="xcallgirl_logo"
        />
      </div>
    );
  }

  Container() {
    return (
      <div className="xcg-container">
        <Typography
          align="center"
          variant="title"
        >
          <FormattedMessage id="data.loading.is" />
        </Typography>
      </div>
    );
  }

  initElement() {
    const { className } = this.props;

    return (
      <div className={classNames('xcg-container-loading', className)}>
        {this.Background()}
        {this.Container()}
      </div>
    );
  }

  render() {
    return this.initElement();
  }
};

ContainerLoading.propTypes = {
  className: propTypes.string,
};

ContainerLoading.defaultProps = {
  className: 'xcg-containers'
};

module.exports = ContainerLoading;
