const React = require('react');
const propTypes = require('prop-types');
const classNames = require('classnames');
const countries = require('../../../../../commons/datas/countries');
const { FormattedMessage } = require('react-intl');
const { TextField, MenuItem } = require('@material-ui/core');

const SelectCountry = class SelectCountry extends React.Component {
  constructor(props) {
    super(props);

    const { type, value } = this.props;
    this.state = {
      [type]: value || ''
    };

    this.handleChange = this.handleChange.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    const { isUpdatePropsToState } = this.props;

    if (isUpdatePropsToState) {
      const { type, value } = nextProps;

      if (type) {
        this.setState({ [type]: value || '' });
      }
    }
  }

  getData() {
    const { type, displayType } = this.props;

    return countries.map((country) => {
      return (
        <MenuItem key={country.id} value={country[type]}>
          <span>
            {`${country.flag} ${country[displayType]}`}
          </span>
        </MenuItem>
      );
    });
  }

  handleChange(event) {
    const { value } = event.target;
    const { type, handleChangeParent } = this.props;

    this.setState({ [type]: value });

    if (handleChangeParent) {
      handleChangeParent(value);
    }
  }

  initElement() {
    const {
      className,
      disabled,
      type,
      err,
    } = this.props;
    const { state } = this;

    return (
      <div className={classNames('xcg-select-country', className)}>
        <TextField
          label={<FormattedMessage id="info.address.country.is" />}
          select
          fullWidth
          value={state[type]}
          disabled={disabled}
          error={err}
          onChange={this.handleChange}
          helperText={err ? <FormattedMessage id="info.address.country.notVaild" /> : null}
        >
          {this.getData()}
        </TextField>
      </div>
    );
  }

  render() {
    return this.initElement();
  }
};

SelectCountry.propTypes = {
  className: propTypes.string,
  value: propTypes.string,
  disabled: propTypes.bool,
  err: propTypes.bool,
  handleChangeParent: propTypes.func,
  isUpdatePropsToState: propTypes.bool,
  type: propTypes.string.isRequired,
  displayType: propTypes.string.isRequired,
};

SelectCountry.defaultProps = {
  className: '',
  value: '',
  disabled: false,
  err: false,
  handleChangeParent: null,
  isUpdatePropsToState: false
};

module.exports = SelectCountry;
