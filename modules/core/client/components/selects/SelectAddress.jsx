const React = require('react');
const propTypes = require('prop-types');
const classNames = require('classnames');
const SelectCountry = require('../selects/SelectCountry');
const cities = require('../../../../../commons/datas/cities');
const counties = require('../../../../../commons/datas/counties');
const constant = require('../../../../../commons/utils/constant');
const { FormattedMessage } = require('react-intl');
const {
  TextField,
  MenuItem,
  FormHelperText
} = require('@material-ui/core');

const SelectAddress = class SelectAddress extends React.Component {
  constructor(props) {
    super(props);

    const { value } = this.props;
    this.state = {
      country: value && value.country ? value.country : '',
      city: value && value.city ? value.city : '',
      county: value && value.county ? value.county : '',
      local: value && value.local ? value.local : '',
      error: false
    };

    this.handleCheck = this.handleCheck.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleChangeCountry = this.handleChangeCountry.bind(this);
  }

  componentDidMount() {
    const { check } = this.props;
    const {
      country,
      city,
      county,
      local
    } = this.state;

    if (check && (country || city || county || local)) {
      this.handleCheck();
    }
  }

  componentWillReceiveProps(nextProps) {
    const { isUpdatePropsToState, check } = this.props;

    if (isUpdatePropsToState) {
      const { value, required } = this.props;

      const country = value && value.country ? value.country : '';
      const city = value && value.city ? value.city : '';
      const county = value && value.county ? value.county : '';
      const local = value && value.local ? value.local : '';

      this.setState({
        country,
        city,
        county,
        local,
        error: check && !(
          country
          && (required.city ? city : true)
          && (required.county ? county : true)
          && (required.local ? local : true)
        )
      });
    }
  }

  getData(type) {
    const { country, city } = this.state;

    switch (type) {
      case constant.CITY: {
        if (country) {
          return cities[country]
            .map((city) => {
              return (
                <MenuItem key={city.id} value={city.id}>
                  {city.name}
                </MenuItem>
              );
            });
        }

        return [];
      }
      case constant.COUNTY: {
        if (country && city) {
          return (counties[country])[city]
            .map((county) => {
              return (
                <MenuItem key={county.id} value={county.id}>
                  {county.name}
                </MenuItem>
              );
            });
        }

        return [];
      }
      default:
        return [];
    }
  }

  handleCheck() {
    const { required } = this.props;
    const {
      country,
      city,
      county,
      local
    } = this.state;

    this.setState({
      error: !(
        country
        && (required.city ? city : true)
        && (required.county ? county : true)
        && (required.local ? local : true)
      )
    });
  }

  handleChangeCountry(country) {
    const { required } = this.props;
    const {
      city,
      county,
      local
    } = this.state;

    this.setState({
      country,
      error: !(
        country
        && (required.city ? city : true)
        && (required.county ? county : true)
        && (required.local ? local : true)
      )
    });
  }

  handleChange(type) {
    return (event) => {
      const { value } = event.target;
      const { required } = this.props;

      const { state } = this;

      const currentState = state;
      currentState[type] = value;

      this.setState({
        [type]: value,
        error: !(
          currentState.country
          && (required.city ? currentState.city : true)
          && (required.county ? currentState.county : true)
          && (required.local ? currentState.local : true)
        )
      });
    };
  }

  initElement() {
    const {
      className,
      disabled,
      err
    } = this.props;
    const {
      country,
      city,
      county,
      local,
      error
    } = this.state;

    return (
      <div className={classNames('xcg-select-address', className)}>
        <SelectCountry
          type="id"
          displayType="name"
          value={country}
          handleChangeParent={this.handleChangeCountry}
          disabled={disabled}
          err={err || error}
        />
        <TextField
          label={<FormattedMessage id="info.address.city.is" />}
          select
          fullWidth
          value={city}
          disabled={disabled}
          error={err || error}
          onChange={this.handleChange(constant.CITY)}
        >
          <MenuItem value="" />
          {this.getData(constant.CITY)}
        </TextField>
        <TextField
          label={<FormattedMessage id="info.address.county.is" />}
          select
          fullWidth
          value={county}
          disabled={disabled}
          error={err || error}
          onChange={this.handleChange(constant.COUNTY)}
        >
          <MenuItem value="" />
          {this.getData(constant.COUNTY)}
        </TextField>
        <TextField
          label={<FormattedMessage id="info.address.local.is" />}
          type="text"
          multiline
          rows="1"
          rowsMax="5"
          fullWidth
          value={local}
          disabled={disabled}
          error={err || error}
          onChange={this.handleChange(constant.LOCAL)}
        />
        {
          err || error
            ? <FormHelperText error>
              <FormattedMessage id="info.address.notVaild" />
            </FormHelperText>
            : null
        }
      </div>
    );
  }

  render() {
    return this.initElement();
  }
};

SelectAddress.propTypes = {
  className: propTypes.string,
  value: propTypes.shape({
    country: propTypes.string,
    city: propTypes.string,
    county: propTypes.string,
    local: propTypes.string,
  }),
  disabled: propTypes.bool,
  check: propTypes.bool,
  required: propTypes.shape({
    city: propTypes.bool,
    county: propTypes.bool,
    local: propTypes.bool,
  }),
  err: propTypes.bool,
  isUpdatePropsToState: propTypes.bool
};

SelectAddress.defaultProps = {
  className: '',
  value: {
    country: '',
    city: '',
    county: '',
    local: ''
  },
  disabled: false,
  check: false,
  required: {
    city: false,
    county: false,
    local: false
  },
  err: false,
  isUpdatePropsToState: false
};

module.exports = SelectAddress;
