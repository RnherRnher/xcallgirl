const React = require('react');
const propTypes = require('prop-types');
const classNames = require('classnames');
const constant = require('../../../../../commons/utils/constant');
const regular = require('../../../../../commons/utils/regular-expression');
const { FormattedMessage } = require('react-intl');
const {
  TextField,
  MenuItem,
  Grid,
  FormHelperText,
  FormLabel
} = require('@material-ui/core');

const SelectDate = class SelectDate extends React.Component {
  constructor(props) {
    super(props);

    const { value } = this.props;
    this.state = {
      day: value && value.day >= 0 ? value.day : '',
      month: value && value.month >= 0 ? value.month : '',
      year: value && value.year ? value.year : '',
      error: false
    };

    this.handleChange = this.handleChange.bind(this);
  }

  componentDidMount() {
    const { day, month, year } = this.state;

    if (day || month || year) {
      this.setState({ error: (day === '' || month === '' || year === '') });
    }
  }

  componentWillReceiveProps(nextProps) {
    const { isUpdatePropsToState } = this.props;

    if (isUpdatePropsToState) {
      const { value } = nextProps;

      const day = value && value.day >= 0 ? value.day : '';
      const month = value && value.month >= 0 ? value.month : '';
      const year = value && value.year ? value.year : '';

      this.setState({
        day,
        month,
        year,
        error: day === ''
          || month === ''
          || year === ''
      });
    }
  }

  getData() {
    const { date } = this.props;

    const Day = [];
    const Month = [];
    const Year = [];

    for (let index = date.day.min; index <= date.day.max; index++) {
      Day.push(
        <MenuItem key={`day-${index}`} value={index + 1}>
          {index + 1}
        </MenuItem>
      );

      if (index <= date.month.max) {
        Month.push(
          <MenuItem key={`month-${index}`} value={index}>
            {index + 1}
          </MenuItem>
        );
      }
    }

    for (let index = date.year.max; index >= date.year.min; index--) {
      Year.push(
        <MenuItem key={`year-${index}`} value={index}>
          {index}
        </MenuItem>
      );
    }

    return {
      Day,
      Month,
      Year
    };
  }

  handleChange(type) {
    return (event) => {
      const { value } = event.target;

      this.setState((state, props) => {
        const currentState = state;
        currentState[type] = value;

        return {
          [type]: value,
          error: currentState.day === ''
            || currentState.month === ''
            || currentState.year === ''
        };
      });
    };
  }

  initElement() {
    const {
      className,
      disabled,
      err,
      label
    } = this.props;
    const {
      day,
      month,
      year,
      error
    } = this.state;

    const {
      Day,
      Month,
      Year
    } = this.getData();

    return (
      <div className={classNames('xcg-select-date', className)}>
        {label ? <FormLabel>{label}</FormLabel> : null}
        <Grid container spacing={8}>
          <Grid item xs={4}>
            <TextField
              label={<FormattedMessage id="info.time.day" />}
              select
              fullWidth
              value={day}
              disabled={disabled}
              error={error || err}
              onChange={this.handleChange(constant.DAY)}
            >
              {Day}
            </TextField>
          </Grid>
          <Grid item xs={4}>
            <TextField
              label={<FormattedMessage id="info.time.month" />}
              select
              fullWidth
              value={month}
              disabled={disabled}
              error={error || err}
              onChange={this.handleChange(constant.MONTH)}
            >
              {Month}
            </TextField>
          </Grid>
          <Grid item xs={4}>
            <TextField
              label={<FormattedMessage id="info.time.year" />}
              select
              fullWidth
              value={year}
              disabled={disabled}
              error={error || err}
              onChange={this.handleChange(constant.YEAR)}
            >
              {Year}
            </TextField>
          </Grid>
        </Grid>
        {error || err
          ? <FormHelperText error>
            <FormattedMessage id="info.time.notVaild" />
          </FormHelperText>
          : null
        }
      </div>
    );
  }

  render() {
    return this.initElement();
  }
};

SelectDate.propTypes = {
  className: propTypes.string,
  value: propTypes.shape({
    day: propTypes.any,
    month: propTypes.any,
    year: propTypes.any,
  }),
  disabled: propTypes.bool,
  err: propTypes.bool,
  date: propTypes.object,
  label: propTypes.node,
  isUpdatePropsToState: propTypes.bool,
};

SelectDate.defaultProps = {
  className: '',
  value: {
    day: '',
    month: '',
    year: ''
  },
  disabled: false,
  err: false,
  date: {
    day: {
      min: 0,
      max: 30,
    },
    month: {
      min: 0,
      max: 11,
    },
    year: {
      min: new Date().getFullYear() - (100 - regular.app.ageRestriction),
      max: new Date().getFullYear() - regular.app.ageRestriction,
    },
  },
  label: null,
  isUpdatePropsToState: false
};

module.exports = SelectDate;
