const React = require('react');
const propTypes = require('prop-types');
const classNames = require('classnames');
const constant = require('../../../../../commons/utils/constant');
const { FormattedMessage } = require('react-intl');
const {
  TextField,
  MenuItem,
  Grid,
  FormHelperText,
  FormLabel
} = require('@material-ui/core');

const SelectTime = class SelectTime extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      hours: '',
      minutes: '',
      error: false
    };

    this.handleChange = this.handleChange.bind(this);
  }

  getData(type) {
    const { max, min } = this.props;
    const { hours } = this.state;

    const Hours = [];
    const Minutes = [];

    const currentMinHours = min.minutes > max.minutes
      ? min.hour === max.hours ? 0 : min.hours + 1
      : min.hours;

    for (
      let index = currentMinHours;
      index <= max.hours;
      index++
    ) {
      Hours.push(
        <MenuItem key={`hours-${index}`} value={index}>
          {index}
        </MenuItem>
      );
    }

    for (
      let index = hours === currentMinHours
        ? (
          min.minutes > max.minutes
            ? min.minutes - max.minutes
            : min.minutes
        )
        : 0;
      index <= max.minutes;
      index++
    ) {
      Minutes.push(
        <MenuItem key={`minutes-${index}`} value={index}>
          {index}
        </MenuItem>
      );
    }

    switch (type) {
      case constant.HOURS:
        return Hours;
      case constant.MINUTES:
        return Minutes;
      default:
        break;
    }
  }

  handleChange(type) {
    return (event) => {
      const { value } = event.target;

      this.setState((state, props) => {
        const currentState = state;
        currentState[type] = value;

        if (props.handleChangeParent) {
          props.handleChangeParent({ [type]: value });
        }

        return {
          [type]: value,
          error: currentState.hours === '' || currentState.minutes === ''
        };
      });
    };
  }

  initElement() {
    const {
      className,
      disabled,
      err,
      label
    } = this.props;
    const {
      hours,
      minutes,
      error
    } = this.state;

    return (
      <div className={classNames('xcg-select-time', className)}>
        {label ? <FormLabel>{label}</FormLabel> : null}
        <Grid container spacing={8}>
          <Grid item xs={6}>
            <TextField
              label={<FormattedMessage id="info.time.hours" />}
              select
              fullWidth
              value={hours}
              disabled={disabled}
              error={error || err}
              onChange={this.handleChange(constant.HOURS)}
            >
              <MenuItem value="" />
              {this.getData(constant.HOURS)}
            </TextField>
          </Grid>
          <Grid item xs={6}>
            <TextField
              label={<FormattedMessage id="info.time.minutes" />}
              select
              fullWidth
              value={minutes}
              disabled={disabled}
              error={error || err}
              onChange={this.handleChange(constant.MINUTES)}
            >
              <MenuItem value="" />
              {this.getData(constant.MINUTES)}
            </TextField>
          </Grid>
        </Grid>
        {error || err
          ? <FormHelperText error>
            <FormattedMessage id="info.time.notVaild" />
          </FormHelperText>
          : null
        }
      </div>
    );
  }

  render() {
    return this.initElement();
  }
};

SelectTime.propTypes = {
  className: propTypes.string,
  disabled: propTypes.bool,
  err: propTypes.bool,
  label: propTypes.node,
  min: propTypes.shape({
    hours: propTypes.number.isRequired,
    minutes: propTypes.number.isRequired,
  }),
  max: propTypes.shape({
    hours: propTypes.number.isRequired,
    minutes: propTypes.number.isRequired,
  }),
  handleChangeParent: propTypes.func
};

SelectTime.defaultProps = {
  className: '',
  disabled: false,
  err: false,
  label: null,
  min: {
    hours: 0,
    minutes: 0,
  },
  max: {
    hours: 23,
    minutes: 59,
  },
  handleChangeParent: null
};

module.exports = SelectTime;
