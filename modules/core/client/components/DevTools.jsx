const React = require('react');
const LogMonitor = require('redux-devtools-log-monitor').default;
const DockMonitor = require('redux-devtools-dock-monitor').default;
const { createDevTools, persistState } = require('redux-devtools');

const _DevTools_ = (
  <DockMonitor
    toggleVisibilityKey="ctrl-h"
    changePositionKey="ctrl-q"
  >
    <LogMonitor />
  </DockMonitor>
);
const DevTools = createDevTools(_DevTools_);

module.exports.DevTools = DevTools;
module.exports.instrument = DevTools.instrument;
module.exports.persistState = persistState;
