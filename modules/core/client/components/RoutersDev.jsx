const React = require('react');
const propTypes = require('prop-types');
const Locale = require('./Locale');
const Theme = require('./Theme');
const url = require('../utils/url');
const constant = require('../../../../commons/utils/constant');
const Notifications = require('../../../notifications/client/components/Notifications');
const ActivityHistorys = require('../../../activity-historys/client/components/ActivityHistorys');
// const { DevTools } = require('./DevTools');
const { createStructuredSelector } = require('reselect');
const { Provider, connect } = require('react-redux');
const { FormattedMessage } = require('react-intl');
const { ConnectedRouter } = require('connected-react-router/immutable');
const { Switch, Route, } = require('react-router-dom');
const { NavigationMenu, NavigationMain } = require('./Navigations');
const { System } = require('../../../system/client/components/Systems');
const { Chats, CommonDetailChat } = require('../../../chats/client/components/Chats');
const {
  ForgotPassword,
  Signin,
  Signup
} = require('../../../users/client/components/Authentication');
const {
  SettingsMenu,
  UserSettingLanguage,
  UserSettingNotifications,
  UserSettingInfoAccount,
  UserSettingPassword,
  UserSettingVerify,
  UserSettingPrivacy,
  CommonDetailUser,
  ManagerUser,
} = require('../../../users/client/components/Users');
const {
  UsersManager,
  UsersSearch
} = require('../../../users/client/components/Admins');
const { NavigationAdmin } = require('../../../users/client/components/Navigations');
const { Store } = require('../../../store/client/components/Stores');
const {
  ManagerBusinessCard,
  EditBusinessCard,
  CommonDetailBusinessCard,
  BusinessCards
} = require('../../../business-cards/client/components/BusinessCards');
const { AlertSnackbar } = require('../../../core/client/components/Alerts');
const { Loadable } = require('./Loadables');
const { makeSelectAlertSnackbar } = require('../reselects/core.client.reselect');
const { makeSelectLocation } = require('../reselects/router.client.reselect');
const { makeSelectSelfUser } = require('../../../users/client/reselects/user.client.reselect');
const { initApp } = require('../actions/app.client.action');
const { DealHistorys, CommonDetailDealHistory } = require('../../../business-cards/client/components/DealHistorys');

const RoutersDev = class RoutersDev extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    const { user, initApp } = this.props;

    if (!user.data) {
      initApp();
    }
  }

  Alert() {
    const { alertSnackbar } = this.props;

    return (
      alertSnackbar
        ? <AlertSnackbar
          type="popup"
          isAction
          variant={alertSnackbar.variant}
          autoHideDuration={5000}
          message={<FormattedMessage id={alertSnackbar.code} />}
        />
        : null
    );
  }

  SetingsRoute() {
    const { location } = this.props;

    let Setting = null;
    switch (location.pathname) {
      case url.SETTINGS:
        Setting = SettingsMenu;
        break;
      case url.SETTINGS_LANGUAGE:
        Setting = UserSettingLanguage;
        break;
      case url.SETTINGS_NOTIFICATIONS:
        Setting = UserSettingNotifications;
        break;
      case url.SETTINGS_ACCOUNT:
        Setting = UserSettingInfoAccount;
        break;
      case url.SETTINGS_PASSWORD:
        Setting = UserSettingPassword;
        break;
      case url.SETTINGS_VERIFY:
        Setting = UserSettingVerify;
        break;
      case url.SETTINGS_PRIVACY:
        Setting = UserSettingPrivacy;
        break;
      default:
        break;
    }

    return Setting;
  }

  BusinessCardRoute() {
    const { location } = this.props;

    let BusinessCard = null;
    switch (location.pathname) {
      case url.BUSINESS_CARD:
        BusinessCard = ManagerBusinessCard;
        break;
      case url.BUSINESS_CARD_EDIT:
        BusinessCard = EditBusinessCard;
        break;
      case url.BUSINESS_CARD_GET:
        BusinessCard = CommonDetailBusinessCard;
        break;
      default:
        break;
    }

    return BusinessCard;
  }

  UserRoute() {
    const { location } = this.props;

    let User = null;
    switch (location.pathname) {
      case url.USER:
        User = ManagerUser;
        break;
      case url.USER_GET:
        User = CommonDetailUser;
        break;
      default:
        break;
    }

    return User;
  }

  GuestRouters() {
    return (
      <div>
        <Route exact path={url.ROOT} component={Signin} />
        <Route path={url.AUTH_SIGNIN} component={Signin} />
        <Route path={url.AUTH_SIGNUP} component={Signup} />
        <Route path={url.AUTH_FORGOT_PASSWORD} component={ForgotPassword} />
      </div>
    );
  }

  AdminRoute() {
    const { location } = this.props;

    let Admin = null;
    switch (location.pathname) {
      case url.ADMIN:
        Admin = NavigationAdmin;
        break;
      case url.ADMIN_USERS_MANAGER:
        Admin = UsersManager;
        break;
      default:
        break;
    }

    return Admin;
  }

  AdminRouters() {
    return (
      <div>
        <Route path={url.ADMIN} component={this.AdminRoute()} />
      </div>
    );
  }

  SystemRouters() {
    return (
      <div>
        <Route exact path={url.ROOT} component={System} />
        <Route exact path={url.SEARCH} component={UsersSearch} />
      </div>
    );
  }

  UserRouters() {
    const { user } = this.props;

    return (
      <div>
        {
          user.data && user.data.roles.value === constant.SYSTEM
            ? this.SystemRouters()
            : <div>
              <Route exact path={url.ROOT} component={BusinessCards} />
              <Route path={url.CHATS} component={Chats} />
              <Route path={url.CHAT_GET} component={CommonDetailChat} />
              <Route path={url.DEAL_HISTORYS} component={DealHistorys} />
              <Route path={url.DEAL_HISTORY_GET} component={CommonDetailDealHistory} />
              <Route path={url.BUSINESS_CARD} component={this.BusinessCardRoute()} />
            </div>
        }
        <Route path={url.NOTIFICATIONS} component={Notifications} />
        <Route path={url.MENU} component={NavigationMenu} />
        <Route path={url.USER} component={this.UserRoute()} />
        <Route path={url.ACTIVITY_HISTORYS} component={ActivityHistorys} />
        {
          user.data
            && (user.data.roles.value === constant.ADMIN
              || user.data.roles.value === constant.SYSTEM)
            ? this.AdminRouters()
            : null
        }
        <Route path={url.STORE} component={Store} />
        <Route path={url.SETTINGS} component={this.SetingsRoute()} />
      </div>
    );
  }

  initElement() {
    const {
      user,
      store,
      history,
    } = this.props;

    return (
      <Provider store={store}>
        <Theme>
          <Locale>
            <ConnectedRouter history={history}>
              <div className="xcg-routers">
                {this.Alert()}
                <Switch>
                  <div className="xcg-switch">
                    <Loadable
                      result={user.result}
                      SuccessNode={this.UserRouters()}
                      FailedNode={this.GuestRouters()}
                    />
                  </div>
                </Switch>
                <Loadable
                  result={user.result}
                  WaitNode={null}
                  SuccessNode={<NavigationMain />}
                />
                {/* <DevTools /> */}
              </div>
            </ConnectedRouter>
          </Locale>
        </Theme>
      </Provider>
    );
  }

  render() {
    return this.initElement();
  }
};

RoutersDev.propTypes = {
  alertSnackbar: propTypes.object,
  user: propTypes.object.isRequired,
  store: propTypes.object.isRequired,
  history: propTypes.object.isRequired,
  location: propTypes.object.isRequired,
  initApp: propTypes.func.isRequired,
};

RoutersDev.defaultProps = {
  alertSnackbar: null
};

function mapStateToProps(state, props) {
  return createStructuredSelector({
    alertSnackbar: makeSelectAlertSnackbar(),
    user: makeSelectSelfUser(),
    location: makeSelectLocation(),
  });
}

function mapDispatchToProps(dispatch, props) {
  return {
    initApp: () => {
      return dispatch(initApp());
    }
  };
}

module.exports = connect(
  mapStateToProps,
  mapDispatchToProps,
)(RoutersDev);
