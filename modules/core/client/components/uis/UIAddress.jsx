const React = require('react');
const propTypes = require('prop-types');
const classNames = require('classnames');
const constant = require('../../../../../commons/utils/constant');
const { FormattedMessage } = require('react-intl');
const {
  Table,
  TableBody,
  TableRow,
  TableCell,
} = require('@material-ui/core');
const { getAddress } = require('../../../../core/client/utils/middleware');

const UIAddress = class UIAddress extends React.Component {
  constructor(props) {
    super(props);
  }

  initElement() {
    const { className, value } = this.props;

    return (
      <div className={classNames('xcg-ui-address', className)}>
        <Table>
          <TableBody>
            <TableRow>
              <TableCell>
                <FormattedMessage id="info.address.country.is" />
              </TableCell>
              <TableCell>
                {getAddress(constant.COUNTRY, value).name}
              </TableCell>
            </TableRow>
            <TableRow>
              <TableCell>
                <FormattedMessage id="info.address.city.is" />
              </TableCell>
              <TableCell>
                {getAddress(constant.CITY, value).name}
              </TableCell>
            </TableRow>
            <TableRow>
              <TableCell>
                <FormattedMessage id="info.address.county.is" />
              </TableCell>
              <TableCell>
                {getAddress(constant.COUNTY, value).name}
              </TableCell>
            </TableRow>
            <TableRow>
              <TableCell>
                <FormattedMessage id="info.address.local.is" />
              </TableCell>
              <TableCell>
                {getAddress(constant.LOCAL, value).name}
              </TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </div>
    );
  }

  render() {
    return this.initElement();
  }
};

UIAddress.propTypes = {
  className: propTypes.string,
  value: propTypes.object.isRequired,
};

UIAddress.defaultProps = {
  className: ''
};

module.exports = UIAddress;
