const React = require('react');
const propTypes = require('prop-types');
const classNames = require('classnames');
const UIComment = require('./UIComment');
const constant = require('../../../../../commons/utils/constant');
const { FormattedMessage } = require('react-intl');
const {
  Close,
  Replay
} = require('@material-ui/icons');
const {
  AppBar,
  IconButton,
  Toolbar,
  Dialog,
  Slide,
  FormLabel
} = require('@material-ui/core');
const { InputMessage } = require('../../../../chats/client/components/Inputs');
const { LoadableComment } = require('../Loadables');
const { ContainerNotData } = require('../Containers');

const UIListComment = class UIListComment extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      currentComment: null,
      currentAction: constant.ADD
    };

    this.handleClickMenuItem = this.handleClickMenuItem.bind(this);
    this.handleClickResetInputComment = this.handleClickResetInputComment.bind(this);
    this.handleClickClose = this.handleClickClose.bind(this);

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  getData() {
    const {
      type,
      value,
      disabled,
      ownerID
    } = this.props;

    let ListComment = null;

    if (value.length) {
      ListComment = value.map((value) => {
        return <UIComment
          ownerID={ownerID}
          key={value.comment._id}
          value={value}
          handleClickMenuItem={this.handleClickMenuItem}
        />;
      });
    } else if (disabled.push) {
      ListComment = <div>
        <LoadableComment />
        <LoadableComment />
        <LoadableComment />
      </div>;
    } else {
      ListComment = <div>
        <ContainerNotData />
        <LoadableComment />
      </div>;
    }

    return (
      <div
        className={
          type === constant.DIALOG
            ? 'xcg-dialog-group-comments'
            : 'xcg-static-group-comments'
        }
      >
        {ListComment}
      </div>
    );
  }

  handleClickClose(event) {
    const { handleClickClose } = this.props;

    this.resetState();

    handleClickClose();
  }

  resetState() {
    this.setState({
      currentComment: null,
      currentAction: constant.ADD
    });
  }

  handleClickMenuItem(action, comment) {
    const { handleClickMenuItem } = this.props;

    switch (action) {
      case constant.EDIT:
        this.setState({
          currentComment: comment,
          currentAction: action
        });
        break;
      case constant.DELETE:
        handleClickMenuItem(action, comment);
        break;
      default:
        this.resetState();
        break;
    }
  }

  handleSubmit(msg) {
    const { handleSubmit } = this.props;
    const { currentAction, currentComment } = this.state;

    const comment = currentComment || {};
    comment.msg = msg;

    handleSubmit(currentAction, comment);

    this.resetState();
  }

  handleClickResetInputComment() {
    this.resetState();
  }

  Transition(props) {
    return <Slide direction="up" {...props} />;
  }

  Dialog(children) {
    const {
      className,
      open,
      handleClickPush,
      pushTitleNode,
      disabled,
    } = this.props;

    return (
      <Dialog
        className={classNames('xcg-ui-list-commnet', className)}
        fullScreen
        open={open}
        onClose={this.handleClickClose}
        TransitionComponent={this.Transition}
      >
        <AppBar className="xcg-top-app-bar">
          <Toolbar>
            <IconButton
              onClick={handleClickPush}
              disabled={disabled.push}
            >
              <Replay />
            </IconButton>
            {pushTitleNode}
            <IconButton
              className="xcg-close-button"
              onClick={this.handleClickClose}
              disabled={
                disabled.editSubmit
                || disabled.addSubmit
                || disabled.push
              }
            >
              <Close />
            </IconButton>
          </Toolbar>
        </AppBar>
        {children}
      </Dialog>
    );
  }

  Static(children) {
    const {
      className,
      handleClickPush,
      pushTitleNode,
      disabled,
    } = this.props;

    return (
      <div className={classNames('xcg-ui-list-commnet', className)}>
        <Toolbar>
          <IconButton
            color="secondary"
            onClick={handleClickPush}
            disabled={disabled.push}
          >
            <Replay />
          </IconButton>
          <FormLabel>
            {pushTitleNode}
          </FormLabel>
        </Toolbar>
        {children}
      </div>
    );
  }

  Children() {
    const { validate, disabled } = this.props;
    const { currentComment, currentAction } = this.state;

    return (
      <div>
        {this.getData()}
        <AppBar className="xcg-botton-app-bar" position="fixed">
          <InputMessage
            TitleNode={<FormattedMessage id="info.comment.is" />}
            type={currentAction}
            variant={constant.COMMENT}
            isUpdatePropsToState
            value={currentComment && currentComment.msg ? currentComment.msg : ''}
            validate={validate}
            disabled={disabled.edit || disabled.add}
            handleSubmit={this.handleSubmit}
            handleClickReset={this.handleClickResetInputComment}
          />
        </AppBar>
      </div>
    );
  }

  initElement() {
    const { type } = this.props;

    switch (type) {
      case constant.DIALOG:
        return this.Dialog(this.Children());
      case constant.STATIC:
        return this.Static(this.Children());
      default:
        return null;
    }
  }

  render() {
    return this.initElement();
  }
};

UIListComment.propTypes = {
  className: propTypes.string,
  type: propTypes.oneOf([constant.DIALOG, constant.STATIC]),
  value: propTypes.array,
  validate: propTypes.any,
  pushTitleNode: propTypes.node,
  open: propTypes.bool,
  handleClickClose: propTypes.func,
  disabled: propTypes.shape({
    push: propTypes.bool.isRequired,
    add: propTypes.bool.isRequired,
    edit: propTypes.bool.isRequired
  }).isRequired,
  ownerID: propTypes.string.isRequired,
  handleClickPush: propTypes.func.isRequired,
  handleClickMenuItem: propTypes.func.isRequired,
  handleSubmit: propTypes.func.isRequired,
};

UIListComment.defaultProps = {
  className: 'xcg-dialog',
  type: constant.DIALOG,
  value: [],
  validate: null,
  pushTitleNode: null,
  open: false,
  handleClickClose: null
};

module.exports = UIListComment;
