const React = require('react');
const propTypes = require('prop-types');
const classNames = require('classnames');
const constant = require('../../../../../commons/utils/constant');

const UITimer = class UITimer extends React.Component {
  constructor(props) {
    super(props);
  }

  initElement() {
    const { className, value } = this.props;

    const hours = value < 0 ? 0 : value / constant.ENUM.TIME.ONE_HOURS;
    const minutes = value < 0 ? 0 : (hours % 1) * 60;
    const milliseconds = value < 0 ? 0 : (minutes % 1) * 60;

    return (
      <div className={classNames('xcg-ui-timer', className)}>
        {parseInt(hours, 10)}{'h'}{parseInt(minutes, 10)}{'\''}{parseInt(milliseconds, 10)}
      </div>
    );
  }

  render() {
    return this.initElement();
  }
};

UITimer.propTypes = {
  className: propTypes.string,
  value: propTypes.any.isRequired,
};

UITimer.defaultProps = {
  className: ''
};

module.exports = UITimer;
