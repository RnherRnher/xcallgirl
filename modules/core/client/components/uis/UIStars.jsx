const React = require('react');
const propTypes = require('prop-types');
const classNames = require('classnames');
const {
  Star,
  StarHalf,
  StarBorder
} = require('@material-ui/icons');

const UIStars = class UIStars extends React.Component {
  constructor(props) {
    super(props);
  }

  initElement() {
    const {
      className,
      value,
      maxStar
    } = this.props;

    const Stars = [];
    let currentValue = value;

    for (let index = 0; index < maxStar; index++) {
      if (index < value) {
        currentValue -= 1;
        if (currentValue % 1 === -0.5) {
          Stars.push(<StarHalf key={index} />);
        } else {
          Stars.push(<Star key={index} />);
        }
      } else {
        Stars.push(<StarBorder key={index} />);
      }
    }

    return (
      <div className={classNames('xcg-ui-stars', className)}>
        {Stars}
      </div>
    );
  }

  render() {
    return this.initElement();
  }
};

UIStars.propTypes = {
  className: propTypes.string,
  maxStar: propTypes.number,
  value: propTypes.number.isRequired,
};

UIStars.defaultProps = {
  className: '',
  maxStar: 5
};

module.exports = UIStars;
