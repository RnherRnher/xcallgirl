const React = require('react');
const propTypes = require('prop-types');
const classNames = require('classnames');
const constant = require('../../../../../commons/utils/constant');
const url = require('../../../../core/client/utils/url');
const UIDate = require('./UIDate');
const { FormattedMessage } = require('react-intl');
const { push } = require('connected-react-router/immutable');
const { connect } = require('react-redux');
const {
  Store,
  Whatshot,
  MoreVert,
  Edit,
  Delete,
  VerifiedUser
} = require('@material-ui/icons');
const {
  Card,
  CardHeader,
  ListItemText,
  CardActionArea,
  Avatar,
  Typography,
  IconButton,
  Menu,
  MenuItem,
  ListItemIcon,
  CardContent
} = require('@material-ui/core');

const UIComment = class UIComment extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      clicked: false,
      anchorEl: null
    };

    this.handleClick = this.handleClick.bind(this);
    this.handleClickMenuItem = this.handleClickMenuItem.bind(this);
    this.handleCloseMenu = this.handleCloseMenu.bind(this);
  }

  handleClick(type) {
    return (event) => {
      const {
        redirect,
        value
      } = this.props;

      switch (type) {
        case constant.USER:
          redirect(`${url.USER_GET}?id=${value.user._id}`);
          break;
        case constant.MORE:
          this.setState({
            clicked: true,
            anchorEl: event.target
          });
          break;
        default:
          break;
      }
    };
  }

  handleClickMenuItem(action) {
    return (event) => {
      const { value, handleClickMenuItem } = this.props;

      handleClickMenuItem(action, value.comment);

      this.handleCloseMenu();
    };
  }

  handleCloseMenu(event) {
    this.setState({
      clicked: false,
      anchorEl: null
    });
  }

  Menu() {
    const { clicked, anchorEl } = this.state;

    return (
      <Menu
        anchorEl={anchorEl}
        open={clicked}
        onClose={this.handleCloseMenu}
      >
        <MenuItem
          onClick={this.handleClickMenuItem(constant.EDIT)}
          value={constant.EDIT}
        >
          <ListItemIcon>
            <Edit />
          </ListItemIcon>
          <ListItemText
            inset
            primary={<FormattedMessage id="actions.edit" />}
          />
        </MenuItem>
        <MenuItem
          onClick={this.handleClickMenuItem(constant.DELETE)}
          value={constant.DELETE}
        >
          <ListItemIcon>
            <Delete />
          </ListItemIcon>
          <ListItemText
            inset
            primary={<FormattedMessage id="actions.delete" />}
          />
        </MenuItem>
      </Menu>
    );
  }

  IconProfileUser() {
    const { value, ownerID } = this.props;

    let IconProfileUser = null;

    if (value.user._id === ownerID) {
      IconProfileUser = <Store className="xcg-profile-user-icon xcg-success" />;
    } else if (value.user.roles === constant.ADMIN) {
      IconProfileUser = <Whatshot className="xcg-profile-user-icon xcg-error" />;
    } else if (value.user.verifyAccount) {
      IconProfileUser = <VerifiedUser className="xcg-profile-user-icon xcg-info" />;
    }

    return IconProfileUser;
  }

  initElement() {
    const {
      className,
      value,
    } = this.props;

    return (
      <div className={classNames('xcg-ui-comment', className)}>
        <Card className="xcg-card">
          <CardHeader
            title={
              <div className="xcg-profile-user-group">
                <div className="xcg-profile-user-title">
                  {value.user.nick}
                </div>
                <div className="xcg-profile-user-list-icon">
                  {this.IconProfileUser()}
                </div>
                <div className="xcg-profile-user-caption">
                  <Typography variant="caption">
                    <UIDate value={value.comment.initDate} />
                  </Typography>
                </div>
              </div>
            }
            avatar={
              <CardActionArea onClick={this.handleClick(constant.USER)}>
                <Avatar alt={value.user.nick} src={value.user.avatar} />
              </CardActionArea>
            }
            action={
              value.isOwner
                ? <IconButton onClick={this.handleClick(constant.MORE)}>
                  <MoreVert />
                </IconButton>
                : null
            }
          />
          <CardContent className="xcg-card-content">
            <Typography>
              {value.comment.msg}
            </Typography>
          </CardContent>
        </Card>
        {value.isOwner ? this.Menu() : null}
      </div>
    );
  }

  render() {
    return this.initElement();
  }
};

UIComment.propTypes = {
  className: propTypes.string,
  ownerID: propTypes.string.isRequired,
  value: propTypes.shape({
    user: propTypes.object.isRequired,
    comment: propTypes.object.isRequired,
    isOwner: propTypes.bool.isRequired
  }).isRequired,
  redirect: propTypes.func.isRequired,
  handleClickMenuItem: propTypes.func.isRequired
};

UIComment.defaultProps = {
  className: ''
};


function mapStateToProps(state, props) {
  return {};
}

function mapDispatchToProps(dispatch, props) {
  return {
    redirect: (location) => {
      return dispatch(push(location));
    }
  };
}

module.exports = connect(
  mapStateToProps,
  mapDispatchToProps,
)(UIComment);
