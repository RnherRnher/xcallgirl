const React = require('react');
const propTypes = require('prop-types');
const classNames = require('classnames');
const constant = require('../../../../../commons/utils/constant');
const {
  FormattedDate,
  FormattedRelative,
  FormattedTime
} = require('react-intl');

const UIDate = class UIDate extends React.Component {
  constructor(props) {
    super(props);
  }

  initElement() {
    const {
      className,
      value,
      limitDate
    } = this.props;

    const isWeekLimitDate = Date.now() >= new Date(value).getTime() + limitDate;
    const isDayLimitDate = Date.now() >= new Date(value).getTime() + constant.ENUM.TIME.ONE_DAY;

    return (
      <span className={classNames('xcg-ui-date', className)}>
        {
          isWeekLimitDate
            ? <FormattedDate
              value={value}
              weekday="narrow"
              // era="narrow"
              year="numeric"
              month="numeric"
              day="numeric"
              hour="numeric"
              minute="numeric"
              second="numeric"
            />
            : isDayLimitDate
              ? <span>
                <FormattedTime value={value} />
                <span className="xcg-space">&bull;</span>
                <FormattedRelative value={value} />
              </span>
              : <FormattedRelative value={value} />
        }
      </span>
    );
  }

  render() {
    return this.initElement();
  }
};

UIDate.propTypes = {
  className: propTypes.string,
  limitDate: propTypes.number,
  value: propTypes.any.isRequired,
};

UIDate.defaultProps = {
  className: '',
  limitDate: 7 * constant.ENUM.TIME.ONE_DAY
};

module.exports = UIDate;
