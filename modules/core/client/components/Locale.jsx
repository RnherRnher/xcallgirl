const React = require('react');
const propTypes = require('prop-types');
const { IntlProvider } = require('react-intl');
const { connect } = require('react-redux');
const { createStructuredSelector } = require('reselect');
const { makeSelectLocale } = require('../reselects/locale.client.reselect');

const Locale = class Locale extends React.Component {
  constructor(props) {
    super(props);
  }

  initElement() {
    const { children, locale } = this.props;

    return (
      <IntlProvider
        locale={locale.locale}
        messages={locale.messages}
      >
        {children}
      </IntlProvider>
    );
  }

  render() {
    return this.initElement();
  }
};

Locale.propTypes = {
  locale: propTypes.object.isRequired,
  children: propTypes.element.isRequired,
};


function mapStateToProps(state, props) {
  return createStructuredSelector({
    locale: makeSelectLocale(),
  });
}

function mapDispatchToProps(dispatch, props) {
  return {};
}

module.exports = connect(
  mapStateToProps,
  mapDispatchToProps,
)(Locale);
