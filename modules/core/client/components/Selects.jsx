const SelectAddress = require('./selects/SelectAddress');
const SelectCountry = require('./selects/SelectCountry');
const SelectDate = require('./selects/SelectDate');
const SelectTime = require('./selects/SelectTime');

module.exports = {
  SelectAddress,
  SelectCountry,
  SelectDate,
  SelectTime
};
