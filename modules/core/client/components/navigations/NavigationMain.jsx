const React = require('react');
const propTypes = require('prop-types');
const classNames = require('classnames');
const url = require('../../utils/url');
const constant = require('../../../../../commons/utils/constant');
const { connect } = require('react-redux');
const { createStructuredSelector } = require('reselect');
const { push } = require('connected-react-router/immutable');
const {
  Home,
  Notifications,
  Chat,
  Gavel,
  Menu,
  Search,
  BarChart
} = require('@material-ui/icons');
const {
  BottomNavigation,
  BottomNavigationAction,
  Badge
} = require('@material-ui/core');
const { makeSelectCountNotifications } = require('../../../../notifications/client/reselects/notification.client.reselect');
const { makeSelectCountDealHistorys } = require('../../../../business-cards/client/reselects/deal-history.client.reselect');
const { makeSelectAllCountChats } = require('../../../../chats/client/reselects/chat.client.reselect');
const { makeSelectSelfUser } = require('../../../../users/client/reselects/user.client.reselect');

const NavigationMain = class NavigationMain extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      naviga: url.ROOT
    };

    this.handleChangeNavigation = this.handleChangeNavigation.bind(this);
  }

  handleChangeNavigation(event, value) {
    const { redirect } = this.props;

    this.setState({ naviga: value });
    redirect(value);
  }

  initElement() {
    const {
      className,
      user,
      noticationCount,
      dealHistoryCount,
      chatCount,
      countLimit
    } = this.props;
    const { naviga } = this.state;

    return (
      <div className={classNames('xcg-navigation-main', className)}>
        <BottomNavigation value={naviga} onChange={this.handleChangeNavigation}>
          <BottomNavigationAction
            value={url.ROOT}
            icon={
              user.data && user.data.roles.value !== constant.SYSTEM
                ? <Home />
                : <BarChart />
            }
          />
          <BottomNavigationAction
            value={url.NOTIFICATIONS}
            icon={
              noticationCount
                ? <Badge
                  color="primary"
                  badgeContent={
                    noticationCount > countLimit
                      ? `${countLimit}+`
                      : noticationCount
                  }
                >
                  <Notifications />
                </Badge>
                : <Notifications />
            }
          />
          {
            user.data && user.data.roles.value !== constant.SYSTEM
              ? <BottomNavigationAction
                value={url.CHATS}
                icon={
                  chatCount
                    ? <Badge
                      color="primary"
                      badgeContent={
                        chatCount > countLimit
                          ? `${countLimit}+`
                          : chatCount
                      }
                    >
                      <Chat />
                    </Badge>
                    : <Chat />
                }
              />
              : null
          }
          {
            user.data && user.data.roles.value !== constant.SYSTEM
              ? <BottomNavigationAction
                value={url.DEAL_HISTORYS}
                icon={
                  dealHistoryCount
                    ? <Badge
                      color="primary"
                      badgeContent={
                        dealHistoryCount > countLimit
                          ? `${countLimit}+`
                          : dealHistoryCount
                      }
                    >
                      <Gavel />
                    </Badge>
                    : <Gavel />
                }
              />
              : <BottomNavigationAction
                value={url.SEARCH}
                icon={<Search />}
              />
          }
          <BottomNavigationAction
            value={url.MENU}
            icon={<Menu />}
          />
        </BottomNavigation>
      </div>
    );
  }

  render() {
    return this.initElement();
  }
};

NavigationMain.propTypes = {
  className: propTypes.string,
  countLimit: propTypes.number,
  user: propTypes.object.isRequired,
  noticationCount: propTypes.number.isRequired,
  dealHistoryCount: propTypes.number.isRequired,
  chatCount: propTypes.number.isRequired,
  redirect: propTypes.func.isRequired,
};

NavigationMain.defaultProps = {
  className: '',
  countLimit: constant.ENUM.COUNT.LIMIT
};

function mapStateToProps(state, props) {
  return createStructuredSelector({
    user: makeSelectSelfUser(),
    noticationCount: makeSelectCountNotifications(),
    dealHistoryCount: makeSelectCountDealHistorys(),
    chatCount: makeSelectAllCountChats(),
  });
}

function mapDispatchToProps(dispatch, props) {
  return {
    redirect: (location) => {
      return dispatch(push(location));
    }
  };
}

module.exports = connect(
  mapStateToProps,
  mapDispatchToProps,
)(NavigationMain);
