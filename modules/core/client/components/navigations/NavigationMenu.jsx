const React = require('react');
const propTypes = require('prop-types');
const classNames = require('classnames');
const url = require('../../utils/url');
const constant = require('../../../../../commons/utils/constant');
const { FormattedMessage } = require('react-intl');
const { createStructuredSelector } = require('reselect');
const { connect } = require('react-redux');
const { push } = require('connected-react-router/immutable');
const {
  Settings,
  ContactMail,
  Restore,
  ExitToApp,
  AccountCircle,
  Whatshot,
  ShoppingCart
} = require('@material-ui/icons');
const {
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
  Avatar,
} = require('@material-ui/core');
const { signoutRequest } = require('../../../../users/client/actions/authentication.client.action');
const { makeSelectSelfUser } = require('../../../../users/client/reselects/user.client.reselect');

const NavigationMenu = class NavigationMenu extends React.Component {
  constructor(props) {
    super(props);

    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(location) {
    return (event) => {
      const { redirect, signoutRequest } = this.props;

      switch (location) {
        case url.ROOT:
          signoutRequest();
          break;
        default:
          redirect(location);
          break;
      }
    };
  }

  initElement() {
    const { className, user } = this.props;

    return (
      <div className={classNames('xcg-navigation-menu', className)}>
        <List>
          <ListItem button onClick={this.handleClick(url.USER)}>
            <ListItemAvatar>
              <Avatar>
                <AccountCircle />
              </Avatar>
            </ListItemAvatar>
            <ListItemText inset primary={<FormattedMessage id="user.is" />} />
          </ListItem>
          {
            user.data && user.data.roles.value !== constant.SYSTEM
              ? <ListItem button onClick={this.handleClick(url.BUSINESS_CARD)}>
                <ListItemAvatar>
                  <Avatar>
                    <ContactMail />
                  </Avatar>
                </ListItemAvatar>
                <ListItemText inset primary={<FormattedMessage id="businessCard.is" />} />
              </ListItem>
              : null
          }
          <ListItem button onClick={this.handleClick(url.ACTIVITY_HISTORYS)}>
            <ListItemAvatar>
              <Avatar>
                <Restore />
              </Avatar>
            </ListItemAvatar>
            <ListItemText inset primary={<FormattedMessage id="activityHistory.is" />} />
          </ListItem>
          {
            user.data
              && (user.data.roles.value === constant.ADMIN
                || user.data.roles.value === constant.SYSTEM)
              ? <ListItem button onClick={this.handleClick(url.ADMIN)}>
                <ListItemAvatar>
                  <Avatar>
                    <Whatshot />
                  </Avatar>
                </ListItemAvatar>
                <ListItemText inset primary={<FormattedMessage id="user.roles.admin" />} />
              </ListItem>
              : null
          }
          {
            user.data && user.data.roles.value !== constant.SYSTEM
              ? <ListItem button onClick={this.handleClick(url.STORE)}>
                <ListItemAvatar>
                  <Avatar>
                    <ShoppingCart />
                  </Avatar>
                </ListItemAvatar>
                <ListItemText inset primary={<FormattedMessage id="store.is" />} />
              </ListItem>
              : null
          }
          <ListItem button onClick={this.handleClick(url.SETTINGS)}>
            <ListItemAvatar>
              <Avatar>
                <Settings />
              </Avatar>
            </ListItemAvatar>
            <ListItemText inset primary={<FormattedMessage id="user.setting.is" />} />
          </ListItem>
          <ListItem button onClick={this.handleClick(url.ROOT)}>
            <ListItemAvatar>
              <Avatar>
                <ExitToApp />
              </Avatar>
            </ListItemAvatar>
            <ListItemText inset primary={<FormattedMessage id="user.sigout.is" />} />
          </ListItem>
        </List>
      </div>
    );
  }

  render() {
    return this.initElement();
  }
};

NavigationMenu.propTypes = {
  className: propTypes.string,
  user: propTypes.object.isRequired,
  redirect: propTypes.func.isRequired,
  signoutRequest: propTypes.func.isRequired
};

NavigationMenu.defaultProps = {
  className: '',
};

function mapStateToProps(state, props) {
  return createStructuredSelector({
    user: makeSelectSelfUser(),
  });
}

function mapDispatchToProps(dispatch, props) {
  return {
    redirect: (location) => {
      return dispatch(push(location));
    },
    signoutRequest: () => {
      return dispatch(signoutRequest());
    }
  };
}

module.exports = connect(
  mapStateToProps,
  mapDispatchToProps,
)(NavigationMenu);
