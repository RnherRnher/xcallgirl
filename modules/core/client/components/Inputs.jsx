const InputLimit = require('./inputs/InputLimit');
const InputNumber = require('./inputs/InputNumber');

module.exports = {
  InputLimit,
  InputNumber
};
