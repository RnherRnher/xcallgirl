const ContainerLoading = require('./containers/ContainerLoading');
const ContainerNotVerifyAccount = require('./containers/ContainerNotVerifyAccount');
const ContainerNotData = require('./containers/ContainerNotData');

module.exports = {
  ContainerLoading,
  ContainerNotVerifyAccount,
  ContainerNotData
};
