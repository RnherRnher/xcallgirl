const React = require('react');
const propTypes = require('prop-types');
const classNames = require('classnames');
const CommonLoadable = require('../../../../core/client/components/loadables/Loadable');
const { List, WindowScroller } = require('react-virtualized');
const { ContainerNotData } = require('../../../../core/client/components/Containers');
const { ResultEnum } = require('../../../../core/client/constants/core.client.constant');

const WindowScroll = class WindowScroll extends React.Component {
  constructor(props) {
    super(props);

    this.listRef = React.createRef();

    this.rowRenderer = this.rowRenderer.bind(this);
    this.onScroll = this.onScroll.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (this.listRef.current) {
      this.listRef.current.forceUpdateGrid();
    }
  }

  onScroll(event) {
    const {
      result,
      initialLoad,
      handleLoadMore
    } = this.props;

    if (
      result !== ResultEnum.wait
      && result !== ResultEnum.failed
      && handleLoadMore
      && (event.scrollTop + event.clientHeight)
      > (event.scrollHeight - event.clientHeight / 3)
    ) {
      handleLoadMore(initialLoad);
    }
  }

  rowRenderer(event) {
    const { value, Children } = this.props;

    return (
      <div key={event.key} style={event.style}>
        <Children value={value[event.index]} />
      </div>
    );
  }

  WindowScroller() {
    const {
      List,
      value,
      rowHeight
    } = this.props;

    return (
      <WindowScroller>
        {(propsWindowScroller) => {
          return <List
            ref={this.listRef}
            autoHeight
            rowCount={value.length}
            rowHeight={rowHeight}
            onScroll={this.onScroll}
            rowRenderer={this.rowRenderer}
            isScrolling={propsWindowScroller.isScrolling}
            scrollTop={propsWindowScroller.scrollTop}
            width={propsWindowScroller.width}
            height={propsWindowScroller.height}
          />;
        }}
      </WindowScroller>
    );
  }

  Loadables() {
    const { Loadable } = this.props;

    return (
      <div>
        <Loadable />
        <Loadable />
        <Loadable />
      </div>
    );
  }

  ContainerNotData() {
    const { Loadable } = this.props;

    return (
      <div>
        <ContainerNotData />
        <Loadable />
      </div>
    );
  }

  initElement() {
    const {
      className,
      value,
      result
    } = this.props;

    return (
      <div className={classNames('xcg-window-scroll', className)}>
        {
          result === null
            ? this.WindowScroller()
            : <CommonLoadable
              result={result}
              NoneNode={this.WindowScroller()}
              WaitNode={
                value.length
                  ? this.WindowScroller()
                  : this.Loadables()
              }
              SuccessNode={
                value.length
                  ? this.WindowScroller()
                  : this.ContainerNotData()
              }
              FailedNode={
                value.length
                  ? this.WindowScroller()
                  : this.ContainerNotData()
              }
            />
        }
      </div>
    );
  }

  render() {
    return this.initElement();
  }
};

WindowScroll.propTypes = {
  className: propTypes.string,
  value: propTypes.array,
  initialLoad: propTypes.bool,
  result: propTypes.number,
  List: propTypes.any,
  Loadable: propTypes.any,
  handleLoadMore: propTypes.func,
  Children: propTypes.any.isRequired,
  rowHeight: propTypes.number.isRequired
};

WindowScroll.defaultProps = {
  className: '',
  value: [],
  initialLoad: false,
  result: null,
  List,
  Loadable: null,
  handleLoadMore: null
};

module.exports = WindowScroll;
