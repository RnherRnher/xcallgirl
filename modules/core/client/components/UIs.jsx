const UIAddress = require('./uis/UIAddress');
const UIComment = require('./uis/UIComment');
const UIDate = require('./uis/UIDate');
const UIListComment = require('./uis/UIListComment');
const UIStars = require('./uis/UIStars');
const UITimer = require('./uis/UITimer');

module.exports = {
  UIAddress,
  UIComment,
  UIDate,
  UIListComment,
  UIStars,
  UITimer
};
