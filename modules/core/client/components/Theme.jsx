const React = require('react');
const propTypes = require('prop-types');
const { MuiThemeProvider } = require('@material-ui/core/styles');
const { connect } = require('react-redux');
const { createStructuredSelector } = require('reselect');
const { makeSelectTheme } = require('../reselects/theme.client.reselect');

const Theme = class Theme extends React.Component {
  constructor(props) {
    super(props);
  }

  initElement() {
    const { children, theme } = this.props;

    return (
      <MuiThemeProvider theme={theme}>
        {children}
      </MuiThemeProvider>
    );
  }

  render() {
    return this.initElement();
  }
};

Theme.propTypes = {
  theme: propTypes.object.isRequired,
  children: propTypes.element.isRequired,
};


function mapStateToProps(state, props) {
  return createStructuredSelector({
    theme: makeSelectTheme(),
  });
}

function mapDispatchToProps(dispatch, props) {
  return {};
}

module.exports = connect(
  mapStateToProps,
  mapDispatchToProps,
)(Theme);
