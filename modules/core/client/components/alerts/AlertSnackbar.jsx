const React = require('react');
const propTypes = require('prop-types');
const classNames = require('classnames');
const constant = require('../../../../../commons/utils/constant');
const { connect } = require('react-redux');
const {
  IconButton,
  Snackbar,
  SnackbarContent
} = require('@material-ui/core');
const {
  Warning,
  CheckCircle,
  Error,
  Info,
  Close,
  // Slide
} = require('@material-ui/icons');
const { popAlertSnackbar } = require('../../actions/core.client.action');

const AlertSnackbar = class AlertSnackbar extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      open: true
    };

    this.handleClick = this.handleClick.bind(this);
    this.handleClose = this.handleClose.bind(this);
  }

  handleClick(event) {
    this.setState({ open: true });
  }

  handleClose(event, reason) {
    const { popAlertSnackbar } = this.props;

    if (reason === 'clickaway') {
      return;
    }

    this.setState({ open: false });
    popAlertSnackbar();
  }

  Message() {
    const { message, variant, variantIcon } = this.props;

    const Icon = variantIcon[variant];

    return (
      <span className="xcg-message">
        <Icon className={classNames('xcg-icon', 'xcg-icon-variant', `xcg-alert-${variant}`)} />
        {message}
      </span>
    );
  }

  Actions() {
    return [
      <IconButton
        key="close"
        onClick={this.handleClose}
      >
        <Close className="xcg-icon" />
      </IconButton>
    ];
  }

  // Transition(props) {
  //   return <Slide direction="down" {...props} />;
  // }

  Popup() {
    const {
      variant,
      anchorOrigin,
      autoHideDuration,
      isHide
    } = this.props;
    const { open } = this.state;

    return (
      <Snackbar
        anchorOrigin={anchorOrigin}
        open={open}
        autoHideDuration={isHide ? autoHideDuration : null}
        onClose={this.handleClose}
      // TransitionComponent={this.Transition}
      >
        <SnackbarContent
          className={`xcg-alert-${variant}`}
          message={this.Message()}
          action={this.Actions()}
        />
      </Snackbar>
    );
  }

  Static() {
    const { variant, isAction } = this.props;
    const { open } = this.state;

    return (
      open
        ? <SnackbarContent
          className={`xcg-alert-${variant}`}
          message={this.Message()}
          action={isAction ? this.Actions() : null}
        />
        : null
    );
  }

  initElement() {
    const { className, type } = this.props;

    let Alert = null;
    switch (type) {
      case constant.POPUP:
        Alert = this.Popup();
        break;
      case constant.STATIC:
        Alert = this.Static();
        break;
      default:
        break;
    }

    return (
      <div className={classNames('xcg-alert-snackbar', className)}>
        {Alert}
      </div>
    );
  }

  render() {
    return this.initElement();
  }
};

AlertSnackbar.propTypes = {
  className: propTypes.string,
  type: propTypes.oneOf([constant.STATIC, constant.POPUP]),
  variant: propTypes.oneOf([
    constant.SUCCESS,
    constant.WARNING,
    constant.WAIT,
    constant.ERROR,
    constant.FAILED,
    constant.INFO,
  ]),
  anchorOrigin: propTypes.object,
  variantIcon: propTypes.object,
  autoHideDuration: propTypes.number,
  isHide: propTypes.bool,
  isAction: propTypes.bool,
  message: propTypes.node.isRequired,
  popAlertSnackbar: propTypes.func.isRequired,
};

AlertSnackbar.defaultProps = {
  className: '',
  type: constant.STATIC,
  variant: constant.INFO,
  anchorOrigin: {
    vertical: 'top',
    horizontal: 'center',
  },
  variantIcon: {
    success: CheckCircle,
    warning: Warning,
    wait: Warning,
    error: Error,
    failed: Error,
    info: Info,
  },
  autoHideDuration: 3000,
  isHide: true,
  isAction: false,
};

function mapStateToProps(state, props) {
  return {};
}

function mapDispatchToProps(dispatch, props) {
  return {
    popAlertSnackbar: () => {
      return dispatch(popAlertSnackbar());
    }
  };
}

module.exports = connect(
  mapStateToProps,
  mapDispatchToProps,
)(AlertSnackbar);
