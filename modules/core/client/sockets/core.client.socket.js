const userSocket = require('../../../users/client/sockets/user.client.socket');
const dealHistorySocket = require('../../../business-cards/client/sockets/deal-history.client.socket');
const chatSocket = require('../../../chats/client/sockets/chat.client.socket');
const notificationSocket = require('../../../notifications/client/sockets/notification.client.socket');
const { socket } = require('../lib/socket-io');

const init = (dispatch) => {
  userSocket(dispatch, socket);
  dealHistorySocket(dispatch, socket);
  chatSocket(dispatch, socket);
  notificationSocket(dispatch, socket);
};
module.exports.init = init;
