const coreReducer = require('../reducers/core.client.reducer');
const createSagaMiddleware = require('redux-saga');
const { createBrowserHistory } = require('history');
const { createStore, compose, applyMiddleware } = require('redux');
const { routerMiddleware } = require('connected-react-router/immutable');
const { fromJS } = require('immutable');

/**
 * @override Cấu hình
 *
 * @param   {Object} preloadedState
 * @returns { store: {StoreCreator}, history: {History} }
 */
const configureStore = (preloadedState = {}) => {
  const history = createBrowserHistory();
  const reduxSaga = createSagaMiddleware.default();
  const router = routerMiddleware(history);

  const store = createStore(
    coreReducer(history),
    fromJS(preloadedState),
    compose(
      applyMiddleware(
        reduxSaga,
        router,
      ),
    ),
  );

  store.runSaga = reduxSaga.run;
  store.close = () => {
    return store.dispatch(createSagaMiddleware.END);
  };

  return {
    store,
    history,
  };
};
module.exports.configureStore = configureStore;
