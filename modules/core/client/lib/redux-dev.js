const coreReducer = require('../reducers/core.client.reducer');
const createSagaMiddleware = require('redux-saga');
const { createLogger } = require('redux-logger');
const { createBrowserHistory } = require('history');
const { instrument, persistState } = require('../components/DevTools');
const { createStore, compose, applyMiddleware } = require('redux');
const { routerMiddleware } = require('connected-react-router/immutable');
const { fromJS, isCollection } = require('immutable');

/**
 * @override Cấu hình
 *
 * @param   {Object} preloadedState
 * @returns { store: {StoreCreator}, history: {History} }
 */
const configureStore = (preloadedState = {}) => {
  const history = createBrowserHistory();
  const router = routerMiddleware(history);
  const reduxSaga = createSagaMiddleware.default();

  const stateTransformer = (state) => {
    if (isCollection(state)) {
      return state.toJS();
    }

    return state;
  };

  const store = createStore(
    coreReducer(history),
    fromJS(preloadedState),
    compose(
      applyMiddleware(
        reduxSaga,
        router,
        createLogger({ stateTransformer }),
      ),
      instrument(),
      persistState(window.location.href.match(/[?&]debug_session=([^&#]+)\b/)),
    ),
  );

  store.runSaga = reduxSaga.run;
  store.close = () => {
    return store.dispatch(createSagaMiddleware.END);
  };

  return {
    store,
    history,
  };
};
module.exports.configureStore = configureStore;
