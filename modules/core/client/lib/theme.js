const { createMuiTheme } = require('@material-ui/core/styles');

const enabledThemes = ['default'];
module.exports.enabledThemes = enabledThemes;

const defaultTheme = createMuiTheme({
  palette: {
    common: {
      black: '#000',
      white: '#fff',
      type: 'dark'
    },
    background: {
      paper: '#1a1a1a',
      default: '#333333'
    },
    primary: {
      main: '#ba000d',
      light: '#ff7961',
      dark: '#404040',
      contrastText: '#fff'
    },
    secondary: {
      main: '#3f51b5',
      light: '#7986cb',
      dark: '#303f9f',
      contrastText: '#fff'
    },
    text: {
      primary: '#d9d9d9',
      secondary: '#e6e6e6',
      disabled: '#f2f2f2',
      hint: '#f2f2f2',
      divider: '#f2f2f2'
    },
    action: {
      active: '#404040',
      hover: '#666666',
      selected: '#595959',
      disabled: '#4d4d4d',
      disabledBackground: '#666666',
      hoverOpacity: 0.1
    }
  }
});

module.exports.themeData = {
  default: {
    name: 'default',
    data: defaultTheme
  }
};
