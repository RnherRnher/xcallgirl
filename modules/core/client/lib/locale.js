const areIntlLocalesSupported = require('intl-locales-supported');
const intl = require('intl');
const constant = require('../../../../commons/utils/constant');
const { addLocaleData } = require('react-intl');

const enabledLanguages = [constant.VI];
module.exports.enabledLanguages = enabledLanguages;

const localizationData = {};
module.exports.localizationData = localizationData;

const flattenMessages = (nestedMessages = {}, prefix = '') => {
  return Object.keys(nestedMessages).reduce((messages, key) => {
    const value = nestedMessages[key];
    const prefixedKey = prefix ? `${prefix}.${key}` : key;

    if (typeof value === 'string') {
      messages[prefixedKey] = value;
    } else {
      Object.assign(messages, flattenMessages(value, prefixedKey));
    }

    return messages;
  }, {});
};

if (Intl) {
  if (!areIntlLocalesSupported(enabledLanguages)) {
    Intl.NumberFormat = intl.NumberFormat;
    Intl.DateTimeFormat = intl.DateTimeFormat;
  }
} else {
  Intl = intl;
}

enabledLanguages.forEach((language) => {
  require(`intl/locale-data/jsonp/${language}`);
  addLocaleData(require(`react-intl/locale-data/${language}`));
  localizationData[language] = require(`../locales/${language}.locale.client`);
  (localizationData[language]).messages = flattenMessages((localizationData[language]).messages);
});
