const socketio = require('socket.io-client');

const socket = socketio({ autoConnect: false });
module.exports.socket = socket;

const emitSockets = (sockets) => {
  return new Promise((resovle, reject) => {
    if (sockets) {
      sockets.forEach((value) => {
        socket.emit(...value.names, value.data);
      });
    }

    resovle();
  });
};
module.exports.emitSockets = emitSockets;
