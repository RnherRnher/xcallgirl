const { createSelector } = require('reselect');
const { makeSelect } = require('../utils/middleware');

const selectTheme = (state, props) => {
  return state.get('theme');
};

const makeSelectTheme = () => {
  return createSelector(
    selectTheme,
    (themeState) => {
      return makeSelect(themeState.get('data'));
    }
  );
};
module.exports.makeSelectTheme = makeSelectTheme;
