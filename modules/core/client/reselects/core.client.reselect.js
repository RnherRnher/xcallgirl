const { createSelector } = require('reselect');
const { makeSelect } = require('../utils/middleware');

const selectCore = (state, props) => {
  return state.get('core');
};

const makeSelectAlertSnackbar = () => {
  return createSelector(
    selectCore,
    (coreState) => {
      return makeSelect(coreState.get('alertSnackbar'));
    }
  );
};
module.exports.makeSelectAlertSnackbar = makeSelectAlertSnackbar;
