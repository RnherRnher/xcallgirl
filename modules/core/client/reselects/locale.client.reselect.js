const { createSelector } = require('reselect');
const { makeSelect } = require('../utils/middleware');

const selectLocale = (state, props) => {
  return state.get('locale');
};

const makeSelectLocale = () => {
  return createSelector(
    selectLocale,
    (localeState) => {
      return makeSelect(localeState);
    }
  );
};
module.exports.makeSelectLocale = makeSelectLocale;
