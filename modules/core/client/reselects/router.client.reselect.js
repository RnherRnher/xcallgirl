const { createSelector } = require('reselect');
const { makeSelect } = require('../utils/middleware');

const selectRouter = (state, props) => {
  return state.get('router');
};

const makeSelectLocation = () => {
  return createSelector(
    selectRouter,
    (routerState) => {
      return makeSelect(routerState.get('location'));
    }
  );
};
module.exports.makeSelectLocation = makeSelectLocation;
