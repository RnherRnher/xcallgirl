const constant = require('../constants/theme.client.constant');
const { assignIn } = require('lodash');
const { fromJS } = require('immutable');
const { enabledThemes, themeData } = require('../lib/theme');

const initialState = fromJS(
  assignIn(
    { enabledThemes },
    themeData[enabledThemes[0]]
  )
);

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case constant.CHANGE_THEME: {
      const currentTheme = themeData[action.theme];

      return state
        .set('name', currentTheme.name)
        .set('data', currentTheme.data);
    }
    case constant.RESERT_THEME_STORE: {
      return initialState;
    }
    default:
      return state;
  }
};
module.exports = reducer;
