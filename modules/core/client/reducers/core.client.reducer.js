const locale = require('./locale.client.reducer');
const theme = require('./theme.client.reducer');
const system = require('../../../system/client/reducers/system.client.reducer');
const authentication = require('../../../users/client/reducers/authentication.client.reducer');
const user = require('../../../users/client/reducers/user.client.reducer');
const admin = require('../../../users/client/reducers/admin.client.reducer');
const businessCard = require('../../../business-cards/client/reducers/business-card.client.reducer');
const businessCardDealHistory = require('../../../business-cards/client/reducers/deal-history.client.reducer');
const notification = require('../../../notifications/client/reducers/notification.client.reducer');
const activityHistory = require('../../../activity-historys/client/reducers/activity-history.client.reducer');
const chat = require('../../../chats/client/reducers/chat.client.reducer');
const credit = require('../../../credits/client/reducers/credit.client.reducer');
const store = require('../../../store/client/reducers/store.client.reducer');
const constant = require('../constants/core.client.constant');
const { combineReducers } = require('redux-immutable');
const { connectRouter } = require('connected-react-router/immutable');
const { fromJS } = require('immutable');

const initialState = fromJS({
  alertSnackbar: null
});

const core = (state = initialState, action) => {
  switch (action.type) {
    case constant.PUSH_ALERT_SNACKBAR: {
      return state.set(
        'alertSnackbar',
        {
          variant: action.variant,
          code: action.code
        }
      );
    }
    case constant.POP_ALERT_SNACKBAR: {
      return state.set('alertSnackbar', null);
    }
    case constant.RESERT_CORE_STORE: {
      return initialState;
    }
    default:
      return state;
  }
};

module.exports = (history) => {
  return connectRouter(history)(combineReducers({
    core,
    locale,
    theme,
    system,
    authentication,
    user,
    admin,
    businessCard,
    businessCardDealHistory,
    notification,
    activityHistory,
    chat,
    credit,
    store
  }));
};
