const constant = require('../constants/locale.client.constant');
const { assignIn } = require('lodash');
const { fromJS } = require('immutable');
const { localizationData, enabledLanguages } = require('../lib/locale');

const initialState = fromJS(
  assignIn(
    { enabledLanguages },
    localizationData[enabledLanguages[0]]
  )
);

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case constant.CHANGE_LANGUAGE: {
      const currentLanguage = localizationData[action.language];

      return state
        .set('locale', currentLanguage.locale)
        .set('messages', currentLanguage.messages);
    }
    case constant.RESERT_LOCALE_STORE: {
      return initialState;
    }
    default:
      return state;
  }
};
module.exports = reducer;
