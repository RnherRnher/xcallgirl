const coreSaga = require('../sagas/core.client.saga');
const App = require('../components/App');
const socket = require('../sockets/core.client.socket');

const isDev = window.__isDev__;

if (isDev) {
  const coreReducer = require('../reducers/core.client.reducer');
  const { hot } = require('react-hot-loader');
  const { configureStore } = require('../lib/redux-dev');
  const { store, history } = configureStore();

  socket.init(store.dispatch);
  store.runSaga(coreSaga);

  hot(module)(App(store, history, isDev));
  if (module.hot) {
    module.hot.accept('../components/App', () => {
      App(store, history, isDev);
    });

    module.hot.accept('../reducers/core.client.reducer', () => {
      store.replaceReducer(coreReducer(history));
    });
  }
} else {
  const { configureStore } = require('../lib/redux-prod');
  const { store, history } = configureStore();

  socket.init(store.dispatch);
  store.runSaga(coreSaga);

  App(store, history, isDev);
}
