
const path = require('path');
const core = require('../controllers/core.server.controller');
const url = require(path.resolve('./commons/modules/core/datas/url.data'));

/**
 * @overview Khới tạo tuyến đường core
 *
 * @param {Express} app
 */
const init = (app) => {
  app.route(url.ROOT)
    .get(core.renderIndex);

  app.route(url.CSRF)
    .get(core.getCSRF);
};
module.exports = init;
