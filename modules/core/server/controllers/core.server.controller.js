/**
 * @overview Xuất trang xuống máy khách
 *
 * @param {Request} req
 * @param {Response} res
 * @param {Function} next
 */
const renderIndex = (req, res, next) => {
  const userSelf = req.user;

  let { language } = req.session;

  if (userSelf) {
    ({ language } = userSelf.settings.general);
  } else if (!language) {
    ({ language } = req.app.locals);
  }

  return res
    .status(200)
    .render('modules/core/server/views/index', {
      language,
      title: req.app.locals.title,
      description: req.app.locals.description[language],
      isDev: req.app.locals.env === 'development',
    });
};
module.exports.renderIndex = renderIndex;

/**
 * @overview Lấy mã csrf
 *
 * @param {Request} req
 * @param {Response} res
 * @param {Function} next
 */
const getCSRF = (req, res, next) => {
  const csrfToken = req.csrfToken();
  let dataRes = null;

  if (!csrfToken) {
    // Tạo mã CSRF thất bại
    dataRes = {
      status: 400,
      bodyRes: {
        code: 'data.csrf.failed',
      },
    };
  } else {
    dataRes = {
      status: 200,
      bodyRes: {
        code: 'data.csrf.success',
        csrfToken,
      },
    };
  }

  return res
    .status(dataRes.status)
    .send(dataRes.bodyRes)
    .end();
};
module.exports.getCSRF = getCSRF;
