/**
 * @overview Khới tạo cấu hình xử lý yêu cầu
 *
 * @param {Express} app
 */
const initConfigHandleRequest = (app) => {
  // //TODO: Chỉ được phép truy cập bằng điện thoại
  // if (process.env.NODE_ENV !== 'test') {
  //   app.use((req, res, next) => {
  //     if (!req.useragent.isMobile) {
  //       return res
  //         .status(401)
  //         .send({
  //           code: 'error.client.support.notExist',
  //         })
  //         .end();
  //     }

  //     return next();
  //   });
  // }

  app.use((req, res, next) => {
    if (!req.xhr && req.path !== '/') {
      // Không được phép tự điều hướng URL
      return res.redirect('/');
    }

    return next();
  });
};


/**
 * @overview Khới tạo cấu hình core
 *
 * @param {Express} app
 */
const init = (app) => {
  initConfigHandleRequest(app);
};
module.exports = init;
