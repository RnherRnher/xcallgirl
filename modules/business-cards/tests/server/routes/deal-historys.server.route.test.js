const path = require('path');
const mongoose = require('mongoose');
const request = require('supertest');
const chai = require('chai');
const _ = require('lodash');
const cloudinary = require(path.resolve('config/lib/cloudinary'));
const express = require(path.resolve('./config/lib/express'));
const url = require(path.resolve('./commons/modules/business-cards/datas/url.data'));
const usersUrl = require(path.resolve('./commons/modules/users/datas/url.data'));
const constant = require(path.resolve('./commons/utils/constant'));
const regular = require(path.resolve('./commons/utils/regular-expression'));
const utils = require(path.resolve('./utils/test'));

describe('### Deal History Server Routes Tests', () => {
  let app = null;
  let agent = null;

  let BusinessCard = null;
  let User = null;
  let ActivityHistory = null;
  let BusinessCardDealHistory = null;
  let Notification = null;

  let businessCardID = null;
  let businessCardDealHistoryID = null;

  let verifyUserAccount = null;
  let user = null;
  let someUser = null;
  let adminUser = null;

  const password = 'password';

  let token = null;

  /**
   * @overview Kiểm tra chấp nhận giao dịch
   *
   * @param {ObjectId} businessCardID
   * @param {Boolean} isAccept
   * @param {Function} done
   * @param {Boolean} isUser
   * @param {Boolean} isExist
   */
  const testAccept = (businessCardID, isAccept, done, isUser = true, isExist = true) => {
    BusinessCardDealHistory
      .findOne({
        businessCardID,
      })
      .exec((err, dealHistory) => {
        chai.assert.notExists(err);
        chai.assert.exists(dealHistory);

        if (isExist) {
          chai.assert.isTrue(isAccept === (isUser ? dealHistory.accept.user.is : dealHistory.accept.provider.is));
        } else if (!isExist) {
          if (isUser) {
            chai.assert.isTrue(dealHistory.accept.user.is);
          } else {
            chai.assert.notExists(dealHistory.accept.provider);
          }
        }

        ActivityHistory
          .findOne({
            type: constant.BUSINESS_CARD_DEAL_HISTORY,
            result: constant.SUCCESS,
            content: `businessCardDealHistory.accept.${isAccept}`,
          })
          .exec((err, activityHistory) => {
            chai.assert.notExists(err);

            if (isExist) {
              chai.assert.exists(activityHistory);
              chai.assert.isTrue(activityHistory.userID.equals(isUser ? dealHistory.userID : dealHistory.providerID));
            } else {
              chai.assert.notExists(activityHistory);
            }

            Notification
              .findOne({
                type: constant.BUSINESS_CARD_DEAL_HISTORY,
                result: constant.INFO,
                content: `businessCardDealHistory.accept.${isAccept}`,
              })
              .exec((err, notification) => {
                chai.assert.notExists(err);

                if (isExist) {
                  chai.assert.exists(notification);
                  chai.assert.isTrue(notification.receiveID.equals(isUser ? dealHistory.providerID : dealHistory.userID));
                  chai.assert.isTrue(notification.sendID.equals(isUser ? dealHistory.userID : dealHistory.providerID));
                } else {
                  chai.assert.notExists(notification);
                }

                if (done) done();
              });
          });
      });
  };

  /**
   * @overview Kiểm tra lấy mã giao dịch
   *
   * @param {ObjectId} businessCardDealHistoryID
   * @param {ObjectId} userID
   * @param {Function} done
   * @param {Boolean} isExist
   */
  const testGetToken = (businessCardDealHistoryID, userID, done, isExist = true) => {
    BusinessCardDealHistory
      .findById(businessCardDealHistoryID)
      .exec((err, dealHistory) => {
        chai.assert.notExists(err);
        chai.assert.exists(dealHistory);

        if (isExist) {
          chai.assert.deepEqual(dealHistory.status, constant.VERIFY);
          chai.assert.exists(dealHistory.token);
        } else {
          chai.assert.deepEqual(dealHistory.status, constant.TOKEN);
          chai.assert.notExists(dealHistory.token);
        }

        ActivityHistory
          .findOne({
            userID,
            type: constant.BUSINESS_CARD_DEAL_HISTORY,
            result: constant.SUCCESS,
            content: 'businessCardDealHistory.token.create.is',
          })
          .exec((err, activityHistory) => {
            chai.assert.notExists(err);

            if (isExist) {
              chai.assert.exists(activityHistory);
            } else {
              chai.assert.notExists(activityHistory);
            }

            if (done) done();
          });
      });
  };

  /**
   * @overview Kiểm tra tố cáo giao dịch
   *
   * @param {ObjectId} businessCardDealHistoryID
   * @param {ObjectId} userID
   * @param {Function} done
   * @param {Boolean} isExist
   */
  const testReport = (businessCardDealHistoryID, userID, done, isExist = true) => {
    BusinessCardDealHistory
      .findOne({
        $or: [{
          _id: businessCardDealHistoryID,
          userID,
          'reports.user.is': true,
          'result.is': { $in: [constant.FAILED, constant.SUCCESS] },
        }, {
          _id: businessCardDealHistoryID,
          providerID: userID,
          'reports.provider.is': true,
          'result.is': { $in: [constant.FAILED, constant.SUCCESS] },
        }],
      })
      .exec((err, dealHistory) => {
        chai.assert.notExists(err);

        if (isExist) {
          chai.assert.exists(dealHistory);
        } else {
          chai.assert.notExists(dealHistory);
        }

        ActivityHistory
          .findOne({
            userID,
            type: constant.BUSINESS_CARD_DEAL_HISTORY,
            result: constant.SUCCESS,
            content: 'businessCardDealHistory.report.is',
          })
          .exec((err, activityHistory) => {
            chai.assert.notExists(err);

            if (isExist) {
              chai.assert.exists(activityHistory);
            } else {
              chai.assert.notExists(activityHistory);
            }

            if (done) done();
          });
      });
  };

  before((done) => {
    app = express.init(mongoose.connection.db);
    agent = request.agent(app);

    User = mongoose.model('User');
    BusinessCard = mongoose.model('BusinessCard');
    BusinessCardDealHistory = mongoose.model('BusinessCardDealHistory');
    ActivityHistory = mongoose.model('ActivityHistory');
    Notification = mongoose.model('Notification');

    done();
  });

  after((done) => {
    cloudinary.deleteResources('test')
      .catch((err) => {
        chai.assert.notExists(err);
      });

    done();
  });

  beforeEach((done) => {
    user = {
      name: {
        first: 'first',
        last: 'last',
        display: 'last first',
        nick: 'user_user',
      },
      numberPhone: {
        code: '+84',
        number: '1111111111',
      },
      gender: constant.MALE,
      birthday: {
        day: 1,
        month: 1,
        year: 2000,
      },
      password,
      address: {
        country: 'country',
        city: 'city',
        county: 'county',
        local: 'local',
      },
      settings: {
        general: {
          language: constant.VI,
        },
      },
    };

    adminUser = _.cloneDeep(user);
    adminUser.numberPhone.number = '9999999999';
    adminUser.name.nick = 'user_admin';
    adminUser.verify = {
      account: {
        is: true,
      },
    };
    adminUser.roles = {
      value: constant.ADMIN,
    };

    verifyUserAccount = _.cloneDeep(user);
    verifyUserAccount.numberPhone.number = '0000000000';
    verifyUserAccount.name.nick = 'user_verify';
    verifyUserAccount.verify = {
      account: {
        is: true,
      },
    };

    someUser = _.cloneDeep(user);
    someUser.numberPhone.number = '2222222222';
    someUser.name.nick = 'some_user';

    user = new User(user);
    verifyUserAccount = new User(verifyUserAccount);
    adminUser = new User(adminUser);
    someUser = new User(someUser);

    verifyUserAccount.save((err) => {
      chai.assert.notExists(err);

      user.save((err) => {
        chai.assert.notExists(err);

        adminUser.save((err) => {
          chai.assert.notExists(err);

          someUser.save((err) => {
            chai.assert.notExists(err);

            utils.signin(agent, verifyUserAccount.numberPhone, password)
              .expect(200)
              .end((err, res) => {
                if (err) {
                  return done(err);
                }

                agent.post(url.BUSINESS_CARDS)
                  .set('X-Requested-With', 'XMLHttpRequest')
                  .send({
                    title: 'title',
                    descriptions: 'descriptions',
                    blurFace: true,
                    costs: {
                      rooms: [{
                        value: constant.ONE_TIME,
                        moneys: 50,
                      }],
                      services: [{
                        value: constant.ONE_SLOT,
                        moneys: 150,
                      }],
                      currencyUnit: constant.DOMESTIC,
                    },
                    measurements3: {
                      breast: 90,
                      waist: 45,
                      hips: 90,
                    },
                    address: {
                      country: 'country',
                      city: 'city',
                      county: 'county',
                      local: 'local',
                    },
                  })
                  .expect(200)
                  .end((err, res) => {
                    if (err) {
                      return done(err);
                    }

                    chai.assert.exists(res.body.businessCard);
                    businessCardID = res.body.businessCard._id;

                    agent.get(usersUrl.AUTH_SIGNOUT)
                      .expect(200)
                      .set('X-Requested-With', 'XMLHttpRequest')
                      .end((err, res) => {
                        if (err) {
                          return done(err);
                        }

                        utils.signin(agent, user.numberPhone, password)
                          .expect(200)
                          .end((err, res) => {
                            if (err) {
                              return done(err);
                            }

                            agent.post(url.BUSINESS_CARDS_DEAL_ID.replace(':id', businessCardID))
                              .expect(200)
                              .set('X-Requested-With', 'XMLHttpRequest')
                              .send({
                                costs: {
                                  room: {
                                    value: constant.ONE_TIME,
                                    moneys: 50,
                                  },
                                  service: {
                                    value: constant.ONE_SLOT,
                                    moneys: 150,
                                  },
                                  currencyUnit: constant.DOMESTIC,
                                },
                                note: 'note',
                                timer: regular.businessCardDealHistory.expires.timer + constant.ENUM.TIME.ONE_MINUTES
                              })
                              .end((err, res) => {
                                if (err) {
                                  return done(err);
                                }

                                chai.assert.exists(res.body.businessCardDealHistoryID);
                                const id = res.body.businessCardDealHistoryID;
                                businessCardDealHistoryID = id;

                                return done();
                              });
                          });
                      });
                  });
              });
          });
        });
      });
    });
  });

  afterEach((done) => {
    User.deleteMany({}, (err) => {
      chai.assert.notExists(err);
      BusinessCard.deleteMany({}, (err) => {
        chai.assert.notExists(err);
        ActivityHistory.deleteMany({}, (err) => {
          chai.assert.notExists(err);
          Notification.deleteMany({}, (err) => {
            chai.assert.notExists(err);
            BusinessCardDealHistory.deleteMany({}, (err) => {
              chai.assert.notExists(err);
              done();
            });
          });
        });
      });
    });
  });

  describe('## Lấy thông tin lịch sử giao dịch danh thiếp theo ID', () => {
    it('should success get deal historys business card with ID', (done) => {
      agent.get(url.BUSINESS_CARDS_DEAL_HISTORYS_GET_ID.replace(':id', businessCardDealHistoryID))
        .expect(200)
        .set('X-Requested-With', 'XMLHttpRequest')
        .query({ type: constant.FULL })
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          chai.assert.exists(res.body.businessCardDealHistory);
          chai.assert.exists(res.body.provider);

          return done();
        });
    });

    it('should success get deal historys business card with roles admin', (done) => {
      agent.get(usersUrl.AUTH_SIGNOUT)
        .expect(200)
        .set('X-Requested-With', 'XMLHttpRequest')
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          utils.signin(agent, adminUser.numberPhone, password)
            .expect(200)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              agent.get(url.BUSINESS_CARDS_DEAL_HISTORYS_GET_ID.replace(':id', businessCardDealHistoryID))
                .expect(200)
                .set('X-Requested-With', 'XMLHttpRequest')
                .query({ type: constant.FULL })
                .end((err, res) => {
                  if (err) {
                    return done(err);
                  }

                  chai.assert.exists(res.body.businessCardDealHistory);
                  chai.assert.exists(res.body.provider);
                  chai.assert.exists(res.body.user);

                  return done();
                });
            });
        });
    });

    it('should failed get deal historys business card with bad id', (done) => {
      agent.get(url.BUSINESS_CARDS_DEAL_HISTORYS_GET_ID.replace(':id', mongoose.Types.ObjectId()))
        .expect(404)
        .set('X-Requested-With', 'XMLHttpRequest')
        .query({ type: constant.FULL })
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          chai.assert.deepEqual(res.body.code, 'businessCardDealHistory.notExist');
          return done();
        });
    });

    it('should failed get deal historys business card with not signin', (done) => {
      agent.get(usersUrl.AUTH_SIGNOUT)
        .expect(200)
        .set('X-Requested-With', 'XMLHttpRequest')
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.get(url.BUSINESS_CARDS_DEAL_HISTORYS_GET_ID.replace(':id', businessCardDealHistoryID))
            .expect(401)
            .set('X-Requested-With', 'XMLHttpRequest')
            .query({ type: constant.FULL })
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.code, 'user.permission.notExist');
              return done();
            });
        });
    });

    it('should failed get deal historys business card with not permission', (done) => {
      agent.get(usersUrl.AUTH_SIGNOUT)
        .expect(200)
        .set('X-Requested-With', 'XMLHttpRequest')
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          utils.signin(agent, someUser.numberPhone, password)
            .expect(200)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              agent.get(url.BUSINESS_CARDS_DEAL_HISTORYS_GET_ID.replace(':id', businessCardDealHistoryID))
                .expect(401)
                .set('X-Requested-With', 'XMLHttpRequest')
                .query({ type: constant.FULL })
                .end((err, res) => {
                  if (err) {
                    return done(err);
                  }

                  chai.assert.deepEqual(res.body.code, 'user.permission.notExist');
                  return done();
                });
            });
        });
    });
  });

  describe('## Chấp nhận lịch sử giao dịch danh thiếp', () => {
    it('should success accept deal historys business card with user accept is false', (done) => {
      agent.post(url.BUSINESS_CARDS_DEAL_HISTORYS_ACCEPT_ID.replace(':id', businessCardDealHistoryID))
        .expect(200)
        .set('X-Requested-With', 'XMLHttpRequest')
        .send({
          accept: {
            is: false,
            reason: 'reason',
          },
        })
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          chai.assert.exists(res.body.businessCardDealHistory);
          chai.assert.deepEqual(res.body.businessCardDealHistory.result.is, constant.FAILED);

          testAccept(businessCardID, false, done);
        });
    });

    it('should failed accept deal historys business card with expires', (done) => {
      BusinessCardDealHistory.findById(businessCardDealHistoryID, (err, businessCardDealHistory) => {
        if (err) {
          return done(err);
        }

        businessCardDealHistory.timer = Date.now();
        businessCardDealHistory.save((err) => {
          if (err) {
            return done(err);
          }

          agent.post(url.BUSINESS_CARDS_DEAL_HISTORYS_ACCEPT_ID.replace(':id', businessCardDealHistoryID))
            .expect(401)
            .set('X-Requested-With', 'XMLHttpRequest')
            .send({
              accept: {
                is: false,
                reason: 'reason',
              },
            })
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.exists(res.body.businessCardDealHistory);
              chai.assert.deepEqual(res.body.code, 'businessCardDealHistory.accept.failed');

              testAccept(businessCardID, true, done, true, false);
            });
        });
      });
    });

    it('should failed accept deal historys business card with user has accept deal historys business card', (done) => {
      agent.post(url.BUSINESS_CARDS_DEAL_HISTORYS_ACCEPT_ID.replace(':id', businessCardDealHistoryID))
        .expect(200)
        .set('X-Requested-With', 'XMLHttpRequest')
        .send({
          accept: {
            is: false,
            reason: 'reason',
          },
        })
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          chai.assert.exists(res.body.businessCardDealHistory);
          chai.assert.deepEqual(res.body.businessCardDealHistory.result.is, constant.FAILED);

          agent.post(url.BUSINESS_CARDS_DEAL_HISTORYS_ACCEPT_ID.replace(':id', businessCardDealHistoryID))
            .expect(404)
            .set('X-Requested-With', 'XMLHttpRequest')
            .send({
              accept: {
                is: true,
              },
            })
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.code, 'businessCardDealHistory.notExist');
              testAccept(businessCardID, false, done, true);
            });
        });
    });

    it('should failed accept deal historys business card with not signin', (done) => {
      agent.get(usersUrl.AUTH_SIGNOUT)
        .expect(200)
        .set('X-Requested-With', 'XMLHttpRequest')
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.post(url.BUSINESS_CARDS_DEAL_HISTORYS_ACCEPT_ID.replace(':id', businessCardDealHistoryID))
            .expect(401)
            .set('X-Requested-With', 'XMLHttpRequest')
            .send({
              accept: {
                is: false,
                reason: 'reason',
              },
            })
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.code, 'user.permission.notExist');
              testAccept(businessCardID, true, done, true, false);
            });
        });
    });

    it('should failed accept deal historys business card with bad id', (done) => {
      agent.post(url.BUSINESS_CARDS_DEAL_HISTORYS_ACCEPT_ID.replace(':id', mongoose.Types.ObjectId()))
        .expect(404)
        .set('X-Requested-With', 'XMLHttpRequest')
        .send({
          accept: {
            is: false,
            reason: 'reason',
          },
        })
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          chai.assert.deepEqual(res.body.code, 'businessCardDealHistory.notExist');
          testAccept(businessCardID, true, done, true, false);
        });
    });

    it('should success accept deal historys business card with provider accept is true', (done) => {
      agent.get(usersUrl.AUTH_SIGNOUT)
        .expect(200)
        .set('X-Requested-With', 'XMLHttpRequest')
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          utils.signin(agent, verifyUserAccount.numberPhone, password)
            .expect(200)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              agent.post(url.BUSINESS_CARDS_DEAL_HISTORYS_ACCEPT_ID.replace(':id', businessCardDealHistoryID))
                .expect(200)
                .set('X-Requested-With', 'XMLHttpRequest')
                .send({
                  accept: {
                    is: true,
                  },
                })
                .end((err, res) => {
                  if (err) {
                    return done(err);
                  }

                  chai.assert.exists(res.body.businessCardDealHistory);
                  chai.assert.deepEqual(res.body.businessCardDealHistory.status, constant.TOKEN);

                  testAccept(businessCardID, true, done, false);
                });
            });
        });
    });

    it('should success accept deal historys business card with provider accept is false', (done) => {
      agent.get(usersUrl.AUTH_SIGNOUT)
        .expect(200)
        .set('X-Requested-With', 'XMLHttpRequest')
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          utils.signin(agent, verifyUserAccount.numberPhone, password)
            .expect(200)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              agent.post(url.BUSINESS_CARDS_DEAL_HISTORYS_ACCEPT_ID.replace(':id', businessCardDealHistoryID))
                .expect(200)
                .set('X-Requested-With', 'XMLHttpRequest')
                .send({
                  accept: {
                    is: true,
                  },
                })
                .end((err, res) => {
                  if (err) {
                    return done(err);
                  }

                  chai.assert.exists(res.body.businessCardDealHistory);
                  chai.assert.deepEqual(res.body.businessCardDealHistory.status, constant.TOKEN);

                  agent.post(url.BUSINESS_CARDS_DEAL_HISTORYS_ACCEPT_ID.replace(':id', businessCardDealHistoryID))
                    .expect(200)
                    .set('X-Requested-With', 'XMLHttpRequest')
                    .send({
                      accept: {
                        is: false,
                        reason: 'reason',
                      },
                    })
                    .end((err, res) => {
                      if (err) {
                        return done(err);
                      }

                      chai.assert.exists(res.body.businessCardDealHistory);
                      chai.assert.deepEqual(res.body.businessCardDealHistory.result.is, constant.FAILED);

                      testAccept(businessCardID, false, done, false);
                    });
                });
            });
        });
    });

    it('should failed accept deal historys business card with provider has accept deal historys business card', (done) => {
      agent.get(usersUrl.AUTH_SIGNOUT)
        .expect(200)
        .set('X-Requested-With', 'XMLHttpRequest')
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          utils.signin(agent, verifyUserAccount.numberPhone, password)
            .expect(200)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              agent.post(url.BUSINESS_CARDS_DEAL_HISTORYS_ACCEPT_ID.replace(':id', businessCardDealHistoryID))
                .expect(200)
                .set('X-Requested-With', 'XMLHttpRequest')
                .send({
                  accept: {
                    is: false,
                    reason: 'reason',
                  },
                })
                .end((err, res) => {
                  if (err) {
                    return done(err);
                  }

                  chai.assert.exists(res.body.businessCardDealHistory);
                  chai.assert.deepEqual(res.body.businessCardDealHistory.result.is, constant.FAILED);

                  agent.post(url.BUSINESS_CARDS_DEAL_HISTORYS_ACCEPT_ID.replace(':id', businessCardDealHistoryID))
                    .expect(404)
                    .set('X-Requested-With', 'XMLHttpRequest')
                    .send({
                      accept: {
                        is: false,
                        reason: 'reason_2',
                      },
                    })
                    .end((err, res) => {
                      if (err) {
                        return done(err);
                      }

                      chai.assert.deepEqual(res.body.code, 'businessCardDealHistory.notExist');
                      testAccept(businessCardID, false, done, false);
                    });
                });
            });
        });
    });
  });

  describe('## Lấy mã lịch sử giao dịch danh thiếp', () => {
    beforeEach((done) => {
      agent.get(usersUrl.AUTH_SIGNOUT)
        .expect(200)
        .set('X-Requested-With', 'XMLHttpRequest')
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          utils.signin(agent, verifyUserAccount.numberPhone, password)
            .expect(200)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              agent.post(url.BUSINESS_CARDS_DEAL_HISTORYS_ACCEPT_ID.replace(':id', businessCardDealHistoryID))
                .expect(200)
                .set('X-Requested-With', 'XMLHttpRequest')
                .send({
                  accept: {
                    is: true,
                  },
                })
                .end((err, res) => {
                  if (err) {
                    return done(err);
                  }

                  chai.assert.exists(res.body.businessCardDealHistory);
                  chai.assert.deepEqual(res.body.businessCardDealHistory.status, constant.TOKEN);

                  agent.get(usersUrl.AUTH_SIGNOUT)
                    .expect(200)
                    .set('X-Requested-With', 'XMLHttpRequest')
                    .end((err, res) => {
                      if (err) {
                        return done(err);
                      }

                      utils.signin(agent, user.numberPhone, password)
                        .expect(200)
                        .end((err, res) => {
                          if (err) {
                            return done(err);
                          }

                          return done();
                        });
                    });
                });
            });
        });
    });

    it('should success get token deal historys business card', (done) => {
      agent.get(url.BUSINESS_CARDS_DEAL_HISTORYS_TOKEN_ID.replace(':id', businessCardDealHistoryID))
        .expect(200)
        .set('X-Requested-With', 'XMLHttpRequest')
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          chai.assert.exists(res.body.businessCardDealHistory);
          chai.assert.exists(res.body.businessCardDealHistory.token);

          testGetToken(businessCardDealHistoryID, user._id, done);
        });
    });

    it('should failed get token deal historys business card with expires', (done) => {
      BusinessCardDealHistory.findById(businessCardDealHistoryID, (err, businessCardDealHistory) => {
        if (err) {
          return done(err);
        }

        businessCardDealHistory.timer = Date.now();
        businessCardDealHistory.save((err) => {
          if (err) {
            return done(err);
          }

          agent.get(url.BUSINESS_CARDS_DEAL_HISTORYS_TOKEN_ID.replace(':id', businessCardDealHistoryID))
            .expect(401)
            .set('X-Requested-With', 'XMLHttpRequest')
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.exists(res.body.businessCardDealHistory);
              chai.assert.deepEqual(res.body.businessCardDealHistory.result.is, constant.FAILED);

              testGetToken(businessCardDealHistoryID, user._id, done, false);
            });
        });
      });
    });

    it('should failed get token deal historys business card with has get token deal historys business card', (done) => {
      agent.get(url.BUSINESS_CARDS_DEAL_HISTORYS_TOKEN_ID.replace(':id', businessCardDealHistoryID))
        .expect(200)
        .set('X-Requested-With', 'XMLHttpRequest')
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          chai.assert.exists(res.body.businessCardDealHistory);
          chai.assert.exists(res.body.businessCardDealHistory.token);

          agent.get(url.BUSINESS_CARDS_DEAL_HISTORYS_TOKEN_ID.replace(':id', businessCardDealHistoryID))
            .expect(404)
            .set('X-Requested-With', 'XMLHttpRequest')
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.code, 'businessCardDealHistory.notExist');
              testGetToken(businessCardDealHistoryID, user._id, done);
            });
        });
    });

    it('should failed get token deal historys business card with not signin', (done) => {
      agent.get(usersUrl.AUTH_SIGNOUT)
        .expect(200)
        .set('X-Requested-With', 'XMLHttpRequest')
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.get(url.BUSINESS_CARDS_DEAL_HISTORYS_TOKEN_ID.replace(':id', businessCardDealHistoryID))
            .expect(401)
            .set('X-Requested-With', 'XMLHttpRequest')
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.code, 'user.permission.notExist');
              testGetToken(businessCardDealHistoryID, user._id, done, false);
            });
        });
    });
  });

  describe('## Xác thực mã lịch sử giao dịch danh thiếp', () => {
    beforeEach((done) => {
      agent.get(usersUrl.AUTH_SIGNOUT)
        .expect(200)
        .set('X-Requested-With', 'XMLHttpRequest')
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          utils.signin(agent, verifyUserAccount.numberPhone, password)
            .expect(200)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              agent.post(url.BUSINESS_CARDS_DEAL_HISTORYS_ACCEPT_ID.replace(':id', businessCardDealHistoryID))
                .expect(200)
                .set('X-Requested-With', 'XMLHttpRequest')
                .send({
                  accept: {
                    is: true,
                  },
                })
                .end((err, res) => {
                  if (err) {
                    return done(err);
                  }

                  chai.assert.exists(res.body.businessCardDealHistory);
                  chai.assert.deepEqual(res.body.businessCardDealHistory.status, constant.TOKEN);

                  agent.get(usersUrl.AUTH_SIGNOUT)
                    .expect(200)
                    .set('X-Requested-With', 'XMLHttpRequest')
                    .end((err, res) => {
                      if (err) {
                        return done(err);
                      }

                      utils.signin(agent, user.numberPhone, password)
                        .expect(200)
                        .end((err, res) => {
                          if (err) {
                            return done(err);
                          }

                          agent.get(url.BUSINESS_CARDS_DEAL_HISTORYS_TOKEN_ID.replace(':id', businessCardDealHistoryID))
                            .expect(200)
                            .set('X-Requested-With', 'XMLHttpRequest')
                            .end((err, res) => {
                              if (err) {
                                return done(err);
                              }

                              chai.assert.exists(res.body.businessCardDealHistory);
                              chai.assert.exists(res.body.businessCardDealHistory.token);
                              ({ token } = res.body.businessCardDealHistory);

                              BusinessCardDealHistory.findById(businessCardDealHistoryID, (err, businessCardDealHistory) => {
                                if (err) {
                                  return done(err);
                                }

                                businessCardDealHistory.timer = Date.now() + regular.businessCardDealHistory.expires.token;
                                businessCardDealHistory.save((err) => {
                                  if (err) {
                                    return done(err);
                                  }

                                  agent.get(usersUrl.AUTH_SIGNOUT)
                                    .expect(200)
                                    .set('X-Requested-With', 'XMLHttpRequest')
                                    .end((err, res) => {
                                      if (err) {
                                        return done(err);
                                      }

                                      utils.signin(agent, verifyUserAccount.numberPhone, password)
                                        .expect(200)
                                        .end((err, res) => {
                                          if (err) {
                                            return done(err);
                                          }

                                          return done();
                                        });
                                    });
                                });
                              });
                            });
                        });
                    });
                });
            });
        });
    });

    it('should success verify token deal historys business card', (done) => {
      agent.post(url.BUSINESS_CARDS_DEAL_HISTORYS_TOKEN_ID.replace(':id', businessCardDealHistoryID))
        .expect(200)
        .set('X-Requested-With', 'XMLHttpRequest')
        .send({
          token,
        })
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          chai.assert.exists(res.body.businessCardDealHistory);
          chai.assert.deepEqual(res.body.businessCardDealHistory.result.is, constant.SUCCESS);

          return done();
        });
    });

    it('should failed verify token deal historys business card with bad token', (done) => {
      agent.post(url.BUSINESS_CARDS_DEAL_HISTORYS_TOKEN_ID.replace(':id', businessCardDealHistoryID))
        .expect(400)
        .set('X-Requested-With', 'XMLHttpRequest')
        .send({
          token: 'some_token',
        })
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          chai.assert.deepEqual(res.body.code, 'businessCardDealHistory.token.verify.failed');
          return done();
        });
    });

    it('should failed verify token deal historys business card with not signin', (done) => {
      agent.get(usersUrl.AUTH_SIGNOUT)
        .expect(200)
        .set('X-Requested-With', 'XMLHttpRequest')
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.post(url.BUSINESS_CARDS_DEAL_HISTORYS_TOKEN_ID.replace(':id', businessCardDealHistoryID))
            .expect(401)
            .set('X-Requested-With', 'XMLHttpRequest')
            .send({
              token,
            })
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.code, 'user.permission.notExist');
              return done();
            });
        });
    });

    it('should failed verify token deal historys business card with expires', (done) => {
      BusinessCardDealHistory.findById(businessCardDealHistoryID, (err, businessCardDealHistory) => {
        if (err) {
          return done(err);
        }

        businessCardDealHistory.timer = Date.now();
        businessCardDealHistory.save((err) => {
          if (err) {
            return done(err);
          }

          agent.post(url.BUSINESS_CARDS_DEAL_HISTORYS_TOKEN_ID.replace(':id', businessCardDealHistoryID))
            .expect(401)
            .set('X-Requested-With', 'XMLHttpRequest')
            .send({
              token,
            })
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.exists(res.body.businessCardDealHistory);
              chai.assert.deepEqual(res.body.businessCardDealHistory.result.is, constant.FAILED);

              return done();
            });
        });
      });
    });

    it('should failed verify token deal historys business card with has verify token deal historys business card', (done) => {
      agent.post(url.BUSINESS_CARDS_DEAL_HISTORYS_TOKEN_ID.replace(':id', businessCardDealHistoryID))
        .expect(200)
        .set('X-Requested-With', 'XMLHttpRequest')
        .send({
          token,
        })
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          chai.assert.exists(res.body.businessCardDealHistory);
          chai.assert.deepEqual(res.body.businessCardDealHistory.result.is, constant.SUCCESS);

          agent.post(url.BUSINESS_CARDS_DEAL_HISTORYS_TOKEN_ID.replace(':id', businessCardDealHistoryID))
            .expect(404)
            .set('X-Requested-With', 'XMLHttpRequest')
            .send({
              token,
            })
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.code, 'businessCardDealHistory.notExist');
              return done();
            });
        });
    });
  });

  describe('## Tố cáo lịch sử giao dịch danh thiếp', () => {
    beforeEach((done) => {
      agent.get(usersUrl.AUTH_SIGNOUT)
        .expect(200)
        .set('X-Requested-With', 'XMLHttpRequest')
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          utils.signin(agent, verifyUserAccount.numberPhone, password)
            .expect(200)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              agent.post(url.BUSINESS_CARDS_DEAL_HISTORYS_ACCEPT_ID.replace(':id', businessCardDealHistoryID))
                .expect(200)
                .set('X-Requested-With', 'XMLHttpRequest')
                .send({
                  accept: {
                    is: true,
                  },
                })
                .end((err, res) => {
                  if (err) {
                    return done(err);
                  }

                  chai.assert.exists(res.body.businessCardDealHistory);
                  chai.assert.deepEqual(res.body.businessCardDealHistory.status, constant.TOKEN);

                  agent.get(usersUrl.AUTH_SIGNOUT)
                    .expect(200)
                    .set('X-Requested-With', 'XMLHttpRequest')
                    .end((err, res) => {
                      if (err) {
                        return done(err);
                      }

                      utils.signin(agent, user.numberPhone, password)
                        .expect(200)
                        .end((err, res) => {
                          if (err) {
                            return done(err);
                          }

                          agent.get(url.BUSINESS_CARDS_DEAL_HISTORYS_TOKEN_ID.replace(':id', businessCardDealHistoryID))
                            .expect(200)
                            .set('X-Requested-With', 'XMLHttpRequest')
                            .end((err, res) => {
                              if (err) {
                                return done(err);
                              }

                              chai.assert.exists(res.body.businessCardDealHistory);
                              chai.assert.exists(res.body.businessCardDealHistory.token);
                              ({ token } = res.body.businessCardDealHistory);

                              BusinessCardDealHistory.findById(businessCardDealHistoryID, (err, businessCardDealHistory) => {
                                if (err) {
                                  return done(err);
                                }

                                businessCardDealHistory.timer = Date.now() + regular.businessCardDealHistory.expires.token;
                                businessCardDealHistory.save((err) => {
                                  if (err) {
                                    return done(err);
                                  }

                                  agent.get(usersUrl.AUTH_SIGNOUT)
                                    .expect(200)
                                    .set('X-Requested-With', 'XMLHttpRequest')
                                    .end((err, res) => {
                                      if (err) {
                                        return done(err);
                                      }

                                      utils.signin(agent, verifyUserAccount.numberPhone, password)
                                        .expect(200)
                                        .end((err, res) => {
                                          if (err) {
                                            return done(err);
                                          }

                                          agent.post(url.BUSINESS_CARDS_DEAL_HISTORYS_TOKEN_ID.replace(':id', businessCardDealHistoryID))
                                            .expect(200)
                                            .set('X-Requested-With', 'XMLHttpRequest')
                                            .send({
                                              token,
                                            })
                                            .end((err, res) => {
                                              if (err) {
                                                return done(err);
                                              }

                                              return done();
                                            });
                                        });
                                    });
                                });
                              });
                            });
                        });
                    });
                });
            });
        });
    });

    it('should success report deal historys business card', (done) => {
      agent.post(url.BUSINESS_CARDS_DEAL_HISTORYS_REPORT_ID.replace(':id', businessCardDealHistoryID))
        .expect(200)
        .set('X-Requested-With', 'XMLHttpRequest')
        .send({
          report: {
            reason: 'reason',
          },
        })
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          testReport(businessCardDealHistoryID, verifyUserAccount._id, done);
        });
    });

    it('should failed report deal historys business card with not signin', (done) => {
      agent.get(usersUrl.AUTH_SIGNOUT)
        .expect(200)
        .set('X-Requested-With', 'XMLHttpRequest')
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.post(url.BUSINESS_CARDS_DEAL_HISTORYS_REPORT_ID.replace(':id', businessCardDealHistoryID))
            .expect(401)
            .set('X-Requested-With', 'XMLHttpRequest')
            .send({
              report: {
                reason: 'reason',
              },
            })
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.code, 'user.permission.notExist');
              testReport(businessCardDealHistoryID, verifyUserAccount._id, done, false);
            });
        });
    });

    it('should failed report deal historys business card with bad id', (done) => {
      agent.post(url.BUSINESS_CARDS_DEAL_HISTORYS_REPORT_ID.replace(':id', mongoose.Types.ObjectId()))
        .expect(404)
        .set('X-Requested-With', 'XMLHttpRequest')
        .send({
          report: {
            reason: 'reason',
          },
        })
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          chai.assert.deepEqual(res.body.code, 'businessCardDealHistory.notExist');
          testReport(businessCardDealHistoryID, verifyUserAccount._id, done, false);
        });
    });

    it('should failed report deal historys business card with has report deal historys business card', (done) => {
      agent.post(url.BUSINESS_CARDS_DEAL_HISTORYS_REPORT_ID.replace(':id', businessCardDealHistoryID))
        .expect(200)
        .set('X-Requested-With', 'XMLHttpRequest')
        .send({
          report: {
            reason: 'reason',
          },
        })
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.post(url.BUSINESS_CARDS_DEAL_HISTORYS_REPORT_ID.replace(':id', businessCardDealHistoryID))
            .expect(404)
            .set('X-Requested-With', 'XMLHttpRequest')
            .send({
              report: {
                reason: 'reason',
              },
            })
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.code, 'businessCardDealHistory.notExist');
              testReport(businessCardDealHistoryID, verifyUserAccount._id, done);
            });
        });
    });

    it('should failed report deal historys business card with result is wait', (done) => {
      BusinessCardDealHistory
        .findById(businessCardDealHistoryID)
        .exec((err, businessCardDealHistory) => {
          chai.assert.notExists(err);

          businessCardDealHistory.result.is = constant.WAIT;
          businessCardDealHistory.save((err) => {
            chai.assert.notExists(err);

            agent.post(url.BUSINESS_CARDS_DEAL_HISTORYS_REPORT_ID.replace(':id', businessCardDealHistoryID))
              .expect(404)
              .set('X-Requested-With', 'XMLHttpRequest')
              .send({
                report: {
                  reason: 'reason',
                },
              })
              .end((err, res) => {
                if (err) {
                  return done(err);
                }

                chai.assert.deepEqual(res.body.code, 'businessCardDealHistory.notExist');
                testReport(businessCardDealHistoryID, verifyUserAccount._id, done, false);
              });
          });
        });
    });
  });

  describe('## Tìm kiếm lịch sử giao dịch danh thiếp', () => {
    describe('Tìm kiếm tự động', () => {
      it('should success auto search', (done) => {
        agent.get(url.BUSINESS_CARDS_DEAL_HISTORYS_SEARCH)
          .expect(200)
          .set('X-Requested-With', 'XMLHttpRequest')
          .query({
            type: constant.AUTO,
            skip: 0,
          })
          .end((err, res) => {
            if (err) {
              return done(err);
            }

            chai.assert.deepEqual(res.body.skip, 1);
            chai.assert.deepEqual(res.body.businessCardDealHistorys.length, 1);

            return done();
          });
      });

      it('should failed auto search with not signin', (done) => {
        agent.get(usersUrl.AUTH_SIGNOUT)
          .expect(200)
          .set('X-Requested-With', 'XMLHttpRequest')
          .end((err, res) => {
            if (err) {
              return done(err);
            }

            agent.get(url.BUSINESS_CARDS_DEAL_HISTORYS_SEARCH)
              .expect(401)
              .set('X-Requested-With', 'XMLHttpRequest')
              .query({
                type: constant.AUTO,
                skip: 0,
              })
              .end((err, res) => {
                if (err) {
                  return done(err);
                }

                chai.assert.deepEqual(res.body.code, 'user.permission.notExist');
                return done();
              });
          });
      });
    });

    describe('Tìm kiếm mở rộng', () => {
      it('should success extend search', (done) => {
        agent.get(url.BUSINESS_CARDS_DEAL_HISTORYS_SEARCH)
          .expect(200)
          .set('X-Requested-With', 'XMLHttpRequest')
          .query({
            type: constant.EXTEND,
            skip: 0,
          })
          .send({
            extendSearch: {
              result: constant.WAIT,
              status: constant.RESPONSE,
            },
          })
          .end((err, res) => {
            if (err) {
              return done(err);
            }

            chai.assert.deepEqual(res.body.skip, 1);
            chai.assert.deepEqual(res.body.businessCardDealHistorys.length, 1);

            return done();
          });
      });

      it('should failed extend search with not signin', (done) => {
        agent.get(usersUrl.AUTH_SIGNOUT)
          .expect(200)
          .set('X-Requested-With', 'XMLHttpRequest')
          .end((err, res) => {
            if (err) {
              return done(err);
            }

            agent.get(url.BUSINESS_CARDS_DEAL_HISTORYS_SEARCH)
              .expect(401)
              .set('X-Requested-With', 'XMLHttpRequest')
              .query({
                skip: 0,
              })
              .send({
                extendSearch: {
                  result: constant.RESPONSE,
                  status: constant.TOKEN,
                },
              })
              .query({
                type: constant.EXTEND,
              })
              .end((err, res) => {
                if (err) {
                  return done(err);
                }

                chai.assert.deepEqual(res.body.code, 'user.permission.notExist');
                return done();
              });
          });
      });
    });
  });
});
