const path = require('path');
const mongoose = require('mongoose');
const request = require('supertest');
const chai = require('chai');
const _ = require('lodash');
const cloudinary = require(path.resolve('config/lib/cloudinary'));
const express = require(path.resolve('./config/lib/express'));
const url = require(path.resolve('./commons/modules/business-cards/datas/url.data'));
const usersUrl = require(path.resolve('./commons/modules/users/datas/url.data'));
const constant = require(path.resolve('./commons/utils/constant'));
const regular = require(path.resolve('./commons/utils/regular-expression'));
const utils = require(path.resolve('./utils/test'));

describe('### Business Cards Server Routes Tests', () => {
  let app = null;
  let agent = null;

  let ActivityHistory = null;
  let BusinessCardDealHistory = null;
  let Notification = null;

  let BusinessCard = null;
  let businessCard = null;
  let businessCardID = null;

  let User = null;
  let verifyUserAccount = null;
  let adminUser = null;

  const password = 'password';

  let publicID = null;
  const descriptions = 'some_descriptions';
  const address = {
    country: 'some_country',
    city: 'some_city',
    county: 'some_county',
    local: 'some_local',
  };
  let comment = 'Comment_1';
  let commentID = null;

  /**
   * @overview Kiểm tra nhận xét
   *
   * @param {String} type
   * @param {ObjectId} id
   * @param {ObjectId} businessCardID
   * @param {String} msg
   * @param {Function} done
   * @param {Boolean} isExist
   * @param {Boolean} isCha
   */
  const testComment = (type, id, businessCardID, msg, done, isExist = true, isOwner = true) => {
    BusinessCard
      .findOne({
        $or: [{
          _id: businessCardID,
          'comments.userID': id,
        }, {
          _id: businessCardID,
          'comments._id': id,
        }],
      })
      .exec((err, businessCard) => {
        chai.assert.notExists(err);

        if (isExist) {
          switch (type) {
            case 'create': {
              chai.assert.exists(businessCard);
              const indexComment = _.findIndex(businessCard.comments, (comment) => {
                return (
                  comment._id.equals(id)
                  && comment.msg === msg
                  && comment.change.is === false
                );
              });
              chai.assert.isFalse(indexComment === -1);
            }
              break;
            case 'change': {
              chai.assert.exists(businessCard);
              const indexComment = _.findIndex(businessCard.comments, (comment) => {
                return (
                  comment._id.equals(id)
                  && comment.msg === msg
                  && comment.change.is === false
                );
              });
              chai.assert.isTrue(indexComment === -1);
            }
              break;
            case constant.DELETE:
              chai.assert.notExists(businessCard);
              break;
            default:
              break;
          }
        } else {
          switch (type) {
            case 'create':
              chai.assert.notExists(businessCard);
              break;
            case 'change': {
              chai.assert.exists(businessCard);
              const indexComment = _.findIndex(businessCard.comments, (comment) => {
                return (
                  comment._id.equals(id)
                  && comment.msg === msg
                  && comment.change.is === false
                );
              });
              chai.assert.isTrue(indexComment !== -1);
            }
              break;
            case constant.DELETE:
              chai.assert.exists(businessCard);
              break;
            default:
              break;
          }
        }

        let filter = {};
        switch (type) {
          case 'create':
            filter = {
              type: constant.BUSINESS_CARD,
              result: constant.SUCCESS,
              content: 'businessCard.comment.is',
            };
            break;
          case 'change':
            filter = {
              type: constant.BUSINESS_CARD,
              result: constant.SUCCESS,
              content: 'businessCard.comment.change.is',
            };
            break;
          case constant.DELETE:
            filter = {
              type: constant.BUSINESS_CARD,
              result: constant.SUCCESS,
              content: 'businessCard.comment.delete.is',
            };
            break;
          default:
            break;
        }

        ActivityHistory
          .findOne(filter)
          .exec((err, activityHistory) => {
            chai.assert.notExists(err);

            if (isExist) {
              chai.assert.exists(activityHistory);

              if (!isOwner) {
                switch (type) {
                  case 'create':
                    Notification
                      .findOne({
                        receiveID: businessCard.userID,
                        type: constant.BUSINESS_CARD,
                        result: constant.INFO,
                        content: 'businessCard.comment.exist',
                      })
                      .exec((err, notification) => {
                        chai.assert.notExists(err);

                        if (isExist) {
                          chai.assert.exists(notification);
                        } else {
                          chai.assert.notExists(notification);
                        }

                        if (done) done();
                      });
                    break;
                  default:
                    if (done) done();
                    break;
                }
              } else if (done) {
                done();
              }
            } else {
              chai.assert.notExists(activityHistory);
              if (done) done();
            }
          });
      });
  };

  /**
   * @overview Kiểm tra tố cáo
   *
   * @param {ObjectId} userID
   * @param {ObjectId} businessCardID
   * @param {Function} done
   * @param {Boolean} isExist
   */
  const testReport = (userID, businessCardID, done, isExist = true) => {
    BusinessCard
      .findOne({
        _id: businessCardID,
        'reports.userID': userID,
      })
      .exec((err, businessCard) => {
        chai.assert.notExists(err);

        if (isExist) {
          chai.assert.exists(businessCard);
        } else {
          chai.assert.notExists(businessCard);
        }

        ActivityHistory
          .findOne({
            userID,
            type: constant.BUSINESS_CARD,
            result: constant.SUCCESS,
            content: 'businessCard.report.is',
          })
          .exec((err, activityHistory) => {
            chai.assert.notExists(err);

            if (isExist) {
              chai.assert.exists(activityHistory);
            } else {
              chai.assert.notExists(activityHistory);
            }

            Notification
              .findOne({
                sendID: userID,
                type: constant.BUSINESS_CARD,
                result: constant.WARNING,
                content: 'businessCard.report.exist',
              })
              .exec((err, notification) => {
                chai.assert.notExists(err);

                if (isExist) {
                  chai.assert.exists(notification);
                } else {
                  chai.assert.notExists(notification);
                }

                if (done) done();
              });
          });
      });
  };

  /**
   * @overview Kiểm tra giao dịch
   *
   * @param {ObjectId} userID
   * @param {Function} done
   * @param {Boolean} isExist
   */
  const testDeal = (userID, done, isExist = true) => {
    BusinessCardDealHistory
      .findOne({
        userID,
      })
      .exec((err, dealHistory) => {
        chai.assert.notExists(err);

        if (isExist) {
          chai.assert.exists(dealHistory);
        } else {
          chai.assert.notExists(dealHistory);
        }

        ActivityHistory
          .findOne({
            type: constant.BUSINESS_CARD_DEAL_HISTORY,
            result: constant.SUCCESS,
            content: 'businessCardDealHistory.create.is',
          })
          .exec((err, activityHistory) => {
            chai.assert.notExists(err);

            if (isExist) {
              chai.assert.exists(activityHistory);
              chai.assert.isTrue(activityHistory.userID.equals(dealHistory.userID));
            } else {
              chai.assert.notExists(activityHistory);
            }

            ActivityHistory
              .findOne({
                type: constant.BUSINESS_CARD_DEAL_HISTORY,
                result: constant.SUCCESS,
                content: 'businessCardDealHistory.exist',
              })
              .exec((err, activityHistory) => {
                chai.assert.notExists(err);

                if (isExist) {
                  chai.assert.exists(activityHistory);
                  chai.assert.isTrue(activityHistory.userID.equals(dealHistory.providerID));
                } else {
                  chai.assert.notExists(activityHistory);
                }

                Notification
                  .findOne({
                    type: constant.BUSINESS_CARD_DEAL_HISTORY,
                    result: constant.INFO,
                    content: 'businessCardDealHistory.exist',
                  })
                  .exec((err, notification) => {
                    chai.assert.notExists(err);

                    if (isExist) {
                      chai.assert.exists(notification);
                      chai.assert.isTrue(notification.receiveID.equals(dealHistory.providerID));
                      chai.assert.isTrue(notification.sendID.equals(dealHistory.userID));
                    } else {
                      chai.assert.notExists(notification);
                    }

                    if (done) done();
                  });
              });
          });
      });
  };

  /**
   * @overview Kiểm tra dăng ảnh
   *
   * @param {ObjectId} userID
   * @param {ObjectId} businessCardID
   * @param {Number} limit
   * @param {Function} done
   * @param {Boolean} isExist
   * @param {Boolean} isDelete
   */
  const testUploadImage = (userID, businessCardID, limit, done, isExist = true, isDelete = false) => {
    BusinessCard
      .findById(businessCardID)
      .exec((err, businessCard) => {
        chai.assert.notExists(err);
        chai.assert.exists(businessCard);

        if (isExist) {
          chai.assert.deepEqual(businessCard.images.length, limit);
        } else {
          chai.assert.notDeepEqual(businessCard.images.length, limit);
        }

        let filter = {
          userID,
          type: constant.BUSINESS_CARD,
          result: constant.SUCCESS,
          content: 'businessCard.image.upload.is',
        };

        if (isDelete) {
          filter = {
            userID,
            type: constant.BUSINESS_CARD,
            result: constant.SUCCESS,
            content: 'businessCard.image.delete.is',
          };
        }

        ActivityHistory
          .findOne(filter)
          .exec((err, activityHistory) => {
            chai.assert.notExists(err);

            if (isExist) {
              chai.assert.exists(activityHistory);
            } else {
              chai.assert.notExists(activityHistory);
            }

            if (done) done();
          });
      });
  };

  /**
   * @overview Kiểm tra thay đổi địa chỉ
   *
   * @param {ObjectId} userID
   * @param {ObjectId} businessCardID
   * @param {Object} address
   * @param {Function} done
   * @param {Boolean} isExist
   */
  const testAddressBusinessCard = (userID, businessCardID, address, done, isExist = true) => {
    BusinessCard
      .findById(businessCardID)
      .exec((err, businessCard) => {
        chai.assert.notExists(err);
        chai.assert.exists(businessCard);

        if (isExist) {
          chai.assert.deepEqual(businessCard.address.city, address.city);
          chai.assert.deepEqual(businessCard.address.country, address.country);
          chai.assert.deepEqual(businessCard.address.county, address.county);
          chai.assert.deepEqual(businessCard.address.local, address.local);
        } else {
          chai.assert.notDeepEqual(businessCard.address.city, address.city);
          chai.assert.notDeepEqual(businessCard.address.country, address.country);
          chai.assert.notDeepEqual(businessCard.address.county, address.county);
          chai.assert.notDeepEqual(businessCard.address.local, address.local);
        }

        ActivityHistory
          .findOne({
            userID,
            type: constant.BUSINESS_CARD,
            result: constant.SUCCESS,
            content: 'businessCard.address.change.is',
          })
          .exec((err, activityHistory) => {
            chai.assert.notExists(err);

            if (isExist) {
              chai.assert.exists(activityHistory);
            } else {
              chai.assert.notExists(activityHistory);
            }

            if (done) done();
          });
      });
  };

  /**
   * @overview Kiểm tra thay đổi mô tả
   *
   * @param {ObjectId} userID
   * @param {ObjectId} businessCardID
   * @param {String} note
   * @param {Function} done
   * @param {Boolean} isExist
   */
  const testDescriptionsBusinessCard = (userID, businessCardID, descriptions, done, isExist = true) => {
    BusinessCard
      .findById(businessCardID)
      .exec((err, businessCard) => {
        chai.assert.notExists(err);
        chai.assert.exists(businessCard);

        if (isExist) {
          chai.assert.deepEqual(businessCard.descriptions, descriptions);
        } else {
          chai.assert.notDeepEqual(businessCard.descriptions, descriptions);
        }

        ActivityHistory
          .findOne({
            userID,
            type: constant.BUSINESS_CARD,
            result: constant.SUCCESS,
            content: 'businessCard.descriptions.change.is',
          })
          .exec((err, activityHistory) => {
            chai.assert.notExists(err);

            if (isExist) {
              chai.assert.exists(activityHistory);
            } else {
              chai.assert.notExists(activityHistory);
            }

            if (done) done();
          });
      });
  };

  /**
   * @overview Kiểm tra thay đổi trạng tháy
   *
   * @param {ObjectId} userID
   * @param {ObjectId} businessCardID
   * @param {Boolean} isStatus
   * @param {Function} done
   * @param {Boolean} isExist
   */
  const testStatusBusinessCard = (userID, businessCardID, isStatus, done, isExist = true) => {
    BusinessCard
      .findById(businessCardID)
      .exec((err, businessCard) => {
        chai.assert.notExists(err);
        chai.assert.exists(businessCard);

        if (isExist) {
          chai.assert.isTrue(businessCard.status.is === isStatus);
        } else {
          chai.assert.isFalse(businessCard.status.is === isStatus);
        }

        ActivityHistory
          .findOne({
            userID,
            type: constant.BUSINESS_CARD,
            result: constant.SUCCESS,
            content: 'businessCard.status.change.is',
          })
          .exec((err, activityHistory) => {
            chai.assert.notExists(err);

            if (isExist) {
              chai.assert.exists(activityHistory);
            } else {
              chai.assert.notExists(activityHistory);
            }

            if (done) done();
          });
      });
  };

  /**
 * @overview Kiểm tra đăng lại
 *
 * @param {ObjectId} userID
 * @param {ObjectId} businessCardID
 * @param {Boolean} isStatus
 * @param {Function} done
 * @param {Boolean} isExist
 */
  const testReUploadBusinessCard = (userID, businessCardID, reUpload, done, isExist = true) => {
    BusinessCard
      .findById(businessCardID)
      .exec((err, businessCard) => {
        chai.assert.notExists(err);
        chai.assert.exists(businessCard);

        if (isExist) {
          chai.assert.deepEqual(businessCard.reUpload.is, reUpload.is);
          chai.assert.deepEqual(businessCard.reUpload.count, reUpload.count);
        } else {
          chai.assert.notDeepEqual(businessCard.reUpload, reUpload);
        }

        ActivityHistory
          .findOne({
            userID,
            type: constant.BUSINESS_CARD,
            result: constant.SUCCESS,
            content: 'businessCard.reUpload.is',
          })
          .exec((err, activityHistory) => {
            chai.assert.notExists(err);

            if (isExist) {
              chai.assert.exists(activityHistory);
            } else {
              chai.assert.notExists(activityHistory);
            }

            if (done) done();
          });
      });
  };

  /**
   * @overview Kiểm tra thích
   *
   * @param {ObjectId} userID
   * @param {ObjectId} businessCardID
   * @param {Function} done
   * @param {Boolean} isExist
   * @param {String} type
   */
  const testLike = (userID, businessCardID, done, isExist = true, isOwner = true) => {
    BusinessCard
      .findOne({
        _id: businessCardID,
        'likes.userID': userID,
      })
      .exec((err, businessCard) => {
        chai.assert.notExists(err);

        if (isExist) {
          chai.assert.exists(businessCard);
        } else {
          chai.assert.notExists(businessCard);
        }

        ActivityHistory
          .findOne({
            userID,
            type: constant.BUSINESS_CARD,
            result: constant.SUCCESS,
            content: 'businessCard.like.is',
          })
          .exec((err, activityHistory) => {
            chai.assert.notExists(err);

            if (isExist) {
              chai.assert.exists(activityHistory);
            } else {
              chai.assert.notExists(activityHistory);
            }

            if (!isOwner && isExist) {
              Notification
                .findOne({
                  sendID: userID,
                  type: constant.BUSINESS_CARD,
                  result: constant.INFO,
                  content: 'businessCard.like.exist',
                })
                .exec((err, notification) => {
                  chai.assert.notExists(err);

                  if (isExist) {
                    chai.assert.exists(notification);
                  } else {
                    chai.assert.notExists(notification);
                  }

                  if (done) done();
                });
            } else if (done) {
              done();
            }
          });
      });
  };

  /**
   * @overview Kiểm tra bỏ thích
   *
   * @param {ObjectId} userID
   * @param {ObjectId} businessCardID
   * @param {Function} done
   * @param {Boolean} isExist
   * @param {String} type
   */
  const testUnLike = (userID, businessCardID, done, isExist = true) => {
    BusinessCard
      .findOne({
        _id: businessCardID,
        'likes.userID': userID,
      })
      .exec((err, businessCard) => {
        chai.assert.notExists(err);

        if (isExist) {
          chai.assert.notExists(businessCard);
        } else {
          chai.assert.exists(businessCard);
        }

        ActivityHistory
          .findOne({
            userID,
            type: constant.BUSINESS_CARD,
            result: constant.SUCCESS,
            content: 'businessCard.unlike.is',
          })
          .exec((err, activityHistory) => {
            chai.assert.notExists(err);

            if (isExist) {
              chai.assert.exists(activityHistory);
            } else {
              chai.assert.notExists(activityHistory);
            }

            if (done) done();
          });
      });
  };

  /**
   * @overview Kiểm tra tạo danh thiếp
   *
   * @param {ObjectId} userID
   * @param {Function} done
   * @param {Boolean} isExist
   */
  const testCreateBusinessCard = (userID, done, isExist = true) => {
    BusinessCard
      .findOne({ userID })
      .exec((err, businessCard) => {
        chai.assert.notExists(err);

        if (isExist) {
          chai.assert.exists(businessCard);
        } else {
          chai.assert.notExists(businessCard);
        }

        ActivityHistory
          .findOne({
            userID,
            type: constant.BUSINESS_CARD,
            result: constant.SUCCESS,
            content: 'businessCard.create.is',
          })
          .exec((err, activityHistory) => {
            chai.assert.notExists(err);

            if (isExist) {
              chai.assert.exists(activityHistory);
            } else {
              chai.assert.notExists(activityHistory);
            }

            if (done) done();
          });
      });
  };

  /**
   * @overview Kiểm tra xoá danh thiếp
   *
   * @param {ObjectId} userID
   * @param {Function} done
   * @param {Boolean} isExist
   */
  const testDeleteBusinessCard = (userID, done, isExist = true) => {
    BusinessCard
      .findOne({ userID })
      .exec((err, businessCard) => {
        chai.assert.notExists(err);

        if (isExist) {
          chai.assert.notExists(businessCard);
        } else {
          chai.assert.exists(businessCard);
        }

        ActivityHistory
          .findOne({
            userID,
            type: constant.BUSINESS_CARD,
            result: constant.SUCCESS,
            content: 'businessCard.delete.is',
          })
          .exec((err, activityHistory) => {
            chai.assert.notExists(err);

            if (isExist) {
              chai.assert.exists(activityHistory);
            } else {
              chai.assert.notExists(activityHistory);
            }

            if (done) done();
          });
      });
  };

  const uploadImage = (name) => {
    return new Promise((resolve, reason) => {
      agent.post(url.BUSINESS_CARDS_IMAGE)
        .expect(200)
        .set('X-Requested-With', 'XMLHttpRequest')
        .attach(name, `./modules/business-cards/tests/server/images/business-cards/${name}.png`)
        .query({
          name,
        })
        .end((err, res) => {
          if (err) {
            reason(err);
          }

          resolve(res);
        });
    });
  };

  before((done) => {
    app = express.init(mongoose.connection.db);
    agent = request.agent(app);

    User = mongoose.model('User');
    BusinessCard = mongoose.model('BusinessCard');
    ActivityHistory = mongoose.model('ActivityHistory');
    Notification = mongoose.model('Notification');
    BusinessCardDealHistory = mongoose.model('BusinessCardDealHistory');

    done();
  });

  after((done) => {
    cloudinary.deleteResources('test')
      .catch((err) => {
        chai.assert.notExists(err);
      });

    done();
  });

  beforeEach((done) => {
    businessCard = {
      title: 'title',
      blurFace: true,
      costs: {
        rooms: [{
          value: constant.ONE_TIME,
          moneys: 50,
        }],
        services: [{
          value: constant.ONE_SLOT,
          moneys: 150,
        }],
        currencyUnit: constant.DOMESTIC,
      },
      measurements3: {
        breast: 90,
        waist: 45,
        hips: 90,
      },
      descriptions: 'descriptions',
      address: {
        country: 'country',
        city: 'city',
        county: 'county',
        local: 'local',
      },
    };

    verifyUserAccount = {
      verify: {
        account: {
          is: true,
        },
      },
      name: {
        first: 'first',
        last: 'last',
        display: 'last first',
        nick: 'user_verify',
      },
      numberPhone: {
        code: '+84',
        number: '0000000000',
      },
      gender: constant.MALE,
      birthday: {
        day: 1,
        month: 1,
        year: 2000,
      },
      password,
      address: {
        country: 'country',
        city: 'city',
        county: 'county',
        local: 'local',
      },
      settings: {
        general: {
          language: constant.VI,
        },
      },
    };

    adminUser = _.cloneDeep(verifyUserAccount);
    adminUser.name.nick = 'user_admin';
    adminUser.numberPhone.number = '9999999999';
    adminUser.roles = {
      value: constant.ADMIN,
    };

    verifyUserAccount = new User(verifyUserAccount);
    adminUser = new User(adminUser);

    verifyUserAccount.save((err) => {
      chai.assert.notExists(err);

      adminUser.save((err) => {
        chai.assert.notExists(err);

        utils.signin(agent, verifyUserAccount.numberPhone, password)
          .expect(200)
          .end((err, res) => {
            if (err) {
              return done(err);
            }

            agent.post(url.BUSINESS_CARDS)
              .set('X-Requested-With', 'XMLHttpRequest')
              .send(businessCard)
              .expect(200)
              .end((err, res) => {
                if (err) {
                  return done(err);
                }

                chai.assert.exists(res.body.businessCard);
                businessCardID = res.body.businessCard._id;

                return done();
              });
          });
      });
    });
  });

  afterEach((done) => {
    User.deleteMany({}, (err) => {
      chai.assert.notExists(err);
      BusinessCard.deleteMany({}, (err) => {
        chai.assert.notExists(err);
        ActivityHistory.deleteMany({}, (err) => {
          chai.assert.notExists(err);
          Notification.deleteMany({}, (err) => {
            chai.assert.notExists(err);
            BusinessCardDealHistory.deleteMany({}, (err) => {
              chai.assert.notExists(err);
              done();
            });
          });
        });
      });
    });
  });

  describe('## Tạo danh thiếp', () => {
    it('should success create business card', (done) => {
      agent.delete(url.BUSINESS_CARDS)
        .expect(200)
        .set('X-Requested-With', 'XMLHttpRequest')
        .send({
          password: 'password'
        })
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.post(url.BUSINESS_CARDS)
            .set('X-Requested-With', 'XMLHttpRequest')
            .send(businessCard)
            .expect(200)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.exists(res.body.businessCard);
              testCreateBusinessCard(verifyUserAccount._id, done);
            });
        });
    });

    it('should failed create business card with not signin user', (done) => {
      agent.get(usersUrl.AUTH_SIGNOUT)
        .set('X-Requested-With', 'XMLHttpRequest')
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.post(url.BUSINESS_CARDS)
            .set('X-Requested-With', 'XMLHttpRequest')
            .send(businessCard)
            .expect(401)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.code, 'user.permission.notExist');
              return done();
            });
        });
    });

    it('should failed create business card with not verify user', (done) => {
      agent.delete(url.BUSINESS_CARDS)
        .expect(200)
        .set('X-Requested-With', 'XMLHttpRequest')
        .send({
          password: 'password'
        })
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          verifyUserAccount.verify.account.is = false;
          verifyUserAccount.save((err) => {
            chai.assert.notExists(err);

            agent.post(url.BUSINESS_CARDS)
              .set('X-Requested-With', 'XMLHttpRequest')
              .send(businessCard)
              .expect(401)
              .end((err, res) => {
                if (err) {
                  return done(err);
                }

                chai.assert.deepEqual(res.body.code, 'user.verify.account.notExist');
                return done();
              });
          });
        });
    });

    it('should failed create business card with exist business card', (done) => {
      agent.post(url.BUSINESS_CARDS)
        .set('X-Requested-With', 'XMLHttpRequest')
        .send(businessCard)
        .expect(401)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          chai.assert.deepEqual(res.body.code, 'user.permission.notExist');
          testCreateBusinessCard(verifyUserAccount._id, done);
        });
    });
  });

  describe('## Xoá danh thiếp', () => {
    it('should success delete business card', (done) => {
      agent.delete(url.BUSINESS_CARDS)
        .expect(200)
        .set('X-Requested-With', 'XMLHttpRequest')
        .send({
          password: 'password'
        })
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          testDeleteBusinessCard(verifyUserAccount._id, done);
        });
    });

    it('should failed delete business card with not business card exist', (done) => {
      agent.delete(url.BUSINESS_CARDS)
        .expect(200)
        .set('X-Requested-With', 'XMLHttpRequest')
        .send({
          password: 'password'
        })
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.delete(url.BUSINESS_CARDS)
            .expect(401)
            .set('X-Requested-With', 'XMLHttpRequest')
            .send({
              password: 'password'
            })
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.code, 'user.permission.notExist');
              return done();
            });
        });
    });

    it('should failed delete business card with bad body', (done) => {
      agent.delete(url.BUSINESS_CARDS)
        .expect(401)
        .set('X-Requested-With', 'XMLHttpRequest')
        .send({
          password: 'passwordsome'
        })
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          chai.assert.deepEqual(res.body.code, 'user.signin.failed');
          testDeleteBusinessCard(verifyUserAccount._id, done, false);
        });
    });

    it('should failed delete business card with not signin', (done) => {
      agent.get(usersUrl.AUTH_SIGNOUT)
        .expect(200)
        .set('X-Requested-With', 'XMLHttpRequest')
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.delete(url.BUSINESS_CARDS)
            .expect(401)
            .set('X-Requested-With', 'XMLHttpRequest')
            .send({
              password: 'some_password',
            })
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.code, 'user.permission.notExist');
              testDeleteBusinessCard(verifyUserAccount._id, done, false);
            });
        });
    });
  });

  describe('## Lây thông tin danh thiếp', () => {
    it('should success get self business card', (done) => {
      agent.get(url.BUSINESS_CARDS)
        .expect(200)
        .set('X-Requested-With', 'XMLHttpRequest')
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          chai.assert.exists(res.body.businessCard);
          return done();
        });
    });

    it('should failed get self business card with not business card exist', (done) => {
      agent.delete(url.BUSINESS_CARDS)
        .expect(200)
        .set('X-Requested-With', 'XMLHttpRequest')
        .send({
          password: 'password'
        })
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.get(url.BUSINESS_CARDS)
            .expect(401)
            .set('X-Requested-With', 'XMLHttpRequest')
            .send({
              password: 'password'
            })
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.code, 'user.permission.notExist');
              return done();
            });
        });
    });

    it('should failed get self business card with not signin', (done) => {
      agent.get(usersUrl.AUTH_SIGNOUT)
        .expect(200)
        .set('X-Requested-With', 'XMLHttpRequest')
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.get(url.BUSINESS_CARDS)
            .expect(401)
            .set('X-Requested-With', 'XMLHttpRequest')
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.code, 'user.permission.notExist');
              return done();
            });
        });
    });
  });

  describe('## Lây thông tin danh thiếp theo ID', () => {
    it('should success get business card', (done) => {
      agent.get(url.BUSINESS_CARDS_GET_ID.replace(':id', businessCardID))
        .expect(200)
        .set('X-Requested-With', 'XMLHttpRequest')
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          chai.assert.exists(res.body.businessCard);
          return done();
        });
    });

    it('should failed get business card with bad id', (done) => {
      agent.get(url.BUSINESS_CARDS_GET_ID.replace(':id', mongoose.Types.ObjectId()))
        .expect(404)
        .set('X-Requested-With', 'XMLHttpRequest')
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          chai.assert.deepEqual(res.body.code, 'businessCard.notExist');
          return done();
        });
    });

    it('should failed get business card with not signin', (done) => {
      agent.get(usersUrl.AUTH_SIGNOUT)
        .expect(200)
        .set('X-Requested-With', 'XMLHttpRequest')
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.get(url.BUSINESS_CARDS_GET_ID.replace(':id', businessCardID))
            .expect(401)
            .set('X-Requested-With', 'XMLHttpRequest')
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.code, 'user.permission.notExist');
              return done();
            });
        });
    });
  });

  describe('## Đăng lại danh thiếp', () => {
    it('should success reupload business card', (done) => {
      agent.post(url.BUSINESS_CARDS_REUPLOAD)
        .expect(200)
        .set('X-Requested-With', 'XMLHttpRequest')
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          chai.assert.exists(res.body.reUpload);
          testReUploadBusinessCard(verifyUserAccount._id, businessCardID, res.body.reUpload, done);
        });
    });

    it('should failed reupload business card with not count reupload', (done) => {
      agent.post(url.BUSINESS_CARDS_REUPLOAD)
        .expect(200)
        .set('X-Requested-With', 'XMLHttpRequest')
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          chai.assert.exists(res.body.reUpload);

          agent.post(url.BUSINESS_CARDS_REUPLOAD)
            .expect(401)
            .set('X-Requested-With', 'XMLHttpRequest')
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.code, 'businessCard.reUpload.failed');

              return done();
            });
        });
    });

    it('should failed reupload business card with not business card exist', (done) => {
      agent.delete(url.BUSINESS_CARDS)
        .expect(200)
        .set('X-Requested-With', 'XMLHttpRequest')
        .send({
          password: 'password'
        })
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.post(url.BUSINESS_CARDS_REUPLOAD)
            .expect(401)
            .set('X-Requested-With', 'XMLHttpRequest')
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.code, 'user.permission.notExist');

              return done();
            });
        });
    });

    it('should failed reupload business card with not signin', (done) => {
      agent.get(usersUrl.AUTH_SIGNOUT)
        .expect(200)
        .set('X-Requested-With', 'XMLHttpRequest')
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.post(url.BUSINESS_CARDS_REUPLOAD)
            .expect(401)
            .set('X-Requested-With', 'XMLHttpRequest')
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.code, 'user.permission.notExist');

              return done();
            });
        });
    });
  });

  describe('## Thay đổi trạng thái danh thiếp', () => {
    it('should success change status business card (false)', (done) => {
      agent.post(url.BUSINESS_CARDS_STATUS)
        .expect(200)
        .set('X-Requested-With', 'XMLHttpRequest')
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          chai.assert.exists(res.body.status);
          testStatusBusinessCard(verifyUserAccount._id, businessCardID, false, done);
        });
    });

    it('should failed change status business card with not business card exist', (done) => {
      agent.delete(url.BUSINESS_CARDS)
        .expect(200)
        .set('X-Requested-With', 'XMLHttpRequest')
        .send({
          password: 'password'
        })
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.post(url.BUSINESS_CARDS_STATUS)
            .expect(401)
            .set('X-Requested-With', 'XMLHttpRequest')
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.code, 'user.permission.notExist');

              return done();
            });
        });
    });

    it('should failed change status business card  with not signin', (done) => {
      agent.get(usersUrl.AUTH_SIGNOUT)
        .expect(200)
        .set('X-Requested-With', 'XMLHttpRequest')
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.post(url.BUSINESS_CARDS_STATUS)
            .expect(401)
            .set('X-Requested-With', 'XMLHttpRequest')
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.code, 'user.permission.notExist');
              testStatusBusinessCard(verifyUserAccount._id, businessCardID, false, done, false);
            });
        });
    });
  });

  describe('## Thay đổi địa chỉ danh thiếp', () => {
    it('should success change address business card', (done) => {
      agent.post(url.BUSINESS_CARDS_ADDRESS)
        .expect(200)
        .set('X-Requested-With', 'XMLHttpRequest')
        .send({
          address,
        })
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          chai.assert.exists(res.body.address);
          testAddressBusinessCard(verifyUserAccount._id, businessCardID, address, done);
        });
    });

    it('should failed change address business card with bad body', (done) => {
      agent.post(url.BUSINESS_CARDS_ADDRESS)
        .expect(400)
        .set('X-Requested-With', 'XMLHttpRequest')
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          chai.assert.deepEqual(res.body.code, 'data.notVaild');
          testAddressBusinessCard(verifyUserAccount._id, businessCardID, address, done, false);
        });
    });

    it('should failed change address business card with not business card exist', (done) => {
      agent.delete(url.BUSINESS_CARDS)
        .expect(200)
        .set('X-Requested-With', 'XMLHttpRequest')
        .send({
          password: 'password'
        })
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.post(url.BUSINESS_CARDS_ADDRESS)
            .expect(401)
            .set('X-Requested-With', 'XMLHttpRequest')
            .send({
              address,
            })
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.code, 'user.permission.notExist');

              return done();
            });
        });
    });

    it('should failed change address business card with not signin', (done) => {
      agent.get(usersUrl.AUTH_SIGNOUT)
        .expect(200)
        .set('X-Requested-With', 'XMLHttpRequest')
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.post(url.BUSINESS_CARDS_ADDRESS)
            .expect(401)
            .set('X-Requested-With', 'XMLHttpRequest')
            .send({
              address,
            })
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.code, 'user.permission.notExist');
              testAddressBusinessCard(verifyUserAccount._id, businessCardID, address, done, false);
            });
        });
    });
  });

  describe('## Thay đổi mô tả danh thiếp', () => {
    it('should success change descriptions business card', (done) => {
      agent.post(url.BUSINESS_CARDS_DESCRIPTIONS)
        .expect(200)
        .set('X-Requested-With', 'XMLHttpRequest')
        .send({
          descriptions,
        })
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          chai.assert.exists(res.body.descriptions);
          testDescriptionsBusinessCard(verifyUserAccount._id, businessCardID, descriptions, done);
        });
    });

    it('should failed change descriptionsbusiness card with not business card exist', (done) => {
      agent.delete(url.BUSINESS_CARDS)
        .expect(200)
        .set('X-Requested-With', 'XMLHttpRequest')
        .send({
          password: 'password'
        })
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.post(url.BUSINESS_CARDS_DESCRIPTIONS)
            .expect(401)
            .set('X-Requested-With', 'XMLHttpRequest')
            .send({
              descriptions,
            })
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.code, 'user.permission.notExist');

              return done();
            });
        });
    });

    it('should failed change descriptionsbusiness card with not signin', (done) => {
      agent.get(usersUrl.AUTH_SIGNOUT)
        .expect(200)
        .set('X-Requested-With', 'XMLHttpRequest')
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.post(url.BUSINESS_CARDS_DESCRIPTIONS)
            .expect(401)
            .set('X-Requested-With', 'XMLHttpRequest')
            .send({
              descriptions,
            })
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.code, 'user.permission.notExist');
              testDescriptionsBusinessCard(verifyUserAccount._id, businessCardID, descriptions, done, false);
            });
        });
    });
  });

  describe('## Thích danh thiếp', () => {
    it('should success like business card with self', (done) => {
      agent.post(url.BUSINESS_CARDS_LIKE_ID.replace(':id', businessCardID))
        .expect(200)
        .set('X-Requested-With', 'XMLHttpRequest')
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          testLike(verifyUserAccount._id, businessCardID, done);
        });
    });

    it('should success like business card with user', (done) => {
      agent.get(usersUrl.AUTH_SIGNOUT)
        .expect(200)
        .set('X-Requested-With', 'XMLHttpRequest')
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          utils.signin(agent, adminUser.numberPhone, password)
            .expect(200)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              agent.post(url.BUSINESS_CARDS_LIKE_ID.replace(':id', businessCardID))
                .expect(200)
                .set('X-Requested-With', 'XMLHttpRequest')
                .end((err, res) => {
                  if (err) {
                    return done(err);
                  }

                  testLike(adminUser._id, businessCardID, done, true, false);
                });
            });
        });
    });

    it('should failed like business card with exist like business card', (done) => {
      agent.post(url.BUSINESS_CARDS_LIKE_ID.replace(':id', businessCardID))
        .expect(200)
        .set('X-Requested-With', 'XMLHttpRequest')
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.post(url.BUSINESS_CARDS_LIKE_ID.replace(':id', businessCardID))
            .expect(401)
            .set('X-Requested-With', 'XMLHttpRequest')
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.code, 'businessCard.like.exist');
              testLike(verifyUserAccount._id, businessCardID, done);
            });
        });
    });

    it('should failed like business card with bad id', (done) => {
      agent.post(url.BUSINESS_CARDS_LIKE_ID.replace(':id', mongoose.Types.ObjectId()))
        .expect(404)
        .set('X-Requested-With', 'XMLHttpRequest')
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          chai.assert.deepEqual(res.body.code, 'businessCard.notExist');
          testLike(verifyUserAccount._id, businessCardID, done, false);
        });
    });

    it('should failed like business card with not signin', (done) => {
      agent.get(usersUrl.AUTH_SIGNOUT)
        .expect(200)
        .set('X-Requested-With', 'XMLHttpRequest')
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.post(url.BUSINESS_CARDS_LIKE_ID.replace(':id', businessCardID))
            .expect(401)
            .set('X-Requested-With', 'XMLHttpRequest')
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.code, 'user.permission.notExist');
              testLike(verifyUserAccount._id, businessCardID, done, false);
            });
        });
    });
  });

  describe('## Bỏ thích danh thiếp', () => {
    beforeEach((done) => {
      agent.post(url.BUSINESS_CARDS_LIKE_ID.replace(':id', businessCardID))
        .expect(200)
        .set('X-Requested-With', 'XMLHttpRequest')
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          return done();
        });
    });

    it('should success unlike business card', (done) => {
      agent.put(url.BUSINESS_CARDS_LIKE_ID.replace(':id', businessCardID))
        .expect(200)
        .set('X-Requested-With', 'XMLHttpRequest')
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          testUnLike(verifyUserAccount._id, businessCardID, done);
        });
    });

    it('should failed unlike business card with exist unlike business card', (done) => {
      agent.put(url.BUSINESS_CARDS_LIKE_ID.replace(':id', businessCardID))
        .expect(200)
        .set('X-Requested-With', 'XMLHttpRequest')
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          testUnLike(verifyUserAccount._id, businessCardID);

          agent.put(url.BUSINESS_CARDS_LIKE_ID.replace(':id', businessCardID))
            .expect(404)
            .set('X-Requested-With', 'XMLHttpRequest')
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.code, 'businessCard.like.notExist');
              testUnLike(verifyUserAccount._id, businessCardID, done);
            });
        });
    });

    it('should failed unlike business card with bad id', (done) => {
      agent.put(url.BUSINESS_CARDS_LIKE_ID.replace(':id', mongoose.Types.ObjectId()))
        .expect(404)
        .set('X-Requested-With', 'XMLHttpRequest')
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          chai.assert.deepEqual(res.body.code, 'businessCard.like.notExist');
          testUnLike(verifyUserAccount._id, businessCardID, done, false);
        });
    });

    it('should failed unlike business card with not signin', (done) => {
      agent.get(usersUrl.AUTH_SIGNOUT)
        .expect(200)
        .set('X-Requested-With', 'XMLHttpRequest')
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.put(url.BUSINESS_CARDS_LIKE_ID.replace(':id', businessCardID))
            .expect(401)
            .set('X-Requested-With', 'XMLHttpRequest')
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.code, 'user.permission.notExist');
              testUnLike(verifyUserAccount._id, businessCardID, done, false);
            });
        });
    });
  });

  describe('## Nhận xét danh thiếp', () => {
    describe('# Thêm nhận xét danh thiếp', () => {
      it('should success comment business card', (done) => {
        agent.post(url.BUSINESS_CARDS_COMMENT_ID.replace(':id', businessCardID))
          .expect(200)
          .set('X-Requested-With', 'XMLHttpRequest')
          .send({
            comment,
          })
          .end((err, res) => {
            if (err) {
              return done(err);
            }

            chai.assert.exists(res.body.comment);
            commentID = res.body.comment.comment._id;

            testComment('create', commentID, businessCardID, comment, done);
          });
      });

      it('should success comment business card with user', (done) => {
        agent.get(usersUrl.AUTH_SIGNOUT)
          .expect(200)
          .set('X-Requested-With', 'XMLHttpRequest')
          .end((err, res) => {
            if (err) {
              return done(err);
            }

            utils.signin(agent, adminUser.numberPhone, password)
              .expect(200)
              .end((err, res) => {
                if (err) {
                  return done(err);
                }

                agent.post(url.BUSINESS_CARDS_COMMENT_ID.replace(':id', businessCardID))
                  .expect(200)
                  .set('X-Requested-With', 'XMLHttpRequest')
                  .send({
                    comment,
                  })
                  .end((err, res) => {
                    if (err) {
                      return done(err);
                    }

                    chai.assert.exists(res.body.comment);
                    commentID = res.body.comment.comment._id;

                    testComment('create', commentID, businessCardID, comment, done, true, false);
                  });
              });
          });
      });

      it('should success comment business card with exist comment business card', (done) => {
        agent.post(url.BUSINESS_CARDS_COMMENT_ID.replace(':id', businessCardID))
          .expect(200)
          .set('X-Requested-With', 'XMLHttpRequest')
          .send({
            comment,
          })
          .end((err, res) => {
            if (err) {
              return done(err);
            }

            chai.assert.exists(res.body.comment);
            commentID = res.body.comment.comment._id;

            testComment('create', commentID, businessCardID, comment);

            comment = 'comment_2';

            agent.post(url.BUSINESS_CARDS_COMMENT_ID.replace(':id', businessCardID))
              .expect(200)
              .set('X-Requested-With', 'XMLHttpRequest')
              .send({
                comment,
              })
              .end((err, res) => {
                if (err) {
                  return done(err);
                }

                chai.assert.exists(res.body.comment);
                commentID = res.body.comment.comment._id;

                testComment('create', commentID, businessCardID, comment, done);
              });
          });
      });

      it('should failed comment business card with bad id', (done) => {
        agent.post(url.BUSINESS_CARDS_COMMENT_ID.replace(':id', mongoose.Types.ObjectId()))
          .expect(404)
          .set('X-Requested-With', 'XMLHttpRequest')
          .send({
            comment,
          })
          .end((err, res) => {
            if (err) {
              return done(err);
            }

            chai.assert.deepEqual(res.body.code, 'businessCard.notExist');
            testComment('create', commentID, businessCardID, comment, done, false);
          });
      });

      it('should failed comment business card with bad body', (done) => {
        agent.post(url.BUSINESS_CARDS_COMMENT_ID.replace(':id', businessCardID))
          .expect(400)
          .set('X-Requested-With', 'XMLHttpRequest')
          .end((err, res) => {
            if (err) {
              return done(err);
            }

            chai.assert.deepEqual(res.body.code, 'data.notVaild');
            testComment('create', commentID, businessCardID, comment, done, false);
          });
      });

      it('should failed comment business card  with not signin', (done) => {
        agent.get(usersUrl.AUTH_SIGNOUT)
          .expect(200)
          .set('X-Requested-With', 'XMLHttpRequest')
          .end((err, res) => {
            if (err) {
              return done(err);
            }

            agent.post(url.BUSINESS_CARDS_COMMENT_ID.replace(':id', businessCardID))
              .expect(401)
              .set('X-Requested-With', 'XMLHttpRequest')
              .send({
                comment,
              })
              .end((err, res) => {
                if (err) {
                  return done(err);
                }

                chai.assert.deepEqual(res.body.code, 'user.permission.notExist');
                testComment('create', commentID, businessCardID, comment, done, false);
              });
          });
      });
    });

    describe('# Thay đổi nhận xét danh thiếp', () => {
      beforeEach((done) => {
        agent.post(url.BUSINESS_CARDS_COMMENT_ID.replace(':id', businessCardID))
          .expect(200)
          .set('X-Requested-With', 'XMLHttpRequest')
          .send({
            comment,
          })
          .end((err, res) => {
            if (err) {
              return done(err);
            }

            chai.assert.exists(res.body.comment);
            comment = 'comment_2';
            commentID = res.body.comment.comment._id;

            done();
          });
      });

      it('should success change comment business card', (done) => {
        agent.put(url.BUSINESS_CARDS_COMMENT_ID.replace(':id', businessCardID))
          .expect(200)
          .set('X-Requested-With', 'XMLHttpRequest')
          .send({
            comment,
          })
          .query({
            commentID,
          })
          .end((err, res) => {
            if (err) {
              return done(err);
            }

            chai.assert.exists(res.body.comment);
            testComment('change', commentID, businessCardID, comment, done);
          });
      });

      it('should success change comment business card with user', (done) => {
        agent.get(usersUrl.AUTH_SIGNOUT)
          .expect(200)
          .set('X-Requested-With', 'XMLHttpRequest')
          .end((err, res) => {
            if (err) {
              return done(err);
            }

            utils.signin(agent, adminUser.numberPhone, password)
              .expect(200)
              .end((err, res) => {
                if (err) {
                  return done(err);
                }

                agent.post(url.BUSINESS_CARDS_COMMENT_ID.replace(':id', businessCardID))
                  .expect(200)
                  .set('X-Requested-With', 'XMLHttpRequest')
                  .send({
                    comment,
                  })
                  .end((err, res) => {
                    if (err) {
                      return done(err);
                    }

                    chai.assert.exists(res.body.comment);
                    comment = 'comment_2';
                    commentID = res.body.comment.comment._id;

                    agent.put(url.BUSINESS_CARDS_COMMENT_ID.replace(':id', businessCardID))
                      .expect(200)
                      .set('X-Requested-With', 'XMLHttpRequest')
                      .send({
                        comment,
                      })
                      .query({
                        commentID,
                      })
                      .end((err, res) => {
                        if (err) {
                          return done(err);
                        }

                        chai.assert.exists(res.body.comment);
                        testComment('change', commentID, businessCardID, comment, done, true, false);
                      });
                  });
              });
          });
      });

      it('should success change comment business card with exist change comment business card', (done) => {
        agent.put(url.BUSINESS_CARDS_COMMENT_ID.replace(':id', businessCardID))
          .expect(200)
          .set('X-Requested-With', 'XMLHttpRequest')
          .send({
            comment,
          })
          .query({
            commentID,
          })
          .end((err, res) => {
            if (err) {
              return done(err);
            }

            chai.assert.exists(res.body.comment);

            testComment('change', commentID, businessCardID, comment);

            comment = 'comment_3';

            agent.put(url.BUSINESS_CARDS_COMMENT_ID.replace(':id', businessCardID))
              .expect(200)
              .set('X-Requested-With', 'XMLHttpRequest')
              .send({
                comment,
              })
              .query({
                commentID,
              })
              .end((err, res) => {
                if (err) {
                  return done(err);
                }

                chai.assert.exists(res.body.comment);
                testComment('change', commentID, businessCardID, comment, done);
              });
          });
      });

      it('should failed change comment business card with bad id', (done) => {
        agent.put(url.BUSINESS_CARDS_COMMENT_ID.replace(':id', mongoose.Types.ObjectId()))
          .expect(404)
          .set('X-Requested-With', 'XMLHttpRequest')
          .send({
            comment,
          })
          .query({
            commentID,
          })
          .end((err, res) => {
            if (err) {
              return done(err);
            }

            chai.assert.deepEqual(res.body.code, 'businessCard.comment.change.failed');
            return done();
          });
      });

      it('should failed change comment business card with bad body', (done) => {
        agent.put(url.BUSINESS_CARDS_COMMENT_ID.replace(':id', businessCardID))
          .expect(400)
          .set('X-Requested-With', 'XMLHttpRequest')
          .query({
            commentID,
          })
          .end((err, res) => {
            if (err) {
              return done(err);
            }

            chai.assert.deepEqual(res.body.code, 'data.notVaild');
            testComment('change', commentID, businessCardID, comment, done, false);
          });
      });

      it('should failed change comment business card with not signin', (done) => {
        agent.get(usersUrl.AUTH_SIGNOUT)
          .expect(200)
          .set('X-Requested-With', 'XMLHttpRequest')
          .end((err, res) => {
            if (err) {
              return done(err);
            }

            agent.put(url.BUSINESS_CARDS_COMMENT_ID.replace(':id', businessCardID))
              .expect(401)
              .set('X-Requested-With', 'XMLHttpRequest')
              .send({
                comment,
              })
              .query({
                commentID,
              })
              .end((err, res) => {
                if (err) {
                  return done(err);
                }

                chai.assert.deepEqual(res.body.code, 'user.permission.notExist');
                testComment('change', commentID, businessCardID, comment, done, false);
              });
          });
      });
    });

    describe('# Xoá nhận xét danh thiếp', () => {
      beforeEach((done) => {
        agent.post(url.BUSINESS_CARDS_COMMENT_ID.replace(':id', businessCardID))
          .expect(200)
          .set('X-Requested-With', 'XMLHttpRequest')
          .send({
            comment,
          })
          .end((err, res) => {
            if (err) {
              return done(err);
            }

            chai.assert.exists(res.body.comment);
            commentID = res.body.comment.comment._id;

            return done();
          });
      });

      it('should success delete comment business card', (done) => {
        agent.delete(url.BUSINESS_CARDS_COMMENT_ID.replace(':id', businessCardID))
          .expect(200)
          .set('X-Requested-With', 'XMLHttpRequest')
          .query({
            commentID,
          })
          .end((err, res) => {
            if (err) {
              return done(err);
            }

            testComment(constant.DELETE, verifyUserAccount._id, businessCardID, comment, done);
          });
      });

      it('should failed delete comment business card with exist delete comment business card', (done) => {
        agent.delete(url.BUSINESS_CARDS_COMMENT_ID.replace(':id', businessCardID))
          .expect(200)
          .set('X-Requested-With', 'XMLHttpRequest')
          .query({
            commentID,
          })
          .end((err, res) => {
            if (err) {
              return done(err);
            }

            testComment(constant.DELETE, verifyUserAccount._id, businessCardID, comment);

            agent.delete(url.BUSINESS_CARDS_COMMENT_ID.replace(':id', businessCardID))
              .expect(404)
              .set('X-Requested-With', 'XMLHttpRequest')
              .query({
                commentID,
              })
              .end((err, res) => {
                if (err) {
                  return done(err);
                }

                chai.assert.deepEqual(res.body.code, 'businessCard.comment.delete.failed');
                testComment(constant.DELETE, verifyUserAccount._id, businessCardID, comment, done);
              });
          });
      });

      it('should failed delete comment business card with bad id', (done) => {
        agent.delete(url.BUSINESS_CARDS_COMMENT_ID.replace(':id', mongoose.Types.ObjectId()))
          .expect(404)
          .set('X-Requested-With', 'XMLHttpRequest')
          .query({
            commentID,
          })
          .end((err, res) => {
            if (err) {
              return done(err);
            }

            chai.assert.deepEqual(res.body.code, 'businessCard.comment.delete.failed');
            testComment(constant.DELETE, verifyUserAccount._id, businessCardID, comment, done, false);
          });
      });

      it('should failed delete comment business card with not signin', (done) => {
        agent.get(usersUrl.AUTH_SIGNOUT)
          .expect(200)
          .set('X-Requested-With', 'XMLHttpRequest')
          .end((err, res) => {
            if (err) {
              return done(err);
            }

            agent.delete(url.BUSINESS_CARDS_COMMENT_ID.replace(':id', businessCardID))
              .expect(401)
              .set('X-Requested-With', 'XMLHttpRequest')
              .query({
                commentID,
              })
              .end((err, res) => {
                if (err) {
                  return done(err);
                }

                chai.assert.deepEqual(res.body.code, 'user.permission.notExist');
                testComment(constant.DELETE, verifyUserAccount._id, businessCardID, comment, done, false);
              });
          });
      });
    });
  });

  describe('## Tố cáo danh thiếp', () => {
    it('should success report business card', (done) => {
      agent.post(url.BUSINESS_CARDS_REPORT_ID.replace(':id', businessCardID))
        .expect(200)
        .set('X-Requested-With', 'XMLHttpRequest')
        .send({
          report: {
            reason: 'report',
          },
        })
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          testReport(verifyUserAccount._id, businessCardID, done);
        });
    });

    it('should failed report business card with exist report business card', (done) => {
      agent.post(url.BUSINESS_CARDS_REPORT_ID.replace(':id', businessCardID))
        .expect(200)
        .set('X-Requested-With', 'XMLHttpRequest')
        .send({
          report: {
            reason: 'report_1',
          },
        })
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.post(url.BUSINESS_CARDS_REPORT_ID.replace(':id', businessCardID))
            .expect(401)
            .set('X-Requested-With', 'XMLHttpRequest')
            .send({
              report: {
                reason: 'report_2',
              },
            })
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.code, 'businessCard.report.exist');
              testReport(verifyUserAccount._id, businessCardID, done);
            });
        });
    });

    it('should failed report business card with bad id', (done) => {
      agent.post(url.BUSINESS_CARDS_REPORT_ID.replace(':id', mongoose.Types.ObjectId()))
        .expect(404)
        .set('X-Requested-With', 'XMLHttpRequest')
        .send({
          report: {
            reason: 'report',
          },
        })
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          chai.assert.deepEqual(res.body.code, 'businessCard.notExist');
          testReport(verifyUserAccount._id, businessCardID, done, false);
        });
    });

    it('should failed report business card with not signin', (done) => {
      agent.get(usersUrl.AUTH_SIGNOUT)
        .expect(200)
        .set('X-Requested-With', 'XMLHttpRequest')
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.post(url.BUSINESS_CARDS_REPORT_ID.replace(':id', businessCardID))
            .expect(401)
            .set('X-Requested-With', 'XMLHttpRequest')
            .send({
              report: {
                reason: 'report',
              },
            })
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.code, 'user.permission.notExist');
              testReport(verifyUserAccount._id, businessCardID, done, false);
            });
        });
    });
  });

  describe('## Ảnh giao dịch danh thiếp', () => {
    describe('## Đăng ảnh giao dịch danh thiếp', () => {
      it('should success upload images deal business card', (done) => {
        agent.post(url.BUSINESS_CARDS_IMAGE)
          .expect(200)
          .set('X-Requested-With', 'XMLHttpRequest')
          .attach(constant.BACKGROUND, './modules/business-cards/tests/server/images/business-cards/background.png')
          .query({
            name: constant.BACKGROUND,
          })
          .end((err, res) => {
            if (err) {
              return done(err);
            }

            chai.assert.exists(res.body.image);
            testUploadImage(verifyUserAccount._id, businessCardID, 1, done);
          });
      });

      it('should success upload images business card with limit', (done) => {
        uploadImage(constant.BACKGROUND).then((resolve) => {
          chai.assert.exists(resolve.body.image);
          uploadImage(constant.FACE).then((resolve) => {
            chai.assert.exists(resolve.body.image);
            uploadImage(constant.BREAST).then((resolve) => {
              chai.assert.exists(resolve.body.image);
              uploadImage(constant.WAIST).then((resolve) => {
                chai.assert.exists(resolve.body.image);
                uploadImage(constant.HIPS).then((resolve) => {
                  chai.assert.exists(resolve.body.image);
                  uploadImage(constant.BODY).then((resolve) => {
                    chai.assert.exists(resolve.body.image);
                    testUploadImage(verifyUserAccount._id, businessCardID, 6, done);
                  }, (reason) => {
                    return done(reason);
                  });
                }, (reason) => {
                  return done(reason);
                });
              }, (reason) => {
                return done(reason);
              });
            }, (reason) => {
              return done(reason);
            });
          }, (reason) => {
            return done(reason);
          });
        }, (reason) => {
          return done(reason);
        });
      }).timeout(constant.ENUM.TIME.ONE_MINUTES * 2);

      it('should failed upload images business card with max limit', (done) => {
        uploadImage(constant.BACKGROUND).then((resolve) => {
          chai.assert.exists(resolve.body.image);
          uploadImage(constant.FACE).then((resolve) => {
            chai.assert.exists(resolve.body.image);
            uploadImage(constant.BREAST).then((resolve) => {
              chai.assert.exists(resolve.body.image);
              uploadImage(constant.WAIST).then((resolve) => {
                chai.assert.exists(resolve.body.image);
                uploadImage(constant.HIPS).then((resolve) => {
                  chai.assert.exists(resolve.body.image);
                  uploadImage(constant.BODY).then((resolve) => {
                    chai.assert.exists(resolve.body.image);
                    agent.post(url.BUSINESS_CARDS_IMAGE)
                      .expect(401)
                      .set('X-Requested-With', 'XMLHttpRequest')
                      .attach(constant.BODY, './modules/business-cards/tests/server/images/business-cards/body.png')
                      .query({
                        name: constant.BODY
                      })
                      .end((err, res) => {
                        if (err) {
                          return done(err);
                        }

                        chai.assert.deepEqual(res.body.code, 'businessCard.image.upload.failed');
                        testUploadImage(verifyUserAccount._id, businessCardID, 6, done);
                      });
                  }, (reason) => {
                    return done(reason);
                  });
                }, (reason) => {
                  return done(reason);
                });
              }, (reason) => {
                return done(reason);
              });
            }, (reason) => {
              return done(reason);
            });
          }, (reason) => {
            return done(reason);
          });
        }, (reason) => {
          return done(reason);
        });
      }).timeout(constant.ENUM.TIME.ONE_MINUTES);

      it('should failed upload images business card with not signin', (done) => {
        agent.get(usersUrl.AUTH_SIGNOUT)
          .expect(200)
          .set('X-Requested-With', 'XMLHttpRequest')
          .end((err, res) => {
            if (err) {
              return done(err);
            }

            agent.post(url.BUSINESS_CARDS_IMAGE)
              .expect(401)
              .set('X-Requested-With', 'XMLHttpRequest')
              .attach(constant.BACKGROUND, './modules/business-cards/tests/server/images/business-cards/background.png')
              .query({
                name: constant.BACKGROUND
              })
              .end((err, res) => {
                if (err) {
                  return done(err);
                }

                chai.assert.deepEqual(res.body.code, 'user.permission.notExist');
                testUploadImage(verifyUserAccount._id, businessCardID, 1, done, false);
              });
          });
      });

      it('should failed upload images business card with bad file', (done) => {
        agent.post(url.BUSINESS_CARDS_IMAGE)
          .expect(400)
          .set('X-Requested-With', 'XMLHttpRequest')
          .attach('some_background', './modules/business-cards/tests/server/images/business-cards/background.png')
          .query({
            name: 'some_background',
          })
          .end((err, res) => {
            if (err) {
              return done(err);
            }

            chai.assert.deepEqual(res.body.code, 'data.notVaild');
            testUploadImage(verifyUserAccount._id, businessCardID, 1, done, false);
          });
      });
    });

    describe('## Xoá ảnh giao dịch danh thiếp', () => {
      beforeEach((done) => {
        agent.post(url.BUSINESS_CARDS_IMAGE)
          .expect(200)
          .set('X-Requested-With', 'XMLHttpRequest')
          .attach(constant.BACKGROUND, './modules/business-cards/tests/server/images/business-cards/background.png')
          .query({
            name: constant.BACKGROUND,
          })
          .end((err, res) => {
            if (err) {
              return done(err);
            }

            chai.assert.exists(res.body.image);
            ({ publicID } = res.body.image);

            testUploadImage(verifyUserAccount._id, businessCardID, 1, done);
          });
      });

      it('should success delete images deal business card', (done) => {
        agent.delete(url.BUSINESS_CARDS_IMAGE)
          .expect(200)
          .set('X-Requested-With', 'XMLHttpRequest')
          .query({
            publicID,
          })
          .end((err, res) => {
            if (err) {
              return done(err);
            }

            chai.assert.exists(res.body.images);
            chai.assert.deepEqual(res.body.images.length, 0);

            testUploadImage(verifyUserAccount._id, businessCardID, 0, done, true, true);
          });
      });

      it('should failed delete images deal business card with bad publicID', (done) => {
        agent.delete(url.BUSINESS_CARDS_IMAGE)
          .expect(404)
          .set('X-Requested-With', 'XMLHttpRequest')
          .query({
            publicID: 'some_publicID',
          })
          .end((err, res) => {
            if (err) {
              return done(err);
            }

            chai.assert.deepEqual(res.body.code, 'businessCard.notExist');
            testUploadImage(verifyUserAccount._id, businessCardID, 0, done, false, true);
          });
      });

      it('should failed delete images deal business card with not signin', (done) => {
        agent.get(usersUrl.AUTH_SIGNOUT)
          .expect(200)
          .set('X-Requested-With', 'XMLHttpRequest')
          .end((err, res) => {
            if (err) {
              return done(err);
            }
            agent.delete(url.BUSINESS_CARDS_IMAGE)
              .expect(401)
              .set('X-Requested-With', 'XMLHttpRequest')
              .query({
                publicID,
              })
              .end((err, res) => {
                if (err) {
                  return done(err);
                }

                chai.assert.deepEqual(res.body.code, 'user.permission.notExist');
                testUploadImage(verifyUserAccount._id, businessCardID, 0, done, false, true);
              });
          });
      });
    });
  });

  describe('## Giao dịch danh thiếp', () => {
    it('should success deal business card', (done) => {
      agent.get(usersUrl.AUTH_SIGNOUT)
        .expect(200)
        .set('X-Requested-With', 'XMLHttpRequest')
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          utils.signin(agent, adminUser.numberPhone, password)
            .expect(200)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              agent.post(url.BUSINESS_CARDS_DEAL_ID.replace(':id', businessCardID))
                .expect(200)
                .set('X-Requested-With', 'XMLHttpRequest')
                .send({
                  costs: {
                    room: {
                      value: constant.ONE_TIME,
                      moneys: 50,
                    },
                    service: {
                      value: constant.ONE_SLOT,
                      moneys: 150,
                    },
                    currencyUnit: constant.DOMESTIC,
                  },
                  note: 'note',
                  timer: regular.businessCardDealHistory.expires.timer + constant.ENUM.TIME.ONE_MINUTES,
                })
                .end((err, res) => {
                  if (err) {
                    return done(err);
                  }

                  chai.assert.exists(res.body.businessCardDealHistoryID);

                  testDeal(adminUser._id, done);
                });
            });
        });
    });

    it('should failed deal business card with business card self', (done) => {
      agent.post(url.BUSINESS_CARDS_DEAL_ID.replace(':id', businessCardID))
        .expect(401)
        .set('X-Requested-With', 'XMLHttpRequest')
        .send({
          costs: {
            room: {
              value: constant.ONE_TIME,
              moneys: 50,
            },
            service: {
              value: constant.ONE_SLOT,
              moneys: 150,
            },
            currencyUnit: constant.DOMESTIC,
          },
          note: 'note',
          timer: regular.businessCardDealHistory.expires.timer + constant.ENUM.TIME.ONE_MINUTES,
        })
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          chai.assert.deepEqual(res.body.code, 'user.permission.notExist');
          testDeal(adminUser._id, done, false);
        });
    });

    it('should failed deal business card with business card self admin', (done) => {
      agent.post(url.BUSINESS_CARDS_DEAL_ID.replace(':id', businessCardID))
        .expect(401)
        .set('X-Requested-With', 'XMLHttpRequest')
        .send({
          costs: {
            room: {
              value: constant.ONE_TIME,
              moneys: 50,
            },
            service: {
              value: constant.ONE_SLOT,
              moneys: 150,
            },
            currencyUnit: constant.DOMESTIC,
          },
          note: 'note',
          timer: regular.businessCardDealHistory.expires.timer + constant.ENUM.TIME.ONE_MINUTES,
        })
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          chai.assert.deepEqual(res.body.code, 'user.permission.notExist');
          testDeal(adminUser._id, done, false);
        });
    });

    it('should failed deal business card with exist deal business card', (done) => {
      agent.get(usersUrl.AUTH_SIGNOUT)
        .expect(200)
        .set('X-Requested-With', 'XMLHttpRequest')
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          utils.signin(agent, adminUser.numberPhone, password)
            .expect(200)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              agent.post(url.BUSINESS_CARDS_DEAL_ID.replace(':id', businessCardID))
                .expect(200)
                .set('X-Requested-With', 'XMLHttpRequest')
                .send({
                  costs: {
                    room: {
                      value: constant.ONE_TIME,
                      moneys: 50,
                    },
                    service: {
                      value: constant.ONE_SLOT,
                      moneys: 150,
                    },
                    currencyUnit: constant.DOMESTIC,
                  },
                  note: 'note',
                  timer: regular.businessCardDealHistory.expires.timer + constant.ENUM.TIME.ONE_MINUTES,
                })
                .end((err, res) => {
                  if (err) {
                    return done(err);
                  }

                  agent.post(url.BUSINESS_CARDS_DEAL_ID.replace(':id', businessCardID))
                    .expect(401)
                    .set('X-Requested-With', 'XMLHttpRequest')
                    .send({
                      costs: {
                        room: {
                          value: constant.ONE_TIME,
                          moneys: 50,
                        },
                        service: {
                          value: constant.ONE_SLOT,
                          moneys: 150,
                        },
                        currencyUnit: constant.DOMESTIC,
                      },
                      note: 'note',
                      timer: regular.businessCardDealHistory.expires.timer + constant.ENUM.TIME.ONE_MINUTES,
                    })
                    .end((err, res) => {
                      if (err) {
                        return done(err);
                      }

                      chai.assert.deepEqual(res.body.code, 'businessCardDealHistory.exist');
                      testDeal(adminUser._id, done);
                    });
                });
            });
        });
    });

    it('should failed deal business card with not signin', (done) => {
      agent.get(usersUrl.AUTH_SIGNOUT)
        .expect(200)
        .set('X-Requested-With', 'XMLHttpRequest')
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.post(url.BUSINESS_CARDS_DEAL_ID.replace(':id', businessCardID))
            .expect(401)
            .set('X-Requested-With', 'XMLHttpRequest')
            .send({
              costs: {
                room: {
                  value: constant.ONE_TIME,
                  moneys: 50,
                },
                service: {
                  value: constant.ONE_SLOT,
                  moneys: 150,
                },
                currencyUnit: constant.DOMESTIC,
              },
              note: 'note',
              timer: regular.businessCardDealHistory.expires.timer + constant.ENUM.TIME.ONE_MINUTES,
            })
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.code, 'user.permission.notExist');
              testDeal(adminUser._id, done, false);
            });
        });
    });
  });

  describe('## Tìm kiếm danh thiếp', () => {
    it('should success extend search business card', (done) => {
      agent.get(usersUrl.AUTH_SIGNOUT)
        .expect(200)
        .set('X-Requested-With', 'XMLHttpRequest')
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          utils.signin(agent, adminUser.numberPhone, password)
            .expect(200)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              agent.post(url.BUSINESS_CARDS)
                .set('X-Requested-With', 'XMLHttpRequest')
                .send(businessCard)
                .expect(200)
                .end((err, res) => {
                  if (err) {
                    return done(err);
                  }

                  chai.assert.exists(res.body.businessCard);
                  agent.get(url.BUSINESS_CARDS_SEARCH)
                    .expect(200)
                    .set('X-Requested-With', 'XMLHttpRequest')
                    .query({
                      skip: 0,
                      type: constant.EXTEND,
                      extendSearch: {
                        blurFace: true,
                        // star: 1,
                        costs: {
                          room: {
                            min: 1,
                            max: 50,
                            values: [constant.ONE_TIME],
                          },
                          service: {
                            min: 1,
                            max: 150,
                            values: [constant.ONE_SLOT],
                          },
                          currencyUnit: constant.DOMESTIC,
                        },
                        address: {
                          country: 'country',
                          city: 'city',
                          county: 'county',
                        },
                      },
                    })
                    .end((err, res) => {
                      if (err) {
                        return done(err);
                      }

                      chai.assert.deepEqual(res.body.skip, 2);
                      chai.assert.deepEqual(res.body.businessCards.topActivity.length, 2);
                      chai.assert.deepEqual(res.body.businessCards.topNewUp.length, 2);
                      chai.assert.deepEqual(res.body.businessCards.topRank.length, 2);

                      return done();
                    });
                });
            });
        });
    });

    it('should failed search business card with not signin user', (done) => {
      agent.get(usersUrl.AUTH_SIGNOUT)
        .expect(200)
        .set('X-Requested-With', 'XMLHttpRequest')
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.get(url.BUSINESS_CARDS_SEARCH)
            .expect(401)
            .set('X-Requested-With', 'XMLHttpRequest')
            .query({
              skip: 0,
              type: constant.EXTEND,
              extendSearch: {
                blurFace: true,
                // star: 1,
                costs: {
                  room: {
                    min: 0,
                    max: 50,
                    value: [constant.ONE_TIME],
                  },
                  service: {
                    min: 0,
                    max: 150,
                    value: [constant.ONE_SLOT],
                  },
                  currencyUnit: constant.DOMESTIC,
                },
                address: {
                  country: 'country',
                  city: 'city',
                  county: 'county',
                },
              },
            })
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.code, 'user.permission.notExist');
              return done();
            });
        });
    });

    it('should failed search business card with bad body', (done) => {
      agent.get(usersUrl.AUTH_SIGNOUT)
        .expect(200)
        .set('X-Requested-With', 'XMLHttpRequest')
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          utils.signin(agent, adminUser.numberPhone, password)
            .expect(200)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              agent.post(url.BUSINESS_CARDS)
                .set('X-Requested-With', 'XMLHttpRequest')
                .send(businessCard)
                .expect(200)
                .end((err, res) => {
                  if (err) {
                    return done(err);
                  }

                  chai.assert.exists(res.body.businessCard);
                  agent.get(url.BUSINESS_CARDS_SEARCH)
                    .expect(400)
                    .set('X-Requested-With', 'XMLHttpRequest')
                    .query({
                      type: constant.EXTEND,
                    })
                    .end((err, res) => {
                      if (err) {
                        return done(err);
                      }

                      chai.assert.deepEqual(res.body.code, 'data.notVaild');
                      return done();
                    });
                });
            });
        });
    });

    it('should success auto search business card', (done) => {
      agent.get(usersUrl.AUTH_SIGNOUT)
        .expect(200)
        .set('X-Requested-With', 'XMLHttpRequest')
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          utils.signin(agent, adminUser.numberPhone, password)
            .expect(200)
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              agent.post(url.BUSINESS_CARDS)
                .set('X-Requested-With', 'XMLHttpRequest')
                .send(businessCard)
                .expect(200)
                .end((err, res) => {
                  if (err) {
                    return done(err);
                  }

                  chai.assert.exists(res.body.businessCard);
                  agent.get(url.BUSINESS_CARDS_SEARCH)
                    .expect(200)
                    .set('X-Requested-With', 'XMLHttpRequest')
                    .query({
                      skip: 0,
                      type: constant.AUTO,
                    })
                    .end((err, res) => {
                      if (err) {
                        return done(err);
                      }

                      chai.assert.deepEqual(res.body.skip, 2);
                      chai.assert.deepEqual(res.body.businessCards.topActivity.length, 2);
                      chai.assert.deepEqual(res.body.businessCards.topNewUp.length, 2);
                      chai.assert.deepEqual(res.body.businessCards.topRank.length, 2);

                      return done();
                    });
                });
            });
        });
    });

    it('should failed auto search business card with not signin user', (done) => {
      agent.get(usersUrl.AUTH_SIGNOUT)
        .expect(200)
        .set('X-Requested-With', 'XMLHttpRequest')
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          agent.get(url.BUSINESS_CARDS_SEARCH)
            .expect(401)
            .set('X-Requested-With', 'XMLHttpRequest')
            .query({
              skip: 0,
              type: constant.AUTO,
            })
            .end((err, res) => {
              if (err) {
                return done(err);
              }

              chai.assert.deepEqual(res.body.code, 'user.permission.notExist');
              return done();
            });
        });
    });
  });
});
