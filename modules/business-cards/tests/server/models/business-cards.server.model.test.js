const mongoose = require('mongoose');
const chai = require('chai');
const path = require('path');
const constant = require(path.resolve('./commons/utils/constant'));

describe('### Business Cards Server Model Tests', () => {
  let BusinessCard = null;
  let businessCard = null;

  before((done) => {
    BusinessCard = mongoose.model('BusinessCard');

    done();
  });

  beforeEach((done) => {
    businessCard = {
      title: 'title',
      descriptions: 'descriptions',
      blurFace: false,
      measurements3: {
        breast: 0,
        waist: 1,
        hips: 2,
      },
      address: {
        country: 'country',
        city: 'city',
        county: 'county',
        local: 'local',
      },
      costs: {
        rooms: [{
          value: constant.ONE_TIME,
          moneys: 50,
        }, {
          value: constant.OVER_NIGHT,
          moneys: 150,
        }],
        services: [{
          value: constant.ONE_SLOT,
          moneys: 200,
        }, {
          value: constant.OVER_NIGHT,
          moneys: 250,
        }, {
          value: constant.SOME,
          moneys: 300,
        }, {
          value: constant.SOME_OVER_NIGHT,
          moneys: 350,
        }],
      },
    };

    done();
  });

  afterEach((done) => {
    BusinessCard.deleteMany({}, (err) => {
      chai.assert.notExists(err);
      done();
    });
  });

  describe('## Tạo danh thiếp', () => {
    it('Should success new business cards', (done) => {
      businessCard = new BusinessCard(BusinessCard.getDefaultValue(mongoose.Types.ObjectId(), constant.FEMALE, businessCard));
      businessCard.save((err) => {
        chai.assert.notExists(err);
        done();
      });
    });
  });

  describe('## Trạng thái danh thiếp', () => {
    it('Should success change off (false) status business cards', (done) => {
      businessCard = new BusinessCard(BusinessCard.getDefaultValue(mongoose.Types.ObjectId(), constant.FEMALE, businessCard));
      businessCard.save((err) => {
        chai.assert.notExists(err);

        businessCard.setStatus(false);
        businessCard.save((err) => {
          chai.assert.notExists(err);

          chai.assert.isFalse(businessCard.status.is);
          done();
        });
      });
    });
  });

  describe('## Thêm tố cáo danh thiếp', () => {
    it('Should success add report business cards', (done) => {
      businessCard = new BusinessCard(BusinessCard.getDefaultValue(mongoose.Types.ObjectId(), constant.FEMALE, businessCard));
      businessCard.save((err) => {
        chai.assert.notExists(err);

        businessCard.addReport(mongoose.Types.ObjectId(), 'reason');
        businessCard.save((err) => {
          chai.assert.notExists(err);

          chai.assert.deepEqual(businessCard.reports.length, 1);
          chai.assert.deepEqual(businessCard.points, 0);

          done();
        });
      });
    });
  });

  describe('## Thêm nhận xét danh thiếp', () => {
    it('Should success add comment business cards', (done) => {
      businessCard = new BusinessCard(BusinessCard.getDefaultValue(mongoose.Types.ObjectId(), constant.FEMALE, businessCard));
      businessCard.save((err) => {
        chai.assert.notExists(err);

        businessCard.addComment(mongoose.Types.ObjectId(), 'msg');
        businessCard.save((err) => {
          chai.assert.notExists(err);

          chai.assert.deepEqual(businessCard.comments.length, 1);
          chai.assert.deepEqual(businessCard.points, 0.2);

          done();
        });
      });
    });
  });

  describe('## Thêm ghé thăm danh thiếp', () => {
    it('Should success add views business cards', (done) => {
      businessCard = new BusinessCard(BusinessCard.getDefaultValue(mongoose.Types.ObjectId(), constant.FEMALE, businessCard));
      businessCard.save((err) => {
        chai.assert.notExists(err);

        businessCard.addView(mongoose.Types.ObjectId());
        businessCard.save((err) => {
          chai.assert.notExists(err);

          chai.assert.deepEqual(businessCard.views.length, 1);
          chai.assert.deepEqual(businessCard.points, 0.1);

          done();
        });
      });
    });
  });

  describe('## Điểm danh thiếp', () => {
    it('Should success plus point success business cards', (done) => {
      businessCard = new BusinessCard(BusinessCard.getDefaultValue(mongoose.Types.ObjectId(), constant.FEMALE, businessCard));
      businessCard.save((err) => {
        chai.assert.notExists(err);

        businessCard.plusPointSuccess();
        businessCard.save((err) => {
          chai.assert.notExists(err);

          chai.assert.deepEqual(businessCard.points, 40);
          done();
        });
      });
    });

    it('Should success minus point failed business cards', (done) => {
      businessCard = new BusinessCard(BusinessCard.getDefaultValue(mongoose.Types.ObjectId(), constant.FEMALE, businessCard));
      businessCard.save((err) => {
        chai.assert.notExists(err);

        businessCard.minusPointFailed();
        businessCard.save((err) => {
          console.log(err);
          chai.assert.notExists(err);

          chai.assert.deepEqual(businessCard.points, -50);
          done();
        });
      });
    });
  });

  describe('## Giá cả danh thiếp', () => {
    it('Should success test cost validate business cards', (done) => {
      businessCard = new BusinessCard(BusinessCard.getDefaultValue(mongoose.Types.ObjectId(), constant.FEMALE, businessCard));
      businessCard.save((err) => {
        chai.assert.notExists(err);

        const testCostValidate = businessCard.testCostValidate({
          room: {
            value: constant.ONE_TIME,
            moneys: 50,
          },
          service: {
            value: constant.ONE_SLOT,
            moneys: 200,
          },
          currencyUnit: constant.DOMESTIC,
        });

        chai.assert.isTrue(testCostValidate);
        done();
      });
    });

    it('Should failed test cost validate business cards with bad value', (done) => {
      businessCard = new BusinessCard(BusinessCard.getDefaultValue(mongoose.Types.ObjectId(), constant.FEMALE, businessCard));
      businessCard.save((err) => {
        chai.assert.notExists(err);

        const testCostValidate = businessCard.testCostValidate({
          room: {
            value: 'some_value',
            moneys: 50,
          },
          service: {
            value: constant.ONE_SLOT,
            moneys: 200,
          },
          currencyUnit: constant.DOMESTIC,
        });

        chai.assert.isFalse(testCostValidate);
        done();
      });
    });

    it('Should failed test cost validate business cards with bad moneys', (done) => {
      businessCard = new BusinessCard(BusinessCard.getDefaultValue(mongoose.Types.ObjectId(), constant.FEMALE, businessCard));
      businessCard.save((err) => {
        chai.assert.notExists(err);

        const testCostValidate = businessCard.testCostValidate({
          room: {
            value: constant.ONE_TIME,
            moneys: -1,
          },
          service: {
            value: constant.ONE_SLOT,
            moneys: 200,
          },
          currencyUnit: constant.DOMESTIC,
        });

        chai.assert.isFalse(testCostValidate);
        done();
      });
    });

    it('Should failed test cost validate business cards with bad currencyUnit', (done) => {
      businessCard = new BusinessCard(BusinessCard.getDefaultValue(mongoose.Types.ObjectId(), constant.FEMALE, businessCard));
      businessCard.save((err) => {
        chai.assert.notExists(err);

        const testCostValidate = businessCard.testCostValidate({
          room: {
            value: constant.ONE_TIME,
            moneys: 50,
          },
          service: {
            value: constant.ONE_SLOT,
            moneys: 200,
          },
          currencyUnit: 'some_currencyUnit',
        });

        chai.assert.isFalse(testCostValidate);
        done();
      });
    });
  });

  describe('## Điểm và Sao danh thiếp', () => {
    it('Should success get star business cards. With poinst < 1 star', (done) => {
      businessCard = new BusinessCard(BusinessCard.getDefaultValue(mongoose.Types.ObjectId(), constant.FEMALE, businessCard));
      businessCard.points = 30 * 50 - 1;
      businessCard.save((err) => {
        chai.assert.notExists(err);

        const star = businessCard.getStar();
        chai.assert.deepEqual(star, 0);

        done();
      });
    });

    it('Should success get star business cards. With poinst > 5 star', (done) => {
      businessCard = new BusinessCard(BusinessCard.getDefaultValue(mongoose.Types.ObjectId(), constant.FEMALE, businessCard));
      businessCard.points = 30 * 19200 + 1;
      businessCard.save((err) => {
        chai.assert.notExists(err);

        const star = businessCard.getStar();
        chai.assert.deepEqual(star, 5);

        done();
      });
    });
  });
});
