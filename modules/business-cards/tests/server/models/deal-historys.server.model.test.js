const mongoose = require('mongoose');
const chai = require('chai');
const path = require('path');
const constant = require(path.resolve('./commons/utils/constant'));
const regular = require(path.resolve('./commons/utils/regular-expression'));

describe('### Deal Historys Server Model Tests', () => {
  let BusinessCardDealHistory = null;
  let businessCardDealHistory = null;

  const userID = mongoose.Types.ObjectId();
  const providerID = mongoose.Types.ObjectId();
  const businessCardID = mongoose.Types.ObjectId();

  before((done) => {
    BusinessCardDealHistory = mongoose.model('BusinessCardDealHistory');
    done();
  });

  beforeEach((done) => {
    businessCardDealHistory = {
      userID,
      providerID,
      businessCardID,
      address: {
        country: 'country',
        city: 'city',
        county: 'county',
        local: 'local',
      },
      costs: {
        room: {
          value: constant.ONE_TIME,
          moneys: 50,
        },
        service: {
          value: constant.ONE_SLOT,
          moneys: 200,
        },
        currencyUnit: constant.DOMESTIC,
      },
      timer: regular.businessCardDealHistory.expires.timer * constant.ENUM.TIME.ONE_MINUTES,
    };

    done();
  });

  afterEach((done) => {
    BusinessCardDealHistory.deleteMany({}, (err) => {
      chai.assert.notExists(err);
      done();
    });
  });

  describe('## Tạo lịch sử danh thiếp', () => {
    it('Should success new business card deal history', (done) => {
      businessCardDealHistory = new BusinessCardDealHistory(businessCardDealHistory);
      businessCardDealHistory.save((err) => {
        chai.assert.notExists(err);
        done();
      });
    });
  });

  describe('## Thời gian lịch sử danh thiếp', () => {
    it('Should success expires business card deal history', (done) => {
      businessCardDealHistory = new BusinessCardDealHistory(businessCardDealHistory);
      businessCardDealHistory.timer = Date.now();
      businessCardDealHistory.save((err) => {
        chai.assert.notExists(err);

        chai.assert.isTrue(businessCardDealHistory.checkExpires());

        done();
      });
    });
  });

  describe('## Chấp nhận lịch sử danh thiếp', () => {
    it('Should success validate accept business card deal history with validate accept true business card deal history with provider', (done) => {
      businessCardDealHistory = new BusinessCardDealHistory(businessCardDealHistory);
      businessCardDealHistory.validateAccept(providerID, true);
      businessCardDealHistory.save((err) => {
        chai.assert.notExists(err);

        chai.assert.isTrue(businessCardDealHistory.accept.provider.is);
        chai.assert.deepEqual(businessCardDealHistory.status, constant.TOKEN);

        done();
      });
    });

    it('Should success validate accept business card deal history with validate accept false business card deal history with provider', (done) => {
      businessCardDealHistory = new BusinessCardDealHistory(businessCardDealHistory);
      businessCardDealHistory.validateAccept(providerID, false, 'reason');
      businessCardDealHistory.save((err) => {
        chai.assert.notExists(err);

        chai.assert.isFalse(businessCardDealHistory.accept.provider.is);
        chai.assert.deepEqual(businessCardDealHistory.accept.provider.reason, 'reason');
        chai.assert.deepEqual(businessCardDealHistory.result.is, constant.FAILED);

        done();
      });
    });

    it('Should success validate accept business card deal history with validate accept false business card deal history with user', (done) => {
      businessCardDealHistory = new BusinessCardDealHistory(businessCardDealHistory);
      businessCardDealHistory.validateAccept(userID, false, 'reason');
      businessCardDealHistory.save((err) => {
        chai.assert.notExists(err);

        chai.assert.isFalse(businessCardDealHistory.accept.user.is);
        chai.assert.deepEqual(businessCardDealHistory.accept.user.reason, 'reason');
        chai.assert.deepEqual(businessCardDealHistory.result.is, constant.FAILED);

        done();
      });
    });

    it('Should failed validate accept business card deal history with validate accept false business card deal history with bad ID', (done) => {
      businessCardDealHistory = new BusinessCardDealHistory(businessCardDealHistory);
      businessCardDealHistory.validateAccept(mongoose.Types.ObjectId(), false, 'reason');
      businessCardDealHistory.save((err) => {
        chai.assert.notExists(err);

        chai.assert.isTrue(businessCardDealHistory.accept.user.is);
        chai.assert.deepEqual(businessCardDealHistory.result.is, constant.WAIT);
        chai.assert.deepEqual(businessCardDealHistory.status, constant.RESPONSE);

        done();
      });
    });
  });

  describe('## Thêm tố cáo lịch sử danh thiếp', () => {
    it('Should success add report business card deal history with add report business card deal history with provider', (done) => {
      businessCardDealHistory = new BusinessCardDealHistory(businessCardDealHistory);
      businessCardDealHistory.addReport(providerID, 'reason');
      businessCardDealHistory.save((err) => {
        chai.assert.notExists(err);

        chai.assert.isTrue(businessCardDealHistory.reports.provider.is);
        chai.assert.deepEqual(businessCardDealHistory.reports.provider.reason, 'reason');

        done();
      });
    });

    it('Should success add report business card deal history with add report business card deal history with user', (done) => {
      businessCardDealHistory = new BusinessCardDealHistory(businessCardDealHistory);
      businessCardDealHistory.addReport(userID, 'reason');
      businessCardDealHistory.save((err) => {
        chai.assert.notExists(err);

        chai.assert.isTrue(businessCardDealHistory.reports.user.is);
        chai.assert.deepEqual(businessCardDealHistory.reports.user.reason, 'reason');

        done();
      });
    });

    it('Should failed add report business card deal history with add report business card deal history with bad ID', (done) => {
      businessCardDealHistory = new BusinessCardDealHistory(businessCardDealHistory);
      businessCardDealHistory.addReport(mongoose.Types.ObjectId(), 'reason');
      businessCardDealHistory.save((err) => {
        chai.assert.notExists(err);

        chai.assert.isFalse(businessCardDealHistory.reports.user.is);
        chai.assert.isFalse(businessCardDealHistory.reports.provider.is);

        done();
      });
    });
  });

  describe('## Tạo mã thông báo lịch sử danh thiếp', () => {
    it('Should success create token business card deal history with create token business card deal history with provider', (done) => {
      businessCardDealHistory = new BusinessCardDealHistory(businessCardDealHistory);
      businessCardDealHistory.status = constant.TOKEN;
      businessCardDealHistory.save((err) => {
        chai.assert.notExists(err);

        businessCardDealHistory
          .createToken()
          .then((token) => {
            businessCardDealHistory.save((err) => {
              chai.assert.notExists(err);

              chai.assert.deepEqual(businessCardDealHistory.token, token);
              chai.assert.deepEqual(businessCardDealHistory.status, constant.VERIFY);

              done();
            });
          }, (err) => {
            done(err);
          });
      });
    });

    it('Should failed create token business card deal history with create token business card deal history with bad status', (done) => {
      businessCardDealHistory = new BusinessCardDealHistory(businessCardDealHistory);
      businessCardDealHistory
        .createToken()
        .then((token) => {
          done(token);
        }, (err) => {
          done();
        });
    });

    it('Should failed create token business card deal history with create token business card deal history with bad body', (done) => {
      businessCardDealHistory = new BusinessCardDealHistory(businessCardDealHistory);
      businessCardDealHistory.status = constant.TOKEN;
      businessCardDealHistory.save((err) => {
        chai.assert.notExists(err);

        businessCardDealHistory
          .createToken()
          .then((token) => {
            businessCardDealHistory.save((err) => {
              chai.assert.notExists(err);

              chai.assert.deepEqual(businessCardDealHistory.token, token);
              chai.assert.deepEqual(businessCardDealHistory.status, constant.VERIFY);

              businessCardDealHistory
                .createToken()
                .then((token) => {
                  chai.assert.deepEqual(businessCardDealHistory.status, constant.TOKEN);
                }, (err) => {
                  chai.assert.deepEqual(businessCardDealHistory.status, constant.VERIFY);
                  done();
                });
            });
          }, (err) => {
            done(err);
          });
      });
    });
  });

  describe('## Xác thực mã thông báo lịch sử danh thiếp', () => {
    let token = null;

    beforeEach((done) => {
      businessCardDealHistory = new BusinessCardDealHistory(businessCardDealHistory);
      businessCardDealHistory.status = constant.TOKEN;
      businessCardDealHistory.save((err) => {
        chai.assert.notExists(err);

        businessCardDealHistory
          .createToken()
          .then((resToken) => {
            token = resToken;

            businessCardDealHistory.save((err) => {
              chai.assert.notExists(err);

              chai.assert.deepEqual(businessCardDealHistory.token, token);
              chai.assert.deepEqual(businessCardDealHistory.status, constant.VERIFY);

              done();
            });
          }, (err) => {
            done(err);
          });
      });
    });

    it('Should success validate token business card deal history with validate token business card deal history with provider', (done) => {
      businessCardDealHistory.timer = Date.now() + regular.businessCardDealHistory.expires.token - constant.ENUM.TIME.ONE_MINUTES;
      businessCardDealHistory.save((err) => {
        chai.assert.notExists(err);

        chai.assert.isTrue(businessCardDealHistory.validateToken(token));

        businessCardDealHistory.save((err) => {
          chai.assert.notExists(err);

          chai.assert.deepEqual(businessCardDealHistory.result.is, constant.SUCCESS);
          done();
        });
      });
    });

    it('Should failed validate token business card deal history with validate token business card deal history with bad timer', (done) => {
      businessCardDealHistory.timer = Date.now() + regular.businessCardDealHistory.expires.token + constant.ENUM.TIME.ONE_MINUTES;
      businessCardDealHistory.save((err) => {
        chai.assert.notExists(err);

        chai.assert.isFalse(businessCardDealHistory.validateToken(token));

        businessCardDealHistory.save((err) => {
          chai.assert.notExists(err);

          chai.assert.deepEqual(businessCardDealHistory.result.is, constant.WAIT);
          done();
        });
      });
    });

    it('Should failed validate token business card deal history with validate token business card deal history with bad timer', (done) => {
      businessCardDealHistory.timer = Date.now() - regular.businessCardDealHistory.expires.token;
      businessCardDealHistory.save((err) => {
        chai.assert.notExists(err);

        chai.assert.isFalse(businessCardDealHistory.validateToken(token));

        businessCardDealHistory.save((err) => {
          chai.assert.notExists(err);

          chai.assert.deepEqual(businessCardDealHistory.result.is, constant.WAIT);
          done();
        });
      });
    });
  });
});
