
const path = require('path');
const policy = require('../policies/business-cards.server.policy');
const businessCards = require('../controllers/business-cards.server.controller');
const validate = require(path.resolve('./commons/modules/business-cards/validates/business-cards.validate'));
const middleware = require(path.resolve('./utils/middleware'));
const url = require(path.resolve('./commons/modules/business-cards/datas/url.data'));

const init = (app) => {
  app.route(url.BUSINESS_CARDS_SEARCH)
    .all(policy.isAllowed)
    .get(middleware.paramsValidation(validate, 'search'), businessCards.search);
  app.route(url.BUSINESS_CARDS_GET_ID)
    .all(policy.isAllowed)
    .get(middleware.paramsValidation(validate, 'get'), businessCards.get);

  app.route(url.BUSINESS_CARDS)
    .all(policy.isAllowed)
    .get(businessCards.getSelf)
    .post(middleware.paramsValidation(validate, 'create'), businessCards.create)
    .delete(middleware.paramsValidation(validate, 'deletee'), businessCards.deletee);
  app.route(url.BUSINESS_CARDS_STATUS)
    .all(policy.isAllowed)
    .post(businessCards.changeStatus);
  app.route(url.BUSINESS_CARDS_IMAGE)
    .all(policy.isAllowed)
    .post(middleware.paramsValidation(validate, 'uploadImage'), businessCards.uploadImage)
    .delete(middleware.paramsValidation(validate, 'deleteImage'), businessCards.deleteImage);
  app.route(url.BUSINESS_CARDS_ADDRESS)
    .all(policy.isAllowed)
    .post(middleware.paramsValidation(validate, 'changeAddress'), businessCards.changeAddress);
  app.route(url.BUSINESS_CARDS_DESCRIPTIONS)
    .all(policy.isAllowed)
    .post(middleware.paramsValidation(validate, 'changeDescriptions'), businessCards.changeDescriptions);
  app.route(url.BUSINESS_CARDS_REUPLOAD)
    .all(policy.isAllowed)
    .post(businessCards.reUpload);

  app.route(url.BUSINESS_CARDS_REPORT_ID)
    .all(policy.isAllowed)
    .post(middleware.paramsValidation(validate, 'report'), businessCards.report);
  app.route(url.BUSINESS_CARDS_LIKE_ID)
    .all(policy.isAllowed)
    .post(middleware.paramsValidation(validate, 'like'), businessCards.like)
    .put(middleware.paramsValidation(validate, 'unLike'), businessCards.unLike);
  app.route(url.BUSINESS_CARDS_COMMENT_ID)
    .all(policy.isAllowed)
    .get(middleware.paramsValidation(validate, 'comments'), businessCards.comments)
    .post(middleware.paramsValidation(validate, 'comment'), businessCards.comment)
    .put(middleware.paramsValidation(validate, 'changeComment'), businessCards.changeComment)
    .delete(middleware.paramsValidation(validate, 'deleteComment'), businessCards.deleteComment);
  app.route(url.BUSINESS_CARDS_DEAL_ID)
    .all(policy.isAllowed)
    .post(middleware.paramsValidation(validate, 'deal'), businessCards.deal);
};
module.exports = init;
