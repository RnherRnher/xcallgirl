const path = require('path');
const policy = require('../policies/deal-historys.server.policy');
const dealHistorys = require('../controllers/deal-historys.server.controller');
const validate = require(path.resolve('./commons/modules/business-cards/validates/deal-historys.validate'));
const middleware = require(path.resolve('./utils/middleware'));
const url = require(path.resolve('./commons/modules/business-cards/datas/url.data'));

const init = (app) => {
  app.route(url.BUSINESS_CARDS_DEAL_HISTORYS_SEARCH)
    .all(policy.isAllowed)
    .get(middleware.paramsValidation(validate, 'search'), dealHistorys.search);
  app.route(url.BUSINESS_CARDS_DEAL_HISTORYS_GET_ID)
    .all(policy.isAllowed)
    .get(middleware.paramsValidation(validate, 'get'), dealHistorys.get);

  app.route(url.BUSINESS_CARDS_DEAL_HISTORYS_ACCEPT_ID)
    .all(policy.isAllowed)
    .post(middleware.paramsValidation(validate, 'accept'), dealHistorys.accept);
  app.route(url.BUSINESS_CARDS_DEAL_HISTORYS_TOKEN_ID)
    .all(policy.isAllowed)
    .get(middleware.paramsValidation(validate, 'getToken'), dealHistorys.getToken)
    .post(middleware.paramsValidation(validate, 'validateToken'), dealHistorys.validateToken);
  app.route(url.BUSINESS_CARDS_DEAL_HISTORYS_REPORT_ID)
    .all(policy.isAllowed)
    .post(middleware.paramsValidation(validate, 'report'), dealHistorys.report);
};
module.exports = init;
