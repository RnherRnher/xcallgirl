const crypto = require('crypto');
const path = require('path');
const chalk = require('chalk');
const mongoose = require('mongoose');
const regular = require(path.resolve('./commons/utils/regular-expression'));
const constant = require(path.resolve('./commons/utils/constant'));
const { validateModelBusinessCardDealHistory } = require(path.resolve('./utils/validate'));

const { Schema } = mongoose;

const validates = validateModelBusinessCardDealHistory();

const BusinessCardDealHistorySchema = new Schema({
  userID: {
    type: mongoose.SchemaTypes.ObjectId,
    required: true,
  },
  providerID: {
    type: mongoose.SchemaTypes.ObjectId,
    required: true,
  },
  businessCardID: {
    type: mongoose.SchemaTypes.ObjectId,
    required: true,
  },
  timer: {
    type: Date,
    required: true,
  },
  accept: {
    user: {
      is: {
        type: Boolean,
        required: true,
        default: true,
      },
      date: {
        type: Date,
        required: true,
        default: Date.now
      },
      reason: {
        type: String,
        trim: true,
        maxlength: regular.commons.stringLength.large,
      },
    },
    provider: {
      is: {
        type: Boolean,
      },
      date: {
        type: Date,
      },
      reason: {
        type: String,
        trim: true,
        maxlength: regular.commons.stringLength.large,
      },
    },
  },
  result: {
    is: {
      type: String,
      required: true,
      enum: validates.result,
      default: constant.WAIT,
    },
    date: {
      type: Date,
      required: true,
      default: Date.now
    },
  },
  status: {
    type: String,
    required: true,
    enum: regular.businessCardDealHistory.status,
    default: constant.RESPONSE,
  },
  reports: {
    user: {
      is: {
        type: Boolean,
        required: true,
        default: false,
      },
      date: {
        type: Date
      },
      reason: {
        type: String,
        trim: true,
        maxlength: regular.commons.stringLength.large,
      },
    },
    provider: {
      is: {
        type: Boolean,
        required: true,
        default: false,
      },
      date: {
        type: Date
      },
      reason: {
        type: String,
        trim: true,
        maxlength: regular.commons.stringLength.large,
      },
    },
  },
  costs: {
    room: {
      value: {
        type: String,
        enum: regular.businessCard.costs.rooms,
      },
      moneys: {
        type: Number,
        min: regular.commons.moneys.min,
        max: regular.commons.moneys.max,
      },
    },
    service: {
      value: {
        type: String,
        enum: regular.businessCard.costs.services,
      },
      moneys: {
        type: Number,
        required: true,
        min: regular.commons.moneys.min,
        max: regular.commons.moneys.max,
      },
    },
    currencyUnit: {
      type: String,
      required: true,
      enum: regular.commons.moneys.currencyUnit,
    },
  },
  token: {
    type: String,
    unique: true,
    sparse: true,
    maxlength: regular.commons.stringLength.medium,
  },
  address: {
    country: {
      type: String,
      required: true,
      maxlength: regular.commons.stringLength.medium,
    },
    city: {
      type: String,
      required: true,
      maxlength: regular.commons.stringLength.medium,
    },
    county: {
      type: String,
      required: true,
      maxlength: regular.commons.stringLength.medium,
    },
    local: {
      type: String,
      required: true,
      maxlength: regular.commons.stringLength.medium,
    },
  },
  note: {
    type: String,
    trim: true,
    maxlength: regular.commons.stringLength.medium,
  },
  initDate: {
    type: Date,
    required: true,
    default: Date.now
  },
});

/**
 * @overview Kiểm tra hết hạn lịch sử giao dịch danh thiếp
 *
 * @returns {Boolean}
 */
BusinessCardDealHistorySchema.methods.checkExpires = function () {
  if (
    this.result.is === constant.WAIT
    && this.timer.getTime() < Date.now()
  ) {
    this.result.is = constant.FAILED;
    this.result.date = Date.now();

    return true;
  }

  return false;
};

/**
 * @overview Chấp nhận lịch sử giao dịch danh thiếp
 *
 * @param     {ObjectId} userID
 * @param     {Boolean} is
 * @param     {String?} reason
 * @returns   {Boolean}
 */
BusinessCardDealHistorySchema.methods.validateAccept = function (userID, is, reason) {
  let isExist = false;

  if (userID.equals(this.userID)) {
    this.accept.user.is = is;
    this.accept.user.date = Date.now();
    this.accept.user.reason = reason;
    isExist = true;
  } else if (userID.equals(this.providerID)) {
    this.accept.provider.is = is;
    this.accept.provider.date = Date.now();
    isExist = true;

    if (!is) {
      this.accept.provider.reason = reason;
    }
  }

  if (isExist && !is) {
    this.result.is = constant.FAILED;
    this.result.date = Date.now();
    return false;
  }

  if (this.accept.provider.is && this.accept.user.is) {
    this.status = constant.TOKEN;
    return true;
  }
};

/**
 * @overview Thêm tố cáo lịch sử giao dịch danh thiếp
 *
 * @param {ObjectId} userID
 * @param {String?} reason
 * @returns {Boolean}
 */
BusinessCardDealHistorySchema.methods.addReport = function (userID, reason) {
  if (userID.equals(this.userID)) {
    if (this.reports.user.is) {
      return false;
    }

    this.reports.user.is = true;
    this.reports.user.date = Date.now();
    this.reports.user.reason = reason;
  } else if (userID.equals(this.providerID)) {
    if (this.reports.provider.is) {
      return false;
    }

    this.reports.provider.is = true;
    this.reports.provider.date = Date.now();
    this.reports.provider.reason = reason;
  }

  return true;
};

/**
 * @overview Tạo mã thông báo lihcj sử giao dịch danh thiếp
 *
 * @param     {Boolean?} overwrite
 * @returns   {Promise}
 */
BusinessCardDealHistorySchema.methods.createToken = function (overwrite) {
  return new Promise((resolve, reject) => {
    if (this.status === constant.TOKEN) {
      crypto.randomBytes(4, (err, buffer) => {
        if (err) {
          return reject(err);
        }

        this.token = buffer.toString('base64').replace(/=/g, '').replace(/\+/g, '').replace(/\//g, '');
        this.status = constant.VERIFY;
        return resolve(this.token);
      });
    } else if (this.status === constant.VERIFY && this.token && !overwrite) {
      return reject(new Error('Exist token'));
    } else {
      return reject(new Error('Not permission'));
    }
  });
};

/**
 * @overview Xác thực mã thông báo lịch sử giao dịch danh thiếp
 *
 * @param     {String} token
 * @returns   {Boolean}
 */
BusinessCardDealHistorySchema.methods.validateToken = function (token) {
  if (this.status === constant.VERIFY && this.token === token
    && this.timer.getTime() > Date.now()
    && this.timer.getTime() < Date.now() + regular.businessCardDealHistory.expires.token
  ) {
    this.result.is = constant.SUCCESS;
    this.result.date = Date.now();

    return true;
  }

  return false;
};

/**
 * @overview Lấy thông tin tóm tắt lịch sử giao dịch danh thiếp
 * @returns {BusinessCardDealHistory}
 */
BusinessCardDealHistorySchema.methods.getShortcutsInfo = function () {
  return {
    _id: this._id,
    result: this.result,
    status: this.status,
    initDate: this.initDate,
  };
};

/**
 * @overview Xác thực quyền lịch sử giao dịch danh thiếp
 * @param     {User} user
 * @returns   {Boolean}
 */
BusinessCardDealHistorySchema.methods.authenticate = function (user) {
  const User = mongoose.model('User');

  if (user.getAuthorizedLevelByRoles() >= User.getAuthorizedLevelByRoles(constant.ADMIN)
    || (user.getAuthorizedLevelByRoles() === User.getAuthorizedLevelByRoles(constant.PROVIDER) && user.equalsByID(this.providerID))
    || (user.getAuthorizedLevelByRoles() === User.getAuthorizedLevelByRoles(constant.USER) && user.equalsByID(this.userID))
  ) {
    return true;
  }

  return false;
};

/**
 * @overview Lấy thông tin đầy đủ lịch sử giao dịch danh thiếp
 * @param     {User} user
 * @returns   {BusinessCardDealHistory}
 */
BusinessCardDealHistorySchema.methods.getInfoByAuthorizedLevel = function (user) {
  const _this = this.toObject();

  _this.chatID = undefined;
  _this.roles = constant.ADMIN;

  if (user.equalsByID(_this.userID)) {
    _this.isReport = _this.reports.user.is;
    _this.reports = undefined;
    _this.roles = constant.USER;
  } else if (user.equalsByID(_this.providerID)) {
    _this.isReport = _this.reports.provider.is;
    _this.reports = undefined;
    _this.reports = undefined;
    _this.roles = constant.PROVIDER;

    _this.token = undefined;
  }

  return _this;
};

/**
 * @overview Tạo mẫu cơ sở dữ liệu
 *
 * @param   {Object} doc
 * @param   {overwrite: Boolean} options
 * @returns {Promise}
 */
BusinessCardDealHistorySchema.statics.seed = function (doc, options) {
  const BusinessCardDealHistory = mongoose.model('BusinessCardDealHistory');

  return new Promise((resolve, reject) => {
    function skipDocument() {
      return new Promise((resolve, reject) => {
        BusinessCardDealHistory.findOne({ userID: doc.userID })
          .exec((err, existing) => {
            if (err) {
              return reject(err);
            }

            if (!existing) {
              return resolve(false);
            }

            if (existing && !options.overwrite) {
              return resolve(true);
            }

            // Remove BusinessCardDealHistory (overwrite)

            existing.remove((err) => {
              if (err) {
                return reject(err);
              }

              return resolve(false);
            });
          });
      });
    }

    function add(skip) {
      return new Promise((resolve, reject) => {
        if (skip) {
          return resolve({
            message: chalk.yellow(`Database Seeding: BusinessCardDealHistory\t\t id: ${doc.businessCardID} skipped`),
          });
        }

        const businessCardDealHistory = new BusinessCardDealHistory(doc);
        businessCardDealHistory.save((err) => {
          if (err) {
            return reject(err);
          }

          return resolve({
            message: `Database Seeding: BusinessCardDealHistory\t\t id: ${businessCardDealHistory.businessCardID} added`,
          });
        });
      });
    }

    skipDocument()
      .then(add)
      .then((response) => {
        return resolve(response);
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

/**
 * @overview Lấy giá trị mặt định để tạo BusinessCardDealHistory
 *
 * @param   {ObjetcId} userID
 * @param   {BusinessCard} businessCard
 * @param   {Object} data
 * @returns {{}||{BusinessCardDealHistory}}
 */
BusinessCardDealHistorySchema.statics.getDefaultValue = function (userID, businessCard, data) {
  if (
    businessCard.testCostValidate(data.costs)
    && data.timer >= regular.businessCardDealHistory.expires.timer
  ) {
    const currentDate = Date.now();

    return {
      userID,
      providerID: businessCard.userID,
      businessCardID: businessCard._id,
      address: businessCard.address,
      note: data.note,
      costs: data.costs,
      timer: currentDate + data.timer,
      initDate: currentDate
    };
  }

  return {};
};

mongoose.model('BusinessCardDealHistory', BusinessCardDealHistorySchema);
