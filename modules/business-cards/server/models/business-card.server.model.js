const async = require('async');
const _ = require('lodash');
const chalk = require('chalk');
const path = require('path');
const mongoose = require('mongoose');
const regular = require(path.resolve('./commons/utils/regular-expression'));
const constant = require(path.resolve('./commons/utils/constant'));

const { Schema } = mongoose;

const defaultValues = {
  points: {
    comment: 0.2,
    like: 10,
    view: 0.1,
    deal: {
      success: 40,
      failed: -50,
    },
  },
  star: {
    0: 30 * 50,
    0.5: 30 * 100,
    1: 30 * 150,
    1.5: 30 * 300,
    2: 30 * 600,
    2.5: 30 * 1200,
    3: 30 * 2400,
    3.5: 30 * 4800,
    4: 30 * 9600,
    4.5: 30 * 19200,
  },
};

const BusinessCardSchema = new Schema({
  gender: {
    type: String,
    required: true,
    enum: regular.user.gender,
  },
  userID: {
    type: mongoose.SchemaTypes.ObjectId,
    required: true,
    unique: true,
  },
  points: {
    type: Number,
    required: true,
    default: 0,
  },
  pointsDay: {
    type: Number,
    required: true,
    min: 0,
    default: 0,
  },
  blurFace: {
    type: Boolean,
    required: true,
    default: true,
  },
  status: {
    is: {
      type: Boolean,
      required: true,
      default: true,
    },
    date: {
      type: Date,
      required: true,
      default: Date.now
    },
  },
  costs: {
    rooms: {
      type: [{
        value: {
          type: String,
          enum: regular.businessCard.costs.rooms,
        },
        moneys: {
          type: Number,
          min: regular.commons.moneys.min,
          max: regular.commons.moneys.max,
        },
      }],
    },
    services: {
      type: [{
        value: {
          type: String,
          required: true,
          enum: regular.businessCard.costs.services,
        },
        moneys: {
          type: Number,
          required: true,
          min: regular.commons.moneys.min,
          max: regular.commons.moneys.max,
        },
      }],
      validate: [(services) => {
        return services.length > 0;
      }]
    },
    currencyUnit: {
      type: String,
      required: true,
      enum: regular.commons.moneys.currencyUnit,
      default: constant.DOMESTIC,
    },
  },
  images: {
    type: [{
      name: {
        type: String,
        required: true,
        enum: regular.businessCard.image.names,
      },
      url: {
        type: String,
        required: true,
        unique: true,
        sparse: true,
        maxlength: regular.commons.stringLength.medium,
        match: regular.app.urlCloudinary,
      },
      publicID: {
        type: String,
        required: true,
        unique: true,
        sparse: true,
        maxlength: regular.commons.stringLength.medium,
      },
    }],
    maxlength: regular.businessCard.image.max,
  },
  measurements3: {
    breast: {
      type: Number,
      required: true,
      min: 0,
    },
    waist: {
      type: Number,
      required: true,
      min: 0,
    },
    hips: {
      type: Number,
      required: true,
      min: 0,
    },
  },
  title: {
    type: String,
    required: true,
    trim: true,
    maxlength: regular.commons.stringLength.medium,
  },
  descriptions: {
    type: String,
    required: true,
    trim: true,
    maxlength: regular.commons.stringLength.large,
  },
  address: {
    country: {
      type: String,
      required: true,
      maxlength: regular.commons.stringLength.medium,
    },
    city: {
      type: String,
      required: true,
      maxlength: regular.commons.stringLength.medium,
    },
    county: {
      type: String,
      required: true,
      maxlength: regular.commons.stringLength.medium,
    },
    local: {
      type: String,
      required: true,
      maxlength: regular.commons.stringLength.medium,
    },
  },
  likes: {
    type: [{
      userID: {
        type: mongoose.SchemaTypes.ObjectId,
        required: true,
      },
      initDate: {
        type: Date,
        required: true,
        default: Date.now
      },
    }],
  },
  comments: {
    type: [{
      _id: {
        type: mongoose.SchemaTypes.ObjectId,
        required: true,
        unique: true,
        sparse: true,
      },
      userID: {
        type: mongoose.SchemaTypes.ObjectId,
        required: true,
      },
      msg: {
        type: String,
        required: true,
        trim: true,
        maxlength: regular.commons.stringLength.medium,
      },
      change: {
        is: {
          type: Boolean,
          required: true,
          default: false,
        },
        date: {
          type: Date,
        },
      },
      initDate: {
        type: Date,
        required: true,
        default: Date.now
      },
    }],
  },
  views: {
    type: [{
      userID: {
        type: mongoose.SchemaTypes.ObjectId,
        required: true,
      },
      initDate: {
        type: Date,
        required: true,
        default: Date.now
      },
    }],
  },
  reports: {
    type: [{
      userID: {
        type: mongoose.SchemaTypes.ObjectId,
        required: true,
      },
      reason: {
        type: String,
        required: true,
        trim: true,
        maxlength: regular.commons.stringLength.large,
      },
      adminID: {
        type: mongoose.SchemaTypes.ObjectId,
      },
      verify: {
        is: {
          type: Boolean,
          required: true,
          default: false,
        },
        confirmDate: {
          type: Date,
        },
      },
      initDate: {
        type: Date,
        required: true,
        default: Date.now
      },
    }],
  },
  initDate: {
    type: Date,
    required: true,
    default: Date.now
  },
  updateDate: {
    type: Date,
    required: true,
    default: Date.now
  },
  reUpload: {
    is: {
      type: Boolean,
      required: true,
      default: false,
    },
    count: {
      type: Number,
      min: 0,
      required: true,
      default: regular.businessCard.reUpload.count
    },
    date: {
      type: Date,
      required: true,
      default: Date.now
    },
  }
});

/**
 * @overview Cộng điểm danh thiếp
 *
 * @param {Number}
 */
BusinessCardSchema.methods.plusPoints = function (points) {
  this.points += points;

  if (points > 0) {
    this.pointsDay += points;
  }
};

/**
 * @overview Thêm tố cáo danh thiếp
 *
 * @param    {ObjectId} userID
 * @param    {String} reason
 * @returns  {Object}
 */
BusinessCardSchema.methods.addReport = function (userID, reason) {
  const dataReport = {
    userID,
    reason
  };

  this.reports.push(dataReport);

  return dataReport;
};

/**
 * @overview Thêm nhận xét danh thiếp
 *
 * @param    {ObjectId} userID
 * @param    {String} msg
 * @returns  {Object}
 */
BusinessCardSchema.methods.addComment = function (userID, msg) {
  const dataComment = {
    _id: mongoose.Types.ObjectId(),
    userID,
    msg,
    change: {
      is: false,
    },
    initDate: Date.now()
  };

  this.plusPoints(defaultValues.points.comment);
  this.comments.push(dataComment);

  return dataComment;
};

/**
 * @overview Thêm thích danh thiếp
 *
 * @param    {ObjectId} userID
 * @returns  {Object}
 */
BusinessCardSchema.methods.addLike = function (userID) {
  const dataLike = { userID };

  this.plusPoints(defaultValues.points.like);
  this.likes.push(dataLike);

  return dataLike;
};

/**
 * @overview Trừ điểm khi bỏ thích danh thiếp
 *
 */
BusinessCardSchema.methods.unLike = function () {
  this.plusPoints(-defaultValues.points.like);
};

/**
 * @overview Thêm thăm danh thiếp
 *
 * @param    {ObjectId} userID
 * @returns  {Object}
 */
BusinessCardSchema.methods.addView = function (userID) {
  const dataView = { userID };

  this.plusPoints(defaultValues.points.view);
  this.views.push(dataView);

  return dataView;
};

/**
 * @overview Đăng lại danh thiếp
 *
 * @returns  {Boolean}
 */
BusinessCardSchema.methods.setReUpload = function () {
  if (this.reUpload.count) {
    this.reUpload.is = true;
    this.reUpload.count -= 1;
    this.reUpload.date = Date.now();

    return true;
  }

  return false;
};

/**
 * @overview Thiết lập trạng thái danh thiếp
 *
 * @param    {Boolean} isStatus
 * @returns  {Object}
 */
BusinessCardSchema.methods.setStatus = function (isStatus) {
  this.status.is = isStatus;
  this.status.date = Date.now();

  return this.status;
};

/**
 * @overview Cộng điểm khi hoàn thành lịch sử giao dịch danh thiếp
 *
 * @returns {Number}
 */
BusinessCardSchema.methods.plusPointSuccess = function () {
  this.plusPoints(defaultValues.points.deal.success);

  return this.points;
};

/**
 * @overview Trừ điểm khi thất bại lịch sử giao dịch danh thiếp
 *
 * @returns {Number}
 */
BusinessCardSchema.methods.minusPointFailed = function () {
  this.plusPoints(defaultValues.points.deal.failed);

  return this.points;
};

/**
 * @overview Kiểm tra số lượng ảnh tối đa được tải lên danh thiếp
 *
 * @returns {Number}
 */
BusinessCardSchema.methods.checkImagesLimit = function () {
  return this.images.length === regular.businessCard.image.max;
};

/**
 * @overview Thêm phụ kiện danh thiếp
 *
 * @param   {Object} item
 * @returns {Boolean}
 */
BusinessCardSchema.methods.addItem = function (item, value) {
  switch (item.itemID) {
    case '0':
      this.reUpload.count += value;
      break;
    default:
      return false;
  }

  return true;
};

/**
 * @overview Lấy số lượng sao đạt được với số điểm hiện có danh thiếp
 *
 * @returns {Number}
 */
BusinessCardSchema.methods.getStar = function () {
  let { star } = defaultValues;
  const { points } = this;

  if (star['0'] > points) {
    star = 0;
  } else if (star['0'] <= points && points < star['0.5']) {
    star = 0.5;
  } else if (star['0.5'] <= points && points < star['1']) {
    star = 1;
  } else if (star['1'] <= points && points < star['1.5']) {
    star = 1.5;
  } else if (star['1.5'] <= points && points < star['2']) {
    star = 2;
  } else if (star['2'] <= points && points < star['2.5']) {
    star = 2.5;
  } else if (star['2.5'] <= points && points < star['3']) {
    star = 3;
  } else if (star['3'] <= points && points < star['3.5']) {
    star = 3.5;
  } else if (star['3.5'] <= points && points < star['4']) {
    star = 4;
  } else if (star['4'] <= points && points < star['4.5']) {
    star = 4.5;
  } else {
    star = 5;
  }

  return star;
};

/**
 * @overview Lấy hình ảnh đại diện cho danh thiếp
 *
 * @param   {String} name
 * @returns  {Object}
 */
BusinessCardSchema.methods.getBackground = function (name = regular.businessCard.image.names[0]) {
  return this.images.length
    ? this.images[_.findIndex(this.images, (image) => {
      return image.name === name;
    })].url
    || this.images[0].url
    : undefined;
};

/**
 * @overview Lấy thông tim tóm tắt danh thiếp
 *
 * @returns {Object}
 */
BusinessCardSchema.methods.getShortcutsInfo = function (userID) {
  return new Promise((resolve, reject) => {
    const _this = this.toObject();

    _this.points = undefined;
    _this.pointsDay = undefined;
    _this.updateDate = undefined;
    _this.reUpload = undefined;
    _this.status = undefined;
    _this.measurements3 = undefined;
    _this.reports = undefined;
    _this.star = this.getStar();
    _this.likes = {
      isSelf: false,
      count: _this.likes.length,
      data: []
    };
    _this.comments = {
      count: _this.comments.length,
      data: []
    };
    _this.views = {
      count: _this.views.length,
      data: []
    };

    async.map(this.likes, (like, done) => {
      if (like.userID.equals(userID)) {
        _this.likes.isSelf = true;
        done(new Error('Exist like'));
      } else {
        done(null);
      }
    }, (err, result) => {
      return resolve(_this);
    });
  });
};

/**
 * @overview Lấy thông tim đầy đủ danh thiếp
 *
 * @returns {Object}
 */
BusinessCardSchema.methods.getFullInfo = function (userID) {
  return this.getShortcutsInfo(userID)
    .then((_this) => {
      const isOwner = this.userID.equals(userID);

      return _.assignIn(
        _this,
        {
          // points: isOwner ? this.points : undefined,
          // pointsDay: isOwner ? this.pointsDay : undefined,
          reUpload: isOwner ? this.reUpload : undefined,
          updateDate: this.updateDate,
          status: this.status,
          measurements3: this.measurements3,
          address: this.address,
        }
      );
    });
};

/**
 * @overview Kiểm tra giá trị cost room có phù hợp với giá trị costs room trong danh thiếp hay không
 *
 * @param    {Object} room
 * @returns  {Boolean}
 */
BusinessCardSchema.methods.testRoomValidate = function (room) {
  if (!_.isEmpty(this.costs.rooms)) {
    return _.some(this.costs.rooms, room);
  }

  return true;
};

/**
 * @overview Kiểm tra giá trị cost service có phù hợp với giá trị costs service trong danh thiếp hay không
 *
 * @param   {Object} service
 * @returns  {Boolean}
 */
BusinessCardSchema.methods.testServiceValidate = function (service) {
  return _.some(this.costs.services, service);
};

/**
 * @overview Kiểm tra giá trị cost currencyUnit có phù hợp với giá trị costs currencyUnit trong danh thiếp hay không
 *
 * @param   {Object} currencyUnit
 * @returns  {Boolean}
 */
BusinessCardSchema.methods.testCurrencyUnitValidate = function (currencyUnit) {
  return this.costs.currencyUnit === currencyUnit;
};

/**
 * @overview Kiểm tra giá trị cost có phù hợp với giá trị costs trong danh thiếp hay không
 *
 * @param   {Object} currencyUnit
 * @returns  {Boolean}
 */
BusinessCardSchema.methods.testCostValidate = function (cost) {
  if (this.testRoomValidate(cost.room)
    && this.testServiceValidate(cost.service)
    && this.testCurrencyUnitValidate(cost.currencyUnit)) {
    return true;
  }

  return false;
};

/**
 * @overview Tạo mẫu cơ sở dữ liệu
 *
 * @param   {Object} doc
 * @param   {overwrite: Boolean} options
 * @returns {Promise}
 */
BusinessCardSchema.statics.seed = function (doc, options) {
  const BusinessCard = mongoose.model('BusinessCard');

  return new Promise((resolve, reject) => {
    function skipDocument() {
      return new Promise((resolve, reject) => {
        BusinessCard.findOne({ userID: doc.userID })
          .exec((err, existing) => {
            if (err) {
              return reject(err);
            }

            if (!existing) {
              return resolve(false);
            }

            if (existing && !options.overwrite) {
              return resolve(true);
            }

            // Remove BusinessCard (overwrite)

            existing.remove((err) => {
              if (err) {
                return reject(err);
              }

              return resolve(false);
            });
          });
      });
    }

    function add(skip) {
      return new Promise((resolve, reject) => {
        if (skip) {
          return resolve({
            message: chalk.yellow(`Database Seeding: BusinessCard\t\t title: ${doc.title} skipped`),
          });
        }

        const businessCard = new BusinessCard(doc);
        businessCard.save((err) => {
          if (err) {
            return reject(err);
          }

          return resolve({
            message: `Database Seeding: BusinessCard\t\t title: ${businessCard.title} added`,
          });
        });
      });
    }

    skipDocument()
      .then(add)
      .then((response) => {
        return resolve(response);
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

/**
 * @overview Lấy giá trị mặt định để tạo BusinessCardDealHistory
 *
 * @param   {ObjetcId} userID
 * @param   {String} gender
 * @param   {Object} data
 * @returns  {BusinessCard}
 */
BusinessCardSchema.statics.getDefaultValue = function (userID, gender, data) {
  return {
    gender,
    userID,
    title: data.title,
    descriptions: data.descriptions,
    points: data.blurFace ? defaultValues.star['0'] : 0,
    blurFace: data.blurFace,
    costs: data.costs,
    measurements3: data.measurements3,
    address: data.address,
  };
};

/**
 * @overview Lấy giá trị truy vấn số điểm của danh thiếp từ số sao
 *
 * @param   {Number} star
 * @returns  {Object}
 */
BusinessCardSchema.statics.getQueryPoints = function (star) {
  switch (star) {
    case 0:
      return {
        $lt: defaultValues.star[0],
      };
    case 0.5:
      return {
        $gte: defaultValues.star['0'],
        $lt: defaultValues.star['0.5']
      };
    case 1:
      return {
        $gte: defaultValues.star['0.5'],
        $lt: defaultValues.star['1'],
      };
    case 1.5:
      return {
        $gte: defaultValues.star['1'],
        $lt: defaultValues.star['1.5'],
      };
    case 2:
      return {
        $gte: defaultValues.star['1.5'],
        $lt: defaultValues.star['2'],
      };
    case 2.5:
      return {
        $gte: defaultValues.star['2'],
        $lt: defaultValues.star['2.5'],
      };
    case 3:
      return {
        $gte: defaultValues.star['2.5'],
        $lt: defaultValues.star['3'],
      };
    case 3.5:
      return {
        $gte: defaultValues.star['3'],
        $lt: defaultValues.star['3.5'],
      };
    case 4:
      return {
        $gte: defaultValues.star['3.5'],
        $lt: defaultValues.star['4'],
      };
    case 4.5:
      return {
        $gte: defaultValues.star['4'],
        $lt: defaultValues.star['4.5'],
      };
    case 5:
      return {
        $gte: defaultValues.star['4.5']
      };
    default:
      return {
        $lt: defaultValues.star['0.5']
      };
  }
};

mongoose.model('BusinessCard', BusinessCardSchema);
