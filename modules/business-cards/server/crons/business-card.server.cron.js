const path = require('path');
const cron = require('cron');
const mongoose = require('mongoose');
const logger = require(path.resolve('./config/lib/logger'));
const constant = require(path.resolve('./commons/utils/constant'));
const regular = require(path.resolve('./commons/utils/regular-expression'));

const BusinessCard = mongoose.model('BusinessCard');

const startUpdate = () => {
  const resetPointsDay = () => {
    BusinessCard.updateMany(
      {},
      {
        $set: {
          pointsDay: 0,
          'reUpload.is': false,
          'reUpload.date': Date.now()
        }
      }
    ).exec((err, businessCards) => {
      if (err) {
        logger.logError(
          'business-card.server.cron',
          'resetPointsDay',
          constant.BUSINESS_CARD,
          businessCards,
          err
        );
      }
    });
  };

  const resetNewUp = () => {
    BusinessCard.updateMany(
      {
        'reUpload.count': 0
      },
      {
        $set: {
          'reUpload.count':
            regular
              .businessCard
              .reUpload
              .count
        }
      }
    ).exec((err, businessCards) => {
      if (err) {
        logger.logError(
          'business-card.server.cron',
          'resetNewUp',
          constant.BUSINESS_CARD,
          businessCards,
          err
        );
      }
    });
  };

  const onTick = () => {
    resetPointsDay();
    resetNewUp();
  };

  const onComplete = () => {
  };

  return new cron.CronJob('0 */24 * * *', onTick, onComplete, true);
};

const init = (app) => {
  startUpdate();
};
module.exports = init;
