const path = require('path');
const mongoose = require('mongoose');
const async = require('async');
const cloudinary = require(path.resolve('config/lib/cloudinary'));
const multer = require(path.resolve('config/lib/multer'));
const logger = require(path.resolve('./config/lib/logger'));
const constant = require(path.resolve('./commons/utils/constant'));

const User = mongoose.model('User');
const BusinessCard = mongoose.model('BusinessCard');
const ActivityHistory = mongoose.model('ActivityHistory');

/**
 * @overview Đăng lại
 *
 * @param {Request} req
 * @param {Response} res
 * @param {Function} next
 * @param {Function} getInfo
 *
 * @input  { }
 * @output {
 *           code: String,
 *           reUpload?: {Object},
 *         }
 */
const reUpload = (req, res, next) => {
  const userSelf = req.user;
  let dataRes = null;

  async.waterfall([
    (done) => {
      /* Kiểm tra đầu vào */

      if (userSelf) {
        BusinessCard
          .findOne({ userID: userSelf._id })
          .exec((err, businessCard) => {
            if (err || !businessCard) {
              // Danh thiếp không tồn tại
              dataRes = {
                status: 404,
                bodyRes: {
                  code: 'businessCard.notExist',
                },
              };

              done(err || new Error('Not business card exists'), dataRes);
            } else {
              done(null, businessCard);
            }
          });
      } else {
        // Chưa đăng nhập người dùng
        dataRes = {
          status: 401,
          bodyRes: {
            code: 'user.signin.notExist',
          },
        };

        done(new Error('User not signin'), dataRes);
      }
    }, (businessCard, done) => {
      /* Cập nhật danh thiếp */

      if (businessCard.setReUpload()) {
        businessCard.save((err) => {
          if (err) {
            // Đăng lại danh thiếp thất bại
            dataRes = {
              status: 400,
              bodyRes: {
                code: 'businessCard.reUpload.failed',
              },
            };
          } else {
            const activityHistory = new ActivityHistory({
              userID: userSelf._id,
              toURL: businessCard._id,
              type: constant.BUSINESS_CARD,
              result: constant.SUCCESS,
              content: 'businessCard.reUpload.is',
            });

            activityHistory.save((err) => {
              if (err) {
                logger.logError(
                  'business-cards.profile.server.controller',
                  'reUpload',
                  constant.ACTIVITY_HISTORY,
                  activityHistory,
                  err
                );
              }
            });

            dataRes = {
              status: 200,
              bodyRes: {
                code: 'businessCard.reUpload.success',
                reUpload: businessCard.reUpload,
              },
            };
          }

          done(err, dataRes);
        });
      } else {
        // Đăng lại danh thiếp thất bại
        dataRes = {
          status: 401,
          bodyRes: {
            code: 'businessCard.reUpload.failed',
          },
        };

        done(new Error('Not count reUpload business card'), dataRes);
      }
    }], (err, dataRes) => {
      return res
        .status(dataRes.status)
        .send(dataRes.bodyRes)
        .end();
    });
};
module.exports.reUpload = reUpload;

/**
 * @overview Thay đổi trạng thái
 *
 * @param {Request} req
 * @param {Response} res
 * @param {Function} next
 * @param {Function} getInfo
 *
 * @input  { }
 * @output {
 *           code: String,
 *           status?: {Object},
 *           updateDate?: Date
 *         }
 */
const changeStatus = (req, res, next) => {
  const userSelf = req.user;
  let dataRes = null;

  async.waterfall([
    (done) => {
      /* Kiểm tra đầu vào */

      if (userSelf) {
        BusinessCard
          .findOne({ userID: userSelf._id })
          .exec((err, businessCard) => {
            if (err || !businessCard) {
              // Danh thiếp không tồn tại
              dataRes = {
                status: 404,
                bodyRes: {
                  code: 'businessCard.notExist',
                },
              };

              done(err || new Error('Not business card exists'), dataRes);
            } else {
              done(null, businessCard);
            }
          });
      } else {
        // Chưa đăng nhập người dùng
        dataRes = {
          status: 401,
          bodyRes: {
            code: 'user.signin.notExist',
          },
        };

        done(new Error('User not signin'), dataRes);
      }
    }, (businessCard, done) => {
      /* Thay đổi thạng thái danh thiếp */

      businessCard.setStatus(!businessCard.status.is);
      businessCard.updateDate = Date.now();
      businessCard.save((err) => {
        if (err) {
          // Thay đổi trạng thái danh thiếp thất bại
          dataRes = {
            status: 400,
            bodyRes: {
              code: 'businessCard.status.change.failed',
            },
          };
        } else {
          const activityHistory = new ActivityHistory({
            userID: userSelf._id,
            toURL: businessCard._id,
            type: constant.BUSINESS_CARD,
            result: constant.SUCCESS,
            content: 'businessCard.status.change.is',
          });

          activityHistory.save((err) => {
            if (err) {
              logger.logError(
                'business-cards.profile.server.controller',
                'changeStatus',
                constant.ACTIVITY_HISTORY,
                activityHistory,
                err
              );
            }
          });

          dataRes = {
            status: 200,
            bodyRes: {
              code: 'businessCard.status.change.success',
              status: businessCard.status,
              updateDate: businessCard.updateDate
            },
          };
        }

        done(err, dataRes);
      });
    }], (err, dataRes) => {
      return res
        .status(dataRes.status)
        .send(dataRes.bodyRes)
        .end();
    });
};
module.exports.changeStatus = changeStatus;

/**
 * @overview Thay đổi mô tả
 *
 * @param {Request} req
 * @param {Response} res
 * @param {Function} next
 * @param {Function} getInfo
 *
 * @input  body { descriptions: String }
 * @output      {
 *                code: String,
 *                descriptions?: {String},
 *                updateDate?: Date
 *              }
 */
const changeDescriptions = (req, res, next) => {
  const bodyReq = req.body;
  const userSelf = req.user;
  let dataRes = null;

  async.waterfall([
    (done) => {
      /* Kiểm tra đầu vào */

      if (userSelf) {
        BusinessCard
          .findOne({ userID: userSelf._id })
          .exec((err, businessCard) => {
            if (err || !businessCard) {
              // Danh thiếp không tồn tại
              dataRes = {
                status: 404,
                bodyRes: {
                  code: 'businessCard.notExist',
                },
              };

              done(err || new Error('Not business card exists'), dataRes);
            } else {
              done(null, businessCard);
            }
          });
      } else {
        // Chưa đăng nhập người dùng
        dataRes = {
          status: 401,
          bodyRes: {
            code: 'user.signin.notExist',
          },
        };

        done(new Error('User not signin'), dataRes);
      }
    }, (businessCard, done) => {
      /* Thay đổi mô tả danh thiếp */

      businessCard.descriptions = bodyReq.descriptions;
      businessCard.updateDate = Date.now();
      businessCard.save((err) => {
        if (err) {
          // Thay đổi mô tả danh thiếp thất bại
          dataRes = {
            status: 400,
            bodyRes: {
              code: 'businessCard.descriptions.change.failed',
            },
          };
        } else {
          const activityHistory = new ActivityHistory({
            userID: userSelf._id,
            toURL: businessCard._id,
            type: constant.BUSINESS_CARD,
            result: constant.SUCCESS,
            content: 'businessCard.descriptions.change.is',
          });

          activityHistory.save((err) => {
            if (err) {
              logger.logError(
                'business-cards.profile.server.controller',
                'changeDescriptions',
                constant.ACTIVITY_HISTORY,
                activityHistory,
                err
              );
            }
          });

          dataRes = {
            status: 200,
            bodyRes: {
              code: 'businessCard.descriptions.change.success',
              descriptions: businessCard.descriptions,
              updateDate: businessCard.updateDate
            },
          };
        }

        done(err, dataRes);
      });
    }], (err, dataRes) => {
      return res
        .status(dataRes.status)
        .send(dataRes.bodyRes)
        .end();
    });
};
module.exports.changeDescriptions = changeDescriptions;

/**
 * @overview Thay đổi địa chỉ
 *
 * @param {Request} req
 * @param {Response} res
 * @param {Function} next
 * @param {Function} getInfo
 *
 * @input body
  {
    address: {
      country: String,
      city: String,
      county:String,
      local: String
    }
  }
 * @output      {
 *                code: String,
 *                address?: {String},
 *                updateDate?: Date
 *              }
 */
const changeAddress = (req, res, next) => {
  const bodyReq = req.body;
  const userSelf = req.user;
  let dataRes = null;

  async.waterfall([
    (done) => {
      /* Kiểm tra đầu vào */

      if (userSelf) {
        BusinessCard
          .findOne({ userID: userSelf._id })
          .exec((err, businessCard) => {
            if (err || !businessCard) {
              // Danh thiếp không tồn tại
              dataRes = {
                status: 404,
                bodyRes: {
                  code: 'businessCard.notExist',
                },
              };

              done(err || new Error('Not business card exists'), dataRes);
            } else {
              done(null, businessCard);
            }
          });
      } else {
        // Chưa đăng nhập người dùng
        dataRes = {
          status: 401,
          bodyRes: {
            code: 'user.signin.notExist',
          },
        };

        done(new Error('User not signin'), dataRes);
      }
    }, (businessCard, done) => {
      /* Thay đổi địa chỉ danh thiếp */

      const { address } = bodyReq;

      if (address.country) {
        businessCard.address.country = address.country;
      }

      if (address.city) {
        businessCard.address.city = address.city;
      }

      if (address.county) {
        businessCard.address.county = address.county;
      }

      if (address.local) {
        businessCard.address.local = address.local;
      }

      businessCard.updateDate = Date.now();
      businessCard.save((err) => {
        if (err) {
          // Thay đổi địa chỉ danh thiếp thất bại
          dataRes = {
            status: 400,
            bodyRes: {
              code: 'businessCard.address.change.failed',
            },
          };
        } else {
          const activityHistory = new ActivityHistory({
            userID: userSelf._id,
            toURL: businessCard._id,
            type: constant.BUSINESS_CARD,
            result: constant.SUCCESS,
            content: 'businessCard.address.change.is',
          });

          activityHistory.save((err) => {
            if (err) {
              logger.logError(
                'business-cards.profile.server.controller',
                'changeAddress',
                constant.ACTIVITY_HISTORY,
                activityHistory,
                err
              );
            }
          });

          dataRes = {
            status: 200,
            bodyRes: {
              code: 'businessCard.address.change.success',
              address: businessCard.address,
              updateDate: businessCard.updateDate
            },
          };
        }

        done(err, dataRes);
      });
    }], (err, dataRes) => {
      return res
        .status(dataRes.status)
        .send(dataRes.bodyRes)
        .end();
    });
};
module.exports.changeAddress = changeAddress;

/**
 * @overview Thay đổi hình ảnh
 *
 * @param {Request} req
 * @param {Response} res
 * @param {Function} next
 * @param {Function} getInfo
 *
 * @input query { name: String }
 *        file  {Object}
 * @output      {
 *                code: String,
 *                image?: {String},
 *                updateDate?: Date
 *              }
 */
const uploadImage = (req, res, next) => {
  const queryReq = req.query;
  const userSelf = req.user;
  let dataRes = null;

  async.waterfall([
    (done) => {
      /* Kiểm tra đầu vào */

      if (userSelf) {
        BusinessCard
          .findOne({ userID: userSelf._id })
          .exec((err, businessCard) => {
            if (err || !businessCard) {
              // Danh thiếp không tồn tại
              dataRes = {
                status: 404,
                bodyRes: {
                  code: 'businessCard.notExist',
                },
              };

              done(err || new Error('Not business card exists'), dataRes);
            } else {
              done(null, businessCard);
            }
          });
      } else {
        // Chưa đăng nhập người dùng
        dataRes = {
          status: 401,
          bodyRes: {
            code: 'user.signin.notExist',
          },
        };

        done(new Error('User not signin'), dataRes);
      }
    }, (businessCard, done) => {
      /* Tải hình danh thiếp */

      if (!businessCard.checkImagesLimit()) {
        const upload = multer().single(queryReq.name);
        upload(req, res, (err) => {
          const { file } = req;
          const errorFile = file ? multer.testUploadImage(constant.BUSINESS_CARD, file) : new Error('Not file');

          if (!err && file && !errorFile) {
            done(null, businessCard, file);
          } else {
            // Bad request.file
            dataRes = {
              status: 400,
              bodyRes: {
                code: 'data.notVaild',
              },
            };

            done(err || errorFile, dataRes);
          }
        });
      } else {
        // Tải hình danh thiếp thất bại
        dataRes = {
          status: 401,
          bodyRes: {
            code: 'businessCard.image.upload.failed',
          },
        };

        done(new Error('Failed upload business card image'), dataRes);
      }
    }, (businessCard, file, done) => {
      /* Đăng hình danh thiếp */

      cloudinary.uploadBusinessCardImage(userSelf._id, businessCard._id, file)
        .then((dataImage) => {
          done(null, businessCard, dataImage);
        })
        .catch((err) => {
          // Tải hình danh thiếp thất bại
          dataRes = {
            status: 400,
            bodyRes: {
              code: 'businessCard.image.upload.failed',
            },
          };

          done(err, dataRes);
        });
    }, (businessCard, image, done) => {
      /* Tải hình danh thiếp */

      const dataImage = {
        name: queryReq.name,
        url: image.secure_url,
        publicID: image.public_id,
      };

      businessCard.images.push(dataImage);
      businessCard.updateDate = Date.now();
      businessCard.save((err) => {
        if (err) {
          // Tải hình danh thiếp thất bại
          dataRes = {
            status: 400,
            bodyRes: {
              code: 'businessCard.image.upload.failed',
            },
          };
        } else {
          const activityHistory = new ActivityHistory({
            userID: userSelf._id,
            toURL: businessCard._id,
            type: constant.BUSINESS_CARD,
            result: constant.SUCCESS,
            content: 'businessCard.image.upload.is',
          });

          activityHistory.save((err) => {
            if (err) {
              logger.logError(
                'business-cards.profile.server.controller',
                'uploadImage',
                constant.ACTIVITY_HISTORY,
                activityHistory,
                err
              );
            }
          });

          dataRes = {
            status: 200,
            bodyRes: {
              code: 'businessCard.image.upload.success',
              image: dataImage,
              updateDate: businessCard.updateDate
            },
          };
        }

        done(err, dataRes);
      });
    }], (err, dataRes) => {
      return res
        .status(dataRes.status)
        .send(dataRes.bodyRes)
        .end();
    });
};
module.exports.uploadImage = uploadImage;

/**
 * @overview Thay đổi hình ảnh
 *
 * @param {Request} req
 * @param {Response} res
 * @param {Function} next
 * @param {Function} getInfo
 *
 * @input query { publicID: String }
 * @output      {
 *                code: String,
 *                images?: { [{Object}] || []},
 *                updateDate?: Date
 *              }
 */
const deleteImage = (req, res, next) => {
  const queryReq = req.query;
  const userSelf = req.user;
  let dataRes = null;

  const { publicID } = queryReq;

  async.waterfall([
    (done) => {
      /* Kiểm tra đầu vào */

      if (userSelf) {
        BusinessCard.findOneAndUpdate(
          {
            userID: userSelf._id,
            'images.publicID': publicID,
          },
          {
            $pull: {
              images: {
                publicID,
              },
            },
            $set: {
              updateDate: Date.now(),
            },
          },
          {
            new: true,
          }
        )
          .exec((err, businessCard) => {
            if (err) {
              // Xoá hình danh thiếp thất bại
              dataRes = {
                status: 400,
                bodyRes: {
                  code: 'businessCard.image.delete.failed',
                },
              };

              done(err, dataRes);
            } else if (!businessCard) {
              // Danh thiếp không tồn tại
              dataRes = {
                status: 404,
                bodyRes: {
                  code: 'businessCard.notExist',
                },
              };

              done(new Error('Not business card exists'), dataRes);
            } else {
              done(null, businessCard);
            }
          });
      } else {
        // Chưa đăng nhập người dùng
        dataRes = {
          status: 401,
          bodyRes: {
            code: 'user.signin.notExist',
          },
        };

        done(new Error('User not signin'), dataRes);
      }
    }, (businessCard, done) => {
      /* Xoá hình danh thiếp */

      cloudinary.deleteImageByPublicID(publicID)
        .then((dataImage) => {
          const activityHistory = new ActivityHistory({
            userID: userSelf._id,
            toURL: businessCard._id,
            type: constant.BUSINESS_CARD,
            result: constant.SUCCESS,
            content: 'businessCard.image.delete.is',
          });

          activityHistory.save((err) => {
            if (err) {
              logger.logError(
                'business-cards.profile.server.controller',
                'deleteImage',
                constant.ACTIVITY_HISTORY,
                activityHistory,
                err
              );
            }
          });

          dataRes = {
            status: 200,
            bodyRes: {
              code: 'businessCard.image.delete.success',
              images: businessCard.images,
              updateDate: businessCard.updateDate
            },
          };

          done(null, dataRes);
        })
        .catch((err) => {
          // Xoá hình danh thiếp thất bại
          dataRes = {
            status: 400,
            bodyRes: {
              code: 'businessCard.image.delete.failed',
            },
          };

          done(err, dataRes);
        });
    }], (err, dataRes) => {
      return res
        .status(dataRes.status)
        .send(dataRes.bodyRes)
        .end();
    });
};
module.exports.deleteImage = deleteImage;

/**
 * @overview Xoá danh thiếp
 *
 * @param {Request} req
 * @param {Response} res
 * @param {Function} next
 *
 * @input body { password: String }
 * @output     { code: String }
 */
const deletee = (req, res, next) => {
  const bodyReq = req.body;
  const userSelf = req.user;
  let dataRes = null;

  async.waterfall([
    (done) => {
      /* Kiểm tra đầu vào */

      if (userSelf) {
        BusinessCard
          .findOne({ userID: userSelf._id })
          .exec((err, businessCard) => {
            if (err || !businessCard) {
              // Danh thiếp không tồn tại
              dataRes = {
                status: 404,
                bodyRes: {
                  code: 'businessCard.notExist',
                },
              };

              done(err || new Error('Not business card exists'), dataRes);
            } else {
              User
                .findById(userSelf._id)
                .exec((err, user) => {
                  if (err || !user) {
                    // Người dùng không tồn tại
                    dataRes = {
                      status: 404,
                      bodyRes: {
                        code: 'user.notExist',
                      },
                    };

                    done(err || new Error('Not user exists'), dataRes);
                  } else if (user.authenticate(bodyReq.password)) {
                    done(null, businessCard);
                  } else {
                    // Đăng nhập người dùng thất bại
                    dataRes = {
                      status: 401,
                      bodyRes: {
                        code: 'user.signin.failed',
                      },
                    };

                    done(new Error('User failed signin'), dataRes);
                  }
                });
            }
          });
      } else {
        // Chưa đăng nhập người dùng
        dataRes = {
          status: 401,
          bodyRes: {
            code: 'user.signin.notExist',
          },
        };

        done(new Error('User not signin'), dataRes);
      }
    }, (businessCard, done) => {
      /* Cập nhật danh thiếp và xoá hình */

      const { images } = businessCard;
      images.forEach((image) => {
        cloudinary.deleteImageByPublicID(image.publicID)
          .then((dataImage) => {
          })
          .catch((err) => {
            logger.logError(
              'business-cards.profile.server.controller',
              'deletee',
              constant.BUSINESS_CARD,
              businessCard,
              err
            );
          });
      });

      BusinessCard.deleteOne({ _id: businessCard._id }, (err) => {
        if (err) {
          // Xoá danh thiếp thất bại
          dataRes = {
            status: 400,
            bodyRes: {
              code: 'businessCard.delete.failed',
            },
          };

          done(err, dataRes);
        } else {
          done(null, businessCard);
        }
      });
    }, (businessCard, done) => {
      /* Cập nhật người dùng */

      userSelf.roles.value = (userSelf.roles.value === constant.PROVIDER) ? constant.USER : userSelf.roles.value;
      userSelf.roles.initDate = (userSelf.roles.value === constant.PROVIDER) ? Date.now() : userSelf.roles.initDate;
      userSelf.save((err) => {
        if (err) {
          // Xoá danh thiếp thất bại
          dataRes = {
            status: 400,
            bodyRes: {
              code: 'businessCard.delete.failed',
            },
          };
        } else {
          const activityHistory = new ActivityHistory({
            userID: userSelf._id,
            toURL: businessCard._id,
            type: constant.BUSINESS_CARD,
            result: constant.SUCCESS,
            content: 'businessCard.delete.is',
          });

          activityHistory.save((err) => {
            if (err) {
              logger.logError(
                'business-cards.profile.server.controller',
                'deletee',
                constant.ACTIVITY_HISTORY,
                activityHistory,
                err
              );
            }
          });

          dataRes = {
            status: 200,
            bodyRes: {
              code: 'businessCard.delete.success',
            },
          };
        }

        done(err, dataRes);
      });
    }], (err, dataRes) => {
      return res
        .status(dataRes.status)
        .send(dataRes.bodyRes)
        .end();
    });
};
module.exports.deletee = deletee;
