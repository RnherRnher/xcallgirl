const path = require('path');
const mongoose = require('mongoose');
const async = require('async');
const _ = require('lodash');
const logger = require(path.resolve('./config/lib/logger'));
const constant = require(path.resolve('./commons/utils/constant'));
const twilio = require(path.resolve('config/lib/twilio'));
const { createSocketData } = require(path.resolve('./commons/utils/middleware'));
const { SOCKET_NOTIFICATION_PUSH } = require(path.resolve('./commons/modules/notifications/datas/socket.data'));
const { SOCKET_BUSINESS_CARD_DEAL_HISTORY_PUSH } = require(path.resolve('./commons/modules/business-cards/datas/socket.data'));

const User = mongoose.model('User');
const BusinessCard = mongoose.model('BusinessCard');
const ActivityHistory = mongoose.model('ActivityHistory');
const BusinessCardDealHistory = mongoose.model('BusinessCardDealHistory');
const Notification = mongoose.model('Notification');

/**
 * @overview Tìm kiếm
 *
 * @param {Request} req
 * @param {Response} res
 * @param {Function} next
 * @param {Function} getInfo
 *
 * @input  query { type: String }
 * @output       { skip?: Number, businessCards?: [{BusinessCard}] }
 */
const search = (req, res, next) => {
  const queryReq = req.query;
  let dataRes = null;

  /**
 * @override Lấy kết quả tìm kiếm của danh thiếp
 *
 * @param
 * {{
 *    topActivityLimit?: [{businessCard}],
 *    topNewUpLimit?: [{businessCard}],
 *    topRank?: [{businessCard}]
 * }}
 * @param   {Number} skip
 * @param   {Number} limit
 * @returns {Object}
 */
  const getSearchFullBusinessCard = (resultBusinessCards, skip, limit) => {
    let dataRes = null;
    const currentSkip = skip > 0 ? skip : 0;

    if (
      (resultBusinessCards.topActivity && resultBusinessCards.topActivity.length)
      || (resultBusinessCards.topNewUp && resultBusinessCards.topNewUp.length)
      || (resultBusinessCards.topRank && resultBusinessCards.topRank.length)
    ) {
      dataRes = {
        status: 200,
        bodyRes: {
          code: 'businessCard.exist',
          skip: currentSkip + (
            resultBusinessCards.topRank.length < limit
              ? resultBusinessCards.topRank.length
              : limit
          ),
          businessCards: resultBusinessCards,
        },
      };
    } else {
      // Danh thiếp không tồn tại
      dataRes = {
        status: 404,
        bodyRes: {
          code: 'businessCard.notExist',
        },
      };
    }

    return dataRes;
  };

  /**
   * @override Tìm - Sấp xếp - Bỏ qua - Giớ hạn danh thiếp
   *
   * @param   {Object} conditions
   * @param   {Object} sort
   * @param   {Number} skip
   * @param   {Number} limit
   * @returns {Model}
   */
  const findBusinessCard = (conditions, sort, skip, limit) => {
    return BusinessCard
      .find(conditions)
      .sort(sort)
      .skip(skip)
      .limit(limit);
  };

  /**
   *  @override Lấy đầy đủ thông tin danh thiếp (danh thiếp + người dùng)
   *
   * @param   {[{BusinessCard}]} listBusinessCards
   * @param   {ObjectId} userID
   * @param   {String} type
   * @returns {Promise}
   */
  const getShortcutsInfoBusinessCard = (listBusinessCards, userID, type) => {
    return new Promise((resolve, reject) => {
      async.map(listBusinessCards, (businessCard, callback) => {
        businessCard
          .getShortcutsInfo(userID)
          .then((businessCard) => {
            callback(null, businessCard);
          });
        // });
      }, (err, resultBusinessCards) => {
        if (err) {
          return reject(err);
        }

        return resolve({ [type]: resultBusinessCards });
      });
    });
  };

  /**
   * @override Tìm kiếm danh thiếp theo bố cục ứng dụng
   *
   * @param   {Number} topActivityLimit
   * @param   {Number} topNewUpLimit
   * @param   {Number} searchLimit
   * @param   {ObjectId} userID
   * @param   {Object} conditions
   * @returns {Promise}
   */
  const searchBusinessCard = (topActivityLimit, topNewUpLimit, searchLimit, userID, conditions) => {
    return new Promise((resolve, reject) => {
      async.series([
        (callback) => {
          /* Top activity */

          findBusinessCard(
            conditions,
            { pointsDay: -1 },
            0,
            topActivityLimit
          ).exec((err, businessCardsTopActivitys) => {
            if (err) {
              callback(err);
            } else {
              getShortcutsInfoBusinessCard(businessCardsTopActivitys, userID, 'topActivity')
                .then((resultBusinessCards) => {
                  callback(null, resultBusinessCards);
                })
                .catch((err) => {
                  callback(err);
                });
            }
          });
        }, (callback) => {
          /* Top new up */

          findBusinessCard(
            conditions,
            { 'reUpload.date': -1 },
            0,
            topNewUpLimit
          ).exec((err, businessCardsTopNewUps) => {
            if (err) {
              callback(err);
            } else {
              getShortcutsInfoBusinessCard(businessCardsTopNewUps, userID, 'topNewUp')
                .then((resultBusinessCards) => {
                  callback(null, resultBusinessCards);
                })
                .catch((err) => {
                  callback(err);
                });
            }
          });
        }, (callback) => {
          /* Top rank */

          findBusinessCard(
            conditions,
            { points: -1 },
            0,
            searchLimit
          ).exec((err, businessCardsTopRanks) => {
            if (err) {
              callback(err);
            } else {
              getShortcutsInfoBusinessCard(businessCardsTopRanks, userID, 'topRank')
                .then((resultBusinessCards) => {
                  callback(null, resultBusinessCards);
                })
                .catch((err) => {
                  callback(err);
                });
            }
          });
        },
      ], (err, listResultBusinessCards) => {
        if (err) {
          return reject(err);
        }

        let resultBusinessCards = {};
        async.each(listResultBusinessCards, (businessCards, done) => {
          resultBusinessCards = _.assignIn(businessCards, resultBusinessCards);
          done(null);
        }, (err) => {
          return resolve(resultBusinessCards);
        });
      });
    });
  };

  /**
   * @override Tìm kiếm đầy đủ danh thiêp
   *
   * @param {Number} topActivityLimit
   * @param {Number} topNewUpLimit
   * @param {Number} searchLimit
   * @param {ObjectId} userID
   * @param {Number} skip
   * @param {Object} conditions
   * @returns {Promise}
   */
  const searchShortcutsBusinessCard = (topActivityLimit, topNewUpLimit, searchLimit, userID, skip, conditions) => {
    return new Promise((resolve, reject) => {
      let dataRes = null;

      if (skip <= 0) {
        searchBusinessCard(
          topActivityLimit,
          topNewUpLimit,
          searchLimit,
          userID,
          conditions
        ).then((resultBusinessCards) => {
          return resolve(getSearchFullBusinessCard(resultBusinessCards, skip, searchLimit));
        }).catch((err) => {
          // Bad request.body
          dataRes = {
            status: 400,
            bodyRes: {
              code: 'data.notVaild',
            },
          };

          return reject(dataRes);
        });
      } else {
        findBusinessCard(
          conditions,
          { points: -1 },
          skip,
          searchLimit
        ).exec((err, businessCardsTopRank) => {
          if (err) {
            // Bad request.body
            dataRes = {
              status: 400,
              bodyRes: {
                code: 'data.notVaild',
              },
            };

            return reject(dataRes);
          }

          getShortcutsInfoBusinessCard(businessCardsTopRank, userID, 'topRank')
            .then((resultBusinessCards) => {
              return resolve(getSearchFullBusinessCard(resultBusinessCards, skip, searchLimit));
            })
            .catch((err) => {
              // Bad request.body
              dataRes = {
                status: 400,
                bodyRes: {
                  code: 'data.notVaild',
                },
              };

              return reject(dataRes);
            });
        });
      }
    });
  };

  /**
   * Giải mả đầy đủ query tìm kiếm mở rộng danh thiếp
   *
   * @param   {Object} query
   * @returns {Object}
   */
  const decodeQueryExtendSearch = (query) => {
    const conditions = {
      'status.is': true,
    };

    if (_.has(query, 'gender')) {
      conditions.gender = query.gender;
    }

    // if (_.has(query, 'blurFace')) {
    //   conditions.blurFace = query.blurFace;
    // }

    // if (_.has(query, 'star')) {
    //   conditions.points = BusinessCard.getQueryPoints(query.star);
    // }

    // if (!_.isEmpty(query.costs)) {
    //   if (!_.isEmpty(query.costs.room)) {
    //     if (_.has(query, 'costs.room.values') && _.isArray(query.costs.room.values)) {
    //       conditions['costs.rooms.value'] = { $in: query.costs.room.values };
    //     }

    //     if (_.has(query, 'costs.room.max')) {
    //       conditions['costs.rooms.moneys'] = { $lte: query.costs.room.max };
    //     }

    //     if (_.has(query, 'costs.room.min')) {
    //      conditions['costs.rooms.moneys']= _.merge(
    //         conditions['costs.rooms.moneys'] ,
    //         { $gte: query.costs.room.min }
    //       );
    //     }
    //   }

    //   if (!_.isEmpty(query.costs.service)) {
    //     if (_.has(query, 'costs.service.values') && _.isArray(query.costs.service.values)) {
    //       conditions['costs.services.value'] = { $in: query.costs.service.values };
    //     }

    //     if (_.has(query, 'costs.service.max')) {
    //       conditions['costs.services.moneys'] = { $lte: query.costs.service.max };
    //     }

    //     if (_.has(query, 'costs.service.min')) {
    //      conditions['costs.services.moneys'] = _.merge(
    //         conditions['costs.services.moneys'],
    //         { $gte: query.costs.service.min }
    //       );
    //     }
    //   }

    //   if (_.has(query, 'costs.currencyUnit')) {
    //     conditions['costs.currencyUnit'] = query.costs.currencyUnit;
    //   }
    // }

    if (!_.isEmpty(query.address)) {
      if (_.has(query, 'address.country')) {
        conditions['address.country'] = query.address.country;
      }

      if (_.has(query, 'address.city')) {
        conditions['address.city'] = query.address.city;
      }

      if (_.has(query, 'address.county')) {
        conditions['address.county'] = query.address.county;
      }
    }

    return conditions;
  };

  /**
   * @overview Tìm kiếm tự động
   *
   * @param {Request} req
   * @param {Response} res
   * @param {Function} next
   * @param {Function} getInfo
   *
   * @input  query { skip: Number }
   * @output
   * {
   *  code: String,
   *  skip: Number,
   *  businessCards: {
   *    topActivityLimit?: [{businessCard}],
   *    topNewUpLimit?: [{businessCard}],
   *    topRank: [{businessCard}]
   *  }
   * }
   */
  const autoSearch = (req, res, next) => {
    const queryReq = req.query;
    const userSelf = req.user;
    let dataRes = null;

    const { searchLimit } = req.app.locals.config;
    const { topActivityLimit } = req.app.locals.config.businessCard;
    const { topNewUpLimit } = req.app.locals.config.businessCard;

    if (userSelf) {
      const conditions = { 'status.is': true };

      searchShortcutsBusinessCard(
        topActivityLimit,
        topNewUpLimit,
        searchLimit,
        userSelf._id,
        queryReq.skip,
        conditions
      ).then((dataRes) => {
        return res
          .status(dataRes.status)
          .send(dataRes.bodyRes)
          .end();
      })
        .catch((dataRes) => {
          return res
            .status(dataRes.status)
            .send(dataRes.bodyRes)
            .end();
        });
    } else {
      // Chưa đăng nhập người dùng
      dataRes = {
        status: 401,
        bodyRes: {
          code: 'user.signin.notExist',
        },
      };

      return res
        .status(dataRes.status)
        .send(dataRes.bodyRes)
        .end();
    }
  };

  /**
   * @overview Tìm kiếm mở rộng
   *
   * @param {Request} req
   * @param {Response} res
   * @param {Function} next
   * @param {Function} getInfo
   *
   * @input  body
    {
      skip: Number,
      extendSearch: {
        blurFace?: Boolean,
        gender?: String,
        star?: Number,
        costs?: {
          room: {
            max: Number,
            min: Number,
            values: [String]
          },
          service: {
            max: Number,
            min: Number,
            values: [String]
          },
          currencyUnit: String
        },
        address?: {
          country: String,
          city: String,
          county: String
        }
      }
    }
   * @output { code: String, skip: Number, businessCards: [{BusinessCard}] }
   */
  const extendSearch = (req, res, next) => {
    const queryReq = req.query;
    const userSelf = req.user;
    let dataRes = null;

    const { searchLimit } = req.app.locals.config;
    const { topActivityLimit } = req.app.locals.config.businessCard;
    const { topNewUpLimit } = req.app.locals.config.businessCard;

    if (userSelf) {
      const { extendSearch } = queryReq;

      if (!_.isEmpty(extendSearch)) {
        const conditions = decodeQueryExtendSearch(extendSearch);

        searchShortcutsBusinessCard(
          topActivityLimit,
          topNewUpLimit,
          searchLimit,
          userSelf._id,
          queryReq.skip,
          conditions
        ).then((dataRes) => {
          return res
            .status(dataRes.status)
            .send(dataRes.bodyRes)
            .end();
        })
          .catch((dataRes) => {
            return res
              .status(dataRes.status)
              .send(dataRes.bodyRes)
              .end();
          });
      } else {
        // Bad request.body
        dataRes = {
          status: 400,
          bodyRes: {
            code: 'data.notVaild',
          },
        };

        return res
          .status(dataRes.status)
          .send(dataRes.bodyRes)
          .end();
      }
    } else {
      // Chưa đăng nhập người dùng
      dataRes = {
        status: 401,
        bodyRes: {
          code: 'user.signin.notExist',
        },
      };

      return res
        .status(dataRes.status)
        .send(dataRes.bodyRes)
        .end();
    }
  };

  switch (queryReq.type) {
    case constant.AUTO:
      autoSearch(req, res, next);
      break;
    case constant.EXTEND:
      extendSearch(req, res, next);
      break;
    default: {
      dataRes = {
        status: 400,
        bodyRes: {
          code: 'data.notVaild',
        },
      };

      return res
        .status(dataRes.status)
        .send(dataRes.bodyRes)
        .end();
    }
  }
};
module.exports.search = search;

/**
 * @overview Lây thông tin theo ID
 *
 * @param {Request} req
 * @param {Response} res
 * @param {Function} next
 * @param {Function} getInfo
 *
 * @input  params { id: String }
 * @output        {
 *                  code: String,
 *                  user: {User},
 *                  businessCard: {BusinessCard}
 *                 }
 */
const get = (req, res, next) => {
  const paramsReq = req.params;
  const userSelf = req.user;
  let dataRes = null;

  async.waterfall([
    (done) => {
      /* Kiểm tra đầu vào */

      if (userSelf) {
        BusinessCard
          .findById(paramsReq.id)
          .exec((err, businessCard) => {
            if (err || !businessCard) {
              // Danh thiếp không tồn tại
              dataRes = {
                status: 404,
                bodyRes: {
                  code: 'businessCard.notExist',
                },
              };

              done(err || new Error('Not business card exists'), dataRes);
            } else {
              done(null, businessCard);
            }
          });
      } else {
        // Chưa đăng nhập người dùng
        dataRes = {
          status: 401,
          bodyRes: {
            code: 'user.signin.notExist',
          },
        };

        done(new Error('User not signin'), dataRes);
      }
    }, (businessCard, done) => {
      /* Tính điểm danh thiếp */

      if (!userSelf.equalsByID(businessCard.userID)) {
        businessCard.addView(userSelf._id);
        businessCard.save((err) => {
          if (err) {
            logger.logError(
              'business-cards.action.server.controller',
              'get',
              constant.BUSINESS_CARD,
              businessCard,
              err
            );
          }

          done(null, businessCard);
        });
      } else {
        done(null, businessCard);
      }
    }, (businessCard, done) => {
      /* Lấy dữ liệu danh thiếp */

      businessCard
        .getFullInfo(userSelf._id)
        .then((businessCard) => {
          dataRes = {
            status: 200,
            bodyRes: {
              code: 'businessCard.exist',
              businessCard
            },
          };

          done(null, dataRes);
        });
    }], (err, dataRes) => {
      return res
        .status(dataRes.status)
        .send(dataRes.bodyRes)
        .end();
    });
};
module.exports.get = get;

/**
 * @overview Lây thông của mình
 *
 * @param {Request} req
 * @param {Response} res
 * @param {Function} next
 *
 * @input  { }
 * @output { code: String, businessCard: {BusinessCard} }
 */
const getSelf = (req, res, next) => {
  const userSelf = req.user;
  let dataRes = null;

  if (userSelf) {
    BusinessCard
      .findOne({ userID: userSelf._id })
      .exec((err, businessCard) => {
        if (err || !businessCard) {
          // Danh thiếp không tồn tại
          dataRes = {
            status: 404,
            bodyRes: {
              code: 'businessCard.notExist',
            },
          };

          return res
            .status(dataRes.status)
            .send(dataRes.bodyRes)
            .end();
        }

        businessCard
          .getFullInfo(userSelf._id)
          .then((businessCard) => {
            dataRes = {
              status: 200,
              bodyRes: {
                code: 'businessCard.exist',
                businessCard,
              },
            };

            return res
              .status(dataRes.status)
              .send(dataRes.bodyRes)
              .end();
          });
      });
  } else {
    // Chưa đăng nhập người dùng
    dataRes = {
      status: 401,
      bodyRes: {
        code: 'user.signin.notExist',
      },
    };

    return res
      .status(dataRes.status)
      .send(dataRes.bodyRes)
      .end();
  }
};
module.exports.getSelf = getSelf;

/**
 * @overview Tạo danh thiếp
 *
 * @param {Request} req
 * @param {Response} res
 * @param {Function} next
 *
 * @input body
  {
    title: String,
    descriptions: String,
    blurFace: Boolean,
    costs: {
      rooms: [{
        value: String,
        moneys: Number
      }],
      services: [{
        value: String,
        moneys: Number
      }],
      currencyUnit: String
    },
    measurements3: {
      breast: Number,
      waist: Number,
      hips: Number
    },
    address: {
      country: String,
      city: String,
      county: String,
      local: String
    }
  }
 * @output { code: String, businessCard: {BusinessCard} }
 */
const create = (req, res, next) => {
  const bodyReq = req.body;
  const userSelf = req.user;
  let dataRes = null;

  async.waterfall([
    (done) => {
      /* Kiểm tra đầu vào */

      if (userSelf) {
        if (userSelf.checkVerifyAccount()) {
          BusinessCard
            .findOne({ userID: userSelf._id })
            .exec((err, businessCard) => {
              if (businessCard) {
                // Danh thiếp tồn tại
                dataRes = {
                  status: 401,
                  bodyRes: {
                    code: 'businessCard.exist',
                  },
                };

                done(new Error('Business card exists'), dataRes);
              }

              done(null);
            });
        } else {
          // Chưa xác thực người dùng
          dataRes = {
            status: 401,
            bodyRes: {
              code: 'user.verify.account.notExist',
            },
          };

          done(new Error('User not verify'), dataRes);
        }
      } else {
        // Chưa đăng nhập người dùng
        dataRes = {
          status: 401,
          bodyRes: {
            code: 'user.signin.notExist',
          },
        };

        done(new Error('User not signin'), dataRes);
      }
    }, (done) => {
      /* Tạo danh thiếp */

      const businessCard = new BusinessCard(
        BusinessCard
          .getDefaultValue(
            userSelf._id,
            userSelf.gender,
            bodyReq
          )
      );

      businessCard.save((err) => {
        if (err) {
          // Dữ liệu không hợp lệ
          dataRes = {
            status: 400,
            bodyRes: {
              code: 'data.notVaild',
            },
          };
        } else {
          dataRes = businessCard;
        }

        done(err, dataRes);
      });
    }, (businessCard, done) => {
      /* Cập nhật người dùng */

      userSelf.roles.value = (userSelf.roles.value === constant.USER) ? constant.PROVIDER : userSelf.roles.value;
      userSelf.roles.initDate = (userSelf.roles.value === constant.USER) ? Date.now() : userSelf.roles.initDate;
      userSelf.save((err) => {
        if (err) {
          // Tạo danh thiếp thất bại
          dataRes = {
            status: 400,
            bodyRes: {
              code: 'businessCard.create.failed',
            },
          };

          done(err, dataRes);
        } else {
          const activityHistory = new ActivityHistory({
            userID: userSelf._id,
            toURL: businessCard._id,
            type: constant.BUSINESS_CARD,
            result: constant.SUCCESS,
            content: 'businessCard.create.is',
          });

          activityHistory.save((err) => {
            if (err) {
              logger.logError(
                'business-cards.action.server.controller',
                'create',
                constant.ACTIVITY_HISTORY,
                activityHistory,
                err
              );
            }
          });

          businessCard
            .getFullInfo(userSelf._id)
            .then((businessCard) => {
              dataRes = {
                status: 200,
                bodyRes: {
                  code: 'businessCard.create.success',
                  businessCard,
                },
              };

              done(null, dataRes);
            });
        }
      });
    }], (err, dataRes) => {
      return res
        .status(dataRes.status)
        .send(dataRes.bodyRes)
        .end();
    });
};
module.exports.create = create;

/**
 * @overview Tố cáo danh thiếp
 *
 * @param {Request} req
 * @param {Response} res
 * @param {Function} next
 *
 * @input  params { id: String }
 *         body   { report: { reason: String } }
 * @output        {
 *                  code: String,
 *                  businessCardID: ObjectId,
 *                  sockets?: [{Object}]
 *                }
 */
const report = (req, res, next) => {
  const bodyReq = req.body;
  const paramsReq = req.params;
  const userSelf = req.user;
  let dataRes = null;

  async.waterfall([
    (done) => {
      /* Kiểm tra đầu vào */

      if (userSelf) {
        BusinessCard
          .findOne({
            _id: paramsReq.id,
            'reports.userID': userSelf._id,
          })
          .exec((err, businessCard) => {
            if (err) {
              // Danh thiếp không tồn tại
              dataRes = {
                status: 404,
                bodyRes: {
                  code: 'businessCard.notExist',
                },
              };

              done(err, dataRes);
            } else if (businessCard) {
              // Tố cáo danh thiếp tồn tại
              dataRes = {
                status: 401,
                bodyRes: {
                  code: 'businessCard.report.exist',
                },
              };

              done(new Error('Exist business card report'), dataRes);
            } else {
              done(null);
            }
          });
      } else {
        // Chưa đăng nhập người dùng
        dataRes = {
          status: 401,
          bodyRes: {
            code: 'user.signin.notExist',
          },
        };

        done(new Error('User not signin'), dataRes);
      }
    }, (done) => {
      /* Cập nhật danh thiếp */

      BusinessCard
        .findById(paramsReq.id)
        .exec((err, businessCard) => {
          if (err || !businessCard) {
            // Danh thiếp không tồn tại
            dataRes = {
              status: 404,
              bodyRes: {
                code: 'businessCard.notExist',
              },
            };

            done(err || new Error('Not business card exists'), dataRes);
          } else {
            businessCard.addReport(userSelf._id, bodyReq.report.reason);
            businessCard.save((err) => {
              if (err) {
                // Tố cáo danh thiếp không thành công
                dataRes = {
                  status: 400,
                  bodyRes: {
                    code: 'businessCard.report.failed',
                  },
                };

                done(err, dataRes);
              } else {
                done(err, businessCard);
              }
            });
          }
        });
    }, (businessCard, done) => {
      /* Tạo thông báo */

      dataRes = {
        status: 200,
        bodyRes: {
          code: 'businessCard.report.success',
        },
      };

      const activityHistory = new ActivityHistory({
        userID: userSelf._id,
        toURL: businessCard._id,
        type: constant.BUSINESS_CARD,
        result: constant.SUCCESS,
        content: 'businessCard.report.is',
      });
      activityHistory.save((err) => {
        if (err) {
          logger.logError(
            'business-cards.action.server.controller',
            'report',
            constant.ACTIVITY_HISTORY,
            activityHistory,
            err
          );
        }
      });

      const notification = new Notification({
        receiveID: businessCard.userID,
        sendID: userSelf._id,
        toURL: businessCard._id,
        type: constant.BUSINESS_CARD,
        result: constant.WARNING,
        content: 'businessCard.report.exist',
        background: businessCard.getBackground()
      });
      notification.save((err) => {
        if (err) {
          logger.logError(
            'business-cards.action.server.controller',
            'report',
            constant.NOTIFICATION,
            notification,
            err
          );
        } else {
          dataRes.bodyRes.sockets = [
            createSocketData(
              [SOCKET_NOTIFICATION_PUSH],
              notification.receiveID,
              { notificationID: notification._id }
            )
          ];
        }

        done(null, dataRes);
      });
    }], (err, dataRes) => {
      dataRes.bodyRes.businessCardID = paramsReq.id;

      return res
        .status(dataRes.status)
        .send(dataRes.bodyRes)
        .end();
    });
};
module.exports.report = report;

/**
 * @overview Bỏ thích danh thiếp
 *
 * @param {Request} req
 * @param {Response} res
 * @param {Function} next
 *
 * @input  params { id: String }
 * @output        { code: String, businessCardID: ObjectId }
 */
const unLike = (req, res, next) => {
  const paramsReq = req.params;
  const userSelf = req.user;
  let dataRes = null;

  async.waterfall([
    (done) => {
      /* Kiểm tra đầu vào */

      if (userSelf) {
        BusinessCard.findOneAndUpdate(
          {
            _id: paramsReq.id,
            'likes.userID': userSelf._id,
          },
          {
            $pull: {
              likes: {
                userID: userSelf._id,
              },
            },
          },
          {
            new: true,
          }
        )
          .exec((err, businessCard) => {
            if (err || !businessCard) {
              // Thích danh thiếp không tồn tại
              dataRes = {
                status: 404,
                bodyRes: {
                  code: 'businessCard.like.notExist',
                },
              };

              done(err || new Error('Not business card like exists'), dataRes);
            } else {
              done(null, businessCard);
            }
          });
      } else {
        // Chưa đăng nhập người dùng
        dataRes = {
          status: 401,
          bodyRes: {
            code: 'user.signin.notExist',
          },
        };

        done(new Error('User not signin'), dataRes);
      }
    }, (businessCard, done) => {
      /* Cập nhật danh thiếp và người dùng */

      businessCard.unLike();
      businessCard.save((err) => {
        if (err) {
          // Bỏ thích danh thiếp không thành công
          dataRes = {
            status: 400,
            bodyRes: {
              code: 'businessCard.unlike.failed',
            },
          };

          done(err, dataRes);
        } else {
          const activityHistory = new ActivityHistory({
            userID: userSelf._id,
            toURL: businessCard._id,
            type: constant.BUSINESS_CARD,
            result: constant.SUCCESS,
            content: 'businessCard.unlike.is',
          });

          activityHistory.save((err) => {
            if (err) {
              logger.logError(
                'business-cards.action.server.controller',
                'unLike',
                constant.ACTIVITY_HISTORY,
                activityHistory,
                err
              );
            }
          });

          dataRes = {
            status: 200,
            bodyRes: {
              code: 'businessCard.unlike.success'
            },
          };

          done(null, dataRes);
        }
      });
    }], (err, dataRes) => {
      dataRes.bodyRes.businessCardID = paramsReq.id;

      return res
        .status(dataRes.status)
        .send(dataRes.bodyRes)
        .end();
    });
};
module.exports.unLike = unLike;

/**
 * @overview Thích danh thiếp
 *
 * @param {Request} req
 * @param {Response} res
 * @param {Function} next
 *
 * @input  params { id: String }
 * @output        {
 *                    code: String,
 *                    businessCardID: ObjectId,
 *                    sockets?: [{Object}],
 *                 }
 */
const like = (req, res, next) => {
  const paramsReq = req.params;
  const userSelf = req.user;
  let dataRes = null;

  async.waterfall([
    (done) => {
      /* Kiểm tra đầu vào */

      if (userSelf) {
        BusinessCard
          .findOne({
            _id: paramsReq.id,
            'likes.userID': userSelf._id,
          })
          .exec((err, businessCard) => {
            if (err) {
              // Danh thiếp không tồn tại
              dataRes = {
                status: 404,
                bodyRes: {
                  code: 'businessCard.notExist',
                },
              };

              done(err, dataRes);
            } else if (businessCard) {
              // Thích danh thiếp tồn tại
              dataRes = {
                status: 401,
                bodyRes: {
                  code: 'businessCard.like.exist',
                },
              };

              done(new Error('Exist business card like'), dataRes);
            } else {
              done(null);
            }
          });
      } else {
        // Chưa đăng nhập người dùng
        dataRes = {
          status: 401,
          bodyRes: {
            code: 'user.signin.notExist',
          },
        };

        done(new Error('User not signin'), dataRes);
      }
    }, (done) => {
      /* Cập nhật danh thiếp */

      BusinessCard
        .findById(paramsReq.id)
        .exec((err, businessCard) => {
          if (err || !businessCard) {
            // Danh thiếp không tồn tại
            dataRes = {
              status: 404,
              bodyRes: {
                code: 'businessCard.notExist',
              },
            };

            done(err || new Error('Not business card exists'), dataRes);
          } else {
            businessCard.addLike(userSelf._id);
            businessCard.save((err) => {
              if (err) {
                // Thích danh thiếp không thành công
                dataRes = {
                  status: 400,
                  bodyRes: {
                    code: 'businessCard.like.failed',
                  },
                };
              } else {
                dataRes = businessCard;
              }

              done(err, dataRes);
            });
          }
        });
    }, (businessCard, done) => {
      /* Tạo thông báo */

      dataRes = {
        status: 200,
        bodyRes: {
          code: 'businessCard.like.success'
        }
      };

      const activityHistory = new ActivityHistory({
        userID: userSelf._id,
        toURL: businessCard._id,
        type: constant.BUSINESS_CARD,
        result: constant.SUCCESS,
        content: 'businessCard.like.is',
      });

      activityHistory.save((err) => {
        if (err) {
          logger.logError(
            'business-cards.action.server.controller',
            'like',
            constant.ACTIVITY_HISTORY,
            activityHistory,
            err
          );
        }
      });

      if (!userSelf.equalsByID(businessCard.userID)) {
        const notification = new Notification({
          receiveID: businessCard.userID,
          sendID: userSelf._id,
          toURL: businessCard._id,
          type: constant.BUSINESS_CARD,
          result: constant.INFO,
          content: 'businessCard.like.exist',
          background: businessCard.getBackground()
        });
        notification.save((err) => {
          if (err) {
            logger.logError(
              'business-cards.action.server.controller',
              'like',
              constant.NOTIFICATION,
              notification,
              err
            );
          } else {
            dataRes.bodyRes.sockets = [
              createSocketData(
                [SOCKET_NOTIFICATION_PUSH],
                notification.receiveID,
                { notificationID: notification._id }
              )
            ];
          }

          done(null, dataRes);
        });
      } else {
        done(null, dataRes);
      }
    }], (err, dataRes) => {
      dataRes.bodyRes.businessCardID = paramsReq.id;

      return res
        .status(dataRes.status)
        .send(dataRes.bodyRes)
        .end();
    });
};
module.exports.like = like;

/**
 * @overview Lấy nhận xét danh thiếp
 *
 * @param {Request} req
 * @param {Response} res
 * @param {Function} next
 *
 * @input  params { id: String }
 *         query  { max: Number }
 * @output        {
 *                  code: String,
 *                  businessCardID: ObjectId
 *                  max: Number,
 *                  comments: [{Object}]
 *                }
 */
const comments = (req, res, next) => {
  const paramsReq = req.params;
  const queryReq = req.query;
  const userSelf = req.user;
  let dataRes = null;

  if (userSelf) {
    BusinessCard
      .findById(paramsReq.id)
      .exec((err, businessCard) => {
        if (err || !businessCard) {
          // Danh thiếp không tồn tại
          dataRes = {
            status: 404,
            bodyRes: {
              code: 'businessCard.notExist',
              businessCardID: paramsReq.id
            },
          };

          return res
            .status(dataRes.status)
            .send(dataRes.bodyRes)
            .end();
        }

        let { max } = queryReq;
        if (max === -1 || max > businessCard.comments.length) {
          max = businessCard.comments.length;
        }
        const limit = req.app.locals.config.loadLimit;
        const min = (max - limit) > 0 ? (max - limit) : 0;

        const currentComments = _.slice(businessCard.comments, min, max);

        if (currentComments.length) {
          async.map(currentComments, (comment, done) => {
            User
              .findById(comment.userID, `-${constant.SALT} -${constant.PASSWORD}`)
              .exec((err, user) => {
                if (user) {
                  done(null, {
                    user: user.getShortcutsInfo(),
                    comment,
                    isOwner: userSelf.equalsByID(user._id)
                  });
                } else {
                  done(null, {
                    user: User.getShortcutsInfoUndefined(),
                    comment,
                    isOwner: false
                  });
                }
              });
          }, (err, resultComments) => {
            dataRes = {
              status: 200,
              bodyRes: {
                code: 'businessCard.comment.exist',
                businessCardID: paramsReq.id,
                comments: resultComments,
                max: min,
              },
            };

            return res
              .status(dataRes.status)
              .send(dataRes.bodyRes)
              .end();
          });
        } else {
          // Nhận xét danh thiếp không tồn tại
          dataRes = {
            status: 404,
            bodyRes: {
              code: 'businessCard.comment.notExist',
              businessCardID: paramsReq.id
            },
          };

          return res
            .status(dataRes.status)
            .send(dataRes.bodyRes)
            .end();
        }
      });
  } else {
    // Chưa đăng nhập người dùng
    dataRes = {
      status: 401,
      bodyRes: {
        code: 'user.signin.notExist',
        businessCardID: paramsReq.id
      },
    };

    return res
      .status(dataRes.status)
      .send(dataRes.bodyRes)
      .end();
  }
};
module.exports.comments = comments;

/**
 * @overview Nhận xét danh thiếp
 *
 * @param {Request} req
 * @param {Response} res
 * @param {Function} next
 *
 * @input  params { id: String }
 *         body   { comment: String }
 * @output        {
 *                  code: String,
 *                  businessCardID: ObjectId
 *                  comment: {
 *                    user: {User},
 *                    comment: {Object},
 *                  }
 *                  sockets?: [{Object}]
 *                 }
 */
const comment = (req, res, next) => {
  const bodyReq = req.body;
  const paramsReq = req.params;
  const userSelf = req.user;
  let dataRes = null;

  async.waterfall([
    (done) => {
      /* Kiểm tra đầu vào */

      if (userSelf) {
        if (bodyReq.comment) {
          BusinessCard
            .findById(paramsReq.id)
            .exec((err, businessCard) => {
              if (err || !businessCard) {
                // Danh thiếp không tồn tại
                dataRes = {
                  status: 404,
                  bodyRes: {
                    code: 'businessCard.notExist',
                  },
                };

                done(err || new Error('Not business card exists'), dataRes);
              } else {
                done(null, businessCard);
              }
            });
        } else {
          // Bad request.body
          dataRes = {
            status: 400,
            bodyRes: {
              code: 'data.notVaild',
            },
          };

          done(new Error('Bad request.body'), dataRes);
        }
      } else {
        // Chưa đăng nhập người dùng
        dataRes = {
          status: 401,
          bodyRes: {
            code: 'user.signin.notExist',
          },
        };

        done(new Error('User not signin'), dataRes);
      }
    }, (businessCard, done) => {
      /* Cập nhật danh thiếp */

      const comment = businessCard.addComment(userSelf._id, bodyReq.comment);
      businessCard.save((err) => {
        if (err) {
          // Nhận xét danh thiếp không thành công
          dataRes = {
            status: 400,
            bodyRes: {
              code: 'businessCard.comment.failed',
            },
          };

          done(err, dataRes);
        } else {
          done(null, businessCard, comment);
        }
      });
    }, (businessCard, comment, done) => {
      /* Tạo thông báo */

      dataRes = {
        status: 200,
        bodyRes: {
          code: 'businessCard.comment.success',
          comment: {
            comment,
            user: userSelf.getShortcutsInfo(),
            isOwner: true
          }
        },
      };

      const activityHistory = new ActivityHistory({
        userID: userSelf._id,
        toURL: businessCard._id,
        type: constant.BUSINESS_CARD,
        result: constant.SUCCESS,
        content: 'businessCard.comment.is',
      });

      activityHistory.save((err) => {
        if (err) {
          logger.logError(
            'business-cards.action.server.controller',
            'comment',
            constant.ACTIVITY_HISTORY,
            activityHistory,
            err
          );
        }
      });

      if (!userSelf.equalsByID(businessCard.userID)) {
        const notification = new Notification({
          receiveID: businessCard.userID,
          sendID: userSelf._id,
          toURL: businessCard._id,
          type: constant.BUSINESS_CARD,
          result: constant.INFO,
          content: 'businessCard.comment.exist',
          background: businessCard.getBackground()
        });
        notification.save((err) => {
          if (err) {
            logger.logError(
              'business-cards.action.server.controller',
              'comment',
              constant.NOTIFICATION,
              notification,
              err
            );
          } else {
            dataRes.bodyRes.sockets = [
              createSocketData(
                [SOCKET_NOTIFICATION_PUSH],
                notification.receiveID,
                { notificationID: notification._id }
              )
            ];
          }

          done(null, dataRes);
        });
      } else {
        done(null, dataRes);
      }
    }], (err, dataRes) => {
      dataRes.bodyRes.businessCardID = paramsReq.id;

      return res
        .status(dataRes.status)
        .send(dataRes.bodyRes)
        .end();
    });
};
module.exports.comment = comment;

/**
 * @overview Thay đổi nhận xét danh thiếp
 *
 * @param {Request} req
 * @param {Response} res
 * @param {Function} next
 *
 * @input  params { id: String }
 *         body   { comment: String }
 *         query  { commentID: String }
 * @output        {
 *                  code: String,
 *                  businessCardID: ObjectId
 *                  commentID: ObjectId
 *                  comment?: {Object}
 *                }
 */
const changeComment = (req, res, next) => {
  const bodyReq = req.body;
  const paramsReq = req.params;
  const queryReq = req.query;
  const userSelf = req.user;
  let dataRes = null;

  async.waterfall([
    (done) => {
      /* Kiểm tra đầu vào */

      if (userSelf) {
        if (bodyReq.comment) {
          const changeDate = Date.now();
          const isChange = true;

          BusinessCard.findOneAndUpdate(
            {
              _id: paramsReq.id,
              comments: {
                $elemMatch: {
                  _id: queryReq.commentID,
                  userID: userSelf._id,
                },
              },
            },
            {
              $set: {
                'comments.$.msg': bodyReq.comment,
                'comments.$.change': {
                  is: isChange,
                  date: changeDate,
                },
              },
            },
            {
              new: true,
            }
          )
            .exec((err, businessCard) => {
              if (err || !businessCard) {
                // Thay đổi nhận xét danh thiếp không thành công
                dataRes = {
                  status: 404,
                  bodyRes: {
                    code: 'businessCard.comment.change.failed',
                  },
                };

                done(err || new Error('Not business card exists'), dataRes);
              } else {
                const activityHistory = new ActivityHistory({
                  userID: userSelf._id,
                  toURL: businessCard._id,
                  type: constant.BUSINESS_CARD,
                  result: constant.SUCCESS,
                  content: 'businessCard.comment.change.is',
                });

                activityHistory.save((err) => {
                  if (err) {
                    logger.logError(
                      'business-cards.action.server.controller',
                      'changeComment',
                      constant.ACTIVITY_HISTORY,
                      activityHistory,
                      err
                    );
                  }
                });

                dataRes = {
                  status: 200,
                  bodyRes: {
                    code: 'businessCard.comment.change.success',
                    comment: {
                      msg: bodyReq.comment,
                      change: {
                        is: isChange,
                        date: changeDate,
                      },
                    },
                  },
                };

                done(null, dataRes);
              }
            });
        } else {
          // Bad request.body
          dataRes = {
            status: 400,
            bodyRes: {
              code: 'data.notVaild',
            },
          };

          done(new Error('Bad request.body'), dataRes);
        }
      } else {
        // Chưa đăng nhập người dùng
        dataRes = {
          status: 401,
          bodyRes: {
            code: 'user.signin.notExist',
          },
        };

        done(new Error('User not signin'), dataRes);
      }
    }], (err, dataRes) => {
      dataRes.bodyRes.businessCardID = paramsReq.id;
      dataRes.bodyRes.commentID = queryReq.commentID;

      return res
        .status(dataRes.status)
        .send(dataRes.bodyRes)
        .end();
    });
};
module.exports.changeComment = changeComment;

/**
 * @overview Xoá nhận xét danh thiếp
 *
 * @param {Request} req
 * @param {Response} res
 * @param {Function} next
 *
 * @input  params { id: String }
 *         query  { commentID: String }
 * @output        {
 *                  code: String,
 *                  businessCardID: ObjectId
 *                  commentID: ObjectId
 *                }
 */
const deleteComment = (req, res, next) => {
  const paramsReq = req.params;
  const queryReq = req.query;
  const userSelf = req.user;
  let dataRes = null;

  async.waterfall([
    (done) => {
      /* Kiểm tra đầu vào */

      if (userSelf) {
        BusinessCard.findOneAndUpdate(
          {
            _id: paramsReq.id,
            comments: {
              $elemMatch: {
                _id: queryReq.commentID,
                userID: userSelf._id,
              },
            },
          },
          {
            $pull: {
              comments: {
                _id: queryReq.commentID,
                userID: userSelf._id,
              },
            },
          },
          {
            new: true,
          }
        )
          .exec((err, businessCard) => {
            if (err || !businessCard) {
              // Xoá nhận xét danh thiếp không thành công
              dataRes = {
                status: 404,
                bodyRes: {
                  code: 'businessCard.comment.delete.failed',
                },
              };

              done(err || new Error('Not business card exists'), dataRes);
            } else {
              const activityHistory = new ActivityHistory({
                userID: userSelf._id,
                toURL: businessCard._id,
                type: constant.BUSINESS_CARD,
                result: constant.SUCCESS,
                content: 'businessCard.comment.delete.is',
              });

              activityHistory.save((err) => {
                if (err) {
                  logger.logError(
                    'business-cards.action.server.controller',
                    'deleteComment',
                    constant.ACTIVITY_HISTORY,
                    activityHistory,
                    err
                  );
                }
              });

              dataRes = {
                status: 200,
                bodyRes: {
                  code: 'businessCard.comment.delete.success',
                },
              };

              done(null, dataRes);
            }
          });
      } else {
        // Chưa đăng nhập người dùng
        dataRes = {
          status: 401,
          bodyRes: {
            code: 'user.signin.notExist',
          },
        };

        done(new Error('User not signin'), dataRes);
      }
    }], (err, dataRes) => {
      dataRes.bodyRes.businessCardID = paramsReq.id;
      dataRes.bodyRes.commentID = queryReq.commentID;

      return res
        .status(dataRes.status)
        .send(dataRes.bodyRes)
        .end();
    });
};
module.exports.deleteComment = deleteComment;

/**
 * @overview Giao dịch danh thiếp
 *
 * @param {Request} req
 * @param {Response} res
 * @param {Function} next
 *
 * @input  params { id: String }
 *         body
 * {
 *   costs: {
 *     room?: {
 *       value: String,
 *       moneys: Number
 *     },
 *     service: {
 *       value: String,
 *       moneys: Number
 *     },
 *     currencyUnit: String
 *   },
 *   note?: String,
 *   timer: Date
 * }
 * @output {
 *           code: String,
 *           businessCardDealHistoryID?: ObjectId
 *           businessCardID: ObjectId,
 *           sockets?: [{Object}]
 *         }
 */
const deal = (req, res, next) => {
  const bodyReq = req.body;
  const paramsReq = req.params;
  const userSelf = req.user;
  let dataRes = null;

  async.waterfall([
    (done) => {
      /* Kiểm tra đầu vào */

      if (userSelf) {
        BusinessCard
          .findOne({ userID: userSelf._id })
          .exec((err, businessCard) => {
            if (userSelf.getAuthorizedLevelByRoles() !== User.getAuthorizedLevelByRoles(constant.PROVIDER) && !businessCard) {
              BusinessCard
                .findById(paramsReq.id)
                .exec((err, businessCard) => {
                  if (err || !businessCard) {
                    // Danh thiếp không tồn tại
                    dataRes = {
                      status: 404,
                      bodyRes: {
                        code: 'businessCard.notExist',
                      },
                    };

                    done(err || new Error('Not business card exists'), dataRes);
                  } else {
                    BusinessCardDealHistory
                      .findOne({
                        businessCardID: businessCard._id,
                        userID: userSelf._id,
                        'result.is': constant.WAIT,
                      })
                      .exec((err, businessCardDealHistory) => {
                        if (businessCardDealHistory) {
                          // Lịch sử giao dịch danh thiếp tồn tại
                          dataRes = {
                            status: 401,
                            bodyRes: {
                              code: 'businessCardDealHistory.exist',
                            },
                          };

                          done(err || new Error('Business card deal historys exists '), dataRes);
                        } else {
                          dataRes = businessCard;
                          done(null, dataRes);
                        }
                      });
                  }
                });
            } else {
              // Người dùng không có quyền
              dataRes = {
                status: 401,
                bodyRes: {
                  code: 'user.permission.notExist',
                },
              };

              done(new Error('Not rermission'), dataRes);
            }
          });
      } else {
        // Chưa đăng nhập người dùng
        dataRes = {
          status: 401,
          bodyRes: {
            code: 'user.signin.notExist',
          },
        };

        done(new Error('User not signin'), dataRes);
      }
    }, (businessCard, done) => {
      /* Tạo lịch sử giao dịch danh thiếp */

      let businessCardDealHistory = BusinessCardDealHistory.getDefaultValue(userSelf._id, businessCard, bodyReq);
      if (!_.isEmpty(businessCardDealHistory)) {
        businessCardDealHistory = new BusinessCardDealHistory(businessCardDealHistory);
        businessCardDealHistory.save((err) => {
          if (err) {
            // Tạo lịch sử giao dịch danh thiếp không thành công
            dataRes = {
              status: 400,
              bodyRes: {
                code: 'businessCardDealHistory.create.failed',
              },
            };

            done(err, dataRes);
          } else {
            done(null, businessCard, businessCardDealHistory);
          }
        });
      } else {
        // Bad request.body
        dataRes = {
          status: 400,
          bodyRes: {
            code: 'data.notVaild',
          },
        };

        done(new Error('Bad request.body'), dataRes);
      }
    }, (businessCard, businessCardDealHistory, done) => {
      /* Tạo thông báo */

      User
        .findById(businessCardDealHistory.providerID)
        .exec((err, user) => {
          if (
            user
            && user.online.date > (Date.now() - constant.ENUM.TIME.ONE_MINUTES * 30)
            && user.checkSendSMS()
          ) {
            twilio.sendSMS(
              constant.INVITE,
              user.numberPhone,
              {
                language: user.settings.general.language,
                type: constant.BUSINESS_CARD_DEAL_HISTORY
              }
            );
          }
        });

      dataRes = {
        status: 200,
        bodyRes: {
          code: 'businessCardDealHistory.create.success',
          businessCardDealHistoryID: businessCardDealHistory._id,
          sockets: [
            createSocketData(
              [SOCKET_BUSINESS_CARD_DEAL_HISTORY_PUSH],
              businessCardDealHistory.userID,
              { businessCardDealHistoryID: businessCardDealHistory._id }
            ),
            createSocketData(
              [SOCKET_BUSINESS_CARD_DEAL_HISTORY_PUSH],
              businessCardDealHistory.providerID,
              { businessCardDealHistoryID: businessCardDealHistory._id }
            ),
          ],
        },
      };

      const activityHistoryUser = new ActivityHistory({
        userID: businessCardDealHistory.userID,
        toURL: businessCardDealHistory._id,
        type: constant.BUSINESS_CARD_DEAL_HISTORY,
        result: constant.SUCCESS,
        content: 'businessCardDealHistory.create.is',
      });
      activityHistoryUser.save((err) => {
        if (err) {
          logger.logError(
            'business-cards.action.server.controller',
            'deal',
            constant.ACTIVITY_HISTORY,
            activityHistoryUser,
            err
          );
        }
      });

      const activityHistoryProvider = new ActivityHistory({
        userID: businessCardDealHistory.providerID,
        toURL: businessCardDealHistory._id,
        type: constant.BUSINESS_CARD_DEAL_HISTORY,
        result: constant.SUCCESS,
        content: 'businessCardDealHistory.exist',
      });
      activityHistoryProvider.save((err) => {
        if (err) {
          logger.logError(
            'business-cards.action.server.controller',
            'deal',
            constant.ACTIVITY_HISTORY,
            activityHistoryProvider,
            err
          );
        }
      });

      const notification = new Notification({
        receiveID: businessCardDealHistory.providerID,
        sendID: userSelf._id,
        toURL: businessCardDealHistory._id,
        type: constant.BUSINESS_CARD_DEAL_HISTORY,
        result: constant.INFO,
        content: 'businessCardDealHistory.exist',
        background: businessCard.getBackground()
      });
      notification.save((err) => {
        if (err) {
          logger.logError(
            'business-cards.action.server.controller',
            'deal',
            constant.NOTIFICATION,
            notification,
            err
          );
        } else {
          dataRes.bodyRes.sockets.push(
            createSocketData(
              [SOCKET_NOTIFICATION_PUSH],
              businessCardDealHistory.providerID,
              { notificationID: notification._id }
            )
          );
        }

        done(null, dataRes);
      });
    }], (err, dataRes) => {
      dataRes.bodyRes.businessCardID = paramsReq.id;

      return res
        .status(dataRes.status)
        .send(dataRes.bodyRes)
        .end();
    });
};
module.exports.deal = deal;
