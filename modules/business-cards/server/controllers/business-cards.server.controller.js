const _ = require('lodash');
const profile = require('./business-cards/business-cards.profile.server.controller');
const action = require('./business-cards/business-cards.action.server.controller');

module.exports = _.assignIn(
  profile,
  action
);
