const path = require('path');
const mongoose = require('mongoose');
const async = require('async');
const logger = require(path.resolve('./config/lib/logger'));
const constant = require(path.resolve('./commons/utils/constant'));
const { createSocketData } = require(path.resolve('./commons/utils/middleware'));
const { SOCKET_NOTIFICATION_PUSH } = require(path.resolve('./commons/modules/notifications/datas/socket.data'));
const { SOCKET_BUSINESS_CARD_DEAL_HISTORY_UPDATE, SOCKET_DETAIL_BUSINESS_CARD_DEAL_HISTORY_UPDATE } = require(path.resolve('./commons/modules/business-cards/datas/socket.data'));

const BusinessCardDealHistory = mongoose.model('BusinessCardDealHistory');
const ActivityHistory = mongoose.model('ActivityHistory');
const Notification = mongoose.model('Notification');
const BusinessCard = mongoose.model('BusinessCard');

/**
 * @overview Chấp nhận lịch sử giao dịch danh thiếp
 *
 * @param {Request} req
 * @param {Response} res
 * @param {Function} next
 * @param {Function} getInfo
 *
 * @input params { id: String }
 *        body   { accept: { is: Boolean, reason?: String } }
 * @output       {
 *                  code: String,
 *                  sockets: [{Object}],
 *                  businessCardDealHistory: {BusinessCardDealHistory}
 *               }
 */
const accept = (req, res, next) => {
  const bodyReq = req.body;
  const paramsReq = req.params;
  const userSelf = req.user;
  let dataRes = null;

  async.waterfall([
    (done) => {
      /* Kiểm tra đầu vào */

      if (userSelf) {
        BusinessCardDealHistory
          .findOne({
            $or: [{
              _id: paramsReq.id,
              'result.is': constant.WAIT,
              userID: userSelf._id,
            }, {
              _id: paramsReq.id,
              'result.is': constant.WAIT,
              providerID: userSelf._id,
            }],
          })
          .exec((err, businessCardDealHistory) => {
            if (err || !businessCardDealHistory) {
              // Lịch sử giao dịch danh thiếp không tồn tại
              dataRes = {
                status: 404,
                bodyRes: {
                  code: 'businessCardDealHistory.notExist',
                },
              };

              done(err || new Error('Not business card deal history exists'), dataRes);
            } else if (businessCardDealHistory.checkExpires()) {
              businessCardDealHistory.save((err) => {
                dataRes = {
                  status: 401,
                  bodyRes: {
                    code: 'businessCardDealHistory.accept.failed',
                    businessCardDealHistory: businessCardDealHistory.getInfoByAuthorizedLevel(userSelf),
                  },
                };

                done(err || new Error('Expires business card deal history'), dataRes);
              });
            } else {
              done(null, businessCardDealHistory);
            }
          });
      } else {
        // Chưa đăng nhập người dùng
        dataRes = {
          status: 401,
          bodyRes: {
            code: 'user.signin.notExist',
          },
        };

        done(new Error('User not signin'), dataRes);
      }
    }, (businessCardDealHistory, done) => {
      /* Cập nhật lịch sử giao dịch danh thiếp */

      businessCardDealHistory.validateAccept(
        userSelf._id,
        bodyReq.accept.is,
        bodyReq.accept.reason
      );
      businessCardDealHistory.save((err) => {
        if (err) {
          // Chấp nhận lịch sử giao dịch danh thiếp không thành công
          dataRes = {
            status: 400,
            bodyRes: {
              code: 'businessCardDealHistory.accept.failed',
            },
          };
        } else {
          dataRes = businessCardDealHistory;
        }

        done(err, dataRes);
      });
    }, (businessCardDealHistory, done) => {
      /* Tạo thông báo */

      dataRes = {
        status: 200,
        bodyRes: {
          code: `businessCardDealHistory.accept.${bodyReq.accept.is}`,
          businessCardDealHistory: businessCardDealHistory.getInfoByAuthorizedLevel(userSelf),
          sockets: [
            createSocketData(
              [SOCKET_DETAIL_BUSINESS_CARD_DEAL_HISTORY_UPDATE],
              businessCardDealHistory._id,
              { businessCardDealHistoryID: businessCardDealHistory._id }
            ),
            createSocketData(
              [SOCKET_BUSINESS_CARD_DEAL_HISTORY_UPDATE],
              businessCardDealHistory.providerID,
              { businessCardDealHistoryID: businessCardDealHistory._id }
            ),
            createSocketData(
              [SOCKET_BUSINESS_CARD_DEAL_HISTORY_UPDATE],
              businessCardDealHistory.userID,
              { businessCardDealHistoryID: businessCardDealHistory._id }
            )
          ],
        },
      };

      const activityHistory = new ActivityHistory({
        userID: userSelf._id,
        toURL: businessCardDealHistory._id,
        type: constant.BUSINESS_CARD_DEAL_HISTORY,
        result: constant.SUCCESS,
        content: `businessCardDealHistory.accept.${bodyReq.accept.is}`,
      });
      activityHistory.save((err) => {
        if (err) {
          logger.logError(
            'business-cards.authorization.server.controller',
            'accept',
            constant.ACTIVITY_HISTORY,
            activityHistory,
            err
          );
        }
      });

      const receiveID = userSelf.equalsByID(businessCardDealHistory.userID)
        ? businessCardDealHistory.providerID
        : businessCardDealHistory.userID;

      const notification = new Notification({
        receiveID,
        sendID: userSelf._id,
        toURL: businessCardDealHistory._id,
        type: constant.BUSINESS_CARD_DEAL_HISTORY,
        result: constant.INFO,
        content: `businessCardDealHistory.accept.${bodyReq.accept.is}`,
      });
      notification.save((err) => {
        if (err) {
          logger.logError(
            'deal-historys.authorization.server.controller',
            'accept',
            constant.NOTIFICATION,
            notification,
            err
          );
        } else {
          dataRes.bodyRes.sockets.push(
            createSocketData(
              [SOCKET_NOTIFICATION_PUSH],
              receiveID,
              { notificationID: notification._id }
            )
          );
        }

        done(null, dataRes);
      });
    }], (err, dataRes) => {
      return res
        .status(dataRes.status)
        .send(dataRes.bodyRes)
        .end();
    });
};
module.exports.accept = accept;

/**
 * @overview Lấy mã thông báo lịch sử giao dịch danh thiếp
 *
 * @param {Request} req
 * @param {Response} res
 * @param {Function} next
 * @param {Function} getInfo
 *
 * @input params { id: String }
 * @output       {
 *                  code: String,
 *                  sockets: [{Object}],
 *                  businessCardDealHistory: {BusinessCardDealHistory}
 *               }
 */
const getToken = (req, res, next) => {
  const paramsReq = req.params;
  const userSelf = req.user;
  let dataRes = null;

  async.waterfall([
    (done) => {
      /* Kiểm tra đầu vào */

      if (userSelf) {
        BusinessCardDealHistory
          .findOne({
            _id: paramsReq.id,
            userID: userSelf._id,
            'result.is': constant.WAIT,
            status: constant.TOKEN,
          })
          .exec((err, businessCardDealHistory) => {
            if (err || !businessCardDealHistory) {
              // Lịch sử giao dịch danh thiếp không tồn tại
              dataRes = {
                status: 404,
                bodyRes: {
                  code: 'businessCardDealHistory.notExist',
                },
              };

              done(err || new Error('Not business card deal history exists'), dataRes);
            } else if (businessCardDealHistory.checkExpires()) {
              businessCardDealHistory.save((err) => {
                dataRes = {
                  status: 401,
                  bodyRes: {
                    code: 'businessCardDealHistory.accept.failed',
                    businessCardDealHistory: businessCardDealHistory.getInfoByAuthorizedLevel(userSelf),
                  },
                };

                done(err || new Error('Expires business card deal history'), dataRes);
              });
            } else {
              done(null, businessCardDealHistory);
            }
          });
      } else {
        // Chưa đăng nhập người dùng
        dataRes = {
          status: 401,
          bodyRes: {
            code: 'user.signin.notExist',
          },
        };

        done(new Error('User not signin'), dataRes);
      }
    }, (businessCardDealHistory, done) => {
      /* Cập nhật lịch sử giao dịch danh thiếp */

      businessCardDealHistory
        .createToken()
        .then((token) => {
          businessCardDealHistory.save((err) => {
            if (err) {
              dataRes = {
                status: 400,
                bodyRes: {
                  code: 'businessCardDealHistory.token.create.failed',
                },
              };
            } else {
              dataRes = {
                status: 200,
                bodyRes: {
                  code: 'businessCardDealHistory.token.create.success',
                  businessCardDealHistory: businessCardDealHistory.getInfoByAuthorizedLevel(userSelf),
                  sockets: [
                    createSocketData(
                      [SOCKET_DETAIL_BUSINESS_CARD_DEAL_HISTORY_UPDATE],
                      businessCardDealHistory._id,
                      { businessCardDealHistoryID: businessCardDealHistory._id }
                    ),
                    createSocketData(
                      [SOCKET_BUSINESS_CARD_DEAL_HISTORY_UPDATE],
                      businessCardDealHistory.userID,
                      { businessCardDealHistoryID: businessCardDealHistory._id }
                    ),
                    createSocketData(
                      [SOCKET_BUSINESS_CARD_DEAL_HISTORY_UPDATE],
                      businessCardDealHistory.providerID,
                      { businessCardDealHistoryID: businessCardDealHistory._i }
                    ),
                  ],
                },
              };

              const activityHistory = new ActivityHistory({
                userID: userSelf._id,
                toURL: businessCardDealHistory._id,
                type: constant.BUSINESS_CARD_DEAL_HISTORY,
                result: constant.SUCCESS,
                content: 'businessCardDealHistory.token.create.is',
              });
              activityHistory.save((err) => {
                if (err) {
                  logger.logError(
                    'business-cards.authorization.server.controller',
                    'getToken',
                    constant.ACTIVITY_HISTORY,
                    activityHistory,
                    err
                  );
                }
              });
            }

            done(null, dataRes);
          });
        })
        .catch((err) => {
          dataRes = {
            status: 400,
            bodyRes: {
              code: 'businessCardDealHistory.token.create.failed',
            },
          };

          done(err, dataRes);
        });
    }], (err, dataRes) => {
      return res
        .status(dataRes.status)
        .send(dataRes.bodyRes)
        .end();
    });
};
module.exports.getToken = getToken;

/**
 * @overview Xác thực mã thông báo lịch sử giao dịch danh thiếp
 *
 * @param {Request} req
 * @param {Response} res
 * @param {Function} next
 * @param {Function} getInfo
 *
 * @input params { id: String }
 *        body   { token: String }
 * @output       { code: String, sockets: [{Object}], businessCardDealHistory: {BusinessCardDealHistory} }
 */
const validateToken = (req, res, next) => {
  const bodyReq = req.body;
  const paramsReq = req.params;
  const userSelf = req.user;
  let dataRes = null;

  async.waterfall([
    (done) => {
      /* Kiểm tra đầu vào */

      if (userSelf) {
        BusinessCardDealHistory
          .findOne({
            _id: paramsReq.id,
            providerID: userSelf._id,
            'result.is': constant.WAIT,
            status: constant.VERIFY,
          })
          .exec((err, businessCardDealHistory) => {
            if (err || !businessCardDealHistory) {
              // Lịch sử giao dịch danh thiếp không tồn tại
              dataRes = {
                status: 404,
                bodyRes: {
                  code: 'businessCardDealHistory.notExist',
                },
              };

              done(err || new Error('Not business card deal history exists'), dataRes);
            } else if (businessCardDealHistory.checkExpires()) {
              businessCardDealHistory.save((err) => {
                dataRes = {
                  status: 401,
                  bodyRes: {
                    code: 'businessCardDealHistory.accept.failed',
                    businessCardDealHistory: businessCardDealHistory.getInfoByAuthorizedLevel(userSelf),
                  },
                };

                done(new Error('Failed verify business card deal history'), dataRes);
              });
            } else {
              done(null, businessCardDealHistory);
            }
          });
      } else {
        // Chưa đăng nhập người dùng
        dataRes = {
          status: 401,
          bodyRes: {
            code: 'user.signin.notExist',
          },
        };

        done(new Error('User not signin'), dataRes);
      }
    },
    (businessCardDealHistory, done) => {
      /* Cập nhật lịch sử giao dịch danh thiếp */

      if (businessCardDealHistory.validateToken(bodyReq.token)) {
        BusinessCard.findById(businessCardDealHistory.businessCardID, (err, businessCard) => {
          if (businessCard) {
            businessCard.plusPointSuccess();
            businessCard.save((err) => {
              if (err) {
                logger.logError(
                  'deal-historys.authorization.server.controller',
                  'validateToken',
                  constant.BUSINESS_CARD,
                  businessCard,
                  err
                );
              }
            });
          } else {
            logger.logError(
              'deal-historys.authorization.server.controller',
              'validateToken',
              constant.BUSINESS_CARD,
              businessCard,
              err
            );
          }
        });

        businessCardDealHistory.save((err) => {
          if (err) {
            // Xác thực mã thông báo sử giao dịch danh thiếp không thành công
            dataRes = {
              status: 400,
              bodyRes: {
                code: 'businessCardDealHistory.token.verify.failed',
              },
            };
          } else {
            dataRes = businessCardDealHistory;
          }

          done(err, dataRes);
        });
      } else {
        // Xác thực mã thông báo sử giao dịch danh thiếp không thành công
        dataRes = {
          status: 400,
          bodyRes: {
            code: 'businessCardDealHistory.token.verify.failed',
          },
        };

        done(new Error('Failed veviry token business card deal history'), dataRes);
      }
    }, (businessCardDealHistory, done) => {
      /* Tạo thông báo */

      dataRes = {
        status: 200,
        bodyRes: {
          code: 'businessCardDealHistory.token.verify.success',
          businessCardDealHistory: businessCardDealHistory.getInfoByAuthorizedLevel(userSelf),
          sockets: [
            createSocketData(
              [SOCKET_DETAIL_BUSINESS_CARD_DEAL_HISTORY_UPDATE],
              businessCardDealHistory._id,
              { businessCardDealHistoryID: businessCardDealHistory._id }
            ),
            createSocketData(
              [SOCKET_BUSINESS_CARD_DEAL_HISTORY_UPDATE],
              businessCardDealHistory.providerID,
              { businessCardDealHistoryID: businessCardDealHistory._id }
            ),
            createSocketData(
              [SOCKET_BUSINESS_CARD_DEAL_HISTORY_UPDATE],
              businessCardDealHistory.userID,
              { businessCardDealHistoryID: businessCardDealHistory._id }
            ),
          ],
        },
      };

      const activityHistory = new ActivityHistory({
        userID: userSelf._id,
        toURL: businessCardDealHistory._id,
        type: constant.BUSINESS_CARD_DEAL_HISTORY,
        result: constant.SUCCESS,
        content: 'businessCardDealHistory.token.verify.is',
      });

      activityHistory.save((err) => {
        if (err) {
          logger.logError(
            'business-cards.authorization.server.controller',
            'validateToken',
            constant.ACTIVITY_HISTORY,
            activityHistory,
            err
          );
        }
      });

      const notification = new Notification({
        receiveID: businessCardDealHistory.userID,
        sendID: businessCardDealHistory.providerID,
        toURL: businessCardDealHistory._id,
        type: constant.BUSINESS_CARD_DEAL_HISTORY,
        result: constant.SUCCESS,
        content: 'businessCardDealHistory.token.verify.is',
      });

      notification.save((err) => {
        if (err) {
          logger.logError(
            'deal-historys.authorization.server.controller',
            'validateToken',
            constant.NOTIFICATION,
            notification,
            err
          );
        } else {
          dataRes.bodyRes.sockets.push(
            createSocketData(
              [SOCKET_NOTIFICATION_PUSH],
              businessCardDealHistory.userID,
              { notificationID: notification._id }
            )
          );
        }

        done(null, dataRes);
      });
    }], (err, dataRes) => {
      return res
        .status(dataRes.status)
        .send(dataRes.bodyRes)
        .end();
    });
};
module.exports.validateToken = validateToken;

/**
 * @overview Tố cáo lịch sử giao dịch danh thiếp
 *
 * @param {Request} req
 * @param {Response} res
 * @param {Function} next
 * @param {Function} getInfo
 *
 * @input params { id: String }
 *        body   { report: { reason: String } }
 * @output       { code: String, businessCardDealHistory: {BusinessCardDealHistory} }
 */
const report = (req, res, next) => {
  const bodyReq = req.body;
  const paramsReq = req.params;
  const userSelf = req.user;
  let dataRes = null;

  async.waterfall([
    (done) => {
      /* Kiểm tra đầu vào */

      if (userSelf) {
        BusinessCardDealHistory
          .findOne({
            $or: [{
              _id: paramsReq.id,
              userID: userSelf._id,
              'reports.user.is': false,
              'result.is': { $in: [constant.FAILED, constant.SUCCESS] },
            }, {
              _id: paramsReq.id,
              providerID: userSelf._id,
              'reports.provider.is': false,
              'result.is': { $in: [constant.FAILED, constant.SUCCESS] },
            }],
          })
          .exec((err, businessCardDealHistory) => {
            if (err || !businessCardDealHistory) {
              // Lịch sử giao dịch danh thiếp không tồn tại
              dataRes = {
                status: 404,
                bodyRes: {
                  code: 'businessCardDealHistory.notExist',
                },
              };

              done(err || new Error('Not business card deal history exists'), dataRes);
            } else {
              done(null, businessCardDealHistory);
            }
          });
      } else {
        // Chưa đăng nhập người dùng
        dataRes = {
          status: 401,
          bodyRes: {
            code: 'user.signin.notExist',
          },
        };

        done(new Error('User not signin'), dataRes);
      }
    },
    (businessCardDealHistory, done) => {
      /* Cập nhật lịch sử giao dịch danh thiếp */

      if (businessCardDealHistory.addReport(userSelf._id, bodyReq.report.reason)) {
        businessCardDealHistory.save((err) => {
          if (err) {
            dataRes = {
              status: 400,
              bodyRes: {
                code: 'businessCardDealHistory.report.failed',
              },
            };

            done(err, dataRes);
          } else {
            dataRes = {
              status: 200,
              bodyRes: {
                code: 'businessCardDealHistory.report.success',
                businessCardDealHistory: businessCardDealHistory.getInfoByAuthorizedLevel(userSelf),
              },
            };

            const activityHistory = new ActivityHistory({
              userID: userSelf._id,
              toURL: businessCardDealHistory._id,
              type: constant.BUSINESS_CARD_DEAL_HISTORY,
              result: constant.SUCCESS,
              content: 'businessCardDealHistory.report.is',
            });

            activityHistory.save((err) => {
              if (err) {
                logger.logError(
                  'business-cards.authorization.server.controller',
                  'report',
                  constant.ACTIVITY_HISTORY,
                  activityHistory,
                  err
                );
              }
            });

            done(null, dataRes);
          }
        });
      } else {
        dataRes = {
          status: 400,
          bodyRes: {
            code: 'businessCardDealHistory.report.failed',
          },
        };

        done(new Error('Report business card deal history exists'), dataRes);
      }
    }], (err, dataRes) => {
      return res
        .status(dataRes.status)
        .send(dataRes.bodyRes)
        .end();
    });
};
module.exports.report = report;
