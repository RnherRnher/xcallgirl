const path = require('path');
const mongoose = require('mongoose');
const async = require('async');
const _ = require('lodash');
const logger = require(path.resolve('./config/lib/logger'));
const constant = require(path.resolve('./commons/utils/constant'));

const User = mongoose.model('User');
const BusinessCardDealHistory = mongoose.model('BusinessCardDealHistory');

/**
 * @overview Lấy thông tin theo ID
 *
 * @param {Request} req
 * @param {Response} res
 * @param {Function} next
 * @param {Function} getInfo
 *
 * @input  params { id: String }
 *         query  { type: String }
 * @output        {
 *                  code: String,
 *                  businessCardDealHistory: {BusinessCardDealHistory}
 *                }
 */
const get = (req, res, next) => {
  const paramsReq = req.params;
  const queryReq = req.query;
  const userSelf = req.user;
  let dataRes = null;

  async.waterfall([
    (done) => {
      /* Kiểm tra đầu vào */

      if (userSelf) {
        BusinessCardDealHistory
          .findById(paramsReq.id)
          .exec((err, businessCardDealHistory) => {
            if (err || !businessCardDealHistory) {
              // Lịch sử giao dịch danh thiếp không tồn tại
              dataRes = {
                status: 404,
                bodyRes: {
                  code: 'businessCardDealHistory.notExist',
                },
              };

              done(err || new Error('Not business card deal history exists'), dataRes);
            } else {
              done(null, businessCardDealHistory);
            }
          });
      } else {
        // Chưa đăng nhập người dùng
        dataRes = {
          status: 401,
          bodyRes: {
            code: 'user.signin.notExist',
          },
        };

        done(new Error('User not signin'), dataRes);
      }
    }, (businessCardDealHistory, done) => {
      /* Xác thực */

      if (businessCardDealHistory.authenticate(userSelf)) {
        if (businessCardDealHistory.checkExpires()) {
          businessCardDealHistory.save((err) => {
            if (err) {
              logger.logError(
                'deal-historys.action.server.controller',
                'get',
                constant.BUSINESS_CARD_DEAL_HISTORY,
                businessCardDealHistory,
                err
              );
            }

            done(null, businessCardDealHistory);
          });
        } else {
          done(null, businessCardDealHistory);
        }
      } else {
        // Người dùng không có quyền
        dataRes = {
          status: 401,
          bodyRes: {
            code: 'user.permission.notExist',
          },
        };

        done(new Error('Not permission'), dataRes);
      }
    }, (businessCardDealHistory, done) => {
      /* Lấy dữ liệu */

      const findUser = (idUser, callback) => {
        User
          .findById(idUser, `-${constant.SALT} -${constant.PASSWORD}`)
          .exec((err, user) => {
            let dataUser = null;
            if (user) {
              dataUser = user.getShortcutsInfo();
            } else {
              dataUser = User.getShortcutsInfoUndefined();
            }

            if (callback) callback(dataUser);
          });
      };

      dataRes = {
        status: 200,
        bodyRes: {
          code: 'businessCardDealHistory.exist',
        },
      };

      switch (queryReq.type) {
        case constant.FULL:
          dataRes.bodyRes.businessCardDealHistory = businessCardDealHistory.getInfoByAuthorizedLevel(userSelf);
          break;
        case constant.SHORTCUT:
          dataRes.bodyRes.businessCardDealHistory = businessCardDealHistory.getShortcutsInfo();
          break;
        default:
          break;
      }

      if (userSelf.equalsByID(businessCardDealHistory.userID)) {
        findUser(businessCardDealHistory.providerID, (user) => {
          dataRes.bodyRes.provider = user;

          done(null, dataRes);
        });
      } else if (userSelf.equalsByID(businessCardDealHistory.providerID)) {
        findUser(businessCardDealHistory.userID, (user) => {
          dataRes.bodyRes.user = user;

          done(null, dataRes);
        });
      } else {
        findUser(businessCardDealHistory.providerID, (user) => {
          dataRes.bodyRes.provider = user;

          findUser(businessCardDealHistory.userID, (user) => {
            dataRes.bodyRes.user = user;

            done(null, dataRes);
          });
        });
      }
    }], (err, dataRes) => {
      return res
        .status(dataRes.status)
        .send(dataRes.bodyRes)
        .end();
    });
};
module.exports.get = get;

/**
 * @overview Tìm kiếm
 *
 * @param {Request} req
 * @param {Response} res
 * @param {Function} next
 * @param {Function} getInfo
 *
 * @input query { type: String }
 * @output      {
 *                code: String,
 *                businessCardDealHistorys?: [{BusinessCardDealHistory}],
 *                skip?: Number
 *              }
 */
const search = (req, res, next) => {
  const queryReq = req.query;
  let dataRes = null;


  /**
   * @overview Tìm kiếm tự động
   *
   * @param {Request} req
   * @param {Response} res
   * @param {Function} next
   * @param {Function} getInfo
   *
   * @input  query { skip: Number }
   * @output       {
   *                  code: String,
   *                  businessCardDealHistorys?: [{BusinessCardDealHistory}],
   *                  count?: Number,
   *                  skip?: Number
   *               }
   */
  const autoSearch = (req, res, next, getInfo) => {
    const queryReq = req.query;
    const userSelf = req.user;
    let dataRes = null;

    const limit = req.app.locals.config.searchLimit;
    const currentSkip = queryReq.skip > 0 ? queryReq.skip : 0;

    if (userSelf) {
      BusinessCardDealHistory
        .find({
          $or: [{
            providerID: userSelf._id,
          }, {
            userID: userSelf._id,
          }],
        })
        .sort({ initDate: -1 })
        .skip(currentSkip)
        .limit(limit)
        .exec((err, businessCardDealHistorys) => {
          if (err) {
            // Bad request.body
            dataRes = {
              status: 400,
              bodyRes: {
                code: 'data.notVaild',
              },
            };

            return res
              .status(dataRes.status)
              .send(dataRes.bodyRes)
              .end();
          }

          getInfo(businessCardDealHistorys, userSelf, queryReq.skip, limit);
        });
    } else {
      // Chưa đăng nhập người dùng
      dataRes = {
        status: 401,
        bodyRes: {
          code: 'user.signin.notExist',
        },
      };

      return res
        .status(dataRes.status)
        .send(dataRes.bodyRes)
        .end();
    }
  };

  /**
   * @overview Tìm kiếm mở rộng
   *
   * @param {Request} req
   * @param {Response} res
   * @param {Function} next
   * @param {Function} getInfo
   *
   * @input body
     {
      extendSearch: {
        result?: String,
        status?: String,
        initDate?: {
          max?: Date,
          min?: Date
        }
      }
    }
   * query   { skip: Number }
   * @output {
   *            code: String,
   *            businessCardDealHistorys?: [{BusinessCardDealHistory}],
   *            count?: Number,
   *            skip?: Number
   *         }
   */
  const extendSearch = (req, res, next, getInfo) => {
    const bodyReq = req.body;
    const queryReq = req.query;
    const userSelf = req.user;
    let dataRes = null;

    const limit = req.app.locals.config.searchLimit;
    const currentSkip = queryReq.skip > 0 ? queryReq.skip : 0;

    if (userSelf) {
      const { extendSearch } = bodyReq;
      const conditionsExtendSearch = {};

      if (_.has(extendSearch, 'result')) {
        conditionsExtendSearch['result.is'] = extendSearch.result;
      }

      if (_.has(extendSearch, 'status')) {
        conditionsExtendSearch.status = extendSearch.status;
      }

      if (!_.isEmpty(extendSearch.initDate)) {
        if (_.has(extendSearch, 'initDate.max')) {
          conditionsExtendSearch.initDate = { $lte: extendSearch.initDate.max };
        }

        if (_.has(extendSearch, 'initDate.min')) {
          _.merge(
            conditionsExtendSearch.initDate ? conditionsExtendSearch.initDate : {},
            { $gte: extendSearch.initDate.min }
          );
        }
      }

      BusinessCardDealHistory
        .find({
          $or: [
            _.merge({ providerID: userSelf._id }, conditionsExtendSearch),
            _.merge({ userID: userSelf._id }, conditionsExtendSearch),
          ],
        })
        .skip(currentSkip)
        .limit(limit)
        .exec((err, businessCardDealHistorys) => {
          if (err) {
            // Bad request.body
            dataRes = {
              status: 400,
              bodyRes: {
                code: 'data.notVaild',
              },
            };

            return res
              .status(dataRes.status)
              .send(dataRes.bodyRes)
              .end();
          }

          getInfo(businessCardDealHistorys, userSelf, currentSkip, limit);
        });
    } else {
      // Chưa đăng nhập người dùng
      dataRes = {
        status: 401,
        bodyRes: {
          code: 'user.signin.notExist',
        },
      };

      return res
        .status(dataRes.status)
        .send(dataRes.bodyRes)
        .end();
    }
  };

  const getInfo = (businessCardDealHistorys, userSelf, currentSkip, limit) => {
    const findUser = (idUser, callback) => {
      User
        .findById(idUser, `-${constant.SALT} -${constant.PASSWORD}`)
        .exec((err, user) => {
          let dataUser = null;
          if (user) {
            dataUser = user.getShortcutsInfo();
          } else {
            dataUser = User.getShortcutsInfoUndefined();
          }

          if (callback) callback(dataUser);
        });
    };

    async.map(businessCardDealHistorys, (businessCardDealHistory, done) => {
      async.waterfall([
        (callback) => {
          if (businessCardDealHistory.authenticate(userSelf)) {
            if (businessCardDealHistory.checkExpires()) {
              businessCardDealHistory.save((err) => {
                if (err) {
                  logger.logError(
                    'deal-historys.action.server.controller',
                    'getInfo',
                    constant.BUSINESS_CARD_DEAL_HISTORY,
                    businessCardDealHistory,
                    err
                  );
                }

                callback(null, businessCardDealHistory);
              });
            } else {
              callback(null, businessCardDealHistory);
            }
          } else {
            // Người dùng không có quyền
            dataRes = {
              status: 401,
              bodyRes: {
                code: 'user.permission.notExist',
              },
            };

            callback(new Error('Not permission'), dataRes);
          }
        }, (businessCardDealHistory, callback) => {
          findUser(
            userSelf.equalsByID(businessCardDealHistory.userID)
              ? businessCardDealHistory.providerID
              : businessCardDealHistory.userID,
            (user) => {
              callback(null, {
                businessCardDealHistory: businessCardDealHistory.getShortcutsInfo(),
                user,
              });
            }
          );
        }], (err, dataRes) => {
          if (err) {
            return res
              .status(dataRes.status)
              .send(dataRes.bodyRes)
              .end();
          }

          return done(null, dataRes);
        });
    }, (err, resultBusinessCardDealHistorys) => {
      if (resultBusinessCardDealHistorys.length) {
        BusinessCardDealHistory
          .countDocuments({
            $or: [
              {
                providerID: userSelf._id,
                'result.is': constant.WAIT
              },
              {
                userID: userSelf._id,
                'result.is': constant.WAIT
              }
            ]
          })
          .exec((err, count) => {
            dataRes = {
              status: 200,
              bodyRes: {
                code: 'businessCardDealHistory.exist',
                skip: currentSkip + (businessCardDealHistorys.length < limit ? businessCardDealHistorys.length : limit),
                businessCardDealHistorys: resultBusinessCardDealHistorys,
                count
              },
            };

            return res
              .status(dataRes.status)
              .send(dataRes.bodyRes)
              .end();
          });
      } else {
        // Lịch sử giao dịch danh thiếp không tồn tại
        dataRes = {
          status: 404,
          bodyRes: {
            code: 'businessCardDealHistory.notExist',
          },
        };

        return res
          .status(dataRes.status)
          .send(dataRes.bodyRes)
          .end();
      }
    });
  };

  switch (queryReq.type) {
    case constant.AUTO:
      autoSearch(req, res, next, getInfo);
      break;
    case constant.EXTEND:
      extendSearch(req, res, next, getInfo);
      break;
    default: {
      dataRes = {
        status: 400,
        bodyRes: {
          code: 'data.notVaild',
        },
      };

      return res
        .status(dataRes.status)
        .send(dataRes.bodyRes)
        .end();
    }
  }
};
module.exports.search = search;
