const _ = require('lodash');
const authorization = require('./deal-historys/deal-historys.authorization.server.controller');
const action = require('./deal-historys/deal-historys.action.server.controller');

module.exports = _.assignIn(
  authorization,
  action
);
