const path = require('path');
const url = require(path.resolve('./commons/modules/business-cards/datas/url.data'));

module.exports = [
  /* Search */
  {
    resources: url.BUSINESS_CARDS_DEAL_HISTORYS_SEARCH,
    permissions: ['get'],
  }, {
    resources: url.BUSINESS_CARDS_DEAL_HISTORYS_GET_ID,
    permissions: ['get'],
  },
  /* DealHistorys */
  {
    resources: url.BUSINESS_CARDS_DEAL_HISTORYS_ACCEPT_ID,
    permissions: ['post'],
  }, {
    resources: url.BUSINESS_CARDS_DEAL_HISTORYS_TOKEN_ID,
    permissions: ['get', 'post'],
  }, {
    resources: url.BUSINESS_CARDS_DEAL_HISTORYS_REPORT_ID,
    permissions: ['post'],
  },
];
