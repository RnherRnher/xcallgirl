const path = require('path');
const url = require(path.resolve('./commons/modules/business-cards/datas/url.data'));

module.exports = [
  /* Search */
  {
    resources: url.BUSINESS_CARDS_SEARCH,
    permissions: ['get'],
  }, {
    resources: url.BUSINESS_CARDS_GET_ID,
    permissions: ['get'],
  },
  /* BusinessCard */
  {
    resources: url.BUSINESS_CARDS,
    permissions: ['post'],
  },
  /* Acction */
  {
    resources: url.BUSINESS_CARDS_REPORT_ID,
    permissions: ['post'],
  }, {
    resources: url.BUSINESS_CARDS_LIKE_ID,
    permissions: ['post', 'put'],
  }, {
    resources: url.BUSINESS_CARDS_COMMENT_ID,
    permissions: ['get', 'post', 'put', 'delete'],
  }, {
    resources: url.BUSINESS_CARDS_DEAL_ID,
    permissions: ['post'],
  },
];
