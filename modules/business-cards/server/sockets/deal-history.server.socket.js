const path = require('path');
const constant = require(path.resolve('./commons/utils/constant'));
const {
  SOCKET_BUSINESS_CARD_DEAL_HISTORY_UPDATE,
  SOCKET_DETAIL_BUSINESS_CARD_DEAL_HISTORY_UPDATE,
  SOCKET_BUSINESS_CARD_DEAL_HISTORY_PUSH,
  SOCKET_BUSINESS_CARD_DEAL_HISTORY_LISTEN
} = require(path.resolve('./commons/modules/business-cards/datas/socket.data'));

/**
 * @overview Khởi tạo
 *
 * @param {SocketIO} io
 * @param {Socket} socket
 */
const init = (io, socket) => {
  socket.on(SOCKET_BUSINESS_CARD_DEAL_HISTORY_LISTEN, (dataReq) => {
    switch (dataReq.type) {
      case constant.ADD:
        socket.join(dataReq.id);
        break;
      case constant.REMOVE:
        socket.leave(dataReq.id);
        break;
      default:
        break;
    }
  });

  socket.on(SOCKET_BUSINESS_CARD_DEAL_HISTORY_UPDATE, (dataReq) => {
    io.to(dataReq.to)
      .emit(
        SOCKET_BUSINESS_CARD_DEAL_HISTORY_UPDATE,
        dataReq.body
      );
  });

  socket.on(SOCKET_DETAIL_BUSINESS_CARD_DEAL_HISTORY_UPDATE, (dataReq) => {
    socket.to(dataReq.to)
      .emit(
        SOCKET_DETAIL_BUSINESS_CARD_DEAL_HISTORY_UPDATE,
        dataReq.body
      );
  });

  socket.on(SOCKET_BUSINESS_CARD_DEAL_HISTORY_PUSH, (dataReq) => {
    io.to(dataReq.to)
      .emit(
        SOCKET_BUSINESS_CARD_DEAL_HISTORY_PUSH,
        dataReq.body
      );
  });
};
module.exports = init;
