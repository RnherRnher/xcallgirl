const UICosts = require('./uis/UICosts');
const UIInfoBusinessCard = require('./uis/UIInfoBusinessCard');
const UIInfoDealHistory = require('./uis/UIInfoDealHistory');

module.exports = {
  UICosts,
  UIInfoBusinessCard,
  UIInfoDealHistory,
};
