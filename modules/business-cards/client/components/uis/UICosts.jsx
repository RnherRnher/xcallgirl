const React = require('react');
const propTypes = require('prop-types');
const classNames = require('classnames');
const constant = require('../../../../../commons/utils/constant');
const { isEmpty } = require('lodash');
const { FormattedMessage, FormattedNumber } = require('react-intl');
const { Delete } = require('@material-ui/icons');
const {
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
  IconButton
} = require('@material-ui/core');

const UICosts = class UICosts extends React.Component {
  constructor(props) {
    super(props);
  }

  Rooms() {
    const {
      rooms,
      room,
      currencyUnit,
      handleClickRemove
    } = this.props;

    const Room = (room) => {
      return (
        <TableRow key={room.value}>
          <TableCell>
            <FormattedMessage id={`businessCard.costs.rooms.${room.value}`} />
          </TableCell>
          <TableCell>
            {
              room.moneys !== '0'
                ? <FormattedNumber value={room.moneys} />
                : <FormattedMessage id="info.moneys.free" />
            }
          </TableCell>
          {
            currencyUnit
              ? <TableCell>
                <FormattedMessage id={`info.moneys.currencyUnit.${currencyUnit}`} />
              </TableCell>
              : null
          }
          {
            handleClickRemove
              ? <TableCell>
                <IconButton onClick={handleClickRemove(constant.ROOMS, room.value)}>
                  <Delete />
                </IconButton>
              </TableCell>
              : null
          }

        </TableRow>
      );
    };

    if (!isEmpty(rooms)) {
      return rooms.map((room) => {
        return Room(room);
      });
    }

    if (!isEmpty(room)) {
      return Room(room);
    }

    return null;
  }

  Services() {
    const {
      services,
      service,
      currencyUnit,
      handleClickRemove
    } = this.props;

    const Service = (service) => {
      return (
        <TableRow key={service.value}>
          <TableCell>
            <FormattedMessage id={`businessCard.costs.services.${service.value}`} />
          </TableCell>
          <TableCell>
            {
              service.moneys !== '0'
                ? <FormattedNumber value={service.moneys} />
                : <FormattedMessage id="info.moneys.free" />
            }
          </TableCell>
          {
            currencyUnit
              ? <TableCell>
                <FormattedMessage id={`info.moneys.currencyUnit.${currencyUnit}`} />
              </TableCell>
              : null
          }
          {
            handleClickRemove
              ? <TableCell>
                <IconButton onClick={handleClickRemove(constant.SERVICERS, service.value)}>
                  <Delete />
                </IconButton>
              </TableCell>
              : null
          }
        </TableRow>
      );
    };

    if (!isEmpty(services)) {
      return services.map((service) => {
        return Service(service);
      });
    }

    if (!isEmpty(service)) {
      return Service(service);
    }

    return null;
  }

  initElement() {
    const { className } = this.props;

    return (
      <div className={classNames('xcg-ui-costs-business-card', className)}>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>
                <FormattedMessage id="businessCard.costs.rooms.is" />
              </TableCell>
              <TableCell>
                <FormattedMessage id="businessCard.costs.value" />
              </TableCell>
              <TableCell>
                <FormattedMessage id="info.moneys.currencyUnit.is" />
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {this.Rooms()}
          </TableBody>
        </Table>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>
                <FormattedMessage id="businessCard.costs.services.is" />
              </TableCell>
              <TableCell>
                <FormattedMessage id="businessCard.costs.value" />
              </TableCell>
              <TableCell>
                <FormattedMessage id="info.moneys.currencyUnit.is" />
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {this.Services()}
          </TableBody>
        </Table>
      </div>
    );
  }

  render() {
    return this.initElement();
  }
};

UICosts.propTypes = {
  className: propTypes.string,
  handleClickRemove: propTypes.func,
  rooms: propTypes.array,
  services: propTypes.array,
  room: propTypes.object,
  service: propTypes.object,
  currencyUnit: propTypes.string.isRequired,
};

UICosts.defaultProps = {
  className: '',
  rooms: [],
  services: [],
  room: null,
  service: null,
  handleClickRemove: null
};

module.exports = UICosts;
