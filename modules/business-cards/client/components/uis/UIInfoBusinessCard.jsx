const React = require('react');
const propTypes = require('prop-types');
const classNames = require('classnames');
const constant = require('../../../../../commons/utils/constant');
const { connect } = require('react-redux');
const { createStructuredSelector } = require('reselect');
const {
  FormattedMessage,
  FormattedNumber,
  FormattedDate
} = require('react-intl');
const {
  Lens,
  NewReleases,
  Update,
  PlusOne,
  ArrowUpward
} = require('@material-ui/icons');
const {
  ListItem,
  ListItemText,
  ListItemIcon,
  List,
  ListItemSecondaryAction,
  Switch,
  IconButton,
  Badge
} = require('@material-ui/core');
const { ResultEnum } = require('../../../../core/client/constants/core.client.constant');
const {
  makeSelectReUploadBusinessCard,
  makeSelectChangesBusinessCard
} = require('../../reselects/business-card.client.reselect');


const UIInfoBusinessCard = class UIInfoBusinessCard extends React.Component {
  constructor(props) {
    super(props);
  }

  UIStatus() {
    const {
      value,
      isOwner,
      status,
      handleSubmit
    } = this.props;

    const disabled = status.result === ResultEnum.wait;

    return (
      <ListItem divider>
        <ListItemIcon>
          <Lens className={value.status.is ? 'xcg-success' : 'xcg-error'} />
        </ListItemIcon>
        <ListItemText
          inset
          primary={<FormattedMessage id="info.status.is" />}
          secondary={<FormattedMessage id={`info.status.${value.status.is ? 'online' : 'offline'}`} />}
        />
        {
          isOwner
            ? <ListItemSecondaryAction>
              <Switch
                disabled={disabled}
                onChange={handleSubmit(constant.STATUS)}
                checked={value.status.is}
              />
            </ListItemSecondaryAction>
            : null
        }
      </ListItem>
    );
  }

  UIReUpload() {
    const {
      value,
      isOwner,
      reUpload,
      handleSubmit
    } = this.props;

    const disabled = reUpload.result === ResultEnum.wait;

    return (
      isOwner
        ? <ListItem divider>
          <ListItemIcon>
            <PlusOne />
          </ListItemIcon>
          <ListItemText
            inset
            primary={<FormattedMessage id="businessCard.reUpload.is" />}
            secondary={
              <FormattedDate
                weekday="narrow"
                year="numeric"
                month="numeric"
                day="numeric"
                hour="numeric"
                minute="numeric"
                second="numeric"
                value={value.reUpload.date}
              />
            }
          />
          {
            value.reUpload.count
              ? <ListItemSecondaryAction>
                <IconButton
                  disabled={disabled}
                  color="secondary"
                  onClick={handleSubmit(constant.REUPLOAD)}
                >
                  <Badge
                    badgeContent={
                      <FormattedNumber
                        value={value.reUpload.count}
                      />
                    }
                  >
                    <ArrowUpward />
                  </Badge>
                </IconButton>
              </ListItemSecondaryAction>
              : null
          }
        </ListItem>
        : null
    );
  }

  UIUpdate() {
    const { value } = this.props;

    return (
      <ListItem divider>
        <ListItemIcon>
          <Update />
        </ListItemIcon>
        <ListItemText
          inset
          primary={<FormattedMessage id="info.time.update" />}
          secondary={
            <FormattedDate
              weekday="narrow"
              year="numeric"
              month="numeric"
              day="numeric"
              hour="numeric"
              minute="numeric"
              second="numeric"
              value={value.updateDate}
            />
          }
        />
      </ListItem>
    );
  }

  UIInit() {
    const { value } = this.props;

    return (
      <ListItem>
        <ListItemIcon>
          <NewReleases />
        </ListItemIcon>
        <ListItemText
          inset
          primary={<FormattedMessage id="info.time.init" />}
          secondary={
            <FormattedDate
              weekday="narrow"
              year="numeric"
              month="numeric"
              day="numeric"
              hour="numeric"
              minute="numeric"
              second="numeric"
              value={value.initDate}
            />
          }
        />
      </ListItem>
    );
  }

  initElement() {
    const { className } = this.props;

    return (
      <div className={classNames('xcg-ui-info-business-card', className)}>
        <List dense>
          {this.UIStatus()}
          {this.UIReUpload()}
          {this.UIUpdate()}
          {this.UIInit()}
        </List>
      </div>
    );
  }

  render() {
    return this.initElement();
  }
};

UIInfoBusinessCard.propTypes = {
  className: propTypes.string,
  isOwner: propTypes.bool.isRequired,
  value: propTypes.object.isRequired,
  status: propTypes.object.isRequired,
  reUpload: propTypes.object.isRequired,
  handleSubmit: propTypes.func.isRequired
};

UIInfoBusinessCard.defaultProps = {
  className: '',
};

function mapStateToProps(state, props) {
  return createStructuredSelector({
    status: makeSelectChangesBusinessCard(constant.STATUS),
    reUpload: makeSelectReUploadBusinessCard(),
  });
}

function mapDispatchToProps(dispatch, props) {
  return {};
}

module.exports = connect(
  mapStateToProps,
  mapDispatchToProps,
)(UIInfoBusinessCard);
