const React = require('react');
const propTypes = require('prop-types');
const classNames = require('classnames');
const constant = require('../../../../../commons/utils/constant');
const { FormattedMessage, FormattedDate } = require('react-intl');
const {
  CompareArrows,
  Textsms,
  Fingerprint,
  Sync,
  CheckCircle,
  Error
} = require('@material-ui/icons');
const {
  Table,
  TableBody,
  TableRow,
  TableCell,
  LinearProgress,
  IconButton,
  Tooltip
} = require('@material-ui/core');
const { UITimer } = require('../../../../core/client/components/UIs');

const UIInfoDealHistory = class UIInfoDealHistory extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      tooltiped: null
    };

    this.handleClickTooltipOpen = this.handleClickTooltipOpen.bind(this);
    this.handleClickTooltipClose = this.handleClickTooltipClose.bind(this);
  }

  handleClickTooltipOpen(tooltiped) {
    return (event) => {
      this.setState({ tooltiped });
    };
  }

  handleClickTooltipClose(event) {
    this.setState({ tooltiped: false });
  }

  Status() {
    const { businessCardDealHistory } = this.props;

    let Status = null;
    switch (businessCardDealHistory.status) {
      case constant.RESPONSE:
        Status = <CompareArrows />;
        break;
      case constant.TOKEN:
        Status = <Textsms />;
        break;
      case constant.VERIFY:
        Status = <Fingerprint />;
        break;
      default:
        break;
    }

    return Status;
  }

  Result() {
    const { businessCardDealHistory } = this.props;

    let Result = null;
    switch (businessCardDealHistory.result.is) {
      case constant.WAIT:
        Result = <Sync className="xcg-info" />;
        break;
      case constant.SUCCESS:
        Result = <CheckCircle className="xcg-success" />;
        break;
      case constant.FAILED:
        Result = <Error className="xcg-error" />;
        break;
      default:
        break;
    }

    return Result;
  }

  initElement() {
    const {
      className,
      businessCardDealHistory,
      completed,
      timer
    } = this.props;
    const { tooltiped } = this.state;

    let linearClass = 'xcg-linear-info';
    let timerClass = 'xcg-info';
    if (completed <= 25) {
      linearClass = 'xcg-linear-error';
      timerClass = 'xcg-error';
    } else if (completed <= 50) {
      linearClass = 'xcg-linear-warning';
      timerClass = 'xcg-warning';
    }

    return (
      <div className={classNames('xcg-ui-info-deal-history', className)}>
        <Table>
          <TableBody>
            {
              businessCardDealHistory.result.is === constant.WAIT
                && businessCardDealHistory.status === constant.VERIFY
                && businessCardDealHistory.token
                ? <TableRow>
                  <TableCell>
                    <FormattedMessage id="info.security.token.is" />
                  </TableCell>
                  <TableCell>
                    {businessCardDealHistory.token}
                  </TableCell>
                </TableRow>
                : null
            }
            <TableRow>
              <TableCell>
                <FormattedMessage id="info.result.is" />
              </TableCell>
              <TableCell>
                <FormattedMessage id={`info.result.${businessCardDealHistory.result.is}`} />
              </TableCell>
              <TableCell>
                <Tooltip
                  placement="top"
                  onClose={this.handleClickTooltipClose}
                  open={tooltiped === constant.RESULT}
                  title={
                    <FormattedDate
                      weekday="narrow"
                      year="numeric"
                      month="numeric"
                      day="numeric"
                      hour="numeric"
                      minute="numeric"
                      second="numeric"
                      value={businessCardDealHistory.result.date}
                    />
                  }
                >
                  <IconButton onClick={this.handleClickTooltipOpen(constant.RESULT)}>
                    {this.Result()}
                  </IconButton>
                </Tooltip>
              </TableCell>
            </TableRow>
            <TableRow>
              <TableCell>
                <FormattedMessage id="info.time.timer" />
              </TableCell>
              <TableCell>
                <LinearProgress
                  className={linearClass}
                  variant="determinate"
                  value={completed}
                />
              </TableCell>
              <TableCell>
                <UITimer className={timerClass} value={timer} />
              </TableCell>
            </TableRow>
            <TableRow>
              <TableCell>
                <FormattedMessage id="info.status.is" />
              </TableCell>
              <TableCell>
                <FormattedMessage id={`info.status.${businessCardDealHistory.status}`} />
              </TableCell>
              <TableCell>
                {this.Status()}
              </TableCell>
            </TableRow>
            <TableRow>
              <TableCell>
                <FormattedMessage id="info.time.init" />
              </TableCell>
              <TableCell>
                <FormattedDate
                  weekday="narrow"
                  year="numeric"
                  month="numeric"
                  day="numeric"
                  hour="numeric"
                  minute="numeric"
                  second="numeric"
                  value={businessCardDealHistory.initDate}
                />
              </TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </div>
    );
  }

  render() {
    return this.initElement();
  }
};

UIInfoDealHistory.propTypes = {
  className: propTypes.string,
  businessCardDealHistory: propTypes.object.isRequired,
  completed: propTypes.number.isRequired,
  timer: propTypes.number.isRequired,
};

UIInfoDealHistory.defaultProps = {
  className: '',
};

module.exports = UIInfoDealHistory;
