const InputCosts = require('./inputs/InputCosts');
const InputDeal = require('./inputs/InputDeal');
const InputImages = require('./inputs/InputImages');
const InputMeasurements3 = require('./inputs/InputMeasurements3');
const InputReport = require('./inputs/InputReport');

module.exports = {
  InputCosts,
  InputDeal,
  InputImages,
  InputMeasurements3,
  InputReport
};
