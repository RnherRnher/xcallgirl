const React = require('react');
const propTypes = require('prop-types');
const classNames = require('classnames');
const ContentLoader = require('react-content-loader').default;

const LoadableDetailDealHistory = class LoadableDetailDealHistory extends React.Component {
  constructor(props) {
    super(props);
  }

  initElement() {
    const { className } = this.props;

    return (
      <div className={classNames('xcg-loadable-detail-business-card-deal-history', className)}>
        <ContentLoader
          height={500}
          width={350}
          speed={2}
          primaryColor="#666666"
          secondaryColor="#737373"
        >
          <rect x="20" y="10" rx="5" ry="5" width="310" height="100" />
          <rect x="20" y="120" rx="5" ry="5" width="310" height="50" />
          <rect x="20" y="180" rx="5" ry="5" width="310" height="300" />
        </ContentLoader>
      </div>
    );
  }

  render() {
    return this.initElement();
  }
};

LoadableDetailDealHistory.propTypes = {
  className: propTypes.string,
};

LoadableDetailDealHistory.defaultProps = {
  className: '',
};

module.exports = LoadableDetailDealHistory;
