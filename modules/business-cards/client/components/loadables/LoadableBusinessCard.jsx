const React = require('react');
const propTypes = require('prop-types');
const classNames = require('classnames');
const ContentLoader = require('react-content-loader').default;

const LoadableBusinessCard = class LoadableBusinessCard extends React.Component {
  constructor(props) {
    super(props);
  }

  initElement() {
    const { className } = this.props;

    return (
      <div className={classNames('xcg-loadable-business-card', className)}>
        <ContentLoader
          height={450}
          width={350}
          speed={2}
          primaryColor="#666666"
          secondaryColor="#737373"
        >
          <rect x="90" y="15" rx="5" ry="5" width="150" height="15" />
          <rect x="90" y="40" rx="5" ry="5" width="100" height="10" />
          <rect x="90" y="60" rx="5" ry="5" width="150" height="15" />
          <rect x="15" y="90" rx="5" ry="5" width="320" height="10" />
          <rect x="15" y="110" rx="5" ry="5" width="320" height="10" />
          <rect x="15" y="130" rx="5" ry="5" width="220" height="10" />
          <rect x="0" y="160" rx="5" ry="5" width="350" height="280" />
          <circle cx="45" cy="45" r="30" />
        </ContentLoader>
      </div>
    );
  }

  render() {
    return this.initElement();
  }
};

LoadableBusinessCard.propTypes = {
  className: propTypes.string,
};

LoadableBusinessCard.defaultProps = {
  className: '',
};

module.exports = LoadableBusinessCard;
