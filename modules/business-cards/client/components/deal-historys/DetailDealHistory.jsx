const React = require('react');
const propTypes = require('prop-types');
const classNames = require('classnames');
const joi = require('joi');
const regular = require('../../../../../commons/utils/regular-expression');
const constant = require('../../../../../commons/utils/constant');
const { connect } = require('react-redux');
const { createStructuredSelector } = require('reselect');
const { indexOf } = require('lodash');
const { FormattedMessage } = require('react-intl');
const {
  ExpandMore,
  LocationOn,
  MonetizationOn,
  Info,
  Flag,
  Check,
  Cancel,
  Close,
  Textsms,
  Fingerprint
} = require('@material-ui/icons');
const {
  Avatar,
  ExpansionPanelSummary,
  ListItem,
  ListItemAvatar,
  ListItemText,
  ExpansionPanelDetails,
  ExpansionPanel,
  Card,
  CardContent,
  Typography,
  CardActions,
  Stepper,
  Step,
  StepLabel,
  Button,
  Divider,
  Slide,
  Dialog,
  DialogTitle,
  DialogContent,
  TextField,
  DialogActions
} = require('@material-ui/core');
const { UICosts, UIInfoDealHistory } = require('../UIs');
const { AlertSnackbar } = require('../../../../core/client/components/Alerts');
const { UIAddress, UITimer } = require('../../../../core/client/components/UIs');
const { User } = require('../../../../users/client/components/Users');
const { makeSelectActionsBusinessCardDealHistory } = require('../../reselects/deal-history.client.reselect');
const { ResultEnum } = require('../../../../core/client/constants/core.client.constant');
const {
  getBusinessCardsDealHistoryIDRequest,
  acceptBusinessCardsDealHistoryIDRequest,
  getTokenBusinessCardsDealHistoryIDRequest,
  validateTokenBusinessCardsDealHistoryIDRequest,
  reportBusinessCardsDealHistoryIDRequest,
  listenBusinessCardsDealHistory,
} = require('../../actions/deal-history.client.action');
const { InputReport } = require('../Inputs');
const {
  accept,
  validateToken,
  report
} = require('../../../../../commons/modules/business-cards/validates/deal-historys.validate');

const DetailDealHistory = class DetailDealHistory extends React.Component {
  constructor(props) {
    super(props);

    const { value } = this.props;
    const { businessCardDealHistory } = value;

    const rootTimer = new Date(businessCardDealHistory.timer).getTime() - new Date(businessCardDealHistory.initDate).getTime();
    const currentTimer = new Date(businessCardDealHistory.timer).getTime() - Date.now();

    this.state = {
      completed: businessCardDealHistory.result.is === constant.WAIT ? currentTimer * 100 / rootTimer : 0,
      currentTimer,
      rootTimer,
      expanded: null,
      reason: '',
      token: '',
    };

    this.handleCheck = this.handleCheck.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.handleClickExpand = this.handleClickExpand.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  componentDidMount() {
    const {
      value,
      interval,
      getBusinessCardsDealHistoryIDRequest,
      listenBusinessCardsDealHistory
    } = this.props;
    const { businessCardDealHistory } = value;

    if (businessCardDealHistory.result.is === constant.WAIT) {
      this.timer = setInterval(() => {
        this.setState((state, props) => {
          const currentTimer = state.currentTimer - interval;
          const completed = currentTimer * 100 / state.rootTimer;

          if (completed <= 0) {
            clearInterval(this.timer);
            getBusinessCardsDealHistoryIDRequest({ businessCardDealHistoryID: businessCardDealHistory._id });
          }

          return {
            completed,
            currentTimer
          };
        });
      }, interval);
    }

    listenBusinessCardsDealHistory(constant.ADD, value.businessCardDealHistory._id);
  }

  componentWillUnmount() {
    const { value, listenBusinessCardsDealHistory } = this.props;

    if (this.timer) {
      clearInterval(this.timer);
    }

    listenBusinessCardsDealHistory(constant.REMOVE, value.businessCardDealHistory._id);
  }

  handleCheck(body, validate) {
    return joi.validate(body, validate);
  }

  handleClick(type) {
    return (event) => {
      const {
        value,
        validate,
        acceptBusinessCardsDealHistoryIDRequest,
        getTokenBusinessCardsDealHistoryIDRequest,
        reportBusinessCardsDealHistoryIDRequest,
        validateTokenBusinessCardsDealHistoryIDRequest
      } = this.props;
      const { reason, token } = this.state;
      const { businessCardDealHistory } = value;

      switch (type) {
        case constant.ACCEPT:
        case constant.NOT_ACCEPT:
          this.handleCheck({
            accept: {
              is: type === constant.ACCEPT,
              reason: type === constant.ACCEPT ? undefined : reason || undefined
            }
          }, validate.accept)
            .then((body) => {
              acceptBusinessCardsDealHistoryIDRequest(businessCardDealHistory._id, body);

              if (type === constant.NOT_ACCEPT) {
                this.handleClickExpand(type)();
              }
            });
          break;
        case constant.TOKEN:
          getTokenBusinessCardsDealHistoryIDRequest(businessCardDealHistory._id);
          break;
        case constant.VERIFY:
          this.handleCheck({ token }, validate.validateToken)
            .then((body) => {
              validateTokenBusinessCardsDealHistoryIDRequest(businessCardDealHistory._id, body);
            });
          break;
        case constant.REPORT:
          this.handleCheck(event, validate.report)
            .then((body) => {
              reportBusinessCardsDealHistoryIDRequest(businessCardDealHistory._id, body);
            });
          break;
        default:
          break;
      }
    };
  }

  handleClickExpand(type) {
    return (event) => {
      this.setState((state, props) => {
        return {
          expanded: state.expanded === type ? null : type,
          reason: '',
          token: ''
        };
      });
    };
  }

  handleChange(type) {
    return (event) => {
      const { value } = event.target;

      this.setState({ [type]: value });
    };
  }

  Stepper() {
    const { value } = this.props;
    const { businessCardDealHistory } = value;

    const activeStep = indexOf(regular.businessCardDealHistory.status, businessCardDealHistory.status);

    return (
      <Stepper activeStep={activeStep} alternativeLabel>
        {regular.businessCardDealHistory.status.map((status) => {
          return (
            <Step key={status}>
              <StepLabel>
                <FormattedMessage id={`info.status.${status}`} />
              </StepLabel>
            </Step>
          );
        })}
      </Stepper>
    );
  }

  Card() {
    const {
      value,
      accept,
      token,
      verify
    } = this.props;
    const { businessCardDealHistory } = value;

    const UIUser = (type) => {
      const { value } = this.props;
      const { user, provider, businessCardDealHistory } = value;

      const _user = type === constant.USER ? user : provider;

      return (
        _user
          ? <div>
            <User value={_user} />
            {
              businessCardDealHistory.accept[type] && !businessCardDealHistory.accept[type].is
                ? <CardContent>
                  <Typography variant="title">
                    <FormattedMessage id="info.reason.is" />
                  </Typography>
                  <Typography>
                    {businessCardDealHistory.accept[type].reason}
                  </Typography>
                </CardContent>
                : null
            }
          </div>
          : null
      );
    };

    const ActionsProvider = () => {
      const acceptDisabled = accept.result === ResultEnum.wait;
      const verifyDisabled = verify.result === ResultEnum.wait;

      return (
        <CardActions>
          {
            businessCardDealHistory.result.is === constant.WAIT
              ? <div>
                {
                  !businessCardDealHistory.accept.provider
                    ? <div>
                      <Button
                        color="primary"
                        disabled={acceptDisabled}
                        onClick={this.handleClickExpand(constant.NOT_ACCEPT)}
                      >
                        <Close />
                        <FormattedMessage id="actions.notAccept" />
                      </Button>
                      <Button
                        color="secondary"
                        disabled={acceptDisabled}
                        onClick={this.handleClick(constant.ACCEPT)}
                      >
                        <Check />
                        <FormattedMessage id="actions.accept" />
                      </Button>
                    </div>
                    : <div>
                      {
                        businessCardDealHistory.accept.provider.is
                          ? <div>
                            <Button
                              color="primary"
                              disabled={acceptDisabled}
                              onClick={this.handleClickExpand(constant.NOT_ACCEPT)}
                            >
                              <Cancel />
                              <FormattedMessage id="actions.cancel" />
                            </Button>
                            {
                              businessCardDealHistory.status === constant.VERIFY
                                ? <Button
                                  disabled={verifyDisabled}
                                  onClick={this.handleClickExpand(constant.VERIFY)}
                                  color="secondary"
                                >
                                  <Fingerprint />
                                  <FormattedMessage id="actions.vetify" />
                                </Button>
                                : null
                            }
                          </div>
                          : null
                      }
                    </div>
                }
              </div>
              : <div>
                {
                  businessCardDealHistory.isReport
                    ? null
                    : <Button
                      color="primary"
                      onClick={this.handleClickExpand(constant.REPORT)}
                    >
                      <Flag />
                      <FormattedMessage id="actions.report" />
                    </Button>

                }
              </div>
          }
        </CardActions>
      );
    };

    const ActionsUser = () => {
      const tokenDisabled = token.result === ResultEnum.wait;
      const acceptDisabled = accept.result === ResultEnum.wait;

      return (
        <CardActions>
          {
            businessCardDealHistory.result.is === constant.WAIT
              ? <div>
                {
                  businessCardDealHistory.accept.user.is
                    ? <div>
                      <Button
                        color="primary"
                        disabled={acceptDisabled}
                        onClick={this.handleClickExpand(constant.NOT_ACCEPT)}
                      >
                        <Cancel />
                        <FormattedMessage id="actions.cancel" />
                      </Button>
                      {
                        businessCardDealHistory.accept.provider
                          && businessCardDealHistory.accept.provider.is
                          && businessCardDealHistory.status === constant.TOKEN
                          ? <Button
                            disabled={tokenDisabled}
                            onClick={this.handleClick(constant.TOKEN)}
                            color="secondary"
                          >
                            <Textsms />
                            <FormattedMessage id="actions.getToken" />
                          </Button>
                          : null
                      }
                    </div>
                    : null
                }
              </div>
              : <div>
                {
                  businessCardDealHistory.isReport
                    ? null
                    : <Button
                      color="primary"
                      onClick={this.handleClickExpand(constant.REPORT)}
                    >
                      <Flag />
                      <FormattedMessage id="actions.report" />
                    </Button>

                }
              </div>
          }
        </CardActions>
      );
    };

    let Actions = null;
    switch (businessCardDealHistory.roles) {
      case constant.PROVIDER:
        Actions = ActionsProvider();
        break;
      case constant.USER:
        Actions = ActionsUser();
        break;
      case constant.ADMIN:
        break;
      default:
        break;
    }

    return (
      <Card>
        {Actions}
        <Divider />
        {UIUser(constant.PROVIDER)}
        {UIUser(constant.USER)}
      </Card>
    );
  }

  Extend() {
    const { value } = this.props;
    const { completed, currentTimer } = this.state;
    const { businessCardDealHistory } = value;

    const Expansion = (title, icon, children) => {
      return (
        <ExpansionPanel>
          <ExpansionPanelSummary expandIcon={<ExpandMore />}>
            <ListItem>
              <ListItemAvatar>
                <Avatar>
                  {icon}
                </Avatar>
              </ListItemAvatar>
              <ListItemText
                inset
                primary={title}
              />
            </ListItem>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            {children}
          </ExpansionPanelDetails>
        </ExpansionPanel>
      );
    };

    return (
      <div>
        {
          Expansion(
            <FormattedMessage id="info.general.is" />,
            <Info />,
            <UIInfoDealHistory
              businessCardDealHistory={businessCardDealHistory}
              completed={completed}
              timer={currentTimer}
            />
          )
        }
        {
          Expansion(
            <FormattedMessage id="info.address.is" />,
            <LocationOn />,
            <UIAddress value={businessCardDealHistory.address} />
          )
        }
        {
          Expansion(
            <FormattedMessage id="businessCard.costs.is" />,
            <MonetizationOn />,
            <UICosts {...businessCardDealHistory.costs} />
          )
        }
      </div>
    );
  }

  Transition(props) {
    return <Slide direction="up" {...props} />;
  }

  NotAcceptDialog() {
    const { accept } = this.props;
    const { reason, expanded } = this.state;

    const disabled = accept.result === ResultEnum.wait;
    const open = expanded === constant.NOT_ACCEPT;

    return (
      <Dialog
        open={open}
        onClose={this.handleClickExpand(constant.NOT_ACCEPT)}
        TransitionComponent={this.Transition}
      >
        <DialogTitle>
          <FormattedMessage id="businessCardDealHistory.accept.reason.is" />
        </DialogTitle>
        <DialogContent>
          <TextField
            label={<FormattedMessage id="info.reason.is" />}
            type="text"
            multiline
            rows="5"
            rowsMax="5"
            fullWidth
            value={reason}
            disabled={disabled}
            onChange={this.handleChange(constant.REASON)}
          />
        </DialogContent>
        <DialogActions>
          <Button
            onClick={this.handleClick(constant.NOT_ACCEPT)}
            disabled={disabled}
            color="primary"
          >
            <FormattedMessage id="actions.notAccept" />
          </Button>
          <Button
            onClick={this.handleClickExpand(constant.NOT_ACCEPT)}
            disabled={disabled}
          >
            <FormattedMessage id="actions.back" />
          </Button>
        </DialogActions>
      </Dialog>
    );
  }

  VerifyDialog() {
    const { verify } = this.props;
    const { expanded, token, currentTimer } = this.state;

    const disabled = verify.result === ResultEnum.wait;
    const open = verify.result === ResultEnum.success ? false : expanded === constant.VERIFY;

    return (
      <Dialog
        open={open}
        onClose={this.handleClickExpand(constant.VERIFY)}
        TransitionComponent={this.Transition}
      >
        <DialogTitle>
          <FormattedMessage id="businessCardDealHistory.token.verify.is" />
        </DialogTitle>
        <DialogContent>
          {
            currentTimer - regular.businessCardDealHistory.expires.token > 0
              ? <div>
                <FormattedMessage id="businessCardDealHistory.token.verify.note" /> <UITimer
                  className="xcg-error"
                  value={currentTimer - regular.businessCardDealHistory.expires.token}
                />
              </div>
              : <TextField
                label={<FormattedMessage id="info.security.token.is" />}
                type="text"
                fullWidth
                value={token}
                disabled={disabled}
                onChange={this.handleChange(constant.TOKEN)}
              />
          }
        </DialogContent>
        <DialogActions>
          {
            currentTimer - regular.businessCardDealHistory.expires.token > 0
              ? null
              : <Button
                onClick={this.handleClick(constant.VERIFY)}
                disabled={disabled}
                color="secondary"
              >
                <FormattedMessage id="actions.vetify" />
              </Button>
          }
          <Button
            onClick={this.handleClickExpand(constant.VERIFY)}
            disabled={disabled}
          >
            <FormattedMessage id="actions.back" />
          </Button>
        </DialogActions>
      </Dialog>
    );
  }

  ReportDialog() {
    const { report } = this.props;
    const { expanded } = this.state;

    const disabled = report.result === ResultEnum.wait;
    const open = report.result === ResultEnum.success ? false : expanded === constant.REPORT;

    return (
      <InputReport
        open={open}
        disabled={disabled}
        Titile={<FormattedMessage id="businessCardDealHistory.report.is" />}
        handleClickClose={this.handleClickExpand(constant.REPORT)}
        handleSubmit={this.handleClick(constant.REPORT)}
      />
    );
  }

  initElement() {
    const { className, value } = this.props;
    const { businessCardDealHistory } = value;

    return (
      <div className={classNames('xcg-detail-business-card-deal-history', className)}>
        {
          businessCardDealHistory.result.is !== constant.WAIT
            ? <AlertSnackbar
              variant={businessCardDealHistory.result.is === constant.SUCCESS ? constant.SUCCESS : constant.ERROR}
              message={<FormattedMessage id={`businessCardDealHistory.${businessCardDealHistory.result.is}`} />}
            />
            : null
        }
        {
          businessCardDealHistory.result.is === constant.WAIT
            && businessCardDealHistory.status === constant.VERIFY
            && businessCardDealHistory.token
            ? <AlertSnackbar
              variant={constant.INFO}
              message={
                <div>
                  <FormattedMessage id="businessCardDealHistory.token.is" /> {businessCardDealHistory.token}
                </div>
              }
            />
            : null
        }
        {this.Stepper()}
        {this.Card()}
        {
          businessCardDealHistory.note
            ? <AlertSnackbar
              variant={constant.WARNING}
              message={businessCardDealHistory.note}
            />
            : null
        }
        {this.Extend()}
        {this.NotAcceptDialog()}
        {this.VerifyDialog()}
        {this.ReportDialog()}
      </div>
    );
  }

  render() {
    return this.initElement();
  }
};

DetailDealHistory.propTypes = {
  className: propTypes.string,
  validate: propTypes.object,
  interval: propTypes.number,
  value: propTypes.object.isRequired,
  accept: propTypes.object.isRequired,
  token: propTypes.object.isRequired,
  verify: propTypes.object.isRequired,
  report: propTypes.object.isRequired,
  getBusinessCardsDealHistoryIDRequest: propTypes.func.isRequired,
  acceptBusinessCardsDealHistoryIDRequest: propTypes.func.isRequired,
  getTokenBusinessCardsDealHistoryIDRequest: propTypes.func.isRequired,
  validateTokenBusinessCardsDealHistoryIDRequest: propTypes.func.isRequired,
  reportBusinessCardsDealHistoryIDRequest: propTypes.func.isRequired,
  listenBusinessCardsDealHistory: propTypes.func.isRequired,
};

DetailDealHistory.defaultProps = {
  className: '',
  validate: {
    accept: accept.body,
    validateToken: validateToken.body,
    report: report.body
  },
  interval: constant.ENUM.TIME.ONE_SECONDS
};

function mapStateToProps(state, props) {
  return createStructuredSelector({
    accept: makeSelectActionsBusinessCardDealHistory(constant.ACCEPT),
    token: makeSelectActionsBusinessCardDealHistory(constant.TOKEN),
    verify: makeSelectActionsBusinessCardDealHistory(constant.VERIFY),
    report: makeSelectActionsBusinessCardDealHistory(constant.REPORT),
  });
}

function mapDispatchToProps(dispatch, props) {
  return {
    getBusinessCardsDealHistoryIDRequest: (body) => {
      return dispatch(getBusinessCardsDealHistoryIDRequest(body));
    },
    acceptBusinessCardsDealHistoryIDRequest: (businessCardDealHistoryID, accept) => {
      return dispatch(acceptBusinessCardsDealHistoryIDRequest(businessCardDealHistoryID, accept));
    },
    getTokenBusinessCardsDealHistoryIDRequest: (businessCardDealHistoryID) => {
      return dispatch(getTokenBusinessCardsDealHistoryIDRequest(businessCardDealHistoryID));
    },
    validateTokenBusinessCardsDealHistoryIDRequest: (businessCardDealHistoryID, token) => {
      return dispatch(validateTokenBusinessCardsDealHistoryIDRequest(businessCardDealHistoryID, token));
    },
    reportBusinessCardsDealHistoryIDRequest: (businessCardDealHistoryID, report) => {
      return dispatch(reportBusinessCardsDealHistoryIDRequest(businessCardDealHistoryID, report));
    },
    listenBusinessCardsDealHistory: (fragment, businessCardDealHistoryID) => {
      return dispatch(listenBusinessCardsDealHistory(fragment, businessCardDealHistoryID));
    }
  };
}

module.exports = connect(
  mapStateToProps,
  mapDispatchToProps,
)(DetailDealHistory);
