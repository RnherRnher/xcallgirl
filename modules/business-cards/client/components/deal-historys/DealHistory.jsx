const React = require('react');
const propTypes = require('prop-types');
const classNames = require('classnames');
const constant = require('../../../../../commons/utils/constant');
const url = require('../../../../core/client/utils/url');
const { FormattedRelative } = require('react-intl');
const { push } = require('connected-react-router/immutable');
const { connect } = require('react-redux');
const {
  CheckCircle,
  CompareArrows,
  Fingerprint,
  Error,
  Textsms,
  Sync
} = require('@material-ui/icons');
const {
  ListItem,
  Avatar,
  ListItemAvatar,
  ListItemText,
  ListItemIcon
} = require('@material-ui/core');

const DealHistory = class DealHistory extends React.Component {
  constructor(props) {
    super(props);

    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(event) {
    const { redirect, value } = this.props;

    redirect(`${url.DEAL_HISTORY_GET}?id=${value.businessCardDealHistory._id}`);
  }

  Status() {
    const { value } = this.props;

    let Status = null;
    switch (value.businessCardDealHistory.status) {
      case constant.RESPONSE:
        Status = <CompareArrows />;
        break;
      case constant.TOKEN:
        Status = <Textsms />;
        break;
      case constant.VERIFY:
        Status = <Fingerprint />;
        break;
      default:
        break;
    }

    return <span>{Status}</span>;
  }

  Result() {
    const { value } = this.props;

    let Result = null;
    switch (value.businessCardDealHistory.result.is) {
      case constant.WAIT:
        Result = <Sync className="xcg-info" />;
        break;
      case constant.SUCCESS:
        Result = <CheckCircle className="xcg-success" />;
        break;
      case constant.FAILED:
        Result = <Error className="xcg-error" />;
        break;
      default:
        break;
    }

    return <span>{Result}</span>;
  }

  initElement() {
    const { className, value } = this.props;

    const user = value.user || value.provider;

    return (
      <div className={classNames('xcg-business-card-deal-history', className)}>
        <ListItem
          button
          onClick={this.handleClick}
        >
          <ListItemAvatar>
            <Avatar
              alt={user.nick}
              src={user.avatar}
            />
          </ListItemAvatar>
          <ListItemText
            inset
            primary={user.nick}
            secondary={<FormattedRelative value={value.businessCardDealHistory.initDate} />}
          />
          <ListItemIcon>
            <div className="xcg-list-icon">
              {this.Status()}
              {this.Result()}
            </div>
          </ListItemIcon>
        </ListItem>
      </div>
    );
  }

  render() {
    return this.initElement();
  }
};

DealHistory.propTypes = {
  className: propTypes.string,
  value: propTypes.object.isRequired,
  redirect: propTypes.func.isRequired
};

DealHistory.defaultProps = {
  className: ''
};

function mapStateToProps(state, props) {
  return {};
}

function mapDispatchToProps(dispatch, props) {
  return {
    redirect: (location) => {
      return dispatch(push(location));
    }
  };
}

module.exports = connect(
  mapStateToProps,
  mapDispatchToProps,
)(DealHistory);
