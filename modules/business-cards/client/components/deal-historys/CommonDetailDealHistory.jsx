const React = require('react');
const propTypes = require('prop-types');
const classNames = require('classnames');
const DetailDealHistory = require('./DetailDealHistory');
const { connect } = require('react-redux');
const { createStructuredSelector } = require('reselect');
const { getBusinessCardsDealHistoryIDRequest } = require('../../actions/deal-history.client.action');
const { Loadable } = require('../../../../core/client/components/Loadables');
const { LoadableDetailDealHistory } = require('../Loadables');
const { makeSelectDetailBusinessCardDealHistory } = require('../../reselects/deal-history.client.reselect');
const { makeSelectLocation } = require('../../../../core/client/reselects/router.client.reselect');
const { ContainerNotData } = require('../../../../core/client/components/Containers');

const CommonDetailDealHistory = class CommonDetailDealHistory extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    const { getBusinessCardsDealHistoryIDRequest, location } = this.props;

    const { search } = location;
    const businessCardDealHistoryID = search.slice(search.indexOf('=') + 1);

    getBusinessCardsDealHistoryIDRequest({ businessCardDealHistoryID });
  }

  initElement() {
    const { className, businessCardDealHistory } = this.props;

    return (
      <div className={classNames('xcg-common-detail-business-card-deal-history', className)}>
        <Loadable
          result={businessCardDealHistory.result}
          WaitNode={<LoadableDetailDealHistory />}
          SuccessNode={<DetailDealHistory value={businessCardDealHistory.data} />}
          FailedNode={
            <div>
              <ContainerNotData />
              <LoadableDetailDealHistory />
            </div>
          }
        />
      </div>
    );
  }

  render() {
    return this.initElement();
  }
};

CommonDetailDealHistory.propTypes = {
  className: propTypes.string,
  businessCardDealHistory: propTypes.object.isRequired,
  location: propTypes.object.isRequired,
  getBusinessCardsDealHistoryIDRequest: propTypes.func.isRequired,
};

CommonDetailDealHistory.defaultProps = {
  className: ''
};

function mapStateToProps(state, props) {
  return createStructuredSelector({
    businessCardDealHistory: makeSelectDetailBusinessCardDealHistory(),
    location: makeSelectLocation(),
  });
}

function mapDispatchToProps(dispatch, props) {
  return {
    getBusinessCardsDealHistoryIDRequest: (body) => {
      return dispatch(getBusinessCardsDealHistoryIDRequest(body));
    }
  };
}

module.exports = connect(
  mapStateToProps,
  mapDispatchToProps,
)(CommonDetailDealHistory);
