const React = require('react');
const propTypes = require('prop-types');
const classNames = require('classnames');
const constant = require('../../../../../commons/utils/constant');
const DealHistory = require('./DealHistory');
const { LoadableDealHistory } = require('../Loadables');
const { connect } = require('react-redux');
const { createStructuredSelector } = require('reselect');
const { WindowScroll } = require('../../../../core/client/components/Scrolls');
const { getBusinessCardsDealHistorysRequest } = require('../../actions/deal-history.client.action');
const { makeSelectSelfBusinessCardDealHistory } = require('../../reselects/deal-history.client.reselect');

const DealHistorys = class DealHistorys extends React.Component {
  constructor(props) {
    super(props);

    this.handleLoad = this.handleLoad.bind(this);
  }

  handleLoad(reload) {
    const { getBusinessCardsDealHistorysRequest, businessCardsDealHistorys } = this.props;

    getBusinessCardsDealHistorysRequest(
      constant.AUTO,
      businessCardsDealHistorys.skip,
      reload
    );
  }

  initElement() {
    const {
      className,
      businessCardsDealHistorys,
      rowHeight
    } = this.props;

    return (
      <div className={classNames('xcg-business-card-deal-historys', className)}>
        <WindowScroll
          result={businessCardsDealHistorys.result}
          handleLoadMore={this.handleLoad}
          initialLoad={!businessCardsDealHistorys.data.length}
          value={businessCardsDealHistorys.data}
          Children={DealHistory}
          Loadable={LoadableDealHistory}
          rowHeight={rowHeight}
        />
      </div>
    );
  }

  render() {
    return this.initElement();
  }
};

DealHistorys.propTypes = {
  className: propTypes.string,
  rowHeight: propTypes.number,
  businessCardsDealHistorys: propTypes.object.isRequired,
  getBusinessCardsDealHistorysRequest: propTypes.func.isRequired,
};

DealHistorys.defaultProps = {
  className: '',
  rowHeight: 76.5
};

function mapStateToProps(state, props) {
  return createStructuredSelector({
    businessCardsDealHistorys: makeSelectSelfBusinessCardDealHistory(),
  });
}

function mapDispatchToProps(dispatch, props) {
  return {
    getBusinessCardsDealHistorysRequest: (
      fragment,
      skip,
      reload
    ) => {
      return dispatch(getBusinessCardsDealHistorysRequest(
        fragment,
        skip,
        reload
      ));
    }
  };
}

module.exports = connect(
  mapStateToProps,
  mapDispatchToProps,
)(DealHistorys);
