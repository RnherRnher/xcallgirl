const CommonDetailDealHistory = require('./deal-historys/CommonDetailDealHistory');
const DealHistory = require('./deal-historys/DealHistory');
const DealHistorys = require('./deal-historys/DealHistorys');
const DetailDealHistory = require('./deal-historys/DetailDealHistory');

module.exports = {
  CommonDetailDealHistory,
  DealHistory,
  DealHistorys,
  DetailDealHistory
};
