const React = require('react');
const propTypes = require('prop-types');
const joi = require('joi');
const classNames = require('classnames');
const regular = require('../../../../../commons/utils/regular-expression');
const constant = require('../../../../../commons/utils/constant');
const url = require('../../../../core/client/utils/url');
const { connect } = require('react-redux');
const { createStructuredSelector } = require('reselect');
const { isEmpty } = require('lodash');
const { push } = require('connected-react-router/immutable');
const {
  FormattedMessage,
  FormattedRelative,
  FormattedNumber
} = require('react-intl');
const {
  Favorite,
  Visibility,
  Gavel,
  ModeComment,
  Flag,
  ExpandMore,
  LocationOn,
  MonetizationOn,
  Face,
  MoreVert,
  Edit,
  Delete,
  Info,
} = require('@material-ui/icons');
const {
  Card,
  CardHeader,
  Avatar,
  IconButton,
  CardMedia,
  CardContent,
  Typography,
  CardActions,
  Badge,
  CardActionArea,
  ExpansionPanel,
  ExpansionPanelSummary,
  ListItem,
  ListItemAvatar,
  ListItemText,
  ExpansionPanelDetails,
  Menu,
  MenuItem,
  ListItemIcon,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  Button,
  Slide,
} = require('@material-ui/core');
const { getAddress } = require('../../../../core/client/utils/middleware');
const { ResultEnum } = require('../../../../core/client/constants/core.client.constant');
const { comment, deletee } = require('../../../../../commons/modules/business-cards/validates/business-cards.validate');
const { UICosts, UIInfoBusinessCard } = require('../UIs');
const {
  likeBusinessCardRequest,
  unLikeBusinessCardRequest,
  getCommentsBusinessCardRequest,
  commentBusinessCardRequest,
  changeCommentBusinessCardRequest,
  deleteCommentBusinessCardRequest,
  dealBusinessCardRequest,
  reportBusinessCardRequest,
  deleteBusinessCardRequest,
  resetBusinessCardStore,
  changeStatusBusinessCardRequest,
  reUploadBusinessCardRequest
} = require('../../actions/business-card.client.action');
const {
  InputDeal,
  InputReport,
  InputMeasurements3
} = require('../Inputs');
const {
  UIListComment,
  UIStars,
  UIAddress
} = require('../../../../core/client/components/UIs');
const { InputPassword } = require('../../../../users/client/components/Inputs');
const {
  makeSelectLikesActionBusinessCard,
  makeSelectCommentsActionBusinessCard,
  makeSelectDealsActionBusinessCard,
  makeSelectReportsActionBusinessCard,
  makeSelectDeleteBusinessCard,
  makeSelectIsOwner
} = require('../../reselects/business-card.client.reselect');

const DetailBusinessCard = class DetailBusinessCard extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      clicked: false,
      anchorEl: null,
      expanded: null,
      activeStep: 0,
      password: '',
    };

    this.handleCheck = this.handleCheck.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleClickExpand = this.handleClickExpand.bind(this);
    this.handleClickMenuItem = this.handleClickMenuItem.bind(this);
    this.handleClickImage = this.handleClickImage.bind(this);
    this.handleClickPush = this.handleClickPush.bind(this);
    this.handleClickMore = this.handleClickMore.bind(this);
    this.handleCloseMenu = this.handleCloseMenu.bind(this);

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    const { value, comments } = this.props;

    const pushDisabled = comments && comments.get ? comments.get.result === ResultEnum.wait : false;
    if (!pushDisabled && !value.comments.data.length) {
      this.handleClickPush(constant.COMMENT)();
    }
  }

  componentWillUnmount() {
    const { resetBusinessCardStore, isOwner } = this.props;

    if (!isOwner) {
      resetBusinessCardStore();
    }
  }

  handleChange(type) {
    return (value) => {
      this.setState({ [type]: value });
    };
  }

  handleClickExpand(type) {
    return (event) => {
      this.setState((state, props) => {
        return {
          expanded: state.expanded === type ? null : type,
          password: ''
        };
      });
    };
  }

  handleClickMenuItem(type) {
    return (action, data) => {
      const {
        value,
        deleteCommentBusinessCardRequest,
        redirect
      } = this.props;

      switch (type) {
        case constant.COMMENT:
          switch (action) {
            case constant.DELETE:
              deleteCommentBusinessCardRequest(value._id, data._id);
              break;
            default:
              break;
          }
          break;
        case constant.EDIT:
          redirect(url.BUSINESS_CARD_EDIT);
          break;
        case constant.DELETE:
          this.handleClickExpand(type)();
          break;
        default:
          break;
      }

      this.handleCloseMenu();
    };
  }

  handleClickImage(event) {
    this.setState((state, props) => {
      let currentActiveStep = state.activeStep + 1;
      if (currentActiveStep === props.value.images.length) {
        currentActiveStep = 0;
      }
      return { activeStep: currentActiveStep };
    });
  }

  handleClickPush(type) {
    return (event) => {
      const {
        value,
        comments,
        like,
        likeBusinessCardRequest,
        unLikeBusinessCardRequest,
        getCommentsBusinessCardRequest,
      } = this.props;

      switch (type) {
        case constant.COMMENT:
          getCommentsBusinessCardRequest(
            value._id,
            comments && comments.get ? comments.get.max : -1,
          );
          break;
        case constant.LIKE:
          if (like) {
            switch (like.nextType) {
              case constant.LIKE:
                likeBusinessCardRequest(value._id);
                break;
              case constant.UNLIKE:
                unLikeBusinessCardRequest(value._id);
                break;
              default:
                break;
            }
          } else if (value.likes.isSelf) {
            unLikeBusinessCardRequest(value._id);
          } else {
            likeBusinessCardRequest(value._id);
          }
          break;
        default:
          break;
      }
    };
  }

  handleClickMore(type) {
    return (event) => {
      this.setState({
        clicked: type,
        anchorEl: event.target
      });
    };
  }

  handleCloseMenu(event) {
    this.setState({
      clicked: null,
      anchorEl: null
    });
  }

  handleCheck(body, validate) {
    return joi.validate(body, validate);
  }

  handleSubmit(type) {
    const {
      value,
      validate,
      commentBusinessCardRequest,
      changeCommentBusinessCardRequest,
      dealBusinessCardRequest,
      reportBusinessCardRequest,
      deleteBusinessCardRequest,
      changeStatusBusinessCardRequest,
      reUploadBusinessCardRequest
    } = this.props;

    switch (type) {
      case constant.COMMENT:
        return (action, comment) => {
          switch (action) {
            case constant.ADD:
              commentBusinessCardRequest(value._id, { comment: comment.msg });
              break;
            case constant.EDIT:
              changeCommentBusinessCardRequest(
                value._id,
                comment._id,
                { comment: comment.msg }
              );
              break;
            default:
              break;
          }
        };
      case constant.DEAL:
        return (deal) => {
          dealBusinessCardRequest(value._id, deal);
        };
      case constant.REPORT:
        return (report) => {
          reportBusinessCardRequest(value._id, report);
        };
      case constant.DELETE:
        return (event) => {
          const { password } = this.state;

          this.handleCheck({ password }, validate.delete)
            .then((deletee) => {
              this.handleClickExpand(constant.DELETE);

              deleteBusinessCardRequest(deletee);
            });
        };
      case constant.STATUS:
        return (event) => {
          changeStatusBusinessCardRequest();
        };
      case constant.REUPLOAD:
        return (event) => {
          reUploadBusinessCardRequest();
        };
      default:
        break;
    }
  }

  Transition(props) {
    return <Slide direction="up" {...props} />;
  }

  Owner() {
    const { deletee } = this.props;
    const {
      clicked,
      anchorEl,
      expanded
    } = this.state;

    const Menus = () => {
      return (
        <Menu
          anchorEl={anchorEl}
          open={clicked === constant.MENU}
          onClose={this.handleCloseMenu}
        >
          <MenuItem
            onClick={this.handleClickMenuItem(constant.EDIT)}
            value={constant.EDIT}
          >
            <ListItemIcon>
              <Edit />
            </ListItemIcon>
            <ListItemText
              inset
              primary={<FormattedMessage id="actions.edit" />}
            />
          </MenuItem>
          <MenuItem
            onClick={this.handleClickMenuItem(constant.DELETE)}
            value={constant.DELETE}
          >
            <ListItemIcon>
              <Delete />
            </ListItemIcon>
            <ListItemText
              inset
              primary={<FormattedMessage id="actions.delete" />}
            />
          </MenuItem>
        </Menu>
      );
    };

    const DeleteDialog = () => {
      const disabled = deletee.result === ResultEnum.wait;

      return (
        <Dialog
          open={expanded === constant.DELETE}
          onClose={this.handleClickExpand(constant.DELETE)}
          TransitionComponent={this.Transition}
        >
          <DialogTitle>
            <FormattedMessage id="businessCard.delete.is" />
          </DialogTitle>
          <DialogContent>
            <InputPassword
              handleChangeParent={this.handleChange(constant.PASSWORD)}
              disabled={disabled}
            />
          </DialogContent>
          <DialogActions>
            <Button
              onClick={this.handleSubmit(constant.DELETE)}
              disabled={disabled}
              color="primary"
            >
              <FormattedMessage id="actions.delete" />
            </Button>
            <Button onClick={this.handleClickExpand(constant.DELETE)}>
              <FormattedMessage id="actions.back" />
            </Button>
          </DialogActions>
        </Dialog>
      );
    };

    return (
      <div>
        {Menus()}
        {DeleteDialog()}
      </div>
    );
  }

  Actions() {
    const {
      value,
      like,
      countLimit
    } = this.props;

    const likeDisabled = like ? like.result === ResultEnum.wait : false;

    return (
      <CardActions className="xcg-card-action" disableActionSpacing>
        <IconButton
          className="xcg-card-action-button"
          disabled={likeDisabled}
          onClick={this.handleClickPush(constant.LIKE)}
        >
          <Badge
            badgeContent={
              value.likes.count > countLimit
                ? `${countLimit}+`
                : value.likes.count
            }
          >
            <Favorite color={value.likes.isSelf ? 'secondary' : 'inherit'} />
          </Badge>
        </IconButton>
        <IconButton className="xcg-card-action-button">
          <Badge
            badgeContent={
              value.views.count > countLimit
                ? `${countLimit}+`
                : value.views.count
            }
          >
            <Visibility />
          </Badge>
        </IconButton>
        <IconButton className="xcg-card-action-button">
          <Badge
            badgeContent={
              value.comments.count > countLimit
                ? `${countLimit}+`
                : value.comments.count
            }
          >
            <ModeComment />
          </Badge>
        </IconButton>
        <IconButton
          className="xcg-card-action-button"
          onClick={this.handleClickExpand(constant.DEAL)}
        >
          <Gavel />
        </IconButton>
        <IconButton
          className="xcg-card-action-button"
          onClick={this.handleClickExpand(constant.REPORT)}
        >
          <Flag />
        </IconButton>
      </CardActions>
    );
  }

  Comment() {
    const {
      value,
      comments,
      validate
    } = this.props;

    const pushDisabled = comments && comments.get ? comments.get.result === ResultEnum.wait : false;
    const addDisabled = comments && comments.post ? comments.post.result === ResultEnum.wait : false;

    return (
      <UIListComment
        ownerID={value.userID}
        type={constant.STATIC}
        value={value.comments.data}
        validate={validate.comment}
        disabled={{
          push: pushDisabled,
          add: addDisabled,
          edit: false
        }}
        pushTitleNode={<FormattedMessage id="info.comment.push" />}
        handleClickPush={this.handleClickPush(constant.COMMENT)}
        handleClickMenuItem={this.handleClickMenuItem(constant.COMMENT)}
        handleSubmit={this.handleSubmit(constant.COMMENT)}
      />
    );
  }

  Deal() {
    const { value, deal } = this.props;
    const { expanded } = this.state;

    const disabled = deal ? deal.result === ResultEnum.wait : false;
    const open = deal && deal.result === ResultEnum.success ? false : expanded === constant.DEAL;


    return (
      <InputDeal
        open={open}
        value={value.costs}
        disabled={disabled}
        handleClickClose={this.handleClickExpand(constant.DEAL)}
        handleSubmit={this.handleSubmit(constant.DEAL)}
      />
    );
  }

  Report() {
    const { report } = this.props;
    const { expanded } = this.state;

    const disabled = report ? report.result === ResultEnum.wait : false;
    const open = report && report.result === ResultEnum.success ? false : expanded === constant.REPORT;

    return (
      <InputReport
        open={open}
        disabled={disabled}
        Titile={<FormattedMessage id="businessCard.report.is" />}
        handleClickClose={this.handleClickExpand(constant.REPORT)}
        handleSubmit={this.handleSubmit(constant.REPORT)}
      />
    );
  }

  Extend() {
    const {
      value,
      isOwner,
    } = this.props;

    const Expansion = (title, icon, children) => {
      return (
        <ExpansionPanel>
          <ExpansionPanelSummary expandIcon={<ExpandMore />}>
            <ListItem>
              <ListItemAvatar>
                <Avatar>
                  {icon}
                </Avatar>
              </ListItemAvatar>
              <ListItemText
                inset
                primary={title}
              />
            </ListItem>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails className="xcg-info-group">
            {children}
          </ExpansionPanelDetails>
        </ExpansionPanel>
      );
    };

    return (
      <div>
        {
          Expansion(
            <FormattedMessage id="info.general.is" />,
            <Info />,
            <UIInfoBusinessCard
              value={value}
              isOwner={isOwner}
              handleSubmit={this.handleSubmit}
            />
          )
        }
        {
          Expansion(
            <FormattedMessage id="info.address.is" />,
            <LocationOn />,
            <UIAddress value={value.address} />
          )
        }
        {
          Expansion(
            <FormattedMessage id="businessCard.costs.is" />,
            <MonetizationOn />,
            <UICosts {...value.costs} />
          )
        }
        {
          Expansion(
            <FormattedMessage id="businessCard.measurements3.is" />,
            <Face />,
            <InputMeasurements3
              gender={value.gender}
              value={value.measurements3}
              disabled
            />
          )
        }
        {this.Comment()}
        {this.Deal()}
        {this.Report()}
      </div>
    );
  }

  initElement() {
    const {
      className,
      value,
      isOwner
    } = this.props;
    const { activeStep } = this.state;

    const defaultImage = {
      name: 'default',
      url: '/images/core/client/images/thumbnail.png'
    };

    let avatar = value.images.length ? value.images[0] : defaultImage;
    let image = defaultImage;

    value.images.forEach((img) => {
      if (img.name === regular.businessCard.image.names[0]) {
        avatar = img;
      }

      if (img.name === regular.businessCard.image.names[activeStep]) {
        image = img;
      }

      if (!isEmpty(avatar) && !isEmpty(image)) {
        return false;
      }
    });

    const { flag } = getAddress(constant.COUNTRY, value.address);
    const city = getAddress(constant.CITY, value.address).name;

    return (
      <div className={classNames('xcg-detail-business-card', className)}>
        <Card className="xcg-card">
          <CardHeader
            className="xcg-card-header"
            title={value.title}
            subheader={
              <div>
                <div>
                  {flag} {city}
                  <span className="xcg-space">&bull;</span>
                  <FormattedRelative value={value.initDate} />
                </div>
                <UIStars value={value.star} />
              </div>
            }
            avatar={
              <Avatar
                className="xcg-avatar"
                alt={avatar.name}
                src={avatar.url}
              />
            }
            action={
              isOwner
                ? <IconButton onClick={this.handleClickMore(constant.MENU)}>
                  <MoreVert />
                </IconButton>
                : null
            }
          />
          <CardContent className="xcg-card-content">
            <Typography className="xcg-descriptions">
              {value.descriptions}
            </Typography>
          </CardContent>
          <CardActionArea className="xcg-card-action-area" onClick={this.handleClickImage}>
            <CardMedia
              className="xcg-card-media"
              image={
                value.blurFace
                  ? image.url.replace(regular.app.rootUrlCloudinary, `${regular.app.rootUrlCloudinary}/e_blur_faces: 2000/`)
                  : image.url
              }
              title={image.name}
            />
          </CardActionArea>
          <CardContent className="xcg-card-content">
            <Typography className="xcg-count" variant="caption">
              <FormattedNumber value={value.likes.count} /> <FormattedMessage id="info.like.count" />
              <span className="xcg-space">&bull;</span>
              <FormattedNumber value={value.views.count} /> <FormattedMessage id="info.view.count" />
              <span className="xcg-space">&bull;</span>
              <FormattedNumber value={value.comments.count} /> <FormattedMessage id="info.comment.count" />
            </Typography>
          </CardContent>
          {this.Actions()}
        </Card>
        {this.Extend()}
        {isOwner ? this.Owner() : null}
      </div>
    );
  }

  render() {
    return this.initElement();
  }
};

DetailBusinessCard.propTypes = {
  className: propTypes.string,
  countLimit: propTypes.number,
  validate: propTypes.object,
  like: propTypes.object,
  comments: propTypes.object,
  deal: propTypes.object,
  report: propTypes.object,
  isOwner: propTypes.bool.isRequired,
  deletee: propTypes.object.isRequired,
  value: propTypes.object.isRequired,
  likeBusinessCardRequest: propTypes.func.isRequired,
  unLikeBusinessCardRequest: propTypes.func.isRequired,
  getCommentsBusinessCardRequest: propTypes.func.isRequired,
  commentBusinessCardRequest: propTypes.func.isRequired,
  changeCommentBusinessCardRequest: propTypes.func.isRequired,
  deleteCommentBusinessCardRequest: propTypes.func.isRequired,
  dealBusinessCardRequest: propTypes.func.isRequired,
  reportBusinessCardRequest: propTypes.func.isRequired,
  deleteBusinessCardRequest: propTypes.func.isRequired,
  changeStatusBusinessCardRequest: propTypes.func.isRequired,
  reUploadBusinessCardRequest: propTypes.func.isRequired,
  redirect: propTypes.func.isRequired,
  resetBusinessCardStore: propTypes.func.isRequired
};

DetailBusinessCard.defaultProps = {
  className: '',
  countLimit: constant.ENUM.COUNT.LIMIT,
  validate: {
    comment: comment.body,
    delete: deletee.body
  },
  like: null,
  comments: null,
  deal: null,
  report: null
};

function mapStateToProps(state, props) {
  const businessCardID = props.value._id;

  return createStructuredSelector({
    isOwner: makeSelectIsOwner(businessCardID),
    like: makeSelectLikesActionBusinessCard(businessCardID),
    comments: makeSelectCommentsActionBusinessCard(businessCardID),
    deal: makeSelectDealsActionBusinessCard(businessCardID),
    report: makeSelectReportsActionBusinessCard(businessCardID),
    deletee: makeSelectDeleteBusinessCard(),
  });
}

function mapDispatchToProps(dispatch, props) {
  return {
    likeBusinessCardRequest: (businessCardID) => {
      return dispatch(likeBusinessCardRequest(businessCardID));
    },
    unLikeBusinessCardRequest: (businessCardID) => {
      return dispatch(unLikeBusinessCardRequest(businessCardID));
    },
    getCommentsBusinessCardRequest: (businessCardID, max) => {
      return dispatch(getCommentsBusinessCardRequest(businessCardID, max));
    },
    commentBusinessCardRequest: (businessCardID, comment) => {
      return dispatch(commentBusinessCardRequest(businessCardID, comment));
    },
    changeCommentBusinessCardRequest: (
      businessCardID,
      commentID,
      comment
    ) => {
      return dispatch(changeCommentBusinessCardRequest(
        businessCardID,
        commentID,
        comment
      ));
    },
    deleteCommentBusinessCardRequest: (businessCardID, commentID) => {
      return dispatch(deleteCommentBusinessCardRequest(businessCardID, commentID));
    },
    dealBusinessCardRequest: (businessCardID, deal) => {
      return dispatch(dealBusinessCardRequest(businessCardID, deal));
    },
    reportBusinessCardRequest: (businessCardID, report) => {
      return dispatch(reportBusinessCardRequest(businessCardID, report));
    },
    deleteBusinessCardRequest: (deletee) => {
      return dispatch(deleteBusinessCardRequest(deletee));
    },
    changeStatusBusinessCardRequest: () => {
      return dispatch(changeStatusBusinessCardRequest());
    },
    reUploadBusinessCardRequest: () => {
      return dispatch(reUploadBusinessCardRequest());
    },
    redirect: (location) => {
      return dispatch(push(location));
    },
    resetBusinessCardStore: () => {
      return dispatch(resetBusinessCardStore());
    }
  };
}

module.exports = connect(
  mapStateToProps,
  mapDispatchToProps,
)(DetailBusinessCard);
