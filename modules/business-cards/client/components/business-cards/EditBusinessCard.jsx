const React = require('react');
const propTypes = require('prop-types');
const classNames = require('classnames');
const joi = require('joi');
const regular = require('../../../../../commons/utils/regular-expression');
const constant = require('../../../../../commons/utils/constant');
const { FormattedMessage } = require('react-intl');
const { connect } = require('react-redux');
const { createStructuredSelector } = require('reselect');
const {
  TextField,
  Button,
  Typography
} = require('@material-ui/core');
const { InputImages } = require('../Inputs');
const { ResultEnum } = require('../../../../core/client/constants/core.client.constant');
const { SelectAddress } = require('../../../../core/client/components/Selects');
const { changeAddress, changeDescriptions } = require('../../../../../commons/modules/business-cards/validates/business-cards.validate');
const {
  uploadImageBusinessCardRequest,
  deleteImageBusinessCardRequest,
  changeAddressBusinessCardRequest,
  changeDescriptionsBusinessCardRequest
} = require('../../../../business-cards/client/actions/business-card.client.action');
const { Loadable } = require('../../../../core/client/components/Loadables');
const { makeSelectSelfBusinessCard, makeSelectChangesBusinessCard } = require('../../reselects/business-card.client.reselect');

const EditBusinessCard = class EditBusinessCard extends React.Component {
  constructor(props) {
    super(props);

    const { businessCard } = this.props;
    this.state = {
      publicID: null,
      name: null,
      descriptions: businessCard.data ? businessCard.data.descriptions : null
    };

    this.addressRef = React.createRef();

    this.handleChangeFile = this.handleChangeFile.bind(this);
    this.handleClickRemoveFile = this.handleClickRemoveFile.bind(this);
    this.handleChangeDescriptions = this.handleChangeDescriptions.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChangeFile(image) {
    const { uploadImageBusinessCardRequest } = this.props;

    this.setState({ name: image.name });

    uploadImageBusinessCardRequest(image);
  }

  handleClickRemoveFile(publicID) {
    const { deleteImageBusinessCardRequest } = this.props;

    this.setState({ publicID });

    deleteImageBusinessCardRequest(publicID);
  }

  handleChangeDescriptions(event) {
    const { value } = event.target;

    this.setState((state, props) => {
      return { descriptions: value };
    });
  }

  handleSubmit(type) {
    return (event) => {
      const {
        validate,
        changeAddressBusinessCardRequest,
        changeDescriptionsBusinessCardRequest
      } = this.props;

      switch (type) {
        case constant.DESCRIPTIONS: {
          const { descriptions } = this.state;

          joi.validate({ descriptions }, validate.changeDescriptions)
            .then((descriptions) => {
              changeDescriptionsBusinessCardRequest(descriptions);
            });
        }
          break;
        case constant.ADDRESS: {
          const {
            country,
            city,
            county,
            local
          } = this.addressRef.current.state;

          joi.validate({
            address: {
              country,
              city,
              county,
              local
            }
          }, validate.changeAddress)
            .then((address) => {
              changeAddressBusinessCardRequest(address);
            });
        }
          break;
        default:
          break;
      }

      event.preventDefault();
    };
  }

  Container() {
    const {
      businessCard,
      responseChanges,
      image
    } = this.props;
    const {
      descriptions,
      publicID,
      name
    } = this.state;

    const responseImage = responseChanges[constant.IMAGE];
    const disabledInputImages = (responseImage[publicID]
      ? responseImage[publicID].result === ResultEnum.wait : false)
      || (responseImage[name]
        ? responseImage[name].result === ResultEnum.wait : false);
    const disabledInputDescriptions = responseChanges[constant.DESCRIPTIONS].result === ResultEnum.wait;
    const disabledInputAddress = responseChanges[constant.ADDRESS].result === ResultEnum.wait;

    const getDataInputImage = () => {
      return {
        images: image.names.map((name) => {
          let findIndex = -1;
          for (let index = 0; index < businessCard.data.images.length; index++) {
            if (businessCard.data.images[index].name === name) {
              findIndex = index;
              break;
            }
          }

          return {
            url: findIndex === -1 ? image.url[name] : businessCard.data.images[findIndex].url,
            name: findIndex === -1 ? name : businessCard.data.images[findIndex].name,
            isDefault: findIndex === -1,
            blob: null,
            publicID: findIndex === -1 ? undefined : businessCard.data.images[findIndex].publicID
          };
        })
      };
    };

    return (
      <div>
        <InputImages
          isUpdatePropsToState
          disabled={disabledInputImages}
          value={businessCard.data ? getDataInputImage() : null}
          handleClickRemoveFile={this.handleClickRemoveFile}
          handleChangeFile={this.handleChangeFile}
        />
        <div className="xcg-group-input">
          <TextField
            label={<FormattedMessage id="businessCard.descriptions.is" />}
            type="text"
            multiline
            rows="5"
            rowsMax="5"
            fullWidth
            value={descriptions}
            disabled={disabledInputDescriptions}
            onChange={this.handleChangeDescriptions}
          />
          <div className="xcg-primary-btn">
            <Button
              onClick={this.handleSubmit(constant.DESCRIPTIONS)}
              fullWidth
              disabled={disabledInputDescriptions}
              color="secondary"
            >
              <Typography
                align="center"
                variant="button"
                color="secondary"
              >
                <FormattedMessage id="businessCard.descriptions.change.is" />
              </Typography>
            </Button>
          </div>
        </div>
        <div className="xcg-group-input">
          <SelectAddress
            ref={this.addressRef}
            disabled={disabledInputAddress}
            check
            value={businessCard.data ? businessCard.data.address : null}
            required={{
              city: true,
              county: true,
              local: true
            }}
          />
          <div className="xcg-primary-btn">
            <Button
              onClick={this.handleSubmit(constant.ADDRESS)}
              fullWidth
              disabled={disabledInputAddress}
              color="secondary"
            >
              <Typography
                align="center"
                variant="button"
                color="secondary"
              >
                <FormattedMessage id="businessCard.address.change.is" />
              </Typography>
            </Button>
          </div>
        </div>
      </div>
    );
  }

  initElement() {
    const { className, businessCard } = this.props;

    return (
      <div className={classNames('xcg-edit-business-card', className)}>
        <Loadable
          result={businessCard.result}
          SuccessNode={this.Container()}
        />
      </div>
    );
  }

  render() {
    return this.initElement();
  }
};

EditBusinessCard.propTypes = {
  className: propTypes.string,
  validate: propTypes.object,
  image: propTypes.object,
  businessCard: propTypes.object.isRequired,
  responseChanges: propTypes.object.isRequired,
  uploadImageBusinessCardRequest: propTypes.func.isRequired,
  deleteImageBusinessCardRequest: propTypes.func.isRequired,
  changeAddressBusinessCardRequest: propTypes.func.isRequired,
  changeDescriptionsBusinessCardRequest: propTypes.func.isRequired,
};

EditBusinessCard.defaultProps = {
  className: '',
  validate: {
    changeAddress: changeAddress.body,
    changeDescriptions: changeDescriptions.body
  },
  image: {
    names: regular.businessCard.image.names,
    url: regular.businessCard.image.url
  }
};

function mapStateToProps(state, props) {
  return createStructuredSelector({
    businessCard: makeSelectSelfBusinessCard(),
    responseChanges: makeSelectChangesBusinessCard()
  });
}

function mapDispatchToProps(dispatch, props) {
  return {
    uploadImageBusinessCardRequest: (image) => {
      return dispatch(uploadImageBusinessCardRequest(image));
    },
    deleteImageBusinessCardRequest: (publicID) => {
      return dispatch(deleteImageBusinessCardRequest(publicID));
    },
    changeAddressBusinessCardRequest: (address) => {
      return dispatch(changeAddressBusinessCardRequest(address));
    },
    changeDescriptionsBusinessCardRequest: (descriptions) => {
      return dispatch(changeDescriptionsBusinessCardRequest(descriptions));
    }
  };
}

module.exports = connect(
  mapStateToProps,
  mapDispatchToProps,
)(EditBusinessCard);
