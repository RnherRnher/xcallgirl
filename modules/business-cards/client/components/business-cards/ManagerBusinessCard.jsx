const React = require('react');
const propTypes = require('prop-types');
const classNames = require('classnames');
const CreateBusinessCard = require('./CreateBusinessCard');
const DetailBusinessCard = require('./DetailBusinessCard');
const { connect } = require('react-redux');
const { createStructuredSelector } = require('reselect');
const { Loadable } = require('../../../../core/client/components/Loadables');
const { LoadableBusinessCard } = require('../Loadables');
const { makeSelectSelfUser } = require('../../../../users/client/reselects/user.client.reselect');
const { makeSelectSelfBusinessCard } = require('../../reselects/business-card.client.reselect');
const { ContainerNotData, ContainerNotVerifyAccount } = require('../../../../core/client/components/Containers');
const { getProfileBusinessCardRequest } = require('../../actions/business-card.client.action');

const ManagerBusinessCard = class ManagerBusinessCard extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    const { getProfileBusinessCardRequest } = this.props;

    getProfileBusinessCardRequest(false);
  }

  initElement() {
    const {
      className,
      user,
      businessCard,
    } = this.props;

    return (
      <div className={classNames('xcg-manager-business-card', className)}>
        <Loadable
          result={user.result}
          SuccessNode={
            user.data && user.data.verify.account.is
              ? <Loadable
                result={businessCard.result}
                WaitNode={<LoadableBusinessCard />}
                SuccessNode={<DetailBusinessCard value={businessCard.data} />}
                FailedNode={<CreateBusinessCard />}
              />
              : <ContainerNotVerifyAccount />
          }
          FailedNode={
            <div>
              <ContainerNotData />
              <LoadableBusinessCard />
            </div>
          }
        />
      </div>
    );
  }

  render() {
    return this.initElement();
  }
};

ManagerBusinessCard.propTypes = {
  className: propTypes.string,
  businessCard: propTypes.object.isRequired,
  user: propTypes.object.isRequired,
  getProfileBusinessCardRequest: propTypes.func.isRequired
};

ManagerBusinessCard.defaultProps = {
  className: ''
};

function mapStateToProps(state, props) {
  return createStructuredSelector({
    businessCard: makeSelectSelfBusinessCard(),
    user: makeSelectSelfUser()
  });
}

function mapDispatchToProps(dispatch, props) {
  return {
    getProfileBusinessCardRequest: (isAlert) => {
      return dispatch(getProfileBusinessCardRequest(isAlert));
    }
  };
}

module.exports = connect(
  mapStateToProps,
  mapDispatchToProps,
)(ManagerBusinessCard);
