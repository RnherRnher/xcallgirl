const React = require('react');
const propTypes = require('prop-types');
const classNames = require('classnames');
const joi = require('joi');
const { some, assignIn } = require('lodash');
const { FormattedMessage } = require('react-intl');
const { createStructuredSelector } = require('reselect');
const { connect } = require('react-redux');
const { KeyboardArrowLeft, KeyboardArrowRight } = require('@material-ui/icons');
const {
  Button,
  MobileStepper,
  TextField
} = require('@material-ui/core');
const {
  InputCosts,
  InputMeasurements3,
  InputImages
} = require('../Inputs');
const { create } = require('../../../../../commons/modules/business-cards/validates/business-cards.validate');
const { ResultEnum } = require('../../../../core/client/constants/core.client.constant');
const { SelectAddress } = require('../../../../core/client/components/Selects');
const { createBusinessCardRequest } = require('../../actions/business-card.client.action');
const { makeSelectCreateBusinessCard } = require('../../reselects/business-card.client.reselect');
const { makeSelectSelfUserInfo } = require('../../../../users/client/reselects/user.client.reselect');

const CreateBusinessCard = class CreateBusinessCard extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      activeStep: 0,
      images: [],
      body: {
        title: '',
        descriptions: '',
        blurFace: false,
        measurements3: {},
        address: {},
        costs: {},
      }
    };

    this.imagesRef = React.createRef();
    this.measurements3Ref = React.createRef();
    this.costsRef = React.createRef();
    this.addressRef = React.createRef();

    this.handleCheck = this.handleCheck.bind(this);
    this.handleChangeInfo = this.handleChangeInfo.bind(this);

    this.handleClickStep = this.handleClickStep.bind(this);
  }

  getData() {
    const { disabled, self } = this.props;
    const { body, images } = this.state;

    return [
      <div>
        <InputImages
          ref={this.imagesRef}
          value={{ images, blurFace: body.blurFace }}
          disabled={disabled}
        />
        <div className="xcg-group-input">
          <TextField
            label={<FormattedMessage id="businessCard.title.is" />}
            type="text"
            fullWidth
            value={body.title}
            disabled={disabled}
            onChange={this.handleChangeInfo('title')}
          />
          <TextField
            label={<FormattedMessage id="businessCard.descriptions.is" />}
            type="text"
            multiline
            rows="5"
            rowsMax="5"
            fullWidth
            value={body.descriptions}
            disabled={disabled}
            onChange={this.handleChangeInfo('descriptions')}
          />
        </div>
      </div>,
      <InputMeasurements3
        ref={this.measurements3Ref}
        gender={self.gender}
        value={body.measurements3}
        disabled={disabled}
      />,
      <InputCosts
        ref={this.costsRef}
        value={body.costs}
        disabled={disabled}
      />,
      <SelectAddress
        className="xcg-group-input"
        check
        ref={this.addressRef}
        disabled={disabled}
        value={body.address}
        required={{
          city: true,
          county: true,
          local: true
        }}
      />
    ];
  }

  handleChangeInfo(type) {
    return (event) => {
      const { value } = event.target;

      this.setState((state, props) => {
        return {
          body: assignIn(
            state.body,
            { [type]: value }
          )
        };
      });
    };
  }

  handleCheck(body) {
    const { validate } = this.props;

    return joi.validate(body, validate);
  }

  handleSubmit(businessCard) {
    const { createBusinessCardRequest } = this.props;
    const { images } = this.state;

    this.handleCheck(businessCard)
      .then((businessCard) => {
        createBusinessCardRequest(businessCard, images);
      });
  }

  nextState() {
    const { body, activeStep, images } = this.state;
    let errorStep = false;
    let currentImages = images;

    switch (activeStep) {
      case 0: {
        const { images, blurFace } = this.imagesRef.current.state;

        body.blurFace = blurFace;
        currentImages = images;

        errorStep = !some(images, ['isDefault', false])
          || !body.title
          || !body.descriptions;
      }
        break;
      case 1: {
        const {
          breast,
          waist,
          hips
        } = this.measurements3Ref.current.state;

        body.measurements3 = {
          breast,
          waist,
          hips
        };

        errorStep = breast === 0
          || waist === 0
          || hips === 0;
      }
        break;
      case 2: {
        const {
          rooms,
          services,
          currencyUnit
        } = this.costsRef.current.state;

        body.costs = {
          rooms,
          services,
          currencyUnit
        };

        errorStep = !services.length || !currencyUnit;
      }
        break;
      case 3: {
        const {
          country,
          city,
          county,
          local,
          error
        } = this.addressRef.current.state;

        body.address = {
          country,
          city,
          county,
          local,
        };

        errorStep = !country
          || !city
          || !county
          || !local
          || error;
      }
        break;
      default:
        break;
    }

    return {
      images: currentImages,
      body,
      errorStep
    };
  }

  handleClickStep(num) {
    return (event) => {
      const promise = new Promise((resovle, reject) => {
        this.setState((state, props) => {
          const { maxSteps } = props;
          const { activeStep, body, images } = state;
          let nextState = {
            images,
            body,
            errorStep: false
          };

          if (num === 1) {
            nextState = this.nextState();
          }

          if (activeStep === maxSteps - 1 && !nextState.errorStep) {
            resovle(nextState.body);
          }

          return {
            activeStep: nextState.errorStep
              ? num ? activeStep : activeStep + num
              : (activeStep + num) === maxSteps ? activeStep : activeStep + num,
            body: nextState.body,
            images: nextState.images
          };
        });
      });

      promise.then((businessCard) => {
        this.handleSubmit(businessCard);
      });
    };
  }

  initElement() {
    const { className, maxSteps, createResponse } = this.props;
    const { activeStep } = this.state;

    const disabled = createResponse.result === ResultEnum.wait;

    return (
      <div className={classNames('xcg-create-business-card', className)}>
        <MobileStepper
          variant="progress"
          steps={maxSteps}
          position="static"
          activeStep={activeStep}
          nextButton={
            <Button
              size="small"
              onClick={this.handleClickStep(1)}
              disabled={disabled}
              color={
                activeStep === maxSteps - 1
                  ? 'primary'
                  : 'inherit'
              }
            >
              {activeStep === maxSteps - 1
                ? <FormattedMessage id="businessCard.create.is" />
                : <FormattedMessage id="actions.next" />}
              <KeyboardArrowRight />
            </Button>
          }
          backButton={
            <Button
              size="small"
              onClick={this.handleClickStep(-1)}
              disabled={(activeStep === 0) || disabled}
            >
              <KeyboardArrowLeft />
              <FormattedMessage id="actions.back" />
            </Button>
          }
        />
        {this.getData()[activeStep]}
      </div>
    );
  }

  render() {
    return this.initElement();
  }
};

CreateBusinessCard.propTypes = {
  className: propTypes.string,
  disabled: propTypes.bool,
  validate: propTypes.any,
  maxSteps: propTypes.number,
  self: propTypes.object.isRequired,
  createResponse: propTypes.object.isRequired,
  createBusinessCardRequest: propTypes.func.isRequired,
};

CreateBusinessCard.defaultProps = {
  className: '',
  disabled: false,
  validate: create.body,
  maxSteps: 4,
};

function mapStateToProps(state, props) {
  return createStructuredSelector({
    self: makeSelectSelfUserInfo(),
    createResponse: makeSelectCreateBusinessCard(),
  });
}

function mapDispatchToProps(dispatch, props) {
  return {
    createBusinessCardRequest: (businessCard, images) => {
      return dispatch(createBusinessCardRequest(businessCard, images));
    }
  };
}

module.exports = connect(
  mapStateToProps,
  mapDispatchToProps,
)(CreateBusinessCard);
