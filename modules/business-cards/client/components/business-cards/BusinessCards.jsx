const React = require('react');
const propTypes = require('prop-types');
const classNames = require('classnames');
const joi = require('joi');
const constant = require('../../../../../commons/utils/constant');
const { connect } = require('react-redux');
const { createStructuredSelector } = require('reselect');
const { FormattedMessage } = require('react-intl');
const {
  Search
} = require('@material-ui/icons');
const {
  ListItem,
  ListItemSecondaryAction,
  IconButton,
  Dialog,
  Slide,
  DialogContent,
  DialogActions,
  Button
} = require('@material-ui/core');
const { BusinessCardsWindowScroll } = require('../Scrolls');
const { SelectAddress } = require('../../../../core/client/components/Selects');
const { SelectGender } = require('../../../../users/client/components/Selects');
const { getBusinessCardsRequest, resetBusinessCardStore } = require('../../actions/business-card.client.action');
const { ResultEnum } = require('../../../../core/client/constants/core.client.constant');
const { makeSelectListBusinessCards } = require('../../reselects/business-card.client.reselect');
const { makeSelectSelfUserInfo } = require('../../../../users/client/reselects/user.client.reselect');
const { extendSearchBody } = require('../../../../../commons/modules/business-cards/validates/business-cards.validate');

const BusinessCards = class BusinessCards extends React.Component {
  constructor(props) {
    super(props);

    const { self } = this.props;
    const address = self.address.root;
    this.state = {
      expanded: null,
      extendSearch: {
        gender: self.gender,
        address: {
          country: address.country
        }
      }
    };

    this.genderRef = React.createRef();
    this.addressRef = React.createRef();

    this.handleLoad = this.handleLoad.bind(this);
    this.handleCheck = this.handleCheck.bind(this);
    this.handleClickExpand = this.handleClickExpand.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }

  componentWillUnmount() {
    const { resetBusinessCardStore } = this.props;

    resetBusinessCardStore();
  }

  getExtendSearch() {
    const {
      country,
      city,
      county,
    } = this.addressRef.current.state;
    const { gender } = this.genderRef.current.state;

    const address = {};
    if (country) {
      address.country = country;
    }

    if (city) {
      address.city = city;
    }

    if (county) {
      address.county = county;
    }

    return {
      gender,
      address: {
        country,
        city: city || undefined,
        county: county || undefined,
      }
    };
  }

  handleLoad(reload) {
    const {
      validate,
      businessCards,
      getBusinessCardsRequest
    } = this.props;
    const { extendSearch } = this.state;

    this.handleCheck(extendSearch, validate)
      .then((extendSearch) => {
        getBusinessCardsRequest(
          constant.EXTEND,
          businessCards.skip,
          reload,
          extendSearch
        );
      });
  }

  handleCheck(body, validate) {
    return joi.validate(body, validate);
  }

  handleClickExpand(type) {
    return (event) => {
      this.setState((state, props) => {
        return {
          expanded: state.expanded === type ? null : type,
        };
      });
    };
  }

  handleClick(type) {
    return (event) => {
      const {
        validate,
        businessCards,
        getBusinessCardsRequest
      } = this.props;

      switch (type) {
        case constant.SEARCH:
          this.setState((state, props) => {
            const extendSearch = this.getExtendSearch();

            this.handleCheck(extendSearch, validate)
              .then((extendSearch) => {
                getBusinessCardsRequest(
                  constant.EXTEND,
                  businessCards.skip,
                  true,
                  extendSearch
                );
              });

            return {
              expanded: null,
              extendSearch
            };
          });
          break;
        default:
          break;
      }
    };
  }

  Transition(props) {
    return <Slide direction="up" {...props} />;
  }

  Search() {
    const { businessCards, self } = this.props;
    const { expanded, extendSearch } = this.state;

    const SearchDialog = () => {
      const disabled = businessCards.result === ResultEnum.wait;

      return (
        <Dialog
          open={expanded === constant.SEARCH}
          onClose={this.handleClickExpand(constant.SEARCH)}
          TransitionComponent={this.Transition}
        >
          <DialogContent>
            <SelectGender
              isUpdatePropsToState
              ref={this.genderRef}
              disabled={disabled}
              value={extendSearch.gender}
            />
            <SelectAddress
              isUpdatePropsToState
              ref={this.addressRef}
              disabled={disabled}
              value={extendSearch.address}
            />
          </DialogContent>
          <DialogActions>
            <Button
              onClick={this.handleClick(constant.SEARCH)}
              disabled={disabled}
              color="secondary"
            >
              <FormattedMessage id="actions.search" />
            </Button>
            <Button onClick={this.handleClickExpand(constant.SEARCH)}>
              <FormattedMessage id="actions.back" />
            </Button>
          </DialogActions>
        </Dialog>
      );
    };

    return (
      <div className="xcg-search">
        <ListItem>
          {self.address.flag}
          <ListItemSecondaryAction>
            <IconButton onClick={this.handleClickExpand(constant.SEARCH)}>
              <Search />
            </IconButton>
          </ListItemSecondaryAction>
        </ListItem>
        {SearchDialog()}
      </div>
    );
  }

  initElement() {
    const { className } = this.props;

    return (
      <div className={classNames('xcg-business-cards', className)}>
        <BusinessCardsWindowScroll handleLoadMore={this.handleLoad} />
        {this.Search()}
      </div>
    );
  }

  render() {
    return this.initElement();
  }
};

BusinessCards.propTypes = {
  className: propTypes.string,
  validate: propTypes.any,
  self: propTypes.object.isRequired,
  businessCards: propTypes.object.isRequired,
  getBusinessCardsRequest: propTypes.func.isRequired,
  resetBusinessCardStore: propTypes.func.isRequired
};

BusinessCards.defaultProps = {
  className: '',
  validate: extendSearchBody
};

function mapStateToProps(state, props) {
  return createStructuredSelector({
    self: makeSelectSelfUserInfo(),
    businessCards: makeSelectListBusinessCards(),
  });
}

function mapDispatchToProps(dispatch, props) {
  return {
    getBusinessCardsRequest: (
      fragment,
      skip,
      reload,
      extendSearch
    ) => {
      return dispatch(getBusinessCardsRequest(
        fragment,
        skip,
        reload,
        extendSearch
      ));
    },
    resetBusinessCardStore: () => {
      return dispatch(resetBusinessCardStore());
    }
  };
}

module.exports = connect(
  mapStateToProps,
  mapDispatchToProps,
)(BusinessCards);
