const React = require('react');
const propTypes = require('prop-types');
const classNames = require('classnames');
const DetailBusinessCard = require('./DetailBusinessCard');
const { connect } = require('react-redux');
const { createStructuredSelector } = require('reselect');
const { getBusinessCardIDRequest } = require('../../actions/business-card.client.action');
const { Loadable } = require('../../../../core/client/components/Loadables');
const { LoadableBusinessCard } = require('../Loadables');
const { makeSelectDetailBusinessCard } = require('../../reselects/business-card.client.reselect');
const { makeSelectLocation } = require('../../../../core/client/reselects/router.client.reselect');
const { ContainerNotData } = require('../../../../core/client/components/Containers');

const CommonDetailBusinessCard = class CommonDetailBusinessCard extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    const { getBusinessCardIDRequest, location } = this.props;

    const { search } = location;
    const businessCardID = search.slice(search.indexOf('=') + 1);

    getBusinessCardIDRequest(businessCardID);
  }

  initElement() {
    const { className, businessCard } = this.props;

    return (
      <div className={classNames('xcg-common-detail-business-card', className)}>
        <Loadable
          result={businessCard.result}
          WaitNode={<LoadableBusinessCard />}
          SuccessNode={<DetailBusinessCard value={businessCard.data} />}
          FailedNode={
            <div>
              <ContainerNotData />
              <LoadableBusinessCard />
            </div>
          }
        />
      </div>
    );
  }

  render() {
    return this.initElement();
  }
};

CommonDetailBusinessCard.propTypes = {
  className: propTypes.string,
  businessCard: propTypes.object.isRequired,
  location: propTypes.object.isRequired,
  getBusinessCardIDRequest: propTypes.func.isRequired,
};

CommonDetailBusinessCard.defaultProps = {
  className: ''
};

function mapStateToProps(state, props) {
  return createStructuredSelector({
    businessCard: makeSelectDetailBusinessCard(),
    location: makeSelectLocation(),
  });
}

function mapDispatchToProps(dispatch, props) {
  return {
    getBusinessCardIDRequest: (businessCardID) => {
      return dispatch(getBusinessCardIDRequest(businessCardID));
    }
  };
}

module.exports = connect(
  mapStateToProps,
  mapDispatchToProps,
)(CommonDetailBusinessCard);
