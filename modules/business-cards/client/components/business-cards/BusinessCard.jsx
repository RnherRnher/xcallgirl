const React = require('react');
const propTypes = require('prop-types');
const classNames = require('classnames');
const regular = require('../../../../../commons/utils/regular-expression');
const constant = require('../../../../../commons/utils/constant');
const url = require('../../../../core/client/utils/url');
const { getAddress } = require('../../../../core/client/utils/middleware');
const { push } = require('connected-react-router/immutable');
const { connect } = require('react-redux');
const { isEmpty } = require('lodash');
const { FormattedMessage, FormattedRelative } = require('react-intl');
const { createStructuredSelector } = require('reselect');
const {
  Favorite,
  Visibility,
  Gavel,
  ModeComment,
  Flag,
} = require('@material-ui/icons');
const {
  Card,
  CardHeader,
  Avatar,
  IconButton,
  CardMedia,
  CardContent,
  Typography,
  CardActions,
  Badge,
  CardActionArea,
} = require('@material-ui/core');
const {
  likeBusinessCardRequest,
  unLikeBusinessCardRequest,
  getCommentsBusinessCardRequest,
  commentBusinessCardRequest,
  changeCommentBusinessCardRequest,
  deleteCommentBusinessCardRequest,
  dealBusinessCardRequest,
  reportBusinessCardRequest
} = require('../../actions/business-card.client.action');
const { UIListComment, UIStars } = require('../../../../core/client/components/UIs');
const { ResultEnum } = require('../../../../core/client/constants/core.client.constant');
const { comment } = require('../../../../../commons/modules/business-cards/validates/business-cards.validate');
const { InputDeal, InputReport } = require('../Inputs');
const {
  makeSelectLikesActionBusinessCard,
  makeSelectCommentsActionBusinessCard,
  makeSelectDealsActionBusinessCard,
  makeSelectReportsActionBusinessCard
} = require('../../reselects/business-card.client.reselect');

const BusinessCard = class BusinessCard extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      expanded: null,
      activeStep: 0,
      dataLoad: []
    };

    this.handleClickExpand = this.handleClickExpand.bind(this);
    this.handleClickMenuItem = this.handleClickMenuItem.bind(this);
    this.handleClickImage = this.handleClickImage.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleClickPush = this.handleClickPush.bind(this);
  }

  handleClickExpand(type) {
    return (event) => {
      this.setState((state, props) => {
        const { dataLoad, expanded } = state;

        const currentExpanded = expanded === type ? null : type;

        switch (currentExpanded) {
          case constant.COMMENT:
            if (dataLoad.indexOf(currentExpanded) === -1) {
              this.handleClickPush(constant.COMMENT)();
              dataLoad.push(currentExpanded);
            }
            break;
          case constant.VIEW:
            props.redirect(`${url.BUSINESS_CARD_GET}?id=${props.value._id}`);
            break;
          default:
            break;
        }

        return {
          expanded: currentExpanded,
          dataLoad
        };
      });
    };
  }

  handleClickMenuItem(type) {
    return (action, data) => {
      const { value, deleteCommentBusinessCardRequest } = this.props;

      switch (type) {
        case constant.COMMENT:
          switch (action) {
            case constant.DELETE:
              deleteCommentBusinessCardRequest(
                value._id,
                data._id
              );
              break;
            default:
              break;
          }
          break;
        default:
          break;
      }
    };
  }

  handleClickImage(event) {
    this.setState((state, props) => {
      let currentActiveStep = state.activeStep + 1;
      if (currentActiveStep === props.value.images.length) {
        currentActiveStep = 0;
      }
      return { activeStep: currentActiveStep };
    });
  }

  handleSubmit(type) {
    const {
      value,
      commentBusinessCardRequest,
      changeCommentBusinessCardRequest,
      dealBusinessCardRequest,
      reportBusinessCardRequest
    } = this.props;

    switch (type) {
      case constant.COMMENT:
        return (action, comment) => {
          switch (action) {
            case constant.ADD:
              commentBusinessCardRequest(
                value._id,
                { comment: comment.msg }
              );
              break;
            case constant.EDIT:
              changeCommentBusinessCardRequest(
                value._id,
                comment._id,
                { comment: comment.msg }
              );
              break;
            default:
              break;
          }
        };
      case constant.DEAL:
        return (deal) => {
          dealBusinessCardRequest(value._id, deal);
        };
      case constant.REPORT:
        return (report) => {
          reportBusinessCardRequest(value._id, report);
        };
      default:
        break;
    }
  }

  handleClickPush(type) {
    return (event) => {
      const {
        value,
        comments,
        like,
        likeBusinessCardRequest,
        unLikeBusinessCardRequest,
        getCommentsBusinessCardRequest,
      } = this.props;

      switch (type) {
        case constant.COMMENT:
          getCommentsBusinessCardRequest(
            value._id,
            comments && comments.get ? comments.get.max : -1,
          );
          break;
        case constant.LIKE:
          if (like) {
            switch (like.nextType) {
              case constant.LIKE:
                likeBusinessCardRequest(value._id);
                break;
              case constant.UNLIKE:
                unLikeBusinessCardRequest(value._id);
                break;
              default:
                break;
            }
          } else if (value.likes.isSelf) {
            unLikeBusinessCardRequest(value._id);
          } else {
            likeBusinessCardRequest(value._id);
          }
          break;
        default:
          break;
      }
    };
  }

  Actions() {
    const {
      value,
      like,
      countLimit
    } = this.props;

    const likeDisabled = like ? like.result === ResultEnum.wait : false;

    return (
      <CardActions className="xcg-card-action" disableActionSpacing>
        <IconButton
          className="xcg-card-action-button"
          disabled={likeDisabled}
          onClick={this.handleClickPush(constant.LIKE)}
        >
          <Badge
            badgeContent={
              value.likes.count > countLimit
                ? `${countLimit}+`
                : value.likes.count
            }
          >
            <Favorite color={value.likes.isSelf ? 'secondary' : 'inherit'} />
          </Badge>
        </IconButton>
        <IconButton
          className="xcg-card-action-button"
          onClick={this.handleClickExpand(constant.VIEW)}
        >
          <Badge
            badgeContent={
              value.views.count > countLimit
                ? `${countLimit}+`
                : value.views.count
            }
          >
            <Visibility />
          </Badge>
        </IconButton>
        <IconButton
          className="xcg-card-action-button"
          onClick={this.handleClickExpand(constant.COMMENT)}
        >
          <Badge
            badgeContent={
              value.comments.count > countLimit
                ? `${countLimit}+`
                : value.comments.count
            }
          >
            <ModeComment />
          </Badge>
        </IconButton>
        <IconButton
          className="xcg-card-action-button"
          onClick={this.handleClickExpand(constant.DEAL)}
        >
          <Gavel />
        </IconButton>
        <IconButton
          className="xcg-card-action-button"
          onClick={this.handleClickExpand(constant.REPORT)}
        >
          <Flag />
        </IconButton>
      </CardActions>
    );
  }

  Comment() {
    const {
      value,
      comments,
      validate,
    } = this.props;
    const { expanded } = this.state;

    const pushDisabled = comments && comments.get ? comments.get.result === ResultEnum.wait : false;
    const addDisabled = comments && comments.post ? comments.post.result === ResultEnum.wait : false;

    return (
      <UIListComment
        ownerID={value.userID}
        value={value.comments.data}
        validate={validate.comment}
        open={expanded === constant.COMMENT}
        disabled={{
          push: pushDisabled,
          add: addDisabled,
          edit: false
        }}
        pushTitleNode={<FormattedMessage id="info.comment.push" />}
        handleClickPush={this.handleClickPush(constant.COMMENT)}
        handleClickClose={this.handleClickExpand(constant.COMMENT)}
        handleClickMenuItem={this.handleClickMenuItem(constant.COMMENT)}
        handleSubmit={this.handleSubmit(constant.COMMENT)}
      />
    );
  }

  Deal() {
    const { value, deal } = this.props;
    const { expanded } = this.state;

    const disabled = deal ? deal.result === ResultEnum.wait : false;
    const open = deal && deal.result === ResultEnum.success ? false : expanded === constant.DEAL;

    return (
      <InputDeal
        open={open}
        value={value.costs}
        disabled={disabled}
        handleClickClose={this.handleClickExpand(constant.DEAL)}
        handleSubmit={this.handleSubmit(constant.DEAL)}
      />
    );
  }

  Report() {
    const { report } = this.props;
    const { expanded } = this.state;

    const disabled = report ? report.result === ResultEnum.wait : false;
    const open = report && report.result === ResultEnum.success ? false : expanded === constant.REPORT;

    return (
      <InputReport
        open={open}
        disabled={disabled}
        Titile={<FormattedMessage id="businessCard.report.is" />}
        handleClickClose={this.handleClickExpand(constant.REPORT)}
        handleSubmit={this.handleSubmit(constant.REPORT)}
      />
    );
  }

  initElement() {
    const { className, value } = this.props;
    const { activeStep } = this.state;

    const defaultImage = {
      name: 'default',
      url: '/images/core/client/images/thumbnail.png'
    };

    let avatar = value.images.length ? value.images[0] : defaultImage;
    let image = defaultImage;

    value.images.forEach((img) => {
      if (img.name === regular.businessCard.image.names[0]) {
        avatar = img;
      }

      if (img.name === regular.businessCard.image.names[activeStep]) {
        image = img;
      }

      if (!isEmpty(avatar) && !isEmpty(image)) {
        return false;
      }
    });

    const { flag } = getAddress(constant.COUNTRY, value.address);
    const city = getAddress(constant.CITY, value.address).name;

    return (
      <div className={classNames('xcg-business-card', className)}>
        <Card className="xcg-card">
          <CardActionArea
            className="xcg-card-action-area"
            onClick={this.handleClickExpand(constant.VIEW)}
          >
            <CardHeader
              className="xcg-card-header"
              title={value.title}
              subheader={
                <div>
                  <div>
                    {flag} {city}
                    <span className="xcg-space">&bull;</span>
                    <FormattedRelative value={value.initDate} />
                  </div>
                  <UIStars value={value.star} />
                </div>
              }
              avatar={
                <Avatar
                  className="xcg-avatar"
                  alt={avatar.name}
                  src={avatar.url}
                />
              }
            />
            <CardContent className="xcg-card-content">
              <Typography className="xcg-descriptions">
                {value.descriptions}
              </Typography>
            </CardContent>
          </CardActionArea>
          <CardActionArea
            className="xcg-card-action-area xcg-card-media-card-action-area"
            onClick={this.handleClickImage}
          >
            <CardMedia
              className="xcg-card-media"
              image={
                value.blurFace
                  ? image.url.replace(regular.app.rootUrlCloudinary, `${regular.app.rootUrlCloudinary}/e_blur_faces: 2000/`)
                  : image.url
              }
              title={image.name}
            />
          </CardActionArea>
          {this.Actions()}
        </Card>
        {this.Comment()}
        {this.Deal()}
        {this.Report()}
      </div>
    );
  }

  render() {
    return this.initElement();
  }
};

BusinessCard.propTypes = {
  className: propTypes.string,
  countLimit: propTypes.number,
  validate: propTypes.object,
  like: propTypes.object,
  comments: propTypes.object,
  deal: propTypes.object,
  report: propTypes.object,
  value: propTypes.object.isRequired,
  likeBusinessCardRequest: propTypes.func.isRequired,
  unLikeBusinessCardRequest: propTypes.func.isRequired,
  getCommentsBusinessCardRequest: propTypes.func.isRequired,
  commentBusinessCardRequest: propTypes.func.isRequired,
  changeCommentBusinessCardRequest: propTypes.func.isRequired,
  deleteCommentBusinessCardRequest: propTypes.func.isRequired,
  dealBusinessCardRequest: propTypes.func.isRequired,
  reportBusinessCardRequest: propTypes.func.isRequired,
  redirect: propTypes.func.isRequired
};

BusinessCard.defaultProps = {
  className: '',
  countLimit: constant.ENUM.COUNT.LIMIT,
  validate: {
    comment: comment.body
  },
  like: null,
  comments: null,
  deal: null,
  report: null
};

function mapStateToProps(state, props) {
  const businessCardID = props.value._id;

  return createStructuredSelector({
    like: makeSelectLikesActionBusinessCard(businessCardID),
    comments: makeSelectCommentsActionBusinessCard(businessCardID),
    deal: makeSelectDealsActionBusinessCard(businessCardID),
    report: makeSelectReportsActionBusinessCard(businessCardID),
  });
}

function mapDispatchToProps(dispatch, props) {
  return {
    likeBusinessCardRequest: (businessCardID) => {
      return dispatch(likeBusinessCardRequest(businessCardID));
    },
    unLikeBusinessCardRequest: (businessCardID) => {
      return dispatch(unLikeBusinessCardRequest(businessCardID));
    },
    getCommentsBusinessCardRequest: (businessCardID, max) => {
      return dispatch(getCommentsBusinessCardRequest(businessCardID, max));
    },
    commentBusinessCardRequest: (businessCardID, comment) => {
      return dispatch(commentBusinessCardRequest(businessCardID, comment));
    },
    changeCommentBusinessCardRequest: (
      businessCardID,
      commentID,
      comment
    ) => {
      return dispatch(changeCommentBusinessCardRequest(
        businessCardID,
        commentID,
        comment
      ));
    },
    deleteCommentBusinessCardRequest: (businessCardID, commentID) => {
      return dispatch(deleteCommentBusinessCardRequest(businessCardID, commentID));
    },
    dealBusinessCardRequest: (businessCardID, deal) => {
      return dispatch(dealBusinessCardRequest(businessCardID, deal));
    },
    reportBusinessCardRequest: (businessCardID, report) => {
      return dispatch(reportBusinessCardRequest(businessCardID, report));
    },
    redirect: (location) => {
      return dispatch(push(location));
    }
  };
}

module.exports = connect(
  mapStateToProps,
  mapDispatchToProps,
)(BusinessCard);
