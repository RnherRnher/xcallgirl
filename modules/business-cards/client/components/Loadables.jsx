const LoadableBusinessCard = require('./loadables/LoadableBusinessCard');
const LoadableDealHistory = require('./loadables/LoadableDealHistory');
const LoadableDetailDealHistory = require('./loadables/LoadableDetailDealHistory');

module.exports = {
  LoadableBusinessCard,
  LoadableDealHistory,
  LoadableDetailDealHistory
};
