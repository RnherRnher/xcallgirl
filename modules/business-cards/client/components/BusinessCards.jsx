const BusinessCard = require('./business-cards/BusinessCard');
const BusinessCards = require('./business-cards/BusinessCards');
const CommonDetailBusinessCard = require('./business-cards/CommonDetailBusinessCard');
const CreateBusinessCard = require('./business-cards/CreateBusinessCard');
const DetailBusinessCard = require('./business-cards/DetailBusinessCard');
const EditBusinessCard = require('./business-cards/EditBusinessCard');
const ManagerBusinessCard = require('./business-cards/ManagerBusinessCard');

module.exports = {
  BusinessCard,
  BusinessCards,
  CommonDetailBusinessCard,
  CreateBusinessCard,
  DetailBusinessCard,
  EditBusinessCard,
  ManagerBusinessCard,
};
