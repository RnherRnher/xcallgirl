const React = require('react');
const propTypes = require('prop-types');
const classNames = require('classnames');
const BusinessCard = require('../business-cards/BusinessCard');
const { createStructuredSelector } = require('reselect');
const { connect } = require('react-redux');
const { LoadableBusinessCard } = require('../Loadables');
const { Loadable } = require('../../../../core/client/components/Loadables');
const { List, WindowScroller } = require('react-virtualized');
const { ContainerNotData } = require('../../../../core/client/components/Containers');
const { ResultEnum } = require('../../../../core/client/constants/core.client.constant');
const { makeSelectListBusinessCards } = require('../../reselects/business-card.client.reselect');

const BusinessCardsWindowScroll = class BusinessCardsWindowScroll extends React.Component {
  constructor(props) {
    super(props);

    this.listRef = React.createRef();

    this.onScroll = this.onScroll.bind(this);
    this.rowRenderer = this.rowRenderer.bind(this);
    this.countData = this.countData.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (this.listRef.current) {
      this.listRef.current.forceUpdateGrid();
    }
  }

  onScroll(event) {
    const {
      businessCards,
      handleLoadMore,
    } = this.props;

    if (
      businessCards.result !== ResultEnum.wait
      && businessCards.result !== ResultEnum.failed
      && handleLoadMore
      && (event.scrollTop + event.clientHeight)
      > (event.scrollHeight - event.clientHeight / 3)
    ) {
      handleLoadMore(!this.countData());
    }
  }

  rowRenderer(event) {
    const { businessCards } = this.props;

    const currentValue = businessCards.data
      ? businessCards.data.topNewUp
        .concat(
          businessCards.data.topActivity,
          businessCards.data.topRank
        )
      : [];

    return (
      <div key={event.key} style={event.style}>
        <BusinessCard value={currentValue[event.index]} />
      </div>
    );
  }

  countData() {
    const { businessCards } = this.props;

    return businessCards.data
      ? businessCards.data.topActivity.length
      + businessCards.data.topNewUp.length
      + businessCards.data.topRank.length
      : 0;
  }

  WindowScroller() {
    const { rowHeight } = this.props;

    return (
      <WindowScroller>
        {(propsWindowScroller) => {
          return <List
            ref={this.listRef}
            autoHeight
            rowCount={this.countData()}
            rowHeight={rowHeight}
            onScroll={this.onScroll}
            rowRenderer={this.rowRenderer}
            isScrolling={propsWindowScroller.isScrolling}
            scrollTop={propsWindowScroller.scrollTop}
            width={propsWindowScroller.width}
            height={propsWindowScroller.height}
          />;
        }}
      </WindowScroller>
    );
  }

  Loadables() {
    return (
      <div>
        <LoadableBusinessCard />
        <LoadableBusinessCard />
        <LoadableBusinessCard />
      </div>
    );
  }

  ContainerNotData() {
    return (
      <div>
        <ContainerNotData />
        <LoadableBusinessCard />
      </div>
    );
  }

  initElement() {
    const { className, businessCards } = this.props;

    const count = this.countData();

    return (
      <div className={classNames('xcg-business-cards-window-scroll', className)}>
        <Loadable
          result={businessCards.result}
          NoneNode={this.WindowScroller()}
          WaitNode={count ? this.WindowScroller() : this.Loadables()}
          SuccessNode={count ? this.WindowScroller() : this.ContainerNotData()}
          FailedNode={count ? this.WindowScroller() : this.ContainerNotData()}
        />
      </div>
    );
  }

  render() {
    return this.initElement();
  }
};

BusinessCardsWindowScroll.propTypes = {
  className: propTypes.string,
  rowHeight: propTypes.number,
  handleLoadMore: propTypes.func,
  businessCards: propTypes.object.isRequired,
};

BusinessCardsWindowScroll.defaultProps = {
  className: '',
  rowHeight: 550,
  handleLoadMore: null
};

function mapStateToProps(state, props) {
  return createStructuredSelector({
    businessCards: makeSelectListBusinessCards(),
  });
}

function mapDispatchToProps(dispatch, props) {
  return {};
}

module.exports = connect(
  mapStateToProps,
  mapDispatchToProps,
)(BusinessCardsWindowScroll);
