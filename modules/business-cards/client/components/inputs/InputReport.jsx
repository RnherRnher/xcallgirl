const React = require('react');
const propTypes = require('prop-types');
const classNames = require('classnames');
const joi = require('joi');
const { FormattedMessage } = require('react-intl');
const { Close } = require('@material-ui/icons');
const {
  TextField,
  Button,
  Typography,
  Slide,
  Dialog,
  AppBar,
  Toolbar,
  IconButton
} = require('@material-ui/core');
const { report } = require('../../../../../commons/modules/business-cards/validates/business-cards.validate');

const InputReport = class InputReport extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      reason: ''
    };

    this.handleClick = this.handleClick.bind(this);
    this.handleCheck = this.handleCheck.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleClickClose = this.handleClickClose.bind(this);
  }

  handleClickClose(event) {
    const { handleClickClose } = this.props;

    this.setState({ reason: '' });

    handleClickClose();
  }

  handleCheck(body) {
    const { validate } = this.props;

    return joi.validate(body, validate);
  }

  handleClick(event) {
    const { handleSubmit } = this.props;
    const { reason } = this.state;

    this.handleCheck({ report: { reason } })
      .then((body) => {
        handleSubmit(body);
      });
  }

  handleChange(event) {
    const { value } = event.target;

    this.setState({ reason: value });
  }

  Form() {
    const { disabled } = this.props;
    const { reason } = this.state;

    return (
      <div className="xcg-group-input">
        <TextField
          label={<FormattedMessage id="info.reason.is" />}
          type="text"
          multiline
          rows="25"
          rowsMax="25"
          fullWidth
          value={reason}
          disabled={disabled}
          onChange={this.handleChange}
        />
        <AppBar className="xcg-botton-app-bar" position="fixed">
          <Button
            className="xcg-btn-app-bar"
            fullWidth
            disabled={disabled}
            onClick={this.handleClick}
          >
            <Typography
              align="center"
              variant="button"
            >
              <FormattedMessage id="actions.send" />
            </Typography>
          </Button>
        </AppBar>
      </div>
    );
  }

  Transition(props) {
    return <Slide direction="up" {...props} />;
  }

  initElement() {
    const {
      className,
      disabled,
      open,
      Titile
    } = this.props;

    return (
      <Dialog
        className={classNames('xcg-input-report-business-card', className)}
        fullScreen
        onClose={this.handleClickClose}
        open={open}
        TransitionComponent={this.Transition}
      >
        <AppBar className="xcg-top-app-bar">
          <Toolbar>
            {Titile}
            <IconButton
              className="xcg-close-button"
              onClick={this.handleClickClose}
              disabled={disabled}
            >
              <Close />
            </IconButton>
          </Toolbar>
        </AppBar>
        {this.Form()}
      </Dialog>
    );
  }

  render() {
    return this.initElement();
  }
};

InputReport.propTypes = {
  className: propTypes.string,
  validate: propTypes.any,
  disabled: propTypes.bool,
  Titile: propTypes.node,
  open: propTypes.bool.isRequired,
  handleClickClose: propTypes.func.isRequired,
  handleSubmit: propTypes.func.isRequired
};

InputReport.defaultProps = {
  className: 'xcg-dialog',
  validate: report.body,
  disabled: false,
  Titile: null
};

module.exports = InputReport;
