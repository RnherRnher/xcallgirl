const React = require('react');
const propTypes = require('prop-types');
const classNames = require('classnames');
const AlertSnackbar = require('../../../../core/client/components/alerts/AlertSnackbar');
const regular = require('../../../../../commons/utils/regular-expression');
const constant = require('../../../../../commons/utils/constant');
const { FormattedMessage } = require('react-intl');
const { TextField, Typography } = require('@material-ui/core');

const InputMeasurements3 = class InputMeasurements3 extends React.Component {
  constructor(props) {
    super(props);

    const { value } = this.props;
    this.state = {
      breast: value && value.breast ? value.breast : 0,
      waist: value && value.waist ? value.waist : 0,
      hips: value && value.hips ? value.hips : 0
    };

    this.handleChange = this.handleChange.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    const { isUpdatePropsToState } = this.props;

    if (isUpdatePropsToState) {
      const { value } = nextProps;

      const breast = value && value.breast ? value.breast : 0;
      const waist = value && value.waist ? value.waist : 0;
      const hips = value && value.hips ? value.hips : 0;

      this.setState({
        breast,
        waist,
        hips
      });
    }
  }

  handleChange(type) {
    return (event) => {
      const { value } = event.target;
      const { validate } = this.props;

      this.setState({
        [type]: validate.test(value)
          ? parseInt(value, 10)
          : value ? parseInt(value.slice(0, -1), 10) : '0'
      });
    };
  }

  initElement() {
    const {
      className,
      disabled,
      image,
      gender
    } = this.props;
    const {
      breast,
      waist,
      hips,
    } = this.state;

    return (
      <div className={classNames('xcg-input-measurements3-business-card', className)}>
        <div className="xcg-layout">
          <div className="xcg-control">
            <div className="xcg-title-label">
              <Typography
                align="center"
                variant="title"
              >
                <FormattedMessage id="businessCard.measurements3.is" />
              </Typography>
            </div>
            <div className="xcg-group-input">
              <div className="xcg-breast-input">
                <TextField
                  label={<FormattedMessage id="businessCard.measurements3.breast.is" />}
                  type="text"
                  fullWidth
                  value={breast}
                  disabled={disabled}
                  onChange={this.handleChange(constant.BREAST)}
                />
              </div>
              <div className="xcg-hips-input">
                <TextField
                  label={<FormattedMessage id="businessCard.measurements3.hips.is" />}
                  type="text"
                  fullWidth
                  value={hips}
                  disabled={disabled}
                  onChange={this.handleChange(constant.HIPS)}
                />
              </div>
              <div className="xcg-waist-input">
                <TextField
                  label={<FormattedMessage id="businessCard.measurements3.waist.is" />}
                  type="text"
                  fullWidth
                  value={waist}
                  disabled={disabled}
                  onChange={this.handleChange(constant.WAIST)}
                />
              </div>
            </div>
          </div>
          <div>
            <img
              src={image[gender]}
              alt="businessCard_layout"
            />
          </div>
        </div>
        <AlertSnackbar message={<FormattedMessage id="businessCard.measurements3.note" />} />
      </div>
    );
  }

  render() {
    return this.initElement();
  }
};

InputMeasurements3.propTypes = {
  className: propTypes.string,
  value: propTypes.shape({
    breast: propTypes.number,
    waist: propTypes.number,
    hips: propTypes.number,
  }),
  disabled: propTypes.bool,
  validate: propTypes.any,
  isUpdatePropsToState: propTypes.bool,
  image: propTypes.shape({
    male: propTypes.string.isRequired,
    female: propTypes.string.isRequired,
  }),
  gender: propTypes.string.isRequired
};

InputMeasurements3.defaultProps = {
  className: '',
  value: {
    breast: 0,
    waist: 0,
    hips: 0
  },
  disabled: false,
  image: {
    male: regular.businessCard.image.url.male_layout,
    female: regular.businessCard.image.url.female_layout
  },
  validate: regular.commons.number,
  isUpdatePropsToState: false
};

module.exports = InputMeasurements3;
