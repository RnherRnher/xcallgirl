const React = require('react');
const propTypes = require('prop-types');
const classNames = require('classnames');
const joi = require('joi');
const AlertSnackbar = require('../../../../core/client/components/alerts/AlertSnackbar');
const regular = require('../../../../../commons/utils/regular-expression');
const constant = require('../../../../../commons/utils/constant');
const validate = require('../../../../../commons/modules/business-cards/validates/business-cards.validate');
const { assignIn } = require('lodash');
const { FormattedMessage, FormattedNumber } = require('react-intl');
const { Close } = require('@material-ui/icons');
const {
  TextField,
  MenuItem,
  Button,
  Typography,
  ListItemText,
  Menu,
  Slide,
  Dialog,
  AppBar,
  Toolbar,
  IconButton
} = require('@material-ui/core');
const { SelectTime } = require('../../../../core/client/components/Selects');
const { UITimer } = require('../../../../core/client/components/UIs');

const InputDeal = class InputDeal extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      room: {
        moneys: 0,
        value: '',
      },
      service: {
        moneys: 0,
        value: '',
      },
      note: '',
      selected: '',
      anchorEl: null,
      time: {
        hours: 0,
        minutes: 0,
      }
    };

    this.handleClick = this.handleClick.bind(this);
    this.handleCheck = this.handleCheck.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleClickListItem = this.handleClickListItem.bind(this);
    this.handleClickMenuItem = this.handleClickMenuItem.bind(this);
    this.handleCloseMenu = this.handleClose.bind(this);
    this.handleClickClose = this.handleClickClose.bind(this);
  }

  getData(type, types) {
    const { value } = this.props;

    return value[types].map((cost, index) => {
      return (
        <MenuItem key={cost.value} onClick={this.handleClickMenuItem(type, cost)}>
          <ListItemText primary={<FormattedMessage id={`businessCard.costs.${types}.${cost.value}`} />} />
          <ListItemText primary={<FormattedNumber value={cost.moneys} />} />
          <ListItemText primary={<FormattedMessage id={`info.moneys.currencyUnit.${value.currencyUnit}`} />} />
        </MenuItem>
      );
    });
  }

  getItem(types, cost) {
    const { value } = this.props;

    return cost.value
      ? <MenuItem>
        <ListItemText primary={<FormattedMessage id={`businessCard.costs.${types}.${cost.value}`} />} />
        <ListItemText primary={<FormattedNumber value={cost.moneys} />} />
        <ListItemText primary={<FormattedMessage id={`info.moneys.currencyUnit.${value.currencyUnit}`} />} />
      </MenuItem>
      : <MenuItem>
        <ListItemText primary={<FormattedMessage id="actions.select" />} />
      </MenuItem>;
  }

  getInput(primary, type, types, cost, disabled) {
    const { selected, anchorEl } = this.state;

    return (
      <div>
        <div className="xcg-primary-btn">
          <Button
            fullWidth
            disabled={disabled}
            onClick={this.handleClickListItem(type)}
          >
            <Typography
              align="center"
              variant="button"
            >
              {primary}
              {this.getItem(types, cost)}
            </Typography>
          </Button>
        </div>
        <Menu
          anchorEl={anchorEl}
          open={selected === type}
          onClose={this.handleCloseMenu}
        >
          {this.getData(type, types)}
        </Menu>
      </div>
    );
  }

  handleClickClose(event) {
    const { handleClickClose } = this.props;

    this.setState({
      room: {
        moneys: 0,
        value: '',
      },
      service: {
        moneys: 0,
        value: '',
      },
      note: '',
      selected: '',
      anchorEl: null
    });

    handleClickClose();
  }

  handleChange(type) {
    return (event) => {
      switch (type) {
        case constant.TIME:
          this.setState((state, props) => {
            return {
              time: assignIn(
                state.time,
                event
              )
            };
          });
          break;
        case constant.NOTE: {
          const { value } = event.target;

          this.setState({ note: value });
        }
          break;
        default:
          break;
      }
    };
  }

  handleCheck(body) {
    const { validate } = this.props;

    return joi.validate(body, validate);
  }

  handleClick(event) {
    const { value, handleSubmit } = this.props;
    const {
      room,
      service,
      note,
      time
    } = this.state;

    const body = {
      costs: {
        room,
        service,
        currencyUnit: value.currencyUnit,
      },
      timer: time.hours * constant.ENUM.TIME.ONE_HOURS + time.minutes * constant.ENUM.TIME.ONE_MINUTES,
      note,
    };

    body.costs.room = room.value && room.moneys ? room : undefined;
    body.note = note || undefined;

    this.handleCheck(body)
      .then((body) => {
        handleSubmit(body);
      });
  }

  handleClickListItem(type) {
    return (event) => {
      this.setState({
        selected: type,
        anchorEl: event.currentTarget
      });
    };
  }

  handleClickMenuItem(type, cost) {
    return (event) => {
      this.setState({
        [type]: {
          value: cost.value,
          moneys: cost.moneys
        },
        selected: '',
        anchorEl: null
      });
    };
  }

  handleClose(event) {
    this.setState({
      selected: '',
      anchorEl: null
    });
  }

  Form() {
    const { disabled } = this.props;
    const {
      note,
      room,
      service,
      time
    } = this.state;

    return (
      <div className="xcg-group-input">
        <SelectTime
          label={<FormattedMessage id="info.time.timer" />}
          disabled={disabled}
          min={{
            hours: 0,
            minutes: regular.businessCardDealHistory.expires.timer / constant.ENUM.TIME.ONE_MINUTES,
          }}
          handleChangeParent={this.handleChange(constant.TIME)}
        />
        <AlertSnackbar
          variant="info"
          message={
            <div className="xcg-view-time">
              <FormattedMessage id="businessCardDealHistory.create.note" /> <UITimer value={time.hours * constant.ENUM.TIME.ONE_HOURS + time.minutes * constant.ENUM.TIME.ONE_MINUTES} />
            </div>
          }
        />
        {
          this.getInput(
            <FormattedMessage id="businessCard.costs.rooms.is" />,
            constant.ROOM,
            constant.ROOMS,
            room,
            disabled
          )
        }
        {
          this.getInput(
            <FormattedMessage id="businessCard.costs.services.is" />,
            constant.SERVICER,
            constant.SERVICERS,
            service,
            disabled
          )
        }
        <AlertSnackbar
          variant="warning"
          message={<FormattedMessage id="businessCard.costs.services.note" />}
        />
        <TextField
          label={<FormattedMessage id="businessCardDealHistory.note.is" />}
          type="text"
          multiline
          rows="5"
          rowsMax="5"
          fullWidth
          value={note}
          disabled={disabled}
          onChange={this.handleChange(constant.NOTE)}
        />
        <AppBar className="xcg-botton-app-bar" position="fixed">
          <Button
            className="xcg-btn-app-bar"
            fullWidth
            disabled={disabled}
            onClick={this.handleClick}
          >
            <Typography
              align="center"
              variant="button"
            >
              <FormattedMessage id="actions.schedule" />
            </Typography>
          </Button>
        </AppBar>
      </div>
    );
  }

  Transition(props) {
    return <Slide direction="up" {...props} />;
  }

  initElement() {
    const {
      className,
      disabled,
      open
    } = this.props;

    return (
      <Dialog
        className={classNames('xcg-input-deal-business-card', className)}
        fullScreen
        onClose={this.handleClickClose}
        open={open}
        TransitionComponent={this.Transition}
      >
        <AppBar className="xcg-top-app-bar">
          <Toolbar>
            <FormattedMessage id="businessCardDealHistory.create.is" />
            <IconButton
              className="xcg-close-button"
              onClick={this.handleClickClose}
              disabled={disabled}
            >
              <Close />
            </IconButton>
          </Toolbar>
        </AppBar>
        {this.Form()}
      </Dialog>
    );
  }

  render() {
    return this.initElement();
  }
};

InputDeal.propTypes = {
  className: propTypes.string,
  validate: propTypes.any,
  disabled: propTypes.bool,
  value: propTypes.object.isRequired,
  open: propTypes.bool.isRequired,
  handleClickClose: propTypes.func.isRequired,
  handleSubmit: propTypes.func.isRequired
};

InputDeal.defaultProps = {
  className: 'xcg-dialog',
  validate: validate.deal.body,
  disabled: false
};

module.exports = InputDeal;
