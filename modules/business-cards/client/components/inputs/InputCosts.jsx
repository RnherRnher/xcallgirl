const React = require('react');
const propTypes = require('prop-types');
const classNames = require('classnames');
const AlertSnackbar = require('../../../../core/client/components/alerts/AlertSnackbar');
const regular = require('../../../../../commons/utils/regular-expression');
const constant = require('../../../../../commons/utils/constant');
const { FormattedMessage, FormattedNumber } = require('react-intl');
const { Add } = require('@material-ui/icons');
const {
  without,
  filter,
  assignIn,
  concat
} = require('lodash');
const {
  TextField,
  Typography,
  MenuItem,
  Button
} = require('@material-ui/core');
const { UICosts } = require('../UIs');

const InputCosts = class InputCosts extends React.Component {
  constructor(props) {
    super(props);

    const { costs, value } = this.props;
    this.state = {
      costs,
      room: {
        moneys: '0',
        value: '',
      },
      service: {
        moneys: '0',
        value: '',
      },
      rooms: value && value.rooms ? value.rooms : [],
      services: value && value.services ? value.services : [],
      currencyUnit: value && value.currencyUnit
        ? value.currencyUnit : costs.currencyUnit[0]
    };


    this.handleChange = this.handleChange.bind(this);
    this.handleClickAdd = this.handleClickAdd.bind(this);
    this.handleClickRemove = this.handleClickRemove.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    const { isUpdatePropsToState } = this.props;

    if (isUpdatePropsToState) {
      const { costs, value } = nextProps;

      this.setState({
        costs,
        room: {
          moneys: '0',
          value: '',
        },
        service: {
          moneys: '0',
          value: '',
        },
        rooms: value && value.rooms ? value.rooms : [],
        services: value && value.services ? value.services : [],
        currencyUnit: value && value.currencyUnit
          ? value.currencyUnit : costs.currencyUnit[0]
      });
    }
  }

  getData(type) {
    const { costs } = this.state;

    return costs[type].map((value) => {
      return (
        <MenuItem key={value} value={value}>
          <FormattedMessage
            id={type === constant.CURRENCY_UNIT
              ? `info.moneys.currencyUnit.${value}`
              : `businessCard.costs.${type}.${value}`}
          />
        </MenuItem>
      );
    });
  }

  handleChange(cost, type) {
    return (event) => {
      const { value } = event.target;

      this.setState((state, props) => {
        let valueCurrent = value;

        switch (type) {
          case constant.CURRENCY_UNIT:
            return { [cost]: valueCurrent };
          case constant.MONEYS:
            valueCurrent = props.number.test(value)
              ? value
              : value ? value.slice(0, -1) : '0';
            break;
          default:
            break;
        }

        return {
          [cost]: assignIn(
            state[cost],
            { [type]: valueCurrent }
          )
        };
      });
    };
  }

  handleClickAdd(type) {
    return (event) => {
      this.setState((state, props) => {
        const cost = state[type.slice(0, -1)];

        return {
          costs: assignIn(
            state.costs,
            { [type]: without(state.costs[type], cost.value) }
          ),
          [type]: concat(state[type], cost),
          [type.slice(0, -1)]: {
            moneys: '0',
            value: '',
          }
        };
      });
    };
  }

  handleClickRemove(type, value) {
    return (event) => {
      this.setState((state, props) => {
        return {
          costs: assignIn(
            state.costs,
            { [type]: concat(state.costs[type], value) }
          ),
          [type]: filter(state[type], (o) => {
            return o.value !== value;
          })
        };
      });
    };
  }

  InputsService() {
    const { disabled } = this.props;
    const { service, costs } = this.state;

    return (
      <div>
        <div className="xcg-group-input">
          <Typography
            align="center"
            variant="title"
          >
            <FormattedMessage id="businessCard.costs.services.is" />
          </Typography>
          <div className="xcg-group-input-services">
            <TextField
              className="xcg-input-cost"
              label={<FormattedMessage id="businessCard.costs.value" />}
              select
              disabled={disabled}
              fullWidth
              value={service.value}
              onChange={this.handleChange(constant.SERVICER, constant.VALUE)}
            >
              {this.getData(constant.SERVICERS)}
            </TextField>
            <TextField
              className="xcg-input-cost"
              label={<FormattedMessage id="businessCard.costs.moneys" />}
              type="text"
              disabled={disabled}
              fullWidth
              value={service.moneys}
              onChange={this.handleChange(constant.SERVICER, constant.MONEYS)}
              helperText={service.moneys ? <FormattedNumber value={service.moneys} /> : null}
            />
          </div>
          <div className="xcg-btn">
            <Button
              onClick={this.handleClickAdd(constant.SERVICERS)}
              disabled={!service.value || !costs.services.length}
              color="secondary"
            >
              <Add />
              <FormattedMessage id="actions.add" />
            </Button>
          </div>
        </div>
        <AlertSnackbar
          variant="warning"
          message={<FormattedMessage id="businessCard.costs.services.note" />}
        />
      </div>
    );
  }

  InputsRoom() {
    const { disabled } = this.props;
    const { room, costs } = this.state;

    return (
      <div className="xcg-group-input">
        <div>
          <Typography
            align="center"
            variant="title"
          >
            <FormattedMessage id="businessCard.costs.rooms.is" />
          </Typography>
          <div className="xcg-group-input-rooms">
            <TextField
              className="xcg-input-cost"
              label={<FormattedMessage id="businessCard.costs.value" />}
              select
              disabled={disabled}
              fullWidth
              value={room.value}
              onChange={this.handleChange(constant.ROOM, constant.VALUE)}
            >
              {this.getData(constant.ROOMS)}
            </TextField>
            <TextField
              className="xcg-input-cost"
              label={<FormattedMessage id="businessCard.costs.moneys" />}
              type="text"
              disabled={disabled}
              fullWidth
              value={room.moneys}
              onChange={this.handleChange(constant.ROOM, constant.MONEYS)}
              helperText={room.moneys ? <FormattedNumber value={room.moneys} /> : null}
            />
          </div>
          <div className="xcg-btn">
            <Button
              onClick={this.handleClickAdd(constant.ROOMS)}
              disabled={!room.value || !costs.rooms.length}
              color="secondary"
            >
              <Add />
              <FormattedMessage id="actions.add" />
            </Button>
          </div>
        </div>
      </div>
    );
  }

  InputsCurrencyUnit() {
    const { disabled } = this.props;
    const { currencyUnit } = this.state;

    return (
      <div className="xcg-group-input">
        <div className="xcg-input-cost">
          <TextField
            label={<FormattedMessage id="info.moneys.currencyUnit.is" />}
            select
            fullWidth
            value={currencyUnit}
            disabled={disabled}
            onChange={this.handleChange(constant.CURRENCY_UNIT, constant.CURRENCY_UNIT)}
          >
            {this.getData(constant.CURRENCY_UNIT)}
          </TextField>
        </div>
      </div>
    );
  }

  initElement() {
    const { className } = this.props;
    const {
      currencyUnit,
      rooms,
      services
    } = this.state;

    return (
      <div className={classNames('xcg-input-costs-business-card', className)}>
        <UICosts
          rooms={rooms}
          services={services}
          currencyUnit={currencyUnit}
          handleClickRemove={this.handleClickRemove}
        />
        {this.InputsCurrencyUnit()}
        {this.InputsRoom()}
        {this.InputsService()}
      </div>
    );
  }

  render() {
    return this.initElement();
  }
};

InputCosts.propTypes = {
  className: propTypes.string,
  value: propTypes.shape({
    rooms: propTypes.array,
    services: propTypes.array,
    currencyUnit: propTypes.string
  }),
  disabled: propTypes.bool,
  costs: propTypes.object,
  number: propTypes.any,
  isUpdatePropsToState: propTypes.bool,
};

InputCosts.defaultProps = {
  className: '',
  value: {
    rooms: [],
    services: [],
    currencyUnit: regular.commons.moneys.currencyUnit[0]
  },
  disabled: false,
  costs: assignIn(
    regular.businessCard.costs,
    { currencyUnit: regular.commons.moneys.currencyUnit }
  ),
  number: regular.commons.number,
  isUpdatePropsToState: false
};

module.exports = InputCosts;
