const React = require('react');
const propTypes = require('prop-types');
const classNames = require('classnames');
const AlertSnackbar = require('../../../../core/client/components/alerts/AlertSnackbar');
const regular = require('../../../../../commons/utils/regular-expression');
const { FormattedMessage } = require('react-intl');
const {
  KeyboardArrowLeft,
  KeyboardArrowRight,
  Delete
} = require('@material-ui/icons');
const {
  ListItem,
  Typography,
  MobileStepper,
  Button,
  FormControlLabel,
  Checkbox,
  ListItemText,
  ListItemSecondaryAction,
  IconButton
} = require('@material-ui/core');

const InputImages = class InputImages extends React.Component {
  constructor(props) {
    super(props);

    const { image, value } = this.props;
    this.state = {
      activeStep: 0,
      images: value && value.images.length ? value.images : image.names.map((name) => {
        return {
          url: image.url[name],
          name,
          isDefault: true,
          blob: null
        };
      }),
      blurFace: value && value.blurFace ? value.blurFace : false
    };

    this.handleChangeFile = this.handleChangeFile.bind(this);
    this.handleClickRemoveFile = this.handleClickRemoveFile.bind(this);
    this.handleChangeBlurFace = this.handleChangeBlurFace.bind(this);
    this.handleClickStep = this.handleClickStep.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    const { isUpdatePropsToState, image } = this.props;

    if (isUpdatePropsToState) {
      const { value } = nextProps;

      this.setState({
        images: value && value.images.length ? value.images : image.names.map((name) => {
          return {
            url: image.url[name],
            name,
            isDefault: true,
            blob: null
          };
        }),
        blurFace: value && value.blurFace ? value.blurFace : false
      });
    }
  }

  handleClickStep(num) {
    return (event) => {
      this.setState((state, props) => {
        return { activeStep: state.activeStep + num };
      });
    };
  }

  handleChangeBlurFace(event) {
    this.setState((state, props) => {
      return { blurFace: !state.blurFace };
    });
  }

  handleChangeFile(index) {
    return (event) => {
      const { files } = event.target;

      this.setState({ blob: files[0] });

      const reader = new FileReader();
      reader.readAsDataURL(files[0]);
      reader.addEventListener('load', () => {
        this.setState((state, props) => {
          const currentImages = state.images;

          currentImages[index] = {
            url: reader.result,
            name: props.image.names[index],
            isDefault: false,
            blob: state.blob
          };

          if (props.handleChangeFile) {
            props.handleChangeFile(currentImages[index]);
          }

          return { images: currentImages };
        });
      });
    };
  }

  handleClickRemoveFile(index) {
    return (event) => {
      this.setState((state, props) => {
        const name = props.image.names[index];
        const currentImages = state.images;

        if (props.handleClickRemoveFile) {
          props.handleClickRemoveFile(currentImages[index].publicID);
        }

        currentImages[index] = {
          url: props.image.url[name],
          name,
          isDefault: true,
          blob: null
        };

        return { images: currentImages };
      });
    };
  }

  DefaultImage(index, image) {
    const { disabled } = this.props;

    return (
      <div className="xcg-image xcg-default-image-view">
        <input
          type="file"
          accept="image/*"
          className="button-file"
          id="button-file"
          onChange={this.handleChangeFile(index)}
        />
        <label htmlFor="button-file">
          <Button
            className="xcg-upload-button"
            variant="contained"
            component="span"
            disabled={disabled}
          >
            <img
              className="xcg-image"
              src={image.url}
              alt={image.name}
            />
            <div className="xcg-upload-label">
              <Typography
                align="center"
                variant="button"
                color="secondary"
              >
                <FormattedMessage id="businessCard.image.upload.is" />
              </Typography>
            </div>
          </Button>
        </label>
      </div>
    );
  }

  ViewImage() {
    const { activeStep, images } = this.state;
    const { disabled } = this.props;

    const image = images[activeStep];

    return (
      <div>
        <ListItem>
          <ListItemText
            primary={
              <Typography
                align="center"
                variant="title"
              >
                <FormattedMessage id={`businessCard.image.names.${image.name}`} />
              </Typography>
            }
          />
          {
            image.isDefault
              ? null
              : <ListItemSecondaryAction>
                <IconButton
                  disabled={disabled}
                  onClick={this.handleClickRemoveFile(activeStep)}
                >
                  <Delete />
                </IconButton>
              </ListItemSecondaryAction>
          }
        </ListItem>
        {
          image.isDefault
            ? this.DefaultImage(activeStep, image)
            : <div className="xcg-image xcg-custom-image-view">
              <img
                className="xcg-image"
                src={image.url}
                alt={image.name}
              />
            </div>
        }
      </div>
    );
  }

  initElement() {
    const { className, maxSteps, disabled } = this.props;
    const { activeStep, images, blurFace } = this.state;

    return (
      <div className={classNames('xcg-input-images-business-card', className)}>
        <div className="xcg-image-control">
          {this.ViewImage()}
          <MobileStepper
            steps={images.length}
            position="static"
            activeStep={activeStep}
            backButton={
              <Button
                size="small"
                onClick={this.handleClickStep(-1)}
                disabled={(activeStep === 0) || disabled}
              >
                <KeyboardArrowLeft />
                <FormattedMessage id="actions.back" />
              </Button>
            }
            nextButton={
              <Button
                size="small"
                onClick={this.handleClickStep(1)}
                disabled={(activeStep === maxSteps - 1) || disabled}
              >
                <FormattedMessage id="actions.next" />
                <KeyboardArrowRight />
              </Button>
            }
          />
        </div>
        <div className="xcg-checkbox">
          <FormControlLabel
            disabled={disabled}
            control={
              <Checkbox
                checked={blurFace}
                onChange={this.handleChangeBlurFace}
              />
            }
            label={<FormattedMessage id="businessCard.image.blurFace.is" />}
          />
        </div>
        <AlertSnackbar message={<FormattedMessage id="businessCard.image.upload.note" />} />
      </div>
    );
  }

  render() {
    return this.initElement();
  }
};

InputImages.propTypes = {
  className: propTypes.string,
  value: propTypes.shape({
    images: propTypes.array,
    blurFace: propTypes.bool
  }),
  disabled: propTypes.bool,
  maxSteps: propTypes.number,
  image: propTypes.object,
  isUpdatePropsToState: propTypes.bool,
  handleChangeFile: propTypes.func,
  handleClickRemoveFile: propTypes.func,

};

InputImages.defaultProps = {
  className: '',
  value: {
    images: [],
    blurFace: false
  },
  disabled: false,
  maxSteps: regular.businessCard.image.max,
  image: {
    names: regular.businessCard.image.names,
    url: regular.businessCard.image.url
  },
  isUpdatePropsToState: false,
  handleChangeFile: null,
  handleClickRemoveFile: null,
};

module.exports = InputImages;
