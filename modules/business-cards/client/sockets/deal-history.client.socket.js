const { getBusinessCardsDealHistoryIDRequest, getBusinessCardDealHistoryRequest } = require('../actions/deal-history.client.action');
const {
  SOCKET_BUSINESS_CARD_DEAL_HISTORY_UPDATE,
  SOCKET_DETAIL_BUSINESS_CARD_DEAL_HISTORY_UPDATE,
  SOCKET_BUSINESS_CARD_DEAL_HISTORY_PUSH
} = require('../../../../commons/modules/business-cards/datas/socket.data');

const init = (dispatch, socket) => {
  socket.on(SOCKET_DETAIL_BUSINESS_CARD_DEAL_HISTORY_UPDATE, (body) => {
    dispatch(getBusinessCardsDealHistoryIDRequest(body));
  });

  socket.on(SOCKET_BUSINESS_CARD_DEAL_HISTORY_UPDATE, (body) => {
    dispatch(getBusinessCardDealHistoryRequest(body, true));
  });

  socket.on(SOCKET_BUSINESS_CARD_DEAL_HISTORY_PUSH, (body) => {
    dispatch(getBusinessCardDealHistoryRequest(body));
  });
};
module.exports = init;
