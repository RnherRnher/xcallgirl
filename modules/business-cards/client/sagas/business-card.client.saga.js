const nprogress = require('nprogress');
const api = require('../services/business-cards.client.service');
const action = require('../actions/business-card.client.action');
const constant = require('../constants/business-card.client.constant');
const url = require('../../../core/client/utils/url');
const constantCommon = require('../../../../commons/utils/constant');
const {
  takeLatest,
  put,
  call,
  takeEvery
} = require('redux-saga/effects');
const { push } = require('connected-react-router/immutable');
const { pushAlertSnackbar } = require('../../../core/client/actions/core.client.action');
const { csrfRequest } = require('../../../core/client/services/core.client.service');
const { getProfileUserRequest } = require('../../../users/client/actions/user.client.action');
const { emitSockets } = require('../../../core/client/lib/socket-io');
const { initApp } = require('../../../core/client/actions/app.client.action');

const watchGetProfileBusinessCard = function* () {
  yield takeLatest(constant.GET_PROFILE_BUSINESS_CARD_REQUEST, function* ({ isAlert }) {
    try {
      if (isAlert) {
        nprogress.start();
      }

      const response = yield call(api.getProfileBusinessCard);

      yield put(action.getProfileBusinessCardSuccess(response.body));
    } catch (error) {
      if (error && error.body) {
        yield put(action.getProfileBusinessCardFailed(error.body));
        if (isAlert) {
          yield put(pushAlertSnackbar({
            variant: constantCommon.ERROR,
            code: error.body.code
          }));
        }
      } else {
        yield put(initApp());
      }
    } finally {
      if (isAlert) {
        nprogress.done();
      }
    }
  });
};
module.exports.watchGetProfileBusinessCard = watchGetProfileBusinessCard;

const watchGetBusinessCardID = function* () {
  yield takeLatest(constant.GET_BUSINESS_CARD_ID_REQUEST, function* ({ businessCardID }) {
    try {
      nprogress.start();

      const response = yield call(
        api.getBusinessCardID,
        { businessCardID }
      );

      yield put(action.getBusinessCardIDSuccess(response.body));
    } catch (error) {
      if (error && error.body) {
        yield put(action.getBusinessCardIDFailed(error.body));
      } else {
        yield put(initApp());
      }
    } finally {
      nprogress.done();
    }
  });
};
module.exports.watchGetBusinessCardID = watchGetBusinessCardID;

const watchCreateBusinessCard = function* () {
  yield takeLatest(constant.CREATE_BUSINESS_CARD_REQUEST, function* ({ businessCard, images }) {
    try {
      nprogress.start();

      const responseCSRF = yield call(csrfRequest);
      const response = yield call(
        api.createBusinessCard,
        {
          csrfToken: responseCSRF.body.csrfToken,
          businessCard
        }
      );

      yield put(action.createBusinessCardSuccess(response.body));
      yield put(pushAlertSnackbar({
        variant: constantCommon.SUCCESS,
        code: response.body.code
      }));

      yield put(getProfileUserRequest(false, false));

      for (let i = 0; i < images.length; i++) {
        const image = images[i];

        if (!image.isDefault) {
          yield put(action.uploadImageBusinessCardRequest(image));
        }
      }
    } catch (error) {
      if (error && error.body) {
        yield put(action.createBusinessCardFailed(error.body));
        yield put(pushAlertSnackbar({
          variant: constantCommon.ERROR,
          code: error.body.code
        }));
      } else {
        yield put(initApp());
      }
    } finally {
      nprogress.done();
    }
  });
};
module.exports.watchCreateBusinessCard = watchCreateBusinessCard;

const watchDeleteBusinessCard = function* () {
  yield takeLatest(constant.DELETE_BUSINESS_CARD_REQUEST, function* ({ deletee }) {
    try {
      nprogress.start();

      const responseCSRF = yield call(csrfRequest);
      const response = yield call(
        api.deleteBusinessCard,
        {
          csrfToken: responseCSRF.body.csrfToken,
          deletee
        }
      );

      yield put(action.deleteBusinessCardSuccess(response.body));
      yield put(pushAlertSnackbar({
        variant: constantCommon.SUCCESS,
        code: response.body.code
      }));

      yield put(action.getProfileBusinessCardRequest(false));
      yield put(getProfileUserRequest(false, false));
    } catch (error) {
      if (error && error.body) {
        yield put(action.deleteBusinessCardFailed(error.body));
        yield put(pushAlertSnackbar({
          variant: constantCommon.ERROR,
          code: error.body.code
        }));
      } else {
        yield put(initApp());
      }
    } finally {
      nprogress.done();
    }
  });
};
module.exports.watchDeleteBusinessCard = watchDeleteBusinessCard;

const watchUploadImageBusinessCard = function* () {
  yield takeEvery(constant.UPLOAD_IMAGE_BUSINESS_CARD_REQUEST, function* ({ image }) {
    try {
      nprogress.start();

      const responseCSRF = yield call(csrfRequest);
      const response = yield call(
        api.uploadImageBusinessCard,
        {
          csrfToken: responseCSRF.body.csrfToken,
          image
        }
      );

      yield put(action.uploadImageBusinessCardSuccess(response.body));
    } catch (error) {
      if (error && error.body) {
        yield put(action.uploadImageBusinessCardFailed({
          ...error.body,
          name: image.name
        }));
        yield put(pushAlertSnackbar({
          variant: constantCommon.ERROR,
          code: error.body.code
        }));
      } else {
        yield put(initApp());
      }
    } finally {
      nprogress.done();
    }
  });
};
module.exports.watchUploadImageBusinessCard = watchUploadImageBusinessCard;

const watchDeleteImageBusinessCard = function* () {
  yield takeEvery(constant.DELETE_IMAGE_BUSINESS_CARD_REQUEST, function* ({ publicID }) {
    try {
      nprogress.start();

      const responseCSRF = yield call(csrfRequest);
      const response = yield call(
        api.deleteImageBusinessCard,
        {
          csrfToken: responseCSRF.body.csrfToken,
          publicID
        }
      );

      yield put(action.deleteImageBusinessCardSuccess({
        ...response.body,
        publicID
      }));
    } catch (error) {
      if (error && error.body) {
        yield put(action.deleteImageBusinessCardFailed({
          ...error.body,
          publicID
        }));
        yield put(pushAlertSnackbar({
          variant: constantCommon.ERROR,
          code: error.body.code
        }));
      } else {
        yield put(initApp());
      }
    } finally {
      nprogress.done();
    }
  });
};
module.exports.watchDeleteImageBusinessCard = watchDeleteImageBusinessCard;

const watchLikeBusinessCard = function* () {
  yield takeEvery(constant.LIKE_BUSINESS_CARD_REQUEST, function* ({ businessCardID }) {
    try {
      const responseCSRF = yield call(csrfRequest);
      const response = yield call(
        api.likeBusinessCard,
        {
          csrfToken: responseCSRF.body.csrfToken,
          businessCardID
        }
      );

      yield put(action.likeBusinessCardSuccess(response.body));

      yield call(emitSockets, response.body.sockets);
    } catch (error) {
      if (error && error.body) {
        yield put(action.likeBusinessCardFailed(error.body));
        yield put(pushAlertSnackbar({
          variant: constantCommon.ERROR,
          code: error.body.code
        }));
      } else {
        yield put(initApp());
      }
    }
  });
};
module.exports.watchLikeBusinessCard = watchLikeBusinessCard;

const watchUnLikeBusinessCard = function* () {
  yield takeEvery(constant.UNLIKE_BUSINESS_CARD_REQUEST, function* ({ businessCardID }) {
    try {
      const responseCSRF = yield call(csrfRequest);
      const response = yield call(
        api.unLikeBusinessCard,
        {
          csrfToken: responseCSRF.body.csrfToken,
          businessCardID
        }
      );

      yield put(action.unLikeBusinessCardSuccess(response.body));
    } catch (error) {
      if (error && error.body) {
        yield put(action.unLikeBusinessCardFailed(error.body));
        yield put(pushAlertSnackbar({
          variant: constantCommon.ERROR,
          code: error.body.code
        }));
      } else {
        yield put(initApp());
      }
    }
  });
};
module.exports.watchUnLikeBusinessCard = watchUnLikeBusinessCard;

const watchGetCommentsBusinessCard = function* () {
  yield takeLatest(constant.GET_COMMENTS_BUSINESS_CARD_REQUEST, function* ({ businessCardID, max }) {
    try {
      nprogress.start();

      const response = yield call(
        api.getCommentsBusinessCard,
        {
          businessCardID,
          max
        }
      );

      yield put(action.getCommentsBusinessCardSuccess(response.body));
    } catch (error) {
      if (error && error.body) {
        yield put(action.getCommentsBusinessCardFailed(error.body));
        if (max !== -1) {
          yield put(pushAlertSnackbar({
            variant: constantCommon.ERROR,
            code: error.body.code
          }));
        }
      } else {
        yield put(initApp());
      }
    } finally {
      nprogress.done();
    }
  });
};
module.exports.watchGetCommentsBusinessCard = watchGetCommentsBusinessCard;

const watchCommentBusinessCard = function* () {
  yield takeEvery(constant.COMMENT_BUSINESS_CARD_REQUEST, function* ({ businessCardID, comment }) {
    try {
      nprogress.start();

      const responseCSRF = yield call(csrfRequest);
      const response = yield call(
        api.commentBusinessCard,
        {
          csrfToken: responseCSRF.body.csrfToken,
          businessCardID,
          comment
        }
      );

      yield put(action.commentBusinessCardSuccess(response.body));

      yield call(emitSockets, response.body.sockets);
    } catch (error) {
      if (error && error.body) {
        yield put(action.commentBusinessCardFailed(error.body));
        yield put(pushAlertSnackbar({
          variant: constantCommon.ERROR,
          code: error.body.code
        }));
      } else {
        yield put(initApp());
      }
    } finally {
      nprogress.done();
    }
  });
};
module.exports.watchCommentBusinessCard = watchCommentBusinessCard;

const watchChangeCommentBusinessCard = function* () {
  yield takeEvery(constant.CHANGE_COMMENT_BUSINESS_CARD_REQUEST, function* ({
    businessCardID,
    commentID,
    comment
  }) {
    try {
      nprogress.start();

      const responseCSRF = yield call(csrfRequest);
      const response = yield call(
        api.changeCommentBusinessCard,
        {
          csrfToken: responseCSRF.body.csrfToken,
          businessCardID,
          commentID,
          comment
        }
      );

      yield put(action.changeCommentBusinessCardSuccess(response.body));
      yield put(pushAlertSnackbar({
        variant: constantCommon.SUCCESS,
        code: response.body.code
      }));
    } catch (error) {
      if (error && error.body) {
        yield put(action.changeCommentBusinessCardFailed(error.body));
        yield put(pushAlertSnackbar({
          variant: constantCommon.ERROR,
          code: error.body.code
        }));
      } else {
        yield put(initApp());
      }
    } finally {
      nprogress.done();
    }
  });
};
module.exports.watchChangeCommentBusinessCard = watchChangeCommentBusinessCard;

const watchDeleteCommentBusinessCard = function* () {
  yield takeEvery(constant.DELETE_COMMENT_BUSINESS_CARD_REQUEST, function* ({ businessCardID, commentID }) {
    try {
      nprogress.start();

      const responseCSRF = yield call(csrfRequest);
      const response = yield call(
        api.deleteCommentBusinessCard,
        {
          csrfToken: responseCSRF.body.csrfToken,
          businessCardID,
          commentID
        }
      );

      yield put(action.deleteCommentBusinessCardSuccess(response.body));
      yield put(pushAlertSnackbar({
        variant: constantCommon.SUCCESS,
        code: response.body.code
      }));
    } catch (error) {
      if (error && error.body) {
        yield put(action.deleteCommentBusinessCardFailed(error.body));
        yield put(pushAlertSnackbar({
          variant: constantCommon.ERROR,
          code: error.body.code
        }));
      } else {
        yield put(initApp());
      }
    } finally {
      nprogress.done();
    }
  });
};
module.exports.watchDeleteCommentBusinessCard = watchDeleteCommentBusinessCard;

const watchDealBusinessCard = function* () {
  yield takeEvery(constant.DEAL_BUSINESS_CARD_REQUEST, function* ({ businessCardID, deal }) {
    try {
      nprogress.start();

      const responseCSRF = yield call(csrfRequest);
      const response = yield call(
        api.dealBusinessCard,
        {
          csrfToken: responseCSRF.body.csrfToken,
          businessCardID,
          deal
        }
      );

      yield put(action.dealBusinessCardSuccess(response.body));
      yield put(pushAlertSnackbar({
        variant: constantCommon.SUCCESS,
        code: response.body.code
      }));

      yield call(emitSockets, response.body.sockets);

      yield put(push(`${url.DEAL_HISTORY_GET}?id=${response.body.businessCardDealHistoryID}`));
    } catch (error) {
      if (error && error.body) {
        yield put(action.dealBusinessCardFailed(error.body));
        yield put(pushAlertSnackbar({
          variant: constantCommon.ERROR,
          code: error.body.code
        }));
      } else {
        yield put(initApp());
      }
    } finally {
      nprogress.done();
    }
  });
};
module.exports.watchDealBusinessCard = watchDealBusinessCard;

const watchReportBusinessCard = function* () {
  yield takeEvery(constant.REPORT_BUSINESS_CARD_REQUEST, function* ({ businessCardID, report }) {
    try {
      nprogress.start();

      const responseCSRF = yield call(csrfRequest);
      const response = yield call(
        api.reportBusinessCard,
        {
          csrfToken: responseCSRF.body.csrfToken,
          businessCardID,
          report
        }
      );

      yield put(action.reportBusinessCardSuccess(response.body));
      yield put(pushAlertSnackbar({
        variant: constantCommon.SUCCESS,
        code: response.body.code
      }));

      yield call(emitSockets, response.body.sockets);
    } catch (error) {
      if (error && error.body) {
        yield put(action.reportBusinessCardFailed(error.body));
        yield put(pushAlertSnackbar({
          variant: constantCommon.ERROR,
          code: error.body.code
        }));
      } else {
        yield put(initApp());
      }
    } finally {
      nprogress.done();
    }
  });
};
module.exports.watchReportBusinessCard = watchReportBusinessCard;

const watchChangeDescriptionsBusinessCard = function* () {
  yield takeLatest(constant.CHANGE_DESCRIPTIONS_BUSINESS_CARD_REQUEST, function* ({ descriptions }) {
    try {
      nprogress.start();

      const responseCSRF = yield call(csrfRequest);
      const response = yield call(
        api.changeDescriptionsBusinessCard,
        {
          csrfToken: responseCSRF.body.csrfToken,
          descriptions
        }
      );

      yield put(action.changeDescriptionsBusinessCardSuccess(response.body));
      yield put(pushAlertSnackbar({
        variant: constantCommon.SUCCESS,
        code: response.body.code
      }));
    } catch (error) {
      if (error && error.body) {
        yield put(action.changeDescriptionsBusinessCardFailed(error.body));
        yield put(pushAlertSnackbar({
          variant: constantCommon.ERROR,
          code: error.body.code
        }));
      } else {
        yield put(initApp());
      }
    } finally {
      nprogress.done();
    }
  });
};
module.exports.watchChangeDescriptionsBusinessCard = watchChangeDescriptionsBusinessCard;

const watchChangeAddressBusinessCard = function* () {
  yield takeLatest(constant.CHANGE_ADDRESS_BUSINESS_CARD_REQUEST, function* ({ address }) {
    try {
      nprogress.start();

      const responseCSRF = yield call(csrfRequest);
      const response = yield call(
        api.changeAddressBusinessCard,
        {
          csrfToken: responseCSRF.body.csrfToken,
          address
        }
      );

      yield put(action.changeAddressBusinessCardSuccess(response.body));
      yield put(pushAlertSnackbar({
        variant: constantCommon.SUCCESS,
        code: response.body.code
      }));
    } catch (error) {
      if (error && error.body) {
        yield put(action.changeAddressBusinessCardFailed(error.body));
        yield put(pushAlertSnackbar({
          variant: constantCommon.ERROR,
          code: error.body.code
        }));
      } else {
        yield put(initApp());
      }
    } finally {
      nprogress.done();
    }
  });
};
module.exports.watchChangeAddressBusinessCard = watchChangeAddressBusinessCard;

const watchChangeStatusBusinessCard = function* () {
  yield takeLatest(constant.CHANGE_STATUS_BUSINESS_CARD_REQUEST, function* () {
    try {
      nprogress.start();

      const responseCSRF = yield call(csrfRequest);
      const response = yield call(
        api.changeStatusBusinessCard,
        { csrfToken: responseCSRF.body.csrfToken }
      );

      yield put(action.changeStatusBusinessCardSuccess(response.body));
      yield put(pushAlertSnackbar({
        variant: constantCommon.SUCCESS,
        code: response.body.code
      }));
    } catch (error) {
      if (error && error.body) {
        yield put(action.changeStatusBusinessCardFailed(error.body));
        yield put(pushAlertSnackbar({
          variant: constantCommon.ERROR,
          code: error.body.code
        }));
      } else {
        yield put(initApp());
      }
    } finally {
      nprogress.done();
    }
  });
};
module.exports.watchChangeStatusBusinessCard = watchChangeStatusBusinessCard;

const watchReUploadBusinessCard = function* () {
  yield takeLatest(constant.REUPLOAD_BUSINESS_CARD_REQUEST, function* () {
    try {
      nprogress.start();

      const responseCSRF = yield call(csrfRequest);
      const response = yield call(
        api.reUploadBusinessCard,
        { csrfToken: responseCSRF.body.csrfToken }
      );

      yield put(action.reUploadBusinessCardSuccess(response.body));
      yield put(pushAlertSnackbar({
        variant: constantCommon.SUCCESS,
        code: response.body.code
      }));
    } catch (error) {
      if (error && error.body) {
        yield put(action.reUploadBusinessCardFailed(error.body));
        yield put(pushAlertSnackbar({
          variant: constantCommon.ERROR,
          code: error.body.code
        }));
      } else {
        yield put(initApp());
      }
    } finally {
      nprogress.done();
    }
  });
};
module.exports.watchReUploadBusinessCard = watchReUploadBusinessCard;

const watchGetBusinessCards = function* () {
  yield takeLatest(
    constant.GET_BUSINESS_CARDS_REQUEST,
    function* ({
      fragment,
      skip,
      extendSearch
    }) {
      try {
        nprogress.start();

        const response = yield call(
          api.getBusinessCards,
          {
            type: fragment,
            skip,
            extendSearch
          }
        );

        yield put(action.getBusinessCardsSuccess(response.body));
      } catch (error) {
        if (error && error.body) {
          yield put(action.getBusinessCardsFailed(error.body));
          if (skip !== 0) {
            yield put(pushAlertSnackbar({
              variant: constantCommon.ERROR,
              code: error.body.code
            }));
          }
        } else {
          yield put(initApp());
        }
      } finally {
        nprogress.done();
      }
    }
  );
};
module.exports.watchGetBusinessCards = watchGetBusinessCards;
