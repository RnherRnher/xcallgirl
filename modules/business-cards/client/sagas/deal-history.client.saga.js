const nprogress = require('nprogress');
const api = require('../services/deal-history.service');
const action = require('../actions/deal-history.client.action');
const constant = require('../constants/deal-history.client.constant');
const constantCommon = require('../../../../commons/utils/constant');
const {
  takeLatest,
  put,
  call,
  takeEvery
} = require('redux-saga/effects');
const { pushAlertSnackbar } = require('../../../core/client/actions/core.client.action');
const { csrfRequest } = require('../../../core/client/services/core.client.service');
const { emitSockets } = require('../../../core/client/lib/socket-io');
const { initApp } = require('../../../core/client/actions/app.client.action');
const { SOCKET_BUSINESS_CARD_DEAL_HISTORY_LISTEN } = require('../../../../commons/modules/business-cards/datas/socket.data');

const watchGetBusinessCardsDealHistorys = function* () {
  yield takeLatest(constant.GET_BUSINESS_CARDS_DEAL_HISTORYS_REQUEST, function* ({ fragment, skip }) {
    try {
      nprogress.start();

      const response = yield call(
        api.getBusinessCardsDealHistorys,
        {
          type: fragment,
          skip
        }
      );

      yield put(action.getBusinessCardsDealHistorysSuccess(response.body));
    } catch (error) {
      if (error && error.body) {
        yield put(action.getBusinessCardsDealHistorysFailed(error.body));
        if (skip !== 0) {
          yield put(pushAlertSnackbar({
            variant: constantCommon.ERROR,
            code: error.body.code
          }));
        }
      } else {
        yield put(initApp());
      }
    } finally {
      nprogress.done();
    }
  });
};
module.exports.watchGetBusinessCardsDealHistorys = watchGetBusinessCardsDealHistorys;

const watchGetBusinessCardDealHistoryID = function* () {
  yield takeLatest(constant.GET_BUSINESS_CARDS_DEAL_HISTORY_ID_REQUEST, function* ({ businessCardDealHistoryID, fragment }) {
    try {
      nprogress.start();

      const response = yield call(
        api.getBusinessCardDealHistoryID,
        {
          businessCardDealHistoryID,
          type: fragment
        }
      );

      yield put(action.getBusinessCardsDealHistoryIDSuccess(response.body));
    } catch (error) {
      if (error && error.body) {
        yield put(action.getBusinessCardsDealHistoryIDFailed(error.body));
      } else {
        yield put(initApp());
      }
    } finally {
      nprogress.done();
    }
  });
};
module.exports.watchGetBusinessCardDealHistoryID = watchGetBusinessCardDealHistoryID;

const watchGetBusinessCardDealHistory = function* () {
  yield takeEvery(constant.GET_BUSINESS_CARD_DEAL_HISTORY_REQUEST, function* ({ businessCardDealHistoryID, fragment }) {
    const response = yield call(
      api.getBusinessCardDealHistoryID,
      {
        businessCardDealHistoryID,
        type: fragment
      }
    );

    yield put(action.getBusinessCardDealHistorySuccess(response.body));
  });
};
module.exports.watchGetBusinessCardDealHistory = watchGetBusinessCardDealHistory;

const watchAcceptBusinessCardsDealHistoryID = function* () {
  yield takeLatest(constant.ACCEPT_BUSINESS_CARDS_DEAL_HISTORY_ID_REQUEST, function* ({ businessCardDealHistoryID, accept }) {
    try {
      nprogress.start();

      const responseCSRF = yield call(csrfRequest);
      const response = yield call(
        api.acceptBusinessCardsDealHistoryID,
        {
          csrfToken: responseCSRF.body.csrfToken,
          businessCardDealHistoryID,
          accept
        }
      );

      yield put(action.acceptBusinessCardsDealHistoryIDSuccess(response.body));
      yield put(pushAlertSnackbar({
        variant: constantCommon.SUCCESS,
        code: response.body.code
      }));

      yield call(emitSockets, response.body.sockets);
    } catch (error) {
      if (error && error.body) {
        yield put(action.acceptBusinessCardsDealHistoryIDFailed(error.body));
        yield put(pushAlertSnackbar({
          variant: constantCommon.ERROR,
          code: error.body.code
        }));
      } else {
        yield put(initApp());
      }
    } finally {
      nprogress.done();
    }
  });
};
module.exports.watchAcceptBusinessCardsDealHistoryID = watchAcceptBusinessCardsDealHistoryID;

const watchGetTokenBusinessCardsDealHistoryID = function* () {
  yield takeLatest(constant.GET_TOKEN_BUSINESS_CARDS_DEAL_HISTORY_ID_REQUEST, function* ({ businessCardDealHistoryID }) {
    try {
      nprogress.start();

      const response = yield call(
        api.getTokenBusinessCardsDealHistoryID,
        { businessCardDealHistoryID }
      );

      yield put(action.getTokenBusinessCardsDealHistoryIDSuccess(response.body));
      yield put(pushAlertSnackbar({
        variant: constantCommon.SUCCESS,
        code: response.body.code
      }));

      yield call(emitSockets, response.body.sockets);
    } catch (error) {
      if (error && error.body) {
        yield put(action.getTokenBusinessCardsDealHistoryIDFailed(error.body));
        yield put(pushAlertSnackbar({
          variant: constantCommon.ERROR,
          code: error.body.code
        }));
      } else {
        yield put(initApp());
      }
    } finally {
      nprogress.done();
    }
  });
};
module.exports.watchGetTokenBusinessCardsDealHistoryID = watchGetTokenBusinessCardsDealHistoryID;

const watchValidateTokenBusinessCardsDealHistoryID = function* () {
  yield takeLatest(constant.VALIDATE_TOKEN_BUSINESS_CARDS_DEAL_HISTORY_ID_REQUEST, function* ({ businessCardDealHistoryID, token }) {
    try {
      nprogress.start();

      const responseCSRF = yield call(csrfRequest);
      const response = yield call(
        api.validateTokenBusinessCardsDealHistoryID,
        {
          csrfToken: responseCSRF.body.csrfToken,
          businessCardDealHistoryID,
          token
        }
      );

      yield put(action.validateTokenBusinessCardsDealHistoryIDSuccess(response.body));
      yield put(pushAlertSnackbar({
        variant: constantCommon.SUCCESS,
        code: response.body.code
      }));

      yield call(emitSockets, response.body.sockets);
    } catch (error) {
      if (error && error.body) {
        yield put(action.validateTokenBusinessCardsDealHistoryIDFailed(error.body));
        yield put(pushAlertSnackbar({
          variant: constantCommon.ERROR,
          code: error.body.code
        }));
      } else {
        yield put(initApp());
      }
    } finally {
      nprogress.done();
    }
  });
};
module.exports.watchValidateTokenBusinessCardsDealHistoryID = watchValidateTokenBusinessCardsDealHistoryID;

const watchReportBusinessCardsDealHistoryID = function* () {
  yield takeLatest(constant.REPORT_BUSINESS_CARDS_DEAL_HISTORY_ID_REQUEST, function* ({ businessCardDealHistoryID, report }) {
    try {
      nprogress.start();

      const responseCSRF = yield call(csrfRequest);
      const response = yield call(
        api.reportBusinessCardsDealHistoryID,
        {
          csrfToken: responseCSRF.body.csrfToken,
          businessCardDealHistoryID,
          report
        }
      );

      yield put(action.reportBusinessCardsDealHistoryIDSuccess(response.body));
      yield put(pushAlertSnackbar({
        variant: constantCommon.SUCCESS,
        code: response.body.code
      }));
    } catch (error) {
      if (error && error.body) {
        yield put(action.reportBusinessCardsDealHistoryIDFailed(error.body));
        yield put(pushAlertSnackbar({
          variant: constantCommon.ERROR,
          code: error.body.code
        }));
      } else {
        yield put(initApp());
      }
    } finally {
      nprogress.done();
    }
  });
};
module.exports.watchReportBusinessCardsDealHistoryID = watchReportBusinessCardsDealHistoryID;

const watchListenBusinessCardsDealHistory = function* () {
  yield takeEvery(constant.LISTEN_BUSINESS_CARDS_DEAL_HISTORY, function* ({ fragment, businessCardDealHistoryID }) {
    yield call(
      emitSockets,
      [{
        names: [SOCKET_BUSINESS_CARD_DEAL_HISTORY_LISTEN],
        data: {
          type: fragment,
          id: businessCardDealHistoryID
        },
      }]
    );
  });
};
module.exports.watchListenBusinessCardsDealHistory = watchListenBusinessCardsDealHistory;
