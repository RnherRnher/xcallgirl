const { action } = require('../../../core/client/utils/middleware');
const constant = require('../constants/business-card.client.constant');

/**
 * @override Yêu cầu lấy danh thiếp
 *
 * @param   {Boolean} isAlert
 * @returns {Object}
 */
const getProfileBusinessCardRequest = (isAlert = true) => {
  return action(constant.GET_PROFILE_BUSINESS_CARD_REQUEST, { isAlert });
};
module.exports.getProfileBusinessCardRequest = getProfileBusinessCardRequest;

/**
 * @override Lấy danh thiếp thành công
 *
 * @param   {Object}
 * @returns {Object}
 */
const getProfileBusinessCardSuccess = ({ code, businessCard }) => {
  return action(constant.GET_PROFILE_BUSINESS_CARD_SUCCESS, { code, businessCard });
};
module.exports.getProfileBusinessCardSuccess = getProfileBusinessCardSuccess;

/**
 * @override Lấy danh thiếp thất bại
 *
 * @param   {Object}
 * @returns {Object}
 */
const getProfileBusinessCardFailed = ({ code }) => {
  return action(constant.GET_PROFILE_BUSINESS_CARD_FAILED, { code });
};
module.exports.getProfileBusinessCardFailed = getProfileBusinessCardFailed;

/**
 * @override Yêu cầu tạo danh thiếp
 *
 * @param   {Object} businessCard
 * @param   {[Object]} images
 * @returns {Object}
 */
const createBusinessCardRequest = (businessCard, images) => {
  return action(constant.CREATE_BUSINESS_CARD_REQUEST, { businessCard, images });
};
module.exports.createBusinessCardRequest = createBusinessCardRequest;

/**
 * @override Tạo danh thiếp thành công
 *
 * @param   {Object}
 * @returns {Object}
 */
const createBusinessCardSuccess = ({ code, businessCard }) => {
  return action(constant.CREATE_BUSINESS_CARD_SUCCESS, { code, businessCard });
};
module.exports.createBusinessCardSuccess = createBusinessCardSuccess;

/**
 * @override Tạo danh thiếp thất bại
 *
 * @param   {Object}
 * @returns {Object}
 */
const createBusinessCardFailed = ({ code }) => {
  return action(constant.CREATE_BUSINESS_CARD_FAILED, { code });
};
module.exports.createBusinessCardFailed = createBusinessCardFailed;

/**
 * @override Yêu cầu xóa danh thiếp
 *
 * @param   {String} password
 * @returns {Object}
 */
const deleteBusinessCardRequest = (deletee) => {
  return action(constant.DELETE_BUSINESS_CARD_REQUEST, { deletee });
};
module.exports.deleteBusinessCardRequest = deleteBusinessCardRequest;

/**
 * @override Xóa danh thiếp thành công
 *
 * @param   {Object}
 * @returns {Object}
 */
const deleteBusinessCardSuccess = ({ code }) => {
  return action(constant.DELETE_BUSINESS_CARD_SUCCESS, { code });
};
module.exports.deleteBusinessCardSuccess = deleteBusinessCardSuccess;

/**
 * @override Xóa danh thiếp thất bại
 *
 * @param   {Object}
 * @returns {Object}
 */
const deleteBusinessCardFailed = ({ code }) => {
  return action(constant.DELETE_BUSINESS_CARD_FAILED, { code });
};
module.exports.deleteBusinessCardFailed = deleteBusinessCardFailed;

/**
 * @override Yêu cầu tải ảnh danh thiếp
 *
 * @param   {Object} image
 * @returns {Object}
 */
const uploadImageBusinessCardRequest = (image) => {
  return action(constant.UPLOAD_IMAGE_BUSINESS_CARD_REQUEST, { image });
};
module.exports.uploadImageBusinessCardRequest = uploadImageBusinessCardRequest;

/**
 * @override Tải ảnh danh thiếp thành công
 *
 * @param   {Object}
 * @returns {Object}
 */
const uploadImageBusinessCardSuccess = ({
  code,
  image,
  updateDate
}) => {
  return action(
    constant.UPLOAD_IMAGE_BUSINESS_CARD_SUCCESS,
    {
      code,
      image,
      updateDate
    }
  );
};
module.exports.uploadImageBusinessCardSuccess = uploadImageBusinessCardSuccess;

/**
 * @override Tải ảnh danh thiếp thất bại
 *
 * @param   {Object}
 * @returns {Object}
 */
const uploadImageBusinessCardFailed = ({ code, name }) => {
  return action(constant.UPLOAD_IMAGE_BUSINESS_CARD_FAILED, { code, name });
};
module.exports.uploadImageBusinessCardFailed = uploadImageBusinessCardFailed;

/**
 * @override Yêu cầu xóa ảnh danh thiếp
 *
 * @param   {String} publicID
 * @returns {Object}
 */
const deleteImageBusinessCardRequest = (publicID) => {
  return action(constant.DELETE_IMAGE_BUSINESS_CARD_REQUEST, { publicID });
};
module.exports.deleteImageBusinessCardRequest = deleteImageBusinessCardRequest;

/**
 * @override Xóa ảnh danh thiếp thành công
 *
 * @param   {Object}
 * @returns {Object}
 */
const deleteImageBusinessCardSuccess = ({
  code,
  images,
  publicID,
  updateDate
}) => {
  return action(
    constant.DELETE_IMAGE_BUSINESS_CARD_SUCCESS, {
      code,
      images,
      publicID,
      updateDate
    }
  );
};
module.exports.deleteImageBusinessCardSuccess = deleteImageBusinessCardSuccess;

/**
 * @override Xóa ảnh danh thiếp thất bại
 *
 * @param   {Object}
 * @returns {Object}
 */
const deleteImageBusinessCardFailed = ({ code, publicID }) => {
  return action(constant.DELETE_IMAGE_BUSINESS_CARD_FAILED, { code, publicID });
};
module.exports.deleteImageBusinessCardFailed = deleteImageBusinessCardFailed;

/**
 * @override Yêu cầu thích danh thiếp
 *
 * @param   {ObjectId} businessCardID
 * @returns {Object}
 */
const likeBusinessCardRequest = (businessCardID) => {
  return action(constant.LIKE_BUSINESS_CARD_REQUEST, { businessCardID });
};
module.exports.likeBusinessCardRequest = likeBusinessCardRequest;

/**
 * @override Thích danh thiếp thành công
 *
 * @param   {Object}
 * @returns {Object}
 */
const likeBusinessCardSuccess = ({ code, businessCardID }) => {
  return action(constant.LIKE_BUSINESS_CARD_SUCCESS, { code, businessCardID });
};
module.exports.likeBusinessCardSuccess = likeBusinessCardSuccess;

/**
 * @override Thích danh thiếp thất bại
 *
 * @param   {Object}
 * @returns {Object}
 */
const likeBusinessCardFailed = ({ code, businessCardID }) => {
  return action(constant.LIKE_BUSINESS_CARD_FAILED, { code, businessCardID });
};
module.exports.likeBusinessCardFailed = likeBusinessCardFailed;

/**
 * @override Yêu cầu bỏ thích danh thiếp
 *
 * @param   {ObjectId} businessCardID
 * @returns {Object}
 */
const unLikeBusinessCardRequest = (businessCardID) => {
  return action(constant.UNLIKE_BUSINESS_CARD_REQUEST, { businessCardID });
};
module.exports.unLikeBusinessCardRequest = unLikeBusinessCardRequest;

/**
 * @override Bỏ thích danh thiếp thành công
 *
 * @param   {Object}
 * @returns {Object}
 */
const unLikeBusinessCardSuccess = ({ code, businessCardID }) => {
  return action(constant.UNLIKE_BUSINESS_CARD_SUCCESS, { code, businessCardID });
};
module.exports.unLikeBusinessCardSuccess = unLikeBusinessCardSuccess;

/**
 * @override Bỏ thích danh thiếp thất bại
 *
 * @param   {Object}
 * @returns {Object}
 */
const unLikeBusinessCardFailed = ({ code, businessCardID }) => {
  return action(constant.UNLIKE_BUSINESS_CARD_FAILED, { code, businessCardID });
};
module.exports.unLikeBusinessCardFailed = unLikeBusinessCardFailed;

/**
 * @override Yêu cầu lấy nhận xét danh thiếp
 *
 * @param   {ObjectId} businessCardID
 * @param   {Number} max
 * @returns {Object}
 */
const getCommentsBusinessCardRequest = (businessCardID, max) => {
  return action(constant.GET_COMMENTS_BUSINESS_CARD_REQUEST, { businessCardID, max });
};
module.exports.getCommentsBusinessCardRequest = getCommentsBusinessCardRequest;

/**
 * @override Lấy nhận xét danh thiếp thành công
 *
 * @param   {Object}
 * @returns {Object}
 */
const getCommentsBusinessCardSuccess = ({
  code,
  businessCardID,
  max,
  comments
}) => {
  return action(
    constant.GET_COMMENTS_BUSINESS_CARD_SUCCESS,
    {
      code,
      businessCardID,
      max,
      comments
    }
  );
};
module.exports.getCommentsBusinessCardSuccess = getCommentsBusinessCardSuccess;

/**
 * @override Lấy nhận xét danh thiếp thất bại
 *
 * @param   {Object}
 * @returns {Object}
 */
const getCommentsBusinessCardFailed = ({ code, businessCardID }) => {
  return action(constant.GET_COMMENTS_BUSINESS_CARD_FAILED, { code, businessCardID });
};
module.exports.getCommentsBusinessCardFailed = getCommentsBusinessCardFailed;

/**
 * @override Yêu cầu nhận xét danh thiếp
 *
 * @param   {ObjectId} businessCardID
 * @param   {Object} comment
 * @returns {Object}
 */
const commentBusinessCardRequest = (businessCardID, comment) => {
  return action(constant.COMMENT_BUSINESS_CARD_REQUEST, { businessCardID, comment });
};
module.exports.commentBusinessCardRequest = commentBusinessCardRequest;

/**
 * @override Nhận xét danh thiếp thành công
 *
 * @param   {Object}
 * @returns {Object}
 */
const commentBusinessCardSuccess = ({
  code,
  businessCardID,
  comment
}) => {
  return action(
    constant.COMMENT_BUSINESS_CARD_SUCCESS,
    {
      code,
      businessCardID,
      comment
    }
  );
};
module.exports.commentBusinessCardSuccess = commentBusinessCardSuccess;

/**
 * @override Nhận xét danh thiếp thất bại
 *
 * @param   {Object}
 * @returns {Object}
 */
const commentBusinessCardFailed = ({ code, businessCardID }) => {
  return action(constant.COMMENT_BUSINESS_CARD_FAILED, { code, businessCardID });
};
module.exports.commentBusinessCardFailed = commentBusinessCardFailed;

/**
 * @override Yêu cầu thay đổi nhận xét danh thiếp
 *
 * @param   {ObjectId} businessCardID
 * @param   {ObjectId} commentID
 * @param   {Object} comment
 * @returns {Object}
 */
const changeCommentBusinessCardRequest = (
  businessCardID,
  commentID,
  comment,
) => {
  return action(
    constant.CHANGE_COMMENT_BUSINESS_CARD_REQUEST,
    {
      businessCardID,
      commentID,
      comment
    }
  );
};
module.exports.changeCommentBusinessCardRequest = changeCommentBusinessCardRequest;

/**
 * @override Nhận xét thay đổi danh thiếp thành công
 *
 * @param   {Object}
 * @returns {Object}
 */
const changeCommentBusinessCardSuccess = ({
  code,
  businessCardID,
  commentID,
  comment
}) => {
  return action(
    constant.CHANGE_COMMENT_BUSINESS_CARD_SUCCESS,
    {
      code,
      businessCardID,
      commentID,
      comment
    }
  );
};
module.exports.changeCommentBusinessCardSuccess = changeCommentBusinessCardSuccess;

/**
 * @override Nhận xét thay đổi danh thiếp thất bại
 *
 * @param   {Object}
 * @returns {Object}
 */
const changeCommentBusinessCardFailed = ({
  code,
  businessCardID,
  commentID
}) => {
  return action(
    constant.CHANGE_COMMENT_BUSINESS_CARD_FAILED,
    {
      code,
      businessCardID,
      commentID
    }
  );
};
module.exports.changeCommentBusinessCardFailed = changeCommentBusinessCardFailed;

/**
 * @override Yêu cầu xóa nhận xét danh thiếp
 *
 * @param   {ObjectId} businessCardID
 * @param   {ObjectId} commentID
 * @returns {Object}
 */
const deleteCommentBusinessCardRequest = (businessCardID, commentID) => {
  return action(constant.DELETE_COMMENT_BUSINESS_CARD_REQUEST, { businessCardID, commentID });
};
module.exports.deleteCommentBusinessCardRequest = deleteCommentBusinessCardRequest;

/**
 * @override Nhận xét xóa danh thiếp thành công
 *
 * @param   {Object}
 * @returns {Object}
 */
const deleteCommentBusinessCardSuccess = ({
  code,
  businessCardID,
  commentID
}) => {
  return action(
    constant.DELETE_COMMENT_BUSINESS_CARD_SUCCESS,
    {
      code,
      businessCardID,
      commentID
    }
  );
};
module.exports.deleteCommentBusinessCardSuccess = deleteCommentBusinessCardSuccess;

/**
 * @override Nhận xét xóa danh thiếp thất bại
 *
 * @param   {Object}
 * @returns {Object}
 */
const deleteCommentBusinessCardFailed = ({
  code,
  businessCardID,
  commentID
}) => {
  return action(
    constant.DELETE_COMMENT_BUSINESS_CARD_FAILED,
    {
      code,
      businessCardID,
      commentID
    }
  );
};
module.exports.deleteCommentBusinessCardFailed = deleteCommentBusinessCardFailed;

/**
 * @override Yêu cầu giao dịch danh thiếp
 *
 * @param   {ObjectId} businessCardID
 * @param   {Object} deal
 * @returns {Object}
 */
const dealBusinessCardRequest = (businessCardID, deal) => {
  return action(constant.DEAL_BUSINESS_CARD_REQUEST, { businessCardID, deal });
};
module.exports.dealBusinessCardRequest = dealBusinessCardRequest;

/**
 * @override Giao dịch danh thiếp thành công
 *
 * @param   {Object}
 * @returns {Object}
 */
const dealBusinessCardSuccess = ({ code, businessCardID }) => {
  return action(constant.DEAL_BUSINESS_CARD_SUCCESS, { code, businessCardID });
};
module.exports.dealBusinessCardSuccess = dealBusinessCardSuccess;

/**
 * @override Giao dịch danh thiếp thất bại
 *
 * @param   {Object}
 * @returns {Object}
 */
const dealBusinessCardFailed = ({ code, businessCardID }) => {
  return action(constant.DEAL_BUSINESS_CARD_FAILED, { code, businessCardID });
};
module.exports.dealBusinessCardFailed = dealBusinessCardFailed;

/**
 * @override Yêu cầu tố cáo danh thiếp
 *
 * @param   {ObjectId} businessCardID
 * @param   {Object} report
 * @returns {Object}
 */
const reportBusinessCardRequest = (businessCardID, report) => {
  return action(constant.REPORT_BUSINESS_CARD_REQUEST, { businessCardID, report });
};
module.exports.reportBusinessCardRequest = reportBusinessCardRequest;

/**
 * @override Tố cáo danh thiếp thành công
 *
 * @param   {Object}
 * @returns {Object}
 */
const reportBusinessCardSuccess = ({ code, businessCardID }) => {
  return action(constant.REPORT_BUSINESS_CARD_SUCCESS, { code, businessCardID });
};
module.exports.reportBusinessCardSuccess = reportBusinessCardSuccess;

/**
 * @override Tố cáo danh thiếp thất bại
 *
 * @param   {Object}
 * @returns {Object}
 */
const reportBusinessCardFailed = ({ code, businessCardID }) => {
  return action(constant.REPORT_BUSINESS_CARD_FAILED, { code, businessCardID });
};
module.exports.reportBusinessCardFailed = reportBusinessCardFailed;

/**
 * @override Yêu cầu thay đổi mô tả danh thiếp
 *
 * @param   {Object} descriptions
 * @returns {Object}
 */
const changeDescriptionsBusinessCardRequest = (descriptions) => {
  return action(constant.CHANGE_DESCRIPTIONS_BUSINESS_CARD_REQUEST, { descriptions });
};
module.exports.changeDescriptionsBusinessCardRequest = changeDescriptionsBusinessCardRequest;

/**
 * @override Thay đổi mô tả thành công
 *
 * @param   {Object}
 * @returns {Object}
 */
const changeDescriptionsBusinessCardSuccess = ({
  code,
  descriptions,
  updateDate
}) => {
  return action(
    constant.CHANGE_DESCRIPTIONS_BUSINESS_CARD_SUCCESS,
    {
      code,
      descriptions,
      updateDate
    }
  );
};
module.exports.changeDescriptionsBusinessCardSuccess = changeDescriptionsBusinessCardSuccess;

/**
 * @override Thay đổi mô tả thất bại
 *
 * @param   {Object}
 * @returns {Object}
 */
const changeDescriptionsBusinessCardFailed = ({ code }) => {
  return action(constant.CHANGE_DESCRIPTIONS_BUSINESS_CARD_FAILED, { code });
};
module.exports.changeDescriptionsBusinessCardFailed = changeDescriptionsBusinessCardFailed;

/**
 * @override Yêu cầu thay đổi đỉa chỉ danh thiếp
 *
 * @param   {Object} address
 * @returns {Object}
 */
const changeAddressBusinessCardRequest = (address) => {
  return action(constant.CHANGE_ADDRESS_BUSINESS_CARD_REQUEST, { address });
};
module.exports.changeAddressBusinessCardRequest = changeAddressBusinessCardRequest;

/**
 * @override Thay đổi đỉa chỉ thành công
 *
 * @param   {Object}
 * @returns {Object}
 */
const changeAddressBusinessCardSuccess = ({
  code,
  address,
  updateDate
}) => {
  return action(
    constant.CHANGE_ADDRESS_BUSINESS_CARD_SUCCESS,
    {
      code,
      address,
      updateDate
    }
  );
};
module.exports.changeAddressBusinessCardSuccess = changeAddressBusinessCardSuccess;

/**
 * @override Thay đổi đỉa chỉ thất bại
 *
 * @param   {Object}
 * @returns {Object}
 */
const changeAddressBusinessCardFailed = ({ code }) => {
  return action(constant.CHANGE_ADDRESS_BUSINESS_CARD_FAILED, { code });
};
module.exports.changeAddressBusinessCardFailed = changeAddressBusinessCardFailed;

/**
 * @override Yêu cầu đăng lại danh thiếp
 *
 * @returns {Object}
 */
const reUploadBusinessCardRequest = () => {
  return action(constant.REUPLOAD_BUSINESS_CARD_REQUEST);
};
module.exports.reUploadBusinessCardRequest = reUploadBusinessCardRequest;

/**
 * @override Đăng lại danh thiếp thành công
 *
 * @param   {Object}
 * @returns {Object}
 */
const reUploadBusinessCardSuccess = ({ code, reUpload }) => {
  return action(constant.REUPLOAD_BUSINESS_CARD_SUCCESS, { code, reUpload });
};
module.exports.reUploadBusinessCardSuccess = reUploadBusinessCardSuccess;

/**
 * @override Đăng lại danh thiếp thất bại
 *
 * @param   {Object}
 * @returns {Object}
 */
const reUploadBusinessCardFailed = ({ code }) => {
  return action(constant.REUPLOAD_BUSINESS_CARD_FAILED, { code });
};
module.exports.reUploadBusinessCardFailed = reUploadBusinessCardFailed;

/**
 * @override Yêu cầu thay đổi trạng thái danh thiếp
 *
 * @returns {Object}
 */
const changeStatusBusinessCardRequest = () => {
  return action(constant.CHANGE_STATUS_BUSINESS_CARD_REQUEST);
};
module.exports.changeStatusBusinessCardRequest = changeStatusBusinessCardRequest;

/**
 * @override Thay đổi trạng thái danh thiếp thành công
 *
 * @param   {Object}
 * @returns {Object}
 */
const changeStatusBusinessCardSuccess = ({
  code,
  status,
  updateDate
}) => {
  return action(
    constant.CHANGE_STATUS_BUSINESS_CARD_SUCCESS,
    {
      code,
      status,
      updateDate
    }
  );
};
module.exports.changeStatusBusinessCardSuccess = changeStatusBusinessCardSuccess;

/**
 * @override Thay đổi trạng thái danh thiếp thất bại
 *
 * @param   {Object}
 * @returns {Object}
 */
const changeStatusBusinessCardFailed = ({ code }) => {
  return action(constant.CHANGE_STATUS_BUSINESS_CARD_FAILED, { code });
};
module.exports.changeStatusBusinessCardFailed = changeStatusBusinessCardFailed;

/**
 * @override Yêu cầu lấy danh thiếp theo id
 *
 * @param   {ObjectId} businessCardID
 * @returns {Object}
 */
const getBusinessCardIDRequest = (businessCardID) => {
  return action(constant.GET_BUSINESS_CARD_ID_REQUEST, { businessCardID });
};
module.exports.getBusinessCardIDRequest = getBusinessCardIDRequest;

/**
 * @override Lấy danh thiếp theo id thành công
 *
 * @param   {Object}
 * @returns {Object}
 */
const getBusinessCardIDSuccess = ({ code, businessCard }) => {
  return action(constant.GET_BUSINESS_CARD_ID_SUCCESS, { code, businessCard });
};
module.exports.getBusinessCardIDSuccess = getBusinessCardIDSuccess;

/**
 * @override Lấy danh thiếp theo id thất bại
 *
 * @param   {Object}
 * @returns {Object}
 */
const getBusinessCardIDFailed = ({ code }) => {
  return action(constant.GET_BUSINESS_CARD_ID_FAILED, { code });
};
module.exports.getBusinessCardIDFailed = getBusinessCardIDFailed;

/**
 * @override Yêu cầu lấy danh thiếp
 *
 * @param   {String}  fragment
 * @param   {Number}  skip
 * @param   {Boolean} reload
 * @param   {Object}  extendSearch
 * @returns {Object}
 */
const getBusinessCardsRequest = (
  fragment,
  skip,
  reload = false,
  extendSearch
) => {
  return action(
    constant.GET_BUSINESS_CARDS_REQUEST,
    {
      fragment,
      skip: reload ? 0 : (skip || 0),
      reload,
      extendSearch
    }
  );
};
module.exports.getBusinessCardsRequest = getBusinessCardsRequest;

/**
 * @override Lấy danh thiếp thành công
 *
 * @param   {Object}
 * @returns {Object}
 */
const getBusinessCardsSuccess = ({
  code,
  skip,
  businessCards
}) => {
  return action(
    constant.GET_BUSINESS_CARDS_SUCCESS,
    {
      code,
      skip,
      businessCards
    }
  );
};
module.exports.getBusinessCardsSuccess = getBusinessCardsSuccess;

/**
 * @override Lấy danh thiếp thất bại
 *
 * @param   {Object}
 * @returns {Object}
 */
const getBusinessCardsFailed = ({ code }) => {
  return action(constant.GET_BUSINESS_CARDS_FAILED, { code });
};
module.exports.getBusinessCardsFailed = getBusinessCardsFailed;

/**
 * @override Thiết lặp lại cửa hàng
 *
 * @param   {Object}
 */
const resetBusinessCardStore = () => {
  return action(constant.RESERT_BUSINESS_CARD_STORE);
};
module.exports.resetBusinessCardStore = resetBusinessCardStore;
