const constant = require('../constants/deal-history.client.constant');
const commonConstant = require('../../../../commons/utils/constant');
const { action } = require('../../../core/client/utils/middleware');

/**
 * @override Yêu cầu lấy lịch sử giao dịch
 *
 * @param   {String}  fragment
 * @param   {Number}  skip
 * @param   {Boolean} reload
 * @returns {Object}
 */
const getBusinessCardsDealHistorysRequest = (
  fragment,
  skip,
  reload = false
) => {
  return action(
    constant.GET_BUSINESS_CARDS_DEAL_HISTORYS_REQUEST,
    {
      fragment,
      skip: reload ? 0 : (skip || 0),
      reload
    }
  );
};
module.exports.getBusinessCardsDealHistorysRequest = getBusinessCardsDealHistorysRequest;

/**
 * @override Lấy lịch sử giao dịch thành công
 *
 * @param   {Object}
 * @returns {Object}
 */
const getBusinessCardsDealHistorysSuccess = ({
  code,
  skip,
  businessCardDealHistorys,
  count
}) => {
  return action(
    constant.GET_BUSINESS_CARDS_DEAL_HISTORYS_SUCCESS,
    {
      code,
      skip,
      businessCardDealHistorys,
      count
    }
  );
};
module.exports.getBusinessCardsDealHistorysSuccess = getBusinessCardsDealHistorysSuccess;

/**
 * @override Lấy lịch sử giao dịch thất bại
 *
 * @param   {Object}
 * @returns {Object}
 */
const getBusinessCardsDealHistorysFailed = ({ code }) => {
  return action(constant.GET_BUSINESS_CARDS_DEAL_HISTORYS_FAILED, { code });
};
module.exports.getBusinessCardsDealHistorysFailed = getBusinessCardsDealHistorysFailed;

/**
 * @override Yêu cầu lấy lịch sử giao dịch theo id
 *
 * @param   {Object}
 * @returns {Object}
 */
const getBusinessCardsDealHistoryIDRequest = ({ businessCardDealHistoryID }) => {
  return action(constant.GET_BUSINESS_CARDS_DEAL_HISTORY_ID_REQUEST, { businessCardDealHistoryID, fragment: commonConstant.FULL });
};
module.exports.getBusinessCardsDealHistoryIDRequest = getBusinessCardsDealHistoryIDRequest;

/**
 * @override Lấy lịch sử giao dịch theo id thành công
 *
 * @param   {Object}
 * @returns {Object}
 */
const getBusinessCardsDealHistoryIDSuccess = ({
  code,
  businessCardDealHistory,
  user,
  provider
}) => {
  return action(
    constant.GET_BUSINESS_CARD_DEAL_HISTORY_ID_SUCCESS,
    {
      code,
      businessCardDealHistory,
      user,
      provider
    }
  );
};
module.exports.getBusinessCardsDealHistoryIDSuccess = getBusinessCardsDealHistoryIDSuccess;

/**
 * @override Lấy lịch sử giao dịch theo id thất bại
 *
 * @param   {Object}
 * @returns {Object}
 */
const getBusinessCardsDealHistoryIDFailed = ({ code }) => {
  return action(constant.GET_BUSINESS_CARD_DEAL_HISTORY_ID_FAILED, { code });
};
module.exports.getBusinessCardsDealHistoryIDFailed = getBusinessCardsDealHistoryIDFailed;

/**
 * @override Yêu cầu lấy lịch sử giao dịch
 *
 * @param   {Object}
 * @param   {Boolean} reload
 * @returns {Object}
 */
const getBusinessCardDealHistoryRequest = ({ businessCardDealHistoryID }, reload = false) => {
  return action(
    constant.GET_BUSINESS_CARD_DEAL_HISTORY_REQUEST,
    {
      businessCardDealHistoryID,
      fragment: commonConstant.SHORTCUT,
      reload
    }
  );
};
module.exports.getBusinessCardDealHistoryRequest = getBusinessCardDealHistoryRequest;

/**
 * @override Lấy lịch sử giao dịch thành công
 *
 * @param   {Object}
 * @returns {Object}
 */
const getBusinessCardDealHistorySuccess = ({
  code,
  businessCardDealHistory,
  user,
  provider,
}) => {
  return action(
    constant.GET_BUSINESS_CARD_DEAL_HISTORY_SUCCESS,
    {
      code,
      businessCardDealHistory,
      user,
      provider
    }
  );
};
module.exports.getBusinessCardDealHistorySuccess = getBusinessCardDealHistorySuccess;

/**
 * @override Yêu cầu chấp nhận lịch sử giao dịch theo id
 *
 * @param   {ObjectId} businessCardDealHistoryID
 * @param   {Object} accept
 * @returns {Object}
 */
const acceptBusinessCardsDealHistoryIDRequest = (businessCardDealHistoryID, accept) => {
  return action(constant.ACCEPT_BUSINESS_CARDS_DEAL_HISTORY_ID_REQUEST, { businessCardDealHistoryID, accept });
};
module.exports.acceptBusinessCardsDealHistoryIDRequest = acceptBusinessCardsDealHistoryIDRequest;

/**
 * @override Chấp nhận lịch sử giao dịch theo id thành công
 *
 * @param   {Object}
 * @returns {Object}
 */
const acceptBusinessCardsDealHistoryIDSuccess = ({ code, businessCardDealHistory }) => {
  return action(constant.ACCEPT_BUSINESS_CARDS_DEAL_HISTORY_ID_SUCCESS, { code, businessCardDealHistory });
};
module.exports.acceptBusinessCardsDealHistoryIDSuccess = acceptBusinessCardsDealHistoryIDSuccess;

/**
 * @override Chấp nhận lịch sử giao dịch theo id thất bại
 *
 * @param   {Object}
 * @returns {Object}
 */
const acceptBusinessCardsDealHistoryIDFailed = ({ code }) => {
  return action(constant.ACCEPT_BUSINESS_CARDS_DEAL_HISTORY_ID_FAILED, { code });
};
module.exports.acceptBusinessCardsDealHistoryIDFailed = acceptBusinessCardsDealHistoryIDFailed;

/**
 * @override Yêu cầu lấy mã thông báo lịch sử giao dịch theo id
 *
 * @param   {ObjectId} businessCardDealHistoryID
 * @returns {Object}
 */
const getTokenBusinessCardsDealHistoryIDRequest = (businessCardDealHistoryID) => {
  return action(constant.GET_TOKEN_BUSINESS_CARDS_DEAL_HISTORY_ID_REQUEST, { businessCardDealHistoryID });
};
module.exports.getTokenBusinessCardsDealHistoryIDRequest = getTokenBusinessCardsDealHistoryIDRequest;

/**
 * @override Lấy mã thông báo lịch sử giao dịch theo id thành công
 *
 * @param   {Object}
 * @returns {Object}
 */
const getTokenBusinessCardsDealHistoryIDSuccess = ({ code, businessCardDealHistory }) => {
  return action(constant.GET_TOKEN_BUSINESS_CARDS_DEAL_HISTORY_ID_SUCCESS, { code, businessCardDealHistory });
};
module.exports.getTokenBusinessCardsDealHistoryIDSuccess = getTokenBusinessCardsDealHistoryIDSuccess;

/**
 * @override Lấy mã thông báo lịch sử giao dịch theo id thất bại
 *
 * @param   {Object}
 * @returns {Object}
 */
const getTokenBusinessCardsDealHistoryIDFailed = ({ code }) => {
  return action(constant.GET_TOKEN_BUSINESS_CARDS_DEAL_HISTORY_ID_FAILED, { code });
};
module.exports.getTokenBusinessCardsDealHistoryIDFailed = getTokenBusinessCardsDealHistoryIDFailed;

/**
 * @override Yêu cầu xác thực mã thông báo lịch sử giao dịch theo id
 *
 * @param   {ObjectId} businessCardDealHistoryID
 * @param   {String} token
 * @returns {Object}
 */
const validateTokenBusinessCardsDealHistoryIDRequest = (businessCardDealHistoryID, token) => {
  return action(constant.VALIDATE_TOKEN_BUSINESS_CARDS_DEAL_HISTORY_ID_REQUEST, { businessCardDealHistoryID, token });
};
module.exports.validateTokenBusinessCardsDealHistoryIDRequest = validateTokenBusinessCardsDealHistoryIDRequest;

/**
 * @override Xác thực mã thông báo lịch sử giao dịch theo id thành công
 *
 * @param   {Object}
 * @returns {Object}
 */
const validateTokenBusinessCardsDealHistoryIDSuccess = ({ code, businessCardDealHistory }) => {
  return action(constant.VALIDATE_TOKEN_BUSINESS_CARDS_DEAL_HISTORY_ID_SUCCESS, { code, businessCardDealHistory });
};
module.exports.validateTokenBusinessCardsDealHistoryIDSuccess = validateTokenBusinessCardsDealHistoryIDSuccess;

/**
 * @override Xác thực mã thông báo lịch sử giao dịch theo id thất bại
 *
 * @param   {Object}
 * @returns {Object}
 */
const validateTokenBusinessCardsDealHistoryIDFailed = ({ code }) => {
  return action(constant.VALIDATE_TOKEN_BUSINESS_CARDS_DEAL_HISTORY_ID_FAILED, { code });
};
module.exports.validateTokenBusinessCardsDealHistoryIDFailed = validateTokenBusinessCardsDealHistoryIDFailed;

/**
 * @override Yêu cầu tố cáo lịch sử giao dịch theo id
 *
 * @param   {ObjectId} businessCardDealHistoryID
 * @param   {String} token
 * @returns {Object}
 */
const reportBusinessCardsDealHistoryIDRequest = (businessCardDealHistoryID, report) => {
  return action(constant.REPORT_BUSINESS_CARDS_DEAL_HISTORY_ID_REQUEST, { businessCardDealHistoryID, report });
};
module.exports.reportBusinessCardsDealHistoryIDRequest = reportBusinessCardsDealHistoryIDRequest;

/**
 * @override Tố cáo lịch sử giao dịch theo id thành công
 *
 * @param   {Object}
 * @returns {Object}
 */
const reportBusinessCardsDealHistoryIDSuccess = ({ code, businessCardDealHistory }) => {
  return action(constant.REPORT_BUSINESS_CARDS_DEAL_HISTORY_ID_SUCCESS, { code, businessCardDealHistory });
};
module.exports.reportBusinessCardsDealHistoryIDSuccess = reportBusinessCardsDealHistoryIDSuccess;

/**
 * @override Tố cáo lịch sử giao dịch theo id thất bại
 *
 * @param   {Object}
 * @returns {Object}
 */
const reportBusinessCardsDealHistoryIDFailed = ({ code }) => {
  return action(constant.REPORT_BUSINESS_CARDS_DEAL_HISTORY_ID_FAILED, { code });
};
module.exports.reportBusinessCardsDealHistoryIDFailed = reportBusinessCardsDealHistoryIDFailed;

/**
 * @override Lắng nghe lịch sử giao dịch theo id
 *
 * @param   {Object}
 * @returns {Object}
 */
const listenBusinessCardsDealHistory = (fragment, businessCardDealHistoryID) => {
  return action(constant.LISTEN_BUSINESS_CARDS_DEAL_HISTORY, { fragment, businessCardDealHistoryID });
};
module.exports.listenBusinessCardsDealHistory = listenBusinessCardsDealHistory;

/**
 * @override Thiết lặp lại cửa hàng
 *
 * @param   {Object}
 */
const resetBusinessCardsDealHistoryStore = () => {
  return action(constant.RESERT_BUSINESS_CARDS_DEAL_HISTORY_STORE);
};
module.exports.resetBusinessCardsDealHistoryStore = resetBusinessCardsDealHistoryStore;
