const constant = require('../constants/business-card.client.constant');
const constantCommon = require('../../../../commons/utils/constant');
const {
  findIndex,
  isArray,
  assignIn
} = require('lodash');
const { fromJS } = require('immutable');
const { responseState } = require('../../../core/client/utils/middleware');
const { ResultEnum } = require('../../../core/client/constants/core.client.constant');

const initialState = fromJS({
  create: responseState(),
  delete: responseState(),
  reUpload: responseState(),
  self: responseState(),
  changes: {
    image: {},
    address: responseState(),
    descriptions: responseState(),
    status: responseState(),
  },
  detail: responseState(),
  list: responseState(),
  actions: {
    likes: {},
    comments: {},
    deals: {},
    reports: {}
  }
});

const update = (state, action, handle) => {
  if (state.getIn(['self', 'data', '_id']) === action.businessCardID) {
    state = handle(state, action, ['self', 'data']);
  }

  if (state.getIn(['detail', 'data', '_id']) === action.businessCardID) {
    state = handle(state, action, ['detail', 'data']);
  }

  const indexTopNewUp = findIndex(
    state.getIn(['list', 'data', 'topNewUp']),
    (businessCard) => {
      return businessCard._id === action.businessCardID;
    }
  );

  const indexTopActivity = findIndex(
    state.getIn(['list', 'data', 'topActivity']),
    (businessCard) => {
      return businessCard._id === action.businessCardID;
    }
  );

  const indexTopRank = findIndex(
    state.getIn(['list', 'data', 'topRank']),
    (businessCard) => {
      return businessCard._id === action.businessCardID;
    }
  );

  if (indexTopNewUp !== -1) {
    state = handle(state, action, ['list', 'data', 'topNewUp'], indexTopNewUp);
  }

  if (indexTopActivity !== -1) {
    state = handle(state, action, ['list', 'data', 'topActivity'], indexTopActivity);
  }

  if (indexTopRank !== -1) {
    state = handle(state, action, ['list', 'data', 'topRank'], indexTopRank);
  }

  return state;
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case constant.GET_PROFILE_BUSINESS_CARD_REQUEST: {
      return state.set(
        'self',
        { result: ResultEnum.wait }
      );
    }
    case constant.GET_PROFILE_BUSINESS_CARD_SUCCESS: {
      return state.set(
        'self',
        {
          result: ResultEnum.success,
          code: action.code,
          data: action.businessCard
        }
      );
    }
    case constant.GET_PROFILE_BUSINESS_CARD_FAILED: {
      return state.set(
        'self',
        {
          result: ResultEnum.failed,
          code: action.code
        }
      );
    }
    case constant.CREATE_BUSINESS_CARD_REQUEST: {
      return state.set(
        'create',
        { result: ResultEnum.wait }
      );
    }
    case constant.CREATE_BUSINESS_CARD_SUCCESS: {
      return state
        .setIn(['self', 'data'], action.businessCard)
        .set(
          'create',
          {
            result: ResultEnum.success,
            code: action.code
          }
        );
    }
    case constant.CREATE_BUSINESS_CARD_FAILED: {
      return state.set(
        'create',
        {
          result: ResultEnum.failed,
          code: action.code
        }
      );
    }
    case constant.DELETE_BUSINESS_CARD_REQUEST: {
      return state.set(
        'delete',
        { result: ResultEnum.wait }
      );
    }
    case constant.DELETE_BUSINESS_CARD_SUCCESS: {
      return state
        .set(
          'delete',
          {
            result: ResultEnum.success,
            code: action.code
          }
        );
    }
    case constant.DELETE_BUSINESS_CARD_FAILED: {
      return state.set(
        'delete',
        {
          result: ResultEnum.failed,
          code: action.code
        }
      );
    }
    case constant.UPLOAD_IMAGE_BUSINESS_CARD_REQUEST: {
      return state.setIn(
        ['changes', 'image', action.image.name],
        {
          result: ResultEnum.wait,
          type: constantCommon.UPLOAD
        }
      );
    }
    case constant.UPLOAD_IMAGE_BUSINESS_CARD_SUCCESS: {
      return state
        .updateIn(
          ['self', 'data'],
          (data) => {
            data.images = data.images.concat(action.image);
            data.updateDate = action.updateDate;

            return data;
          }
        )
        .setIn(
          ['changes', 'image', action.image.name],
          {
            result: ResultEnum.success,
            code: action.code
          }
        );
    }
    case constant.UPLOAD_IMAGE_BUSINESS_CARD_FAILED: {
      return state
        .setIn(
          ['changes', 'image', action.image.name],
          {
            result: ResultEnum.failed,
            code: action.code
          }
        );
    }
    case constant.DELETE_IMAGE_BUSINESS_CARD_REQUEST: {
      return state
        .setIn(
          ['changes', 'image', action.publicID],
          {
            result: ResultEnum.wait,
            type: constantCommon.DELETE
          }
        );
    }
    case constant.DELETE_IMAGE_BUSINESS_CARD_SUCCESS: {
      return state
        .updateIn(
          ['self', 'data'],
          (data) => {
            data.images = action.images;
            data.updateDate = action.updateDate;

            return data;
          }
        )
        .setIn(
          ['changes', 'image', action.publicID],
          {
            result: ResultEnum.success,
            code: action.code
          }
        );
    }
    case constant.DELETE_IMAGE_BUSINESS_CARD_FAILED: {
      return state
        .setIn(
          ['changes', 'image', action.publicID],
          {
            result: ResultEnum.failed,
            code: action.code
          }
        );
    }
    case constant.LIKE_BUSINESS_CARD_REQUEST: {
      return state.setIn(
        ['actions', 'likes', action.businessCardID],
        {
          nextType: constantCommon.LIKE,
          result: ResultEnum.wait,
        }
      );
    }
    case constant.LIKE_BUSINESS_CARD_SUCCESS: {
      let _state = state;

      const handle = (state, action, type, index) => {
        return state
          .setIn(
            ['actions', 'likes', action.businessCardID],
            {
              result: ResultEnum.success,
              code: action.code,
              nextType: constantCommon.UNLIKE,
            }
          )
          .updateIn(
            type,
            (value) => {
              if (isArray(value)) {
                value[index].likes.count += 1;
                value[index].likes.isSelf = true;
              } else {
                value.likes.count += 1;
                value.likes.isSelf = true;
              }

              return value;
            }
          );
      };

      _state = update(_state, action, handle);

      return _state;
    }
    case constant.LIKE_BUSINESS_CARD_FAILED: {
      return state.setIn(
        ['actions', 'likes', action.businessCardID],
        {
          nextType: constantCommon.LIKE,
          result: ResultEnum.failed,
          code: action.code
        }
      );
    }
    case constant.UNLIKE_BUSINESS_CARD_REQUEST: {
      return state.setIn(
        ['actions', 'likes', action.businessCardID],
        {
          nextType: constantCommon.UNLIKE,
          result: ResultEnum.wait,
        }
      );
    }
    case constant.UNLIKE_BUSINESS_CARD_SUCCESS: {
      let _state = state;

      const handle = (state, action, type, index) => {
        return _state
          .setIn(
            ['actions', 'likes', action.businessCardID],
            {
              result: ResultEnum.success,
              code: action.code,
              nextType: constantCommon.LIKE,
            }
          )
          .updateIn(
            type,
            (value) => {
              if (isArray(value)) {
                value[index].likes.count -= 1;
                value[index].likes.isSelf = false;
              } else {
                value.likes.count -= 1;
                value.likes.isSelf = false;
              }

              return value;
            }
          );
      };

      _state = update(_state, action, handle);

      return _state;
    }
    case constant.UNLIKE_BUSINESS_CARD_FAILED: {
      return state.setIn(
        ['actions', 'likes', action.businessCardID],
        {
          nextType: constantCommon.UNLIKE,
          result: ResultEnum.failed,
          code: action.code
        }
      );
    }
    case constant.GET_COMMENTS_BUSINESS_CARD_REQUEST: {
      const get = {
        result: ResultEnum.wait,
        max: action.max
      };

      return state.getIn(['actions', 'comments', action.businessCardID])
        ? state.setIn(['actions', 'comments', action.businessCardID, 'get'], get)
        : state.setIn(['actions', 'comments', action.businessCardID], { get });
    }
    case constant.GET_COMMENTS_BUSINESS_CARD_SUCCESS: {
      let _state = state;

      const handle = (state, action, type, index) => {
        return state
          .setIn(
            ['actions', 'comments', action.businessCardID, 'get'],
            {
              result: ResultEnum.success,
              code: action.code,
              max: action.max
            }
          )
          .updateIn(
            type,
            (value) => {
              if (isArray(value)) {
                value[index].comments.data = action.comments.concat(value[index].comments.data);
              } else {
                value.comments.data = action.comments.concat(value.comments.data);
              }

              return value;
            }
          );
      };

      _state = update(_state, action, handle);

      return _state;
    }
    case constant.GET_COMMENTS_BUSINESS_CARD_FAILED: {
      return state.updateIn(
        ['actions', 'comments', action.businessCardID, 'get'],
        (get) => {
          return {
            result: ResultEnum.failed,
            code: action.code,
            max: get.max,
          };
        }
      );
    }
    case constant.COMMENT_BUSINESS_CARD_REQUEST: {
      const post = {
        result: ResultEnum.wait,
      };

      return state.getIn(['actions', 'comments', action.businessCardID])
        ? state.setIn(['actions', 'comments', action.businessCardID, 'post'], post)
        : state.setIn(['actions', 'comments', action.businessCardID], { post });
    }
    case constant.COMMENT_BUSINESS_CARD_SUCCESS: {
      let _state = state;

      const handle = (state, action, type, index) => {
        return state
          .setIn(
            ['actions', 'comments', action.businessCardID, 'post'],
            {
              result: ResultEnum.success,
              code: action.code,
              commentID: action.comment._id
            }
          )
          .updateIn(
            ['actions', 'comments', action.businessCardID, 'get', 'max'],
            (max) => {
              if (max === -1) {
                max = 0;
              }

              return max;
            }
          )
          .updateIn(
            type,
            (value) => {
              if (isArray(value)) {
                value[index].comments.data = value[index].comments.data.concat(action.comment);
                value[index].comments.count += 1;
              } else {
                value.comments.data = value.comments.data.concat(action.comment);
                value.comments.count += 1;
              }

              return value;
            }
          );
      };

      _state = update(_state, action, handle);

      return _state;
    }
    case constant.COMMENT_BUSINESS_CARD_FAILED: {
      return state.setIn(
        ['actions', 'comments', action.businessCardID, 'post'],
        {
          result: ResultEnum.failed,
          code: action.code
        }
      );
    }
    case constant.CHANGE_COMMENT_BUSINESS_CARD_REQUEST: {
      const put = {
        result: ResultEnum.wait,
        commentID: action.commentID
      };

      return state.getIn(['actions', 'comments', action.businessCardID])
        ? state.setIn(['actions', 'comments', action.businessCardID, 'put'], put)
        : state.setIn(['actions', 'comments', action.businessCardID], { put });
    }
    case constant.CHANGE_COMMENT_BUSINESS_CARD_SUCCESS: {
      let _state = state;

      const handle = (state, action, type, index) => {
        return state
          .setIn(
            ['actions', 'comments', action.businessCardID, 'put'],
            {
              result: ResultEnum.success,
              code: action.code,
              commentID: action.commentID
            }
          )
          .updateIn(
            type,
            (value) => {
              if (isArray(value)) {
                const commentIndex = findIndex(value[index].comments.data, (data) => {
                  return data.comment._id === action.commentID;
                });

                value[index].comments.data[commentIndex].comment = assignIn(
                  value[index].comments.data[commentIndex].comment,
                  action.comment
                );
              } else {
                const commentIndex = findIndex(value.comments.data, (data) => {
                  return data.comment._id === action.commentID;
                });

                value.comments.data[commentIndex].comment = assignIn(
                  value.comments.data[commentIndex].comment,
                  action.comment
                );
              }

              return value;
            }
          );
      };

      _state = update(_state, action, handle);

      return _state;
    }
    case constant.CHANGE_COMMENT_BUSINESS_CARD_FAILED: {
      return state
        .setIn(
          ['actions', 'comments', action.businessCardID, 'put'],
          {
            result: ResultEnum.failed,
            code: action.code,
            commentID: action.commentID
          }
        );
    }
    case constant.DELETE_COMMENT_BUSINESS_CARD_REQUEST: {
      const deletee = {
        result: ResultEnum.wait,
        commentID: action.commentID
      };

      return state.getIn(['actions', 'comments', action.businessCardID])
        ? state.setIn(['actions', 'comments', action.businessCardID, 'delete'], deletee)
        : state.setIn(['actions', 'comments', action.businessCardID], { delete: deletee });
    }
    case constant.DELETE_COMMENT_BUSINESS_CARD_SUCCESS: {
      let _state = state;

      const handle = (state, action, type, index) => {
        return state
          .setIn(
            ['actions', 'comments', action.businessCardID, 'delete'],
            {
              result: ResultEnum.success,
              code: action.code,
              commentID: action.commentID
            }
          )
          .updateIn(
            ['actions', 'comments', action.businessCardID, 'get', 'max'],
            (max) => {
              if (max === -1) {
                max = 0;
              }

              return max;
            }
          )
          .updateIn(
            type,
            (value) => {
              if (isArray(value)) {
                const commentIndex = findIndex(value[index].comments.data, (data) => {
                  return data.comment._id === action.commentID;
                });

                value[index].comments.data.splice(commentIndex, 1);
                value[index].comments.count -= 1;
              } else {
                const commentIndex = findIndex(value.comments.data, (data) => {
                  return data.comment._id === action.commentID;
                });

                value.comments.data.splice(commentIndex, 1);
                value.comments.count -= 1;
              }

              return value;
            }
          );
      };

      _state = update(_state, action, handle);

      return _state;
    }
    case constant.DELETE_COMMENT_BUSINESS_CARD_FAILED: {
      let comments = state.getIn(['actions', 'comments', action.businessCardID]);
      comments = comments.set(
        'delete',
        {
          result: ResultEnum.failed,
          code: action.code,
          commentID: action.commentID
        }
      );

      return state.setIn(['actions', 'comments', action.businessCardID], comments);
    }
    case constant.DEAL_BUSINESS_CARD_REQUEST: {
      return state.setIn(
        ['actions', 'deals', action.businessCardID],
        { result: ResultEnum.wait }
      );
    }
    case constant.DEAL_BUSINESS_CARD_SUCCESS: {
      return state.setIn(
        ['actions', 'deals', action.businessCardID],
        {
          result: ResultEnum.success,
          code: action.code,
        }
      );
    }
    case constant.DEAL_BUSINESS_CARD_FAILED: {
      return state.setIn(
        ['actions', 'deals', action.businessCardID],
        {
          result: ResultEnum.failed,
          code: action.code,
        }
      );
    }
    case constant.REPORT_BUSINESS_CARD_REQUEST: {
      return state.setIn(
        ['actions', 'reports', action.businessCardID],
        { result: ResultEnum.wait }
      );
    }
    case constant.REPORT_BUSINESS_CARD_SUCCESS: {
      return state.setIn(
        ['actions', 'reports', action.businessCardID],
        {
          result: ResultEnum.success,
          code: action.code,
        }
      );
    }
    case constant.REPORT_BUSINESS_CARD_FAILED: {
      return state.setIn(
        ['actions', 'reports', action.businessCardID],
        {
          result: ResultEnum.failed,
          code: action.code
        }
      );
    }
    case constant.CHANGE_DESCRIPTIONS_BUSINESS_CARD_REQUEST: {
      return state.setIn(
        ['changes', 'descriptions'],
        { result: ResultEnum.wait }
      );
    }
    case constant.CHANGE_DESCRIPTIONS_BUSINESS_CARD_SUCCESS: {
      return state
        .updateIn(
          ['self', 'data'],
          (data) => {
            data.descriptions = action.descriptions;
            data.updateDate = action.updateDate;

            return data;
          }
        )
        .setIn(
          ['changes', 'descriptions'],
          {
            result: ResultEnum.success,
            code: action.code
          }
        );
    }
    case constant.CHANGE_DESCRIPTIONS_BUSINESS_CARD_FAILED: {
      return state.setIn(
        ['changes', 'descriptions'],
        {
          result: ResultEnum.failed,
          code: action.code
        }
      );
    }
    case constant.CHANGE_ADDRESS_BUSINESS_CARD_REQUEST: {
      return state.setIn(
        ['changes', 'address'],
        { result: ResultEnum.wait }
      );
    }
    case constant.CHANGE_ADDRESS_BUSINESS_CARD_SUCCESS: {
      return state
        .updateIn(
          ['self', 'data'],
          (data) => {
            data.address = action.address;
            data.updateDate = action.updateDate;

            return data;
          }
        )
        .setIn(
          ['changes', 'address'],
          {
            result: ResultEnum.success,
            code: action.code
          }
        );
    }
    case constant.CHANGE_ADDRESS_BUSINESS_CARD_FAILED: {
      return state.setIn(
        ['changes', 'address'],
        {
          result: ResultEnum.failed,
          code: action.code
        }
      );
    }
    case constant.CHANGE_STATUS_BUSINESS_CARD_REQUEST: {
      return state.setIn(
        ['changes', 'status'],
        { result: ResultEnum.wait }
      );
    }
    case constant.CHANGE_STATUS_BUSINESS_CARD_SUCCESS: {
      return state
        .setIn(['self', 'data', 'status'], action.status)
        .setIn(['self', 'data', 'updateDate'], action.updateDate)
        .setIn(
          ['changes', 'status'],
          {
            result: ResultEnum.success,
            code: action.code
          }
        );
    }
    case constant.CHANGE_STATUS_BUSINESS_CARD_FAILED: {
      return state.setIn(
        ['changes', 'status'],
        {
          result: ResultEnum.failed,
          code: action.code
        }
      );
    }
    case constant.REUPLOAD_BUSINESS_CARD_REQUEST: {
      return state.set(
        'reUpload',
        { result: ResultEnum.wait }
      );
    }
    case constant.REUPLOAD_BUSINESS_CARD_SUCCESS: {
      return state
        .setIn(['self', 'data', 'reUpload'], action.reUpload)
        .set(
          'reUpload',
          {
            result: ResultEnum.success,
            code: action.code
          }
        );
    }
    case constant.REUPLOAD_BUSINESS_CARD_FAILED: {
      return state.set(
        'reUpload',
        {
          result: ResultEnum.failed,
          code: action.code
        }
      );
    }
    case constant.GET_BUSINESS_CARD_ID_REQUEST: {
      return state.set(
        'detail',
        { result: ResultEnum.wait }
      );
    }
    case constant.GET_BUSINESS_CARD_ID_SUCCESS: {
      return state.set(
        'detail',
        {
          result: ResultEnum.success,
          code: action.code,
          data: action.businessCard
        }
      );
    }
    case constant.GET_BUSINESS_CARD_ID_FAILED: {
      return state.set(
        'detail',
        {
          result: ResultEnum.failed,
          code: action.code
        }
      );
    }
    case constant.GET_BUSINESS_CARDS_REQUEST: {
      return state
        .setIn(['list', 'result'], ResultEnum.wait)
        .setIn(['list', 'skip'], action.skip)
        .setIn(['list', 'fragment'], action.fragment)
        .setIn(['list', 'reload'], action.reload)
        .setIn(['list', 'code'], undefined);
    }
    case constant.GET_BUSINESS_CARDS_SUCCESS: {
      let _state = state
        .setIn(['list', 'result'], ResultEnum.success)
        .setIn(['list', 'skip'], action.skip)
        .setIn(['list', 'code'], action.code);

      if (_state.getIn(['list', 'reload'])) {
        _state = _state.setIn(
          ['list', 'data'],
          action.businessCards
        );
      } else {
        _state = _state.updateIn(
          ['list', 'data'],
          (data) => {
            let _data = data;

            if (data) {
              _data.topRank = _data.topRank.concat(action.businessCards.topRank);
            } else {
              _data = action.businessCards;
            }

            return _data;
          }
        );
      }

      return _state;
    }
    case constant.GET_BUSINESS_CARDS_FAILED: {
      if (state.getIn(['list', 'reload'])) {
        return state.set(
          'list',
          {
            result: ResultEnum.failed,
            code: action.code,
            data: []
          }
        );
      }

      return state
        .setIn(['list', 'result'], ResultEnum.failed)
        .setIn(['list', 'code'], action.code);
    }
    case constant.RESERT_BUSINESS_CARD_STORE: {
      return initialState;
    }
    default:
      return state;
  }
};
module.exports = reducer;
