const constant = require('../constants/deal-history.client.constant');
const commonConstant = require('../../../../commons/utils/constant');
const { fromJS } = require('immutable');
const { responseState } = require('../../../core/client/utils/middleware');
const { ResultEnum } = require('../../../core/client/constants/core.client.constant');

const initialState = fromJS({
  self: responseState({
    data: [],
    count: 0
  }),
  detail: responseState(),
  actions: {
    get: responseState(),
    accept: responseState(),
    token: responseState(),
    verify: responseState(),
    report: responseState()
  }
});

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case constant.GET_BUSINESS_CARDS_DEAL_HISTORYS_REQUEST: {
      return state
        .setIn(['self', 'result'], ResultEnum.wait)
        .setIn(['self', 'skip'], action.skip)
        .setIn(['self', 'fragment'], action.fragment)
        .setIn(['self', 'reload'], action.reload)
        .setIn(['self', 'code'], undefined);
    }
    case constant.GET_BUSINESS_CARDS_DEAL_HISTORYS_SUCCESS: {
      let _state = state
        .setIn(['self', 'result'], ResultEnum.success)
        .setIn(['self', 'skip'], action.skip)
        .setIn(['self', 'code'], action.code)
        .setIn(['self', 'count'], action.count);

      const businessCardDealHistorys = action.businessCardDealHistorys.sort((a, b) => {
        return a.businessCardDealHistory.initDate - b.businessCardDealHistory.initDate;
      });

      if (_state.getIn(['self', 'reload'])) {
        _state = _state.setIn(['self', 'data'], businessCardDealHistorys);
      } else {
        _state = _state.updateIn(
          ['self', 'data'],
          (data) => {
            return data.concat(businessCardDealHistorys);
          }
        );
      }

      return _state;
    }
    case constant.GET_BUSINESS_CARDS_DEAL_HISTORYS_FAILED: {
      if (state.getIn(['self', 'reload'])) {
        return state.set(
          'self',
          {
            result: ResultEnum.failed,
            code: action.code,
            data: [],
            count: 0
          }
        );
      }

      return state
        .setIn(['self', 'result'], ResultEnum.failed)
        .setIn(['self', 'code'], action.code);
    }
    case constant.GET_BUSINESS_CARDS_DEAL_HISTORY_ID_REQUEST: {
      return state.setIn(['detail', 'result'], ResultEnum.wait);
    }
    case constant.GET_BUSINESS_CARD_DEAL_HISTORY_ID_SUCCESS: {
      return state.set(
        'detail',
        {
          result: ResultEnum.success,
          code: action.code,
          data: {
            businessCardDealHistory: action.businessCardDealHistory,
            user: action.user,
            provider: action.provider
          }
        }
      );
    }
    case constant.GET_BUSINESS_CARD_DEAL_HISTORY_ID_FAILED: {
      return state.set(
        'detail',
        {
          result: ResultEnum.failed,
          code: action.code
        }
      );
    }
    case constant.GET_BUSINESS_CARD_DEAL_HISTORY_REQUEST: {
      return state.setIn(
        ['actions', 'get'],
        {
          result: ResultEnum.wait,
          reload: action.reload,
        }
      );
    }
    case constant.GET_BUSINESS_CARD_DEAL_HISTORY_SUCCESS: {
      if (state.getIn(['actions', 'get', 'reload'])) {
        const businessCardDealHistoryIndex = state.getIn(['self', 'data']).findIndex((value) => {
          return value && value.businessCardDealHistory ? value.businessCardDealHistory._id === action.businessCardDealHistory._id : false;
        });

        return state
          .setIn(
            ['actions', 'get'],
            {
              result: ResultEnum.success,
              code: action.code,
            }
          )
          .updateIn(
            ['self', 'count'],
            (count) => {
              return action.businessCardDealHistory.result.is === commonConstant.WAIT ? count : count - 1;
            }
          )
          .setIn(
            ['self', 'data', businessCardDealHistoryIndex],
            {
              businessCardDealHistory: action.businessCardDealHistory,
              user: action.user,
              provider: action.provider
            }
          );
      }

      return state
        .updateIn(
          ['self', 'skip'],
          (skip) => {
            return skip ? skip + 1 : 1;
          }
        )
        .updateIn(
          ['self', 'count'],
          (count) => {
            return count + 1;
          }
        )
        .updateIn(
          ['self', 'data'],
          (data) => {
            return fromJS(
              [{
                businessCardDealHistory: action.businessCardDealHistory,
                user: action.user,
                provider: action.provider
              }]
            )
              .concat(data);
          }
        );
    }
    case constant.ACCEPT_BUSINESS_CARDS_DEAL_HISTORY_ID_REQUEST: {
      return state.setIn(
        ['actions', 'accept'],
        { result: ResultEnum.wait }
      );
    }
    case constant.ACCEPT_BUSINESS_CARDS_DEAL_HISTORY_ID_SUCCESS: {
      return state
        .setIn(['detail', 'data', 'businessCardDealHistory'], action.businessCardDealHistory)
        .setIn(
          ['actions', 'accept'],
          {
            result: ResultEnum.success,
            code: action.code
          }
        );
    }
    case constant.ACCEPT_BUSINESS_CARDS_DEAL_HISTORY_ID_FAILED: {
      return state.setIn(
        ['actions', 'accept'],
        {
          result: ResultEnum.failed,
          code: action.code
        }
      );
    }
    case constant.GET_TOKEN_BUSINESS_CARDS_DEAL_HISTORY_ID_REQUEST: {
      return state.setIn(
        ['actions', 'token'],
        { result: ResultEnum.wait }
      );
    }
    case constant.GET_TOKEN_BUSINESS_CARDS_DEAL_HISTORY_ID_SUCCESS: {
      return state
        .setIn(['detail', 'data', 'businessCardDealHistory'], action.businessCardDealHistory)
        .setIn(
          ['actions', 'token'],
          {
            result: ResultEnum.success,
            code: action.code
          }
        );
    }
    case constant.GET_TOKEN_BUSINESS_CARDS_DEAL_HISTORY_ID_FAILED: {
      return state.setIn(
        ['actions', 'token'],
        {
          result: ResultEnum.failed,
          code: action.code
        }
      );
    }
    case constant.VALIDATE_TOKEN_BUSINESS_CARDS_DEAL_HISTORY_ID_REQUEST: {
      return state.setIn(
        ['actions', 'verify'],
        { result: ResultEnum.wait }
      );
    }
    case constant.VALIDATE_TOKEN_BUSINESS_CARDS_DEAL_HISTORY_ID_SUCCESS: {
      return state
        .setIn(['detail', 'data', 'businessCardDealHistory'], action.businessCardDealHistory)
        .setIn(
          ['actions', 'verify'],
          {
            result: ResultEnum.success,
            code: action.code
          }
        );
    }
    case constant.VALIDATE_TOKEN_BUSINESS_CARDS_DEAL_HISTORY_ID_FAILED: {
      return state.setIn(
        ['actions', 'verify'],
        {
          result: ResultEnum.failed,
          code: action.code
        }
      );
    }
    case constant.REPORT_BUSINESS_CARDS_DEAL_HISTORY_ID_REQUEST: {
      return state.setIn(
        ['actions', 'report'],
        { result: ResultEnum.wait }
      );
    }
    case constant.REPORT_BUSINESS_CARDS_DEAL_HISTORY_ID_SUCCESS: {
      return state
        .setIn(['detail', 'data', 'businessCardDealHistory'], action.businessCardDealHistory)
        .setIn(
          ['actions', 'report'],
          {
            result: ResultEnum.success,
            code: action.code
          }
        );
    }
    case constant.REPORT_BUSINESS_CARDS_DEAL_HISTORY_ID_FAILED: {
      return state.setIn(
        ['actions', 'report'],
        {
          result: ResultEnum.failed,
          code: action.code
        }
      );
    }
    case constant.RESERT_BUSINESS_CARDS_DEAL_HISTORY_STORE: {
      return initialState;
    }
    default:
      return state;
  }
};
module.exports = reducer;
