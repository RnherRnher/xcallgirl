const request = require('superagent');
const url = require('../../../../commons/modules/business-cards/datas/url.data');

const getProfileBusinessCard = () => {
  return new Promise((resovle, reject) => {
    request
      .get(url.BUSINESS_CARDS)
      .accept('application/json')
      .set('X-Requested-With', 'XMLHttpRequest')
      .end((err, res) => {
        if (err) {
          return reject(res);
        }

        return resovle(res);
      });
  });
};
module.exports.getProfileBusinessCard = getProfileBusinessCard;

const getBusinessCardID = ({ businessCardID }) => {
  return new Promise((resovle, reject) => {
    request
      .get(url.BUSINESS_CARDS_GET_ID.replace(':id', businessCardID))
      .accept('application/json')
      .set('X-Requested-With', 'XMLHttpRequest')
      .end((err, res) => {
        if (err) {
          return reject(res);
        }

        return resovle(res);
      });
  });
};
module.exports.getBusinessCardID = getBusinessCardID;

const createBusinessCard = ({ csrfToken, businessCard }) => {
  return new Promise((resovle, reject) => {
    request
      .post(url.BUSINESS_CARDS)
      .type('application/json')
      .accept('application/json')
      .set('X-Requested-With', 'XMLHttpRequest')
      .set('csrf-token', csrfToken)
      .send(businessCard)
      .end((err, res) => {
        if (err) {
          return reject(res);
        }

        return resovle(res);
      });
  });
};
module.exports.createBusinessCard = createBusinessCard;

const deleteBusinessCard = ({ csrfToken, deletee }) => {
  return new Promise((resovle, reject) => {
    request
      .delete(url.BUSINESS_CARDS)
      .type('application/json')
      .accept('application/json')
      .set('X-Requested-With', 'XMLHttpRequest')
      .set('csrf-token', csrfToken)
      .send(deletee)
      .end((err, res) => {
        if (err) {
          return reject(res);
        }

        return resovle(res);
      });
  });
};
module.exports.deleteBusinessCard = deleteBusinessCard;

const uploadImageBusinessCard = ({ csrfToken, image }) => {
  return new Promise((resovle, reject) => {
    request
      .post(url.BUSINESS_CARDS_IMAGE)
      .query({ name: image.name })
      .accept('application/json')
      .set('X-Requested-With', 'XMLHttpRequest')
      .set('csrf-token', csrfToken)
      .attach(image.name, image.blob)
      .end((err, res) => {
        if (err) {
          return reject(res);
        }

        return resovle(res);
      });
  });
};
module.exports.uploadImageBusinessCard = uploadImageBusinessCard;

const deleteImageBusinessCard = ({ csrfToken, publicID }) => {
  return new Promise((resovle, reject) => {
    request
      .delete(url.BUSINESS_CARDS_IMAGE)
      .query({ publicID })
      .accept('application/json')
      .set('X-Requested-With', 'XMLHttpRequest')
      .set('csrf-token', csrfToken)
      .end((err, res) => {
        if (err) {
          return reject(res);
        }

        return resovle(res);
      });
  });
};
module.exports.deleteImageBusinessCard = deleteImageBusinessCard;

const likeBusinessCard = ({ csrfToken, businessCardID }) => {
  return new Promise((resovle, reject) => {
    request
      .post(url.BUSINESS_CARDS_LIKE_ID.replace(':id', businessCardID))
      .accept('application/json')
      .set('X-Requested-With', 'XMLHttpRequest')
      .set('csrf-token', csrfToken)
      .end((err, res) => {
        if (err) {
          return reject(res);
        }

        return resovle(res);
      });
  });
};
module.exports.likeBusinessCard = likeBusinessCard;

const unLikeBusinessCard = ({ csrfToken, businessCardID }) => {
  return new Promise((resovle, reject) => {
    request
      .put(url.BUSINESS_CARDS_LIKE_ID.replace(':id', businessCardID))
      .accept('application/json')
      .set('X-Requested-With', 'XMLHttpRequest')
      .set('csrf-token', csrfToken)
      .end((err, res) => {
        if (err) {
          return reject(res);
        }

        return resovle(res);
      });
  });
};
module.exports.unLikeBusinessCard = unLikeBusinessCard;

const getCommentsBusinessCard = ({ businessCardID, max }) => {
  return new Promise((resovle, reject) => {
    request
      .get(url.BUSINESS_CARDS_COMMENT_ID.replace(':id', businessCardID))
      .query({ max })
      .accept('application/json')
      .set('X-Requested-With', 'XMLHttpRequest')
      .end((err, res) => {
        if (err) {
          return reject(res);
        }

        return resovle(res);
      });
  });
};
module.exports.getCommentsBusinessCard = getCommentsBusinessCard;

const commentBusinessCard = ({
  csrfToken,
  businessCardID,
  comment
}) => {
  return new Promise((resovle, reject) => {
    request
      .post(url.BUSINESS_CARDS_COMMENT_ID.replace(':id', businessCardID))
      .type('application/json')
      .accept('application/json')
      .set('X-Requested-With', 'XMLHttpRequest')
      .set('csrf-token', csrfToken)
      .send(comment)
      .end((err, res) => {
        if (err) {
          return reject(res);
        }

        return resovle(res);
      });
  });
};
module.exports.commentBusinessCard = commentBusinessCard;

const changeCommentBusinessCard = ({
  csrfToken,
  businessCardID,
  commentID,
  comment
}) => {
  return new Promise((resovle, reject) => {
    request
      .put(url.BUSINESS_CARDS_COMMENT_ID.replace(':id', businessCardID))
      .query({ commentID })
      .type('application/json')
      .accept('application/json')
      .set('X-Requested-With', 'XMLHttpRequest')
      .set('csrf-token', csrfToken)
      .send(comment)
      .end((err, res) => {
        if (err) {
          return reject(res);
        }

        return resovle(res);
      });
  });
};
module.exports.changeCommentBusinessCard = changeCommentBusinessCard;

const deleteCommentBusinessCard = ({
  csrfToken,
  businessCardID,
  commentID
}) => {
  return new Promise((resovle, reject) => {
    request
      .delete(url.BUSINESS_CARDS_COMMENT_ID.replace(':id', businessCardID))
      .query({ commentID })
      .accept('application/json')
      .set('X-Requested-With', 'XMLHttpRequest')
      .set('csrf-token', csrfToken)
      .end((err, res) => {
        if (err) {
          return reject(res);
        }

        return resovle(res);
      });
  });
};
module.exports.deleteCommentBusinessCard = deleteCommentBusinessCard;

const dealBusinessCard = ({
  csrfToken,
  businessCardID,
  deal
}) => {
  return new Promise((resovle, reject) => {
    request
      .post(url.BUSINESS_CARDS_DEAL_ID.replace(':id', businessCardID))
      .type('application/json')
      .accept('application/json')
      .set('X-Requested-With', 'XMLHttpRequest')
      .set('csrf-token', csrfToken)
      .send(deal)
      .end((err, res) => {
        if (err) {
          return reject(res);
        }

        return resovle(res);
      });
  });
};
module.exports.dealBusinessCard = dealBusinessCard;

const reportBusinessCard = ({
  csrfToken,
  businessCardID,
  report
}) => {
  return new Promise((resovle, reject) => {
    request
      .post(url.BUSINESS_CARDS_REPORT_ID.replace(':id', businessCardID))
      .type('application/json')
      .accept('application/json')
      .set('X-Requested-With', 'XMLHttpRequest')
      .set('csrf-token', csrfToken)
      .send(report)
      .end((err, res) => {
        if (err) {
          return reject(res);
        }

        return resovle(res);
      });
  });
};
module.exports.reportBusinessCard = reportBusinessCard;

const changeDescriptionsBusinessCard = ({ csrfToken, descriptions }) => {
  return new Promise((resovle, reject) => {
    request
      .post(url.BUSINESS_CARDS_DESCRIPTIONS)
      .type('application/json')
      .accept('application/json')
      .set('X-Requested-With', 'XMLHttpRequest')
      .set('csrf-token', csrfToken)
      .send(descriptions)
      .end((err, res) => {
        if (err) {
          return reject(res);
        }

        return resovle(res);
      });
  });
};
module.exports.changeDescriptionsBusinessCard = changeDescriptionsBusinessCard;

const changeAddressBusinessCard = ({ csrfToken, address }) => {
  return new Promise((resovle, reject) => {
    request
      .post(url.BUSINESS_CARDS_ADDRESS)
      .type('application/json')
      .accept('application/json')
      .set('X-Requested-With', 'XMLHttpRequest')
      .set('csrf-token', csrfToken)
      .send(address)
      .end((err, res) => {
        if (err) {
          return reject(res);
        }

        return resovle(res);
      });
  });
};
module.exports.changeAddressBusinessCard = changeAddressBusinessCard;

const changeStatusBusinessCard = ({ csrfToken }) => {
  return new Promise((resovle, reject) => {
    request
      .post(url.BUSINESS_CARDS_STATUS)
      .accept('application/json')
      .set('X-Requested-With', 'XMLHttpRequest')
      .set('csrf-token', csrfToken)
      .end((err, res) => {
        if (err) {
          return reject(res);
        }

        return resovle(res);
      });
  });
};
module.exports.changeStatusBusinessCard = changeStatusBusinessCard;

const reUploadBusinessCard = ({ csrfToken }) => {
  return new Promise((resovle, reject) => {
    request
      .post(url.BUSINESS_CARDS_REUPLOAD)
      .accept('application/json')
      .set('X-Requested-With', 'XMLHttpRequest')
      .set('csrf-token', csrfToken)
      .end((err, res) => {
        if (err) {
          return reject(res);
        }

        return resovle(res);
      });
  });
};
module.exports.reUploadBusinessCard = reUploadBusinessCard;

const getBusinessCards = ({ type, skip, extendSearch }) => {
  return new Promise((resovle, reject) => {
    request
      .get(url.BUSINESS_CARDS_SEARCH)
      .type('application/json')
      .accept('application/json')
      .set('X-Requested-With', 'XMLHttpRequest')
      .query({ type, skip, extendSearch })
      .end((err, res) => {
        if (err) {
          return reject(res);
        }

        return resovle(res);
      });
  });
};
module.exports.getBusinessCards = getBusinessCards;
