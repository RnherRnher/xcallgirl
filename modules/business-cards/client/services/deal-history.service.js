const request = require('superagent');
const url = require('../../../../commons/modules/business-cards/datas/url.data');

const getBusinessCardsDealHistorys = ({ type, skip }) => {
  return new Promise((resovle, reject) => {
    request
      .get(url.BUSINESS_CARDS_DEAL_HISTORYS_SEARCH)
      .accept('application/json')
      .set('X-Requested-With', 'XMLHttpRequest')
      .query({ type, skip })
      .end((err, res) => {
        if (err) {
          return reject(res);
        }

        return resovle(res);
      });
  });
};
module.exports.getBusinessCardsDealHistorys = getBusinessCardsDealHistorys;

const getBusinessCardDealHistoryID = ({ businessCardDealHistoryID, type }) => {
  return new Promise((resovle, reject) => {
    request
      .get(url.BUSINESS_CARDS_DEAL_HISTORYS_GET_ID.replace(':id', businessCardDealHistoryID))
      .accept('application/json')
      .set('X-Requested-With', 'XMLHttpRequest')
      .query({ type })
      .end((err, res) => {
        if (err) {
          return reject(res);
        }

        return resovle(res);
      });
  });
};
module.exports.getBusinessCardDealHistoryID = getBusinessCardDealHistoryID;

const acceptBusinessCardsDealHistoryID = ({
  csrfToken,
  businessCardDealHistoryID,
  accept
}) => {
  return new Promise((resovle, reject) => {
    request
      .post(url.BUSINESS_CARDS_DEAL_HISTORYS_ACCEPT_ID.replace(':id', businessCardDealHistoryID))
      .type('application/json')
      .accept('application/json')
      .set('X-Requested-With', 'XMLHttpRequest')
      .set('csrf-token', csrfToken)
      .send(accept)
      .end((err, res) => {
        if (err) {
          return reject(res);
        }

        return resovle(res);
      });
  });
};
module.exports.acceptBusinessCardsDealHistoryID = acceptBusinessCardsDealHistoryID;

const getTokenBusinessCardsDealHistoryID = ({ businessCardDealHistoryID }) => {
  return new Promise((resovle, reject) => {
    request
      .get(url.BUSINESS_CARDS_DEAL_HISTORYS_TOKEN_ID.replace(':id', businessCardDealHistoryID))
      .accept('application/json')
      .set('X-Requested-With', 'XMLHttpRequest')
      .end((err, res) => {
        if (err) {
          return reject(res);
        }

        return resovle(res);
      });
  });
};
module.exports.getTokenBusinessCardsDealHistoryID = getTokenBusinessCardsDealHistoryID;

const validateTokenBusinessCardsDealHistoryID = ({
  csrfToken,
  businessCardDealHistoryID,
  token
}) => {
  return new Promise((resovle, reject) => {
    request
      .post(url.BUSINESS_CARDS_DEAL_HISTORYS_TOKEN_ID.replace(':id', businessCardDealHistoryID))
      .type('application/json')
      .accept('application/json')
      .set('X-Requested-With', 'XMLHttpRequest')
      .set('csrf-token', csrfToken)
      .send(token)
      .end((err, res) => {
        if (err) {
          return reject(res);
        }

        return resovle(res);
      });
  });
};
module.exports.validateTokenBusinessCardsDealHistoryID = validateTokenBusinessCardsDealHistoryID;

const reportBusinessCardsDealHistoryID = ({
  csrfToken,
  businessCardDealHistoryID,
  report
}) => {
  return new Promise((resovle, reject) => {
    request
      .post(url.BUSINESS_CARDS_DEAL_HISTORYS_REPORT_ID.replace(':id', businessCardDealHistoryID))
      .type('application/json')
      .accept('application/json')
      .set('X-Requested-With', 'XMLHttpRequest')
      .set('csrf-token', csrfToken)
      .send(report)
      .end((err, res) => {
        if (err) {
          return reject(res);
        }

        return resovle(res);
      });
  });
};
module.exports.reportBusinessCardsDealHistoryID = reportBusinessCardsDealHistoryID;
