const { createSelector } = require('reselect');
const { makeSelect } = require('../../../core/client/utils/middleware');

const selectBusinessCardDealHistory = (state, props) => {
  return state.get('businessCardDealHistory');
};

const makeSelectSelfBusinessCardDealHistory = () => {
  return createSelector(
    selectBusinessCardDealHistory,
    (businessCardDealHistoryState) => {
      return makeSelect(businessCardDealHistoryState.get('self'));
    }
  );
};
module.exports.makeSelectSelfBusinessCardDealHistory = makeSelectSelfBusinessCardDealHistory;

const makeSelectDetailBusinessCardDealHistory = () => {
  return createSelector(
    selectBusinessCardDealHistory,
    (businessCardDealHistoryState) => {
      return makeSelect(businessCardDealHistoryState.get('detail'));
    }
  );
};
module.exports.makeSelectDetailBusinessCardDealHistory = makeSelectDetailBusinessCardDealHistory;

const makeSelectActionsBusinessCardDealHistory = (type) => {
  return createSelector(
    selectBusinessCardDealHistory,
    (businessCardDealHistoryState) => {
      return makeSelect(businessCardDealHistoryState.getIn(['actions', type]));
    }
  );
};
module.exports.makeSelectActionsBusinessCardDealHistory = makeSelectActionsBusinessCardDealHistory;

const makeSelectCountDealHistorys = () => {
  return createSelector(
    selectBusinessCardDealHistory,
    (businessCardDealHistoryState) => {
      return makeSelect(businessCardDealHistoryState.getIn(['self', 'count']));
    }
  );
};
module.exports.makeSelectCountDealHistorys = makeSelectCountDealHistorys;
