const { createSelector } = require('reselect');
const { makeSelect } = require('../../../core/client/utils/middleware');

const selecBusinessCard = (state, props) => {
  return state.get('businessCard');
};

const makeSelectSelfBusinessCard = () => {
  return createSelector(
    selecBusinessCard,
    (businessCardState) => {
      return makeSelect(businessCardState.get('self'));
    }
  );
};
module.exports.makeSelectSelfBusinessCard = makeSelectSelfBusinessCard;

const makeSelectDetailBusinessCard = () => {
  return createSelector(
    selecBusinessCard,
    (businessCardState) => {
      return makeSelect(businessCardState.get('detail'));
    }
  );
};
module.exports.makeSelectDetailBusinessCard = makeSelectDetailBusinessCard;

const makeSelectIsOwner = (businessCardID) => {
  return createSelector(
    selecBusinessCard,
    (businessCardState) => {
      return makeSelect(businessCardState.getIn(['self', 'data', '_id'])) === businessCardID;
    }
  );
};
module.exports.makeSelectIsOwner = makeSelectIsOwner;

const makeSelectChangesBusinessCard = (type) => {
  return createSelector(
    selecBusinessCard,
    (businessCardState) => {
      if (type) {
        return makeSelect(businessCardState.getIn(['changes', type]));
      }

      return makeSelect(businessCardState.get('changes'));
    }
  );
};
module.exports.makeSelectChangesBusinessCard = makeSelectChangesBusinessCard;

const makeSelectLikesActionBusinessCard = (businessCardID) => {
  return createSelector(
    selecBusinessCard,
    (businessCardState) => {
      return makeSelect(businessCardState.getIn(['actions', 'likes', businessCardID])) || null;
    }
  );
};
module.exports.makeSelectLikesActionBusinessCard = makeSelectLikesActionBusinessCard;

const makeSelectCommentsActionBusinessCard = (businessCardID) => {
  return createSelector(
    selecBusinessCard,
    (businessCardState) => {
      return makeSelect(businessCardState.getIn(['actions', 'comments', businessCardID])) || null;
    }
  );
};
module.exports.makeSelectCommentsActionBusinessCard = makeSelectCommentsActionBusinessCard;

const makeSelectDealsActionBusinessCard = (businessCardID) => {
  return createSelector(
    selecBusinessCard,
    (businessCardState) => {
      return makeSelect(businessCardState.getIn(['actions', 'deals', businessCardID])) || null;
    }
  );
};
module.exports.makeSelectDealsActionBusinessCard = makeSelectDealsActionBusinessCard;

const makeSelectReportsActionBusinessCard = (businessCardID) => {
  return createSelector(
    selecBusinessCard,
    (businessCardState) => {
      return makeSelect(businessCardState.getIn(['actions', 'reports', businessCardID])) || null;
    }
  );
};
module.exports.makeSelectReportsActionBusinessCard = makeSelectReportsActionBusinessCard;

const makeSelectCreateBusinessCard = () => {
  return createSelector(
    selecBusinessCard,
    (businessCardState) => {
      return makeSelect(businessCardState.get('create'));
    }
  );
};
module.exports.makeSelectCreateBusinessCard = makeSelectCreateBusinessCard;

const makeSelectDeleteBusinessCard = () => {
  return createSelector(
    selecBusinessCard,
    (businessCardState) => {
      return makeSelect(businessCardState.get('delete'));
    }
  );
};
module.exports.makeSelectDeleteBusinessCard = makeSelectDeleteBusinessCard;

const makeSelectReUploadBusinessCard = () => {
  return createSelector(
    selecBusinessCard,
    (businessCardState) => {
      return makeSelect(businessCardState.get('reUpload'));
    }
  );
};
module.exports.makeSelectReUploadBusinessCard = makeSelectReUploadBusinessCard;

const makeSelectListBusinessCards = () => {
  return createSelector(
    selecBusinessCard,
    (businessCardState) => {
      return makeSelect(businessCardState.get('list'));
    }
  );
};
module.exports.makeSelectListBusinessCards = makeSelectListBusinessCards;
